<!-- UPDATE STATUS MODAL -->
<!--Update Status Modal -->
  <div class="modal fade" id="updateStatus-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Status</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="updateStatusForm" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="jobStatus">Job Status</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a status">
                            <option>In progress</option>
                            <option>Await approval</option>
                            <option>Job booked</option>
                            <option>Completed</option>
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Notes</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" name="notes" id="notes" required placeholder=""></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" id="updateStatusBtn" class="btn btn-success">Update Status</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->

<script>    
    $(function() {
    $("#date").datepicker({
        dateFormat:"yy/mm/dd",
        maxDate: new Date(), 
        minDate: new Date()});
    $("#date").datepicker("setDate", new Date());
    });
</script>