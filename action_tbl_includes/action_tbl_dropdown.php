<div class="btn-group">
    <button style="color: white; text-decoration: none; background-color: #17a589; font-size: 11px;" class="btn dropdown-toggle" href="#" data-toggle="dropdown">
    Action <span class="caret"></span>
    </button>
    <ul class="dropdown-menu stay-open pull-right" role="menu" style="min-width: 150px;">
        <li><a href="#"><span class="glyphicon glyphicon-pencil addglyphicon"></span> Edit</a></li>
        <li class="sweet-4"><a href="#"><span class="glyphicon glyphicon-trash addglyphicon"></span> Delete</a></li>
        <li class="divider"></li>
        <li><a href="#addNonconformance-modal" data-toggle="modal" data-target="#addNonconformance-modal"><span class="glyphicon glyphicon-plus addglyphicon"></span> Add Non-Conformance</a></li>
        <li><a href="#addCorrectiveAction-modal" data-toggle="modal" data-target="#addCorrectiveAction-modal"><span class="glyphicon glyphicon-plus addglyphicon"></span> Add Corrective Action</a></li>
        <li><a href="#updateStatus-modal" data-toggle="modal" data-target="#updateStatus-modal"><span class="glyphicon glyphicon-plus addglyphicon"></span> Update Status</a></li>
        <li><a href="#masterSignoff-modal" data-toggle="modal" data-target="#masterSignoff-modal"><span class="glyphicon glyphicon-plus addglyphicon"></span> Master Sign Off</a></li>
    </ul>
</div>

<?php 
    include 'update_status_modal.php';
    include 'master_signoff_modal.php';
?>
<script src="js/bootstrap-select.js"></script>

<script>
    $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
         $('.table-responsive').css( "overflow", "auto" );
    });
</script>
<script>
var buttons = document.querySelectorAll('.sweet-4');

for (var i = 0; i < buttons.length; i++) {
  buttons[i].onclick = function(){
	swal({
          title: "Are you sure?",
          text: "You will not be able to recover this data!",
          type: "input",
          showCancelButton: true,
          confirmButtonClass: 'btn-danger',
          confirmButtonText: 'Yes, delete it!',
          closeOnConfirm: false,
          //closeOnCancel: false
          inputPlaceholder: "Reason For Deleting"
        },
        function (inputValue) {
        if (inputValue === false) return false;
        if (inputValue === "") {
          swal.showInputError("You need to provide a reason for deleting!");
          return false;
        }
        swal("Deleted!", "Reason For Deletion: " + inputValue, "success");
      });
};
}
</script>