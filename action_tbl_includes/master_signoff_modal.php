<!-- MASTER SIGN OFF MODAL -->
<!-- Investigate Modal -->
  <div class="modal fade" id="masterSignoff-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Master Sign Off</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="masterSignoffForm" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Notes</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" name="notes" id="notes" required placeholder=""></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="password">Password</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="password" class="form-control" name="password" id="password" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" id="masterSignoffBtn" class="btn btn-success">Sign Off</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->