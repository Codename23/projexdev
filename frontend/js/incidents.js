$(document).ready(function () {
    $("#addNewResourceBtn").click(function () {
        $("#addNewResource-modal").modal('toggle');
        $("#AssetDescription").val('');
    });
    $("#addNewEmployeeBtn").click(function () {
        $("#addNewEmployee-modal").modal('toggle');
    });
    $("#addNewCustomerBtn").click(function () {
        $("#addNewCustomer-modal").modal('toggle');
    });
    $("#addNewServiceProviderBtn").click(function () {
        $("#addNewServiceProvider-modal").modal('toggle');
    });
    $("#addNewSupplierBtn").click(function () {
        $("#addNewSupplier-modal").modal('toggle');
        
    });

    $("#personInvolvedEmployeeBtn").hide();
    $("#personInvolvedCustomerBtn").hide();
    $("#personInvolvedServiceProviderBtn").hide();
    $("#personInvolvedSupplierBtn").hide();
    $("#personInvolvedVisitorBtn").hide();
    $("#personInvolvedPublicBtn").hide();
    
    $('.personInvolvedType').click(function () {
        if ($(this).attr("value") === "1") {
            $(".incidentBox").not(".incidentEmployee").hide();
            $("#personInvolvedEmployeeBtn").show();
            $(".incidentEmployee").show();
            $("#personInvolvedCustomerBtn").hide();
            $("#personInvolvedServiceProviderBtn").hide();
            $("#personInvolvedSupplierBtn").hide();
            $("#personInvolvedVisitorBtn").hide();
            $("#personInvolvedPublicBtn").hide();
        }
        if ($(this).attr("value") === "2") {
            $(".incidentBox").not(".incidentCustomer").hide();
            $("#personInvolvedCustomerBtn").show();
            $("#personInvolvedEmployeeBtn").hide();
            $("#personInvolvedServiceProviderBtn").hide();
            $("#personInvolvedSupplierBtn").hide();
            $("#personInvolvedVisitorBtn").hide();
            $("#personInvolvedPublicBtn").hide();
            $(".incidentCustomer").show();
        }
        if ($(this).attr("value") === "3") {
            $(".incidentBox").not(".incidentServiceProvider").hide();
            $("#personInvolvedServiceProviderBtn").show();
            $("#personInvolvedEmployeeBtn").hide();
            $("#personInvolvedCustomerBtn").hide();
            $("#personInvolvedSupplierBtn").hide();
            $("#personInvolvedVisitorBtn").hide();
            $("#personInvolvedPublicBtn").hide();
            $(".incidentServiceProvider").show();
        }
        if ($(this).attr("value") === "4") {
            $(".incidentBox").not(".incidentSupplier").hide();
            $("#personInvolvedSupplierBtn").show();
            $("#personInvolvedEmployeeBtn").hide();
            $("#personInvolvedCustomerBtn").hide();
            $("#personInvolvedServiceProviderBtn").hide();
            $("#personInvolvedVisitorBtn").hide();
            $("#personInvolvedPublicBtn").hide();
            $(".incidentSupplier").show();
        }
        if ($(this).attr("value") === "5") {
            $(".incidentBox").not(".incidentVisitor").hide();
            $("#personInvolvedVisitorBtn").show();
            $("#personInvolvedEmployeeBtn").hide();
            $("#personInvolvedCustomerBtn").hide();
            $("#personInvolvedServiceProviderBtn").hide();
            $("#personInvolvedSupplierBtn").hide();
            $("#personInvolvedPublicBtn").hide();
            $(".incidentVisitor").show();
        }
        if ($(this).attr("value") === "6") {
            $(".incidentBox").not(".incidentPublic").hide();
            $("#personInvolvedPublicBtn").show();
            $("#personInvolvedEmployeeBtn").hide();
            $("#personInvolvedCustomerBtn").hide();
            $("#personInvolvedServiceProviderBtn").hide();
            $("#personInvolvedSupplierBtn").hide();
            $("#personInvolvedVisitorBtn").hide();
            $(".incidentPublic").show();
        }
    });

    $("#witnessEmployeeBtn").hide();
    $("#witnessCustomerBtn").hide();
    $("#witnessServiceBtn").hide();
    $("#witnessSupplierBtn").hide();
    $("#witnessVisitorBtn").hide();
    $("#witnessPublicBtn").hide();

    $('.witnessType').click(function () {
        if ($(this).attr("value") === "1") {
            $(".incidentBox").not(".incidentEmployee").hide();
            $("#witnessEmployeeBtn").show();
            $("#witnessCustomerBtn").hide();
            $("#witnessServiceBtn").hide();
            $("#witnessSupplierBtn").hide();
            $("#witnessVisitorBtn").hide();
            $("#witnessPublicBtn").hide();
            $(".incidentEmployee").show();
        }
        if ($(this).attr("value") === "2") {
            $(".incidentBox").not(".incidentCustomer").hide();
            $("#personInvolvedCustomerBtn").show();
            $("#witnessCustomerBtn").show();
            $("#witnessEmployeeBtn").hide();
            $("#witnessServiceBtn").hide();
            $("#witnessSupplierBtn").hide();
            $("#witnessVisitorBtn").hide();
            $("#witnessPublicBtn").hide();
            $(".incidentCustomer").show();
        }
        if ($(this).attr("value") === "3") {
            $(".incidentBox").not(".incidentServiceProvider").hide();
            $("#witnessServiceBtn").show();
            $("#witnessEmployeeBtn").hide();
            $("#witnessCustomerBtn").hide();
            $("#witnessSupplierBtn").hide();
            $("#witnessVisitorBtn").hide();
            $("#witnessPublicBtn").hide();
            $(".incidentServiceProvider").show();
        }
        if ($(this).attr("value") === "4") {
            $(".incidentBox").not(".incidentSupplier").hide();
            $("#witnessSupplierBtn").show();
            $("#witnessEmployeeBtn").hide();
            $("#witnessCustomerBtn").hide();
            $("#witnessServiceBtn").hide();
            $("#witnessVisitorBtn").hide();
            $("#witnessPublicBtn").hide();
            $(".incidentSupplier").show();
        }
        if ($(this).attr("value") === "5") {
            $(".incidentBox").not(".incidentVisitor").hide();
            $("#witnessVisitorBtn").show();
            $("#witnessEmployeeBtn").hide();
            $("#witnessCustomerBtn").hide();
            $("#witnessServiceBtn").hide();
            $("#witnessSupplierBtn").hide();
            $("#witnessPublicBtn").hide();
            $(".incidentVisitor").show();
        }
        if ($(this).attr("value") === "6") {
            $(".incidentBox").not(".incidentPublic").hide();
            $("#witnessPublicBtn").show();
            $("#witnessEmployeeBtn").hide();
            $("#witnessCustomerBtn").hide();
            $("#witnessServiceBtn").hide();
            $("#witnessSupplierBtn").hide();
            $("#witnessVisitorBtn").hide();
            $(".incidentPublic").show();
        }
    });

    $('input[name="personInjured"]').click(function () {
        if ($(this).attr("value") === "1") {
            $(".incidentBox2").not(".injuries").hide();
            $(".injuries").show();
        } else {
            $(".injuries").hide();
        }
    });


    $("#investigationDate").datepicker({dateFormat: "yy/mm/dd"}).datepicker();
    $(function () {
        $("#incidentDate").datetimepicker({
            dateFormat: "yy/mm/dd",
            controlType: 'select',
            oneLine: true,
            timeFormat: 'hh:mm tt',
            maxDate: new Date(),
            changeYear: true,
            changeMonth: true,
            yearRange: "1900:3000"
        }).datetimepicker();
    });
});

$("#incidentShift").change(function(){
    var incidentShift = $(this).val();
    if(incidentShift === 'other'){
        $("#hideincidentShift").removeClass('hidden');
    }else{
        $("#hideincidentShift").addClass('hidden');
    }
});

$(document).ready(function () {
        $("#incidentCategory").select2({width: '100%'});
        $("#incidentType").select2({width: '100%'});
        $("#hideIncidentNumber").hide();
        $("#personInvolvedDepartment").select2({width: '100%'});
        $("#incidentAssetDepartment").select2({width: '100%'});
        $("#incidentAssetCategory").select2({width: '100%'});
        $("#incidentAssetCategory").select2({width: '100%'});
        showIncidentType();
        showIncidentAssetType($('#categoryType').val());
        showIncidentAssetType($('#incidentAssetCategory').val());
        providerCompanies('serviceproviderCompanyName');
        providerCompanies('witnessproviderCompanyName');
        supplierCompanies();
});

/**
 * Method to show incident number
 */
function showIncidentNumber(){
    $.ajax({
    type: "POST",
    url : "index.php?module=incidents&action=showIncidentNumber",
    data : "",
    dataType:'json',
        success: function(data){
            $("#incidentNumber").val(data);
            $("#showhideincident").addClass('show');
        },
        error: function(){
            console.log('Error Incident Number');
        }
    });  
}

$("#incidentCategory").change(function(){
    showIncidentType();
});

/**
 * Method to show incident types
 */
function showIncidentType() {
    var incidentCategoryId = $("#incidentCategory").val();
    $.ajax({
        type: "POST",
        url: "index.php?module=incidents&action=getIncidentTypesByCategoryId",
        dataType:'json',
        data: 'incidentCategory=' + incidentCategoryId,
        success: function (categoryTypes) {
            $('#incidentType').empty();
            if (categoryTypes.length > 0) {
                $.each(categoryTypes, function(key, val) {
                    $('#incidentType').append($('<option>').text(val.type_name).attr('value',val.id));
                });
            }
        },
        error: function () {
            console.log('Error show incident types');
        }
    });
}

//------------methods for reported person-------------
$("#reportedPerson").keyup(function(){  
    if(this.value.length > 3 ){
        searchReportedPerson();
    }
});

$('body').on('click', '.reportedPersonBtn', function(){
    var incidentReportedPerson = $('.reportedPersonName').text();
    $('#reportedPerson').val(incidentReportedPerson);
    
    var reportedPersonID = $('.reportedPersonBtn').val();
    $('#reportedPersonID').val(reportedPersonID);
    
    $('#reportedPersonResults').addClass("hidden");
    addIncident(reportedPersonID);
    showIncidentNumber();
    showReportedPerson();

});

/**
 * Method to search reported person
 */
function searchReportedPerson(){
    var reportedPerson = $("#reportedPerson").val();
    var reportedPersonDepartment = $("#reportedPersonDepartment").val();
    $.ajax({
    type: "POST",
    url : "index.php?module=employee&action=searchEmployee",
    dataType:'json',
    data :"searchterm="+reportedPerson+"&department="+reportedPersonDepartment,
        success: function (data){
            $('#reportedPersonResults tr td').empty();
            if(data){
               var Arrlength = data.length;
               var text = "";
                if(Arrlength > 0){
                   for(var i=0; i<Arrlength; i++){
                        text +="<tr><td>"+data[i].branch_name+"</td><td>"+data[i].department_name+"</td><td>"+data[i].firstname+" , "+data[i].lastname+"</td><td><button type='button' value="+data[i].id+" class='reportedPersonBtn btn btn-success btn-xs'><span class='glyphicon glyphicon-plus'></span></button></td></tr>";
                        if(text !==""){
                            $('#reportedPersonResults tr td').remove();
                            $('#reportedPersonResults').append(text).removeClass("hidden");
                        }
                    }
                }
            }else{
               $('#reportedPersonNotFound').removeClass("hidden");
               $('#reportedPersonResults').addClass("hidden");
            }
        },
        error: function () {
            console.log('Search failed');
        }
    });  
}
/**
 * Method to show reported person
 */
function showReportedPerson(){
    $.ajax({
    type: "POST",
    url : "index.php?module=incidents&action=showReportedPerson",
    data : "",
    dataType:'json',
        success: function(data){
            if(data){
               var Arrlength = data.length;
               var text = "";
                if(Arrlength > 0){
                   for(var i=0; i<Arrlength; i++){
                        if(data[i].id && data[i].firstname){
                           text +="<tr><td>"+data[i].firstname+"</td><td>"+data[i].lastname+"</td><td><a href='index.php?module=incidents&action=deleteReportedPerson?id="+data[i].id+"'><span class='glyphicon glyphicon-trash'></span></a></td></tr>";
                           if(text !==""){
                               $('#showReportedPerson tr td').remove();
                               $('#showReportedPerson').append(text).removeClass("hidden");
                           }
                        }
                    }
                }
            }
        },
        error: function(){
            alert('Reported Person');
        }
    });  
}
/*----------------------------End of reported person--------------------------*/

/*----------------------------Method to add Incident reported ----------------*/
function addIncident(reportedPersonID){
    $.ajax({
        type: "POST",
        url : "index.php?module=incidents&action=addIncident",
        dataType:'json',
        data:$('#incidentAddForm').serialize()+"&reportedPersonID="+reportedPersonID,
        success: function (data){
               if(data){
               var Arrlength = data.length;
                if(Arrlength > 0){
                   for(var i=0; i<Arrlength; i++){
                        if(data[i].incident_number){
                           $('#incidentNumber').val(data[i].incident_number);
                        }
                    }
                }
            }
        },
        error: function(){
            console.log('Add Incident Error');
        }
    });  
}
/*
 * Method to update incident
 */
function updateIncident(reportedPersonID){
    $.ajax({
        type: "POST",
        url : "index.php?module=incidents&action=updateIncident",
        data:$('#incidentAddForm').serialize()+"&reportedPersonID="+reportedPersonID,
        success:function(){
            
        },
        error: function(){
            console.log('Update Incident Error');
        }
    });  
}
/*-------------------------------End of incident reported --------------------*/

/*-------------------------------Person Involved -----------------------------*/
$("#personInvolvedFullname").keyup(function(){  
    if(this.value.length > 3 ){
        searchPersonInvolvedEmployee();
    }
});

$('body').on('click', '#personInvolvedEmployeeBtn', function(){
    addPersonInvolved();
    clearForm('incendentReportAddPersonForm');
});

$('body').on('click', '.personInvolvedEmployeeBtn', function(){
    var personInvolvedName = $(this).data("value");
    $('#personInvolvedFullname').val(personInvolvedName);
    
    var personInvolvedID = $(this).attr("value");
    $('#personInvolvedID').val(personInvolvedID);
    
    $("#personInvolvedResults").addClass("hidden"); 
});

/**
 * Method to search employee
 */
function searchPersonInvolvedEmployee(){
    var reportedPersonDepartment = $("#personInvolvedDepartment").val();
    var reportedPerson = $("#personInvolvedFullname").val();
    
    $.ajax({
    type: "POST",
    url : "index.php?module=employee&action=searchEmployee",
    dataType:'json',
    async:false,
    data :"searchterm="+reportedPerson+"&department="+reportedPersonDepartment,
     success: function (data){
            $('#personInvolvedResults tr td').empty();
            if(data){
               var Arrlength = data.length;
               var text = "";
                if(Arrlength > 0){
                   for(var i=0; i<Arrlength; i++){
                        text +="<tr><td>"+data[i].branch_name+"</td><td>"+data[i].department_name+"</td><td>"+data[i].firstname+" , "+data[i].lastname+"</td><td><button type='button' data-value='" +data[i].firstname+" , "+data[i].lastname+"' value='" +data[i].id+"' class='personInvolvedEmployeeBtn btn btn-success btn-xs'><span class='glyphicon glyphicon-plus'></span></button></td></tr>";
                         if(text !==""){
                            $('#personInvolvedResults').append(text).removeClass("hidden");            
                        }
                    }
                }   
            }else{
               $('#EmployeeNotFound').removeClass("hidden");
               $('#personInvolvedResults').addClass("hidden");
            }
        },
        error: function () {
           console.log('Search employee error');
        }
    });  
}

$("#customerNameSurname").keyup(function(){  
    if(this.value.length > 3 ){
        searchCustomerPersonInvolved();
    }
});

$('body').on('click', '.personInvolvedCustomerBtn', function(){
    var customerName = $(this).data("value");
    $('#customerNameSurname').val(customerName);
    
    var customerID = $(this).attr("value");
    $('#personInvolvedID').val(customerID);
    
    $('#searchCustomerPersonInvolved').addClass("hidden");
});

$('body').on('click', '#personInvolvedCustomerBtn', function(){
    addPersonInvolved();
    clearForm('incendentReportAddPersonForm');
});

/**
 * Method to search customer
 */
function searchCustomerPersonInvolved(){
    $.ajax({
        url : "index.php?module=client&action=searchClient",
        type: "POST",
        data :$("#incendentReportAddPersonForm").serialize(),
        dataType:'json',
        success: function (data){
            if(data){
               var Arrlength = data.length;
               var text = "";
                if(Arrlength > 0){
                   for(var i=0; i<Arrlength; i++){
                        text +="<tr><td>"+data[i].company_name+"</td><td>"+data[i].contact_person+"</td><td><button type='button' data-value='"+data[i].contact_person+"' value='" +data[i].id+"' class='personInvolvedCustomerBtn btn btn-success btn-xs'><span class='glyphicon glyphicon-plus'></span></button></td></tr>";
                        if(text !==""){
                            $('#searchCustomerPersonInvolved tr td').empty();
                            $('#searchCustomerPersonInvolved').append(text).removeClass("hidden");
                        }

                    }
                }
            }else{
                $('#customerPersonInvolvedNotFound').removeClass("hidden");
            }
        },
        error: function(error){
            console.log('Request failed: ' + error);
        }
    });
}

$("#serviceproviderNameSurname").keyup(function(){  
    if(this.value.length > 3 ){
        searchProviderPersonInvolved();
    }
});

$('body').on('click', '.personInvolvedProviderBtn', function(){
    var providerName = $(this).data("value");
    $('#serviceproviderNameSurname').val(providerName);
    
    var providerID = $(this).attr("value");
    $('#personInvolvedID').val(providerID);
    
    $('#searchProviderPersonInvolved').addClass("hidden");
});

$('body').on('click', '#personInvolvedServiceProviderBtn', function(){
    addPersonInvolved();
    clearForm('incendentReportAddPersonForm');
});
/**
 * Method to search provider
 */
function searchProviderPersonInvolved(){
    $.ajax({
        url : "index.php?module=provider&action=searchProvider",
        type: "POST",
        data :$("#incendentReportAddPersonForm").serialize(),
        dataType:'json',
        success: function (data){
            if(data){
               var Arrlength = data.length;
               var text = "";
                if(Arrlength > 0){
                   for(var i=0; i<Arrlength; i++){
                        text +="<tr><td>"+data[i].contact_person+"</td><td>"+data[i].provider_name+"</td><td><button type='button' data-value='"+data[i].contact_person+"' value='" +data[i].id+"' class='personInvolvedProviderBtn btn btn-success btn-xs'><span class='glyphicon glyphicon-plus'></span></button></td></tr>";
                        if(text !==""){
                            $('#searchProviderPersonInvolved tr td').remove();
                            $('#searchProviderPersonInvolved').append(text).removeClass("hidden");
                            $('#providerPersonInvolvedNotFound').addClass("hidden");
                        }

                    }
                }
            }else{
                $('#providerPersonInvolvedNotFound').removeClass("hidden");
                $('#searchProviderPersonInvolved').addClass("hidden");
            }
        },
        error: function(error){
            console.log('Request failed: ' + error);
        }
    });
}

$("#supplierNameSurname").keyup(function(){  
    if(this.value.length > 3 ){
        searchSupplierPersonInvolved();
    }
});

$('body').on('click', '.personInvolvedSupplierBtn', function(){
    
    var supplierName = $(this).data("value");
    $('#supplierNameSurname').val(supplierName);
    
    var supplierID = $(this).attr("value");
    $('#personInvolvedID').val(supplierID);
    
    $("#searchSupplierPersonInvolved").addClass("hidden");
    
});

$('body').on('click', '#personInvolvedSupplierBtn', function(){
    addPersonInvolved();
    clearForm('incendentReportAddPersonForm');
});
/**
 *Method to search supplier
 */
function searchSupplierPersonInvolved(){
    $.ajax({
        url : "index.php?module=supplier&action=searchSupplier",
        type: "POST",
        data :$("#incendentReportAddPersonForm").serialize(),
        dataType:'json',
        success: function (data){
            if(data){
               var Arrlength = data.length;
               var text = "";
                if(Arrlength > 0){
                   for(var i=0; i<Arrlength; i++){
                        text +="<tr><td>"+data[i].contact_person+"</td><td>"+data[i].supplier_name+"</td><td><button type='button' data-value='"+data[i].contact_person+"' value='" +data[i].id+"' class='personInvolvedSupplierBtn btn btn-success btn-xs'><span class='glyphicon glyphicon-plus'></span></button></td></tr>";
                        if(text !==""){
                            $('#searchSupplierPersonInvolved tr td').empty();
                            $('#searchSupplierPersonInvolved').append(text).removeClass("hidden");
                            $('#supplierPersonInvolvedNotFound').addClass("hidden");
                        }

                    }
                }
            }else{
                $('#searchSupplierPersonInvolved').addClass("hidden");
                $('#supplierPersonInvolvedNotFound').removeClass("hidden");
            }
        },
        error: function(error){
            console.log('Request failed: ' + error);
        }
    });
}

$('body').on('click', '#personInvolvedPublicBtn', function(){
    addPublicPerson();
    addPersonInvolved();
    clearForm('incendentReportAddPersonForm');
});
/**
 * Method to add person involved
 */
function addPublicPerson(){
    $.ajax({
    type: "POST",
    url : "index.php?module=incidents&action=savePublicPerson",
    dataType:'json',
    async:false,
    data : $('#incendentReportAddPersonForm').serialize(),
        success: function(data){
            $("#personInvolvedID").val('');
            $("#personInvolvedID").val(data);
        },
        error: function(){
            console.log('Visitor Error');
        }
    });  
}
$('body').on('click', '#personInvolvedVisitorBtn', function(){
    addVisitor();
    var personInvolvedID = $("#personInvolvedID").val();
    addPersonInvolved(personInvolvedID);
    clearForm('incendentReportAddPersonForm');
});
/**
 *Method to add visitor
 */
function addVisitor(){
    $.ajax({
    type: "POST",
    url : "index.php?module=incidents&action=saveVisitor",
    dataType:'json',
    async:false,
    data : $('#incendentReportAddPersonForm').serialize(),
        success: function(data){
            $("#personInvolvedID").val('');
            $("#personInvolvedID").val(data);
        },
        error: function(){
            console.log('Visitor Error');
        }
    });  
}
/**
 * Method to add person Involved
 */
function addPersonInvolved(){
    $.ajax({
    type: "POST",
    url : "index.php?module=incidents&action=savePersonInvolved",
    data : $('#incendentReportAddPersonForm').serialize(),
        success: function(){
            
        },
        error: function(){
            console.log('Person Involved Error');
        }
    });  
}
/*----------------------------End of Person Involved methods-----------------------------*/


/**
 *-------------------------- Asset functionality
 */
/**
 * Method to show asset involved
 */
function showAssetInvolved(){
    $.ajax({
    type: "POST",
    url : "index.php?module=incidents&action=showAssetInvolved",
    data : "",
    dataType:'json',
        success: function(data){
            if(data){
               var Arrlength = data.length;
               var text = "";
                if(Arrlength > 0){
                   for(var i=0; i<Arrlength; i++){
                        if(data[i].id && data[i].company_asset_number){
                           text +="<tr><td>"+data[i].department_name+"</td><td>"+data[i].category_name+"</td><td>"+data[i].assetType+"</td><td>"+data[i].name+"</td><td>"+data[i].company_asset_number+"</td><td><a href='index.php?module=incidents&action=deleteAssetInvolved?id="+data[i].id+"'><span class='glyphicon glyphicon-trash'></span></a></td></tr>";
                            if(text !==""){
                                $('#showAssetInvolved tr td').remove();
                               $('#showAssetInvolved').append(text).removeClass("hidden");
                           }
                        }
                    }
                }
            }
        },
        error: function(){
            console.log('Show Assert Error');
        }
    });  
}

$('body').on('click', '#addAssetBtn', function(){
    addNewAsset();
    clearForm('incendentNewAssetForm');
});

function addNewAsset(){
    $.ajax({
    type: "POST",
    url : "index.php?module=assets&action=saveAssetInvolved",
    data : $('#incendentNewAssetForm').serialize(),
        success: function(){
            
        },
        error: function(){
            console.log('Add Asset Involved Error');
        }
    });  
}

$("#incidentAssetDescription").keyup(function(){  
    if(this.value.length > 3 ){
        searchAssetInvolved();
    }
});

$('body').on('click', '.assetInvolvedID', function(){
    var assetInvolvedID = $(".assetInvolvedID").val();
    $('#assetInvolvedID').val(assetInvolvedID);
    var assetName = $(".assetName").text();
    $('#incidentAssetDescription').val(assetName);
    $("#assetInvolvedResults").hide();
});

$('body').on('click', '#addAssetInvolvedBtn', function(){
    var assetID = $("#assetInvolvedID").val();
    addAssetInvolved(assetID); 
    showAssetInvolved();
    clearForm('addAssetFormInvolved');
});

/**
 * Method to search asset involved
 */
function searchAssetInvolved(){
    var assetDepartment = $("#incidentAssetDepartment").val();
    var assetCategory = $("#incidentAssetCategory").val();
    var assetType = $("#incidentAssetType").val();
    var assetDescription = $("#incidentAssetDescription").val();
    var dataString = "incidentAssetDepartment="+assetDepartment+"&incidentAssetCategory="+assetCategory+"&incidentAssetType="+assetType+"&incidentAssetDescription="+assetDescription;
    $.ajax({
    type: "POST",
    url : "frontend/templates/incident/ajax/incident_search_asset.php",
    data : dataString,
        success: function(data){
            return $("#assetInvolvedResults").html(data);
        },
        error: function(){
            console.log('Asset Involved Error');
        }
    });  
}

$('body').on('change', '#incidentAssetCategory', function(){
    var categoryId = $("#incidentAssetCategory").val();
    showIncidentAssetType(categoryId);
});

$('body').on('change', '#categoryType', function(){
    var categoryId = $("#categoryType").val();
    showIncidentAssetType(categoryId);
});

/**
 * Method to show asset types
 */
function showIncidentAssetType(categoryId){
    $.ajax({
        url : "index.php?module=assets&action=showAssetTypes",
        type: "POST",
        data :'categoryId='+ categoryId,
        dataType:'json',
        success: function (categoryTypes){
            $('#incidentAssetType').empty();
            $('#assetType').empty();
            if (categoryTypes.length > 0) {
                $.each(categoryTypes, function(key, val) {
                    $('.incidentAssetType').append($('<option>').text(val.name).attr('value',val.id));
                });
            }
        },
        error: function(error){
            console.log('Request failed: ' + error);
        }
    });
}
/**
 * 
 *Method to add asset involved
 */
function addAssetInvolved(assetID){
    $.ajax({
    type: "POST",
    url : "index.php?module=incidents&action=saveAssetInvolved",
    data : $('#addAssetFormInvolved').serialize()+"&assetID="+assetID,
        success: function(){
            
        },
        error: function(){
            console.log('Asset Involved Error');
        }
    });  
}

/*-----------------------------------End of asset involved -------------------*/

$('body').on('click', '#addPartyResourceBtn', function(){
    addThirdPartyAsset();
    showThirdPartyAsset();
    clearForm('addPartyResourceForm');
});

/**
 *Method to show third party resource
 *
 */
function showThirdPartyAsset(){
    $.ajax({
    type: "POST",
    url : "index.php?module=incidents&action=showThirdPartyAsset",
    data : "",
    dataType:'json',
        success: function(data){  
            if(data){
               var Arrlength = data.length;
               var text = "";
                if(Arrlength > 0){
                   for(var i=0; i<Arrlength; i++){
                        if(data[i].id && data[i].assetType){
                           text +="<tr><td>"+data[i].assetType+"</td><td>"+data[i].description+"</td><td>"+data[i].company_asset_number+"</td><td>"+data[i].company_asset_number+"</td><td><a href='index.php?module=incidents&action=deleteAssetInvolved?id="+data[i].id+"'><span class='glyphicon glyphicon-trash'></span></a></td></tr>";
                            if(text !==""){
                               $('#showThirdPartyAsset tr td').remove();
                               $('#showThirdPartyAsset').append(text).removeClass("hidden");
                           }
                        }
                    }
                }
            }
            
        },
        error: function(){
            console.log('Show Third Party Asset Error');
        }
    });  
}
/**
 *Method to add third party asset
 */
function addThirdPartyAsset(){
    $.ajax({
    type: "POST",
    url : "index.php?module=incidents&action=saveThirdPartyAsset",
    data : $('#addPartyResourceForm').serialize(),
        success: function(){
            
        },
        error: function(){
            console.log('Third Party Asset Involved Error');
        }
    });  
}
/**------------------------End of third party asset----------------------------*/

/**------------------------Functions for wintness----------------------------*/
$("#witnessEmployeeFullname").keyup(function(){  
    if(this.value.length > 3 ){
        searchWitnessEmployee();
    }
});

$('body').on('click', '.witnessEmployeeBtn', function(){
    var witnessID = $(this).attr("value");
    var employeeWitnessName = $(this).data("value");
    
    $('#witnessEmployeeFullname').val(employeeWitnessName);
    $('#witnessID').val(witnessID);
    
    $("#searchWitnessCustomer").addClass("hidden");  
});

$('body').on('click', '#witnessEmployeeBtn', function(){
    addWitnessInvolved();
    clearForm('incendentReportAddWitnessForm');
});

/**
 * Method to search employee
 */
function searchWitnessEmployee(){
    var witnessDepartment = $("#witnessEmployeeDepartment").val();
    var witnessFullname = $("#witnessEmployeeFullname").val();
    
    $.ajax({
    type: "POST",
    url : "index.php?module=employee&action=searchEmployee",
    dataType:'json',
    async:false,
    data :"searchterm="+witnessFullname+"&department="+witnessDepartment,
     success: function (data){
            $('#witnessEmployeeResults tr td').empty();
            if(data){
               var Arrlength = data.length;
               var text = "";
                if(Arrlength > 0){
                   for(var i=0; i<Arrlength; i++){
                        text +="<tr><td>"+data[i].branch_name+"</td><td>"+data[i].department_name+"</td><td>"+data[i].firstname+" , "+data[i].lastname+"</td><td><button type='button' data-value='"+data[i].firstname+" "+data[i].lastname+"' value='" +data[i].id+"' class='witnessEmployeeBtn btn btn-success btn-xs'><span class='glyphicon glyphicon-plus'></span></button></td></tr>";
                         if(text !==""){
                            $('#witnessEmployeeResults').append(text).removeClass("hidden");            
                        }
                    }
                }   
            }else{
               $('#WitnessEmployeeNotFound').removeClass("hidden");
               $('#witnessEmployeeResults').addClass("hidden");
            }
        },
        error: function () {
           console.log('Search employee error');
        }
    });  
}

$("#witnessCustomerFullname").keyup(function(){  
    if(this.value.length > 3 ){
        searchWitnessCustomer();
    }
});

$('body').on('click', '.witnessCustomerBtn', function(){  
    var customerName = $(this).data("value");
    $('#witnessCustomerFullname').val(customerName);
    
    var customerID = $(this).attr("value");
    $('#witnessID').val(customerID);
    
    $("#searchWitnessCustomer").addClass("hidden"); 
    
});

$('body').on('click', '#witnessCustomerBtn', function(){  
    addWitnessInvolved();
    clearForm('incendentReportAddWitnessForm');
});

/**
 *Method to search customer
 */
function searchWitnessCustomer(){
    var customerNameSurname = $("#witnessCustomerFullname").val();
    var customerCompanyName = $("#witnessCustomerCompanyName").val();
    $.ajax({
        url : "index.php?module=client&action=searchClient",
        type: "POST",
        data :"customerNameSurname="+customerNameSurname+"&customerCompanyName="+customerCompanyName,
        dataType:'json',
        success: function (data){
            if(data){
               var Arrlength = data.length;
               var text = "";
                if(Arrlength > 0){
                   for(var i=0; i<Arrlength; i++){
                        text +="<tr><td>"+data[i].company_name+"</td><td>"+data[i].contact_person+"</td><td><button type='button' data-value='"+data[i].contact_person+"' value='" +data[i].id+"' class='witnessCustomerBtn btn btn-success btn-xs'><span class='glyphicon glyphicon-plus'></span></button></td></tr>";
                        if(text !==""){
                            $('#searchWitnessCustomer tr td').empty();
                            $('#searchWitnessCustomer').append(text).removeClass("hidden");
                        }

                    }
                }
            }else{
                $('#customerWitnessNotFound').removeClass("hidden");
                $('#searchWitnessCustomer').addClass("hidden");
            }
        },
        error: function(error){
            console.log('Request failed: ' + error);
        }
    });
}

$("#witnessProviderNameSurname").keyup(function(){  
    if(this.value.length > 3 ){
        searchWitnessProvider();
    }
});

$('body').on('click', '.witnessProviderBtn', function(){
    var providerName = $(this).data("value");
    $('#witnessProviderNameSurname').val(providerName);
    
    var providerID = $(this).attr("value");
    $('#witnessID').val(providerID);
    
    $("#searchWitnessProvider").addClass("hidden");
    $("#witnessProviderNotFound").addClass("hidden");
    
});

$('body').on('click', '#witnessServiceBtn', function(){
    addWitnessInvolved();
    clearForm('incendentReportAddWitnessForm');
});

/**
 * Method to search service provider
 */
function searchWitnessProvider(){
    var serviceproviderNameSurname = $("#witnessProviderNameSurname").val();
    var serviceproviderCompanyName = $("#witnessproviderCompanyName").val();
    $.ajax({
        url : "index.php?module=provider&action=searchProvider",
        type: "POST",
        data :"serviceproviderNameSurname="+serviceproviderNameSurname+"&serviceproviderCompanyName="+serviceproviderCompanyName,
        dataType:'json',
        success: function (data){
            if(data){
               var Arrlength = data.length;
               var text = "";
                if(Arrlength > 0){
                   for(var i=0; i<Arrlength; i++){
                        text +="<tr><td>"+data[i].contact_person+"</td><td>"+data[i].provider_name+"</td><td><button type='button' data-value='"+data[i].contact_person+"' value='" +data[i].id+"' class='witnessProviderBtn btn btn-success btn-xs'><span class='glyphicon glyphicon-plus'></span></button></td></tr>";
                        if(text !==""){
                            $('#searchWitnessProvider tr td').empty();
                            $('#searchWitnessProvider').append(text).removeClass("hidden");
                            $('#witnessProviderNotFound').addClass("hidden");
                        }

                    }
                }
            }else{
                $('#witnessProviderNotFound').removeClass("hidden");
                $('#searchWitnessProvider').addClass("hidden");
            }
        },
        error: function(error){
            console.log('Request failed: ' + error);
        }
    });
}

$("#witnessSupplierName").keyup(function(){  
    if(this.value.length > 3 ){
        searchWitnessSupplier();
    }
});

$('body').on('click', '.witnessSupplierBtn', function(){ 
    var supplierName = $(this).data("value");
    $('#witnessSupplierName').val(supplierName);
    
    var supplierID = $(this).attr("value");
    $('#witnessID').val(supplierID);
    
    $("#searchWitnessSupplier").addClass("hidden");
    $("#witnessSupplierNotFound").addClass("hidden");
    
});

$('body').on('click', '#witnessSupplierBtn', function(){ 
    addWitnessInvolved();
    clearForm('incendentReportAddWitnessForm');
});

/**
 * Method to search supplier
 */
function searchWitnessSupplier(){
    var witnessSupplierName = $("#witnessSupplierName").val();
    var witnessCompanyName = $("#witnessSupplierCompanyName").val();
    $.ajax({
        url : "index.php?module=supplier&action=searchSupplier",
        type: "POST",
        data :"supplierNameSurname="+witnessSupplierName+"&supplierCompanyName="+witnessCompanyName,
        dataType:'json',
        success: function (data){
            if(data){
               var Arrlength = data.length;
               var text = "";
                if(Arrlength > 0){
                   for(var i=0; i<Arrlength; i++){
                        text +="<tr><td>"+data[i].contact_person+"</td><td>"+data[i].supplier_name+"</td><td><button type='button' data-value='"+data[i].contact_person+"' value='" +data[i].id+"' class='witnessSupplierBtn btn btn-success btn-xs'><span class='glyphicon glyphicon-plus'></span></button></td></tr>";
                        if(text !==""){
                            $('#searchWitnessSupplier tr td').empty();
                            $('#searchWitnessSupplier').append(text).removeClass("hidden");
                            $('#witnessSupplierNotFound').addClass("hidden");
                        }

                    }
                }
            }else{
                $('#searchWitnessSupplier').addClass("hidden");
                $('#witnessSupplierNotFound').removeClass("hidden");
            }
        },
        error: function(error){
            console.log('Request failed: ' + error);
        }
    });
}

$('body').on('click', '#witnessVisitorBtn', function(){
   addWitnessVisitor(); 
   addWitnessInvolved();
   clearForm('incendentReportAddWitnessForm');
});
/**
 * Method to add visitor
 */
function addWitnessVisitor(){
    $.ajax({
    type: "POST",
    url : "index.php?module=incidents&action=saveWitnessVisitor",
    dataType:'json',
    async:false,
    data : $('#incendentReportAddWitnessForm').serialize(),
        success: function(data){
            $("#witnessID").val('');
            $("#witnessID").val(data);
        },
        error: function(){
            console.log('Visitor Error');
        }
    });  
}

$('body').on('click', '#witnessPublicBtn', function(){
    addWitnessPublic();
    addWitnessInvolved();
    clearForm('incendentReportAddWitnessForm');
});
/**
 * Method to add public person
 */
function addWitnessPublic(){
    $.ajax({
    type: "POST",
    url : "index.php?module=incidents&action=saveWitnessPublic",
    dataType:'json',
    async:false,
    data : $('#incendentReportAddWitnessForm').serialize(),
        success: function(data){
            $("#witnessID").val('');
            $("#witnessID").val(data);
        },
        error: function(){
            console.log('Visitor Error');
        }
    });  
}

/**
 * Method to add witness involved
 */
function addWitnessInvolved(){
    $.ajax({
    type: "POST",
    url : "index.php?module=incidents&action=saveWitness",
    data : $('#incendentReportAddWitnessForm').serialize(),
        success: function(){
            
        },
        error: function(){
            console.log('Witness Involved error');
        }
    });  
}
/*-------------------------------End of witness involved---------------------------------*/

/*-------------------------------------------Add forms ---------------------------------*/
$('body').on('click', '#employeeBtn', function(){
    addNewEmployeeForm();
    clearForm('frmAddEmployee');
});
/**
 * Method to add new employee
 */
function addNewEmployeeForm(){
    $.ajax({
    type: "POST",
    url : "index.php?module=employee&action=addEmployee",
    data : $('#frmAddEmployee').serialize(),
        success: function(data){
            return $("#frmAddEmployee").html(data);
        },
        error: function(){
            console.log('Asset Involved Error');
        }
    });  
}

$('body').on('click', '#addServiceProvider', function(){
    addNewServiceProviderForm();
    clearForm('addServiceProviderForm');
});

/**
 * Method to add new service provider
 */
function addNewServiceProviderForm(){
    $.ajax({
    type: "POST",
    url : "index.php?module=provider&action=addProvider",
    data : $('#addServiceProviderForm').serialize(),
        success: function(){
            
        },
        error: function(){
            console.log('Asset Involved Error');
        }
    });  
}

$('body').on('click', '#addSupplierBtn', function(){
    addNewSupplierForm();
    clearForm('addSupplierForm');
});

/**
 * Method to add new supplier
 */
function addNewSupplierForm(){
    $.ajax({
    type: "POST",
    url : "index.php?module=supplier&action=addSupplier",
    data : $('#addSupplierForm').serialize(),
        success: function(data){
            
        },
        error: function(){
            console.log('Asset Involved Error');
        }
    });  
}

$('body').on('click', '#addNewCustomerBtn', function(){
    addNewClientForm();
    clearForm('AddNewCustomerForm');
});

/**
 * Method to add new client
 */
function addNewClientForm(){
    $.ajax({
    type: "POST",
    url : "index.php?module=client&action=addClient",
    data : $('#AddNewCustomerForm').serialize(),
        success: function(){
            
        },
        error: function(){
            alert('Client Error');
        }
    });  
}

$('body').on('change', '#customerCompanyType', function(){
    var companies = $("#customerCompanyType").val();
    if(companies === 'individual'){
        $('.customerCompanyName').addClass('hidden');
    }else{
        customerCompanies('customerCompanyName');
    }
});

$('body').on('change', '#companyIndividual', function(){
    var companies = $("#companyIndividual").val();
    if(companies === 'individual'){
        $('.customerCompanyName').addClass('hidden');
    }else{
        customerCompanies('witnessCustomerCompanyName');
    }
});

/**
 * Method to get customer companies
 */
function customerCompanies(companyNameID){
    $.ajax({
        url : "index.php?module=incidents&action=getAllCustomerCompanies",
        type: "POST",
        data :"",
        dataType:'json',
        success: function (data){
            $('#'+companyNameID).empty();
            if (data.length > 0) {
                $.each(data, function(key, val) {
                    $('#'+companyNameID).append($('<option>').text(val.company_name).attr('value',val.id));
                });
            }
        },
        error: function(error){
            console.log('Request failed: ' + error);
        }
    });
}
/**
 * Method to get all provider companies
 */
function providerCompanies(companyProviderID){
    $.ajax({
        url : "index.php?module=incidents&action=getAllProviderCompanies",
        type: "POST",
        data :"",
        dataType:'json',
        success: function (data){
            $('#'+companyProviderID).empty();
            if (data.length > 0) {
                $.each(data, function(key, val) {
                    $('#'+companyProviderID).append($('<option>').text(val.provider_name).attr('value',val.id));
                });
            }
        },
        error: function(error){
            console.log('Request failed: ' + error);
        }
    });
}
/**
 * Method to get allsupplier companies
 * 
 */
function supplierCompanies(){
    $.ajax({
        url :"index.php?module=incidents&action=getAllSupplierCompanies",
        type:"POST",
        data:"",
        dataType:'json',
        success: function (data){
            if (data.length > 0) {
                $.each(data, function(key, val) {
                    $('#supplierCompanyName').append($('<option>').text(val.supplier_name).attr('value',val.id));
                    $('#witnessSupplierCompanyName').append($('<option>').text(val.supplier_name).attr('value',val.id));
                });
            }
        },
        error: function(error){
            console.log('Request failed: ' + error);
        }
    });
}

/*-------------------------------------------End of forms ---------------------------------*/
//clear forms
function clearForm(formName){
    $('#'+formName)[0].reset();
}