/**
 * Created by User on 2017/03/03.
 */
var selectedItems = [];
var currentAlbumId = null;

var triggeredLoaderModal = null;



function showLayer(layerId)
{
    // clear any previous ones
    clearLayers();

    // load layer
    $('#'+layerId).fadeIn(300);
}

function clearLayers()
{
    $('.layer').hide();
}

function loadImages(albumId, pageStart, perPage, filterOrderBy)
{
    if(albumId == null)
    {
        albumId = currentAlbumId;
    }
    if(typeof(pageStart) == 'undefined')
    {
        pageStart = 1;
    }
    if(typeof(perPage) == 'undefined')
    {
        perPage = 0;
    }
    if(typeof(filterOrderBy) == 'undefined')
    {
        filterOrderBy = '';
    }

    if (typeof (setUploadFolderId) === 'function')
    {
        setUploadFolderId(albumId);
    }
    setLastLoadedFolderCookie(currentAlbumId);
    currentAlbumId = albumId;
    successCallback = function(data) {
        //updatePageUrlBar(data.page_url, data.html, data.page_title);
        showLayer('main-ajax-container');
        //scrollTop();
        setupImageBrowsePage();
    }

    loadAjaxContent('#main-ajax-container', AJAX_ROOT+"module=ajax_request&action=ajaxShowFolderDetails", {nodeId: albumId, pageStart: pageStart, perPage: perPage, filterOrderBy: filterOrderBy}, successCallback);
}



function showFileInfo(fileId)
{
    //scrollTop();
    successCallback = function(data) {
        //loadSimilarImages(imageId);
        //updatePageUrlBar(data.page_url, data.html, data.page_title);
        //setupToolTips();
        showLayer('main-ajax-container');
        // setupMobileImageSwipe();
    }
    loadAjaxContent('#main-ajax-container', AJAX_ROOT+"module=ajax_request&action=ajaxShowFileDetails", {fileId: fileId}, successCallback);
}

function setupFileDragSelect()
{
    if (isDesktopUser() == true)
    {
        $('#fileManager .toolbar-container, #fileManager .gallery-env')
            .drag("start", function(ev, dd) {
                return $('<div class="fileManagerDraggleSelection" />')
                    .css('opacity', .50)
                    .appendTo(document.body);
            })
            .drag(function(ev, dd) {
                $(dd.proxy).css({
                    top: Math.min(ev.pageY, dd.startY),
                    left: Math.min(ev.pageX, dd.startX),
                    height: Math.abs(ev.pageY - dd.startY),
                    width: Math.abs(ev.pageX - dd.startX)
                });
            })
            .drag("end", function(ev, dd) {
                $(dd.proxy).remove();
            }, {distance: 10, not: $('div.image-thumb, li, span, a, img')});

        $('.fileIconLi:not(.fileDeletedLi)').draggable({
            revert: function(event, ui) {
                return !event;
            },
            containment: 'body',
            helper: function(event) {
                selectFile($(this).attr('fileId'), true);
                var ret = $(this).clone();
                var textStr = t('file_manager_moving', 'Moving') + ' ' + countSelected() + ' ' + t('file_manager_moving_files', 'file(s)');
                ret.find('.filename').html(textStr);
                ret.find('.back p').html(textStr);
                ret.find('.fileUploadDate').remove();
                ret.find('.filesize').remove();
                ret.find('.fileOptions').remove();
                ret.find('.downloads').remove();
                return ret;
            },
            opacity: 0.50,
            cursorAt: {left: 5, top: 5},
            distance: 10,
            start: function(event, ui)
            {
                selectFile($(this).attr('fileId'), true);
            },
            stop: function(event, ui)
            {
                // clear selected if only 1
                if (countSelected() == 1)
                {
                    elementId = 'fileItem' + $(this).attr('fileId');
                    $('.' + elementId).removeClass('selected');
                    delete selectedItems['k' + $(this).attr('fileId')];
                }
            }
        });

        setupTreeviewDropTarget();
    }
}

function setupTreeviewDropTarget()
{
    $(".jstree-no-dots li a").droppable({
        hoverClass: 'jstree-hovered',
        tolerance: "pointer",
        drop: function(event, ui) {
            folderId = $(this).parent().attr('id');
            moveFiles(folderId);
        }
    });

    $(".fileManager .fileListing .folderIconLi").droppable({
        hoverClass: 'jstree-hovered',
        tolerance: "pointer",
        drop: function(event, ui) {
            folderId = $(this).attr('folderid');
            moveFiles(folderId);
        }
    });
}
function setLastLoadedFolderCookie(folderId)
{
    $.cookie("jstree_select", "#"+folderId);
}

function setupImageBrowsePage()
{
    //formatThumbLayout();
    assignLiOnClick();
    //reloadDragItems();
    highlightSelected();
    //setupFileDragSelect();
    reSelectFolder();
    setupToolTips();
    // make sure treeview node is open
    $("#folderListView").jstree("open_node", $('.jstree #'+$('#nodeId').val()));
}

function assignLiOnClick()
{

    unbindLiOnClick();
    $(".fileManager .fileIconLi.owned-image a.fileDownload").click(function(e) {
        liElement = $(this).parents('.fileIconLi');
        return showFileMenu(liElement, e);
    });
    $(".fileManager .folderIconLi.owned-folder a.fileDownload").click(function(e) {
        liElement = $(this).parents('.folderIconLi');
        return showFolderMenu(liElement, e);
    });
    $(".fileManager .fileIconLi.owned-image, .fileManager .fileIconLi.not-owned-image").click(function(e) {
        e.stopPropagation();
        fileId = $(this).attr('fileId');
        selectFile(fileId);
    });
    assignLiRightClick();
}


function unbindLiOnClick()
{
    $(".fileManager .fileIconLi.owned-image").unbind('click');
    $(".fileManager .folderIconLi.owned-folder").unbind('click');
    unbindLiRightClick();
}

function unbindLiRightClick()
{
    $(".fileManager .fileIconLi.owned-image").unbind('contextmenu');
    $(".fileManager .folderIconLi.owned-folder").unbind('contextmenu');
    $(".fileManager").unbind('contextmenu');
}

function assignLiRightClick()
{

    $(".fileManager .fileIconLi.owned-image").bind('contextmenu', function(e) {
        return showFileMenu(this, e);
    });

    $(".fileManager .folderIconLi.owned-folder").bind('contextmenu', function(e) {
        return showFolderMenu(this, e);
    });

    $(".image-browse .toolbar-container, .image-browse .gallery-env, .image-browse .no-results-wrapper").bind('contextmenu', function(e) {
        e.stopPropagation();
        if((currentAlbumId == '-1') || ($.isNumeric(currentAlbumId)))
        {
            var items = {
                "Upload": {
                    "label": t('upload_files', 'Upload Files'),
                    "icon": "glyphicon glyphicon-cloud-upload",
                    "separator_after": true,
                    "action": function (obj) {
                        uploadFiles(currentAlbumId);
                    }
                },
                "Add": {
                    "label": t('add_sub_folder', 'Add Sub Folder'),
                    "icon": "glyphicon glyphicon-plus",
                    "separator_after": true,
                    "action": function (obj) {
                        showAddFolderForm(currentAlbumId);
                    }
                },
                "SelectAll": {
                    "label": t('account_file_details_select_all_files', 'Select All Files'),
                    "icon": "glyphicon glyphicon-check",
                    "action": function(obj) {
                        selectAllFiles();
                    }
                },
                "ClearAll": {
                    "label": t('account_file_details_clear_selected', 'Clear Selected'),
                    "icon": "glyphicon glyphicon-unchecked",
                    "action": function(obj) {
                        clearSelected();
                    }
                }
            };
        }
        else
        {
            var items = {
                "SelectAll": {
                    "label": t('account_file_details_select_all_files', 'Select All Files'),
                    "icon": "glyphicon glyphicon-check",
                    "action": function(obj) {
                        selectAllFiles();
                    }
                },
                "ClearAll": {
                    "label": t('account_file_details_clear_selected', 'Clear Selected'),
                    "icon": "glyphicon glyphicon-unchecked",
                    "action": function(obj) {
                        clearSelected();
                    }
                }
            };
        }
        $.vakata.context.show(items, $(this), e.pageX, e.pageY, this);
        return false;
    });

    // enable closing of context menus on left click
    $("body").click(function() {
        hideOpenContextMenus();
    });
}

function reSelectFolder()
{
    $('#folderListView .jstree-clicked').removeClass('jstree-clicked');
    if($('#folderListView #'+currentAlbumId+' > a').length > 0)
    {
        $('#folderListView #'+currentAlbumId+' > a').addClass('jstree-clicked');
    }
}

function loadAjaxContent(container, url, params, successCallback, showLayer)
{
    if(typeof(params) == 'undefined')
    {
        params = {};
    }

    if(typeof(showLayer) == 'undefined')
    {
        showLayer = true;
    }

    // store encase we need to refresh or go back
    lastAjaxFilter = {container: container, url: url, params: params, successCallback: successCallback};

    $.ajax({
        method: "POST",
        url: url,
        data: params
    })
        .done(function(data)
        {
            // response expects:
            // data.html
            // data.javascript

            // populate html
            $(container).html(data);
            if(showLayer == true)
            {
                $(container).show();
            }

            // fix heights before images are loads
            fixImageBrowseHeights();

            // eval any javascript
            if((typeof(data.javascript) != 'undefined') && (data.javascript.length > 0))
            {
                eval(data.javascript);
            }

            // call any additional functions
            if(successCallback && typeof(successCallback) === "function")
            {
                successCallback(data);
            }
        });
}



function toggleFullScreenMode()
{
    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
        (!document.mozFullScreen && !document.webkitIsFullScreen)) {
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}

function clearSearchFilters(doFilterLocal)
{
    if (typeof(doFilterLocal) == 'undefined')
    {
        doFilterLocal = true;
    }

    $('#filterText').val('');
    $('#filterUploadedDateRange').val('');
    $('#filterUploadedDateRange').parent().find('.daterange span').html('file_manager_select_range');

    if (doFilterLocal == true)
    {
        doFilter();
    }
}

function toggleViewType()
{
    if ($('.fileManager').hasClass('fileManagerList'))
    {
        $('.fileManager').removeClass('fileManagerList');
        $('.fileManager').addClass('fileManagerIcon');
        $('#viewTypeText').html('<i class="entypo-list"></i>');
        fixImageBrowseHeights();
    }
    else
    {

        $('.fileManager').addClass('fileManagerList');
        $('.fileManager').removeClass('fileManagerIcon');
        $('#viewTypeText').html('<i class="entypo-layout"></i>');
    }
    // store in session via ajax
    updateViewType();
}

function fixImageBrowseHeights(container)
{
    if(typeof(container) == "undefined")
    {
        container = '';
        if ($('.tab-pane.active').length)
        {
            container = '#'+$('.tab-pane.active').attr('id')+' ';
        }
    }
    $(container+'.fileIconLi').height($(container+'.fileIconLi').width());
}

function updateViewType()
{
    var viewType = 'fileManagerIcon';
    if ($('.fileManager').hasClass('fileManagerList'))
    {
        viewType = 'fileManagerList';
    }

    $.ajax({
        type:"POST",
        dataType: "json",
        url: AJAX_ROOT+"module=ajax_request&action=ajaxUpdateFolderView",
        data: {folderViewType: viewType},
        success: function(data) {
            // do nothing, assuming ok is fine in this instance
        },
        error: function(data) {
            // error
        }
    });
}

function clearSelected()
{
    selectedItems = [];
    $('.selected').removeClass('selected');
    //updateSelectedFilesStatusText();
    //updateFileActionButtons();
}


function highlightSelected()
{
    for (i in selectedItems)
    {
        elementId = 'fileItem' + selectedItems[i][0];
        $('.' + elementId+'.owned-image').addClass('selected');
    }
    //updateSelectedFilesStatusText();
}

function updateSelectedFilesStatusText()
{
    count = countSelected();
    if (count == 1)
    {
        totalFilesize = getSizeSelected();
        updateStatusText(count + ' '+ t('selected', 'selected') + '&nbsp;&nbsp;<a href="#" onClick="clearSelected(); return false;">('+t('selected_image_clear', 'clear')+')</a>');
    }
    else if (count > 1)
    {
        totalFilesize = getSizeSelected();
        updateStatusText(count + ' '+ t('selected', 'selected') + '&nbsp;&nbsp;<a href="#" onClick="clearSelected(); return false;">('+t('selected_image_clear', 'clear')+')</a>');
    }
    else if (count == 0)
    {
        updateStatusText(null);
    }
}

function updateStatusText(text)
{
    if (text != null)
    {
        text = '<i class="entypo-bag"></i> ' + text;
    }

    $('#statusText').html(text);
}

function hideOpenContextMenus()
{
    // hide any exiting context menus
    $.vakata.context.hide();
    $('[data-toggle="dropdown"]').parent().removeClass('open');
    clearExistingHoverFileItem();
}

function isPositiveInteger(str)
{
    var n = ~~Number(str);
    return n > 0;
}
// Element Attribute Helper
function attrDefault($el, data_var, default_val)
{
    if(typeof $el.data(data_var) != 'undefined')
    {
        return $el.data(data_var);
    }

    return default_val;
}

function setupToolTips()
{
    $('[data-toggle="tooltip"]').each(function(i, el)
    {
        var $this = $(el),
            placement = attrDefault($this, 'placement', 'top'),
            trigger = attrDefault($this, 'trigger', 'hover'),
            popover_class = $this.hasClass('tooltip-secondary') ? 'tooltip-secondary' : ($this.hasClass('tooltip-primary') ? 'tooltip-primary' : ($this.hasClass('tooltip-default') ? 'tooltip-default' : ''));

        $this.tooltip({
            placement: placement,
            trigger: trigger,
            container: 'body'
        });

        $this.on('shown.bs.tooltip', function(ev)
        {
            var $tooltip = $this.next();

            $tooltip.addClass(popover_class);
        });
    });
}


function showLoaderModal(timer)
{
    if(typeof(timer) == 'undefined')
    {
        timer = 500;
    }

    if(triggeredLoaderModal == null)
    {
        triggeredLoaderModal = setTimeout(showLoaderModal, timer);
        return false;
    }

    $('.loader-modal').modal('hide');
    var pleaseWaitDiv = $('<div class="modal custom-width loader-modal" id="loaderModal" data-keyboard="false"><div class="modal-dialog modal-dialog-center" style="width: 300px;"><div class="progress progress-striped active"><div class="progress-bar" style="width: 100%;"></div></div></div></div>');
    pleaseWaitDiv.modal();
    clearTimeout(triggeredLoaderModal);
    triggeredLoaderModal = null;
}



function hideLoaderModal()
{
    if(triggeredLoaderModal != null)
    {
        clearTimeout(triggeredLoaderModal);
        triggeredLoaderModal = null;
    }
    $('.loader-modal').modal('hide');
}