<?php 
$title = 'Recommended Improvement';
include_once('frontend/templates/headers/default_header_tpl.php');
?>

<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of navigation-->   
    
    <div class="col-lg-10">
        <!--sub menu-->
       <div class="row">
            <div class="col-lg-12">
            <ul class="nav nav-pills nav-justified topbar-menu">
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Non-Conformance</a></li>
                    <li><a href="improvement_incident.php">Incidents</a></li>
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Investigations</a></li>
                    <li ><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action">Corrective Actions</a></li>
                    <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Improvement</a></li> <!-- -->
                    <li><a href="improvement_objective.php">Objective</a></li>
            </ul>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-8">
            <ul class="nav nav-pills nav-justified topbar-menu">
<li><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Active</a></li>
                <li ><a href="<?php echo BASE_URL;?>/index.php?action=getRequestedImprovements&module=improvement">Requested</a></li>
                <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getRecommendedImprovement&module=improvement">Recommended</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getHistoryImprovements&module=improvement">History</a></li>
            </ul>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-8">
            <ul class="nav nav-pills nav-justified topbar-menu">
                <li class="active"><a href="improvement_recommended_improvement.php">Inspections</a></li>
                <li><a href="improvement_recommended_nonconformance.php">Non-Conformance</a></li>
                <li><a href="improvement_recommended_incidents.php">Incidents</a></li>
                <li><a href="improvement_recommended_risk_assessment.php">Risk Assessment</a></li>
            </ul>
            </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-heading">Search Inspections</div>
        <div class="panel-body">
        <form class="form-inline" method="post" name="search" action="">
            <div class="col-md-12" style="margin-bottom: 10px;">    
            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Inspection Type</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more inspection type"data-header="Close">
                    <option>Inspection type 1</option>
                    <option>Inspection type 2</option>
                    <option>Inspection type 3</option>
                    <option>Inspection type 4</option>
                    <option>Inspection type 5</option>
                </select>
            </div>
            </div>

            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Group</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more groups"data-header="Close">
                    <option value="1">OHS (Health & Safety)</option>
                    <option value="2">E (Environment)</option>
                    <option value="3">Q (Quality)</option>
                    <option value="4">F (Food)</option>
                </select>
            </div>
            </div>
            </div>

            <div style="margin-left: 30px;">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Search">
            </div>
            </div>
        </form>
        </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-heading">Inspections</div>
        <div class="panel-body">
        <div class="table-responsive">
            <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Inspection No.</th>
                    <th>Date</th>
                    <th>Inspection Type</th>
                    <th>Group</th>
                    <th>Non-Conformance</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>15 June 2016</td>
                    <td>Inspection</td>
                    <td>OHS</td>
                    <td>Fire Equipment inspection register</td>
                    <td><a href="#">Start Improvement</a></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>15 June 2016</td>
                    <td>Inspection</td>
                    <td>OHS</td>
                    <td>Fire Equipment inspection register</td>
                    <td><a href="#">Start Improvement</a></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>15 June 2016</td>
                    <td>Inspection</td>
                    <td>OHS</td>
                    <td>Fire Equipment inspection register</td>
                    <td><a href="#">Start Improvement</a></td>
                </tr>
            </tbody>
        </table>
        </div>

        </div>
        </div>
    </div>  
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  