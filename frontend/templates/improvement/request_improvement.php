<?php 
$title = 'Improvement Upcoming';
include_once('frontend/templates/headers/default_header_tpl.php');
?>

<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End
<!--End of navigation-->   
    
    <div class="col-lg-10">
        <!--sub menu-->
        <div class="row">
            <div class="col-lg-12">
            <ul class="nav nav-pills nav-justified topbar-menu">
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Non-Conformance</a></li>
                    <li><a href="improvement_incident.php">Incidents</a></li>
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Investigations</a></li>
                    <li ><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action">Corrective Actions</a></li>
                    <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Improvement</a></li> <!-- -->
                    <li><a href="improvement_objective.php">Objective</a></li>
            </ul>
            </div>
        </div> 
        
        <div class="row">
            <div class="col-lg-8">
            <ul class="nav nav-pills nav-justified topbar-menu">
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Active</a></li>
                <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getRequestedImprovements&module=improvement">Requested</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getRecommendedImprovement&module=improvement">Recommended</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getHistoryImprovements&module=improvement">History</a></li>
            </ul>
            </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-heading">Search Upcoming Improvements</div>
        <div class="panel-body">
        <form class="form-inline" method="post" name="search" action="">
            <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="col-md-4 col-sm-4">
            <div class="form-group">
                <p><b>Group</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more groups"data-header="Close">
                    <option value="1">OHS (Health & Safety)</option>
                    <option value="2">E (Environment)</option>
                    <option value="3">Q (Quality)</option>
                    <option value="4">F (Food)</option>
                </select>
            </div>
            </div>

            <div class="col-md-4 col-sm-4">
            <div class="form-group">
                <p><b>Department</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                    <option>I.T</option>
                    <option>Human Resources</option>
                    <option>Marketing</option>
                    <option>Support and Services</option>
                </select>
            </div>
            </div>
                
            <div class="col-md-4 col-sm-4">
            <div class="form-group">
                <p><b>Responsible Person</b></p>
                <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more people" data-header="Close">
                    <option value="1">Nick Botha</option>
                    <option value="2">Sunny Mathole</option>
                    <option value="3">Brian Douglas</option>
                </select> 
            </div>
            </div>
            </div>
            <div style="margin-left: 30px;">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Search">
            </div>
            </div>
        </form>
        </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-heading">Upcoming Improvements</div>
        <div class="panel-body">
        <div class="table-responsive">   
        
        <table width="100%" class="table table-hover">
          <thead>
            <tr>
              <th>Investigation</th>
              <th>No</th>
              <th>Date</th>
              <th>Group</th>
              <th>Department</th>
              <th>Non-Conformance</th>
              <th>Suggested Improvement</th>
              <th>Due Date</th>
              <th>Responsible Person</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Investigation</td>
              <td>021</td>
              <td>2017/02/01</td>
              <td>Food</td>
              <td>Receiving</td>
              <td>Piece of conveyor belt broken off</td>
              <td></td>
              <td>2017/03/20</td>
              <td>Nick Botha</td>
              <td><a href="">Create Improvement</a></td>
            </tr>
            <tr>
              <td>Investigation</td>
              <td>021</td>
              <td>2017/02/01</td>
              <td>OHS</td>
              <td>Welding</td>
              <td>Not using fall protection equipment</td>
              <td></td>
              <td>2017/03/19</td>
              <td>Nick Botha</td>
              <td><a href="">Create Improvement</a></td>
            </tr>
          </tbody>
        </table>

        </div>
        </div>
    </div>
    <!--End of the pane panel-default-->
    </div>  
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  