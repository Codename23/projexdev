<?php 
$title = 'All Improvement Full Strategy';
include_once('frontend/templates/headers/default_header_tpl.php');
?>

<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of navigation-->   
    
    <div class="col-lg-10">
        <!--sub menu-->
      <div class="row">
            <div class="col-lg-12">
            <ul class="nav nav-pills nav-justified topbar-menu">
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Non-Conformance</a></li>
                    <li><a href="improvement_incident.php">Incidents</a></li>
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Investigations</a></li>
                    <li ><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action">Corrective Actions</a></li>
                    <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Improvement</a></li> <!-- -->
                    <li><a href="improvement_objective.php">Objective</a></li>
            </ul>
            </div>
        </div>
        
          <div class="row">
            <div class="col-lg-8">
            <ul class="nav nav-pills nav-justified topbar-menu">
<li  class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Active</a></li>
                <li ><a href="<?php echo BASE_URL;?>/index.php?action=getRequestedImprovements&module=improvement">Requested</a></li>
                <li ><a href="<?php echo BASE_URL;?>/index.php?action=getRecommendedImprovement&module=improvement">Recommended</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getHistoryImprovements&module=improvement">History</a></li>
            </ul>
            </div>
        </div>
        
        <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Improvement Details</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">No</td>
                                    <td>123456789</td>
                                </tr>
                                <tr>
                                    <td>Group</td>
                                    <td>OHS</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Improvement Description</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Date Created</td>
                                    <td>2017/08/20</td>
                                </tr>
                                <tr>
                                    <td>Responsible Person</td>
                                    <td>Sunny Mathole</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Source Information</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Type</td>
                                    <td>Non-Conformance</td>
                                </tr>
                                <tr>
                                    <td>No</td>
                                    <td>123</td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td>2017/05/12</td>
                                </tr>
                                <tr>
                                    <td>Group</td>
                                    <td>OHS</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>PPE</td>
                                </tr>
                                <tr>
                                    <td>Reporting Person</td>
                                    <td>Pieter</td>
                                </tr>
                                <tr>
                                    <td>Non-Conformance</td>
                                    <td>Walk on roof without fall protection</td>
                                </tr>
                                <tr>
                                    <td>Link</td>
                                    <td>Non-Conformance</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Approval</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%"></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
        <div class="panel panel-default">
        <div class="panel-body">
            <a href="#addToList-modal" data-toggle="modal" data-target="#addToList-modal"><button type="button" id="addToList" class="btn btn-success">Add Strategy</button></a>
        </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-heading">Improvement Strategy</div>
        <div class="panel-body">
        <div class="table-responsive">            
            <table width="100%" class="table table-hover">
            <thead>
            <tr>
                <th>Step No.</th>
                <th>Strategy Description</th>
                <th>Responsible Person/s</th>
                <th>Due Date</th>
                <th>View Action Taken</th>
                <th>Status</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <?php
                $step_counter = 0;
                 if(isset($data["allStrategy"]) && !empty($data["allStrategy"]))
                 {
                         foreach($data["allStrategy"] as $strategy)
                         { 
                             $step_counter++;
                    ?>
                    <tr class="<?php
                    if(!empty($strategy['signedOffBy']))
                    {
                        echo 'bg-success';
                    }
                    ?>">
                    <td>
                    <?php echo $step_counter;?></td>
                    <td><?php echo $strategy['description'];?></td>
                    <td><?php echo $strategy['responsible_person'];?></td>
                    <td><?php echo $strategy['duedate'];?></td>
                    <td><a href="#masterSignOff-modal" data-toggle="modal" data-target="#masterSignOff-modal">View Actions</a></td>
                    <td></td>
                    <td><?php
                    if(empty($strategy['signedOffBy']))
                    {
                       ?><a href="#masterSignOff-modal" class="signed_off_invoke" str="<?php echo $strategy['stid'];?>" data-toggle="modal" data-target="#masterSignOff-modal">Sign Off</a><?php
                    }
 else {
      
                    if(!empty($strategy['signedOffBy']))
                    {
                    ?><span class="glyphicon glyphicon-log-out"></span>
                    <?php
                    }
 }
                    ?></td>
                    <td><?php
                    if(empty($strategy['signedOffBy']))
                    {
                       ?><a href="#"><span class="glyphicon glyphicon-pencil"></span></a><?php
                    }
                    ?></td>
                    </tr>
                    <?php
                         }
                }
                ?>                
            </tbody>
        </table>
        </div>

        </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-body">
            <a href="#improvementMasterSignoff-modal" data-toggle="modal" data-target="#improvementMasterSignoff-modal"><button type="button" id="improvementMasterSignoff" class="btn btn-success">Master Sign Off</button></a>
        </div>
        </div>
    
    <!-- Add To Strategy List Modal -->
  <div class="modal fade" id="addToList-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Improvement Strategy To Do List</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addToListForm" action="<?php echo BASE_URL;?>/index.php?action=submitStrategy&module=improvement">
                <input type="hidden" name="txtHidden" value="<?php 
                if(isset($_GET['id']))
                {
                    echo (int)$_GET["id"];
                }
                if(isset($_POST['txtHidden']))
                {
                    echo (int)$_POST["txtHidden"];
                }
?>"/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="responsiblePerson">Responsible Person / Persons</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" required="true" name="strategy_person" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people">
                        <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>
                </select>
                </div>
            </div>   
            <div class="form-group">
                <label class="control-label col-sm-4" for="dueDate">Due Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="dueDate" id="dueDate"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="strategyDescription">Strategy Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" required="true" name="strategyDescription" id="strategyDescription"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="btnSubmitStrategy" name="btnSubmitStrategy" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Master Sign Off Modal -->
  <div class="modal fade" id="masterSignOff-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Master Sign Off</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="masterSignOffForm" action="<?php echo BASE_URL;?>/index.php?action=signedOffImprovement&module=improvement">
                <input type="hidden" name="txtHiddenSignedOff" id="txtHiddenSignedOff"/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="dateCompleted">Date Objective Completed</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="dateCompleted" id="dateCompleted"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="action_taken">Action Taken</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" name="action_taken" id="action_taken"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="signoffPassword">Password</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="password" class="form-control" name="signoffPassword" id="signoffPassword"> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="masterSignOffBtn" name="improvementSignOff" class="btn btn-success">Sign Off</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Improvement Master Sign Off List Modal -->
  <div class="modal fade" id="improvementMasterSignoff-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Master Sign Off</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="improvementMasterSignoffForm" action="improvement_all_full_strategy.php">  
            <div class="form-group">
                <label class="control-label col-sm-4" for="signoffUsername">Username</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="signoffUsername" id="signoffUsername" required=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="signoffPassword">Password</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="password" class="form-control" name="signoffPassword" id="signoffPassword"> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="masterSignoffBtn" name="masterSignoffBtn" class="btn btn-success">Sign Off</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
    </div>  
</div><!--End of container-fluid-->
<script>
    $(document).ready(function()
    {
       $("#dueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    $("#dateCompleted").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
        $(".signed_off_invoke").click(function()
        {
              id = 0;
              id = $(this).attr('str');
              $("#txtHiddenSignedOff").val(id);
        });
    });
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  