<?php 
$title = 'Improvement History';
include_once('frontend/templates/headers/default_header_tpl.php');
?>

<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of navigation-->   
    
    <div class="col-lg-10">
        <!--sub menu-->
         <div class="row">
            <div class="col-lg-12">
            <ul class="nav nav-pills nav-justified topbar-menu">
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Non-Conformance</a></li>
                    <li><a href="improvement_incident.php">Incidents</a></li>
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Investigations</a></li>
                    <li ><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action">Corrective Actions</a></li>
                    <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Improvement</a></li> <!-- -->
                    <li><a href="improvement_objective.php">Objective</a></li>
            </ul>
            </div>
        </div>
        
          <div class="row">
            <div class="col-lg-8">
            <ul class="nav nav-pills nav-justified topbar-menu">
<li><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Active</a></li>
                <li ><a href="<?php echo BASE_URL;?>/index.php?action=getRequestedImprovements&module=improvement">Requested</a></li>
                <li ><a href="<?php echo BASE_URL;?>/index.php?action=getRecommendedImprovement&module=improvement">Recommended</a></li>
                <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getHistoryImprovements&module=improvement">History</a></li>
            </ul>
            </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-heading">Search Improvements</div>
        <div class="panel-body">
        <form class="form-inline" method="post" name="search" action="">
            <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Group</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more group types"data-header="Close">
                    <option value="1">OHS (Health & Safety)</option>
                    <option value="2">E (Environment)</option>
                    <option value="3">Q (Quality)</option>
                    <option value="4">F (Food)</option>
                </select>
            </div>
            </div>
            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Type</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types"data-header="Close">
                    <option>Admin</option>
                    <option>Employee</option>
                    <option>Suppliers</option>
                    <option>Resource/Consumables/Maintenance</option>
                    <option>Document</option>
                    <option>SHEQ Teams</option>
                    <option>Committees</option>
                    <option>Training/Communication</option>
                    <option>Environmental</option>
                    <option>Behaviour</option>
                    <option>Inspections</option>
                    <option>Risk Assessment</option>
                    <option>Incident</option>
                    <option>Audit</option>
                </select>
            </div>
            </div>
            </div>
            <div style="margin-left: 30px;">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Search">
            </div>
            </div>
        </form>
        </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-heading">Improvement History</div>
        <div class="panel-body">
        <div class="table-responsive">   
        
        <table width="100%" class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Date Completed</th>
              <th>Group</th>
              <th>Type</th>
              <th>Description</th>
              <th>Responsible Person</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><a href="improvement_history_view_report.php">View Report</a></td>
            </tr>
            <tr>
              <td>2</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><a href="improvement_history_view_report.php">View Report</a></td>
            </tr>
            <tr>
              <td>3</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><a href="improvement_history_view_report.php">View Report</a></td>
            </tr>
          </tbody>
        </table>

        </div>
        </div>
    </div>
    <!--End of the pane panel-default-->
    </div>  
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('includes/footer.php');
?>  