<?php 
$title = 'Improvement All';
include_once('frontend/templates/headers/default_header_tpl.php');
?>

<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 



?> 
<!--End of navigation-->   
    
    <div class="col-lg-10">
        <!--sub menu-->
        <div class="row">
            <div class="col-lg-12">
            <ul class="nav nav-pills nav-justified topbar-menu">
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Non-Conformance</a></li>
                    <li><a href="improvement_incident.php">Incidents</a></li>
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Investigations</a></li>
                    <li ><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action">Corrective Actions</a></li>
                    <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Improvement</a></li> <!-- -->
                    <li><a href="improvement_objective.php">Objective</a></li>
            </ul>
            </div>
        </div> 
        
        <div class="row">
            <div class="col-lg-8">
            <ul class="nav nav-pills nav-justified topbar-menu">
                <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Active</a></li>
                <li ><a href="<?php echo BASE_URL;?>/index.php?action=getRequestedImprovements&module=improvement">Requested</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getRecommendedImprovement&module=improvement">Recommended</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getHistoryImprovements&module=improvement">History</a></li>
            </ul>
            </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-body">
            <a href="#addImprovement-modal" data-toggle="modal" data-target="#addImprovement-modal"><button type="button" id="addImprovement" class="btn btn-success">Add Improvement</button></a>
        </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-heading">Search Improvements</div>
        <div class="panel-body">
        <form class="form-inline" method="post" name="search" action="">
            <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="col-md-3 col-sm-6">
                <div class="form-group">
                    <p><b>Type</b></p>
                    <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types"data-header="Close">
                        <option>Admin</option>
                        <option>Employee</option>
                        <option>Suppliers</option>
                        <option>Resource/Consumables/Maintenance</option>
                        <option>Document</option>
                        <option>SHEQ Teams</option>
                        <option>Committees</option>
                        <option>Training/Communication</option>
                        <option>Environmental</option>
                        <option>Behaviour</option>
                        <option>Inspections</option>
                        <option>Risk Assessment</option>
                        <option>Incident</option>
                        <option>Audit</option>
                    </select>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Select Department</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                    <option>I.T</option>
                    <option>Human Resources</option>
                    <option>Marketing</option>
                    <option>Support and Services</option>
                </select>
            </div>
            </div>

            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Group</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more group types"data-header="Close">
                    <option value="1">OHS (Health & Safety)</option>
                    <option value="2">E (Environment)</option>
                    <option value="3">Q (Quality)</option>
                    <option value="4">F (Food)</option>
                </select>
            </div>
            </div>

            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Responsible Person</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more person"data-header="Close">
                    <option>Nick</option>
                    <option>Sunny</option>
                    <option>Douglas</option>
                    <option>Warren</option>
                </select>
            </div>
            </div>
            </div>
            <div style="margin-left: 30px;">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Search">
            </div>
            </div>
        </form>
        </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-heading">All Improvements</div>
        <div class="panel-body">
        <div class="table-responsive">   
        
        <table width="100%" class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Date Created</th>
              <th>Group</th>
              <th>Improvement Type</th>
              <th>Improvement Description</th>
              <th>Responsible Person</th>
              <th>Due Date</th>
              <th>Approved</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php if($data['allImprovements']): foreach($data['allImprovements'] as $imp): ?>
            <tr>
              <td><?php echo $imp["tbl_id"];?></td>
              <td><?php echo $imp["date_created"];?></td>
              <td><?php echo $imp["gp"];?></td>
              <td><?php echo $imp["improvement_type"];?></td>
              <td><?php echo $imp["improvement_description"];?></td>
              <td><?php echo $imp["emp"];?></td>
              <td><?php echo $imp["date_created"];?></td>
              <td>Yes</td>
              <td><a href="<?php echo BASE_URL;?>/index.php?id=<?php echo $imp["tbl_id"];?>&action=displayStrategy&module=improvement">View Strategy</a></td>
            </tr>
            <?php endforeach;endif; ?>
          </tbody>
        </table>

        </div>
        </div>
    </div>
    <!--End of the pane panel-default-->

<!-- Add Improvement Modal -->
  <div class="modal fade" id="addImprovement-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Improvement</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addImprovementForm" action="<?php echo BASE_URL;?>/index.php?action=addImprovement&module=improvement">
                 <input type="hidden" name="action" value=""/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="dateCreated">Date Created</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="dateCreated" id="dateCreated"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="improve_group"  required="true" class="form-control">
                    <option value selected>Please select a group</option>
                        <?php
                        foreach($data['allSheqTeams'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['sheqteam_name']}</option>";
                        }?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="improvementType">Improvement Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input  type="text" class="form-control" name="improvementType" id="improvementType"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="improvementDescription">Improvement Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea required="true" class="form-control" rows="5" name="improvementDescription" id="improvementDescription"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="responsiblePerson">Responsible Person</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" data-live-search="true" name="responsible_person" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people">
                           <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>
                </select>
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Approval</h5></span></div>  
            <div class="form-group">
                <label class="control-label col-sm-4" for="managerialApproval">Need Managerial Approval?</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <label class="radio-inline"><input type="radio" name="managerialApproval" id="managerialApproval1" value="1" required>Yes</label>
                    <label class="radio-inline"><input type="radio" name="managerialApproval" id="managerialApproval2" value="0" >No</label>
                </div>
            </div>

            <div class="managerial hidden-div">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="managerialDueDate">Due Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="managerialDueDate" name="managerialDueDate" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="branchLevel">Branch Level</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                        <option>Nick Botha</option>
                        <option>Brian Douglas</option>
                        <option>Sunny Mathole</option>
                    </select> 
                    </div>
               </div>
               <div class="form-group">
                    <label class="control-label col-sm-4" for="groupLevel">Group Level</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                        <option>Nick Botha</option>
                        <option>Brian Douglas</option>
                        <option>Sunny Mathole</option>
                    </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="motivation">Motivation/Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" id="motivation" name="motivation"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="estimateCost">Estimate Cost</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="estimateCost" name="estimateCost" value="R">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="uploadDocs">Upload Documents</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="file" id="uploadDocs" name="uploadDocs">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-4" for="sheqfApproval">Need SHEQF Approval?</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <label class="radio-inline"><input type="radio" name="sheqfApproval" id="sheqfApproval1" value="1" required>Yes</label>
                    <label class="radio-inline"><input type="radio" name="sheqfApproval" id="sheqfApproval2" value="0" >No</label>
                </div>
            </div>

            <div class="sheqf hidden-div2">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="sheqfDueDate">Due Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="sheqfDueDate" name="sheqfDueDate" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="committee">Committee</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="form-control" name="committee" id="committee">
                            <option value selected disabled>Please select a committee</option>
                            <option>Health</option>
                            <option>Safety</option>
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="sheqPersons">SHEQ Persons</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple="" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people" data-header="Close">
                        <option>Nick Botha</option>
                        <option>Brian Douglas</option>
                        <option>Sunny Mathole</option>
                    </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="sheqfMotivation">Motivation/Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" id="sheqfMotivation" name="sheqfMotivation"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addImprovementBtn" name="addImprovementBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
    </div>  
</div><!--End of container-fluid-->

<script>
$(function(){
    $("#dateCreated").datepicker({
        dateFormat:"yy/mm/dd",
        maxDate: new Date(), 
        minDate: new Date()});
    $("#dateCreated").datepicker("setDate", new Date());
    
    $("#managerialDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    $("#sheqfDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    
    $('input[name="sheqfApproval"]').click(function(){
        if($(this).attr("value")==="1"){
            $(".hidden-div2").not(".sheqf").hide();
            $(".sheqf").show();
        }else{
            $(".sheqf").hide();
        }
    });
    $('input[name="managerialApproval"]').click(function(){
            if($(this).attr("value")==="1")
            {
                $(".hidden-div").not(".managerial").hide();
                $(".managerial").show();
            }else{
                $(".managerial").hide();
            }
    });
});
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  