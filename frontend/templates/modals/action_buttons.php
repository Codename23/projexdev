<?php
function getControls()
{
?>
<div class="btn-group">
    <button style="color: white; text-decoration: none; background-color: #17a589;" class="btn btn-default dropdown-toggle" href="#" data-toggle="dropdown">
    Action <span class="caret"></span>
    </button>
    <ul class="dropdown-menu stay-open pull-right" role="menu" style="min-width: 150px;">
        <li><a href="#"><span class="glyphicon glyphicon-pencil addglyphicon"></span> Edit</a></li>
        <li class="sweet-4"><a href="#"><span class="glyphicon glyphicon-trash addglyphicon"></span> Delete</a></li>
        <li class="divider"></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus addglyphicon"></span> Add Non-Conformance</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus addglyphicon"></span> Add Incident</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus addglyphicon"></span> Add Corrective Action</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus addglyphicon"></span> Request Maintenance</a></li>
        <li><a href="#investigate-modal" data-toggle="modal" data-target="#investigate-modal"><span class="glyphicon glyphicon-plus addglyphicon"></span> Request Investigation</a></li>
        <li><a href="#addImprovement-modal" data-toggle="modal" data-target="#addImprovement-modal"><span class="glyphicon glyphicon-plus addglyphicon"></span> Request Improvement</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus addglyphicon"></span> Request Objective</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus addglyphicon"></span> Request Training</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus addglyphicon"></span> Request Communication</a></li>
    </ul>
</div>
<?php 
}
    //use modals here
    include 'request_investigation_modal.php'; 
    include 'request_improvement_modal.php'; 
?>
<script>
    $(document).ready(function()
    {
    $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
         $('.table-responsive').css( "overflow", "auto" );
    });
var buttons = document.querySelectorAll('.sweet-4');

for (var i = 0; i < buttons.length; i++) {
  buttons[i].onclick = function(){
	swal({
          title: "Are you sure?",
          text: "You will not be able to recover this data!",
          type: "input",
          showCancelButton: true,
          confirmButtonClass: 'btn-danger',
          confirmButtonText: 'Yes, delete it!',
          closeOnConfirm: false,
          //closeOnCancel: false
          inputPlaceholder: "Reason For Deleting"
        },
        function (inputValue) {
        if (inputValue === false) return false;
        if (inputValue === "") {
          swal.showInputError("You need to provide a reason for deleting!");
          return false;
        }
        swal("Deleted!", "Reason For Deletion: " + inputValue, "success");
      });
};
}
    });
</script>