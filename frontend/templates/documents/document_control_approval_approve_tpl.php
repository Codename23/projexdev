<?php
$title = 'Approve Documents';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php include_once('frontend/templates/menus/main-menu.php'); 
      include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10"> 
    
    <div class="panel panel-default">
        <div class="panel-heading">Approval Process</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th colspan="2">Document Details</th>
                </tr>
            </thead>
            <?php 
            if(!empty($data['documentDetails'])){
            ?>
            <tbody>
                <tr>
                    <td width="30%">Group</td>
                    <td><?php echo $data['documentDetails']['sheqteam_name']; ?></td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td><?php echo $data['documentDetails']['type_name']; ?></td>
                </tr>
                <tr>
                    <td>Document Description</td>
                    <td><?php echo $data['documentDetails']['description']; ?></td>
                </tr>
                <tr>
                    <td>Document No</td>
                    <td><?php echo $data['documentDetails']['document_number']; ?></td>
                </tr>
                <tr>
                    <td>System No</td>
                    <td><?php echo $data['documentDetails']['system_number']; ?></td>
                </tr>
                <tr>
                    <td>Document Originator</td>
                    <td><?php echo $data['documentDetails']['firstname'] ." ".$data['documentDetails']['lastname']; ?></td>
                </tr>
                <tr>
                    <td>Approved By</td>
                    <td>
                        <?php if(!empty($data['approvedBy'])) {
                            foreach($data['approvedBy'] as $approvedByDetails){
                                echo '<p><a>' . $approvedByDetails['firstname'].' '.$approvedByDetails['lastname'] . '</a></p>';
                            }
                        }else{
                            echo '<p>No one approved the document</p>';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Revision No</td>
                    <td><?php echo $data['documentDetails']['current_revision_number']; ?></td>
                </tr>
                <tr>
                    <td>Revision Frequency</td>
                    <td><?php echo $data['documentDetails']['revision_frequency']; ?></td>
                </tr>
                <tr>
                    <td>Next Revision Date</td>
                    <td><?php echo $data['documentDetails']['next_revision_date']; ?></td>
                </tr>
                <tr>
                    <td>View</td>
                    <td><a href="index.php?action=downloadDocument&module=documents&docId=<?php echo $data['documentDetails']['id']; ?>"><?php echo $data['documentDetails']['description']; ?></a></td>
                </tr>
            </tbody>
            <?php 
                }
            ?>
        </table>
    </div>      
    </div>
    </div>
    
    <form class="form-horizontal" name="ApproveDocForm" id="ApproveDocForm" method="post" action="index.php?action=saveDocumentApprove&module=documents">
    <div class="panel panel-default">
        <div class="panel-heading">Re-Upload Edited Document <span class="pull-right"><p style="color:red;"><b>All amendments should be in red text</b></p></span></div>
    <div class="panel-body">
        
        <div class="form-group">
            <label class="control-label col-sm-4" for="downloadDoc">Download Document</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <a href="index.php?action=downloadDocument&module=documents&docId=<?php echo $data['documentDetails']['id']; ?>"><button type="button" id="addResource" class="btn btn-success">Download</button></a>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="reuploadDoc">Re-upload Word Document</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="file" id="reuploadDoc" name="reuploadDoc">
            </div>
        </div>       
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading">Approve Document</div>
    <div class="panel-body">
        
            <div class="form-group">
                <label class="control-label col-sm-4" for="approveDoc">Approve Document</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <label class="radio-inline"><input type="radio" name="approveDoc" id="approveDoc1" value="0" required>Yes</label>
                    <label class="radio-inline"><input type="radio" name="approveDoc" id="approveDoc2" value="1" >No</label>
                </div>
            </div>
            
            <div class="approveYes approveYesBox">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="amendments">Are there any amendments?</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <label class="radio-inline"><input type="radio" name="amendments" id="amendments1" value="0">Yes</label>
                        <label class="radio-inline"><input type="radio" name="amendments" id="amendments2" value="1" >No</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="comments">Comments</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="comments" id="comments" placeholder="Please enter a comment">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="yesNotification">Notification</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <select name="YesEmail[]" class="selectpicker" name="yesapprovePerson[]" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more approval person">
                           <?php
                           foreach($data['notificationList'] as $EmployeeDetsArr){
                               echo '<option value="' . $EmployeeDetsArr['field_data'] . '">' .$EmployeeDetsArr['firstname'] .' '.$EmployeeDetsArr['lastname'].' - '.$EmployeeDetsArr['field_data'].'</option>';
                           }
                           ?>
                        </select>
                    </div>
                </div>
            </div>
        
            <div class="approveNo approveNoBox">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="approveNoComments">Comments</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="approveNoComments" id="approveNoComments" placeholder="Please enter a comment">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="noNotification">Notification</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <select name="NoEmail[]" class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more approval person">
                           <?php
                           foreach($data['notificationList'] as $EmployeeDetsArr){
                               echo '<option value="' . $EmployeeDetsArr['field_data'] . '">' .$EmployeeDetsArr['firstname'] .' '.$EmployeeDetsArr['lastname'].' - '.$EmployeeDetsArr['field_data'].'</option>';
                           }
                           ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <input type="hidden"  name="id" value="<?php echo $_GET['id']; ?>">
                <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
    </div>
    </div>
    </form>

<!--End of panel panel-default-->

</div>
    </div><!--End of row-->
</div><!--End of container-fluid-->
<script src="js/bootstrap-select.js"></script>
<script>
$(function() {    
$('input[name="approveDoc"]').click(function(){
        if($(this).attr("value")==="0"){
            $(".approveYesBox").not(".approveYes").hide();
            $(".approveYes").show();
        }else{
            $(".approveYes").hide();
        }
});
$('input[name="approveDoc"]').click(function(){
        if($(this).attr("value")==="1"){
            $(".approveNoBox").not(".approveNo").hide();
            $(".approveNo").show();
        }else{
            $(".approveNo").hide();
        }
});

$("a.tr_clone_add").click(function() {
    var $tr    = $(this).closest('.tr_clone');
    var $clone = $tr.clone();
    //$("td:first-child", $clone).empty();
    $("td:last-child", $clone).html('<a href="javascript:void(0);" class="remCF"><span style="color:#5cb85c;" class="glyphicon glyphicon-minus-sign"></span></a>');
    $clone.find(':text').val('');
    $tr.after($clone);
    
    $("table.table").on('click','.remCF',function(){
        $(this).parent().parent().remove();
    });
    
});
    
tinymce.init({
  selector: '#docContent',
  height: 300,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
  ],
  toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
 });
</script>
<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?> 
    