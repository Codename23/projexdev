<?php

global $objDocument;

$docCodes = $objDocument->getDocumentCodes();

$location = FOLDER_DIRECTORY_ROOT.$data['folder']['folder_path'].'/'.$data['document']['filename'];
$output = FOLDER_DIRECTORY_ROOT.$data['folder']['folder_path'].'/';

if(is_array($docCodes) && count($docCodes)>0){
    foreach($docCodes as $code){
        
    }
}

chdir($output);

$convCommand = "libreoffice --headless --convert-to pdf $location";

exec($convCommand);

/*if(substr($location, -1) == 'x'){
    $length = strlen($location) - 4;
    $nameLength = strlen($data['document']['filename']) - 4;
} else {
    $length = strlen($location) - 3;
    $nameLength = strlen($data['document']['filename']) - 3;
}
$pdfLocation = substr($location, 0, $length).'pdf';
$pdfFilename = substr($data['document']['filename'], 0, $nameLength).'pdf';
*/

$pdfLocation = str_ireplace(".docx", ".pdf", $location);
//$pdfLocation = str_ireplace(".doc", ".pdf", $location);

$pdfFilename = str_ireplace(".docx", ".pdf", $data['document']['filename']);
//$pdfFilename = str_ireplace(".doc", ".pdf", $data['document']['filename']);

header('Content-Description: File Transfer');
header('Content-Type: application/pdf'); 
header('Content-Disposition: attachment; filename="'.$pdfFilename.'"');
header('Expires: 0');
header('Pragma: public');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Cache-Control: public');     
header('Content-Length: ' . filesize($pdfLocation));

readfile($pdfLocation);

exec("rm $pdfLocation");