<?php
$title= 'Documents Records';

include_once('frontend/templates/headers/default_header_tpl.php');

?>

<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */
    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <!--End of navigation-->
    <?php
    /*
     * Include side menu from the include file
     */
    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-justified topbar-menu">
                    <li><a href="index.php?action=approvedDocuments&module=documents">Approved Docs</a></li>
                    <li><a href="index.php?action=approvalDocuments&module=documents">Approval</a></li>
                    <li class="active"><a>Records</a></li>
                    <li><a href="index.php?action=documentArchive&module=documents">Document Archive</a></li>
                    <li><a href="">Templates</a></li>
                    <li><a href="">External Docs</a></li>
                    <li><a href="">Document Settings</a></li>
                </ul>
            </div>
        </div>

    <!--End of panel panel-default-->

        <div class="panel">
            <div class="panel-heading">

                <div class="input-group" id="top-search">

                                    <span class="twitter-typeahead" >
                                        <!-- bg-light -->
                                        <input type="text" value="" class="form-control input-sm no-border rounded padder typeahead tt-input" placeholder="Search your files..."
                                               onkeyup="handleTopSearch(event, this); return false;" id="searchInput" autocomplete="off" spellcheck="false" dir="auto" >
                                        <pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: Roboto, sans-serif; font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;">

                                        </pre>
                                        <div class="tt-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none;">
                                            <div class="tt-dataset tt-dataset-search-result">
                                            </div>
                                            <div class="tt-dataset tt-dataset-search-result2"></div>
                                        </div>
                                        </span>
                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm bg-orange rounded" onclick="handleTopSearch(null, $('#searchInput')); return false;" title="" data-original-title="Filter" data-placement="bottom" data-toggle="tooltip"><i class="entypo-search"></i></button>
                                        <button type="submit" class="btn btn-sm bg-orange rounded" onclick="showFilterModal(); return false;" title="" data-original-title="Advanced Search" data-placement="bottom" data-toggle="tooltip"><i class="entypo-cog"></i></button>
                                    </span>

                    <span class="input-group-btn ">
                                        <button class="btn btn-info" type="button" onclick="uploadFiles(); return false;">Upload&nbsp;&nbsp;<span class="glyphicon glyphicon-cloud-upload"></span></button>

                                         <a href="#" onclick="loadImages(-1, 1); return false;" class="btn btn-info">
                                            <i class="glyphicon glyphicon-home"></i>
                                        </a>
                                    </span>

                </div>


            </div>

            <div class="panel-body">
                <div class="row">

                    <div class="col-lg-12">

                        <div class="col-lg-2 col-md-4 folder-main-menu">
                            <br />
                            <div id="folderListView"></div>
                            <div class="clear"></div>
                        </div>

                        <div class="col-lg-10 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="main-ajax-container" class="layer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End of panel panel-default-->
            </div>
        </div>
    </div><!--End of row-->
</div><!--End of container-fluid-->

<?php
/*
 *
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
<link type="text/css" rel="stylesheet" href="<?php echo HOSTNAME .'/frontend/css/file/font-icons/entypo/css/entypo.css'; ?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo HOSTNAME .'/frontend/css/skins/default.css'; ?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo HOSTNAME .'/frontend/css/file/core.css'; ?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo HOSTNAME .'/frontend/css/file/custom.css'; ?>"/>
<script src="<?php echo HOSTNAME .'/frontend/js/jquery.jstree.js'; ?>"></script>


<script>

    var fileId = 0;
    var intialLoad = true;

    var clipboard = null;
    var triggerTreeviewLoad = true;

    $(function () {

        $(window).resize(function () {
            var h = Math.max($(window).height() - 600,800);

            $(".folder-main-menu").height(h).filter('.main-ajax-container').css('lineHeight', h + 'px');
        }).resize();

        // load folder listing
        $("#folderListView").jstree({
            "plugins": [
                "themes", "json_data", "ui", "types", "crrm", "contextmenu", "cookies"
            ],
            "themes": {
                "theme": "default",
                "dots": false,
                "icons": true
            },
            "core": {"animation": 150},
            "json_data": {
                "data": [
                    {
                        "data": "File Manager",
                        "state": "closed",
                        "attr": {"id": "-1", "rel": "home", "original-text": "File Manager"}
                    },
                    {
                        "data": "Recent Files",
                        "attr": {"id": "recent", "rel": "recent", "original-text": "Recent Files"}
                    },
                    {
                        "data": "All Files",
                        "attr": {"id": "all", "rel": "all", "original-text": "All Files"}
                    },
                    {
                        "data": "Trash Can",
                        "attr": {"id": "trash", "rel": "bin", "original-text": "Trash Can"}
                    }
                ],
                "ajax": {

                    "url": function (node) {
                        var nodeId = "";
                        var url = ""
                        if (node == -1)
                        {
                            url = "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxSideMenuFolders';?>";
                        }
                        else
                        {
                            nodeId = node.attr('id');
                            url = "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxSideMenuFolders';?>&folder="+ nodeId;
                        }

                        return url;
                    }
                }
            },
            "contextmenu": {
                "items": buildTreeViewContextMenu
            },
            'progressive_render': true
        }).bind("dblclick.jstree", function (event, data) {

            console.log("double click"+data);
            var node = $(event.target).closest("li");
            if ($(node).hasClass('jstree-leaf') == true)
            {
                return false;
            }

            //$("#folderListView").jstree("toggle_node", node.data("jstree"));
        }).bind("select_node.jstree", function (event, data) {

            // use this to stop the treeview from triggering a reload of the file manager
            if(triggerTreeviewLoad == false)
            {
                triggerTreeviewLoad = true;
                return false;
            }
            // add a slight delay encase this is a double click
            if (intialLoad == false)
            {
                // wait before loading the files, just encase this is a double click
                clickTreeviewNode(event, data);

                return false;
            }
            //alert("Am here"+data);
            clickTreeviewNode(event, data);
        }).bind("load_node.jstree", function (event, data) {
            console.log("load_node"+data);
            // assign click to icon
            assignNodeExpandClick();
            reSelectFolder();
        }).bind("open_node.jstree", function (event, data) {

            console.log("open_node"+data);
            // reassign drag crop for sub-folder
            setupTreeviewDropTarget();
        }).delegate("a", "click", function (event, data) {

            console.log("click _ here"+data);
            event.preventDefault();
        }).bind('loaded.jstree', function (e, data) {
            // load default view if not stored in cookie
            var doIntial = true;
            if (typeof ($.cookie("jstree_open")) != "undefined")
            {
                if ($.cookie("jstree_open").length > 0)
                {
                    doIntial = false;
                }
            }

            if (doIntial == true)
            {
                $("#folderListView").jstree("open_node", $("#-1"));
            }
            console.log(data);

            // reload stats
            updateStatsViaAjax();
        });

        var doIntial = true;
        if (typeof ($.cookie("jstree_select")) != "undefined")
        {
            if ($.cookie("jstree_select").length > 0)
            {
                doIntial = false;
            }
        }
        if (doIntial == true)
        {
            // load file listing
            $('#nodeId').val('-1');
        }

        $('.layer').bind('drop', function (e) {
            // blocks upload popup on internal moves / folder icons
            if($(e.target).hasClass('folderIconLi') == false)
            {
                uploadFiles();
            }
        });

        $("#fileManager").click(function (event) {
            if (ctrlPressed == false)
            {
                if ($(event.target).is('ul') || $(event.target).hasClass('fileManager')) {
                    clearSelected();
                }
            }
        });
        //assignLiOnClick();
        //setupFileDragSelectsetupFileDragSelect();


    });

    function assignNodeExpandClick()
    {
        $('.jstree-icon').off('click');
        $('.jstree-icon').on('click', function (event) {
            var node = $(event.target).parent().parent();
            if ($(node).hasClass('jstree-leaf') != true)
            {
                // expand
                $("#folderListView").jstree("toggle_node", $(node));

                // stop the node from being selected
                event.stopPropagation();
                event.preventDefault();
            }
        });
    }

    function clickTreeviewNode(event, data)
    {
        clearSelected();
        clearSearchFilters(false);


        // load via ajax
        if (intialLoad == true)
        {
            intialLoad = false;
        }
        else
        {
            $('#nodeId').val(data.rslt.obj.attr("id"));
            $('#folderIdDropdown').val($('#nodeId').val());
            if (typeof (setUploadFolderId) === 'function')
            {
                setUploadFolderId($('#nodeId').val());
            }
            loadImages(data.rslt.obj.attr("id"));
        }
    }


    function refreshFolderListing(triggerLoad)
    {
        if(typeof(triggerLoad) != "undefined")
        {
            triggerTreeviewLoad = triggerLoad;
        }

        $("#folderListView").jstree("refresh");
    }

    function buildTreeViewContextMenu(node)
    {
        var items = {};

        console.log("buildTreeViewContextMenu: "+$(node).attr('id'));
        if ($(node).attr('id') == 'trash')
        {
            var items = {
                "Empty": {
                    "label": "Empty Trash",
                    "icon": "glyphicon glyphicon-trash",
                    "action": function (obj) {
                        confirmEmptyTrash();

                    }
                }
            };
        }
        else if ($(node).attr('id') == '-1')
        {
            var items = {
                "Upload": {
                    "label": "Upload Files",
                    "icon": "glyphicon glyphicon-cloud-upload",
                    "separator_after": true,
                    "action": function (obj) {
                        uploadFiles('');
                    }
                },
                "Add": {
                    "label": "Add Folder",
                    "icon": "glyphicon glyphicon-plus",
                    "action": function (obj) {
                        console.log("Adding a new folder");
                        showAddFolderForm(obj.attr("id"));
                    }
                }
            };
        }
        else if ($.isNumeric($(node).attr('id')))
        {
            var items = {
                "Upload": {
                    "label": "Upload Files",
                    "icon": "glyphicon glyphicon-cloud-upload",
                    "separator_after": true,
                    "action": function (obj) {
                        uploadFiles(obj.attr("id"));
                    }
                },
                "Add": {
                    "label": "Add Sub Folder",
                    "icon": "glyphicon glyphicon-plus",
                    "action": function (obj) {
                        showAddFolderForm(obj.attr("id"));
                    }
                },
                "Edit": {
                    "label": "Edit",
                    "icon": "glyphicon glyphicon-pencil",
                    "action": function (obj) {
                        showAddFolderForm(null, obj.attr("id"));
                    }
                },
                "Delete": {
                    "label": "Delete",
                    "icon": "glyphicon glyphicon-remove",
                    "action": function (obj) {
                        confirmRemoveFolder(obj.attr("id"));
                    }
                },
                "Download": {
                    "label": "Download All Files (Zip)",
                    "icon": "glyphicon glyphicon-floppy-save",
                    "separator_before": true,
                    "action": function (obj) {
                        downloadAllFilesFromFolder(obj.attr("id"));
                    }
                }
            };
        }

        return items;
    }

    function confirmRemoveFolder(folderId)
    {
        // only allow actual sub folders
        if (isPositiveInteger(folderId) == false)
        {
            return false;
        }

        if (confirm('<?php echo str_replace('\'', '','Are you sure you want to remove this folder? Any files within the folder will be moved into your default root folder and remain active.'); ?>'))
        {
            removeFolder(folderId);
        }

        return false;
    }

    function removeFolder(folderId)
    {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxRemoveFolder';?>",
            data: {folderId: folderId},
            success: function (data) {
                if (data[0].error == true)
                {
                    showErrorNotification('Error', data.msg);
                }
                else
                {
                    // refresh treeview
                    // showSuccessNotification('Success', data.msg);
                    refreshFolderListing();
                }
            }
        });
    }

    function confirmEmptyTrash()
    {
        if (confirm('<?php echo str_replace('\'', '', 'Are you sure you want to empty the trash can? Any statistics and other file information will be permanently deleted.'); ?>'))
        {
            emptyTrash();
        }

        return false;
    }

    function emptyTrash()
    {
        $.ajax({
            dataType: "json",
            url: "",
            success: function (data) {
                if (data.error == true)
                {
                    alert(data.msg);
                }
                else
                {
                    // reload file listing
                    loadFiles();

                    // reload stats
                    //updateStatsViaAjax();
                }
            }
        });
    }

    var hideLoader = false;
    function loadFiles(folderId)
    {
        // get variables
        if (typeof (folderId) == 'undefined')
        {
            folderId = $('#nodeId').val();
        }

        loadImages(folderId);
    }

    function dblClickFile(fileId)
    {

    }

    function clearExistingHoverFileItem()
    {
        $('.hoverItem').removeClass('hoverItem');
    }


    function showFileMenu(liEle, clickEvent)
    {
        console.log("Am here showFileMenu()");
        clickEvent.stopPropagation();
        hideOpenContextMenus();

        fileId = $(liEle).attr('fileId');
        downloadUrl = $(liEle).attr('dtfullurl');
        statsUrl = $(liEle).attr('dtstatsurl');
        isDeleted = $(liEle).hasClass('fileDeletedLi');
        fileName = $(liEle).attr('dtfilename');
        extraMenuItems = $(liEle).attr('dtextramenuitems');
        var items = {
            "Stats": {
                "label": "Stats",
                "icon": "glyphicon glyphicon-stats",
                "action": function (obj) {
                    showStatsPopup(fileId);
                }
            }
        };

        if (isDeleted == false)
        {
            var items = {};

            // replace any items for overwriting (plugins)
            if (extraMenuItems.length > 0)
            {
                items = JSON.parse(extraMenuItems);
                for (i in items)
                {
                    // setup click action on menu item
                    eval("items['" + i + "']['action'] = " + items[i]['action']);
                }
            }

            // default menu items
            items["View"] = {
                "label": "View",
                "icon": "glyphicon glyphicon-zoom-in",
                "action": function (obj) {
                    showFileInfo(fileId);
                }
            };

            items["Download"] = {
                "label": "Download" + " : "+fileName,
                "icon": "glyphicon glyphicon-download-alt",
                "separator_after": true,
                "action": function (obj) {
                    openUrl('<?php //echo BASE_URL; ?>/index.php?fileId=' + fileId);
                }
            };

            items["Edit"] = {
                "label": "Edit File Info",
                "icon": "glyphicon glyphicon-pencil",
                "action": function (obj) {
                    showEditFileForm(fileId);
                }
            };

            items["Duplicate"] = {
                "label": "Create Copy",
                "icon": "glyphicon glyphicon-plus-sign",
                "action": function (obj) {
                    selectFile(fileId, true);
                    duplicateFiles();
                }
            };

            items["Delete"] = {
                "label": "Delete",
                "icon": "glyphicon glyphicon-remove",
                "separator_after": false,
                "action": function (obj) {
                    selectFile(fileId, true);
                    deleteFiles();
                }
            };



            // replace any items for overwriting
            for (i in extraMenuItems)
            {
                if (typeof (items[i]) != 'undefined')
                {
                    items[i] = extraMenuItems[i];
                }
            }
        }
        $.vakata.context.show(items, $(liEle), clickEvent.pageX - 15, clickEvent.pageY - 8, liEle);
        return false;
    }

    function showFolderMenu(liEle, clickEvent)
    {
        console.log("Am here showFileMenu()");
        clickEvent.stopPropagation();
        folderId = $(liEle).attr('folderId');
        var items = {
            "Upload": {
                "label": "Upload Files",
                "icon": "glyphicon glyphicon-cloud-upload",
                "separator_after": true,
                "action": function (obj) {
                    uploadFiles(folderId);
                }
            },
            "Add": {
                "label": "Add Sub Folder",
                "icon": "glyphicon glyphicon-plus",
                "action": function (obj) {
                    showAddFolderForm(folderId);
                }
            },
            "Edit": {
                "label": "Edit",
                "icon": "glyphicon glyphicon-pencil",
                "action": function (obj) {
                    showAddFolderForm(null, folderId);
                }
            },
            "Delete": {
                "label": "Delete",
                "icon": "glyphicon glyphicon-remove",
                "separator_before": true,
                "action": function (obj) {
                    confirmRemoveFolder(folderId);
                }
            },
            "Download": {
                "label": "Download All Files (Zip)",
                "icon": "glyphicon glyphicon-floppy-save",
                "separator_before": true,
                "action": function (obj) {
                    downloadAllFilesFromFolder(folderId);
                }
            }
        };

        $.vakata.context.show(items, $(liEle), clickEvent.pageX - 15, clickEvent.pageY - 8, liEle);
        return false;
    }

    function selectFile(fileId, onlySelectOn)
    {
        if (typeof (onlySelectOn) == "undefined")
        {
            onlySelectOn = false;
        }

        // clear any selected if ctrl key not pressed
        if ((ctrlPressed == false) && (onlySelectOn == false))
        {
            showFileInformation(fileId);

            return false;
        }

        elementId = 'fileItem' + fileId;
        if (($('.' + elementId).hasClass('selected')) && (onlySelectOn == false))
        {
            $('.' + elementId).removeClass('selected');
            if (typeof (selectedItems['k' + fileId]) != 'undefined')
            {
                delete selectedItems['k' + fileId];
            }
        }
        else
        {
            $('.' + elementId + '.owned-image:not(.fileDeletedLi)').addClass('selected');
            if ($('.' + elementId).hasClass('selected'))
            {
                selectedItems['k' + fileId] = [fileId, $('.' + elementId).attr('dttitle'), $('.' + elementId).attr('dtsizeraw'), $('.' + elementId).attr('dtfullurl'), $('.' + elementId).attr('dturlhtmlcode'), $('.' + elementId).attr('dturlbbcode')];
            }
        }

        updateSelectedFilesStatusText();
        updateFileActionButtons();
    }

    var ctrlPressed = false;
    $(window).keydown(function (evt) {
        if (evt.which == 17) {
            ctrlPressed = true;
        }
    }).keyup(function (evt) {
        if (evt.which == 17) {
            ctrlPressed = false;
        }
    });

    $(window).keydown(function (evt) {
        if (evt.which == 65) {
            if (ctrlPressed == true)
            {
                selectAllFiles();
                return false;
            }
        }
    })

    function updateFileActionButtons()
    {
        totalSelected = countSelected();
        if (totalSelected > 0)
        {
            $('.fileActionLinks').removeClass('disabled');

        }
        else
        {
            $('.fileActionLinks').addClass('disabled');
        }
    }



    function showLightboxNotice()
    {
        jQuery('#generalModal').modal('show', {backdrop: 'static'}).on('shown.bs.modal', function () {
            $('.general-modal .modal-body').html($('#filePopupContentWrapperNotice').html());
        });
    }

    function showFileInformation(fileId)
    {
        // hide any context menus
        hideOpenContextMenus();

        // load overlay
        showFileInline(fileId);
    }



    function downloadAllFilesFromFolder(folderId)
    {
        // only allow actual sub folders
        if (isPositiveInteger(folderId) == false)
        {
            return false;
        }

        if (confirm("<?php echo 'Are you sure you want to download all the files in this folder? This may take some time to complete.'; ?>"))
        {
            downloadAllFilesFromFolderConfirm(folderId);
        }

        return false;
    }

    function downloadAllFilesFromFolderConfirm(folderId)
    {


    }




</script>


<script>
    function showAddFolderForm(parentId, selectedFolderId)
    {
        console.log("Show add folder form");
        // only allow actual sub folders on edit
        if ((typeof (selectedFolderId) != 'undefined') && (isPositiveInteger(selectedFolderId) == false))
        {
            return false;
        }

        showLoaderModal();
        if (typeof (parentId) == 'undefined')
        {
            parentId = $('#nodeId').val();
        }

        if (typeof (selectedFolderId) == 'undefined')
        {
            selectedFolderId = 0;
        }

        var $this=$(this),
            $remote="<?php echo BASE_URL.'/index.php?module=modal_request&action=ajaxAddEditFolder';?>",

            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
        $('body').append($modal);
        $.post('<?php echo BASE_URL.'/index.php?module=modal_request&action=ajaxAddEditFolder';?>', {'parentId':parentId,'selectedFolderId': selectedFolderId}, function(data){
            //$('.area_data').html(data);

            hideLoaderModal();
            $modal.modal({backdrop: 'static', keyboard: false});
            $modal.html(data);

        });

    }

    function showEditFileForm (fileId) {

        showLoaderModal();

           /*var $modal = $('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
            $('body').append($modal);

            $.post(' //echo BASE_URL.'/index.php?module=modal_request&action=ajaxEditFileInfo';?>', function(data){

            hideLoaderModal();
            $modal.modal({backdrop: 'static', keyboard: false});
            $modal.html(data);

        });
        hideLoaderModal();*/

    }

    function uploadFiles(parentId, selectedFolderId)
    {
        console.log("Show add folder form");
        // only allow actual sub folders on edit
        if ((typeof (selectedFolderId) != 'undefined') && (isPositiveInteger(selectedFolderId) == false))
        {
            return false;
        }

        showLoaderModal();
        if (typeof (parentId) == 'undefined')
        {
            parentId = $('#nodeId').val();
        }

        if (typeof (selectedFolderId) == 'undefined')
        {
            selectedFolderId = 0;
        }

        var $this=$(this),
            $remote="<?php echo BASE_URL.'/index.php?module=modal_request&action=ajaxAddEditFolder';?>",

            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
            $('body').append($modal);
            $.post('<?php echo BASE_URL.'/index.php?module=modal_request&action=ajaxAddEditFolder';?>', {'parentId':parentId,'selectedFolderId': selectedFolderId}, function(data){
            //$('.area_data').html(data);

            hideLoaderModal();
            $modal.modal({backdrop: 'static', keyboard: false});
            $modal.html(data);

        });
    }



    function updateStatsViaAjax()
    {
        // first request stats via ajax
        $.ajax({
            dataType: "json",
            url: "",
            success: function (data) {
                updateOnScreenStats(data);
            }
        });
    }

    function updateOnScreenStats(data)
    {
        // update list of folders for breadcrumbs
        folderArray = jQuery.parseJSON(data.folderArray);

        // update folder drop-down list in the popup uploader
        $("#folder_id").html(data.folderSelectForUploader);

        // update root folder stats
        if (data.totalRootFiles > 0)
        {
            $("#folderListView").jstree('set_text', '#-1', $('#-1').attr('original-text') + ' (' + data.totalRootFiles + ')');
        }
        else
        {
            $("#folderListView").jstree('set_text', '#-1', $('#-1').attr('original-text'));
        }

        // update trash folder stats
        if (data.totalTrashFiles > 0)
        {
            $("#folderListView").jstree('set_text', '#trash', $('#trash').attr('original-text') + ' (' + data.totalTrashFiles + ')');
        }
        else
        {
            $("#folderListView").jstree('set_text', '#trash', $('#trash').attr('original-text'));
        }

        // update all folder stats
        $("#folderListView").jstree('set_text', '#all', $('#all').attr('original-text') + ' (' + data.totalActiveFiles + ')');

        // update total storage stats
        $(".remaining-storage .progress .progress-bar").attr('aria-valuenow', data.totalStoragePercentage);
        $(".remaining-storage .progress .progress-bar").width(data.totalStoragePercentage + '%');
        $("#totalActiveFileSize").html(data.totalActiveFileSizeFormatted);
    }


    function duplicateFiles()
    {
        if (countSelected() > 0)
        {
            duplicateFilesConfirm();
        }

        return false;
    }

    function duplicateFilesConfirm()
    {
        // show loader
        showLoaderModal(0);

        // prepare file ids
        fileIds = [];
        for (i in selectedItems)
        {
            fileIds.push(i.replace('k', ''));
        }

        // duplicate files
        $.ajax({
            type: "POST",
            url: "",
            data: {fileIds: fileIds},
            dataType: 'json',
            success: function (json) {
                if (json.error == true)
                {
                    // hide loader
                    hideLoaderModal();
                    $('#filePopupContentNotice').html(json.msg);
                    showLightboxNotice();
                }
                else
                {
                    // done
                    addBulkSuccess(json.msg);
                    finishBulkProcess();
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('#popupContentNotice').html('Failed connecting to server, please try again later.');
                showLightboxNotice();
            }
        });
    }
</script>

<script type="text/javascript">

    function showFileInline(fileId)
    {
      showFileInfo(fileId);
    }



    function showFileInfo(fileId)
    {
        //scrollTop();
        successCallback = function(data) {
            //loadSimilarImages(imageId);
            //updatePageUrlBar(data.page_url, data.html, data.page_title);
            //setupToolTips();
            showLayer('main-ajax-container');
            // setupMobileImageSwipe();
        }
        loadAjaxContent('#main-ajax-container', AJAX_ROOT+"module=ajax_request&action=ajaxShowFileDetails", {fileId: fileId}, successCallback);
    }


    function showImageBrowseSlide(folderId)
    {
        $('#imageBrowseWrapper').show();
        $('#albumBrowseWrapper').hide();
        loadFiles(folderId);
    }

    function handleTopSearch(event, ele, isAdvSearch)
    {
        // make sure we have a default setting for advance search
        if(typeof(isAdvSearch) == 'undefined')
        {
            isAdvSearch = false;
        }

        searchText = $(ele).val();
        $('#filterText').val(searchText);

        // check for enter key
        doSearch = false;
        if(event == null)
        {
            doSearch = true;
        }
        else
        {
            var charCode = (typeof event.which === "number") ? event.which : event.keyCode;
            if (charCode == 13)
            {
                doSearch = true;
            }
        }

        // do search
        if(doSearch == true)
        {
            // make sure we have something to search
            if(searchText.length == 0)
            {
                showErrorNotification('Error', 'Please enter something to search for.');
                return false;
            }

            filterAllFolders = false;
            filterUploadedDateRange = '';
            if(isAdvSearch == true)
            {
                if($('#filterAllFolders').is(':checked'))
                {
                    filterAllFolders = true;
                }
                filterUploadedDateRange = $('#filterUploadedDateRange').val();
            }

            url = WEB_ROOT+'/search/?s=image&filterAllFolders='+filterAllFolders+'&filterUploadedDateRange='+filterUploadedDateRange+'&t='+encodeURIComponent(searchText);
            window.location = url;
        }

        return false;
    }


    function copyToClipboard(ele)
    {
        destroyClipboard();
        clipboard = new Clipboard(ele);
        clipboard.on('success', function(e) {
            showSuccessNotification('Success', 'Copied to clipboard.');
            $('#clipboard-placeholder').html('');
        });

        clipboard.on('error', function(e) {
            showErrorNotification('Error', 'Failed copying to clipboard.');
        });
    }

    function destroyClipboard()
    {
        if(clipboard != null)
        {
            clipboard.destroy();
        }
    }

    callbackcheck = false;
    function showStatsPopup(fileId)
    {
        showLoaderModal();
        jQuery('#statsModal .modal-content').load("", {fileId: fileId}, function () {
            hideLoaderModal();
            jQuery('#statsModal').modal('show', {backdrop: 'static'}).on('show', function() {
                callbackcheck = setTimeout(function(){
                    redrawCharts();
                    clearTimeout(callbackcheck);
                }, 100);
            });
        });
    }

</script>

<script>

    $( document ).ready(function() {

        loadImages('-1');//load root folders
    });
</script>




