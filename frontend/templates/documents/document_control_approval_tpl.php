<?php 
$title = 'Approve Documents';
include_once('frontend/templates/headers/default_header_tpl.php');

?>
<div class="container-fluid"> 
<!--navigation--> 
<?php include_once('frontend/templates/menus/main-menu.php'); 
      include_once('frontend/templates/menus/side-menu.php'); ?> 

<!--End of navigation--> 
<div class="col-lg-10">
    <div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="index.php?action=approvedDocuments&module=documents">Approved Docs</a></li>
        <li class="active"><a>Approval</a></li>
        <li><a href="index.php?action=viewAllDocumentsRecords&module=documents">Records</a></li>
        <li><a href="index.php?action=documentArchive&module=documents">Document Archive</a></li>
        <li><a>Templates</a></li>
        <li><a>External Docs</a></li>
        <li><a>Document Settings</a></li>
    </ul>
    </div>
    </div>  
    <div class="panel-group" id="accordion">
    <?php $count = 0; foreach($data['getDocumentTypes'] as $documentTypesDetails){ ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $documentTypesDetails['id']; ?>"><?php echo $documentTypesDetails['type_name']; ?></a>
            </h4>
        </div>
        <div id="collapse<?php echo $documentTypesDetails['id']; ?>" class="panel-collapse collapse<?php if($count == 0){ echo " in";}?>">
            <div class="panel-body">
            <div class="row">
                <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Doc No</th>
                            <th>System Ref</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>Rev No</th>
                            <th>Next Revision Date</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <?php if(!empty($data['getallApprovalDocs'])) { ?> 
                    <tbody>
                    <?php
                    foreach($data['getallApprovalDocs'] as $docApprovalDetails){
                        if($docApprovalDetails['doc_type_id'] == $documentTypesDetails['id']){
                        echo'<tr>
                                <td>' . $docApprovalDetails['document_number'] . '</td>
                                <td>' . $docApprovalDetails['system_number'] . '</td>
                                <td>' . $docApprovalDetails['description'] . '</td>
                                <td>' . $docApprovalDetails['sheqteam_name'] . '</td>
                                <td>' . $docApprovalDetails['current_revision_number'] . '</td>
                                <td>' . $docApprovalDetails['next_revision_date'] . '</td>
                                <td><a href="index.php?action=viewApprovalProgress&module=documents&id=' . $docApprovalDetails['id'] . '">View Approval Progress</a></td>
                                <td><a href="index.php?action=approveDocuments&module=documents&id=' . $docApprovalDetails['id'] . '">Approve</a></td>
                            </tr>';
                        }
                    }
                    ?>
                    </tbody>
                    <?php } ?>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
    <!--panel panel-default-->
    <?php $count ++;} ?>
    </div>
    <!--End of panel-group-->

</div>
    </div><!--End of row-->
</div><!--End of container-fluid-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>
    