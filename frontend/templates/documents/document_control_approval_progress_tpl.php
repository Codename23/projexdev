<?php
$title = 'Approval Progress';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php include_once('frontend/templates/menus/main-menu.php'); 
      include_once('frontend/templates/menus/side-menu.php'); ?>  
<!--End of navigation--> 
<div class="col-lg-10">
    <div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="index.php?action=approvedDocuments&module=documents">Approved Docs</a></li>
        <li><a href="index.php?action=approvalDocuments&module=documents">Approval</a></li>
        <li><a href="index.php?action=viewAllDocumentsRecords&module=documents">Records</a></li>
        <li><a href="index.php?action=documentArchive&module=documents">Document Archive</a></li>
        <li><a>Templates</a></li>
        <li><a>External Docs</a></li>
        <li><a>Document Settings</a></li>
    </ul>
    </div>
    </div>  
    <?php if(!empty($data['documentDetails'])){ ?>
    
    <div class="panel panel-default">
    <div class="panel-heading">Approval Process</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th colspan="2">Document Details</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td width="30%">Group</td>
                    <td><?php echo $data['documentDetails']['sheqteam_name']; ?></td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td><?php echo $data['documentDetails']['type_name']; ?></td>
                </tr>
                <tr>
                    <td>Document Description</td>
                    <td><?php echo $data['documentDetails']['description']; ?></td>
                </tr>
                <tr>
                    <td>Document No</td>
                    <td><?php echo $data['documentDetails']['document_number']; ?></td>
                </tr>
                <tr>
                    <td>System No</td>
                    <td><?php echo $data['documentDetails']['system_number']; ?></td>
                </tr>
                <tr>
                    <td>Document Originator</td>
                    <td><?php echo $data['documentDetails']['firstname'] ." ".$data['documentDetails']['lastname']; ?></td>
                </tr>
                <tr>
                    <td>Approved By</td>
                    <td>
                        <?php if(!empty($data['approvedBy'])) {
                            foreach($data['approvedBy'] as $approvedByDetails){
                                echo '<p>' . $approvedByDetails['firstname'].' '.$approvedByDetails['lastname'] . '</p>';
                            }
                        }else{
                            echo '<p>No one approved the document</p>';
                        }
                        ?>
                        
                </tr>
                <tr>
                    <td>Revision No</td>
                    <td><?php echo $data['documentDetails']['current_revision_number']; ?></td>
                </tr>
                <tr>
                    <td>Revision Frequency</td>
                    <td><?php echo $data['documentDetails']['revision_frequency']; ?></td>
                </tr>
                <tr>
                    <td>Next Revision Date</td>
                    <td><?php echo $data['documentDetails']['next_revision_date']; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
        
    </div>
    </div>
    
    <?php } ?>
    <div class="panel panel-default">
    <div class="panel-body">
    <div class="table-responsive">
            <table width="100%" class="table table-hover">
                <thead>
                    <tr>
                        <th>Order</th>
                        <th>Due Date</th>
                        <th>Name Surname</th>
                        <th>Department</th>
                        <th>Document</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if(!empty($data['getAllApproval'])){
                    $count = 1;
                    foreach($data['getAllApproval'] as $approvalListDetails){
                        echo '<tr>
                                <td>' . $count .'</td>
                                <td>'. $approvalListDetails['firstname']. ' '. $approvalListDetails['lastname'].'</td>
                                <td>'. $approvalListDetails['due_date'].'</td>
                                <td>'. $approvalListDetails['department_name'].'</td>';
                                if($approvalListDetails['employee_id'] == $_SESSION['user_id']){
                                    echo '<td><a href="index.php?action=approveDocuments&module=documents&id='. $approvalListDetails['approval_id'].'">Approve</a></td>';
                                }else{
                                    echo '<td><a href="index.php?action=downloadDocument&module=documents&docId='. $approvalListDetails['doc_id'].'">View</a></td>';
                                }

                                if($approvalListDetails['is_approved'] == 1){
                                    if($approvalListDetails['employee_id'] == $_SESSION['user_id']){
                                        echo '<td>In Progress</a></td>';
                                    }else{
                                        echo '<td>Approved</td>';  
                                    }
                                }else{
                                    if($approvalListDetails['employee_id'] == $_SESSION['user_id']){
                                        echo '<td>In Progress</a></td>';
                                    }else{
                                        echo '<td>Await</td>';  
                                    }
                                }
                            echo '</tr>';
                    $count ++;
                    }
                }
                ?>
                </tbody>
            </table>    
        </div>
        
    </div>
    </div>
    

<!--End of panel panel-default-->

</div>
    </div><!--End of row-->
</div><!--End of container-fluid-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?> 