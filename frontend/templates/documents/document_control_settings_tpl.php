<?php
$title = 'Document Settings';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid">
    <!--navigation-->
    <?php
    include_once('frontend/templates/menus/main-menu.php');
    include_once('frontend/templates/menus/side-menu.php');
    ?>
<!--End of navigation--> 
<div class="col-lg-10">
    <div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="index.php?action=approvedDocuments&module=documents">Approved Docs</a></li>
        <li><a href="index.php?action=approvalDocuments&module=documents">Approval</a></li>
        <li><a href="index.php?action=viewAllDocumentsRecords&module=documents">Records</a></li>
        <li><a href="index.php?action=documentArchive&module=documents">Document Archive</a></li>
        <li><a>Templates</a></li>
        <li><a>External Docs</a></li>
        <li class="active"><a>Document Settings</a></li>
    </ul>
    </div>
    </div>  
    
    <div class="row">
    <div class="col-lg-6">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a>Codes</a></li>
        <li><a href="">Folders</a></li>
    </ul>
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading">Document Codes</div>
    <div class="panel-body">
    <div class="table-responsive">
    <table width="100%" class="table table-hover">
        <thead>
        <tr>
            <th>Category</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <?php if(!empty($data['document_codes'])) { ?> 
                        <tbody>
                        <?php
                        foreach($data['document_codes'] as $docCodes){
                            echo'<tr>
                                    <td>' . $docCodes['category_name'] . '</td>
                                    <td>' . $docCodes['code_name'] . '</td>
                                    <td>' . $docCodes['code_text'] . '</td>
                                </tr>';
                        }
                        ?>
                        </tbody>
                        <?php } ?>
    </table>
    </div>
    </div>
    </div>
    <!--End of panel panel-default-->

</div>
    </div><!--End of row-->
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    