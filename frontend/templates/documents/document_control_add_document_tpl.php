<?php
$title = 'Add Documents';
include_once('frontend/templates/headers/default_header_tpl.php');

//echo '<pre>';
//print_r($data);
//echo '</pre>';
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php include_once('frontend/templates/menus/main-menu.php'); 
      include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10"> 
    
    <div class="panel-group">
    
    <div class="panel panel-default">
    <div class="panel-heading">Add Document</div>
    <div class="panel-body">
        <form class="form-horizontal" method="post"  enctype="multipart/form-data" name="addDocForm" action="index.php">
        <input type="hidden" class="form-control" name="action" value="saveDocument">
        <input type="hidden" class="form-control" name="module" value="documents">
       <div class="form-group">
            <label class="control-label col-sm-4" for="documentGroup">Group</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" name="documentGroup" id="documentGroup" required>
                <option value selected disabled>Please select a group</option>
                <option value="1">SHEQ</option>
                <option value="2">SHE</option>
                <option value="3">OHS</option>
                <option value="4">Q</option>
                <option value="5">E</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="documentType">Type</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" name="documentType" id="documentType" required>
                <option value selected disabled>Please select a type</option>
                <?php 
                if(!empty($data['documentTypes'])) { 
                    foreach($data['documentTypes'] as $docType){
                        echo '<option value="'.$docType['id'].'">'.$docType['type_name'].'</option>';
                    }
                }
                ?>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="documentDescription">Document Description</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="documentDescription" id="documentDescription" placeholder="Please provide a document description" value="<?php echo (isset($data['documentDetsArr'])) ? $data['documentDetsArr']['description'] : ""; ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="documentNo">Document No</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="documentNo" id="documentNo" placeholder="Please provide a document no">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="documentSystemNo">System No</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="documentSystemNo" id="documentSystemNo" placeholder="Please provide a system no">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="documentOriginatorId">Document Originator</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select name="documentOriginator" id="documentOriginator" class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a document originator" data-header="Close">
                <?php 
                    if(!empty($data['getallEmployee'])) { 
                        foreach($data['getallEmployee'] as $user){
                            echo '<option value="'.$user['user_id'].'">'.$user['firstname'].' '.$user['lastname'].'</option>';
                        }
                    }
                    ?> 
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="branches">Document Visible To</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" name="branches[]" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more branch"data-header="Close">
                    <?php 
                    if(!empty($data['branches'])) { 
                        foreach($data['branches'] as $branch){
                            echo '<option value="'.$branch['id'].'">'.$branch['branch_name'].'</option>';
                        }
                    }    
                    ?> 
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="approvers">Approved By</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" name="approvers[]" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people"data-header="Close">
                    <?php 
                    if(!empty($data['getallEmployee'])) { 
                        foreach($data['getallEmployee'] as $user){
                            echo '<option value="'.$user['user_id'].'">'.$user['firstname'].' '.$user['lastname'].'</option>';
                        }
                    }
                    ?> 
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4" for="documentRevisionNo">Revision No</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="number" class="form-control" name="documentRevisionNo" id="documentRevisionNo" placeholder="Please provide a revision no">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="documentRevisionFrequency">Revision Frequency</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" name="documentRevisionFrequency" id="documentRevisionFrequency" required>
                <option value selected disabled>Please select a revision frequency</option>
                <option value="1">Monthly</option>
                <option value="2">Every 6 Months</option>
                <option value="3">Yearly</option>
                <option value="4">Every 2 Years</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="documentNextRevisionDate">Next Revision Date</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" id="documentNextRevisionDate" name="documentNextRevisionDate" required>
            </div>
        </div>
        <br/>
        <div class="form-group">
            <label class="control-label col-sm-4" for="uploadDoc">Upload Word Document</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="file" id="uploadDoc" name="uploadDoc">
            </div>
        </div>  
        <br/>
       <!-- <div class="form-group">
            <label class="control-label col-sm-4" for="linkToDoc">Link To Document</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" name="linkToDoc[]" id="linkToDoc[]" required>
                <option value selected disabled>Please select a link</option>
                <option value="1">No Link</option>
                <option value="2">Policy</option>
                <option value="3">Procedure</option>
                <option value="4">SOP</option>
                <option value="5">Form</option>
                <option value="6">Inspection</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="linkDescription">Description</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <div class="after-add-another-link">
                <input type="text" class="form-control" id="linkDescription[]" name="linkDescription[]" placeholder="Please enter a description" required>
            </div>
            <br/>
            <div> 
                <button class="btn btn-success add-another-link" type="button"><i class="glyphicon glyphicon-plus"></i> Add Another Link</button>
            </div>
            </div>
        </div>
        
        <div class="copy-another-link hide">
            <div class="control-group" style="margin-top:30px">
              <div class="form-group">
                <div class="col-sm-12">
                    <select class="form-control" name="linkToDoc[]" id="linkToDoc[]" required>
                        <option value selected disabled>Please select a link</option>
                        <option value="1">No Link</option>
                        <option value="2">Policy</option>
                        <option value="3">Procedure</option>
                        <option value="4">SOP</option>
                        <option value="5">Form</option>
                        <option value="6">Inspection</option>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="linkDescription[]" name="linkDescription[]" placeholder="Please enter a description" required>
                </div>
              </div>
            <div> 
              <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
            </div>
          </div>
        </div> -->
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
            <button type="submit" id="addDocBtn" class="btn btn-success">Add Document</button>
            </div>
        </div>        
        </form>
        </div>
    </div><!--End of panel-body-->
    </div><!--End of panel panel-default-->

    <div class="panel panel-default hidden-div" id="sendApprovalForm">
    <div class="panel-heading">Send For Approval</div>
    <div class="panel-body">
    <form class="form-horizontal" name="sendApprovalForm" method="post" action="index.php?action=approvalNotifications&module=documents">
        <div class="form-group">
            <label class="control-label col-sm-4" for="approveDoc">Is the document preapproved?</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <label class="radio-inline"><input type="radio" name="approveDoc" id="approveDoc1" value="0">Yes</label>
                <label class="radio-inline"><input type="radio" <?php $showclass = ""; if(!empty($data['getAllAddApproval'])){ echo 'checked'; $showclass = " show"; } ?> name="approveDoc" id="approveDoc2" value="1" >No</label>
                <label class="radio-inline"><input type="radio" name="approveDoc" id="approveDoc3" value="2" >N/A</label>
            </div>
        </div>
        <div class="message messageBox <?php echo $showclass; ?>">
        <div class="form-group">
            <label class="control-label col-sm-4" for="approvalMessage">Message to person who approve document</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <textarea class="form-control" rows="5" name="approvalMessage" id="approvalMessage" required></textarea>
            </div>
        </div>
       
        <div class="table-responsive">
            <table width="100%" class="table table-hover">
                <thead>
                    <tr>
                        <th>Order approval</th>
                        <th>Name Surname</th>
                        <th>Department</th>
                        <th>Occupation</th>
                        <th>Confirm E-Mail</th>
                        <th>Due Date for Approval</th>
                        <th><a href="#addApproval-modal" data-toggle="modal" data-target="#addApproval-modal"><span class="glyphicon glyphicon-plus"></span></a></th>
                    </tr>
                </thead>
                <tbody>
                 <?php if(!empty($data['getAllAddApproval'])) { 
                    $count = 1;
                    foreach($data['getAllAddApproval'] as $addApprovalDetails){
                        echo
                        "<tr>
                            <td>" . $count . "</td>
                            <td>" . $addApprovalDetails['firstname']." " .$addApprovalDetails['lastname']."</td>
                            <td>" . $addApprovalDetails['department_name']."</td>
                            <td>" . $addApprovalDetails['name']."</td>
                            <td>" . $addApprovalDetails['field_data']."</td>
                            <td>" . $addApprovalDetails['due_date'].'</td>
                            <td><a href="index.php?module=documents&action=deleteApproval&id='. $addApprovalDetails['approval_id'].'"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>';
                        $count ++;
                        
                    }
                    
                    } ?>
                </tbody>
            </table>    
        </div> 
        </div>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
            <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>
    </form>
    </div>
    </div>  
    <!--End of panel panel-default-->

    <!--<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                    <a><button type="button" id="printBtn" class="btn btn-success">Print</button></a>
                    <a><button type="button" id="viewBtn" class="btn btn-success">View</button></a>
                    <a><button type="button" id="downloadBtn" class="btn btn-success">Download</button></a>
            </div>
            <div class="col-md-4"></div>
            </div>
        </div>
    </div>
    </div>-->
    
    
    </div><!--End of panel-group -->
<!--End of panel panel-default-->

<!-- Add Approval Modal -->
  <div class="modal fade" id="addApproval-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Approval</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addApprovalForm" action="index.php?module=documents&action=saveApprovalPerson">
            <!--
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderApproval">Order Approval</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="orderApproval" class="form-control">
                        <option value="" selected disabled>Please select an order</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
            </div>
            !-->
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderDepartment">Department</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select id="orderDepartment" name="orderDepartment" class="form-control">
                    <option value="" selected disabled>Please select a department</option>
                    <?php
                    foreach($data['getAllDepartment'] as $departmentDetsArr){
                        echo '<option value="' . $departmentDetsArr['id'] . '">' .$departmentDetsArr['department_name'].'</option>';
                    }
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderOccupation">Occupation</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select id="orderOccupation" name="orderOccupation" class="form-control">
                        <option value="" selected disabled>Please select an occupation</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderFullname">Name Surname</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="employeeId" id="orderFullname" required>
                        <option value="" selected disabled>Search a user</option>
                    </select>
                </div>
            </div>
            <!--
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderConfirmEmail">Confirming E-Mail</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="email" class="form-control" name="orderConfirmEmail" id="orderConfirmEmail" required placeholder="please confirm e-mail">
                </div>
            </div>
            !-->
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderDueDate">Due Date for Approval</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="orderDueDate" id="orderDueDate" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <input type="hidden"  name="docid" value="1">
                <button type="submit" id="addApprovalBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
    </div><!--End of row-->
</div><!--End of container-fluid-->
<script src="js/bootstrap-select.js"></script>
<?php
if (isset($data['docId'])){ ?>
<script type="text/javascript">
$(document).ready(function(){
    $("#sendApprovalForm").show();
    $("#addDocBtn").hide();
});
</script>
<?php } ?>
<script>
$(function() {  
    $("#orderFullname").select2({width: '100%'});  
    $("#orderDepartment").select2({width: '100%'});  
    $("#orderOccupation").select2({width: '100%'});
    
    $("#documentNextRevisionDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    $("#orderDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    
    $(".add-another-person").click(function(){ 
          var html = $(".copy-approvedBy").html();
          $(".after-add-another-person").after(html);
    });
    $("body").on("click",".remove",function(){ 
        $(this).parents(".control-group").remove();
    });
    
    $(".add-another-link").click(function(){ 
        var html = $(".copy-another-link").html();
        $(".after-add-another-link").after(html);
    });
    $("body").on("click",".remove",function(){ 
    $(this).parents(".control-group").remove();
    });
    
    
    $('input[name="approveDoc"]').click(function(){
        if($(this).attr("value")==="1"){
            $(".messageBox").not(".message").hide();
            $(".message").show();
        }else{
            $(".message").hide();
        }
    });
    
    
    tinymce.init({
  selector: '#docContent',
  height: 500,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
  ],
  toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
});

$('body').on('change', '#orderDepartment', function(){
    var department = $('#orderDepartment').val();
    showDepartmentOccupation(department);
});
/**
 * Method to display occupation
 * 
 * @returns void
 */
function showDepartmentOccupation(department){
    $.ajax({
        url :"index.php?module=ajax_request&action=ajaxDepartmentsOccupations",
        type:"POST",
        data:"department_id="+department,
        dataType:'json',
        success: function (data){
            $('#orderOccupation').empty();
            $("<option value=0>Please select your occupation</option>").prependTo("#orderOccupation");
            if (data.length > 0) {
                $.each(data, function(key, val) {
                    $('#orderOccupation').append($('<option>').text(val.name).attr('value',val.id));
                });
            }
        },
        error: function(error){
            console.log('Request failed: ' + error);
        }
    });
}

$('body').on('change', '#orderOccupation', function(){
    showEmployeeByOccupation();
});
/**
 * Method to display employees 
 * 
 * @returns void
 */
function showEmployeeByOccupation(){
    var occupation = $('#orderOccupation').val();
    $.ajax({
        url :"index.php?module=ajax_request&action=searchEmployeeOccupations",
        type:"POST",
        data:"occupation="+occupation,
        dataType:'json',
        success: function (data){
            $('#orderFullname').empty();
            if(data.length > 0) {
                $.each(data, function(key, val) {
                    $('#orderFullname').append($('<option>').text(val.firstname+' '+val.lastname + ' - '+val.field_data).attr('value',val.id));
                });
            }
        },
        error: function(error){
            console.log('Request failed: ' + error);
        }
    });
}

</script>
<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>