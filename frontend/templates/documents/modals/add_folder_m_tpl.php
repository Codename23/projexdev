<?php


?>
<style>
    .changed {
        -webkit-filter : hue-rotate(180deg);
        filter : hue-rotate(180deg);
    }
</style>
<!-- Modal -->
<div class="modal-dialog">
<form method="post" action="<?php echo BASE_URL . "/index.php?module=folders&action=addNewFolder"; ?>" autocomplete="off">
    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">add folders</h4>
    </div>

    <div class="modal-body">
        <div class="row">

            <div class="col-md-3">
                <div class="modal-icon-left"><img src="<?php echo SITE_IMAGE_PATH .'modal_icons/folder_yellow_plus.png'; ?>" class="changed" /></div>
            </div>

            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="folderName" class="control-label">Folder Name</label>
                            <input type="text" class="form-control" name="folderName" id="folderName" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="parentId" class="control-label">Parent Folder</label>
                            <select class="form-control" name="parentId" id="parentId">
                                <option value="-1">- none -</option>

                                <?php if($data['folderListing']): foreach ($data['folderListing']as $folderListing):


                                    if($data['folderInfo']['id'] == $folderListing['id']){
                                        continue;
                                    }

                                    echo '<option value="' . $folderListing['id']. '"';
                                    if ($data['parentId'] == intval($folderListing['id']))
                                    {
                                        echo ' SELECTED';
                                    }
                                    echo '>' . $folderListing['folder_name'] . '</option>';
                                    ?>
                                <?php endforeach;endif; ?>

                            </select>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="modal-footer">
        <input type="hidden" name="submitme" id="submitme" value="1"/>

        <button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
        <button type="submit" class="btn btn-info">Add Folder <i class="entypo-check"></i></button>
    </div>
</form>
</div>
</div>