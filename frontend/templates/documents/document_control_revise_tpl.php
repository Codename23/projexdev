<?php
$title = 'Revise Documents';
include_once('frontend/templates/headers/default_header_tpl.php');
echo '<pre>';
print_r($data);
echo '</pre>';
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php include_once('frontend/templates/menus/main-menu.php'); 
      include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10"> 
    
    <div class="panel panel-default">
        <div class="panel-heading">Revise</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th colspan="2">Document Details</th>
                </tr>
            </thead>
             <?php 
            if(!empty($data['documentDetails'])){
            ?>
            <tbody>
                <tr>
                    <td width="30%">Group</td>
                    <td><?php echo $data['documentDetails']['sheqteam_name']; ?></td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td><?php echo $data['documentDetails']['type_name']; ?></td>
                </tr>
                <tr>
                    <td>Document Description</td>
                    <td><?php echo $data['documentDetails']['description']; ?></td>
                </tr>
                <tr>
                    <td>Document No</td>
                    <td><?php echo $data['documentDetails']['document_number']; ?></td>
                </tr>
                <tr>
                    <td>System No</td>
                    <td><?php echo $data['documentDetails']['system_number']; ?></td>
                </tr>
                <tr>
                    <td>Previous Originator</td>
                    <td><?php echo $data['documentDetails']['firstname'] ." ".$data['documentDetails']['lastname']; ?></td>
                </tr>
                <tr>
                    <td>Previously Approved By</td>
                    <td><?php if(!empty($data['approvedBy'])) {
                            foreach($data['approvedBy'] as $approvedByDetails){
                                echo '<p><a>' . $approvedByDetails['firstname'].' '.$approvedByDetails['lastname'] . '</a></p>';
                            }
                        }else{
                            echo '<p>No one approved the document</p>';
                        }?>
                    </td>
                </tr>
                <tr>
                    <td>Revision Frequency</td>
                    <td><?php echo $data['documentDetails']['revision_frequency']; ?></td>
                </tr>
                <tr>
                    <td>Next Revision Date</td>
                    <td><?php echo $data['documentDetails']['next_revision_date']; ?></td>
                </tr>
                <tr>
                    <td>View</td>
                    <td><a href="index.php?action=downloadDocument&module=documents&docId=<?php echo $data['documentDetails']['id']; ?>"><?php echo $data['documentDetails']['description']; ?></a></td>
                </tr>
            </tbody>
            <?php 
                }
            ?>
        </table>
    </div>
        
    </div>
    </div>
    <form class="form-horizontal" name="reviseForm" action="index.php">
        <input type="hidden"  name="docId" value="<?php echo $data['documentDetails']['id']; ?>">
        <input type="hidden"  name="action" value="saveRevision">
        <input type="hidden"  name="module" value="documents">
    <div class="panel panel-default">
    <div class="panel-body">
        
        <div class="form-group">
            <label class="control-label col-sm-4" for="documentOriginatorId">New Originator</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select name="documentOriginator" id="documentOriginator" class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a document originator" data-header="Close">
                <?php 
                    if(!empty($data['getallEmployee'])) { 
                        foreach($data['getallEmployee'] as $user){
                            echo '<option value="'.$user['user_id'].'">'.$user['firstname'].' '.$user['lastname'].'</option>';
                        }
                    }
                    ?> 
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="approvers">Approved By</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" name="approvers[]" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people"data-header="Close">
                    <?php 
                    if(!empty($data['getallEmployee'])) { 
                        foreach($data['getallEmployee'] as $user){
                            echo '<option value="'.$user['user_id'].'">'.$user['firstname'].' '.$user['lastname'].'</option>';
                        }
                    }
                    ?> 
                </select>
            </div>
        </div>
            
        <div class="form-group">
            <label class="control-label col-sm-4" for="documentRevisionFrequency">Revision Frequency</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" name="documentRevisionFrequency" id="documentRevisionFrequency" required>
                <option value selected disabled>Please select a revision frequency</option>
                <option value="1">Monthly</option>
                <option value="2">Every 6 Months</option>
                <option value="3">Yearly</option>
                <option value="4">Every 2 Years</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="documentNextRevisionDate">Next Revision Date</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" id="documentNextRevisionDate" name="documentNextRevisionDate" required>
            </div>
        </div>
            
            
            </div> 
    </div>
<!--End of panel panel-default-->      
    <div class="panel panel-default">
        <div class="panel-heading">Re-Upload Edited Document <span class="pull-right"><p style="color:red;"><b>All amendments should be in red text</b></p></span></div>
    <div class="panel-body">
        
        <div class="form-group">
            <label class="control-label col-sm-4" for="downloadDoc">Download Document</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <a href="index.php?action=downloadDocument&module=documents&docId=<?php echo $data['documentDetails']['id']; ?>"><button type="button" id="addResource" class="btn btn-success">Download</button></a>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="reuploadDoc">Re-upload Word Document</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="file" id="reuploadDoc" name="reuploadDoc">
            </div>
        </div>       
    </div>
    </div>
    

<!--End of panel panel-default-->

    <div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-4" for="changes">Were there any changes made to the document?</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <label class="radio-inline"><input type="radio" name="changes" id="changes1" value="0" required>Yes</label>
                <label class="radio-inline"><input type="radio" name="changes" id="changes2" value="1" >No</label>
            </div>
        </div>
        <div class="sendForApprovalBtn sendForApprovalBtnBox">
        <div class="form-group">
            <div class="col-lg-offset-4 col-lg-4 col-md-4 col-sm-8">
            <a href="#sendForApproval-modal"   data-toggle="modal" data-target="#sendForApproval-modal"><button type="button" id="sendApprovalBtn" class="btn btn-success btn-block">Send For Approval</button></a>
            </div>
        </div>
        </div>
        <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" class="btn btn-success">Approve</button>
                </div>
            </div>
    </div>
    </div>

</form>
</div>

<!-- Send For Approval Modal -->
  <div class="modal fade" id="sendForApproval-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Send For Approval</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="sendForApprovalForm" action="document_control_revise.php">
            <!--<div class="form-group">
                <label class="control-label col-sm-4" for="approveDoc">Approve Document</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <label class="radio-inline"><input type="radio" name="approveDoc" id="approveDoc1" value="0" required>Yes</label>
                    <label class="radio-inline"><input type="radio" name="approveDoc" id="approveDoc2" value="1" >No</label>
                </div>
            </div>-->
            <!--<div class="message messageBox">-->
            <div class="form-group">
                <label class="control-label col-sm-4" for="approvalMessage">Message to person who approve document</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <textarea class="form-control" rows="5" name="approvalMessage" id="approvalMessage" required></textarea>
                </div>
            </div>
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Order approval</th>
                            <th>Name Surname</th>
                            <th>Department</th>
                            <th>Occupation</th>
                            <th>Confirm E-Mail</th>
                            <th>Due Date for Approval</th>
                            <th><a href="#addApproval-modal" data-toggle="modal" data-target="#addApproval-modal"><span class="glyphicon glyphicon-plus"></span></a></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>    
            </div>
            <!--</div>-->
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="sendForApprovalBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Add Approval Modal -->
  <div class="modal fade" id="addApproval-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Approval</h4>
        </div>
                <div class="modal-body">
            <form class="form-horizontal" method="post" name="addApprovalForm" action="index.php?module=documents&action=saveApprovalPerson">
            <!--
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderApproval">Order Approval</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="orderApproval" class="form-control">
                        <option value="" selected disabled>Please select an order</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
            </div>
            !-->
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderDepartment">Department</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select id="orderDepartment" name="orderDepartment" class="form-control">
                    <option value="" selected disabled>Please select a department</option>
                    <?php
                    foreach($data['getAllDepartment'] as $departmentDetsArr){
                        echo '<option value="' . $departmentDetsArr['id'] . '">' .$departmentDetsArr['department_name'].'</option>';
                    }
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderOccupation">Occupation</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select id="orderOccupation" name="orderOccupation" class="form-control">
                        <option value="" selected disabled>Please select an occupation</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderFullname">Name Surname</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="employeeId" id="orderFullname" required>
                        <option value="" selected disabled>Search a user</option>
                    </select>
                </div>
            </div>
            <!--
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderConfirmEmail">Confirming E-Mail</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="email" class="form-control" name="orderConfirmEmail" id="orderConfirmEmail" required placeholder="please confirm e-mail">
                </div>
            </div>
            !-->
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderDueDate">Due Date for Approval</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="orderDueDate" id="orderDueDate" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <input type="hidden"  name="docid" value="1">
                <button type="submit" id="addApprovalBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div><!--End of container-fluid-->
<script src="js/bootstrap-select.js"></script>
<script>
$(function() {
$(".add-another-person").click(function(){ 
          var html = $(".copy-approvedBy").html();
          $(".after-add-another-person").after(html);
});

$("#documentNextRevisionDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();

$(".add-another-link").click(function(){ 
    var html = $(".copy-another-link").html();
    $(".after-add-another-link").after(html);
    });
    $("body").on("click",".remove",function(){ 
    $(this).parents(".control-group").remove();
});

$('input[name="changes"]').click(function(){
        if($(this).attr("value")==="0"){
            $(".sendForApprovalBtnBox").not(".sendForApprovalBtn").hide();
            $(".sendForApprovalBtn").show();
        }else{
            $(".sendForApprovalBtn").hide();
        }
});

$("#addApprovalBtn").click(function(){
        $("#addApproval-modal").modal('toggle');
    });

$('input[name="approveDoc"]').click(function(){
        if($(this).attr("value")==="0"){
            $(".messageBox").not(".message").hide();
            $(".message").show();
        }else{
            $(".message").hide();
        }
});


    
tinymce.init({
  selector: '#docContent',
  height: 300,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
  ],
  toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
 });
</script>
<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?> 
    