<?php
/**
 * Ajax results set
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */
header("HTTP/1.0 200 OK");
header('Content-type: application/json; charset=utf-8');
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Pragma: no-cache");

$folderData =  array();
$folderType = "Folder";


if($data['folders']){
    foreach ($data['folders'] as $folder){

        if((int)$folder['subFolderCount'] > 0)
        {
            $folderData[] = array('data'=>$folder['folder_name'].(((int)$folder['documentsCount']>0)?(' ('.number_format($folder['documentsCount']).')'):'').' ', 'attr'=>array('id'=>$folder['id'], 'title'=>'Double click to view/hide subfolders', 'rel'=>$folderType), 'children'=> array('state'=>'closed'), 'state'=>'closed');
        }
        else
        {
         $folderData[] = array('data'=>$folder['folder_name'].(((int)$folder['documentsCount']>0)?(' ('.number_format($folder['documentsCount']).')'):''), 'attr'=>array('id'=>$folder['id'], 'title'=>'', 'rel'=>$folderType));
        }
    }
}

echo json_encode($folderData);
