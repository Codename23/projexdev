<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017/02/27
 * Time: 2:17 PM
 */

if(!isset($_SESSION['folderViewType']))
{
    $_SESSION['folderViewType'] = 'fileManagerIcon';
    if(DEFAULT_FOLDER_VIEW == 'listView')
    {
        $_SESSION['folderViewType'] = 'fileManagerList';
    }
}
?>
<style>
    .changed {
        -webkit-filter : hue-rotate(180deg);
         filter : hue-rotate(180deg);
    }
</style>
<div class="image-browse">
    <div id="fileManager" class="fileManager  <?php echo  $_SESSION['folderViewType']; ?>"><div class="toolbar-container">
            <!-- toolbar -->
            <div class="col-md-6 clearfix">
                <!-- breadcrumbs -->
                <div class="row breadcrumbs-container">
                    <div class="col-md-12 col-sm-12 clearfix">
                        <ol id="folderBreadcrumbs" class="btn-group btn-breadcrumb">
                            <a href="#" onclick="loadImages(-1, 1); return false;" class="btn btn-white mid-item">
                                <i class="glyphicon glyphicon-home"></i>
                            </a>
                            <a href="#" data-toggle="dropdown" class="btn btn-white"><?php echo ($data['folderStats']) ?$data['folderStats']['folder_name'] ."-  File ". $data['folderStats']['subFolderCount'] : 'Root Folder'; ?>&nbsp;&nbsp;<i class="caret"></i></a>
                            <ul role="menu" class="dropdown-menu dropdown-white pull-left">
                                <li>
                                    <a href="#" onclick="uploadFiles('');"><span class="context-menu-icon"><span class="glyphicon glyphicon-cloud-upload"></span></span>Upload Files</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="#" onclick="showAddFolderForm(-1);"><span class="context-menu-icon"><span class="glyphicon glyphicon-plus"></span></span>Add Folder</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="#" onclick="selectAllFiles();"><span class="context-menu-icon"><span class="glyphicon glyphicon-check"></span></span>Select All Files</a>
                                </li>
                            </ul>
                            <a class="add-sub-folder-plus-btn" href="#" onclick="showAddFolderForm(0); return false;" title="" data-original-title="Add Sub Folder" data-placement="bottom" data-toggle="tooltip"><i class="glyphicon glyphicon-plus-sign"></i></a></ol>
                    </div>
                </div>
            </div>
            <div class="col-md-6 clearfix right-toolbar-options">
                <div class="list-inline pull-right">
                    <div class="btn-toolbar pull-right" role="toolbar">
                        <div class="btn-group hidden-xs">
                            <button class="btn btn-white disabled fileActionLinks" type="button" title="" data-original-title="Links" data-placement="bottom" data-toggle="tooltip" onclick="viewFileLinks();
								return false;"><i class="entypo-link"></i></button>
                            <button class="btn btn-white disabled fileActionLinks" type="button" title="" data-original-title="Delete" data-placement="bottom" data-toggle="tooltip" onclick="deleteFiles();
								return false;"><i class="entypo-cancel"></i></button>
                            <button class="btn btn-white" type="button" title="" data-original-title="List View" data-placement="bottom" data-toggle="tooltip" onclick="toggleViewType();
								return false;" id="viewTypeText"><i class="entypo-list"></i>
                            </button>
                            <button class="btn btn-white" type="button" title="" data-original-title="Fullscreen" data-placement="bottom" data-toggle="tooltip" onclick="toggleFullScreenMode();
								return false;"><i class="entypo-resize-full"></i>
                            </button>
                        </div>
                        <div class="btn-group"><div class="btn-group">
                                <button id="filterButton" data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">
                                    Filename ASC <i class="entypo-arrow-combo"></i>
                                </button>
                                <ul role="menu" class="dropdown-menu dropdown-white pull-right">
                                    <li class="disabled"><a href="#">Sort By</a></li><li><a href="#" onclick="updateSorting('order_by_filename_asc', 'Filename ASC', this); return false;">Filename ASC</a></li><li><a href="#" onclick="updateSorting('order_by_filename_desc', 'Filename DESC', this); return false;">Filename DESC</a></li><li><a href="#" onclick="updateSorting('order_by_uploaded_date_asc', 'Uploaded Date ASC', this); return false;">Uploaded Date ASC</a></li><li><a href="#" onclick="updateSorting('order_by_uploaded_date_desc', 'Uploaded Date DESC', this); return false;">Uploaded Date DESC</a></li><li><a href="#" onclick="updateSorting('order_by_downloads_asc', 'Downloads ASC', this); return false;">Downloads ASC</a></li><li><a href="#" onclick="updateSorting('order_by_downloads_desc', 'Downloads DESC', this); return false;">Downloads DESC</a></li><li><a href="#" onclick="updateSorting('order_by_filesize_asc', 'Filesize ASC', this); return false;">Filesize ASC</a></li><li><a href="#" onclick="updateSorting('order_by_filesize_desc', 'Filesize DESC', this); return false;">Filesize DESC</a></li><li><a href="#" onclick="updateSorting('order_by_last_access_date_asc', 'Last Access Date ASC', this); return false;">Last Access Date ASC</a></li><li><a href="#" onclick="updateSorting('order_by_last_access_date_desc', 'Last Access Date DESC', this); return false;">Last Access Date DESC</a></li></ul>
                                <input name="filterOrderBy" id="filterOrderBy" value="order_by_filename_asc" type="hidden">
                            </div>
                            <div class="btn-group">
                                <button id="perPageButton" data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">
                                    30 <i class="entypo-arrow-combo"></i>
                                </button>
                                <ul role="menu" class="dropdown-menu dropdown-white pull-right per-page-menu">
                                    <li class="disabled"><a href="#">Per Page:</a></li><li><a href="#" onclick="updatePerPage('15', '15', this); return false;">15</a></li><li><a href="#" onclick="updatePerPage('30', '30', this); return false;">30</a></li><li><a href="#" onclick="updatePerPage('50', '50', this); return false;">50</a></li><li><a href="#" onclick="updatePerPage('100', '100', this); return false;">100</a></li><li><a href="#" onclick="updatePerPage('250', '250', this); return false;">250</a></li></ul>
                                <input name="perPageElement" id="perPageElement" value="100" type="hidden">
                            </div>
                        </div>
                    </div>
                    <ol id="folderBreadcrumbs2" class="breadcrumb bc-3 pull-right">
                        <li class="active">
                            <span id="statusText"></span>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <div class="gallery-env">

            <?php if($data['folders'] ||$data['files'] ): ?>
            <div class="fileListing" id="fileListing">

                <?php if($data['folders']) : foreach ($data['folders'] as $folder) :?>
                <div id="folderItem<?php echo $folder['id']; ?>" data-clipboard-action="copy" data-clipboard-target="#clipboard-placeholder"
                     class="fileItem folderIconLi fileIconLi col-xs-4 image-thumb owned-folder ui-draggable ui-droppable" onclick="loadImages(<?php echo $folder['id']; ?>); return false;"
                     folderid="<?php echo $folder['id']; ?>"
                     sharing-url="">
                    <div class="thumbIcon">
                        <a name="link">
                            <img src="<?php echo SITE_IMAGE_PATH .'folder_full_fm_grid.png';?>" class="changed">
                        </a>
                    </div>
                    <!-- Add toggle view format -->
                    <span class="filesize"></span>
                    <span class="fileUploadDate"><?php echo $folder['documentsCount']; ?> files</span>
                    <span class="thumbList">
                        <a name="link">
                            <img src="<?php echo SITE_IMAGE_PATH .'folder_full_fm_list.png';?>" class="changed">
                        </a>
                    </span>
                    <!-- Add toggle view format -->
                    <span class="filename"><?php echo $folder['folder_name']; ?></span>

                    <div class="fileOptions">
                        <a class="fileDownload" href="#"><i class="caret"></i></a>
                    </div>
                </div>
                <?php endforeach; endif;?>



                <?php if($data['files'] ) : foreach ($data['files'] as $files) :?>
                <div dttitle="<?php echo $files['filename']; ?>" dtsizeraw=""
                     dtuploaddate="<?php echo $files['date_created']; ?>" dtfullurl=""
                     dtfilename="<?php echo $files['filename']; ?>"
                     dtstatsurl=""
                     dturlhtmlcode=""
                     dturlbbcode="" dtextramenuitems="" title="<?php echo $files['filename']; ?>" fileid="<?php echo $files['id']; ?>" class="col-xs-4 image-thumb image-thumb-middle fileItem366 fileIconLi  owned-image ui-draggable">
                    <div class="thumbIcon"><a name="link">
                            <img src="<?php  ;?>" alt="" class="#" style="max-width: 100%; max-height: 100%; min-width: 30px; min-height: 30px;">
                        </a>
                    </div>
                    <span class="filesize"></span>
                    <span class="fileUploadDate"><?php echo $files['date_created']; ?></span>
                    <span class="fileOwner"></span>
                    <span class="thumbList">
                        <a name="link">
                            <img src="<?php ;?>" alt="">
                        </a>
                    </span>
                    <span class="filename"><?php echo $files['filename']; ?></span>
                    <div class="fileOptions">
                        <a class="fileDownload" href="#"><i class="caret"></i></a>
                    </div>
                </div>
                <?php endforeach; endif;?>

            </div>
            <?php  else:?>
            <h1>No context to display </h1>
            <?php  endif; ?>
        </div>

    </div>
</div>