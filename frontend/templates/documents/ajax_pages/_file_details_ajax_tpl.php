<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017/03/07
 * Time: 10:39 AM
 */
?>
<div class="file-browse-container-wrapper">
    <div class="file-preview-wrapper">
        <div class="row">
            <div class="col-md-9">

                <!--view file-->
                <?php if($data['fileInfo']['folder_path'] != ''): ?>
                <iframe src="https://docs.google.com/gview?url=<?php echo HOSTNAME."/FOLDER_DIRECTORY_ROOT/".$data['fileInfo']['folder_path'].$data['fileInfo']['filename']; ?>&embedded=true" height="700" width="100%" frameborder="0" style="border: 0px solid #ddd;" class="background-loader"></iframe>
                <?php else: ?>
                <h1>No Document to display</h1>
                <?php endif; ?>
            </div>
            <div class="col-md-3">
                <div class="section-wrapper">
                    <span class="text-section">
                                <a href="#" class="text-section-1" onclick="loadImages(<?php echo $data['fileInfo']['id']; ?>); return false;"></a>
                                Added By <?php echo $data['fileInfo']['created_by']; ?>
                    </span>
                </div>

                <div class="section-wrapper">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <td class="view-file-details-first-row">
                                Uploaded By:
                            </td>
                            <td class="responsiveTable">
                                <?php echo $data['fileInfo']['created_by']; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="view-file-details-first-row">
                                Filename:
                            </td>
                            <td class="responsiveTable">
                                <?php echo $data['fileInfo']['filename']; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="view-file-details-first-row">
                                Uploaded:
                            </td>
                            <td class="responsiveTable">
                                <?php echo $data['fileInfo']['date_created']; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="view-file-details-first-row">
                                Version:
                            </td>
                            <td class="responsiveTable">
                                <?php echo $data['fileInfo']['current_revision_number']; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="view-file-details-first-row">
                                Next Revision:
                            </td>
                            <td class="responsiveTable">
                                <?php echo $data['fileInfo']['next_revision_date']; ?>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
