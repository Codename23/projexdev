<?php 
/*
 * Header file
 */
include_once('includes/header.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('includes/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('includes/side-menu.php'); 
?> 
<!--End of navigation--> 
<div class="col-lg-10">
    <div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="document_control.php">Approved Docs</a></li>
        <li><a href="document_control_approval.php">Approval</a></li>
        <li class="active"><a href="document_control_records.php">Records</a></li>
        <li><a href="document_control_archive.php">Document Archive</a></li>
        <li><a href="document_control_templates.php">Templates</a></li>
        <li><a href="">External Docs</a></li>
        <li><a href="document_control_settings.php">Document Settings</a></li>
    </ul>
    </div>
    </div>  
    
    <div class="row">
        <div class="col-lg-9">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li><a href="document_control_records.php">Context Of Business</a></li> 
            <li class="active"><a href="document_control_records_management.php">Management</a></li> 
            <li><a href="">Resource & Maintenance</a></li>
            <li><a href="">Meetings</a></li>
        </ul>
        </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading">Folders & Files</div>
    <div class="panel-body">
        <div id="fileTreeDemo_1" class="demo"></div>
    </div>
    </div>
    <!--End of panel panel-default-->

</div>
    </div><!--End of row-->
</div><!--End of container-fluid-->

<?php 
/*
 * 
 */
include_once('includes/footer.php');
?> 
<script src="jquerytree/jquery.easing.js" type="text/javascript"></script>
<script src="jquerytree/jqueryFileTree.js" type="text/javascript"></script>
<link href="jquerytree/jqueryFileTree.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript">

        $(document).ready( function() {

                $('#fileTreeDemo_1').fileTree({ root: '../module folders/management/', script: 'jquerytree/connectors/jqueryFileTree.php' }, function(file) { 
                        alert(file);
                });
        });
</script>
    