<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Innovatorshill | <?php echo  $pageTitle; ?></title>

        <!-- core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        
        <!-- jQuery (For Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <style>
        body{
            line-height: 1.42857143;
        }
        .header{
            margin-top: 15px;
        }
        .doc_name{
            padding:15px;
        }
    </style>
<body> 

<div class="container"> 
    <div class="row header">
        <div class="col-md-12">
            <div class="col-md-6 col-sm-5">
                <img class="img-responsive" src="images/logo.png" alt="logo" />
            </div>
            <div class="col-md-6 col-sm-7">
                <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <p>Address: 2 Concorde <br/> <span style="margin-left: 47px;">Airport City</span> <br/> <span style="margin-left: 47px;">Cape Town</span></p>
                        <p>PTY no: 123456/2323/23</p>
                        <p>Tel: 021 333 3333</p>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <p>Doc No: 243434234324</p>
                        <p>Doc Type: ffffffff</p>
                        <p>Rev Date: 12 June 2017</p>
                        <p>Rev No: 234234234243</p>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row doc_name">
        <div class="col-md-12">
            <h3>Appointment</h3>
            <h3>Document Name</h3>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div style=" font-weight: bold;" class="col-md-3 col-sm-6 col-xs-6">
                <p>Name Of Appointed Person:</p>
                <p>ID Number:</p>
                <p>Legal Appointment:</p>
                <p>Department:</p>
                <p>Date Appointed:</p>
                <p>Duration:</p>
            </div>
            <div class="col-md-9 col-sm-6 col-xs-6">
                <p>CORLIA CLAASSEN</p>
                <p>7603310056084</p>
                <p>Incident Investigator</p>
                <p>OHS</p>
                <p>09 December 2015</p>
                <p>1 Year</p>
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    
    <div class="row footer">
        <div class="col-md-12">
            <div class="col-md-3 col-sm-3 col-xs-3">
            <h6>Doc No:</h6>
            <h6>System No:</h6>
            </div>
            
            <div class="col-md-3 col-sm-3 col-xs-3">
            <h6>Doc Description:</h6>
            <h6>Doc Type:</h6>
            </div>
            
            <div class="col-md-3 col-sm-3 col-xs-3">
            <h6>Date Approved:</h6>
            <h6>Rev Date:</h6>
            </div>
            
            <div class="col-md-3 col-sm-3 col-xs-3">
            <h6>Approved By:</h6>
            <h6>Rev No: <span class="pull-right">1/1</span></h6>
            </div>
        </div>
    </div>
</div><!--End of container-fluid-->     
</body>
</html>
    