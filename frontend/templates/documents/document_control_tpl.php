<?php 
$title = 'Approved Documents';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
include_once('frontend/templates/menus/main-menu.php'); 
include_once('frontend/templates/menus/side-menu.php'); 
?> 

<!--End of navigation--> 
<div class="col-lg-10">
    <div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a>Approved Docs</a></li>
        <li><a href="index.php?action=approvalDocuments&module=documents">Approval</a></li>
        <li><a href="index.php?action=viewAllDocumentsRecords&module=documents">Records</a></li>
        <li><a href="index.php?action=documentArchive&module=documents">Document Archive</a></li>
        <li><a>Templates</a></li>
        <li><a>External Docs</a></li>
        <li><a href="index.php?action=documentSettings&module=documents">Document Settings</a></li>
    </ul>
    </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-inline" method="post" action="index.php?module=documents&action=addDocuments">
            <div class="form-group">
                <button type="submit" id="addDoc" class="btn btn-success">Add Document</button>
            </div>
        </form>
        </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading">Search Documents</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="searchDocs" action="index.php">
        <input type="hidden" class="form-control" name="action" value="approvedDocuments">
        <input type="hidden" class="form-control" name="module" value="documents">
        <div class="col-md-12" style="margin-bottom: 10px;">    
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Group</b></p>
            <select class="selectpicker" name="groups[]" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more group types"data-header="Close">
                <option value="1">SHEQ</option>
                <option value="2">SHE</option>
                <option value="3">OHS</option>
                <option value="4">Q</option>
                <option value="5">E</option>
            </select>
        </div>
        </div>
        
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Originator</b></p>
            <select class="selectpicker" name="originators[]" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more originators"data-header="Close">
                <?php 
                if(!empty($data['getallEmployee'])) { 
                    foreach($data['getallEmployee'] as $user){
                        echo '<option value="'.$user['user_id'].'">'.$user['firstname'].' '.$user['lastname'].'</option>';
                    }
                }    
                ?> 
            </select>
        </div>
        </div>
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>
    
    <div class="panel-group" id="accordion">
        <?php $count = 0; 
        foreach($data['getDocumentTypes'] as $documentTypesDetails){ 
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $documentTypesDetails['id']; ?>"><?php echo $documentTypesDetails['type_name']; ?></a>
                </h4>
            </div>
            <div id="collapse<?php echo $documentTypesDetails['id']; ?>" class="panel-collapse collapse<?php if($count == 0){ echo " in";}?>">
                <div class="panel-body">
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Doc No</th>
                                <th>System Ref</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Rev No</th>
                                <th>Next Revision Date</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <?php if(!empty($data['getallApprovedDocs'])) { ?> 
                        <tbody>
                        <?php
                        foreach($data['getallApprovedDocs'] as $docApprovalDetails){
                            if($docApprovalDetails['doc_type_id'] == $documentTypesDetails['id']){
                            echo'<tr>
                                    <td>' . $docApprovalDetails['document_number'] . '</td>
                                    <td>' . $docApprovalDetails['system_number'] . '</td>
                                    <td>' . $docApprovalDetails['description'] . '</td>
                                    <td>' . $docApprovalDetails['sheqteam_name'] . '</td>
                                    <td>' . $docApprovalDetails['current_revision_number'] . '</td>
                                    <td>' . $docApprovalDetails['next_revision_date'] . '</td>
                                    <td><a href="index.php?action=downloadDocument&module=documents&docId='.$docApprovalDetails['id'].'"><span class="glyphicon glyphicon-download-alt"></span></a></td>
                                    <td><a href="index.php?action=reviseDocuments&module=documents&id=' . $docApprovalDetails['id'] . '">Revise</a></td>
                                </tr>';
                            }
                        }
                        ?>
                        </tbody>
                        <?php } ?>
                    </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <!--panel panel-default-->
        <?php $count ++; } ?>
    </div>
<!--End of panel-group-->

</div>
    </div><!--End of row-->
</div><!--End of container-fluid-->

<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>  
    