<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2016/12/13
 * Time: 10:33 AM
 */
?>
<script type="text/javascript">
    $(document).ready(function() {

        $('#occupationName').empty();
        $('.occupation_options').hide();

     <?php if(isset($_SESSION['branch_id'])){ ?>

        var opt = '';
        var branch_id = '';
        branch_id = <?php echo $_SESSION['branch_id']; ?>;

        $.ajax({
            type: "POST",
            url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxBranchDepartments';?>",
            data: {'branch_id': branch_id},
            dataType: "json", //return type expected as json
            success: function(departments) {

                $('#departmentName').empty();
                $("<option  value=0>&nbsp;</option>").prependTo("#departmentName");
                if (departments.length > 0) {
                    $.each(departments, function(key, val) {
                        opt = $('<option/>');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#departmentName').append(opt);
                    });
                    $("#departmentName").attr("disabled", false);
                } else {
                    $('#occupationName').empty();
                    $("#occupationName").attr("disabled", true);
                    $("#departmentName").attr("disabled", true);
                }
            }
        });

        <?php } ?>

        $('#branchName').change(function() {
            var opt = '';
            var branch_id = '';
            branch_id = $(this).find(':selected').val()

            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxBranchDepartments';?>",
                data: {'branch_id': branch_id},
                dataType: "json", //return type expected as json
                success: function(departments) {

                $('#departmentName').empty();
                $("<option  value=0>&nbsp;</option>").prependTo("#departmentName");

                if (departments.length > 0) {
                    $.each(departments, function(key, val) {
                        opt = $('<option/>');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#departmentName').append(opt);
                    });
                    $("#departmentName").attr("disabled", false);
                } else {
                    $('#occupationName').empty();
                    $("#occupationName").attr("disabled", true);
                    $("#departmentName").attr("disabled", true);
                }
            }
            });
        });

        /**
         * Get Department occupations..
         */
        $('#departmentName').change(function() {
            var opt = '';
            var department_id = '';
            department_id = $(this).find(':selected').val()

            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxDepartmentsOccupations';?>",
                data: {'department_id': department_id},
                dataType: "json", //return type expected as json
                success: function(occupations) {



                    $('#occupationName').empty();
                    $("<option  value=0>&nbsp;</option>").prependTo("#occupationName ");

                    if (occupations.length === 1 && occupations[0].id === '0' && department_id > 0) {
                        $('.modalAddOccupation').show();
                    }

                if (occupations.length > 0) {
                    $.each(occupations, function(key, val) {
                        opt = $('<option/>');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#occupationName').append(opt);
                    });
                    $('.occupation_options').show();
                    $("#occupationName").attr("disabled", false);
                } else {
                    $("#occupationName").attr("disabled", true);
                }
            }
            });
        });


    });
</script>