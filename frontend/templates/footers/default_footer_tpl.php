<?php
/**  
 * @package sheqonline
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

//include the template
include_once('classes/page_class.php');
?>
            <div id="footer">
                <div class="copyright">
                    <a href="http://themifycloud.com"></a></div>
            </div>
            <!--END FOOTER-->
        </div>
        <!--END PAGE WRAPPER-->
    </div>
</div>    
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!--<script src="script/jquery-1.10.2.min.js"></script>-->
<script src="script/jquery-migrate-1.2.1.min.js"></script>
<!--loading bootstrap js-->
<script src="<?php echo BASE_URL.'/frontend/js/';?>bootstrap.min.js"></script>
<script src="<?php echo BASE_URL.'/frontend/js/';?>jquery.metisMenu.js"></script>
<script src="<?php echo BASE_URL.'/frontend/js/';?>jquery.menu.js"></script>
<!--LOADING SCRIPTS FOR PAGE--><!--CORE JAVASCRIPT-->
<script src="<?php echo BASE_URL.'/frontend/js/';?>main.js"></script>
<script src="<?php echo BASE_URL.'/frontend/js/';?>Chart.bundle.js"></script>
<script src="<?php echo BASE_URL.'/frontend/js/';?>utils.js"></script>
<script src="<?php echo BASE_URL.'/frontend/js/';?>moment.min.js"></script>
<script src="<?php echo BASE_URL.'/frontend/js/';?>jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo BASE_URL.'/frontend/js/';?>fullcalendar.min.js"></script>
<script src="<?php echo BASE_URL.'/frontend/js/';?>jquery.bootstrap.wizard.js"></script>
<script src="<?php echo BASE_URL.'/frontend/js/';?>bootstrap-select.js"></script>
<script src="<?php echo BASE_URL.'/frontend/js/';?>bootstrap-rating.js"></script>
<script src="http://41.185.28.112/sheqmockup/js/sweetalert.js"></script>
<link rel="stylesheet" href="http://41.185.28.112/sheqmockup/css/sweetalert.css">
<!--Tiny MCE-->
<script src="https://cdn.tinymce.com/4/tinymce.min.js"></script>
<script src="<?php echo BASE_URL.'/frontend/js/';?>bootstrap-modal-popover.js"></script>
<script src="<?php echo BASE_URL.'/frontend/js/';?>jquery.tablecheckbox.js"></script>
<?php
if(isset($_GET['message'])){
    $message = $_GET['message'];
    $alert = $_GET['type'];
    ?>
<script type="text/javascript">
$(document).ready(function(){
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    toastr.<?php echo $alert; ?>("<?php echo $message; ?>", "<?php echo "Message"; ?>");
});        
</script>
<?php } ?>
<script>
    $(document).ready(function()
    {
$(".user-companies li a").click(function(e){
    e.preventDefault();
     var company_id;
    company_id = $(this).data('value');
    $.ajax({
        type: "POST",
        url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxCompanyBranches';?>",
        data: {'companyId': company_id},
        dataType: "json", //return type expected as json
        success: function(branches) {
            location.reload(true);
        }
    });
});

$(".user-allowed-branch li a").click(function(e){
    e.preventDefault();
    var branch_id;
    branch_id = $(this).data('value');

    $.ajax({
        type: "POST",
        url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxDefaultBranch';?>",
        data: {'branchId': branch_id},
        dataType: "json", //return type expected as json
        success: function(branches) {

            location.reload(true);
        }
    });    
 
});
});
</script>
<?php $objPage->getFooter(); ?>