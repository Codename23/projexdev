<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2016/12/13
 * Time: 10:33 AM
 */

?>
<script type="text/javascript">
    $(document).ready(function() {


        <?php if (isset($_SESSION['riskNumber'])): ?>
        $('.assessmentHeader').hide();
        <?php endif; ?>
        var selected = [];
        var selectedResource  = [];


        $('#RiskDepartmentName').change(function() {

            selected = [];
            var brands =  $('#RiskDepartmentName option:selected');
            $(brands).each(function(index, brand){
                selected.push($(this).val());
            });
           // $('#RiskDepartmentName').val(selected);
            $('#RiskDepartmentName').selectpicker('refresh');
        });

        $('#selectResource').change(function() {

            selectedResource = [];
            var brands =  $('#selectResource option:selected');
            $(brands).each(function(index, brand){
                selectedResource.push($(this).val());
            });

            //$('#selectResource').val(selectedResource);
            $('#selectResource').selectpicker('refresh');
        });


        $('#resourceCategory').change(function() {
            var opt = '';
            var category_id = '';
            category_id = $(this).find(':selected').val()

            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxAssetType';?>",
                data: {'categoryId': category_id},
                dataType: "json", //return type expected as json
                success: function(resourceTypes) {

                    $('#resourceType').empty();
                    $("<option  value=0>&nbsp;</option>").prependTo("#resourceType");
                    if (resourceTypes.length > 0) {
                        $.each(resourceTypes, function(key, val) {
                            opt = $('<option/>');
                            opt.val(val.id);
                            opt.text(val.name);
                            $('#resourceType').append(opt);
                        });
                        $("#resourceType").attr("disabled", false);
                    } else {

                        $("#resourceType").attr("disabled", true);
                    }
                }
            });
        });

        $('#resourceType').change(function() {
            var opt = '';
            var resourceType = '';
            var category_id = '';

            resourceType = $('#resourceType').find(':selected').val();
            category_id = $('#resourceCategory').find(':selected').val();

            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxSelectRisAssessmentResource';?>",
                data: {'resourceCategory': category_id, 'resourceType':resourceType,'departmentId':selected},
                dataType: "json", //return type expected as json
                success: function(regions) {

                    $('#selectResource').empty();
                    $("<option  value=0>&nbsp;</option>").prependTo("#selectResource");
                    if (regions.length > 0) {
                        $.each(regions, function(key, val) {
                            opt = $('<option/>');
                            opt.val(val.id);
                            opt.text(val.name);
                            $('#selectResource').append(opt);
                        });
                        $("#selectResource").attr("disabled", false);
                        $('#selectResource').selectpicker('refresh');
                    } else {

                        $("#selectResource").attr("disabled", true);
                    }
                }
            });
        });


    });
</script>