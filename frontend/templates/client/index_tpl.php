<?php
/*
 * Header file
 */
$title= 'Customer';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */
    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <?php
    /*
     * Include side menu from the include file
     */
    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->
    <div class="col-lg-10">
        <!--sub menu-->
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-justified topbar-menu">
                    <li class="active"><a href="index.php?module=client&action=viewAllClients">All Customers</a></li>
                    <li><a>Customer Complaints</a></li>
                    <li><a>Settings</a></li>
                </ul>
            </div>
        </div>
        <!--End of sub menu-->
        <div class="panel panel-default">
            <div class="panel-body">
                <a href="<?php echo BASE_URL . "/index.php?module=client&action=addClient"?>" class="btn btn-success">Add Customer</a>
            </div>
        </div>
        <!--End of panel panel-default-->

        <div class="panel panel-default">
            <div class="panel-heading">Customer Search</div>
            <div class="panel-body">
                <form class="form-inline" method="post" name="search" action="">
                    <div class="form-group">
                        <label for="companyName">Name </label>
                        <input type="text" class="form-control" id="companyName" placeholder="Search company name">
                    </div>
                    <div class="form-group">
                        <label for="clientType">Type </label>
                        <input type="text" class="form-control" id="contractorType" placeholder="Search contractor type">
                    </div>
                    <div class="form-group">
                        <label for="operatingArea">Operating Area </label>
                        <select class="form-control" name="headOfficeProvince">
                            <option>Western Cape</option>
                            <option>Northern Cape</option>
                            <option>Gauteng</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Update">
                    </div>
                </form>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Customers</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Company Name/Person</th>
                            <th>Address</th>
                            <th>Contact Person</th>
                            <th>Client Type</th>
                            <th>Email</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if($data['companyClients']): foreach ($data['companyClients'] as $client): ?>
                            <tr>
                                <td><?php echo $client['companyId']; ?></td>
                                <td><a href="<?php echo BASE_URL . "/index.php?module=client&action=viewClientInfo&id=" . $client['companyId']; ?>"><?php echo  ($client['client_type']==1) ? $client['company_name'] :  $client['client_name']." ".$client['client_surname'] ; ?></a></td>
                                <td><?php echo ($client['client_type']==1) ? $client['company_address'] : $client['client_address'] ?></td>
                                <td><?php echo ($client['client_type']==1) ? $client['contact_person'] :$client['client_name']." ".$client['client_surname'] ; ?></td>
                                <td><?php echo $client['type_name'] ?></td>
                                <td><?php echo ($client['client_type']==1) ? $client['email'] : $client['client_email'] ?></td>
                                <td><a href="<?php echo BASE_URL . "/index.php?module=client&action=viewClientInfo&id=" . $client['companyId']; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                <td class="danger"><a href="<?php echo BASE_URL . "/index.php?module=client&action=deleteClient&id=" . $client['companyId']; ?>"><span class="glyphicon glyphicon-trash"></span></a></td>
                            </tr>
                        <?php endforeach;endif; ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--End of the panel panel-default-->

    </div>
</div><!--End of row-->
</div><!--End of container-fluid-->
<?php
/*
 *
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
