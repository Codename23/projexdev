<?php
$title = 'Company Information';
global $objLanguage;
include_once('frontend/templates/headers/default_header_tpl.php');

?>
<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */

    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <!--End of navigation-->

    <?php
    /*
     * Include side menu from the include file
     */

    include_once('frontend/templates/menus/side-menu.php');
    ?>

    <div class="col-lg-10">
        <?php
        /*
         * Include sub menu from the include file
         */

        ?>
        <div class="row company">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h2><span class="glyphicon glyphicon-user" aria-hidden="true"></span><?php echo $data['clientInfo']['client_name']; ?></h2>
                    </div>
                </div>

                <!--BEGIN TABS-->
                <div class="tabbable tabbable-custom tabbable-full-width">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#customer_details" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('CUSTOMER_DETAILS') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#customer_complaints" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('CUSTOMER_COMPLAINTS') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#customer_settings" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('SETTINGS') ;?>
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="customer_details">

                            <div class="panel-group">
                                <form class="form-horizontal" name="editCustomerForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=client&action=editClientInfo">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php  echo $objLanguage->languageText('CUSTOMER_DETAILS') ;?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="individualName"><?php echo $objLanguage->languageText('NAME') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="individualName" name="individualName" value="<?php echo $data['clientInfo']['client_name']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="individualSurname"><?php echo $objLanguage->languageText('SURNAME') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="individualSurname" name="individualSurname" value="<?php echo $data['clientInfo']['client_surname']; ?>"  required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="individualGender"><?php echo $objLanguage->languageText('GENDER') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <select class="form-control" name="individualGender" id="individualGender" required>
                                                        <?php if($data['gender']): foreach ($data['gender'] as $gender):?>

                                                            <?php if($data['clientInfo']['client_gender'] == $gender['id']): ?>
                                                                    <option value="<?php echo $gender['id']; ?>" selected="selected"><?php echo $gender['name']; ?></option>
                                                                <?php else: ?>
                                                                    <option value="<?php echo $gender['id']; ?>" ><?php echo $gender['name']; ?></option>
                                                                <?php endif; ?>

                                                        <?php endforeach;endif;?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="individualEmail"><?php echo $objLanguage->languageText('EMAIL') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="email" class="form-control" id="individualEmail" name="individualEmail" value="<?php echo $data['clientInfo']['client_email']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="individualContactNumber"><?php echo $objLanguage->languageText('CONTACT_NUMBER') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="individualContactNumber" name="individualContactNumber"  value="<?php echo $data['clientInfo']['contact_number']; ?>"  required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="individualAddress"><?php echo $objLanguage->languageText('ADDRESS'); ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="individualAddress"  name="individualAddress"  value="<?php echo $data['clientInfo']['client_address']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <span class="control-label col-sm-4"></span>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="town" name="town"  value="<?php echo $data['clientInfo']['client_town']; ?>"   required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <span class="control-label col-sm-4"></span>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <select name="province" class="form-control" id="province">
                                                        <option>Western Cape</option>
                                                        <option>Northern Cape</option>
                                                        <option>Gauteng</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--End of the panel panel-default-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $objLanguage->languageText('CUSTOMER_TYPE'); ?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="reasonForListing"><?php echo $objLanguage->languageText('REASON_FOR_LISTING'); ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <select name="reasonForListing" class="form-control" id="reasonForListing">
                                                        <option>Complaint</option>
                                                        <option>Normal Listing</option>
                                                        <option>Random</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="uploadDocuments"><?php echo $objLanguage->languageText('UPLOAD_SUPPORTING_DOCUMENTS') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="file" id="uploadDocuments" name="uploadDocuments">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input type="hidden" class="form-control" id="companyId" name="companyId" value="<?php echo $data['clientInfo']['companyId']; ?>">
                                                <input type="hidden" class="form-control" id="companyType" name="companyType" value="<?php echo $data['clientInfo']['client_type']; ?>">
                                                <div class="col-sm-offset-4 col-sm-8">
                                                    <button type="submit" class="btn btn-success"><?php echo $objLanguage->languageText('SUBMIT') ; ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            </form>
                        </div>
                        <!--End of the panel-group-->


                        <!--tab_1_2-->
                        <div class="tab-pane" id="customer_complaints">
                            <div class="complaintsResults"></div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane" id="customer_settings">
                            <div class="settingsResults"></div>
                        </div>
                        <!--end tab-pane-->
                    </div><!--End of row -->
                </div><!--End of container-fluid -->
            </div>

        </div>
    </div>
    <!--END TABS-->
</div>

<!-- END PAGE CONTENT-->


<?php

include_once('frontend/templates/footers/default_footer_tpl.php');
?>

<script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {


        var target = $(e.target).attr("href");

        if ((target == '#customer_complaints')) {
            $.post('<?php echo BASE_URL . "/index.php?module=companies&action=viewAllCompanyBranches&id=" . $data["clientInfo"]['id']; ?>', {}, function (data) {
                $('.scheduleResults').html(data);
            });
        }

        if ((target == '#customer_settings')) {
            $.post('<?php echo BASE_URL . "/index.php?module=customer&action=requestQuote&id=" . $data["clientInfo"]['id']; ?>', {}, function (data) {
                $('.quoteResults').html(data);
            });
        }
    });
</script>