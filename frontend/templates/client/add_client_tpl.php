<?php
/*
 * Header file
 */
$title= 'Add Customer';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */
    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <?php
    /*
     * Include side menu from the include file
     */
    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->
    <div class="col-lg-10">
        <!--sub menu-->
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-justified topbar-menu">
                    <li><a href="index.php?module=client&action=viewAllClients">All Customers</a></li>
                    <li><a>Customer Complaints</a></li>
                    <li><a>Settings</a></li>
                </ul>
            </div>
        </div>
        <!--End of sub menu-->
        <div class="panel-group">
            <form class="form-horizontal" name="addCustomerForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=client&action=addClient">
                <div class="panel panel-default">
                    <div class="panel-heading">New Customer: <b>Company or Individual</b></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="companyIndividual">Company/Individual</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select class="form-control" name="companyIndividual" id="companyIndividual" required>
                                    <option value="" selected disabled>Please select a customer</option>
                                    <?php if($data['companyTypes']): foreach ($data['companyTypes'] as $companyTypes): ?>
                                    <option value="<?php echo strtolower($companyTypes['type_name']); ?>"><?php echo $companyTypes['type_name']; ?></option>
                                    <?php endforeach;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div id="company">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="companyName">Company Name</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" id="companyName" name="companyName" placeholder="Please provide company name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="ptyNumber">PTY Number</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" id="ptyNumber"  name="ptyNumber" placeholder="Please provide PTY number" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="vatNumber">VAT Number</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" id="vatNumber"  name="vatNumber" placeholder="Please provide VAT number" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="officeNumber">Office Number</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" id="officeNumber"  name="officeNumber" placeholder="Please provide office contact number" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="contactPerson">Contact person</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" id="contactPerson"  name="contactPerson" placeholder="Please enter name and surname" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="contactNumber">Contact Number</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" id="contactNumber"  name="contactNumber" placeholder="Please provide your contact number"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="email">E-mail</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="email" class="form-control" id="email"  name="email"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="website">Website</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="url" class="form-control" id="website" name="website" placeholder="Please enter your company website address"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="headOfficeDetails">RSA Head Office Address</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" id="headOfficeDetails" name="headOfficeDetails" placeholder="Please provide your street address"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="control-label col-sm-4"></span>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" id="headOfficeTown" name="headOfficeTown" placeholder="Please provide your town"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="control-label col-sm-4"></span>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <select name="headOfficeProvince" class="form-control" id="headOfficeProvince">
                                        <option>Western Cape</option>
                                        <option>Northern Cape</option>
                                        <option>Gauteng</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div id="individual">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="individualName">Name</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" id="individualName" name="individualName" placeholder="Please provide a name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="individualSurname">Surname</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" id="individualSurname" name="individualSurname" placeholder="Please provide a surname" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="individualGender">Gender</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <select class="form-control" name="individualGender" id="individualGender" required>
                                        <?php if($data['gender']): foreach ($data['gender'] as $gender):?>
                                                <option value="<?php echo $gender['id']; ?>" ><?php echo $gender['name']; ?></option>
                                        <?php endforeach;endif;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="individualEmail">E-mail</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="email" class="form-control" id="individualEmail" name="individualEmail" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="individualContactNumber">Contact Number</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" id="individualContactNumber" name="individualContactNumber" placeholder="Please provide your contact number"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="individualAddress">Address</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" id="individualAddress"  name="individualAddress" placeholder="Please provide your address"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="control-label col-sm-4"></span>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" id="town" name="town" placeholder="Please provide your town"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="control-label col-sm-4"></span>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <select name="province" class="form-control" id="province">
                                        <option>Western Cape</option>
                                        <option>Northern Cape</option>
                                        <option>Gauteng</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End of the panel panel-default-->
                <div class="panel panel-default">
                    <div class="panel-heading">Customer Type</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="reasonForListing">Reason For Listing</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select name="reasonForListing" class="form-control" id="reasonForListing">
                                    <option>Complaint</option>
                                    <option>Normal Listing</option>
                                    <option>Random</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="uploadDocuments">Upload Supporting Documents</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="file" id="uploadDocuments" name="uploadDocuments">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-success">Add Customer</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End of the panel panel-default-->
            </form>
        </div>
        <!--End of the panel-group-->
    </div>
</div><!--End of row-->
</div><!--End of container-fluid-->
<?php
/*
 *
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
