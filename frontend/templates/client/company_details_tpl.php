<?php
$title = 'Company Information';
global $objLanguage;
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */

    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <!--End of navigation-->

    <?php
    /*
     * Include side menu from the include file
     */

    include_once('frontend/templates/menus/side-menu.php');
    ?>

    <div class="col-lg-10">
        <?php
        /*
         * Include sub menu from the include file
         */

        //include_once('frontend/templates/menus/sub-menu.php');
        ?>
        <div class="row company">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?php echo $data['clientInfo']['company_name']; ?></h2>
                    </div>
                </div>

                <!--BEGIN TABS-->
                <div class="tabbable tabbable-custom tabbable-full-width">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#customer_details" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('CUSTOMER_DETAILS') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#customer_complaints" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('CUSTOMER_COMPLAINTS') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#customer_settings" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('SETTINGS') ;?>
                            </a>
                        </li>
                 
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="customer_details">

                            <div class="panel-group">
                                <form class="form-horizontal" name="editCustomerForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=client&action=editClientInfo">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php  echo $objLanguage->languageText('CUSTOMER_DETAILS') ;?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="companyName"><?php echo $objLanguage->languageText('COMPANY_NAME') ;?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="companyName" name="companyName"  value="<?php echo $data['clientInfo']['company_name']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="ptyNumber"><?php echo $objLanguage->languageText('COMPANY_PTY_NUMBER') ;?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="ptyNumber" name="ptyNumber" value="<?php echo $data['clientInfo']['pty_number']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="vatNumber"><?php echo $objLanguage->languageText('COMPANY_VAT_NUMBER') ;?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="vatNumber" name="vatNumber" value="<?php echo $data['clientInfo']['vat_number']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="officeNumber"><?php echo $objLanguage->languageText('OFFICE_NUMBER') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="officeNumber" name="officeNumber" value="<?php echo $data['clientInfo']['office_number']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="contactPerson"><?php echo $objLanguage->languageText('CONTACT_PERSON') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="contactPerson"  name="contactPerson" value="<?php echo $data['clientInfo']['contact_person']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="contactNumber"><?php echo $objLanguage->languageText('CONTACT_NUMBER') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="contactNumber" name="contactNumber" value="<?php echo $data['clientInfo']['company_contact_number']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="email"><?php echo $objLanguage->languageText('EMAIL') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="email" class="form-control" id="email"  name="email" value="<?php echo $data['clientInfo']['email']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="website"><?php echo $objLanguage->languageText('WEBSITE') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="url" class="form-control" id="website" name="website" value="<?php echo $data['clientInfo']['website']; ?>" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="headOfficeDetails"><?php echo $objLanguage->languageText('RSA_HEAD_OFFICE_DETAILS') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                <textarea row="30" cols="10" class="form-control" id="headOfficeDetails" name="headOfficeDetails"><?php echo $data['clientInfo']['company_address']; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <span class="control-label col-sm-4"></span>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="headOfficeTown" name="headOfficeTown" value="<?php echo $data['clientInfo']['company_town']; ?>"  required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <span class="control-label col-sm-4"></span>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <select name="headOfficeProvince" class="form-control" id="headOfficeProvince">
                                                        <option>Western Cape</option>
                                                        <option>Northern Cape</option>
                                                        <option>Gauteng</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--End of the panel panel-default-->

                                    <!--End of the panel panel-default-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $objLanguage->languageText('CUSTOMER_TYPE'); ?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="customerType"><?php echo $objLanguage->languageText('CUSTOMER_TYPE'); ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="customerType" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="reasonForListing"><?php echo $objLanguage->languageText('REASON_FOR_LISTING'); ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <select name="reasonForListing" class="form-control" id="reasonForListing">
                                                        <option>Complaint</option>
                                                        <option>Normal Listing</option>
                                                        <option>Random</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="uploadDocuments"><?php echo $objLanguage->languageText('UPLOAD_SUPPORTING_DOCUMENTS') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="file" id="uploadDocuments">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input type="hidden" class="form-control" id="companyId" name="companyId" value="<?php echo $data['clientInfo']['companyId']; ?>">
                                                <input type="hidden" class="form-control" id="companyType" name="companyType" value="<?php echo $data['clientInfo']['client_type']; ?>">
                                                <div class="col-sm-offset-4 col-sm-8">
                                                    <button type="submit" class="btn btn-success"><?php echo $objLanguage->languageText('UPDATE') ; ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--End of the panel panel-default-->
                                </form>
                            </div>
                            <!--End of the panel-group-->

                        </div>
                        <!--tab_1_2-->
                        <div class="tab-pane" id="customer_complaints">
                            <div class="complaintsResults"></div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane" id="customer_settings">
                            <div class="settingsResults"></div>
                        </div>
                        <!--end tab-pane-->
                    </div><!--End of row -->
                </div><!--End of container-fluid -->
            </div>

        </div>
    </div>
    <!--END TABS-->
</div>
</div>
<!-- END PAGE CONTENT-->

</div>
</div>
<?php

include_once('frontend/templates/footers/default_footer_tpl.php');
?>

<script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {


        var target = $(e.target).attr("href");

        if ((target == '#customer_complaints')) {
            $.post('<?php echo BASE_URL . "/index.php?module=companies&action=viewAllCompanyBranches&id=" .$data["clientInfo"]['id']; ?>', {}, function(data){
                $('.scheduleResults').html(data);
            });
        }

        if ((target == '#customer_settings')) {
            $.post('<?php echo BASE_URL . "/index.php?module=customer&action=requestQuote&id=" .$data["clientInfo"]['id']; ?>', {}, function(data){
                $('.quoteResults').html(data);
            });
        }
</script>