<?php 
/*
 * Header file
 */
$pageTitle = 'Maintenance Plan';
include_once('includes/header.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('includes/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('includes/side-menu.php'); 
?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="maintenance.php">Maintenance Plan</a></li>
        <li><a href="maintenance_upcoming.php">Upcoming</a></li>
        <li><a href="maintenance_job.php">Job</a></li>
        <li><a href="maintenance_history.php">History</a></li>
        <!--<li><a href="maintenance_psa.php">PSA / FMEA</a></li>
        <li><a href="maintenance_schedule.php">Maintenance Schedule</a></li>-->
        <li><a href="maintenance_suggested.php">Suggested Maintenance</a></li>
    </ul>
    </div>
</div> 
<!--End of sub menu-->

    <div class="panel panel-default">
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table no-border-table">
            <thead>
                <tr>
                    <th width="7%">Department</th>
                    <th width="8%">Category</th>
                    <th width="8%">Description</th>
                    <th width="6%">Type</th>
                    <th width="25%">No</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Receiving</td>
                    <td>Moveable Asset</td>
                    <td>Forklift</td>
                    <td>Toyota</td>
                    <td>123</td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>

    <div class="row">
        <div class="col-lg-8">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li class='active'><a href="maintenance_plan.php">Maintenance Plan</a></li>
            <li><a href="maintenance_plan_fmea.php">FMEA</a></li>
            <li><a href="maintenance_plan_suggested.php">Suggested</a></li>
            <li><a href="maintenance_plan_components.php">Components & Part</a></li>
        </ul>
        </div>
    </div>  

    <div class="panel panel-default">
    <div class="panel-heading">Search Maintenance</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
            
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Select Month</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more months"data-header="Close">
                <option>January</option>
                <option>February</option>
                <option>March</option>
                <option>April</option>
                <option>May</option>
                <option>June</option>
                <option>July</option>
                <option>August</option>
                <option>September</option>
                <option>October</option>
                <option>November</option>
                <option>December</option>
            </select>
        </div>
        </div>
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#addMaintenancePlan-modal" data-toggle="modal" data-target="#addMaintenancePlan-modal"><button type="button" id="addAsset" class="btn btn-success">Add Maintenance Plan</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Maintenance Plan</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Next Due Date</th>
                    <th>Frequency</th>
                    <th>Description</th>
                    <th>Service Type</th>
                    <th>Components & Parts</th>
                    <th>Internally / Externally</th>
                    <th>SMP</th>
                    <th>Tools Required</th>
                    <th>SHEQF Factors</th>
                    <th>FMEA Link</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>15/03/2017</td>
                    <td>6 Months</td>
                    <td>Replace old tires with new tires</td>
                    <td>Replace</td>
                    <td><a href=''>View</a></td>
                    <td>Internally</td>
                    <td class="majordanger-table-bg"><a href=''>View</a></td>
                    <td><a href=''>View</a></td>
                    <td><a href=''>View</a></td>
                    <td><a href=''>View</a></td>
                    <td><a href=''>Print Plan</a></td>
                    <td><a href=''>View Plan</a></td>
                    <!--<td><a href="#preventative-modal" data-toggle="modal" data-target="#preventative-modal">Preventative</a></td>-->
                    <td><?php include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                </tr>
                <tr>
                    <td>15/03/2017</td>
                    <td>6 Months</td>
                    <td>Replace oil</td>
                    <td>Replace</td>
                    <td><a href=''>View</a></td>
                    <td>Internally</td>
                    <td class="majordanger-table-bg"><a href=''>View</a></td>
                    <td><a href=''>View</a></td>
                    <td><a href=''>View</a></td>
                    <td><a href=''>View</a></td>
                    <td><a href=''>Print Plan</a></td>
                    <td><a href=''>View Plan</a></td>
                    <!--<td><a href="#preventative-modal" data-toggle="modal" data-target="#preventative-modal">Preventative</a></td>-->
                    <td><?php include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
    
    <!-- Add Maintenance Plan Modal -->
  <div class="modal fade" id="addMaintenancePlan-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Maintenance Plan</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addMaintenancePlanForm" action="maintenance_plan_create.php">
            <div class="form-group">
                <label class="control-label col-sm-4" for="maintenanceDescription">Maintenance Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" name="maintenanceDescription" id="maintenanceDescription" required placeholder=""></textarea> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="maintenanceType">Maintenance Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                    <option value="1">Type 1</option>
                    <option value="2">Type 2</option>
                    <option value="3">Type 3</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="linkFmea">Link FMEA</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a link">
                    <option value="1">Link 1</option>
                    <option value="2">Link 2</option>
                    <option value="3">Link 3</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addMaintenancePlanBtn" class="btn btn-success">Add (Go to Create Page)</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Preventative Modal -->
  <div class="modal fade" id="preventative-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Preventative Plan</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="preventativeForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="nextDate">Next Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="nextDate" id="nextDate" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="frequency">Frequency</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="frequency" class="form-control">
                    <option value selected disabled>Please select a frequency</option>
                    <option value="1">Daily</option>
                    <option value="2">Weekly</option>
                    <option value="3">Monthly</option>
                    <option value="4">Yearly</option>
                    </select>
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="estServiceDuration">Est Service Duration</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="estServiceDuration" id="estServiceDuration" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="responsiblePerson">Responsible Person</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                    <option>Nick Botha</option>
                    <option>Brian Douglas</option>
                    <option>Sunny Mathole</option>
                </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="preventativeBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<script src="js/bootstrap-select.js"></script>
<script>
    $(function() {
    $("#nextDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    });
</script>
<?php 
/*
 * 
 */
include_once('includes/footer.php');
?>  
    