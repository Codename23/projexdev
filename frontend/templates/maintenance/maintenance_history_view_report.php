<?php 
$pageTitle = 'Maintenance History View Report';
include_once('includes/header.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('includes/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('includes/side-menu.php'); 
?> 
<!--End of navigation-->   
    
    <div class="col-lg-10">
        
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Job Details</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Job No</td>
                                    <td>1234</td>
                                </tr>
                                <tr>
                                    <td>Description</td>
                                    <td>Pressure Test Compressor</td>
                                </tr>
                                <tr>
                                    <td>Scheduled Start Date & Time</td>
                                    <td>18/04/2017</td>
                                </tr>
                                <tr>
                                    <td>End Date & Time</td>
                                    <td>18/04/2017</td>
                                </tr>
                                <tr>
                                    <td>Responsible Person</td>
                                    <td><p>Nick Botha</p><p>Sunny Mathole</p></td>
                                </tr>
                                <tr>
                                    <td>Departments Involved</td>
                                    <td><p>I.T</p><p>HR</p></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Job Descriptions</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Maintenance Type</td>
                                    <td>Service</td>
                                </tr>
                                <tr>
                                    <td>Full Job Description</td>
                                    <td>Pressure test all the compressors</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Approval</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">SHEQF</td>
                                    <td>Approved</td>
                                </tr>
                                <tr>
                                    <td>Financial</td>
                                    <td>PO 1234</td>
                                </tr>
                                <tr>
                                    <td>Manageerial</td>
                                    <td>Approved</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Supplier</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Company Name</td>
                                    <td>Western Compressors</td>
                                </tr>
                                <tr>
                                    <td>Rating</td>
                                    <td>5/5</td>
                                </tr>
                                <tr>
                                    <td>Comments</td>
                                    <td>Good Job</td>
                                </tr>
                                <tr>
                                    <td>Cost</td>
                                    <td>R 390.00</td>
                                </tr>
                                <tr>
                                    <td>Documents</td>
                                    <td><a href="">View</a></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>      
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Resources</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Department</th>
                                    <th>Category</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                    <th>No</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Warehouse</td>
                                    <td>Fix Electrical Asset</td>
                                    <td>Compressor</td>
                                    <td>Big Compressor</td>
                                    <td>654</td>
                                </tr>
                            </tbody>
                        </table>   
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Next Service</a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Next Service Date</td>
                                    <td>18/04/2018</td>
                                </tr>
                            </tbody>
                        </table> 
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
        
    </div>  
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('includes/footer.php');
?>  