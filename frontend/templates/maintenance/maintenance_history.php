<?php 
/*
 * Header file
 */
$pageTitle = 'Maintenance History';
include_once('includes/header.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('includes/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('includes/side-menu.php'); 
?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="maintenance.php">Maintenance Plan</a></li>
        <li><a href="maintenance_upcoming.php">Upcoming</a></li>
        <li><a href="maintenance_job.php">Job</a></li>
        <li class="active"><a href="maintenance_history.php">History</a></li>
        <!--<li><a href="maintenance_psa.php">PSA / FMEA</a></li>
        <li><a href="maintenance_schedule.php">Maintenance Schedule</a></li>-->
        <li><a href="maintenance_suggested.php">Suggested Maintenance</a></li>
    </ul>
    </div>
</div>   
<!--End of sub menu-->

<div class="panel panel-default">
    <div class="panel-heading">History</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Job No</th>
                    <th>Starting Date</th>
                    <th>End Date</th>
                    <th>Internal/External</th>
                    <th>Description</th>
                    <th>Department</th>
                    <th>Responsible Person</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>012</td>
                    <td>2017/04/17</td>
                    <td>2017/04/20</td>
                    <td>External</td>
                    <td>Pressure test compressor</td>
                    <td>Warehouse</td>
                    <td>Nick Botha</td>
                    <td><a href="maintenance_history_view_report.php">View Report</a></td>
                </tr>
            </tbody>
        </table>    
    </div>
    </div>
    </div>

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('includes/footer.php');
?>  
    