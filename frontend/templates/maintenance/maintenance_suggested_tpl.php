<?php 
/*
 * Header file
 */
$title = 'Maintenance Suggested';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="maintenance.php">Maintenance Plan</a></li>
        <li><a href="maintenance_upcoming.php">Upcoming</a></li>
        <li><a href="maintenance_job.php">Job</a></li>
        <li><a href="maintenance_history.php">History</a></li>
        <!--<li><a href="maintenance_psa.php">PSA / FMEA</a></li>
        <li><a href="maintenance_schedule.php">Maintenance Schedule</a></li>-->
        <li class="active"><a href="maintenance_suggested.php">Suggested Maintenance</a></li>
    </ul>
    </div>
</div>   
<!--End of sub menu-->

    <div class="panel panel-default">
    <div class="panel-heading">Search Suggested Maintenance</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>Wood</option>
                <option>I.T</option>
                <option>Finance</option>
                <option>HR</option>
            </select>
        </div>
        </div> 
            
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Category</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more categories"data-header="Close">
                <option>Category 1</option>
                <option>Category 2</option>
                <option>Category 3</option>
                <option>Category 4</option>
            </select>
        </div>
        </div> 
 
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Type</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                <option>Building</option>
                <option>Fixed Electrical Equipment</option>
                <option>Key is to be able to add on own behalf</option>
                <option>Forklift</option>
                <option>Wood</option>
                <option>Shutter</option>
                <option>Petrol</option>
                <option>Fire Extinguishers</option>
                <option>Forklift</option>
            </select> 
        </div>
        </div>   
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Suggested Maintenance</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Number</th>
                    <th>Breakdown Date</th>
                    <th>Description / Non-Conformance</th>
                    <th>Parts to be replaced</th>
                    <th>Down Time</th>
                    <th>View Full Job</th>
                    <th>Total Breakdowns</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Wood</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Plainer</td>
                    <td>Wood Plainer</td>
                    <td>1234</td>
                    <td>12/02/2017</td>
                    <td>Chain Broke</td>
                    <td>Chain</td>
                    <td>2 Hours</td>
                    <td><a href=''>View</a></td>
                    <td>23</td>
                    <td><a href="">Create Maintenance Plan</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    