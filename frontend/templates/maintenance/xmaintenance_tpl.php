<?php 
$title = 'Maintenance';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_view&module=maintenance">Dashboard</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_psa&module=maintenance">PSA / FMEA</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_schedule&module=maintenance">Maintenance Schedule</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_suggested&module=maintenance">Suggested Maintenance</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#resourceBreakdown-modal" data-toggle="modal" data-target="#resourceBreakdown-modal"><button type="button" id="resourceBreakdown" class="btn btn-success">Resource Breakdown</button></a><br/><br/>
        <a href="#addAsset-modal" data-toggle="modal" data-target="#addAsset-modal"><button type="button" id="addAsset" class="btn btn-success">Add Asset to Maintenance Table</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Maintenance</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Description</th>
                    <th>Type</th>
                    <th>No</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Receiving</td>
                    <td>Moveable Asset</td>
                    <td>Forklift</td>
                    <td>Toyota</td>
                    <td>1234</td>
                    <td><a href="maintenance_plan.php">Maintenance Plan</a></td>
                    <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    