<?php 
/*
 * Header file
 */
$title = 'Maintenance FMEA';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_view&module=maintenance">Dashboard</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_psa&module=maintenance">PSA / FMEA</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_schedule&module=maintenance">Maintenance Schedule</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_suggested&module=maintenance">Suggested Maintenance</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

    <div class="panel panel-default">
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table no-border-table">
            <thead>
                <tr>
                    <th width="7%">Department</th>
                    <th width="8%">Category</th>
                    <th width="8%">Description</th>
                    <th width="6%">Type</th>
                    <th width="25%">No</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Receiving</td>
                    <td>Moveable Asset</td>
                    <td>Forklift</td>
                    <td>Toyota</td>
                    <td>123</td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#addFmea-modal" data-toggle="modal" data-target="#addFmea-modal"><button type="button" id="addFmea" class="btn btn-success">Add FMEA</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">FMEA</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Failure Description</th>
                    <th>Component to Fail</th>
                    <th>Part to Fail</th>
                    <th>Effect on Asset</th>
                    <th>Effect on Process</th>
                    <th>Likelihood</th>
                    <th>Other Asset</th>
                    <th>Maintenance Plan Created</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td>No Air</td>
                    <td>Compressor Head</td>
                    <td>Fan Belt</td>
                    <td>Stop Operation</td>
                    <td>Prodction Stop</td>
                    <td>Definitely</td>
                    <td>All Compressors</td>
                    <td>No</td>
                    <td><a href="">Create Maintenance</a></td>
                    <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
    
    <!-- Add FMEA Modal -->
  <div class="modal fade" id="addFmea-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add FMEA</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addFmeaForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="failureDescription">Failure Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="failureDescription" id="failureDescription" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="componentToFail">Component to Fail</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="componentToFail" id="componentToFail" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="partToFail">Part to Fail</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="partToFail" id="partToFail" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="effectOnAsset">Effect on Asset</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select an effect on asset">
                        <option>Stop operation</option>
                        <option>Damage only</option>
                        <option>Danger to user</option>
                        <option>Danger to user and other surroundings</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="effectOnProcess">Effect on Process</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select an effect on process">
                        <option>No Effect</option>
                        <option>Discomfort</option>
                        <option>Production stop</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="likelihood">Likelihood</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a likelihood">
                        <option>Not Likely</option>
                        <option>Definitely</option>
                        <option>Soon</option>
                        <option>Expected</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="assetTypeGroups">Link to Other Asset type groups</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a group">
                        <option>Group 1</option>
                        <option>Group 2</option>
                        <option>Group 3</option>
                        <option>Group 4</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addCriticalAssetBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    