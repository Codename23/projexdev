<?php
$title = 'Import Assets';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?>  
<!--End of navigation--> 
<div class="col-lg-10">
    <!--sub menu-->
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_view&module=maintenance">Maintenance Plan</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_upcoming&module=maintenance">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_job&module=maintenance">Job</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_history&module=maintenance">History</a></li>
        <!--<li><a href="maintenance_psa.php">PSA / FMEA</a></li>
        <li><a href="maintenance_schedule.php">Maintenance Schedule</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_suggested&module=maintenance">Suggested Maintenance</a></li>
    </ul>
    </div>
</div>  
    
    <div class="panel panel-default">
    <div class="panel-heading">Search Resources</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>Wood</option>
                <option>I.T</option>
                <option>Finance</option>
                <option>HR</option>
            </select>
        </div>
        </div> 
            
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Category</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more categories" data-header="Close">
                <option>Fixed Assets</option>
                <option>Moveable Assets</option>
                <option>Raw Materials</option>
                <option>Final Products</option>
                <option>Consumables</option>
                <option>Emergency Equipment</option>
                <option>Motorized Equipment</option>
            </select> 
        </div>
        </div>
        
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Type</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                <option>Building</option>
                <option>Fixed Electrical Equipment</option>
                <option>Key is to be able to add on own behalf</option>
                <option>Forklift</option>
                <option>Wood</option>
                <option>Shutter</option>
                <option>Petrol</option>
                <option>Fire Extinguishers</option>
                <option>Forklift</option>
            </select> 
        </div>
        </div>    
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>
    <form method="POST" action="<?php echo BASE_URL;?>/index.php?action=importAssets&module=maintenance">
    <div class="panel panel-default">
        <div class="panel-body">
            <button type="submit" id="importAssets" name="importAssets" class="btn btn-success">Import selected assets</button>
        </div>
    </div>
    <div class="panel panel-default">
    <div class="panel-heading">Import Assets</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="resourceTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description Name</th>
                    <th>Number</th>                                        
                </tr>
            </thead>
            <tbody>
            <?php
                $count = 1; 
               // echo count($data['assets']);
               if($data['assets']): foreach($data['assets'] as $assetDetail):                     
                      echo '<tr>
                              <td><input type="checkbox" class="select-item checkbox" name="assetname[]" value="' . $assetDetail['id'] .'"></td>
                              <td>' . $assetDetail['department_name'] . '</td>
                              <td>' . $assetDetail['categoryType'] . '</td>
                              <td>' . $assetDetail['assetType'] . '</td>
                              <td>' . $assetDetail['description'] . '</td>
                              <td>' . $assetDetail['company_asset_number'] . '</td>           
                            </tr>';
               //<td><a data-toggle="tooltip" title="Edit asset" href="index.php?module=assets&action=editAsset&id=' . $assetDetail['id'] . '"><span class="glyphicon glyphicon-pencil"></span></td>
                              //<td><a class="trash" data-toggle="modal" data-target="#myModal" data-id="' . $assetDetail['id'] . '"><span class="glyphicon glyphicon-trash"></span></a></td>
                          $count ++;                         
                   endforeach;endif; 
            ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
   </form>     
    <!--End of the panel panel-default-->
<!-- Add Non-Conformance Modal -->
  <div class="modal fade" id="addNonConformance-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Non-Conformance</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addNonConformanceForm" action="resource.php">
            <div class="form-group">
                <label class="control-label col-sm-4" for="date">Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="date" id="date"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="dueDate">Due Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="dueDate" id="dueDate"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="group" class="form-control">
                    <option value selected disabled>Please select a group</option>
                    <option value="1">OHS (Health & Safety)</option>
                    <option value="2">E (Environment)</option>
                    <option value="3">Q (Quality)</option>
                    <option value="4">F (Food)</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="group" class="form-control">
                    <option value selected disabled>Please select a type</option>
                    <option value="1">Not Specified</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="department">Department</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments">
                        <option>I.T</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="nonConformanceDetails">Non-Conformance Details</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="nonConformanceDetails" id="nonConformanceDetails"></textarea>
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="recommendedImprovement">Recommended Improvement</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="recommendedImprovement" id="recommendedImprovement"></textarea>
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="resourceInvolved">Resource Involved</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a resource">
                        <option>Resource 1</option>
                        <option>Resource 2</option>
                        <option>Resource 3</option>
                    </select> 
                </div>
            </div>
            <span class="control-label col-sm-4"></span>
            <div class="col-lg-6 col-md-4 col-sm-8">
                <p style="color:red;"><b>Resource not listed</b></p>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notListedResourceType">Not Listed</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedResourceType" id="notListedResourceType" placeholder="Type"> 
                </div>
            </div>
            <div class="form-group">
                <span class="col-sm-4" for="notListedResourceDescription"></span>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedResourceDescription" id="notListedResourceDescription" placeholder="Description"> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="personInvolved">Person Involved</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                        <option>Nick Botha</option>
                        <option>Sunny Mathole</option>
                        <option>Brian Douglas</option>
                    </select> 
                </div>
            </div>
            <span class="control-label col-sm-4"></span>
            <div class="col-lg-6 col-md-4 col-sm-8">
                <p style="color:red;"><b>Person not listed</b></p>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notListedPersonType">Not Listed</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedPersonType" id="notListedPersonType" placeholder="Type"> 
                </div>
            </div>
            <div class="form-group">
                <span class="col-sm-4" for="notListedPersonDescription"></span>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedPersonDescription" id="notListedPersonDescription" placeholder="Description"> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="supplier">Supplier</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a supplier">
                        <option>Supplier 1</option>
                        <option>Supplier 2</option>
                        <option>Supplier 3</option>
                    </select> 
                </div>
            </div>
            <span class="control-label col-sm-4"></span>
            <div class="col-lg-6 col-md-4 col-sm-8">
                <p style="color:red;"><b>Supplier not listed</b></p>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notListedSupplierCompany">Not Listed</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedSupplierCompany" id="notListedSupplierCompany" placeholder="Company Name"> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="uploadPhoto">Take/Upload Photo</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="file" name="uploadPhoto" id="uploadPhoto"> 
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Send To Investigate</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigator">Investigator</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a investigator">
                        <option>Nick Botha</option>
                        <option>Sunny Mathole</option>
                        <option>Brian Douglas</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigatorDueDate">Due Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="investigatorDueDate" id="investigatorDueDate"> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addNonConformanceBtn" class="btn btn-success">Add Non-Conformance</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<!--<script src="js/jquery.tablecheckbox.js"></script>-->
<script>
$(document).ready(function()
{
         //button select all or cancel
        $(".chkbox").click(function () {
            var all = $("input.select-all")[0];
            all.checked = !all.checked
            var checked = all.checked;
            $("input.select-item").each(function (index,item) {
                item.checked = checked;
            });
        });

        //button select invert
        $("#select-invert").click(function () {
            $("input.select-item").each(function (index,item) {
                item.checked = !item.checked;
            });
            checkSelected();
        });

        //button get selected info
        $("#selected").click(function () {
            var items=[];
            $("input.select-item:checked:checked").each(function (index,item) {
                items[index] = item.value;
            });
            if (items.length < 1) {
                alert("no selected items!!!");
            }else {
                var values = items.join(',');
                console.log(values);
                var html = $("<div></div>");
                html.html("selected:"+values);
                html.appendTo("body");
            }
        });

        //column checkbox select all or cancel
        $("input.select-all").click(function () {
            var checked = this.checked;
            $("input.select-item").each(function (index,item) {
                item.checked = checked;
            });
        });

        //check selected items
        $("input.select-item").click(function () {
            var checked = this.checked;
            console.log(checked);
            checkSelected();
        });

        //check is all selected
        function checkSelected() {
            var all = $("input.select-all")[0];
            var total = $("input.select-item").length;
            var len = $("input.select-item:checked:checked").length;
            console.log("total:"+total);
            console.log("len:"+len);
            all.checked = len===total;
        }
});
</script>
<?php include_once('frontend/templates/footers/default_footer_tpl.php');  
    