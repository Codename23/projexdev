<?php 
/*
 * Header file
 */
$title = 'Maintenance Upcoming';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_view&module=maintenance">Maintenance Plan</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_upcoming&module=maintenance">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_job&module=maintenance">Job</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_history&module=maintenance">History</a></li>
        <!--<li><a href="maintenance_psa.php">PSA / FMEA</a></li>
        <li><a href="maintenance_schedule.php">Maintenance Schedule</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_suggested&module=maintenance">Suggested Maintenance</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

    <div class="panel panel-default">
    <div class="panel-heading">Search Maintenance</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>Wood</option>
                <option>I.T</option>
                <option>Finance</option>
                <option>HR</option>
            </select>
        </div>
        </div> 
 
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Resource Type</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                <option>Building</option>
                <option>Fixed Electrical Equipment</option>
                <option>Key is to be able to add on own behalf</option>
                <option>Forklift</option>
                <option>Wood</option>
                <option>Shutter</option>
                <option>Petrol</option>
                <option>Fire Extinguishers</option>
                <option>Forklift</option>
            </select> 
        </div>
        </div>   
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#createJob-modal" data-toggle="modal" data-target="#createJob-modal"><button type="button" id="createJob" class="btn btn-success">Create Job</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Upcoming</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="maintenanceUpcomingTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Preventative Date</th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Number</th>
                    <th>Internal / External</th>
                    <th>Service Type</th>
                    <th>Description</th>
                    <th>Maintenance Plan</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>25/04/2017</td>
                    <td>Wood</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Plainer</td>
                    <td>Wood Plainer</td>
                    <td>1234</td>
                    <td>Internal</td>
                    <td>Replace</td>
                    <td>Replace all gears</td>
                    <td><a href="">View</a></td>
                    <td><a href="">View Asset</a></td>
                    <td><a href="#createJob-modal" data-toggle="modal" data-target="#createJob-modal">Create Job</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->

<!-- Create Job Modal -->
  <div class="modal fade" id="createJob-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Job</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="createJobInternalForm" action="">
            <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Job Details</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="description">Description</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="description" id="description" required placeholder=""> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="scheduledStartDateTime">Scheduled Start Date & Time</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="scheduledStartDateTime" id="scheduledStartDateTime" required placeholder=""> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="endDateTime">End Date & Time</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="endDateTime" id="endDateTime" required placeholder=""> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="responsiblePerson">Responsible Person</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people" data-header="Close">
                                            <option value="1">Nick Botha</option>
                                            <option value="2">Sunny Mathole</option>
                                            <option value="3">Brian Douglas</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="departmentInvolved">Department Involved</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                                            <option value="1">IT</option>
                                            <option value="2">Finance</option>
                                            <option value="3">HR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Job Description</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="maintenanceType">Maintenance Type</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                                            <option value="1">Service</option>
                                            <option value="2">Maintenance</option>
                                            <option value="3">Replacement</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="fullJobDetails">Job Details</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <textarea rows="5" class="form-control" name="fullJobDetails" id="fullJobDetails" required placeholder=""></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Risk Factor</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="riskLevel">Risk Level</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a level">
                                            <option value="1">Low</option>
                                            <option value="2">Medium</option>
                                            <option value="3">High</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="hazardTypes">Hazard Types</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more types" data-header="Close">
                                            <option>Working at heights</option>
                                            <option>Electricity</option>
                                            <option>Machines</option>
                                            <option>Hot work</option>
                                            <option>Noise</option>
                                            <option>Fumes</option>
                                            <option>Vessels under pressure</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Approval</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="sheqfRequest">SHEQF</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <a href=""><button type="button" id="" class="btn btn-success">Request</button></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="financialRequest">Financial</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <a href=""><button type="button" id="" class="btn btn-success">Request</button></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="managerialRequest">Managerial</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <a href=""><button type="button" id="" class="btn btn-success">Request</button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Supplier</a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="companyName">Company Name</label>
                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a company">
                                            <option value="1">Company 1</option>
                                            <option value="2">Company 2</option>
                                            <option value="3">Company 3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Resource / Tools to be used in the maintenance</a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="nonListedDescription">Non Listed Description</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="nonListedDescription" id="nonListedDescription" required placeholder=""> 
                                        </div>
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8">
                                    <a href="#importFromAssets-modal" data-toggle="modal" data-target="#importFromAssets-modal"><button type="button" id="" class="btn btn-success">Add</button></a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table width="100%" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Category</th>
                                                <th>Type</th>
                                                <th>Description</th>
                                                <th>No</th>
                                                <th>Latest Non-Conformance</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Moveable Asset</td>
                                                <td>Ladder</td>
                                                <td>Aluminium Ladder</td>
                                                <td>12</td>
                                                <td><a href="">View</a></td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Parts</a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="partsNotes">Parts Notes</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="partsNotes" id="partsNotes" required placeholder=""> 
                                        </div>
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8">
                                    <a href="#addComponent-modal" data-toggle="modal" data-target="#addComponent-modal"><button type="button" id="addComponent" class="btn btn-success">Add Part</button></a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table width="100%" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Component</th>
                                                <th>Part</th>
                                                <th>Sub-Part</th>
                                                <th>Part Description</th>
                                                <th>Part Size</th>
                                                <th>Quantity</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Engine</td>
                                                <td>Pistons</td>
                                                <td></td>
                                                <td>Blue Piston</td>
                                                <td>15' x 13'</td>
                                                <td>4</td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>Oil</td>
                                                <td>Castrol Oil</td>
                                                <td>5 Litres</td>
                                                <td>1</td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>Seals</td>
                                                <td>Green Seal</td>
                                                <td>50 x 3</td>
                                                <td>1</td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>Head Cover</td>
                                                <td></td>
                                                <td>Steel Cover</td>
                                                <td>Generic</td>
                                                <td>1</td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8">
                    <button type="submit" id="createJobBtn" class="btn btn-success">Create Job</button>
                    </div>
                </div>
                </form>
                <!--End of panel-default-->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Import From Assets Modal -->
  <div class="modal fade" id="importFromAssets-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Import From Assets</h4>
        </div>
        <div class="modal-body table-responsive">
            <form class="form-horizontal" method="post" name="searchAssets" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="searchDepartment">Department</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                        <option>Warehouse</option>
                        <option>Receiving</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="searchType">Type</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more types" data-header="Close">
                        <option>Compressor</option>
                        <option>Forklift</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="button" id="searchAssetsBtn" class="btn btn-success">View</button>
                    </div>
                </div>
            </form>
            <table width="100%" class="table table-hover" id="importFromAssetsTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Number</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
            </tbody>
        </table>
        <div class="form-group">
            <div class="col-sm-8">
            <button type="button" id="importFromAssetsBtn" class="btn btn-success">Add</button>
            </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Add Component Modal -->
  <div class="modal fade" id="addComponent-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Component / Part</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addComponentForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="selectComponent">Select Component</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select component">
                        <option>Tires</option>
                        <option>Engine</option>
                        <option>Gear Box</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="newComponent">New Component</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <a href="#newComponent-modal" data-toggle="modal" data-target="#newComponent-modal"><button type="button" id="newComponent" class="btn btn-success">Add New</button></a>
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>New Component / Part</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="selectPart">Select Part</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select part">
                        <option>Pistons</option>
                        <option>Head Cover</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="newPart">New Part</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <a href="#newPart-modal" data-toggle="modal" data-target="#newPart-modal"><button type="button" id="newPart" class="btn btn-success">Add New</button></a>
                </div>
            </div><br/>
            
            <div class="form-group">
                <label class="control-label col-sm-4" for="partType">Part Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="partType" id="partType" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="partDescription">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="partDescription" id="partDescription" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="size">Size</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="size" id="size" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="quantity">Quantity</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="quantity" id="quantity" required placeholder=""> 
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Sub Parts</h5></span></div>
            <div class="form-group">
                <div class="col-lg-6 col-md-4 col-sm-8">
                <a href="#addSubPart-modal" data-toggle="modal" data-target="#addSubPart-modal"><button type="button" id="addSubPart" class="btn btn-success">Add Sub Part</button></a>
                </div>
            </div>
            
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Sub Part Type</th>
                            <th>Description</th>
                            <th>Size</th>
                            <th>Quantity</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <div class="col-sm-8">
                <button type="submit" id="addComponentBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- New Component Modal -->
  <div class="modal fade" id="newComponent-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Component</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="newComponentForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="newComponentDescription">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="newComponentDescription" id="newComponentDescription" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="button" id="newComponentBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- New Part Modal -->
  <div class="modal fade" id="newPart-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Part</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="newPartForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="newPartType">Part Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="newPartType" id="newPartType" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="newPartDescription">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="newPartDescription" id="newPartDescription" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="newSize">Size</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="newSize" id="newSize" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="newQuantity">Quantity</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="newQuantity" id="newQuantity" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="button" id="newPartBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Add Sub Part Modal -->
  <div class="modal fade" id="addSubPart-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Sub Part</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addSubPartForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="subPartType">Sub Part Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="subPartType" id="subPartType" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="subPartDescription">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="subPartDescription" id="subPartDescription" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="subPartSize">Size</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="subPartSize" id="subPartSize" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="subParQuantity">Quantity</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="subParQuantity" id="subParQuantity" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="button" id="addSubPartBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
    
</div>
</div><!--End of container-fluid-->
<script src="js/jquery.tablecheckbox.js"></script>
<script>
$(function(){
    $("#scheduledStartDateTime").datetimepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt', 
        maxDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datetimepicker();
    
    $("#endDateTime").datetimepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt', 
        maxDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datetimepicker();
});

$('#maintenanceUpcomingTbl, #importFromAssetsTbl').tablecheckbox();
var chkbox = document.getElementsByClassName("chkbox");

$("#importFromAssetsBtn").click(function(){
    $("#importFromAssets-modal").modal('toggle');
});

$("#newComponentBtn").click(function(){
    $("#newComponent-modal").modal('toggle');
});

$("#newPartBtn").click(function(){
    $("#newPart-modal").modal('toggle');
});

$("#addSubPartBtn").click(function(){
    $("#addSubPart-modal").modal('toggle');
});
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    