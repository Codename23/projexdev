<?php 
/*
 * Header file
 */
$title = 'Active Approval Process';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
<li ><a href="<?php echo BASE_URL;?>/index.php?action=view_requested_maintenance&module=maintenance">Requests</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_jobs&module=maintenance">Job</a></li>
        <!--<li><a href="active_upcoming.php">Upcoming</a></li>
        <li><a href="active_approval_process.php">Approval Process</a></li>
        <li><a href="active_scheduled.php">Scheduled</a></li>
        <li><a href="active_maintenance_active.php">Active</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_active_history&module=maintenance">History</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

<div class="panel panel-default">
    <div class="panel-heading">Approval Process</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Job No</th>
                    <th>Priority</th>
                    <th>Starting Date</th>
                    <th>Description</th>
                    <th>Departments</th>
                    <th>Risk Level</th>
                    <th>Hazard Types</th>
                    <th>Approval Status</th>
                    <th>PO No</th>
                    <th>Responsible Person</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                 <?php
               if(!empty($data['allRequestedJobs'])){foreach($data['allRequestedJobs'] as $nc)
               {
                  echo "<tr>
                                        <td>{$nc['id']}</td>
                                        <td>{$nc['priority']}</td>
                                        <td>{$nc['start_date']}</td>
                                        <td>{$nc['description']}</td>
                                        <td>{$nc['department_involved']}</td>
                                        <td>{$nc['risk_level']}</td>
                                        <td>{$nc['hazard_types']}</td>
                                        <td>{$nc['approval_required']}</td>
                                        <td>1</td>
                                       <td>{$nc['responsible_persons']}</td>                                             
                                       <td><a href='#schedule-modal' data-toggle='modal' data-target='#schedule-modal'>Schedule</a></td>
                                        <td><a href='#-modal' data-toggle='modal' data-target='#-modal'>Reschedule</a></td>
                                        <td><a href='#signoff-modal' data-toggle='modal' data-target='#signoff-modal'>Sign Off</a></td>
               <td><a href=''>View Job</a></td></tr>";   }
               }
               ?>
                
                
            </tbody>
        </table>    
    </div>
    </div>
    </div>

<!-- Schedule Modal -->
  <div class="modal fade" id="schedule-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Job Details</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="scheduleForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="startingDateTime">Starting Date & Time</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="startingDateTime" id="startingDateTime" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="endDate">End Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="endDate" id="endDate" required placeholder=""> 
                </div>
            </div><br/>
            
            <div class="modal-custom-h5"><span><h5>Notifications - Once Scheduled</h5></span></div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="department">Department</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                    <option value="1">IT</option>
                    <option value="2">Finance</option>
                    <option value="3">HR</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="manager">Manager</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a manager">
                    <option value="1">Nick Botha</option>
                    <option value="2">Sunny Mathole</option>
                    <option value="3">Brian Douglas</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="sheqfManager">SHEQF Manager</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a sheqf manager">
                    <option value="1">Nick Botha</option>
                    <option value="2">Sunny Mathole</option>
                    <option value="3">Brian Douglas</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="otherPeople">Other People</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people" data-header="Close">
                    <option value="1">Nick Botha</option>
                    <option value="2">Sunny Mathole</option>
                    <option value="3">Brian Douglas</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="scheduleBtn" class="btn btn-success">Schedule</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Sign Off Modal -->
  <div class="modal fade" id="signoff-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sign Off</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
            <table width="100%" class="table table-hover">
                <thead>
                    <tr>
                        <th colspan="2">Job Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="30%">Maintenance Type</td>
                        <td>Service</td>
                    </tr>
                    <tr>
                        <td>Full Job Description</td>
                        <td>This is a description</td>
                    </tr>
                </tbody>
            </table>
            </div>
            
            <form class="form-horizontal" method="post" name="serviceSignOffForm" action="">
            <div class="modal-custom-h5"><span><h5>Asset That Was Maintained</h5></span></div>
            <div class="form-group">
                <div class="col-sm-8">
                <a href="#importFromAssets-modal" data-toggle="modal" data-target="#importFromAssets-modal"><button type="button" id="signoffAddAsset" class="btn btn-success">Add Asset</button></a>
                </div>
            </div>
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Department</th>
                            <th>Category</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>No</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Receiving</td>
                            <td>Moveable Asset</td>
                            <td>Forklift</td>
                            <td>10 Ton Diesel</td>
                            <td>12</td>
                            <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>
                    </tbody>
                </table>    
            </div>
            
            <div class="modal-custom-h5"><span><h5>Cause of Breakdown & Part to be replaced</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                    <option value="1">Operator no training</option>
                    <option value="2">No preventative training</option>
                    <option value="3">Misuse</option>
                    <option value="4">Age</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="description">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="description" id="description" required></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="downTimeInHours">Down Time In Hours</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="downTimeInHours" id="downTimeInHours">
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="partToBeReplaced">Part To Be Replaced</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="partToBeReplaced" id="partToBeReplaced">
                </div>
            </div> 
            
            <div class="modal-custom-h5"><span><h5>Supplier</h5></span></div>
            <div class="form-group">
                <div class="col-sm-8">
                <a href="#addSupplier-modal" data-toggle="modal" data-target="#addSupplier-modal"><button type="button" id="addSupplier" class="btn btn-success">Add Supplier</button></a>
                </div>
            </div>
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Supplier</th>
                            <th>Service</th>
                            <th>Rating</th>
                            <th>Comments</th>
                            <th>Cost</th>
                            <th>Evidence</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>James Painters</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>R 1200</td>
                            <td></td>
                            <td><a href="#complete-modal" data-toggle="modal" data-target="#complete-modal">Complete</a></td>
                            <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        </tr>
                    </tbody>
                </table>    
            </div><br/>
            <div class="form-group">
                <div class="col-sm-8">
                <button type="submit" id="serviceSignOffBtn" class="btn btn-success">Sign Off</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Add Supplier Modal -->
  <div class="modal fade" id="addSupplier-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Complete Supplier Details</h4>
        </div>
        <div class="modal-body table-responsive">
            <form class="form-horizontal" method="post" name="addSupplierForm" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="signoffSupplierName">Supplier Name</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a supplier">
                        <option value="1">Nick Botha</option>
                        <option value="2">Sunny Mathole</option>
                        <option value="3">Brian Douglas</option>
                        </select>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-sm-4" for="signoffRate">Rate Service Provider</label>
                    <div class="col-lg-6 col-md-4 col-sm-8 star-rating">
                        <input type="hidden" class="rating"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="noteOfServiceProvider">Note of Service Provider</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea class="form-control" rows="5" name="ServiceTypeDescription" id="ServiceTypeDescription" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="supportingDocs">Upload Supporting Documents</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="file" class="supportingDocs"/>
                    </div>
                </div>   
                <div class="form-group">
                    <label class="control-label col-sm-4" for="costOfService">Cost of Service</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="costOfService" id="costOfService" required value="R">
                    </div>
                </div>
            </form>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
            <button type="button" id="addSupplierBtn" class="btn btn-success">Submit</button>
            </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Import From Assets Modal -->
  <div class="modal fade" id="importFromAssets-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Import From Assets</h4>
        </div>
        <div class="modal-body table-responsive">
            <form class="form-horizontal" method="post" name="searchAssets" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="searchDepartment">Department</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                        <option>Warehouse</option>
                        <option>Receiving</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="searchType">Type</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more types" data-header="Close">
                        <option>Compressor</option>
                        <option>Forklift</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="button" id="searchAssetsBtn" class="btn btn-success">View</button>
                    </div>
                </div>
            </form>
            <table width="100%" class="table table-hover" id="importFromAssetsTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Number</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
            </tbody>
        </table>
        <div class="form-group">
            <div class="col-sm-8">
            <button type="button" id="importFromAssetsBtn" class="btn btn-success">Add</button>
            </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<script src="js/bootstrap-rating.js"></script>
<script src="js/jquery.tablecheckbox.js"></script>
<script>
$(function() {
    $("#startingDateTime").datetimepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt', 
        maxDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datetimepicker();
    
    $("#endDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    
    $('.rating').each(function () {
          $('<span class="label label-success"></span>')
            .text($(this).val() || ' ')
            .insertAfter(this);
        });
        $('.rating').on('change', function () {
          $(this).next('.label').text($(this).val());
        });
});

$('#importFromAssetsTbl').tablecheckbox();
var chkbox = document.getElementsByClassName("chkbox");

$("#importFromAssetsBtn").click(function(){
    $("#importFromAssets-modal").modal('toggle');
});
$("#addSupplierBtn").click(function(){
    $("#addSupplier-modal").modal('toggle');
});
</script>

<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    