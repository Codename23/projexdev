<?php 
/*
 * Header file
 */
$title = 'Maintenance';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_view&module=maintenance">Maintenance Plan</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_upcoming&module=maintenance">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_job&module=maintenance">Job</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_history&module=maintenance">History</a></li>
        <!--<li><a href="maintenance_psa.php">PSA / FMEA</a></li>
        <li><a href="maintenance_schedule.php">Maintenance Schedule</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_suggested&module=maintenance">Suggested Maintenance</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

    <div class="panel panel-default">
    <div class="panel-heading">Search Maintenance</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>Wood</option>
                <option>I.T</option>
                <option>Finance</option>
                <option>HR</option>
            </select>
        </div>
        </div> 
 
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Resource Type</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                <option>Building</option>
                <option>Fixed Electrical Equipment</option>
                <option>Key is to be able to add on own behalf</option>
                <option>Forklift</option>
                <option>Wood</option>
                <option>Shutter</option>
                <option>Petrol</option>
                <option>Fire Extinguishers</option>
                <option>Forklift</option>
            </select> 
        </div>
        </div>   
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <!--<div class="panel panel-default">
    <div class="panel-body">
        <a href="#resourceBreakdown-modal" data-toggle="modal" data-target="#resourceBreakdown-modal"><button type="button" id="resourceBreakdown" class="btn btn-success">Resource Breakdown</button></a><br/><br/>
        <a href="#addAsset-modal" data-toggle="modal" data-target="#addAsset-modal"><button type="button" id="addAsset" class="btn btn-success">Add Asset to Maintenance Table</button></a>
    </div>
    </div>-->

    <div class="panel panel-default">
    <div class="panel-body">
        <a href="<?php echo BASE_URL;?>/index.php?action=viewAssetMaintenance&module=maintenance" ><button type="button" id="importFromAssets" class="btn btn-success">Import From Assets</button></a>
        <a href="#addMaintenancePlan-modal" data-toggle="modal" data-target="#addMaintenancePlan-modal"><button type="button" id="addMaintenancePlan" class="btn btn-success">Add Maintenance Plan (Multi)</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Critical Assets</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="criticalAssetTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Number</th>
                    <th>View FMEA</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
               <?php
               
                  if(!empty($data['active_assets'])){
                                foreach($data['active_assets'] as $assetDetail){
                                $url = BASE_URL."/index.php?edit={$assetDetail["record_id"]}&action=statutory_plan&module=assets";
                                echo "<tr>
                                        <td>
                                         <input type='checkbox' class='checkbox' name='cbxAsset[]'/></td>
                                        <td>{$assetDetail['department_name']}</td>
                                        <td>{$assetDetail['categoryType']}</td>
                                        <td>{$assetDetail['assetType']}</td>
                                        <td>{$assetDetail['description']}</td>
                                        <td>{$assetDetail['company_asset_number']}</td>
                                        <td><a href='{$url}'>Statutory Plan</a></td>
                                       <td><a href='#'>View Resource</a></td>
                                        <td></td>
                                        <td></td>";                               
                                }
                            }
                  ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->

<!-- Add Critical Asset Modal -->
  <div class="modal fade" id="addCriticalAsset-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Critical Asset</h4>
        </div>
        <div class="modal-body table-responsive">
            <form class="form-horizontal" method="post" name="searchAssets" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="searchDepartment">Department</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                        <option>Warehouse</option>
                        <option>Receiving</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="searchType">Type</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more types" data-header="Close">
                        <option>Compressor</option>
                        <option>Forklift</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="button" id="searchAssetsBtn" class="btn btn-success">View</button>
                    </div>
                </div>
            </form>
            <table width="100%" class="table table-hover" id="assetsTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Number</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
            </tbody>
        </table>
        <div class="form-group">
            <div class="col-sm-8">
            <button type="button" id="addCriticalAssetBtn" class="btn btn-success">Add</button>
            </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Add Maintenance Plan Modal -->
  <div class="modal fade" id="addMaintenancePlan-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Maintenance Plan</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addMaintenancePlanForm" action="maintenance_plan_create.php">
            <div class="form-group">
                <label class="control-label col-sm-4" for="maintenanceDescription">Maintenance Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" name="maintenanceDescription" id="maintenanceDescription" required placeholder=""></textarea> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="maintenanceType">Maintenance Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                    <option value="1">Type 1</option>
                    <option value="2">Type 2</option>
                    <option value="3">Type 3</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="linkFmea">Link FMEA</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a link">
                    <option value="1">Link 1</option>
                    <option value="2">Link 2</option>
                    <option value="3">Link 3</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addMaintenancePlanBtn" class="btn btn-success">Add (Go to Create Page)</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
    
</div>
</div><!--End of container-fluid-->
<script>
$('#criticalAssetTbl, #assetsTbl').tablecheckbox();
var chkbox = document.getElementsByClassName("chkbox");
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');  
?>  
    