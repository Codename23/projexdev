<?php 
/*
 * Header file
 */
$title = 'Active History';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=view_requested_maintenance&module=maintenance">Requests</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_jobs&module=maintenance">Job</a></li>
        <!--<li><a href="active_upcoming.php">Upcoming</a></li>
        <li><a href="active_approval_process.php">Approval Process</a></li>
        <li><a href="active_scheduled.php">Scheduled</a></li>
        <li><a href="active_maintenance_active.php">Active</a></li>-->
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_active_history&module=maintenance">History</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

<div class="panel panel-default">
    <div class="panel-heading">History</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Job No</th>
                    <th>Starting Date</th>
                    <th>End Date</th>
                    <th>Internal/External</th>
                    <th>Description</th>
                    <th>Department</th>
                    <th>Responsible Person</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>012</td>
                    <td>2017/04/17</td>
                    <td>2017/04/20</td>
                    <td>Internal</td>
                    <td>Install new window</td>
                    <td>I.T</td>
                    <td>Nick Botha</td>
                    <td><a href="active_history_view_report.php">View Report</a></td>
                </tr>
            </tbody>
        </table>    
    </div>
    </div>
    </div>

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    