<?php 
/*
 * Header file
 */
$title = 'Maintenance Schedule';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_view&module=maintenance">Dashboard</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_psa&module=maintenance">PSA / FMEA</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_schedule&module=maintenance">Maintenance Schedule</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_suggested&module=maintenance">Suggested Maintenance</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

    <div class="panel panel-default">
    <div class="panel-heading">Search Maintenance Schedule</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Month</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more months"data-header="Close">
                <option>January</option>
                <option>February</option>
                <option>March</option>
                <option>March</option>
                <option>April</option>
                <option>May</option>
                <option>June</option>
                <option>July</option>
                <option>August</option>
                <option>September</option>
                <option>October</option>
                <option>November</option>
                <option>December</option>
            </select>
        </div>
        </div>
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Resource Type</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more resource types"data-header="Close">
                <option>Type 1</option>
                <option>Type 2</option>
                <option>Type 3</option>
                <option>Type 4</option>
            </select>
        </div>
        </div>
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#addMaintenancePlan-modal" data-toggle="modal" data-target="#addMaintenancePlan-modal"><button type="button" id="addMaintenancePlan" class="btn btn-success">Add Maintenance Plan</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Maintenance Schedule</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Month</th>
                    <th>Preventative</th>
                    <th>Frequency</th>
                    <th>Responsible Person</th>
                    <th>Est Duration</th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>No</th>
                    <th>Maintenance Description</th>
                    <th>Type</th>
                    <th>Component Link</th>
                    <th>Parts</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>June</td>
                    <td>15/06/2017</td>
                    <td>6 Months</td>
                    <td>Nick</td>
                    <td>1 Day</td>
                    <td>Receiving</td>
                    <td>Moveable Asset</td>
                    <td>Forklift</td>
                    <td>Toyota</td>
                    <td>123</td>
                    <td>Replace Forklift Tires</td>
                    <td>Replace</td>
                    <td>Tires</td>
                    <td><a href="#viewParts-modal" data-toggle="modal" data-target="#viewParts-modal">View</a></td>
                    <td><a href="">View Plan</a></td>
                    <td><a href="">Print Plan</a></td>
                    <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
    
    <!-- View Modal -->
  <div class="modal fade" id="viewParts-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">View Parts</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table width="100%" class="table table-hover" id="upcomingTbl">
                    <thead>
                        <tr>
                            <th>Due Month</th>
                            <th>Component</th>
                            <th>Part</th>
                            <th>Part Description</th>
                            <th>Part Size</th>
                            <th>Quantity</th>
                            <th>Frequency</th>
                            <th>Service Due Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>March</td>
                            <td>Tires</td>
                            <td>Tires</td>
                            <td>Forklift tires</td>
                            <td>15' x 13'</td>
                            <td>4</td>
                            <td>Annually</td>
                            <td>2017/03/15</td>
                        </tr>
                        <tr>
                            <td>March</td>
                            <td>Engine</td>
                            <td>Oil</td>
                            <td>Castrol Oil</td>
                            <td>5 litres</td>
                            <td>1</td>
                            <td>Monthly</td>
                            <td>2017/03/15</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    