<?php 
/*
 * Header file
 */
$title = 'Maintenance PSA / FMEA';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_view&module=maintenance">Dashboard</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_psa&module=maintenance">PSA / FMEA</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_schedule&module=maintenance">Maintenance Schedule</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_suggested&module=maintenance">Suggested Maintenance</a></li>
    </ul>
    </div>
</div> 
<!--End of sub menu-->

    <div class="panel panel-default">
    <div class="panel-heading">Search PSA / FMEA</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Select Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>I.T</option>
                <option>Human Resources</option>
                <option>Marketing</option>
                <option>Support and Services</option>
            </select>
        </div>
        </div>
        </div>    
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#addCriticalAsset-modal" data-toggle="modal" data-target="#addCriticalAsset-modal"><button type="button" id="addCriticalAsset" class="btn btn-success">Add Critical Asset</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Process Stoppers Assessment</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>No</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Receiving</td>
                    <td>Moveable Asset</td>
                    <td>Forklift</td>
                    <td>Toyota</td>
                    <td>1234</td>
                    <td><a href="<?php echo BASE_URL;?>/index.php?action=maintenance_fmea&module=maintenance">FMEA</a></td>
                    <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
    
    <!-- Add Critical Asset Modal -->
  <div class="modal fade" id="addCriticalAsset-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Critical Asset</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addCriticalAssetForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="department">Department</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                        <option>I.T</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="searchAsset">Search Asset</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select an asset">
                        <option>Asset 1</option>
                        <option>Asset 2</option>
                        <option>Asset 3</option>
                        <option>Asset 4</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addCriticalAssetBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    