<?php 
/*
 * Header file
 */
$title = 'Active';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=view_requested_maintenance&module=maintenance">Requests</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_jobs&module=maintenance">Job</a></li>
        <!--<li><a href="active_upcoming.php">Upcoming</a></li>
        <li><a href="active_approval_process.php">Approval Process</a></li>
        <li><a href="active_scheduled.php">Scheduled</a></li>
        <li><a href="active_maintenance_active.php">Active</a></li>-->
       <li><a href="<?php echo BASE_URL;?>/index.php?action=view_maintenance_active_history&module=maintenance">History</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

<div class="panel panel-default">
    <div class="panel-heading">Requests</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Priority</th>
                    <th>Suggested Due Date</th>
                    <th>Department</th>
                    <th>Requested Person</th>
                    <th>Maintenance Description</th>
                    <th>Notes</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                   <?php
               if($data['allRequestedMaintenance']): foreach($data['allRequestedMaintenance'] as $nc):
                   ?>
                            <tr>
                                <td><?php echo $nc['id']; ?></td>
                                <td><?php echo $nc['priority']; ?></td>                                
                                <td><?php echo $nc['due_date']; ?></td>
                                <td><?php echo $nc['department']; ?></td>
                                <td><?php echo $nc['requested_by']; ?></td>
                                <td><?php echo $nc['description']; ?></td>
                                <td><?php echo $nc['notes']; ?></td>
                                <td><!--<a href="#-modal" data-toggle="modal" data-target="#-modal">View Request</a>--></td>
                                <td><a href="#CreateJob" class="create_job_link" onclick="$('#upcoming').val($(this).attr('link_id'));" link_id="<?php echo $nc["id"];?>" data-toggle="modal" data-target="#createJob-modal">Create Job</a></td>
                                 <td><a href="#signoff-modal" data-toggle="modal" data-target="#signoff-modal">Sign Off</a></td>
                            </tr>
                 <?php endforeach;endif; ?>
            </tbody>
        </table>    
    </div>
    </div>
    </div>

<!-- Create Job Modal -->
  <div class="modal fade" id="createJob-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Job</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="createJobInternalForm" action="<?php echo BASE_URL;?>/index.php?module=maintenance&action=create_maintenance_job">
                <input type="hidden" name="upcoming[]" id="upcoming"/>
            <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Job Details</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="description">Description</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="description" id="description" required placeholder=""> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="scheduledStartDateTime">Scheduled Start Date & Time</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="scheduledStartDateTime" id="scheduledStartDateTime" required placeholder=""> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="endDateTime">End Date & Time</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="endDateTime" id="endDateTime" required placeholder=""> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                         <input type="hidden" name="person_responsible_selected" id="person_responsible_selected"/>
                                        <label class="control-label col-sm-4" for="responsiblePerson">Responsible Person</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people" data-header="Close">
                                                      <?php
                            foreach($data['allEmployees'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>  
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="departments_selected" id="departments_selected"/>
                                        <label class="control-label col-sm-4" for="departmentInvolved">Department Involved</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                                                              <?php
                                                   foreach($data['allDepartments'] as $group)
                                                   {
                                                       echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                                                   }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Job Description</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="maintenanceType">Maintenance Type</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" name="maintenanceType" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                                            <option value="1">Service</option>
                                            <option value="2">Maintenance</option>
                                            <option value="3">Replacement</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="fullJobDescription">Job Details</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <textarea rows="5" class="form-control" name="fullJobDescription" id="fullJobDetails" required placeholder=""></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Risk Factor</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="riskLevel">Risk Level</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" name="risk_level" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a level">
                                            <option value="1">Low</option>
                                            <option value="2">Medium</option>
                                            <option value="3">High</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="hazardTypes">Hazard Types</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" name="hazardTypes" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more types" data-header="Close">
                                            <option>Working at heights</option>
                                            <option>Electricity</option>
                                            <option>Machines</option>
                                            <option>Hot work</option>
                                            <option>Noise</option>
                                            <option>Fumes</option>
                                            <option>Vessels under pressure</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Approval</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <input type="hidden" class="form-control" name="approval_required" id="approval_required" placeholder="">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="sheqfRequest">SHEQF</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <a href=""><button type="button" id="" class="btn btn-success">Request</button></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="financialRequest">Financial</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <a href=""><button type="button" id="" class="btn btn-success">Request</button></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="managerialRequest">Managerial</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <a href=""><button type="button" id="" class="btn btn-success">Request</button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Resource to be maintained</a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="nonListedResource">Non Listed Resource Description</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="nonListedResource" id="nonListedResource" required placeholder=""> 
                                        </div>
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8">
                                     <a href="#importFromAssets-modal" data-toggle="modal" data-target="#importFromAssets-modal"><button type="button" id="importFromAssets" class="btn btn-success">Add Resource To Table</button></a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table width="100%" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Department</th>
                                                <th>Category</th>
                                                <th>Type</th>
                                                <th>Description</th>
                                                <th>No</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Receiving</td>
                                                <td>Moveable Asset</td>
                                                <td>Forklift</td>
                                                <td>10 Ton Diesel</td>
                                                <td>12</td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Supplier</a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="companyName">Company Name</label>
                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" name="companyName" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a company">
                                                    <?php
                            foreach($data['allSupplier'] as $group)
                            {
                                echo "<option value='{$group["id"]}'>{$group['supplier_name']}</option>";
                            }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Resource / Tools to be used in the maintenance</a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="resources_used" id="resources_used" placeholder=""> 
                                        <label class="control-label col-sm-4" for="nonListedDescription">Non Listed Description</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="nonListedDescription" id="nonListedDescription" required placeholder=""> 
                                        </div>
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8">
                                     <a href="#importFromAssets-modal" data-toggle="modal" data-target="#importFromAssets-modal"><button type="button" id="add" class="btn btn-success">Add</button></a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table width="100%" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Category</th>
                                                <th>Type</th>
                                                <th>Description</th>
                                                <th>No</th>
                                                <th>Latest Non-Conformance</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Moveable Asset</td>
                                                <td>Ladder</td>
                                                <td>Aluminium Ladder</td>
                                                <td>12</td>
                                                <td><a href="">View</a></td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">Parts</a>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="parts_used" id="parts_used" placeholder="">
                                        <label class="control-label col-sm-4" for="partsNotes">Parts Notes</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="partsNotes" id="partsNotes" required placeholder=""> 
                                        </div>
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8">
                                    <a href="#addComponent-modal" data-toggle="modal" data-target="#addComponent-modal"><button type="button" id="addComponent" class="btn btn-success">Add Part</button></a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table width="100%" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Component</th>
                                                <th>Part</th>
                                                <th>Sub-Part</th>
                                                <th>Part Description</th>
                                                <th>Part Size</th>
                                                <th>Quantity</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Engine</td>
                                                <td>Pistons</td>
                                                <td></td>
                                                <td>Blue Piston</td>
                                                <td>15' x 13'</td>
                                                <td>4</td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>Oil</td>
                                                <td>Castrol Oil</td>
                                                <td>5 Litres</td>
                                                <td>1</td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>Seals</td>
                                                <td>Green Seal</td>
                                                <td>50 x 3</td>
                                                <td>1</td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>Head Cover</td>
                                                <td></td>
                                                <td>Steel Cover</td>
                                                <td>Generic</td>
                                                <td>1</td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8">
                    <button type="submit" id="createJobBtn" name="createJobBtn" class="btn btn-success">Create Job</button>
                    </div>
                </div>
                </form>
                <!--End of panel-default-->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->


<!-- Add Request Modal -->
<!--  <div class="modal fade" id="addRequest-modal" role="dialog">
    <div class="modal-dialog">
         Modal content
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Request</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addRequestForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="priority">Priority</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a priority">
                    <option value="1">Low</option>
                    <option value="2">Medium</option>
                    <option value="3">High</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="suggestedDueDate">Suggested Due Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="suggestedDueDate" id="suggestedDueDate" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="department">Department</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                    <option value="1">IT</option>
                    <option value="2">Finance</option>
                    <option value="3">HR</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="requestedPerson">Requested Person</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                    <option value="1">Nick Botha</option>
                    <option value="2">Sunny Mathole</option>
                    <option value="3">Brian Douglas</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="maintenanceDesription">Maintenance Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" name="maintenanceDesription" id="maintenanceDesription" required placeholder=""></textarea> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="nonConformance">Non-Conformance</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" name="nonConformance" id="nonConformance" required placeholder=""></textarea> 
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Resource Involved</h5></span></div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="resourceInvolved">Resource Involved</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                <option value="1">IT</option>
                <option value="2">Finance</option>
                <option value="3">HR</option>
                </select>
                </div>
            </div>
            <div class="form-group">
                <span class="control-label col-sm-4" for="description"></span>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a description">
                <option value="1">Description 1</option>
                <option value="2">Description 2</option>
                <option value="3">Description 3</option>
                </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="nonListedResource">Non-Listed Resource</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="nonListedResource" class="form-control" name="nonListedResource" id="email" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addRequestBtn" class="btn btn-success">Send Request</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>-->
<!--End Of Modal -->

<!-- Sign Off Modal -->
  <div class="modal fade" id="signoff-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sign Off</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="serviceSignOffForm" action="">
            <div class="panel-group" id="accordion2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#signoffCollapseOne">Job Details</a>
                            </h4>
                        </div>
                        <div id="signoffCollapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="signoffMaintenanceType">Maintenance Type</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                                            <option value="1">Service</option>
                                            <option value="2">Maintenance</option>
                                            <option value="3">Replacement</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="signoffFullJobDescription">Full Job Description</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <textarea class="form-control" rows="5" name="signoffFullJobDescription" id="signoffFullJobDescription" required></textarea>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#signoffCollapseTwo">Asset That Was Maintained</a>
                            </h4>
                        </div>
                        <div id="signoffCollapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-8">
                                    <a href="#importFromAssets-modal" data-toggle="modal" data-target="#importFromAssets-modal"><button type="button" id="signoffAddAsset" class="btn btn-success">Add Asset</button></a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table width="100%" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Department</th>
                                                <th>Category</th>
                                                <th>Type</th>
                                                <th>Description</th>
                                                <th>No</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Receiving</td>
                                                <td>Moveable Asset</td>
                                                <td>Forklift</td>
                                                <td>10 Ton Diesel</td>
                                                <td>12</td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#signoffCollapseThree">Cause of Breakdown & Part to be replaced</a>
                            </h4>
                        </div>
                        <div id="signoffCollapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="type">Type</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                                        <option value="1">Operator no training</option>
                                        <option value="2">No preventative training</option>
                                        <option value="3">Misuse</option>
                                        <option value="4">Age</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="description">Description</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <textarea class="form-control" rows="5" name="description" id="description" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="downTimeInHours">Down Time In Hours</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="downTimeInHours" id="downTimeInHours">
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="partToBeReplaced">Part To Be Replaced</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="partToBeReplaced" id="partToBeReplaced">
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="totalCost">Total Cost</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">R</span>
                                            <input type="text" class="form-control" name="totalCost" id="totalCost" required>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#signoffCollapseFour">Supplier</a>
                            </h4>
                        </div>
                        <div id="signoffCollapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-8">
                                    <a href="#addSupplier-modal" data-toggle="modal" data-target="#addSupplier-modal"><button type="button" id="addSupplier" class="btn btn-success">Add Supplier</button></a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table width="100%" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Supplier</th>
                                                <th>Service</th>
                                                <th>Rating</th>
                                                <th>Comments</th>
                                                <th>Cost</th>
                                                <th>Evidence</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>James Painters</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>R 1200</td>
                                                <td></td>
                                                <td><a href="#complete-modal" data-toggle="modal" data-target="#complete-modal">Complete</a></td>
                                                <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
            <div class="form-group">
                <div class="col-sm-8">
                <button type="submit" id="serviceSignOffBtn" class="btn btn-success">Sign Off</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Add Supplier Modal -->
  <div class="modal fade" id="addSupplier-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Complete Supplier Details</h4>
        </div>
        <div class="modal-body table-responsive">
            <form class="form-horizontal" method="post" name="addSupplierForm" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="signoffSupplierName">Supplier Name</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a supplier">
                        <option value="1">Nick Botha</option>
                        <option value="2">Sunny Mathole</option>
                        <option value="3">Brian Douglas</option>
                        </select>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-sm-4" for="signoffRate">Rate Service Provider</label>
                    <div class="col-lg-6 col-md-4 col-sm-8 star-rating">
                        <input type="hidden" class="rating"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="noteOfServiceProvider">Note of Service Provider</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea class="form-control" rows="5" name="ServiceTypeDescription" id="ServiceTypeDescription" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="supportingDocs">Upload Supporting Documents</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="file" class="supportingDocs"/>
                    </div>
                </div>   
                <div class="form-group">
                    <label class="control-label col-sm-4" for="costOfService">Cost of Service</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="costOfService" id="costOfService" required value="R">
                    </div>
                </div>
            </form>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
            <button type="button" id="addSupplierBtn" class="btn btn-success">Submit</button>
            </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Import From Assets Modal -->
  <div class="modal fade" id="importFromAssets-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Import From Assets</h4>
        </div>
        <div class="modal-body table-responsive">
            <form class="form-horizontal" method="post" name="searchAssets" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="searchDepartment">Department</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                        <option>Warehouse</option>
                        <option>Receiving</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="searchType">Type</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more types" data-header="Close">
                        <option>Compressor</option>
                        <option>Forklift</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="button" id="searchAssetsBtn" class="btn btn-success">View</button>
                    </div>
                </div>
            </form>
            <table width="100%" class="table table-hover" id="importFromAssetsTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Number</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
            </tbody>
        </table>
        <div class="form-group">
            <div class="col-sm-8">
            <button type="button" id="importFromAssetsBtn" class="btn btn-success">Add</button>
            </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Add Component Modal -->
  <div class="modal fade" id="addComponent-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Component / Part</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addComponentForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="selectComponent">Select Component</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select component">
                        <option>Tires</option>
                        <option>Engine</option>
                        <option>Gear Box</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="newComponent">New Component</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <a href="#newComponent-modal" data-toggle="modal" data-target="#newComponent-modal"><button type="button" id="newComponent" class="btn btn-success">Add New</button></a>
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>New Component / Part</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="selectPart">Select Part</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select part">
                        <option>Pistons</option>
                        <option>Head Cover</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="newPart">New Part</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <a href="#newPart-modal" data-toggle="modal" data-target="#newPart-modal"><button type="button" id="newPart" class="btn btn-success">Add New</button></a>
                </div>
            </div><br/>
            
            <div class="form-group">
                <label class="control-label col-sm-4" for="partType">Part Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="partType" id="partType" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="partDescription">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="partDescription" id="partDescription" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="size">Size</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="size" id="size" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="quantity">Quantity</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="quantity" id="quantity" required placeholder=""> 
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Sub Parts</h5></span></div>
            <div class="form-group">
                <div class="col-lg-6 col-md-4 col-sm-8">
                <a href="#addSubPart-modal" data-toggle="modal" data-target="#addSubPart-modal"><button type="button" id="addSubPart" class="btn btn-success">Add Sub Part</button></a>
                </div>
            </div>
            
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Sub Part Type</th>
                            <th>Description</th>
                            <th>Size</th>
                            <th>Quantity</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <div class="col-sm-8">
                <button type="submit" id="addComponentBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- New Component Modal -->
  <div class="modal fade" id="newComponent-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Component</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="newComponentForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="newComponentDescription">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="newComponentDescription" id="newComponentDescription" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="button" id="newComponentBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- New Part Modal -->
  <div class="modal fade" id="newPart-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Part</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="newPartForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="newPartType">Part Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="newPartType" id="newPartType" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="newPartDescription">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="newPartDescription" id="newPartDescription" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="newSize">Size</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="newSize" id="newSize" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="newQuantity">Quantity</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="newQuantity" id="newQuantity" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="button" id="newPartBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Add Sub Part Modal -->
  <div class="modal fade" id="addSubPart-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Sub Part</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addSubPartForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="subPartType">Sub Part Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="subPartType" id="subPartType" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="subPartDescription">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="subPartDescription" id="subPartDescription" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="subPartSize">Size</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="subPartSize" id="subPartSize" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="subParQuantity">Quantity</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="subParQuantity" id="subParQuantity" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="button" id="addSubPartBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<script>
$(function(){
    $("#scheduledStartDateTime").datetimepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt', 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datetimepicker();
    
    $("#endDateTime").datetimepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt', 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datetimepicker();
    
    /*$('.rating').each(function () {
          $('<span class="label label-success"></span>')
            .text($(this).val() || ' ')
            .insertAfter(this);
        });
        $('.rating').on('change', function () {
          $(this).next('.label').text($(this).val());
        });*/
});
$('#importFromAssetsTbl').tablecheckbox();
var chkbox = document.getElementsByClassName("chkbox");

$("#importFromAssetsBtn").click(function(){
    $("#importFromAssets-modal").modal('toggle');
});

$("#addSupplierBtn").click(function(){
    $("#addSupplier-modal").modal('toggle');
});

$("#newComponentBtn").click(function(){
    $("#newComponent-modal").modal('toggle');
});

$("#newPartBtn").click(function(){
    $("#newPart-modal").modal('toggle');
});

$("#addSubPartBtn").click(function(){
    $("#addSubPart-modal").modal('toggle');
});
$(".create_job_link").click(function()
{
   // var id = $(this).attr("link_id");
    alert("");
});
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    