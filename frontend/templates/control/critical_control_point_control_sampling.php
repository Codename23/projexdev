<?php 
$title = 'Control Sampling';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?>
<!--End of col-lg-2 side-bar --> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
           <li ><a href="<?php echo BASE_URL;?>/index.php?modulhhhha e=criticalpoint&action=critical_control_point">Upcoming</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=criticalpoint&action=control_sampling">Control / Sampling</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?module=criticalpoint&action=view_settings">Setup</a></li>
            <li><a href="critical_control_point_nonconformances.php">Non-Conformances</a></li>
        <li><a href="critical_control_point_nonconformance_history.php">Non-Conformance History</a></li>
        </ul>
        </div>
    </div>   
    <!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="<?php echo BASE_URL;?>/index.php?action=addControlPoint&module=criticalpoint
           
           <!--7uj9on    ookmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmk nnnnnnnnnnnnnnnnnnn><button type="button" id="addControlSampling" class="btn btn-success">Add Control/Sampling</button></a>-->
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Control / Sampling</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Group</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Document Link</th>
                    <th>Frequency Input</th>
                    <th>Min</th>
                    <th>Baseline</th>
                    <th>Max</th>
                    <th>Units</th>
                    <th>Per</th>
                    <th>View Process</th>
                    <th>Form</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Warehouse</td>
                    <td>Food</td>
                    <td>Freezer Temp</td>
                    <td>Temperature of Freezer</td>
                    <td><a href="">View</a></td>
                    <td>Hourly</td>
                    <td>-20</td>
                    <td>-10</td>
                    <td>0</td>
                    <td>C</td>
                    <td>Hour</td>
                    <td><a href="">View</a></td>
                    <td><a href="#form-modal" data-toggle="modal" data-target="#form-modal">View</a></td>
                    <td><a href="#inputSchedule-modal" data-toggle="modal" data-target="#inputSchedule-modal">Input Schedule</a></td>
                    <td><a href="">View History</a></td>
                    <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                </tr>
                <tr>
                    <td>Mixing Department</td>
                    <td>Quality</td>
                    <td>Weight of plastic</td>
                    <td>Weighing new batch of plastic</td>
                    <td><a href="">View</a></td>
                    <td>Daily</td>
                    <td>1500</td>
                    <td>2000</td>
                    <td>2500</td>
                    <td>kg</td>
                    <td>Day</td>
                    <td><a href="">View</a></td>
                    <td><a href="#form-modal" data-toggle="modal" data-target="#form-modal">View</a></td>
                    <td><a href="#inputSchedule-modal" data-toggle="modal" data-target="#inputSchedule-modal">Input Schedule</a></td>
                    <td><a href="">View History</a></td>
                    <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!-- Input Schedule Modal -->
  <div class="modal fade" id="inputSchedule-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">1 Week Input Schedule</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="inputScheduleForm" action="">
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Day</th>
                            <th>Time</th>
                            <th>Responsible Person</th>
                            <th>Manager</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Monday</td>
                            <td>08:00</td>
                            <td>Nick</td>
                            <td>Werner</td>
                            <td><a href="#add-modal" data-toggle="modal" data-target="#add-modal">Add</a></td>
                            <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                        <tr>
                            <td>Tuesday</td>
                            <td>08:00</td>
                            <td>Nick</td>
                            <td>Werner</td>
                            <td><a href="#add-modal" data-toggle="modal" data-target="#add-modal">Add</a></td>
                            <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                        <tr>
                            <td>Wednesday</td>
                            <td>08:00</td>
                            <td>Nick</td>
                            <td>Werner</td>
                            <td><a href="#add-modal" data-toggle="modal" data-target="#add-modal">Add</a></td>
                            <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                        <tr>
                            <td>Thursday</td>
                            <td>08:00</td>
                            <td>Nick</td>
                            <td>Werner</td>
                            <td><a href="#add-modal" data-toggle="modal" data-target="#add-modal">Add</a></td>
                            <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                        <tr>
                            <td>Friday</td>
                            <td>08:00</td>
                            <td>Nick</td>
                            <td>Werner</td>
                            <td><a href="#add-modal" data-toggle="modal" data-target="#add-modal">Add</a></td>
                            <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                        <tr>
                            <td>Saturday</td>
                            <td>Off</td>
                            <td>Off</td>
                            <td>Off</td>
                            <td><a href="#add-modal" data-toggle="modal" data-target="#add-modal">Add</a></td>
                            <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                        <tr>
                            <td>Sunday</td>
                            <td>Off</td>
                            <td>Off</td>
                            <td>Off</td>
                            <td><a href="#add-modal" data-toggle="modal" data-target="#add-modal">Add</a></td>
                            <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <div class="col-sm-8">
                <button type="submit" id="inputScheduleBtn" class="btn btn-success">Save</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Form Modal -->
  <div class="modal fade" id="form-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Form</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="formForm" action="">
            <div class="form-group">
                <div class="col-lg-4 col-md-4 col-sm-8">
                <button type="button" id="" class="btn btn-success">Add Question</button>
                </div>
            </div>
            <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Yes</th>
                                    <th>No</th>
                                    <th>Comments</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Is the facility clean?</td>
                                    <td><input type="checkbox" class="chkbox"></td>
                                    <td><input type="checkbox" class="chkbox"></td>
                                    <td><textarea rows="1" class="form-control custom-hidden-input" name="measurement" id="measurement" required placeholder=""></textarea></td>
                                </tr>
                                <tr>
                                    <td>Did yu smell anything?</td>
                                    <td><input type="checkbox" class="chkbox"></td>
                                    <td><input type="checkbox" class="chkbox"></td>
                                    <td><textarea rows="1" class="form-control custom-hidden-input" name="measurement" id="measurement" required placeholder=""></textarea></td>
                                </tr>
                                <tr>
                                    <td>Was the lights on?</td>
                                    <td><input type="checkbox" class="chkbox"></td>
                                    <td><input type="checkbox" class="chkbox"></td>
                                    <td><textarea rows="1" class="form-control custom-hidden-input" name="measurement" id="measurement" required placeholder=""></textarea></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
            <div class="form-group">
                <div class="col-sm-8">
                <button type="submit" id="formBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Add Modal -->
  <div class="modal fade" id="add-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Input Details</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="time">Time</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="time" name="time" required >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="responsiblePerson">Responsible Person</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                        <option>Nick Botha</option>
                        <option>Brian Douglas</option>
                        <option>Sunny Mathole</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="manager">Manager</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a manager">
                        <option>Nick Botha</option>
                        <option>Brian Douglas</option>
                        <option>Sunny Mathole</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="button" id="addBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div><!--End of col-lg-10 --> 
</div>
<script>
    $(function() {
    $("#time").timepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt'
    }).datetimepicker();
    });
    
    $("#addBtn").click(function(){
        $("#add-modal").modal('toggle');
    });
</script>
<?php 
/*
 * 
 * 
 * 
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    