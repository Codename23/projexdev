<?php 
/*
 * Header file
 */
$title = 'Critical Control Point';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=criticalpoint&action=critical_control_point">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=criticalpoint&action=control_sampling">Control / Sampling</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=criticalpoint&action=view_settings">Setup</a></li>
        <li><a href="critical_control_point_nonconformances.php">Non-Conformances</a></li>
        <li><a href="critical_control_point_nonconformance_history.php">Non-Conformance History</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

    <div class="panel panel-default">
        <div class="panel-heading">Search Upcoming</div>
        <div class="panel-body">
        <form class="form-inline" method="post" name="search" action="">
            <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="col-md-4 col-sm-4">
            <div class="form-group">
                <p><b>Department</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                    <option>I.T</option>
                    <option>Human Resources</option>
                    <option>Marketing</option>
                    <option>Support and Services</option>
                </select>
            </div>
            </div>
                
            <div class="col-md-4 col-sm-4">
            <div class="form-group">
                <p><b>Group</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more groups"data-header="Close">
                    <option value="1">OHS (Health & Safety)</option>
                    <option value="2">E (Environment)</option>
                    <option value="3">Q (Quality)</option>
                    <option value="4">F (Food)</option>
                </select>
            </div>
            </div>
                
            <div class="col-md-4 col-sm-4">
            <div class="form-group">
                <p><b>Type</b></p>
                <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                    <option value="1">Freezer Temp</option>
                    <option value="2">Weight of plasic</option>
                </select> 
            </div>
            </div>
            </div>
            <div style="margin-left: 30px;">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Search">
            </div>
            </div>
        </form>
        </div>
        </div>

<div class="panel panel-default">
    <div class="panel-heading">Upcoming</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Group</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Document Link</th>
                    <th>Frequency Input</th>
                    <th>Next Input</th>
                    <th>Responsible Person</th>
                    <th>Last Measurement</th>
                    <th>Last Form</th>
                    <th>Date Time</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>             
                <tr>
                    <td>Warehouse</td>
                    <td>Food</td>
                    <td>Freezer Temp</td>
                    <td>Temperature of Freezer</td>
                    <td><a href="">View</a></td>
                    <td>Hourly</td>
                    <td>11/05/2017 13h00</td>
                    <td>Nick Botha</td>
                    <td>19</td>
                    <td><a href="">View</a></td>
                    <td>11/05/2017 12h00</td>
                    <td><a href="#addMeasurement-modal" data-toggle="modal" data-target="#addMeasurement-modal">Add Measurement</a></td>
                    <td><a href="critical_control_point_add_measurement.php">History Report</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                </tr>              
            </tbody>
        </table>    
    </div>
    </div>
    </div>

<!-- Add Measurement Modal -->
  <div class="modal fade" id="addMeasurement-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Dare & Measurements</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addMeasurementForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="measurement">Measurement</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="measurement" id="measurement" required placeholder=""> 
                </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-sm-4" for="docLink">Document Link</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="file" name="docLink" id="docLink"> 
                </div>
            </div><br/>
            
            <div class="modal-custom-h5"><span><h5>Form</h5></span></div>
            <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Yes</th>
                                    <th>No</th>
                                    <th>Comments</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Is the facility clean?</td>
                                    <td><input type="checkbox" class="chkbox"></td>
                                    <td><input type="checkbox" class="chkbox"></td>
                                    <td><textarea rows="1" class="form-control custom-hidden-input" name="measurement" id="measurement" required placeholder=""></textarea></td>
                                </tr>
                                <tr>
                                    <td>Did yu smell anything?</td>
                                    <td><input type="checkbox" class="chkbox"></td>
                                    <td><input type="checkbox" class="chkbox"></td>
                                    <td><textarea rows="1" class="form-control custom-hidden-input" name="measurement" id="measurement" required placeholder=""></textarea></td>
                                </tr>
                                <tr>
                                    <td>Was the lights on?</td>
                                    <td><input type="checkbox" class="chkbox"></td>
                                    <td><input type="checkbox" class="chkbox"></td>
                                    <td><textarea rows="1" class="form-control custom-hidden-input" name="measurement" id="measurement" required placeholder=""></textarea></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="password">Password</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="password" class="form-control" name="password" id="password" required placeholder=""> 
                </div>
            </div> 
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addMeasurementBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    