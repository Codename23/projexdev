<?php 
$title = 'Setup';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of col-lg-2 side-bar --> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
         <li ><a href="<?php echo BASE_URL;?>/index.php?module=criticalpoint&action=critical_control_point">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=criticalpoint&action=control_sampling">Control / Sampling</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=criticalpoint&action=view_settings">Setup</a></li>
            <li><a href="critical_control_point_nonconformances.php">Non-Conformances</a></li>
        <li><a href="critical_control_point_nonconformance_history.php">Non-Conformance History</a></li>
        </ul>
        </div>
    </div>  
    <!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#addTypes-modal" data-toggle="modal" data-target="#addTypes-modal"><button type="button" id="addTypes" class="btn btn-success">Add Types</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Types</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Group</th>
                    <th>Type</th>
                    <th>Date Added</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                       <?php
                        $counter = 0;
             
                       if($data['allTypes']): foreach($data['allTypes'] as $types):
                           $counter++;
                           ?>
                            <tr>
                                <td><?php echo $types['sheqteam_name']; ?></td>
                                <td><?php echo $types['type']; ?></td>                                
                                <td><?php echo $types['date_created']; ?></td>   
                                 <td><span class="glyphicon glyphicon-pencil"></span><a href="<?php echo BASE_URL."/index.php?type_id={$types["id"]}&source_type=Material&action=material_view_sop&module=material";?>"> Edit</a></td>
                                 <td><span class="glyphicon glyphicon-trash"></span><a href="<?php echo BASE_URL;?>/index.php?type_id=<?php echo $types['id'];?>&action=view_material_info&module=material"> Delete</a></td>
                                 <td></td>
                            </tr>
                        <?php endforeach;endif; ?>                    
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!-- Add Types Modal -->
  <div class="modal fade" id="addTypes-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Type</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addTypesForm" action="<?php echo BASE_URL;?>/index.php?module=criticalpoint&action=post_control_types">
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-control" id="search_groups" data-live-search="true" name="group" data-live-search-placeholder="Search" required ultiple title="Select a groups">
                       <?php
                        foreach($data['allSheqTeams'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['sheqteam_name']}</option>";
                        }?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="type" id="type" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addTypesBtn" name="addTypesBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div><!--End of col-lg-10 --> 
</div>
<!--End of container-fluid -->
<?php 
/*
 * 
 * 
 * 
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    