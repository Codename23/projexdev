<?php 
/*
 * Header file
 */
$title = 'Add Measurement';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?>
<!--End of navigation--> 
<div class="col-lg-10">
<form class="form-horizontal" method="post" name="addDataForm" action="">
<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Material Details</a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
            <div class="table-responsive">
            <table width="100%" class="table table-hover">
                <thead>
                    <tr>
                        <th colspan="2">Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="30%">Group</td>
                        <td>Food</td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td>Temperature</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>Food Temperature</td>
                    </tr>
                    <tr>
                        <td>Department</td>
                        <td>Storage</td>
                    </tr>
                </tbody>
            </table>
        </div>       
        </div>
    </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Control Point</a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body">
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                        <tr>
                            <td width="30%">Units</td>
                            <td>C</td>
                        </tr>
                        <tr>
                            <td>Per</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Measurement Reading Frequency</td>
                            <td>Hourly</td>
                        </tr>
                        <tr>
                            <td>Document Link</td>
                            <td>View</td>
                        </tr>
                        <tr>
                            <td>Minimum</td>
                            <td>-20</td>
                        </tr>
                        <tr>
                            <td>Baseline</td>
                            <td>-10</td>
                        </tr>
                        <tr>
                            <td>Maximum</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>Units</td>
                            <td>C</td>
                        </tr>
                        <tr>
                            <td>Per</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Process Description</td>
                            <td><p>Enter facility and take measurement</p><p>Ensure correct ppe is used</p></td>
                        </tr>
                        <tr>
                            <td>Notify when minimum & maximum is reached</td>
                            <td>John PArker</td>
                        </tr>
                    </tbody>
                </table>
            </div> 
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Input Time Table</a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body">
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Day</th>
                            <th>Time</th>
                            <th>Responsible Person</th>
                            <th>Manager</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Monday</td>
                            <td>08:00</td>
                            <td>Nick</td>
                            <td>Werner</td>
                            <td><?php include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                        <tr>
                            <td>Tuesday</td>
                            <td>08:00</td>
                            <td>Nick</td>
                            <td>Werner</td>
                            <td><?php include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                        <tr>
                            <td>Wednesday</td>
                            <td>08:00</td>
                            <td>Nick</td>
                            <td>Werner</td>
                            <td><?php include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                        <tr>
                            <td>Thursday</td>
                            <td>08:00</td>
                            <td>Nick</td>
                            <td>Werner</td>
                            <td><?php include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                        <tr>
                            <td>Friday</td>
                            <td>08:00</td>
                            <td>Nick</td>
                            <td>Werner</td>
                            <td><?php include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                        <tr>
                            <td>Saturday</td>
                            <td>Off</td>
                            <td>Off</td>
                            <td>Off</td>
                            <td><?php include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                        <tr>
                            <td>Sunday</td>
                            <td>Off</td>
                            <td>Off</td>
                            <td>Off</td>
                            <td><?php include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Add Data</a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse">
            <div class="panel-body">
            <div class="form-group">
                <label class="control-label col-sm-4" for="measurement">Measurement</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="measurement" id="measurement" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="units">Units</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="per">Per</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="per" id="per" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="docLink">Document Link</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="file" name="docLink" id="docLink"> 
                </div>
            </div>
            </div>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Form</a>
            </h4>
        </div>
        <div id="collapseFive" class="panel-collapse collapse">
            <div class="panel-body">
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Yes</th>
                            <th>No</th>
                            <th>Comments</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Is the facility clean?</td>
                            <td><input type="checkbox" class="chkbox"></td>
                            <td><input type="checkbox" class="chkbox"></td>
                            <td><textarea rows="1" class="form-control custom-hidden-input" name="measurement" id="measurement" required placeholder=""></textarea></td>
                        </tr>
                        <tr>
                            <td>Did yu smell anything?</td>
                            <td><input type="checkbox" class="chkbox"></td>
                            <td><input type="checkbox" class="chkbox"></td>
                            <td><textarea rows="1" class="form-control custom-hidden-input" name="measurement" id="measurement" required placeholder=""></textarea></td>
                        </tr>
                        <tr>
                            <td>Was the lights on?</td>
                            <td><input type="checkbox" class="chkbox"></td>
                            <td><input type="checkbox" class="chkbox"></td>
                            <td><textarea rows="1" class="form-control custom-hidden-input" name="measurement" id="measurement" required placeholder=""></textarea></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="password">Password</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="password" class="form-control" name="password" id="password" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addDataBtn" class="btn btn-success">Add</button>
                </div>
            </div>
            </div>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Table View</a>
            </h4>
        </div>
        <div id="collapseSix" class="panel-collapse collapse">
            <div class="panel-body">
            <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Start Date</b></p>
                <input style="padding:6px 38px;" type="text" class="form-control" name="startDate" id="startDate" required placeholder=""> 
            </div>
            </div>
            
            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>End Date</b></p>
                <input style="padding:6px 38px;" type="text" class="form-control" name="endDate" id="endDate" required placeholder=""> 
            </div>
            </div>
   
            </div>
            <div style="margin-left: 30px;">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Search">
            </div>
            </div>
            <br/>
                
            <div class="table-responsive">
            <table width="100%" class="table table-hover">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Measurement</th>
                        <th>Units</th>
                        <th>Per</th>
                        <th>View Document</th>
                        <th>View Form</th>
                        <th>Responsible Person</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>11/05/2017</td>
                        <td>08h00</td>
                        <td>-18</td>
                        <td>C</td>
                        <td></td>
                        <td><a href="">View</a></td>
                        <td><a href="">View</a></td>
                        <td>Nick Botha</td>
                        <td><?php include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                    </tr>
                    <tr>
                        <td>11/05/2017</td>
                        <td>09h00</td>
                        <td>-19</td>
                        <td>C</td>
                        <td></td>
                        <td><a href="">View</a></td>
                        <td><a href="">View</a></td>
                        <td>Nick Botha</td>
                        <td><?php include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                    </tr>
                    <tr>
                        <td>11/05/2017</td>
                        <td>10h00</td>
                        <td>-17</td>
                        <td>C</td>
                        <td></td>
                        <td><a href="">View</a></td>
                        <td><a href="">View</a></td>
                        <td>Nick Botha</td>
                        <td><?php include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                    </tr>
                </tbody>
            </table>    
        </div>
            </div>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Statistics</a>
            </h4>
        </div>
        <div id="collapseSeven" class="panel-collapse collapse">
            <div class="panel-body">
            <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Start Date</b></p>
                <input style="padding:6px 38px;" type="text" class="form-control" name="statisticsStartDate" id="statisticsStartDate" required placeholder=""> 
            </div>
            </div>
            
            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>End Date</b></p>
                <input style="padding:6px 38px;" type="text" class="form-control" name="statisticsEndDate" id="statisticsEndDate" required placeholder=""> 
            </div>
            </div>
   
            </div>
            <div style="margin-left: 30px;">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Search">
            </div>
            </div>
            <br/>
            <div class="col-lg-12">
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
            </div>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">Send Report</a>
            </h4>
        </div>
        <div id="collapseEight" class="panel-collapse collapse">
            <div class="panel-body">
            <div class="form-group">
                <label class="control-label col-sm-4" for="name">Name</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="name" name="name" required >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="email">E-main Address</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="email" class="form-control" id="email" name="email" required > 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <a href=""><button type="button" id="sendReportBtn" class="btn btn-success">Send</button></a>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<!--End of panel-default-->  
</form>



    
    
    
    
    
    
    

</div><!--End of container-fluid-->
<script src="js/bootstrap-select.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script>
$(function () {
$("#startDate").datepicker({
    dateFormat:"yy/mm/dd", 
    changeYear:true, 
    changeMonth:true,
    yearRange:"1900:3000"}).datepicker();

$("#endDate").datepicker({
    dateFormat:"yy/mm/dd", 
    changeYear:true, 
    changeMonth:true,
    yearRange:"1900:3000"}).datepicker();

$("#statisticsStartDate").datepicker({
    dateFormat:"yy/mm/dd", 
    changeYear:true, 
    changeMonth:true,
    yearRange:"1900:3000"}).datepicker();

$("#statisticsEndDate").datepicker({
    dateFormat:"yy/mm/dd", 
    changeYear:true, 
    changeMonth:true,
    yearRange:"1900:3000"}).datepicker();
    
Highcharts.chart('container', {

    title: {
        text: 'Freezer Temperature'
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Total'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },

    series: [{
        name: 'Installation',
        data: [0, 25, 10, 5, 15, 0, 25, 10, 5, 15, 0, 25]
    }]

});
});    

/*$("#accordion").hide();
$("#addData").click(function(){
    $("#accordion").toggle('slow');
});

$("#tableViewForm").hide();
$("#tableView").click(function(){
    $("#tableViewForm").toggle('slow');
});

$("#statisticsForm").hide();
$("#statistic").click(function(){
    $("#statisticsForm").toggle('slow');
});*/
</script>
<?php 
/*
 * 
 */
include_once('includes/footer.php');
?>  
    