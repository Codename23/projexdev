<?php
$title = 'Company Information';

include_once('frontend/templates/headers/default_header_tpl.php');
?>
<style>
    /***
    Custom tabs
    ***/

    .nav-tabs > li > a,
    .nav-pills > li > a {
        font-size: 14px;
    }

    .nav-tabs-sm > li > a,
    .nav-pills-sm > li > a {
        font-size: 13px;
    }

    .tabbable-custom {
        margin-bottom: 15px;
        padding: 0px;
        overflow: hidden;
    }

    .tabbable-custom > .nav-tabs {
        border: none;
        margin: 0px;
    }

    .tabbable-custom > .tab-content {
        background-color: #fff;
        border: 1px solid #ddd;
        -webkit-border-radius: 0;
        -moz-border-radius: 0;
        border-radius: 0;
        padding: 10px;
    }

    .tabbable-custom.nav-justified .tab-content {
        margin-top: -1px;
    }

    .tabs-below.tabbable-custom.nav-justified .tab-content {
        margin-top: 0px;
        margin-bottom: -2px;
    }

    .tabbable-custom.boxless > .tab-content {
        padding:15px 0;
        border-left:none;
        border-right:none;
        border-bottom:none;
    }

    .tabbable-custom .nav-tabs > li {
        margin-right: 2px;
        border-top: 2px solid transparent;
    }

    .tabbable-custom .nav-tabs > li > a {
        margin-right: 0;
    }

    .tabbable-custom .nav-tabs > li > a:hover {
        background: none;
        border-color:transparent;
    }

    .tabbable-custom .nav-tabs > li.active {
        border-top: 3px solid #169ef4;
        margin-top: 0;
        position: relative;
    }

    .tabbable-custom .nav-tabs > li.active > a  {
        border-top: none;
        font-weight: 400;
    }

    .tabbable-custom .nav-tabs > li.active > a:hover {
        border-top: none;
        background: #fff;
        border-color: #d4d4d4 #d4d4d4 transparent;
    }

    .tabbable-custom .nav-tabs > li {
        margin-right: 2px;
        border-top: 2px solid transparent;
    }

    /* below tabs */

    .tabs-below.tabbable-custom .nav-tabs > li > a {
        border-top: none;
        border-bottom: 2px solid transparent;
        margin-top: -1px;
    }

    .tabs-below.tabbable-custom .nav-tabs > li.active {
        border-top: none;
        border-bottom: 3px solid #d12610;
        margin-bottom: 0;
        position: relative;
    }

    .tabs-below.tabbable-custom .nav-tabs > li.active > a {
        border-bottom: none
    }

    .tabs-below.tabbable-custom .nav-tabs > li.active > a:hover {
        background: #fff;
        border-color: #d4d4d4 #d4d4d4 transparent;
    }

    /*full width tabs with bigger titles */
    .tabbable-custom.tabbable-full-width > .tab-content {
        padding:15px 0;
        border-left:none;
        border-right:none;
        border-bottom:none;
    }

    .tabbable-custom.tabbable-full-width .nav-tabs > li > a {
        color:#424242;
        font-size:15px;
        padding:9px 15px;
    }

    /***
    Custom portlet tabs
    .portlet > .portlet-title > .caption {

        display: inline-block;
        font-size: 18px;
        line-height: 18px;
        font-weight: 400;
        margin: 0;
        padding: 0;
        margin-bottom: 8px;
    }
    .portlet-tabs > .nav-tabs {
        position: relative;
        top: -41px;
        margin-right: 10px;
        overflow: hidden;
    }

    .portlet-tabs > .nav-tabs > li {
        float: right;
    }

    .portlet-tabs > .nav-tabs {
        border-bottom: none;
    }

    .portlet-tabs > .nav-tabs > li > a {
        color: #fff;
        padding-top: 8px;
        padding-bottom: 10px;
        line-height: 16px;
        margin-top: 6px;
        margin-left: 0px;
        margin-right: 0px;
        border-left: 0;
        border-right: 0;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }

    .portlet-tabs > .nav-tabs > li:last-child > a {
        border-right:0;
    }

    .portlet-tabs > .nav-tabs > li {
        margin-left: 1px;
    }

    .portlet-tabs > .nav-tabs > li.active {
        color: #333;
        border-top-color: transparent;
    }

    .portlet-tabs > .nav-tabs > li.active > a {
        margin-bottom: 0px;
        border-bottom: 0;
        margin-left: 0px;
        margin-right: 0px;
        border-left: 0;
        border-right: 0;
        border-top-color:transparent !important;
    }

    .portlet-tabs > .nav-tabs > li > a:hover {
        color: #333;
        margin-bottom: 0;
        border-bottom-color: transparent;
        margin-left: 0;
        margin-right: 0;
        border-left: 0;
        border-right: 0;
        border-top-color:transparent;
        background-color: #fff;
    }

    .portlet-tabs > .nav-tabs > .active > a  {
        color: #555555;
        cursor: default;
        background-color: #fff;
    }

    .portlet-tabs > .nav-tabs > .active > a:hover {
        background-color: #fff !important;
    }

    .portlet-tabs > .tab-content {
        padding: 10px !important;
        margin: 0px;
        margin-top: -50px !important;
    }

    .portlet.tabbable .portlet-body {
        padding: 0px;
    }
*/
    .tab-pane > p:last-child {
        margin-bottom: 0px;
    }

    /* reverse aligned tabs */

    .tabs-reversed > li {
        float: right;
    }

    .tabs-reversed > li,
    .tabs-reversed > li > a {
        margin-right: 0;
    }

    /*company tabs*/
    .company .tabbable-custom-company .nav-tabs > li > a {
        padding:6px 12px;
    }
    /*company navigation*/
    .company ul.company-nav {
        margin-bottom:30px;
    }

    .company ul.company-nav li {
        position:relative;
    }

    .company ul.company-nav li a {
        color:#557386;
        display:block;
        font-size:14px;
        padding:8px 10px;
        margin-bottom:1px;
        background:#f0f6fa;
        border-left:solid 2px #c4d5df;
    }

    .company ul.company-nav li a:hover {
        color:#169ef4;
        background:#ecf5fb;
        text-decoration:none;
        border-left:solid 2px #169ef4;
    }

    .company ul.company-nav li a.company-edit {
        top:0;
        right:0;
        margin:0;
        color:#fff;
        opacity:0.6;
        border:none;
        padding:3px 9px;
        font-size:12px;
        background:#000;
        position:absolute;
        filter:alpha(opacity=60); /*for ie*/
    }

    .company ul.company-nav li a.company-edit:hover {
        text-decoration:underline;
    }

    .company ul.company-nav a span {
        top:0;
        right:0;
        color:#fff;
        font-size:16px;
        padding:7px 13px;
        position:absolute;
        background:#169ef4;
    }

    .company ul.company-nav a:hover span {
        background:#0b94ea;
    }

    /*company information
    .company-info h1 {
        color:#383839;
        font-size:34px;
        font-weight:400;
        margin:0 0 10px 0;
    }

    .company-info ul {
        margin-bottom:15px;
    }

    .company-info li {
        color:#6b6b6b;
        font-size:13px;
        margin-right:15px;
        margin-bottom:5px;
        padding:0 !important;
    }

    .company-info li i {
        color:#b5c1c9;
        font-size:15px;
    }

    .company-info li:hover i {
        color:#169ef4;
    }
*/
    /*company sales summary*/
    .overview-summary ul {
        margin-top:-12px;
    }
    .overview-summary li {
        padding:10px 0;
        overflow:hidden;
        border-top:solid 1px #eee;
    }

    .overview-summary li:first-child {
        border-top:none;
    }

    .overview-summary li .overview-info {
        float:left;
        color:#646464;
        font-size:14px;
        text-transform:uppercase;
    }

    .overview-summary li .sale-num {
        float:right;
        color:#169ef4;
        font-size:20px;
        font-weight:300;
    }

    .overview-summary li span i {
        top:1px;
        width:13px;
        height:14px;
        margin-left:3px;
        position:relative;
        display:inline-block;
    }

    .overview-summary li i.icon-img-up {
        background:url(../../img/icon-img-up.png) no-repeat !important;
    }

    .overview-summary li i.icon-img-down {
        background:url(../../img/icon-img-down.png) no-repeat !important;
    }

    .overview-summary .caption h4 {
        color:#383839;
        font-size:18px;
    }

    .overview-summary .caption {
        border-color:#c9c9c9;
    }
</style>
<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */

    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <!--End of navigation-->

    <?php
    /*
     * Include side menu from the include file
     */

    include_once('frontend/templates/menus/side-menu.php');
    ?>

    <div class="col-lg-10">
        <?php
        /*
         * Include sub menu from the include file
         */

        //include_once('frontend/templates/menus/sub-menu.php');
        ?>
        <div class="row company">
            <div class="col-md-12">

                <div class="row">
                <div class="col-md-12">
                    <h2><span class="glyphicon glyphicon-user" ></span><?php echo $data["editCompanyInfo"]['company_name']; ?></h2>
                </div>
                </div>
                <!--BEGIN TABS-->
                <div class="tabbable tabbable-custom tabbable-full-width">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#overview" data-toggle="tab">
                                Overview
                            </a>
                        </li>
                        <li>
                            <a href="#companyInfo" data-toggle="tab">
                                Company Information
                            </a>
                        </li>
                        <li>
                            <a href="#companyBranch" data-toggle="tab">
                                Branches
                            </a>
                        </li>
                        <li>
                            <a href="#companyDepartments" data-toggle="tab">
                                Departments
                            </a>
                        </li>
                        <li>
                            <a href="#companyOccupation" data-toggle="tab">
                                Occupation
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="overview">
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="list-unstyled company-nav">
                                        <li>
                                            <img src="" class="img-responsive" alt=""/>
                                            <a href="#" class="company-edit">
                                                edit
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-8 company-info">
                                            <h1><?php echo $data["editCompanyInfo"]['company_name']; ?></h1>

                                            <p>
                                                <a href="#">
                                                    <?php echo $data["editCompanyInfo"]['office_email']; ?>
                                                </a>
                                            </p>

                                        </div>
                                        <!--end col-md-8-->
                                        <div class="col-md-4">
                                            <div class="portlet overview-summary">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        Summary
                                                    </div>
                                                    <div class="tools">
                                                        <a class="reload" href="javascript:;">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <ul class="list-unstyled">
                                                        <li>
																<span class="overview-info">
                                                                    <?php //library get number  branches by id ?>
                                                                    NO. Branches <i class="fa fa-img-up"></i>
																</span>
                                                            <span class="sale-num">
																	 <?php echo $data['noOfBranches']; ?>
																</span>
                                                        </li>
                                                        <li>
																<span class="overview-info">
																	 NO. Departments <i class="fa fa-img-down"></i>
																</span>
                                                            <span class="sale-num">
																	 <?php echo $data['noOfDepartments'] ; ?>
																</span>
                                                        </li>
                                                        <li>
																<span class="overview-info">
																	 NO. Employees
																</span>
                                                            <span class="sale-num">
																	 <?php echo $data['noOfEmployees'] ; ?>
                                                        </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end col-md-4-->
                                    </div>
                                    <!--end row-->
                                </div>
                            </div>
                        </div>
                        <!--tab_1_2-->
                        <div class="tab-pane" id="companyInfo">
                            <div class="row company-account">
                                <div class="col-md-12">
                                    <div class="tab-content">
                                        <div id="tab_1-1" class="tab-pane active">

                                            <form enctype="multipart/form-data" class="form-horizontal" name="companyinfo" id="companyinfoForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=companies&action=editCompany">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><?php
                                                        echo $objLanguage->languageText('COMPANY_INFO'); ?></div>
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="companyName"><?php echo $objLanguage->languageText('COMPANY_NAME'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="text" class="form-control" id="companyName" name="companyName" required value="<?php echo $data["editCompanyInfo"]['company_name']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="vatNumber"><?php echo $objLanguage->languageText('COMPANY_VAT_NUMBER'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="text" class="form-control" name="vatNumber" id="vatNumber" required value="<?php echo $data["editCompanyInfo"]['vat_number']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="logo"><?php echo $objLanguage->languageText('COMPANY_LOGO'); ?></label>
                                                            <div class="col-sm-8">
                                                                <input type="file" name="logo" id="logo">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="ptyNumber"><?php echo $objLanguage->languageText('COMPANY_PTY_NUMBER'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="text" class="form-control" name="ptyNumber" id="ptyNumber" required value="<?php echo $data["editCompanyInfo"]['pty_number']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--End of panel-default-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><?php echo $objLanguage->languageText('HEAD_OFFICE_DETAILS'); ?></div>
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="officeAddress"><?php echo $objLanguage->languageText('OFFICE_ADDRESS'); ?></label>
                                                            <div class="col-lg-6 col-md-4 col-sm-8">
                                                                <textarea class="form-control tinyMCEselector" rows="5" required name="officeAddress" id="officeAddress"><?php echo $data["editCompanyInfo"]['office_address']; ?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="officeNumber"><?php echo $objLanguage->languageText('OFFICE_NUMBER'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="tel" class="form-control" id="officeNumber" name="officeNumber" required  value="<?php echo $data["editCompanyInfo"]['office_tel_number']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="cellphoneNumber"><?php echo $objLanguage->languageText('CELLPHONE_NUMBER'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="tel" class="form-control" name="cellphoneNumber" id="cellphoneNumber" required  value="<?php echo $data["editCompanyInfo"]['cell_number']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="faxNumber"><?php echo $objLanguage->languageText('FAX_NUMBER'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="text" class="form-control" name="faxNumber" id="faxNumber" placeholder="Enter fax number"  value="<?php echo $data["editCompanyInfo"]['fax_number']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="email"><?php echo $objLanguage->languageText('OFFICE_EMAIL'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="email" class="form-control" name="email" id="email" required placeholder="Enter office e-mail address"  value="<?php echo $data["editCompanyInfo"]['office_email']; ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="countryName">Country</label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <select class="form-control" name="countryName" id="countryName">
                                                                    <?php foreach ($data['countries'] as $country): ?>
                                                                        <?php if($data["editCompanyInfo"]['country_id'] == $country['iso']) : ?>
                                                                        <option value="<?php echo $country['iso']; ?>" selected><?php echo $country['name']; ?></option>
                                                                            <?php else : ?>
                                                                            <option value="<?php echo $country['iso']; ?>" ><?php echo $country['name']; ?></option>
                                                                            <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="regionName">State/Province</label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                    <select class="form-control" name="regionName" id="regionName">
                                                                        <?php foreach ($data['companyRegions'] as $region): ?>
                                                                            <?php if($data["editCompanyInfo"]['region_id'] == $region['id']) : ?>
                                                                            <option value="<?php echo $region['id']; ?>" selected><?php echo $region['name']; ?></option>
                                                                                <?php else : ?>
                                                                                <option value="<?php echo $region['id']; ?>"><?php echo $region['name']; ?></option>
                                                                                <?php endif; ?>
                                                                        <?php endforeach; ?>
                                                                    </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-offset-4 col-sm-8">
                                                                <input type="hidden" name="companyId" value="<?php echo $data['allCompanies']['id']; ?>" />
                                                                <input type="hidden" name="id" value="<?php echo $data["editCompanyInfo"]['id']; ?>">
                                                                <button type="submit" class="btn btn-success"><?php echo $objLanguage->languageText('SUBMIT'); ?></button>
                                                                <button type="reset" class="btn btn-default"><?php echo $objLanguage->languageText('RESET'); ?></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--End of panel panel-default-->
                                            </form>

                                        </div>

                                    </div>
                                </div>
                                <!--end col-md-9-->
                            </div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane" id="companyBranch">

                            <!--End of panel panel-default -->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <button type="button" id="addBranch" class="btn btn-success">Add New Branch</button>
                                </div>
                            </div>
                            <div class="panel panel-default" id="additionalbranchform">
                                <form enctype="multipart/form-data" class="form-horizontal" name="additionalbranchForm" id="additionalbranchForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=companies&action=addBranch">
                                    <div class="panel-heading">Branch details</div>
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="companyName">Branch Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="text" class="form-control" id="branchName" name="branchName" required placeholder="Enter your company name" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="officeAddress">Office Address</label>
                                            <div class="col-lg-6 col-md-4 col-sm-8">
                                                <textarea class="form-control tinyMCEselector" name="officeAddress" required id="officeAddress"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="officeNumber">Office Number</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="tel" class="form-control" name="officeNumber" id="officeNumber" required placeholder="Enter office number">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="faxNumber">Fax Number</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="tel" class="form-control" name="faxNumber" id="faxNumber" placeholder="Enter fax number">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="officeEmail">Office E-Mail Address</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="email" class="form-control" name="officeEmail" id="officeEmail" required placeholder="Enter e-mail address">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="branchRegionName">State/Province</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control" name="branchRegionName" id="branchRegionName">
                                                    <?php foreach ($data['companyRegions'] as $region): ?>
                                                            <option value="<?php echo $region['id']; ?>"><?php echo $region['name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-4 col-sm-8">
                                                <input type="hidden" name="companyId" value="<?php echo $data["editCompanyInfo"]['id']; ?>" />
                                                <button type="submit" class="btn btn-success">Save</button>
                                                <button type="reset" class="btn btn-default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--End of panel panel-default -->
                            <div class="branchResults"></div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane" id="companyDepartments">

                            <!--End of panel panel-default-->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form class="form-inline" method="post">
                                        <div class="form-group">
                                            <button type="button" id="addDepartment" class="btn btn-success">Add New Department</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--End of the panel panel-default-->
                            <div class="panel panel-default" id="departmentsform">
                                <div class="panel-heading">Department details</div>
                                <div class="panel-body">
                                    <form class="form-horizontal" name="departmentsform"  method="post" action="<?php echo BASE_URL; ?>/index.php?module=companies&action=addDepartment">

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="departmentBranchName">Branch Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">

                                                <select class="form-control" name="branchId" id="branchId">
                                                    <option value="">None</option>
                                                    <?php foreach ($data['companyBranches'] as $branch): ?>
                                                        <option value="<?php echo $branch['id']; ?>"><?php echo $branch['branch_name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="departmentName">Department Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="text" class="form-control" name="departmentName" required placeholder="Enter department name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="buildingName">Building Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="text" class="form-control" name="buildingName" required placeholder="Enter building name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="floorName">Floor Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="text" class="form-control" name="floorName" required placeholder="Enter floor name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <input type="hidden" class="form-control" id="companyId" name="companyId" value="<?php echo $data["editCompanyInfo"]['id']; ?>">
                                            <div class="col-sm-offset-4 col-sm-8">
                                                <button type="submit" class="btn btn-success">Save</button>
                                                <button type="reset" class="btn btn-default">Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--End of panel panel-default-->
                            <div class="departmentsResults"></div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane" id="companyOccupation">

                            <div class="panel panel-default addOccupation">
                                <div class="panel-body">
                                    <form class="form-inline" method="post">
                                        <div class="form-group">
                                            <button type="button" id="addOccupation" class="btn btn-success">Add New Occupation</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--End of the panel panel-default-->
                            <div class="panel panel-default" id="occupationForm">
                                <div class="panel-heading">Occupation Details</div>
                                <div class="panel-body">
                                    <form class="form-horizontal" name="occupationForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=occupation&action=addOccupation">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="branchName">Branch Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control" name="occupationbranchName" id="occupationbranchName">
                                                    <option value="">None</option>
                                                    <?php foreach ($data['companyBranches'] as $branch): ?>
                                                        <?php if(isset($_SESSION['branch_id']) && $_SESSION['branch_id'] == $branch['id']): ?>
                                                        <option value="<?php echo $branch['id']; ?>" selected><?php echo $branch['branch_name']; ?></option>
                                                            <?php else: ?>
                                                            <option value="<?php echo $branch['id']; ?>"><?php echo $branch['branch_name']; ?></option>
                                                            <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="departmentName">Department Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control" name="departmentName" id="departmentName" required>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="occupationName">Occupations Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="text" class="form-control" name="occupationName" id="occupationName" placeholder="Enter occupation name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-4 col-sm-8">
                                                <input type="hidden" name="companyId" value="<?php echo $data["editCompanyInfo"]['id'] ?>" />
                                                <button type="submit" class="btn btn-success">Save</button>
                                                <button type="reset" class="btn btn-default">Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--End of panel panel-default-->
                            <div class="occupationResults"></div>
                        </div>
                        <!--end tab-pane-->
                    </div><!--End of row -->
                </div><!--End of container-fluid -->
            </div>

        </div>
    </div>
    <!--END TABS-->
</div>
</div>
<!-- END PAGE CONTENT-->

</div>
</div>
<?php

include_once('frontend/templates/footers/default_footer_tpl.php');
include_once('frontend/templates/footers/regions_ajax_tpl.php');
?>

<script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {


       var target = $(e.target).attr("href");

        if ((target == '#companyBranch')) {
            $.post('<?php echo BASE_URL . "/index.php?module=companies&action=viewAllCompanyBranches&id=" .$data["editCompanyInfo"]['id']; ?>', {}, function(data){
                $('.branchResults').html(data);
            });
        }

        if ((target == '#companyDepartments')) {
            $.post('<?php echo BASE_URL . "/index.php?module=companies&action=viewAllCompanyDepartments&id=" .$data["editCompanyInfo"]['id']; ?>', {}, function(data){
                $('.departmentsResults').html(data);
            });
        }

        if ((target == '#companyOccupation')) {
            $.post('<?php echo BASE_URL . "/index.php?module=occupation&action=viewCompanyOccupations&id=" .$data["editCompanyInfo"]['id']; ?>', {}, function(data){
                $('.occupationResults').html(data);
            });
        }



    });
    $(".editOccupation").on("click",function(e){
        e.preventDefault();

        var occupationId = $(this).attr("data");
        $.post('<?php echo BASE_URL . "/index.php?module=occupation&action=editOccupation"?>', {id:occupationId}, function(data){
            //alert( "Data Loaded: " + data );
            $('.occupationResults').html(data);
        });

    });


    $(document).ready(function() {

        <?php if(isset($_SESSION['branch_id'])): ?>
        var opt = '';
        var branch_id = '';
        branch_id = <?php echo $_SESSION['branch_id']; ?>;

        $.ajax({
            type: "POST",
            url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxBranchDepartments';?>",
            data: {'branch_id': branch_id},
            dataType: "json", //return type expected as json
            success: function(departments) {

                $('#departmentName').empty();

                if (departments.length > 0) {
                    $.each(departments, function(key, val) {
                        opt = $('<option/>');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#departmentName').append(opt);
                    });



                }else{

                }
            }
        });

        <?php endif; ?>

        $('#occupationbranchName').change(function() {
            var opt = '';
            var branch_id = '';
            branch_id = $(this).find(':selected').val();

            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxBranchDepartments';?>",
                data: {'branch_id': branch_id},
                dataType: "json", //return type expected as json
                success: function(departments) {

                    $('#departmentName').empty();

                    if (departments.length > 0) {
                        $.each(departments, function(key, val) {
                            opt = $('<option/>');
                            opt.val(val.id);
                            opt.text(val.name);
                            $('#departmentName').append(opt);
                        });



                    }else{

                    }
                }
            });
        });

    });

</script>