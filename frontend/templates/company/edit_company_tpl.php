<?php
$title = 'Company Information';
global $objLanguage;
include_once('frontend/templates/headers/default_header_tpl.php');
?>

<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */

    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <!--End of navigation-->

    <?php
    /*
     * Include side menu from the include file
     */

    include_once('frontend/templates/menus/side-menu.php');
    ?>

    <div class="col-lg-10">
        <?php
        /*
         * Include sub menu from the include file
         */


        ?>
        <div class="row company">
            <div class="col-md-12">

                <div class="row">
                <div class="col-md-12">
                    <h2><span class="glyphicon glyphicon-user" ></span><?php echo $data["editCompanyInfo"]['company_name']; ?></h2>
                </div>
                </div>
                <!--BEGIN TABS-->
                <div class="tabbable tabbable-custom tabbable-full-width">
                    <ul class="nav nav-tabs">
                        <li <?php echo !isset($data['is_active'])  ? 'class="active"' : ''; ?>>
                            <a href="#overview" data-toggle="tab">
                                Overview
                            </a>
                        </li>
                        <li <?php echo ($data['is_active'] == 'tab_company') ? 'class="active"' : ''; ?> >
                            <a href="#companyInfo" data-toggle="tab">
                                Company Information
                            </a>
                        </li>
                        <li <?php echo ($data['is_active'] == 'tab_branch') ? 'class="active"' : ''; ?> >
                            <a href="#companyBranch" data-toggle="tab">
                                Branches
                            </a>
                        </li>
                        <li <?php echo ($data['is_active'] == 'tab_department') ? 'class="active"' : ''; ?>>
                            <a href="#companyDepartments" data-toggle="tab">
                                Departments
                            </a>
                        </li>
                        <li <?php echo ($data['is_active'] == 'tab_occupation') ? 'class="active"' : ''; ?>>
                            <a href="#companyOccupation" data-toggle="tab">
                                Occupation
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane <?php echo !isset($data['is_active'])  ? 'active' : ''; ?>" id="overview">
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="list-unstyled company-nav">
                                        <li>
                                            <img src="" class="img-responsive" alt=""/>
                                            <a href="#" class="company-edit">
                                                edit
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-8 company-info">
                                            <h1><?php echo $data["editCompanyInfo"]['company_name']; ?></h1>

                                            <p>
                                                <a href="#">
                                                    <?php echo $data["editCompanyInfo"]['office_email']; ?>
                                                </a>
                                            </p>

                                        </div>
                                        <!--end col-md-8-->
                                        <div class="col-md-4">
                                            <div class="portlet overview-summary">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        Summary
                                                    </div>
                                                    <div class="tools">
                                                        <a class="reload" href="javascript:;">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <ul class="list-unstyled">
                                                        <li>
																<span class="overview-info">
                                                                    <?php //library get number  branches by id ?>
                                                                    NO. Branches <i class="fa fa-img-up"></i>
																</span>
                                                            <span class="sale-num">
																	 <?php echo $data['noOfBranches']; ?>
																</span>
                                                        </li>
                                                        <li>
																<span class="overview-info">
																	 NO. Departments <i class="fa fa-img-down"></i>
																</span>
                                                            <span class="sale-num">
																	 <?php echo $data['noOfDepartments'] ; ?>
																</span>
                                                        </li>
                                                        <li>
																<span class="overview-info">
																	 NO. Employees
																</span>
                                                            <span class="sale-num">
																	 <?php echo $data['noOfEmployees'] ; ?>
                                                        </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end col-md-4-->
                                    </div>
                                    <!--end row-->
                                </div>
                            </div>
                        </div>
                        <!--tab_1_2-->
                        <div class="tab-pane <?php echo ($data['is_active'] == 'tab_company') ? 'active' : ''; ?>" id="companyInfo">
                            <div class="row company-account">
                                <div class="col-md-12">
                                    <div class="tab-content">
                                        <div id="tab_1-1" class="tab-pane active">

                                            <form enctype="multipart/form-data" class="form-horizontal" name="companyinfo" id="companyinfoForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=companies&action=editCompany">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><?php
                                                        echo $objLanguage->languageText('COMPANY_INFO'); ?></div>
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="companyName"><?php echo $objLanguage->languageText('COMPANY_NAME'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="text" class="form-control" id="companyName" name="companyName" required value="<?php echo $data["editCompanyInfo"]['company_name']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="vatNumber"><?php echo $objLanguage->languageText('COMPANY_VAT_NUMBER'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="text" class="form-control" name="vatNumber" id="vatNumber" required value="<?php echo $data["editCompanyInfo"]['vat_number']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="logo"><?php echo $objLanguage->languageText('COMPANY_LOGO'); ?></label>
                                                            <div class="col-sm-8">
                                                                <input type="file" name="logo" id="logo">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="ptyNumber"><?php echo $objLanguage->languageText('COMPANY_PTY_NUMBER'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="text" class="form-control" name="ptyNumber" id="ptyNumber" required value="<?php echo $data["editCompanyInfo"]['pty_number']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--End of panel-default-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><?php echo $objLanguage->languageText('HEAD_OFFICE_DETAILS'); ?></div>
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="officeAddress"><?php echo $objLanguage->languageText('OFFICE_ADDRESS'); ?></label>
                                                            <div class="col-lg-6 col-md-4 col-sm-8">
                                                                <textarea class="form-control" rows="5" required name="officeAddress" id="officeAddress"><?php echo $data["editCompanyInfo"]['office_address']; ?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="officeNumber"><?php echo $objLanguage->languageText('OFFICE_NUMBER'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="tel" class="form-control" id="officeNumber" name="officeNumber" required  value="<?php echo $data["editCompanyInfo"]['office_tel_number']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="cellphoneNumber"><?php echo $objLanguage->languageText('CELLPHONE_NUMBER'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="tel" class="form-control" name="cellphoneNumber" id="cellphoneNumber" required  value="<?php echo $data["editCompanyInfo"]['cell_number']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="faxNumber"><?php echo $objLanguage->languageText('FAX_NUMBER'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="text" class="form-control" name="faxNumber" id="faxNumber" placeholder="Enter fax number"  value="<?php echo $data["editCompanyInfo"]['fax_number']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="email"><?php echo $objLanguage->languageText('OFFICE_EMAIL'); ?></label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <input type="email" class="form-control" name="email" id="email" required placeholder="Enter office e-mail address"  value="<?php echo $data["editCompanyInfo"]['office_email']; ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="countryName">Country</label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                <select class="form-control" name="countryName" id="countryName">
                                                                    <?php foreach ($data['countries'] as $country): ?>
                                                                        <?php if($data["editCompanyInfo"]['country_id'] == $country['iso']) : ?>
                                                                        <option value="<?php echo $country['iso']; ?>" selected><?php echo $country['name']; ?></option>
                                                                            <?php else : ?>
                                                                            <option value="<?php echo $country['iso']; ?>" ><?php echo $country['name']; ?></option>
                                                                            <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-sm-4" for="regionName">State/Province</label>
                                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                                    <select class="form-control" name="regionName" id="regionName">
                                                                        <?php foreach ($data['companyRegions'] as $region): ?>
                                                                            <?php if($data["editCompanyInfo"]['region_id'] == $region['id']) : ?>
                                                                            <option value="<?php echo $region['id']; ?>" selected><?php echo $region['name']; ?></option>
                                                                                <?php else : ?>
                                                                                <option value="<?php echo $region['id']; ?>"><?php echo $region['name']; ?></option>
                                                                                <?php endif; ?>
                                                                        <?php endforeach; ?>
                                                                    </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-offset-4 col-sm-8">
                                                                <input type="hidden" name="companyId" value="<?php echo $data['allCompanies']['id']; ?>" />
                                                                <input type="hidden" name="id" value="<?php echo $data["editCompanyInfo"]['id']; ?>">
                                                                <input type="hidden" name="is_active" value="tab_company" />
                                                                <button type="submit" class="btn btn-success"><?php echo $objLanguage->languageText('SUBMIT'); ?></button>
                                                                <button type="reset" class="btn btn-default"><?php echo $objLanguage->languageText('RESET'); ?></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--End of panel panel-default-->
                                            </form>

                                        </div>

                                    </div>
                                </div>
                                <!--end col-md-9-->
                            </div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane <?php echo ($data['is_active'] == 'tab_branch') ? 'active' : ''; ?>" id="companyBranch">

                            <!--End of panel panel-default -->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <button type="button" id="addBranch" class="btn btn-success">Add New Branch</button>
                                </div>
                            </div>
                            <div class="panel panel-default" id="additionalbranchform">
                                <form enctype="multipart/form-data" class="form-horizontal" name="additionalbranchForm" id="additionalbranchForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=companies&action=addBranch">
                                    <div class="panel-heading">Branch details</div>
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="companyName">Branch Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="text" class="form-control" id="branchName" name="branchName" required placeholder="Enter your company name" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="officeAddress">Office Address</label>
                                            <div class="col-lg-6 col-md-4 col-sm-8">
                                                <textarea class="form-control" name="officeAddress" required id="officeAddress"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="officeNumber">Office Number</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="tel" class="form-control" name="officeNumber" id="officeNumber" required placeholder="Enter office number">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="faxNumber">Fax Number</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="tel" class="form-control" name="faxNumber" id="faxNumber" placeholder="Enter fax number">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="officeEmail">Office E-Mail Address</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="email" class="form-control" name="officeEmail" id="officeEmail" required placeholder="Enter e-mail address">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="branchCountryName">Country</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control" name="branchCountryName" id="branchCountryName">
                                                    <?php foreach ($data['countries'] as $country): ?>
                                                        <?php if($data["editCompanyInfo"]['country_id'] == $country['iso']) : ?>
                                                            <option value="<?php echo $country['iso']; ?>" selected><?php echo $country['name']; ?></option>
                                                        <?php else : ?>
                                                            <option value="<?php echo $country['iso']; ?>" ><?php echo $country['name']; ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="branchRegionName">State/Province</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control" name="branchRegionName" id="branchRegionName">
                                                    <?php foreach ($data['companyRegions'] as $region): ?>
                                                            <option value="<?php echo $region['id']; ?>"><?php echo $region['name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-4 col-sm-8">
                                                <input type="hidden" name="companyId" value="<?php echo $data["editCompanyInfo"]['id']; ?>" />
                                                <input type="hidden" name="is_active" value="tab_branch" />
                                                <button type="submit" class="btn btn-success">Save</button>
                                                <button type="reset" class="btn btn-default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--End of panel panel-default -->
                            <div class="branchResults"></div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane <?php echo ($data['is_active'] == 'tab_department') ? 'active' : ''; ?>" id="companyDepartments">

                            <!--End of panel panel-default-->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form class="form-inline" method="post">
                                        <div class="form-group">
                                            <button type="button" id="addDepartment" class="btn btn-success">Add New Department</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--End of the panel panel-default-->
                            <div class="panel panel-default" id="departmentsform">
                                <div class="panel-heading">Department details</div>
                                <div class="panel-body">
                                    <form class="form-horizontal" name="departmentsform"  method="post" action="<?php echo BASE_URL; ?>/index.php?module=companies&action=addDepartment">

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="departmentBranchName">Branch Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">

                                                <select class="form-control" name="branchId" id="branchId">
                                                    <option value="">None</option>
                                                    <?php foreach ($data['companyBranches'] as $branch): ?>
                                                        <option value="<?php echo $branch['id']; ?>"><?php echo $branch['branch_name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="departmentName">Department Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="text" class="form-control" name="departmentName" required placeholder="Enter department name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="buildingName">Building Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="text" class="form-control" name="buildingName" required placeholder="Enter building name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="floorName">Floor Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="text" class="form-control" name="floorName" required placeholder="Enter floor name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <input type="hidden" class="form-control" id="companyId" name="companyId" value="<?php echo $data["editCompanyInfo"]['id']; ?>">
                                            <input type="hidden" name="is_active" value="tab_department" />
                                            <div class="col-sm-offset-4 col-sm-8">
                                                <button type="submit" class="btn btn-success">Save</button>
                                                <button type="reset" class="btn btn-default">Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--End of panel panel-default-->
                            <div class="departmentsResults"></div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane <?php echo ($data['is_active'] == 'tab_occupation') ? 'active' : ''; ?>" id="companyOccupation">

                            <div class="panel panel-default addOccupation">
                                <div class="panel-body">
                                    <form class="form-inline" method="post">
                                        <div class="form-group">
                                            <button type="button" id="addOccupation" class="btn btn-success">Add New Occupation</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--End of the panel panel-default-->
                            <div class="panel panel-default" id="occupationForm">
                                <div class="panel-heading">Occupation Details</div>
                                <div class="panel-body">
                                    <form class="form-horizontal" name="occupationForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=occupation&action=addOccupation">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="branchName">Branch Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control" name="occupationbranchName" id="occupationbranchName">
                                                    <option value="">None</option>
                                                    <?php foreach ($data['companyBranches'] as $branch): ?>
                                                        <?php if(isset($_SESSION['branch_id']) && $_SESSION['branch_id'] == $branch['id']): ?>
                                                        <option value="<?php echo $branch['id']; ?>" selected><?php echo $branch['branch_name']; ?></option>
                                                            <?php else: ?>
                                                            <option value="<?php echo $branch['id']; ?>"><?php echo $branch['branch_name']; ?></option>
                                                            <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="departmentName">Department Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control" name="departmentName" id="departmentName" required>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="occupationName">Occupations Name</label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="text" class="form-control" name="occupationName" id="occupationName" placeholder="Enter occupation name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-4 col-sm-8">
                                                <input type="hidden" name="companyId" value="<?php echo $data["editCompanyInfo"]['id'] ?>" />
                                                <input type="hidden" name="is_active" value="tab_occupation" />
                                                <button type="submit" class="btn btn-success">Save</button>
                                                <button type="reset" class="btn btn-default">Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--End of panel panel-default-->
                            <div class="occupationResults"></div>
                        </div>
                        <!--end tab-pane-->
                    </div><!--End of row -->
                </div><!--End of container-fluid -->
            </div>

        </div>
    </div>
    <!--END TABS-->
</div>
</div>
<!-- END PAGE CONTENT-->

</div>
</div>
<?php

include_once('frontend/templates/footers/default_footer_tpl.php');
include_once('frontend/templates/footers/regions_ajax_tpl.php');
?>

<script>
    $('#occupationForm').hide();
    var is_active = '<?php echo $data['is_active'] ?>';

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {


       var target = $(e.target).attr("href");

        if ((target == '#companyBranch')) {
            $.post('<?php echo BASE_URL . "/index.php?module=companies&action=viewAllCompanyBranches&id=" .$data["editCompanyInfo"]['id']; ?>', {}, function(data){
                $('.branchResults').html(data);
            });
        }

        if ((target == '#companyDepartments')) {
            $.post('<?php echo BASE_URL . "/index.php?module=companies&action=viewAllCompanyDepartments&id=" .$data["editCompanyInfo"]['id']; ?>', {}, function(data){
                $('.departmentsResults').html(data);
            });
        }

        if ((target == '#companyOccupation')) {
            $.post('<?php echo BASE_URL . "/index.php?module=occupation&action=viewCompanyOccupations&id=" .$data["editCompanyInfo"]['id']; ?>', {}, function(data){
                $('.occupationResults').html(data);
            });
        }



    });


    $(document).ready(function() {

        <?php if(isset($_SESSION['branch_id']) && isset($_GET['id']) == $_SESSION['company_id'] ): ?>
        var opt = '';
        var branch_id = '';
        branch_id = <?php echo $_SESSION['branch_id']; ?>;

        $.ajax({
            type: "POST",
            url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxBranchDepartments';?>",
            data: {'branch_id': branch_id},
            dataType: "json", //return type expected as json
            success: function(departments) {

                $('#departmentName').empty();
                $("<option  value=0>&nbsp;</option>").prependTo("#departmentName");
                if (departments.length > 0) {
                    $.each(departments, function(key, val) {
                        opt = $('<option/>');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#departmentName').append(opt);
                    });


                }else{

                }
            }
        });

        <?php endif; ?>

        $('#occupationbranchName').change(function() {
            var opt = '';
            var branch_id = '';
            branch_id = $(this).find(':selected').val();

            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxBranchDepartments';?>",
                data: {'branch_id': branch_id},
                dataType: "json", //return type expected as json
                success: function(departments) {

                    $('#departmentName').empty();
                    $("<option  value=0>&nbsp;</option>").prependTo("#departmentName");
                    if (departments.length > 0) {
                        $.each(departments, function(key, val) {
                            opt = $('<option/>');
                            opt.val(val.id);
                            opt.text(val.name);
                            $('#departmentName').append(opt);
                        });

                    }else{

                    }
                }
            });
        });

    });


    if (is_active == 'tab_branch') {
        $.post('<?php echo BASE_URL . "/index.php?module=companies&action=viewAllCompanyBranches&id=" .$data["editCompanyInfo"]['id']; ?>', {}, function(data){
            $('.branchResults').html(data);
        });
    }

    if (is_active == 'tab_department') {
        $.post('<?php echo BASE_URL . "/index.php?module=companies&action=viewAllCompanyDepartments&id=" .$data["editCompanyInfo"]['id']; ?>', {}, function(data){
            $('.departmentsResults').html(data);
        });
    }

    if (is_active == 'tab_occupation') {

        $.post('<?php echo BASE_URL . "/index.php?module=occupation&action=viewCompanyOccupations&id=" .$data["editCompanyInfo"]['id']; ?>', {}, function(data){
            $('.occupationResults').html(data);
        });
    }


</script>