<?php
$title = 'Company Information';
include_once('frontend/templates/headers/default_header_tpl.php');

global $objLanguage;
$objLanguage->addLanguageFromFile();

?>
<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */

    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <!--End of navigation-->

    <?php
    /*
     * Include side menu from the include file
     */

    include_once('frontend/templates/menus/side-menu.php');
    ?>

   
    <div class="col-lg-10">
        <?php
        /*
         * Include sub menu from the include file
         */

        //include_once('frontend/templates/menus/sub-menu.php');
        ?>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">Company Information</div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Company Name</th>
                                <th>VAT Number</th>
                                <th>PTY Number</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if(!empty($data['allCompanies'])){
                                foreach($data['allCompanies'] as $company):  ?>
                                <tr>
                                        <td><?php echo $company['id']; ?></td>
                                        <td><?php echo $company['company_name']; ?></td>
                                        <td><?php echo $company['vat_number']; ?></td>
                                        <td><?php echo $company['pty_number']; ?></td>
                                        <td><a href="<?php echo BASE_URL . "/index.php?module=companies&action=editCompany&id=" . $company['id'] ; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>

                                </tr>
                                <?php endforeach; 
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--End of panel panel-default-->
        </div>
        <!--End of panel-group-->
        <!-- Add Company form -->

            <form enctype="multipart/form-data" class="form-horizontal" name="companyinfo" id="companyinfoForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=companies&action=addCompany">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php
        //$objLanguage->addLanguageFromFile();
                    echo $objLanguage->languageText('COMPANY_INFO'); ?></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="companyName"><?php echo $objLanguage->languageText('COMPANY_NAME'); ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="companyName" name="companyName" required placeholder="Enter your company name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="vatNumber"><?php echo $objLanguage->languageText('COMPANY_VAT_NUMBER'); ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="vatNumber" id="vatNumber" required placeholder="Enter your VAT number">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="logo"><?php echo $objLanguage->languageText('COMPANY_LOGO'); ?></label>
                            <div class="col-sm-8">
                                <input type="file" name="logo" id="logo">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="ptyNumber"><?php echo $objLanguage->languageText('COMPANY_PTY_NUMBER'); ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="ptyNumber" id="ptyNumber" required placeholder="Enter your PTY number">
                            </div>
                        </div>
                    </div>
                </div>
                <!--End of panel-default-->
        <div class="panel panel-default">
            <div class="panel-heading"><?php echo $objLanguage->languageText('HEAD_OFFICE_DETAILS'); ?></div>
            <div class="panel-body">

                <div class="form-group">
                    <label class="control-label col-sm-4" for="officeDescription"><?php echo $objLanguage->languageText('HEAD_OFFICE_DESCRIPTION'); ?></label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="tel" class="form-control" id="officeDescription" name="officeDescription" required placeholder="Enter Head Office Description">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="officeAddress"><?php echo $objLanguage->languageText('OFFICE_ADDRESS'); ?></label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <textarea class="form-control" rows="5" required name="officeAddress" id="officeAddress"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="officeNumber"><?php echo $objLanguage->languageText('OFFICE_NUMBER'); ?></label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="tel" class="form-control" id="officeNumber" name="officeNumber" required placeholder="Enter office number">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="cellphoneNumber"><?php echo $objLanguage->languageText('CELLPHONE_NUMBER'); ?></label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="tel" class="form-control" name="cellphoneNumber" id="cellphoneNumber" required placeholder="Enter cellphone number">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="faxNumber"><?php echo $objLanguage->languageText('FAX_NUMBER'); ?></label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="faxNumber" id="faxNumber" placeholder="Enter fax number">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="email"><?php echo $objLanguage->languageText('OFFICE_EMAIL'); ?></label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="email" class="form-control" name="email" id="email" required placeholder="Enter office e-mail address">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-4" for="countryName">Country</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <select class="form-control" name="countryName" id="countryName">
                            <option value="">None</option>
                            <?php foreach ($data['countries'] as $country): ?>
                                <option value="<?php echo $country['iso']; ?>"><?php echo $country['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-4" for="regionName">State/Province</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <select class="form-control" name="regionName" id="regionName">
                            <option value="">None</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <input type="hidden" name="companyId" value="<?php echo $data['allCompanies']['id']; ?>" />
                        <button type="submit" class="btn btn-success"><?php echo $objLanguage->languageText('SUBMIT'); ?></button>
                        <button type="reset" class="btn btn-default"><?php echo $objLanguage->languageText('RESET'); ?></button>
                    </div>
                </div>
            </div>
        </div>
        <!--End of panel panel-default-->
        </form>

    </div>
</div><!--End of row-->
</div><!--End of container-fluid-->
<?php
/*
 *
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
include_once('frontend/templates/footers/regions_ajax_tpl.php');
?>

