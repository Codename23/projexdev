<?php 
/*
 * Header file
 */
$title= 'Supplier';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php');
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php');
?> 
<!--End of navigation--> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li class="active"><a href="index.php?module=supplier&action=viewAllSuppliers">All Suppliers</a></li>
            <li><a>Request Quote</a></li>
            <li><a>Schedule</a></li>
            <li><a>History</a></li>
            <li><a>SHEQ Compliance</a></li>
        </ul>
        </div>
    </div>
    <!--End of sub menu-->
    <div class="panel panel-default">
        <div class="panel-body">
		<a href="<?php echo BASE_URL . "/index.php?module=supplier&action=addSupplier"?>" class="btn btn-success">Add Supplier</a>
        </div>
    </div> 
    <!--End of panel panel-default-->
    <div class="panel panel-default">
    <div class="panel-heading">Suppliers</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Company Name</th>
                    <th>Operational Area</th>
                    <th>Contractor Type</th>
                    <th>Rating</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>

            <?php if($data['allSuppliers']): foreach($data['allSuppliers'] as $supplier): ?>
                <tr>
                    <td><?php echo $supplier['id']; ?></td>
                    <td><a href="<?php echo BASE_URL . "/index.php?module=supplier&action=viewSupplierInfo&id=" . $supplier['id'] ; ?>"><?php echo $supplier['supplier_name']; ?></a></td>
                    <td><?php echo $supplier['provience']; ?></td>
                    <td>Emergency Equipment</td>
                    <td>5.5</td>
                    <td><a href="<?php echo BASE_URL . "/index.php?module=supplier&action=viewSupplierInfo&id=" . $supplier['id'] ; ?>">Edit</a></td>
                    <td class="danger"><a href="<?php echo BASE_URL . "/index.php?module=supplier&action=deleteSupplier&id=" .$supplier['id']; ?>">Delete</a></td>
                </tr>

            <?php endforeach;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->

        </div>
    </div><!--End of row-->
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?> 