<!--panel panel-default-->
<div class="panel-group">
    <form class="form-horizontal" name="requestQuoteForm" method="post" action="">
        <div class="panel panel-default">
            <div class="panel-heading">Request Quote</div>
            <div class="panel-body">

                <div class="form-group">
                    <label class="control-label col-sm-4" for="companyName">Company Name</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="text" class="form-control" id="companyName" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="email">E-mail</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="email" class="form-control" id="email" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="telephoneNumber">Telephone Number</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="tel" class="form-control" id="telephoneNumber" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="websiteUrl">Website</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="url" class="form-control" id="websiteUrl" required>
                    </div>
                </div>
            </div>
        </div>
        <!--End of panel panel-default-->
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="serviceSpecification">System Specification</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="text" class="form-control" id="serviceSpecification" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="serviceSpecification">Service Type</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <select name="serviceType" class="form-control">
                            <option></option>
                            <option></option>
                            <option></option>
                            <option></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="details">Details</label>
                    <div class="col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="description[]" id="description" placeholder="Description">
                    </div>
                    <div class="col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Quantity">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="details">Details</label>
                    <div class="col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="description[]" id="description" placeholder="Description">
                    </div>
                    <div class="col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Quantity">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="details">Details</label>
                    <div class="col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="description[]" id="description" placeholder="Description">
                    </div>
                    <div class="col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Quantity">
                    </div>
                </div>
            </div>
        </div>
        <!--End of panel panel-default-->
        <div class="panel panel-default">
            <div class="panel-heading">SHEQ Questioner</div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="question_">Question 1</label>
                    <div class="col-lg-2 col-md-2 col-sm-8">
                        <input type="text" class="form-control" name="mytext[]" id="question_">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="question_">Question 2</label>
                    <div class="col-lg-2 col-md-2 col-sm-8">
                        <input type="text" class="form-control" name="mytext[]" id="question_">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="question_">Question 3</label>
                    <div class="col-lg-2 col-md-2 col-sm-8">
                        <input type="text" class="form-control" name="mytext[]" id="question_">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="question_">Question 4</label>
                    <div class="col-lg-2 col-md-2 col-sm-8">
                        <input type="text" class="form-control" name="mytext[]" id="question_">
                    </div>
                </div>
            </div>
        </div>
        <!--End of panel panel-default-->
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="emailTo">Send Quote To E-mail</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <select name="emailTo" class="form-control">
                            <option>Warren Windvogel</option>
                            <option>Nick Both</option>
                            <option>Nango Kala</option>
                            <option>Jeniffer Lawrence</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="emailFrom">From Name</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="text" class="form-control" id="emailFrom" value="Sunnyboy Mathole" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-success">Send Email</button>
                    </div>
                </div>
                <!--End of panel panel-default-->

            </div>
            <!--End of panel-group-->
        </div>
    </form>
</div><!--End of row-->