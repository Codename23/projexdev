<?php 
/*
 * Header file
 */
$title = 'Online Inspection Settings';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online">Upcoming</a></li>
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspect_now">Inspect Now</a></li> 
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_plan">Inspection Plan</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_create_inspection">Create Template</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_history">Inspection History</a></li> 
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_nonconformances">Non-Conformances</a></li> 
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_settings">Settings</a></li> 
        </ul>
        </div>
    </div>
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="createInspection-modal" data-toggle="modal" data-target="#createInspection-modal"><button type="button" id="createInspection" class="btn btn-success">Create Inspection</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Inspections</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Group</th>
                    <th>Inspection Type</th>
                    <th>Description</th>
                    <th>Inspection Purpose</th>
                    <th>Inspection</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
            
               if($data['allInspections']): foreach($data['allInspections'] as $inspection):
                   ?>
                            <tr>
                                <td><?php echo $inspection['sheqteam_name']; ?></td>
                                <td><?php echo $inspection['description_type']; ?></td>                                
                                <td><?php echo $inspection['specific_type']; ?></td>                                                             
                                 <td><a href="#">View</a></td>
                                 <td><a href="#">View Inspection</a></td>
                                 <td></td>
                            </tr>
                        <?php endforeach;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!-- Create Inspection Modal -->
  <div class="modal fade" id="createInspection-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Inspection</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="createInspectionForm" action="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=create_online_inspection">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="group">Group</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select name="group" class="form-control">
                        <option value selected disabled>Please select a group</option>
                        <option value="1">OHS (Health & Safety)</option>
                        <option value="2">E (Environment)</option>
                        <option value="3">Q (Quality)</option>
                        <option value="4">F (Food)</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="inspectionType">Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" name="inspectionType" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select an inspection type">
                            <option>General</option>
                            <option>Specific</option>
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="specificType">Specific Type</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" name="specificType" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a specific type">
                            <option>Resource Inspection</option>
                            <option>Departmental Inspection</option>
                            <option>Occupational</option>
                            <option>Process</option>
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="inspectionDescription">Inspection Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="inspectionDescription" id="inspectionDescription" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" id="createInspectionBtn" name="createInspectionBtn" class="btn btn-success">Create</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<script>
    $(function() {
    $("#nextInspectionDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    });
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    