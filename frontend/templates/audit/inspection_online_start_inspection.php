<?php 
/*
 * Header file
 */
$title = 'Start Online Inspection';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
$id = $_GET["id"];
?>
<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online">Upcoming</a></li>
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspect_now">Inspect Now</a></li> 
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_plan">Inspection Plan</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_create_inspection">Create Template</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_history">Inspection History</a></li> 
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_nonconformances">Non-Conformances</a></li> 
            <li><a href="">Settings</a></li> 
        </ul>
        </div>
    </div>
<style>
    .yellow-table-bg {
    background-color: #ffff6a;
    color: black;
}
</style>
<form class="form-horizontal" method="post" name="startInspectionForm" action="<?php echo BASE_URL.'/index.php?module=online_inspection&action=submitInspection&id='.$id;?>">
    <input type="hidden" id="inspection_id" name="inspection_id" value="<?php echo $id;?>"/>
    <div class="panel panel-default">
    <div class="panel-heading">Inspection</div>
    <div class="panel-body"> 
        <div class="form-group">
            <label class="control-label col-sm-4" for="group">Group</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" name="group" id="group" value="Quality" readonly> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="inspectionDescription">Inspection Description</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" name="inspectionDescription" id="inspectionDescription" value="House Keeping" readonly> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="frequency">Frequency</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" name="frequency" id="frequency" value="Monthly" readonly> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="inspector">Inspector</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" name="inspector" id="inspector" value="Nick Botha" readonly> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="purposeOfInspection">Purpose OF Inspection</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <textarea rows="5" class="form-control" name="purposeOfInspection" id="purposeOfInspection" placeholder="This register has been designed to maintain the condition in the workplace of the employer in a clean, safe and risk free condition" readonly></textarea>
            </div>
        </div>
         
    </div>
    </div>
    
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Legal/ISO/Procedural Reference</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php   
                             $counter = 0;
                        if($data['allIso']): foreach($data['allIso'] as $iso):
                            $counter++;
                            ?>
                                     <tr>
                                         <td><?php echo $counter; ?></td>
                                         <td><?php echo $iso['title']; ?></td>                                
                                         <td><?php echo $iso['description']; ?></td>                                                             
                                          <td></td>
                                          <td></td>
                                          <td></td>
                            </tr>
                        <?php endforeach;endif; ?>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Asset Register</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Department</th>
                                    <th>Category</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                    <th>No</th>
                                    <th>Present</th>
                                    <th>Not Present</th>
                                </tr>
                            </thead>
                            <tbody>
                                   <?php
                $count = 0; 
               // echo count($data['assets']);
               if($data['allAssets']): foreach($data['allAssets'] as $assetDetail):  
$count++;                   
               $name = "asset".$count;
                      echo "<tr>                              
                              <td>" . $assetDetail['department_name'] . "</td>
                              <td>" . $assetDetail['categoryType'] . "</td>
                              <td>" . $assetDetail['assetType'] . "</td>
                              <td><a href='index.php?module=assets&action=view&id='{$assetDetail['id']}>{$assetDetail['description']}</a></td>
                              <td>{$assetDetail['company_asset_number']}</td>           
                              <td>";?>
                               <input type='checkbox' <?php
                               if($assetDetail["present"] == 1)
                               {
                                   echo "checked";
                               }
                               ?> class='chkbox' value='1' location='Asset' checkid = '<?php echo $assetDetail['assid'];?>' name='<?php echo $name;?>'></td>
                              <td><input type='checkbox' <?php
                               if($assetDetail["present"] == 2)
                               {
                                   echo "checked";
                               }
                               ?> class='chkbox' value='2' location='Asset' checkid = '<?php echo $assetDetail['assid'];?>' name='<?php echo $name;?>'></td>                             
                            </tr> <?php               
                          $count ++;                         
                   endforeach;endif; 
            ?>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Materials Register</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Department</th>
                                    <th>Category</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                    <th>No</th>
                                    <th>Present</th>
                                    <th>Not Present</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
               $counter = 0;        
               global $objDepartment;              
               if($data['allMaterials']): foreach($data['allMaterials'] as $material):
                   $counter++;
                $name = "mat".$counter;
                   ?>
                            <tr>
                                <td><?php echo $objDepartment->getAllDepartmentsByGroupId($material["department_use_id"]);?></td>  
                                <td><?php echo $material['category']; ?></td>
                                <td><?php echo $material['type']; ?></td>                                
                                <td><?php echo $material['description']; ?></td>                                
                                <td><?php echo $material['inventory_no']; ?></td>                                                                
                                <td><input type="checkbox" <?php
                               if($material["present"] == 1)
                               {
                                   echo "checked";
                               }
                               ?> location='Material' class="chkbox" checkid = '<?php echo $material['assid'];?>' name="<?php echo $name;?>" value="1"></td>
                                <td><input type="checkbox" <?php
                               if($material["present"] == 2)
                               {
                                   echo "checked";
                               }
                               ?> location='Material' class="chkbox" checkid = '<?php echo $material['assid'];?>' name="<?php echo $name;?>" value="2"></td>   
                            </tr>
                        <?php endforeach;endif; 
                        
                       
                        ?>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Critical Questions</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th colspan="3">Critical Questions</th>
                                    
                                    <th>Status</th>
                                    <th>Comments</th>
                                    <th colspan="2">Photo</th>
                                </tr>
                            </thead>
                                      <tbody>
                                     <?php   
                                     $counter = 0;
                                     $total = 0;
                                     $questionsChecked = 0;
                                     $totalQuests = count($data["allQuestions"]);
                                     
                        if($data['allCategories']): foreach($data['allCategories'] as $category):
                            ?>  <tr>
                                    <td><?php echo $category["category"];?></td>                                 
                                    <td></td>
                                    <td></td>
                                    <td></td><td></td><td></td>
                                </tr>
                                  <?php   
                                  
                                 if($data['allQuestions']): foreach($data['allQuestions'] as $questions):
                                     if($questions["category_id"] == $category["id"])
                                     {
                                         $counter++;
                                ?>
                                <tr>
                                    <td></td>
                                    <td><?php echo $counter;?></td>
                                    <td><?php echo $questions["question"];?></td>
                                    <?php
                                    if($questions["status"] == "Compliant")
                                    {
                                        echo "<td class='bg-success'>{$questions['status']}</td>";
                                        $questionsChecked++;
                                    }
                                    if($questions["status"] == "Not Compliant")
                                    {                                                                                
                                        echo "<td class='bg-danger'>{$questions['status']}</td>";
                                    } 
                                    if($questions["status"] == "")
                                    {
                                        ?>
                                       <td class="yellow-table-bg"><a qid="<?php echo $questions["id"];?>" class="check_question" href="#check-modal" data-toggle="modal" data-target="#check-modal">Check</a></td>
                                            <?php
                                    }
                                    ?>                                    
                                    <td><a href="#">View</a></td>
                                    <td><a href="#">View</a></td>
                                </tr>  
                                <?php 
                                     }
                                endforeach;endif; ?>
                                
                        <?php endforeach;endif;
                        $total = $questionsChecked / $totalQuests ;
                        $percentage = round((float)$total * 100 ) . '%';                         
                        ?>
                                                         
                            </tbody>
                        </table>    
                    </div>
                </div>
                <div class="panel-footer">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="purposeOfInspection">Score</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control bg-success" style="background: #00a65a;color:#fff;" name="inspection_score" id="inspection_score" value="<?php echo $percentage;?>" readonly> 
                            </div>
                        </div>   
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-8">
        <button type="submit" name="startInspectionBtn" id="startInspectionBtn" class="btn btn-success">Submit</button>
        </div>
    </div>
</form>


<!-- Check Modal -->
  <div class="modal fade" id="check-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Check</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="checkForm" enctype="multipart/form-data" action="<?php echo BASE_URL.'/index.php?action=addInspectionCheckStatus&module=online_inspection&id='.$id;?>">
                <input type="hidden" id="inspection_id" value="<?php echo $_GET["id"];?>" name="inspection_id"/>
                <input type="hidden" id="complaint" name="complaint"/>
                <input type="hidden" id="quest_id" name="quest_id"/>
                <input type="hidden" id="department_" name="department_field" value="<?php echo $_GET["dep"];?>"/>
                <input type="hidden" id="group_field" name="group_field" value="<?php echo $_GET["group"];?>"/>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="status">Status</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <button type="button" id="compliant" class="btn btn-default compliant">Compliant</button>
                        <button type="button" id="nonCompliant" class="btn btn-default noncompliant">Non-Compliant</button>
                    </div>
                </div>
                
                <div id="noncompliantComments">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="resourceInvolved">Resource Involved</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" required="true" name="check_resource" id="check_resource" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a resource">
                            <?php
                                 foreach($data['allResources'] as $group)
                                {           
                                    echo "<option value='{$group["id"]}'>{$group['as_name']}</option>";                                   
                                }                                
                                ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="comments">Comments</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="check_comments" required="true" id="check_comments"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="photo">Photo Upload</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="file" name="photo" id="photo"></input>
                    </div>
                </div>
                <div class="form-group" >
                    <label class="control-label col-sm-12 text-center " for="takePhoto" style="
    text-align: center;
"><input type="button" value="Access Camera" onClick="setup(); $(this).hide().next().show();">
                        <input type="button" value="Take Snapshot" onClick="take_snapshot()" style="display:none">                   </label>
                    <script language="JavaScript">
		Webcam.set({
			width: 320,
			height: 240,
			image_format: 'jpeg',
			jpeg_quality: 90
		});
	</script>
        <br/>
                    <div class="col-lg-12 col-md-4 col-sm-12">
                                
                       <div id="my_camera" style="height:200px" class="col-sm-6"></div>
                       <div id="results" style="height:250px" class="col-sm-6"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class=" col-sm-offset-4 col-sm-8">
                    <button type="submit" id="addCheckButtonNon" name="addCheckButtonNon" class="btn btn-success">Add</button>
                    </div>
                </div>    
                </div>
                <div class="form-group" id="btnAddComplaint" style="display:none;">
                <div class=" col-sm-offset-4 col-sm-8">
                <button type="button" id="addCheckButton" class="btn btn-success">Add</button>
                </div>
            </div>
            
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<script type="text/javascript" src="<?php echo BASE_URL;?>/webcamjs-master/webcam.min.js"></script>
	<!-- Code to handle taking the snapshot and displaying it locally -->
	<script language="JavaScript">
		function setup() {
			Webcam.reset();
			Webcam.attach( '#my_camera' );
		}
		
		function take_snapshot() {
			// take snapshot and get image data
			Webcam.snap( function(data_uri) {
				// display results in page
				document.getElementById('results').innerHTML = 
					'<img class="img-responsive" src="'+data_uri+'"/>';
			} );
		}
	</script>
	
<script>
$(document).ready(function(){         
   
    $('.compliant').click(function() {
       $(this).removeClass("btn-default");
        $(this).addClass("btn-success");
        $('.noncompliant').removeClass("btn-danger");
        $('.noncompliant').addClass("btn-default");
        $("#btnAddComplaint").show();
    });

    $('.noncompliant').click(function() {
       $(this).removeClass("btn-default");
        $(this).addClass("btn-danger");
        $('.compliant').removeClass("btn-success");
        $('.compliant').addClass("btn-default");
        $("#btnAddComplaint").hide();
    });
        
 });
  var hasComplained = 0;
  var question_id = 0;
  var rowObj;
  
 $("#noncompliantComments").hide();
   
    $(".noncompliant").click(function(){
    $("#noncompliantComments").show();
    hasComplained = 0;
    });
    $(".compliant").click(function(){
    $("#noncompliantComments").hide();
    hasComplained = 1;
    });
    
    //script handling all the check boxes
    $("input:checkbox").on('click', function() {
  // in the handler, 'this' refers to the box clicked on
    var $box = $(this);
    if ($box.is(":checked")) 
    {
      // the name of the box is retrieved using the .attr() method
      // as it is assumed and expected to be immutable
      var group = "input:checkbox[name='" + $box.attr("name") + "']";
      // the checked state of the group/box on the other hand will change
      // and the current value is retrieved using .prop() method
        $(group).prop("checked", false);
        $box.prop("checked", true);
        var id = $($box).attr("checkid");
        var loc = $($box).attr("location");
        var val = $($box).val();
        
        var inspection = $("#inspection_id").val();
        var ur = "<?php echo BASE_URL;?>/index.php?module=online_inspection&action=addInspectionCheck&id="+id;        
        $.ajax({
          url:ur,
          type:"POST",
          data:{inspection_id:inspection,asset_id:id,present:val,location:loc},
          success:function(result)
          {
             if(parseInt(result) == 200)
             {
               console.log("updated");
             }
          }
       });
    } 
    else 
    {
      $box.prop("checked", false);
    }
    
  
    
  });
  $(".check_question").on("click",function()
  {
      question_id = $(this).attr("qid");
      $("#quest_id").val(question_id);
      rowObj = $(this);
  });
    
  
  
  
    //add check button
    $("#addCheckButton").on("click",function()
    {
        var id = $("#inspection_id").val();
        var ur = "<?php echo BASE_URL;?>/index.php?module=online_inspection&action=addCheck&id="+id;
        qid  = question_id;        
        $.ajax({
          url:ur,
          type:"POST",
          data:{inspection_id:id,question_id:qid,answer:hasComplained},
          success:function(result)
          {
             if(parseInt(result) == 200)
             {
                 if(parseInt(hasComplained) == 1)
                 {                     
                    $(rowObj).html("Compliant");
                    //perform calculation
                    location.reload();
                    $(rowObj).parent().removeClass("yellow-table-bg").addClass("bg-success");
                   // $("#collapseFour").show();
                    
                }
                else
                {
                    $(rowObj).html("Not Compliant");
                    $(rowObj).parent().removeClass("yellow-table-bg").addClass("bg-danger");
                }
             }
          }
       });
    });
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    