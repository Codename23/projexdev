<?php 
/*
 * Header file
 */
$title = 'Inspection Online';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online">Upcoming</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspect_now">Inspect Now</a></li> 
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_nonconformances">Non-Conformances</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_plan">Inspection Plan</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_create_inspection">Create Template</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_history">Inspection History</a></li> 
             
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_settings">Settings</a></li> 
        </ul>
        </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Search Upcoming Inspections</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>I.T</option>
                <option>HR</option>
                <option>Finance</option>
            </select>
        </div>
        </div>
            
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Group</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more groups"data-header="Close">
                <option value="1">OHS (Health & Safety)</option>
                <option value="2">E (Environment)</option>
                <option value="3">Q (Quality)</option>
                <option value="4">F (Food)</option>
            </select>
        </div>
        </div>
        
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Inspection Type</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types"data-header="Close">
                <option value="1">Resource</option>
                <option value="2">General</option>
            </select>
        </div>
        </div> 
            
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Status</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more status types" data-header="Close">
                <option value="1">Due in a month</option>
                <option value="2">Due in 2 months</option>
                <option value="3">Overdue</option>
                <option value="4">Good</option>
            </select> 
        </div>
        </div>
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Upcoming Inspection</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Group</th>
                    <th>Department</th>
                    <th>Description</th>
                    <th>Inspector</th>
                    <th>Due Date</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                      <?php            
                      $current_date = new DateTime("now");
               if($data['allOnlineInspectionsUpcoming']): foreach($data['allOnlineInspectionsUpcoming'] as $inspection):
                   $next_date = new DateTime($inspection["next_inspection_date"]);
                  $interval = date_diff($current_date, $next_date);
                  $STR = $interval->format('%R%a');
                   ?>
                            <tr>
                                <td><?php echo $inspection['sheqteam_name']; ?></td>
                                <td><?php echo $inspection['department_name']; ?></td>                                
                                <td><?php echo $inspection['inspection_description']; ?></td>
                                <td><?php echo $inspection['emp']; ?></td>
                                <td><?php echo $inspection['next_inspection_date']; ?></td>   
                                <?PHP
                                if(0 === strpos($STR, '-'))
                                {
                                    $str = str_replace("-","",$STR);
                                    if($str > 0)
                                    {
                                        //due                                    
                                        echo "<td class='text-danger bg-danger'>Overdue in {$str} days</td>";
                                    }
                                    else
                                    {
                                        echo "<td class='text-success bg-success'>Good</td>";
                                    }
                                }
                                if(0 === strpos($STR, '+'))
                                {
                                    //on time
                                    $str = str_replace($STR, "+","");
                                    if($str  === 1)
                                    {
                                        echo "<td class='text-warning bg-warning'>Due in $STR day</td>";
                                    }
                                    else
                                    {
                                        echo "<td class='text-warning bg-warning'>Due in $STR days</td>";
                                    }
                                }
                                ?>                                
                                <td><a href="<?php echo BASE_URL.'/index.php?action=inspection_online_start_inspection&module=online_inspection&id='.$inspection["online_id"];?>"><?php echo $inspection["online_id"];//Do Inspection?></a></td>                               
                                <td></td>
                            </tr>
                        <?php endforeach;endif; ?>          
            </tbody>
        </table>
    </div>
    </div>
    </div>

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    