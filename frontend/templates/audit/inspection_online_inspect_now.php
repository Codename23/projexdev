<?php 
/*
 * Header file
 */
$title = 'Inspection Online Now';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online">Upcoming</a></li>
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspect_now">Inspect Now</a></li> 
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_nonconformances">Non-Conformances</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_plan">Inspection Plan</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_create_inspection">Create Template</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_history">Inspection History</a></li>              
            <li><a href="">Settings</a></li> 
        </ul>
        </div>
    </div>

<div class="panel panel-default">
    <div class="panel-heading">Inspect Now</div>
    <div class="panel-body">
        <form class="form-horizontal" method="post" name="inspectNowForm" action="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_start_inspection">
            <input type="hidden" id="url" value="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_start_inspection"/>
        <div class="form-group">
            <label class="control-label col-sm-4" for="group">Group</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select name="group" id="group" class="form-control multiselect_form">
                <option value selected disabled>Please select a group</option>
                <option value="1">OHS (Health & Safety)</option>
                <option value="2">E (Environment)</option>
                <option value="3">Q (Quality)</option>
                <option value="4">F (Food)</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="description">Description</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect allInspections" id="allInspections" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a description">
                
                </select> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="department">Department</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" id="dep" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                <?php
                                   foreach($data['allDepartments'] as $group)
                                   {
                                       echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                                   }
                                 ?>
                </select> 
            </div>
        </div> 
        <div class="form-group">
            <label class="control-label col-sm-4" for="inspector">Inspector</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a inspector">
                      <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>        
                </select> 
            </div>
        </div> 
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
            <button type="button" id="inspectNowBtn" class="btn btn-success">Start</button>
            </div>
        </div>
    </form>
    </div>
</div>
</div>
</div><!--End of container-fluid-->
<script>
$(document).ready(function()
{
   $("#inspectNowBtn").on("click",function()
   {
       if($("#allInspections").val() != "")
       {
           var group_st = "&group="+$("#group").val();
           var department = "&dep="+$("#dep").val();
            var ur = $("#url").val() + "&id="+$("#allInspections").val()+group_st+department;
            window.location = ur;
        }
        else{
            alert("Please select inspection description");
        }
   });  
   
   $("#group").on("change",function()
   {
      getMoreInfoDropDownList(this,"#allInspections","<?php echo BASE_URL;?>/index.php?module=online_inspection&action=get_list_of_templates");
   });
});
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    