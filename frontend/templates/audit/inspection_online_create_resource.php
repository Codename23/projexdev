<?php 
/*
 * Header file
 */
$title = 'Start Online Inspection Resource';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
//set the inspection_id
global $objOnlineInspections;
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online">Upcoming</a></li>
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspect_now">Inspect Now</a></li> 
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_plan">Inspection Plan</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_create_inspection">Create Template</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_history">Inspection History</a></li> 
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_nonconformances">Non-Conformances</a></li> 
            <li><a href="">Settings</a></li> 
        </ul>
        </div>
    </div>

<form class="form-horizontal" method="post" name="createInspectionResourceForm" action="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=submit_inspection">
    <input type="hidden" name="inspection_id" value="<?php echo $_GET["inspection_id"];?>"/>
    <div class="panel panel-default">
    <div class="panel-heading">Inspection</div>
    <div class="panel-body"> 
        <div class="form-group">
            <label class="control-label col-sm-4" for="group">Group</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" name="group" id="group" value="<?php echo $objOnlineInspections::$getGroup;?>" readonly> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="inspectionDescription">Inspection Description</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" name="inspectionDescription" id="inspectionDescription" value="<?php echo $objOnlineInspections::$getInspectionDescription;?>" readonly> 
            </div>
        </div>
    </div>
    </div>
    
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Setup</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="purposeOfDescription">Inspection Details</label>
                        <div class="col-lg-4 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="purposeOfDescription" id="purposeOfDescription" required placeholder=""><?php echo $objOnlineInspections::$getDetails;?></textarea>
                        </div>
                    </div>
                    <div class="form-group" id="loadInspectionFrequency">
                        <label class="control-label col-sm-4" for="frequency">Frequency</label>
                        <div class="col-lg-4 col-md-4 col-sm-8">
                        <select class="form-control" name="frequency" id="frequency" required>
                            <option value selected disabled>Please select a frequency</option>
                            <?php
                            $frequency_list = array("Daily","Weekly","Monthly","Every 6 Months","Yearly","Every 2 Years");
                            $frequency = $objOnlineInspections::$getFrequency;
                            $counter = 0;
                            foreach($frequency_list as $freq)
                            {
                                $counter++;
                                if($counter == $frequency)
                                {
                                    echo "<option selected value='{$counter}'>{$freq}</option>";   
                                }
                                else
                                {
                                    echo "<option value='{$counter}'>{$freq}</option>";                                   
                                }
                            }
                            ?>

                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="linkAssetTypes">Link Asset Type</label>
                         <input type="hidden" name="assets_selected" id="assets_selected"/>
                        <div class="col-lg-4 col-md-4 col-sm-8">                     
                            <select class="selectpicker form-multiselect" id="search_assets" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                                <?php
                                 foreach($data['allResources'] as $group)
                                {           
                                    echo "<option value='{$group["id"]}'>{$group['as_name']}</option>";                                   
                                }                                
                                ?>
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="linkMaterialTypes">Link Material Type</label>
                        <?php
                         $haystack = $objOnlineInspections->getAllMaterials();
                         $list = array();
                        foreach($haystack as $stack)
                        {
                            $list[] = $stack["material_id"];
                        }
                        ?>
                         <input type="hidden" name="materials_selected" id="materials_selected"/>
                        <div class="col-lg-4 col-md-4 col-sm-8">
                            <select class="selectpicker form-multiselect" id="search_materials" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                                  <?php
                                         
                                           foreach($data['AllMaterials'] as $pro)
                                           {
                                               if(in_array($pro["material_id"], $list))
                                               {
                                                    echo "<option selected value='{$pro["id"]}'>{$pro['description']}</option>";
                                               }
                                               else
                                               {
                                                   echo "<option value='{$pro["id"]}'>{$pro['description']}</option>";
                                               }
                                           }
                                           ?>                            
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Legal/ISO/Procedural Reference</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#createLegalISO-modal" data-toggle="modal" data-target="#createLegalISO-modal"><button type="button" id="add" class="btn btn-success">Add</button></a>
                        </div>
                    </div>
  
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                             <?php   
                             $counter = 0;
                        if($data['allIso']): foreach($data['allIso'] as $iso):
                            $counter++;
                            ?>
                                     <tr>
                                         <td><?php echo $counter; ?></td>
                                         <td><?php echo $iso['title']; ?></td>                                
                                         <td><?php echo $iso['description']; ?></td>                                                             
                                          <td></td>
                                          <td></td>
                                          <td></td>
                            </tr>
                        <?php endforeach;endif; ?>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Critical Categories</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#createCriticalCategories-modal" data-toggle="modal" data-target="#createCriticalCategories-modal"><button type="button" id="addCategories" class="btn btn-success">Add Categories</button></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php   
                             $counter = 0;
                        if($data['allCategories']): foreach($data['allCategories'] as $category):
                            $counter++;
                            ?>
                                     <tr>
                                         <td><?php echo $category['category']; ?></td>                                
                                         <td><span class="glyphicon glyphicon-pencil"></span></td>                                                             
                                         <td><span class="glyphicon glyphicon-trash"></span></td>                                                             
                            </tr>
                        <?php endforeach;endif; ?>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Questions</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#createQuestions-modal" data-toggle="modal" data-target="#createQuestions-modal"><button type="button" id="addQuestion" class="btn btn-success">Add Question</button></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>No</th>
                                    <th>Questions</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                     <?php   
                                     $counter = 0;
                        if($data['allCategories']): foreach($data['allCategories'] as $category):
                            ?>  <tr>
                                    <td><?php echo $category["category"];?></td>                                 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                  <?php   
                                 if($data['allQuestions']): foreach($data['allQuestions'] as $questions):
                                     if($questions["category_id"] == $category["id"])
                                     {
                                         $counter++;
                                ?>
                                <tr>
                                    <td></td>
                                    <td><?php echo $counter;?></td>
                                    <td><?php echo $questions["question"];?></td>
                                    <td><span class="glyphicon glyphicon-trash"></span></td>
                                </tr>  
                                <?php 
                                     }
                                endforeach;endif; ?>
                                
                        <?php endforeach;endif; ?>
                                                         
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-8">
        <button type="submit" name="createInspectionResourceBtn" id="createInspectionResourceBtn" name="inspection" class="btn btn-success">Add</button>
        </div>
    </div>
</form>

<!-- Create Inspection Modal -->
  <div class="modal fade" id="createLegalISO-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Legal ISO</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="createLegalISOForm" action="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=create_legal_iso">
                <input type="hidden" name="inspection_id" value="<?php echo $_GET["inspection_id"];?>"/>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="iso_title">Title</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="iso_title" id="iso_title" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="iso_description">Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="iso_description" id="iso_description" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" id="createISO" name="createISO" class="btn btn-success">Create</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->


<!-- Create Critical Category Modal -->
  <div class="modal fade" id="createCriticalCategories-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Critical Categories</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="createCriticalCategoriesForm" action="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=create_categories">
                <input type="hidden" name="inspection_id" value="<?php echo $_GET["inspection_id"];?>"/>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="category_title">Category</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="category_title" id="category_title" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" id="createCategory" name="createCategory" class="btn btn-success">Create</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->


<!-- Create Inspection Modal -->
  <div class="modal fade" id="createQuestions-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Question</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="createQuestionsForm" action="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=create_inspection_question">
                <input type="hidden" name="inspection_id" value="<?php echo $_GET["inspection_id"];?>"/>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="category_list">Category</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="form-control" name="category_list" id="category_list">
                                 <?php   
                        if($data['allCategories']): foreach($data['allCategories'] as $category):
                            ?>     
                                         <option value="<?php echo $category['id']; ?>"><?php echo $category['category']; ?></option>                                                                                      
                        <?php endforeach;endif; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="category_quest">Question</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea class="form-control" name="category_quest" id="category_quest" required placeholder=""></textarea> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" id="createQuestion" name="createQuestion" class="btn btn-success">Create</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->


</div>
</div><!--End of container-fluid-->
<script>
    $(document).ready(function()
    {
           $('.selectpicker').on('change', function(){

            var selected = $(this," option:selected").val();
            focus_dropdown = $(this).parent().children('select').attr('id');
            switch(focus_dropdown)
            {
                case 'search_assets':
                    $("#assets_selected").val(selected);
                    break;       
                case 'search_materials':
                    $("#materials_selected").val(selected);
                    break;
            }
           // console.log(focus_dropdown);
        }); 
    });
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    