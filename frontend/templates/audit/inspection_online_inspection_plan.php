<?php 
/*
 * Header file
 */
$title = 'Online Inspection Plan';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online">Upcoming</a></li>
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspect_now">Inspect Now</a></li> 
             <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_nonconformances">Non-Conformances</a></li> 
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_plan">Inspection Plan</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_create_inspection">Create Template</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_history">Inspection History</a></li> 
           
            <li><a href="">Settings</a></li> 
        </ul>
        </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#addInspection-modal" data-toggle="modal" data-target="#addInspection-modal"><button type="button" id="addInspection" class="btn btn-success">Add Inspection To Plan</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Inspection Plan</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Group</th>
                    <th>Description</th>
                    <th>Department</th>
                    <th>Next Inspection</th>
                    <th>Inspector</th>
                    <th>View Inspection</th>
               
                </tr>
            </thead>
            <tbody>
                  <?php            
               if($data['allInspectionPlans']): foreach($data['allInspectionPlans'] as $inspection):
                   ?>
                            <tr>
                                <td><?php echo $inspection['sheqteam_name']; ?></td>                            
                                <td><?php echo $inspection['inspection_description']; ?></td>
                                <td><?php echo $inspection['department_name']; ?></td>
                                <td><?php echo $inspection['next_inspection_date']; ?></td>   
                                <td><?php echo $inspection['emp']; ?></td>   
                                <td><a href="#<?php echo $inspection["inspection_id"];?>">View Inspection</a></td>                               
                            </tr>
                        <?php endforeach;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!-- Add Inspection Modal -->
  <div class="modal fade" id="addInspection-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Inspection To Plan</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addInspectionForm" action="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=create_inspection_online_inspection_plan">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="group">Group</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select name="group" class="form-control">
                        <option value selected disabled>Please select a group</option>
                        <option value="1">OHS (Health & Safety)</option>
                        <option value="2">E (Environment)</option>
                        <option value="3">Q (Quality)</option>
                        <option value="4">F (Food)</option>
                        </select>
                    </div>
                </div>
                    <div class="form-group">
                    <label class="control-label col-sm-4" for="inspectionDepartment">Department</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" name="inspectionDepartment" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" required title="Select a department">
                       <?php
                                   foreach($data['allDepartments'] as $group)
                                   {
                                       echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                                   }
                                 ?>
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="description">Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" name="description" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a description">
                                <?php
                            foreach($data['allInspections'] as $group)
                            {
                                echo "<option value='{$group["id"]}'>{$group['inspection_description']} {$group['lastname']}</option>";
                            }?>                         
                        </select> 
                    </div>
                </div>
                <div class="form-group" id="loadInspectionFrequency">
                    <label class="control-label col-sm-4" for="loadInspectionFrequency">Frequency</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="form-control" name="loadInspectionFrequency" id="loadInspectionFrequency" required>
                        <option value selected disabled>Please select a frequency</option>
                        <option value="1">Daily</option>
                        <option value="2">Weekly</option>
                        <option value="3">Monthly</option>
                        <option value="4">Every 6 Months</option>
                        <option value="5">Yearly</option>
                        <option value="6">Every 2 Years</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="nextInspectionDate">Next Inspection Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="nextInspectionDate" id="nextInspectionDate" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="inspector">Inspector</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" name="inspector" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select an inspector">
                            <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>                            
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" id="addInspectionBtn" name="addInspectionBtn" class="btn btn-success">Add To Plan</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<script>
    $(function() {
    $("#nextInspectionDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    });
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    