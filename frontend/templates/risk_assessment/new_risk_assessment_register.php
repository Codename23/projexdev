<?php 
/*
 * Header file
 */
$title = 'Register Risk Assessment';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=doRiskAssessment">Do Risk Assessment</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RequestedRiskAssessment">Requested</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RegisterRiskAssessment">Register</a></li>
        <!--<li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=ControlsRiskAssessment">Risk Controls</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Setup</a></li>
    </ul>
    </div>
</div>   
<!--End of sub menu-->

<div class="panel panel-default">
<div class="panel-heading">Search Register</div>
<div class="panel-body">
<form class="form-inline" method="post" name="search" action="">
    <div class="col-md-12" style="margin-bottom: 10px;">
    <div class="col-md-4 col-sm-4">
    <div class="form-group">
        <p><b>Risk Assessment Types</b></p>
        <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types"data-header="Close">
            <option value="1">Maintenance</option>
            <option value="2">Occupational</option>
            <option value="3">Resource</option>
            <option value="4">Department Baseline</option>
            <option value="5">Process</option>
            <option value="6">Contractor</option>
            <option value="7">Emergency</option>
        </select>
    </div>
    </div>

    <div class="col-md-4 col-sm-4">
    <div class="form-group">
        <p><b>Department</b></p>
        <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
            <option>I.T</option>
            <option>Human Resources</option>
            <option>Marketing</option>
            <option>Support and Services</option>
        </select>
    </div>
    </div>
        
    <div class="col-md-4 col-sm-4">
    <div class="form-group">
        <p><b>Risk Assessor</b></p>
        <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more risk assessors"data-header="Close">
            <option>Nick Botha</option>
            <option>Aldo van Zyl</option>
            <option>Bianca le Roux</option>
        </select>
    </div>
    </div>
    </div>
    <div style="margin-left: 30px;">
    <div class="form-group">
        <input type="submit" class="btn btn-success" value="Search">
    </div>
    </div>
</form>
</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Baseline</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>RA No</th>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Department</th>
                    <th>Risk Assessor</th>
                    <th>Links</th>
                    <th>Controlled</th>
                    <th>Above 100 Scores</th>
                    <th>Revision Date</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                global $objRiskAssessment;
                
               if($data['allRegistered']): foreach($data['allRegistered'] as $nc):
                   ?>
                            <tr>
                                <td><?php echo $nc['start_id']; ?></td>
                                <td><?php echo $nc['date_submitted']; ?></td>
                                <td><?php echo $nc['risk_template_id']; ?></td>                                                                                                                                                         
                                <td><?php echo $nc['risk_description']; ?></td>
                                <td><a  class="view_departments" department_ids="<?php echo $nc["departments_list"];?>" href="#departments-modal" data-toggle="modal" data-target="#departments-modal">View</a></td>
                                <td><?php echo $nc['assessor'];?></td>
                                <td><a class="view_links" occupation_list="<?php echo $nc["occupation_list"];?>" resource_list="<?php echo $nc["resource_list"];?>" href="#links-modal" data-toggle="modal" data-target="#links-modal">View</a></td>
                                
                                    <?php
                                      $rec = $objRiskAssessment->getRisksControlled($nc["start_id"]);
                                      $questions = $rec[0]["totalQuestions"];
                                      $controlled = $rec[0]["totalControlled"];
                                      if($controlled > 0)
                                      {
                                          echo '<td class="green-table-bg"><a href="#controls-modal" data-toggle="modal" data-target="#controls-modal" class="view_risk_controls" start_id="'.$nc["start_id"].'" style="color:#fff;"><span class="glyphicon glyphicon-wrench"></span> '.$controlled.' / '.$questions.'</a></td>';
                                      }
                                      else
                                      {
                                        echo '<td class="yellow-table-bg"><a start_id="'.$nc["start_id"].'"><span class="glyphicon glyphicon-lock"></span> '.$controlled.' / '.$questions.'</a></td>';
                                      }
                                    ?></td>
                                <?php
                                $total = $objRiskAssessment->getAllRisksAboveSetting($nc["start_id"]);
                                $above = $total[0]["totalRisks"];
                                if($above > 0)
                                {
                                    echo "<td class='red-table-bg'>$above</td>";
                                }
                                else
                                {
                                    echo "<td class='bg-success'>$above</td>";
                                }
                                ?>
                                <td>12/07/2018</td>
                                <td><a href="">View</a></td>
                                <td><a href="">Revise</a></td>
                            </tr>
                 <?php endforeach;endif;                  
              ?>             
            </tbody>
        </table>    
    </div>
    </div>
    </div>


<!-- controls_table Modal -->
  <div class="modal fade" id="controls-modal" role="dialog" >
    <div class="modal-dialog" style="width:90% !important;">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Risk Controls</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table width="100%" class="controls_table table table-hover">
                     <thead>
                 <tr>
                    <th>Risk No</th>
                    <th>Photo</th>
                    <th>Hazard Types</th>
                    <th>Hazard Description</th>
                    <th>Risk Types</th>
                    <th>Risk Description</th>
                    <th>Score</th>
                    <th>Controls</th>
                    <th>Control Description</th>
                    <th>Control In Place</th>
                    <th>Score After Control</th>
                    <th>Actions Taken</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>                
            </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->


<!-- Links Modal -->
  <div class="modal fade" id="links-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Links</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table width="100%" class="links_table table table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Link</th>
                            <th>Descriptions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
<!-- Notes Modal -->
  <div class="modal fade" id="view_risk_notes-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Control Notes</h4>
        </div>
        <div class="modal-body">
            <p class="risk_notes_section"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
<!-- Departments Modal -->
  <div class="modal fade" id="departments-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Departments</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table width="100%" class="departments_table table table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Departments</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>IT</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Welding</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Finance</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<script>
 $(document).ready(function()
 {
    $(".view_departments").click(function()
    {
        var list_departments = $(this).attr('department_ids');        
        $(".departments_table tbody tr").empty();
        $.ajax({
            url:'<?php echo BASE_URL . "/index.php?module=risk_assessment&action=get_list_departments";?>',
            type:'POST',
            data:{departments:list_departments},
            dataType: 'json',
            success:function(result)
            {
             var rs = JSON.stringify(result);
             var action = JSON.parse(rs);
                
             $(action).each(function(i,val)
              {        
                  var row = "<tr><td>"+val.num+"</td><td>"+val.department+"</td><td></tr>";                      
                  $(".departments_table tbody").append(row);
              });                
            }
        });            
    });
    
     $(".view_risk_controls").click(function()
    {
        var obj = $(this);
        $(".controls_table tbody tr").removeClass("active");
        $(obj).addClass("active");
        var start_id = $(this).attr('start_id');           
        $(".controls_table tbody tr").empty();
        $.ajax({
            url:'<?php echo BASE_URL . "/index.php?module=risk_assessment&action=get_list_controls_list";?>',
            type:'POST',
            data:{id:start_id},
            dataType: 'json',
            success:function(result)
            {
             var rs = JSON.stringify(result);
             var action = JSON.parse(rs);
                
             $(action).each(function(i,val)
              {        
                  var row = "<tr><td>"+val.riskNo+"</td><td>"+val.photo+"</td><td>"+val.hazard_type+"</td><td>"+val.hazard_description+"</td><td>"+val.risk_type+"</td><td>"+val.risk_description+"</td><td>"+val.score+"</td><td>"+val.control+"</td><td>"+val.control_description+"</td><td>"+val.control_in_place+"</td><td>"+val.new_score+"</td><td>"+val.action_taken_link+"</td><td></td></tr>";                      
                  $(".controls_table tbody").append(row);
              });                
            }
        });            
    });
    //get links
    $(".view_links").click(function()
    {
        var occupation_list = $(this).attr('occupation_list');        
        var resource_list = $(this).attr('resource_list');
        
        $(".links_table tbody tr").empty();
        $.ajax({
            url:'<?php echo BASE_URL . "/index.php?module=risk_assessment&action=get_list_links";?>',
            type:'POST',
            data:{occ:occupation_list,res:resource_list},
            dataType: 'json',
            success:function(result)
            {
             var rs = JSON.stringify(result);
             var action = JSON.parse(rs);
                
             $(action).each(function(i,val)
              {        
                  var row = "<tr><td>"+val.num+"</td><td>"+val.link+"</td><td>"+val.description+"</td><td></tr>";                      
                  $(".links_table tbody").append(row);
              });                
            }
        });            
    });
 });
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    