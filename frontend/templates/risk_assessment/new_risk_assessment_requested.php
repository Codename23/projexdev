<?php 
/*
 * Header file
 */
$title = 'Requested Risk Assessment';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=doRiskAssessment">Do Risk Assessment</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RequestedRiskAssessment">Requested</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RegisterRiskAssessment">Register</a></li>
        <!--<li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=ControlsRiskAssessment">Risk Controls</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Setup</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

<div class="panel panel-default">
    <div class="panel-heading">Search Upcoming Risk Assessment</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Priority</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more priority"data-header="Close">
                <option>Low</option>
                <option>Medium</option>
                <option>High</option>
            </select>
        </div>
        </div>
            
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Risk Assessment Types</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types"data-header="Close">
                <option value="1">Maintenance</option>
                <option value="2">Occupational</option>
                <option value="3">Resource</option>
                <option value="4">Department Baseline</option>
                <option value="5">Process</option>
                <option value="6">Contractor</option>
                <option value="7">Emergency</option>
            </select>
        </div>
        </div>
            
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Group</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more groups"data-header="Close">
                <option value="1">OHS (Health & Safety)</option>
                <option value="2">E (Environment)</option>
                <option value="3">Q (Quality)</option>
                <option value="4">F (Food)</option>
            </select>
        </div>
        </div>
            
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Risk Assessor</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more risk assessor"data-header="Close">
                <option>Nick Botha</option>
                <option>Sunny Mathole</option>
                <option>Helene Nel</option>
            </select>
        </div>
        </div>
            
        </div>
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
 </div>

<div class="panel panel-default">
    <div class="panel-heading">Requested</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Date Requested</th>
                    <th>Risk Assessment Type</th>
                    <th>Suggested Due Date</th>
                    <th>Priority Level</th>
                    <th>Requested By</th>
                    <th>Notes</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
             <?php
               if($data['allRequestedRA']): foreach($data['allRequestedRA'] as $nc):
                   ?>
                            <tr>
                                <td><?php echo $nc['date_created']; ?></td>
                                <td><?php echo $nc['template_id']; ?></td>
                                <td><?php echo $nc['due_date']; ?></td>
                                <td><?php echo $nc['priority']; ?></td>                                
                                <td><?php echo $nc['requested_by']; ?></td>
                                <td><?php echo $nc['notes']; ?></td>
                                <td><a href="#">View</a></td>
                                <td><a href="">Do Risk Assessment</a></td>
                                <td><a class="schedule_ra" ra="<?php echo $nc["id"];?>" href="#schedule-modal" data-toggle="modal" data-target="#schedule-modal">Schedule</a></td>
                            </tr>
                 <?php endforeach;endif;                  
              ?>
            </tbody>
        </table>    
    </div>
    </div>
    </div>

<!-- Schedule Modal -->
  <div class="modal fade" id="schedule-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Schedule</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="scheduleForm" action="<?php echo BASE_URL;?>/index.php?action=schedule_requested_ra&module=risk_assessment">
                <input type="hidden" id="requested_risk_id" name="requested_risk_id"/>  
            <div class="form-group">
                <label class="control-label col-sm-4" for="suggestedDate">Schedule Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="suggestedDate" id="suggestedDate" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="riskAssessor">Risk Assessor</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect"  name="riskAssessor" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                         <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }
                            ?>     
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="scheduleBtn" name="scheduleBtn" class="btn btn-success">Start</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<script>
$(function(){
    $("#suggestedDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datepicker();
    
    $(".schedule_ra").click(function()
    {
       var req = $(this).attr("ra");
       $("#requested_risk_id").val(req);
    });
});
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    