<?php 
/*
 * Header file
 */
$title = 'Do R/A';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">        
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Upcoming</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=doRiskAssessment">Do Risk Assessment</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RequestedRiskAssessment">Requested</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RegisterRiskAssessment">Register</a></li>
        <!--<li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=ControlsRiskAssessment">Risk Controls</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Setup</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

<div class="panel panel-default">
    <div class="panel-heading">Start Risk Assessment</div>
    <div class="panel-body"> 
        <form class="form-horizontal" method="post" name="startRiskAssessmentForm" action="new_risk_assessment_start_ra.php">
        <div class="form-group">
            <label class="control-label col-sm-4" for="riskAssessmentType">Risk Assessor</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                    <option>Occupation Work Instruction</option>
                    <option>Maintenance</option>
                </select> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="description">Description</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <textarea rows="5" class="form-control" name="description" id="description" required placeholder=""></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="startingDate">Starting Date</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" name="startingDate" id="startingDate" required placeholder=""> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="nextRevisionDate">Next Revision Date</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" name="nextRevisionDate" id="nextRevisionDate" required placeholder=""> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="frequency">Frequency</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a frequency">
                    <option>Monthly</option>
                    <option>Every 6 Months</option>
                    <option>Annually</option>
                    <option>Every 2 Years</option>
                </select>
            </div>
        </div>
        <div class="modal-custom-h5"><span><h5><small><b>Location</b></small></h5></span></div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="department">Department</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                    <option>I.T</option>
                    <option>Finance</option>
                    <option>HR</option>
                </select> 
            </div>
        </div>

        <div class="modal-custom-h5"><span><h5><small><b>Links</b></small></h5></span></div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="resources">Resources</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more resources" data-header="Close">
                    <option>Forklift</option>
                    <option>Grinder</option>
                </select> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="occupations">Occupations</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more occupations" data-header="Close">
                    <option>Developer</option>
                    <option>First Aider</option>
                </select> 
            </div>
        </div>                
    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-8">
        <button type="submit" id="startRiskAssessmentBtn" class="btn btn-success">Start</button>
        </div>
    </div>
    </form>
    </div>
    </div>

</div>
</div><!--End of container-fluid-->
<script>
$(function() {
    $("#startingDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datepicker();
    $("#nextRevisionDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datepicker();
});
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    