<?php 
/*
 * Header file
 */
$title = 'High Risk Assessment';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
<li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=doRiskAssessment">Do Risk Assessment</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RequestedRiskAssessment">Requested</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RegisterRiskAssessment">Register</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=ControlsRiskAssessment">Risk Controls</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Setup</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

<div class="panel panel-default">
<div class="panel-heading">Search High Risks</div>
<div class="panel-body">
<form class="form-inline" method="post" name="search" action="">
    <div class="col-md-12" style="margin-bottom: 10px;">
    <div class="col-md-4 col-sm-4">
    <div class="form-group">
        <p><b>Types</b></p>
        <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types"data-header="Close">
            <option value="1">Maintenance</option>
            <option value="2">Occupational</option>
            <option value="3">Resource</option>
            <option value="4">Department Baseline</option>
            <option value="5">Process</option>
            <option value="6">Contractor</option>
            <option value="7">Emergency</option>
        </select>
    </div>
    </div>
        
    <div class="col-md-4 col-sm-4">
    <div class="form-group">
        <p><b>Description</b></p>
        <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more descriptions"data-header="Close">
            <option value="1">Replacing Lights</option>
            <option value="2">Forklifts</option>
        </select>
    </div>
    </div>
        
    <div class="col-md-4 col-sm-4">
    <div class="form-group">
        <p><b>Risk Assessor</b></p>
        <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more risk assessors"data-header="Close">
            <option value="1">Nick Botha</option>
            <option value="2">Helene Nel</option>
            <option value="3">Brian Douglas</option>
        </select>
    </div>
    </div>
    </div>
    <div style="margin-left: 30px;">
    <div class="form-group">
        <input type="submit" class="btn btn-success" value="Search">
    </div>
    </div>
</form>
</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">High Risks</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>RA No</th>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Department</th>
                    <th>Risk Assessor</th>
                    <th>Links</th>
                    <th>Controlled</th>
                    <th>Above 100 Scores</th>
                    <th>Revision Date</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>12</td>
                    <td>14/06/2017</td>
                    <td>Maintenance</td>
                    <td>Replacing Lights</td>
                    <td><a href="">View</a></td>
                    <td>Nick Botha</td>
                    <td><a href="">View</a></td>
                    <td class="green-table-bg">20 / 20</td>
                    <td class="majordanger-table-bg">4</td>
                    <td>12/07/2018</td>
                    <td><a href="">View</a></td>
                    <td><a href="">Revise</a></td>
                </tr>               
            </tbody>
        </table>   
    </div>
    </div>
    </div>
</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    