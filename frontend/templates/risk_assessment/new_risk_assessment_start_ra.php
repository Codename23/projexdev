<?php 
/*
 * Header file
 */
$title = 'Upcoming';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
global $objAssets;
global $objOccupations;
global $objRiskAssessment;
$start_id = (int)$_GET["start_id"];
$row = $objRiskAssessment->getStartRow($start_id);
$data["companyOccupations"] = $objOccupations->getCompanyOccupationsInSource($row[0]["occupation_list"]);
$data['assets'] = $objAssets->getAllAssetsInSource(null,$row[0]["resource_list"]);
$data['sheqf_notes'] = $objRiskAssessment->getNewSHEQFNotes($start_id);
$data["listRiskAssessments"] = $objRiskAssessment->listRiskAssessments($start_id);
$HasNotCheck = "No";
?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
    <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=doRiskAssessment">Do Risk Assessment</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RequestedRiskAssessment">Requested</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RegisterRiskAssessment">Register</a></li>
        <!--<li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=ControlsRiskAssessment">Risk Controls</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Setup</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

<div class="panel panel-default">
<div class="panel-body">
    <h5>Risk Assessment Type: <?php echo $row[0]["type_name"];?><b></b></h5>
    <h5>Risk Assessment Template: <?php echo $row[0]["template"];?><b></b></h5>
    <h5>Description: <b><?php echo $row[0]["description"];?></b></h5>    
</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Resource Link</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>No</th>
                </tr>
            </thead>
            <tbody>
            <?php
               if($data['assets']): foreach($data['assets'] as $assetDetail):                     
                      echo '<tr>
                              <td>' . $assetDetail['department_name'] . '</td>
                              <td>' . $assetDetail['categoryType'] . '</td>
                              <td>' . $assetDetail['assetType'] . '</td>
                              <td><a href="index.php?module=assets&action=view&id=' . $assetDetail['id']  .'">' . $assetDetail['description'] . '</a></td>
                              <td>' . $assetDetail['company_asset_number'] . '</td>                            
                            </tr>';              
                   endforeach;endif; 
            ?>
            </tbody>
        </table>    
    </div>
    </div>
    </div>

<div class="panel panel-default">
    <div class="panel-heading">Occupation Link</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Occupation</th>
                </tr>
            </thead>
            <tbody>
              <?php if($data['companyOccupations']): foreach($data['companyOccupations'] as $occupation): ?>
                            <tr>
                                <td><?php echo $occupation['department_name']; ?></td>
                                <td><?php echo $occupation['name']; ?></td>
                            </tr>
                        <?php endforeach;endif;?>
            </tbody>
        </table>    
    </div>
    </div>
    </div>

<?php
$style_d = "display:block;";
if($row[0]["setupStatus"] == "")
{  
    $HasNotCheck = "Yes";
        ?>
        <div class="panel panel-default">
            <div class="panel-body"> 
                <form class="form-horizontal" method="post" name="loadRiskAssessment" action="<?php echo BASE_URL.'/index.php?action=loadRiskAssessment&module=risk_assessment&start_id='.$start_id?>">
                    <input type="hidden" name="loadrisk_template_id" value="<?php echo $row[0]["risk_template_id"];?>"/>
                    <input type="hidden" name="loadrisk_type_id" value="<?php echo $row[0]["risk_type_id"];?>"/>
                    <input type="hidden" name="loadrisk_start_id" value="<?php echo $start_id;?>"/>
                        <button type="submit" id="loadAssessment" name="loadAssessment" class="btn btn-primary">Load Risk Assessment</button>
                </form>
            </div>
        </div>
        <?php    
}
if($row[0]["setupStatus"] == "OK")
{    
 ?>

<div class="panel panel-default">
    <div class="panel-heading">Risk Assessment</div>
    <div class="panel-body"> 
        <form class="form-horizontal" method="post" name="startRiskAssessmentForm" action="employees_occupation_add_task.php">
        <div class="table-responsive">
            <table width="100%" class="table table-hover">
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Question</th>
                        <th>Status</th>
                        <th>Photo</th>
                        <th>Hazard Types</th>
                        <th>Description</th>
                        <th>Risk Types</th>
                        <th>Risk Desription</th>
                        <th>Score</th>
                        <th>Control</th>
                        <th>Control Description</th>
                        <th>Controlled</th>
                        <th>New Score</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                     <?php 
                               function getClass($row,$column)
                                {
                                    $class = "";
      
                                      //score
                                      if((int)$row[$column] < 51)
                                      {
                                          $class = "green-btn";
                                      }
                                      if((int)$row[$column] > 50 && (int)$row[$column] < 101)
                                      {
                                          $class = "yellow-btn";
                                      }
                                      if((int)$row[$column] > 101 && (int)$row[$column] < 151)
                                      {
                                          $class = "red-btn";
                                      }
                                      if((int)$row[$column] > 151)
                                      {
                                          $class = "majordanger-btn";
                                      }
                                      return $class;
                                }
                                
                     if($data['listRiskAssessments']): foreach($data['listRiskAssessments'] as $risk): 
                         
                                      $class = "";
                                      $control_in_place = "No";
                                      if($risk["is_control_in_effect"] == 1)
                                      {
                                          $control_in_place = "Yes";
                                      }
                                      $p_class = getClass($risk,"pscore");
                                      $new_score = getClass($risk,"new_score");
                         
                         ?>
                            <tr>
                                <td><?php echo $risk['category']; ?></td>
                                <td><?php echo $risk['question']; ?></td>
                                <?php
                                if($risk["status"] == null)
                                {
                                    $HasNotCheck = "Yes";
                                    ?>
                                        <td class="yellow-table-bg"><a href="#check-modal" class="check_risk_assessment" asses_id="<?php echo $risk["id"];?>" category="<?php echo $risk["category"];?>" question="<?php echo $risk["question"];?>" data-toggle="modal" data-target="#check-modal">Check</a></td>
                                    <?php
                                    for($x = 0 ;$x < 11;$x++)
                                    {
                                      echo "<td></td>";   
                                    }                                    
                                }
                                else if($risk["status"] == "N/A")
                                {
                                     ?>
                                    <td class="red-table-bg"><a style="color:#fff;" href="#check-modal" class="check_risk_assessment" asses_id="<?php echo $risk["id"];?>" category="<?php echo $risk["category"];?>" question="<?php echo $risk["question"];?>">N/A</a></td>
                                    <?php
                                    for($x = 0 ;$x < 11;$x++)
                                    {
                                      echo "<td></td>";   
                                    }    
                                }
                                else
                                {
                                    //display all the others
                                    ?>
                                   <td class="green-table-bg"><a href="#check-modal" style="color:#fff;" class="check_risk_assessment" asses_id="<?php echo $risk["id"];?>" category="<?php echo $risk["category"];?>" question="<?php echo $risk["question"];?>"><?php echo $risk["status"];?></a></td> 
                                   <td>None</td>                                   
                                   <td><?php echo $risk["hazard_type"];?></td>
                                   <td><?php echo $risk["hazard_description"];?></td>
                                   <td><?php echo $risk["risk_type"];?></td>
                                   <td><?php echo $risk["risk_description"];?></td>
                                   <td class="<?php echo $p_class;?>"><?php echo $risk["pscore"];?></td>
                                   <td><?php echo $risk["control_type"];?></td>
                                   <td><?php echo $risk["control_description"];?></td>
                                   <td><?php echo $control_in_place;?></td>
                                   <td class="<?php echo $new_score;?>"><?php echo $risk["new_score"];?></td>
                                    <?php
                                }
                                ?>
                            </tr>
                        <?php endforeach;endif;?>  
                </tbody>
            </table>    
        </div><br/>
        <div class="form-group">
            <div class="col-sm-8">
            <a href="#sheqfNotes-modal" data-toggle="modal" data-target="#sheqfNotes-modal"><button type="button" id="sheqfNotes" class="btn btn-primary">SHEQF Notes</button></a>
            </div>
        </div>
        <div class="table-responsive">
            <table width="100%" class="table table-hover">
                <thead>
                    <tr>
                        <th>Group</th>
                        <th>Topic</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                   <?php
                                        if($data['sheqf_notes']): foreach($data['sheqf_notes'] as $notes):                                
                                        ?>
                                        <tr>                                           
                                           <td><?php echo $notes["group_id"];?></td>
                                           <td><?php echo $notes["topic"];?></td>
                                           <td><?php echo $notes["notes"];?></td>                                     
                                        </tr>
                                   <?php endforeach;endif; ?>
                </tbody>
            </table>    
        </div>      
        </form>
    </div>
</div>
<?php
    }
    if($HasNotCheck == "Yes")
    {
        $style_d = "display:none !important;";
    }
?>    
            <a style="<?php echo $style_d;?>" href="#submitAssessment-modal" data-toggle="modal" data-target="#submitAssessment-modal"><button type="button" id="submitAssessment" class="btn btn-success">Register Assessment</button></a>     
<!-- Check Modal -->
  <div class="modal fade" id="check-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Check Risk Assessment</h4>
        </div>
        <div class="modal-body">
            
            <div class="form-group">
                <label class="control-label col-sm-4" for="status">Status</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <button type="button" id="possibleRisk" class="btn btn-default possibleRisk">Possible Risk</button>
                    <button type="button" id="notApplicable" class="btn btn-default notApplicable">Not Applicable</button>
                </div>
            </div>
            <br/>
            <form class="form-horizontal" method="post" name="checkForm" action="<?php echo BASE_URL.'/index.php?action=updateNotApplicable&module=risk_assessment&start_id='.$start_id;?>">
             <div class="notRiskDiv" style="display:none;">
                  <div class="form-group">
                      <input type="hidden" id="assessment_id" name="assessment_id"/>
                      <br/>
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" id="btnNotApplicableRisk" name="btnNotApplicableRisk"  class="btn btn-success">Update Not Applicable</button>
                    </div>
                 </div>  
            </div>
            </form>
            
            
            <form class="form-horizontal" method="post" enctype="multipart/form-data" name="checkForm" action="<?php echo BASE_URL.'/index.php?action=update_risk_assessment&module=risk_assessment&start_id='.$start_id;?>">
                <input type="hidden" id="form_assessment_id" name="form_assessment_id"/>
                <input type="hidden" id="start_id" name="start_id" value="<?php echo $start_id;?>"/>
            <div id="possibleRiskDiv">
            <div class="form-group">
                <label class="control-label col-sm-4" for="category">Category</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" readonly="true" id="category" value="Fire and Safety Equipment">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="question">Question</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" readonly="true" id="question" value="Walkways not obstructed and slip free">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="photo">Upload Photo</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="file" id="photo">
                </div>
            </div>
            
            <div class="modal-custom-h5"><span><h5>Hazard</h5></span></div>
              <div class="form-group">
                <label class="control-label col-sm-4" for="hazardType">Hazard Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="harzard_type" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a hazard type">
                       <?php
                            foreach($data['hazard'] as $hazard)
                            {
                                echo "<option value='{$hazard["id"]}'>{$hazard['hazard']}</option>";
                            }
                        ?>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="updateHazardDescription">Hazard Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="hazard_description" id="hazardDescription" required placeholder="">
                </div>
            </div>
            
            <div class="modal-custom-h5"><span><h5>Risk</h5></span></div>
          <div class="form-group">
                <label class="control-label col-sm-4" for="riskType">Risk Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" name="risk_type" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more risk types">
                   <?php
                            foreach($data['risk'] as $risk)
                            {
                                echo "<option value='{$risk["id"]}'>{$risk['risk']}</option>";
                            }
                        ?>
                </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="riskDescription">Risk Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="risk_description" id="riskDescription" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <!--<span id="pscore"></span>-->
                <input type="text" name="pscore" id="pscore"/>
                <a href="#risk-assessment-score-modal" data-toggle="modal" data-target="#risk-assessment-score-modal"><button type="button" id="risk-assessment-score" class="btn btn-success">Add Score</button></a>
                </div>
            </div>
            
            <div class="modal-custom-h5"><span><h5>Control</h5></span></div>
             <div class="form-group">
                <label class="control-label col-sm-4" for="controlType">Control Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect get_control_reduction" name="control_type" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more control types">
                     <?php
                            foreach($data['control'] as $control)
                            {
                                echo "<option value='{$control["id"]}'>{$control['control_type']}</option>";
                            }
                        ?>
                </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="controlDescription">Control Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="control_description" id="controlDescription" required placeholder="">
                </div>
            </div>
           <div class="form-group">
                <label class="control-label col-sm-4" for="controlInEffect">Control Currently in Effect</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <label class="radio-inline"><input type="radio" name="controlInEffect" id="controlInEffect1" value="1" required>Yes</label>
                    <label class="radio-inline"><input type="radio" name="controlInEffect" id="controlInEffect2" value="0" >No</label>
                </div>
            </div>
            <div class="control hidden-div">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="updateControlNotes">Notes</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" id="ControlNotes" name="ControlNotes"></textarea>
                    </div>
                </div>
            </div>
            <div class="controlYes hidden-div2">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="controlYesReduction">Reduced By</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" id="controlYesReduction" name="controlYesReduction" value='50%' readonly="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="controlYesScore">New Score</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" id="controlYesScore" name="controlYesScore" value='200' readonly="">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addRiskAssessmentBtn" name="addRiskAssessmentBtn"  class="btn btn-success">Update</button>
                </div>
            </div>
       
        </div>
         </form>    
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
</div>

<!-- Modal Score -->
  <div class="modal fade" id="risk-assessment-score-modal" role="dialog">
    <div class="modal-dialog risk-assessment-score-dialog-modal">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Risk Assessment Score</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" name="riskAssessmentScoreForm" >
              <div class="table-responsive">
                  <table width="100%" class="table table-bordered">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="text-center" colspan="6">Severity of the potential injury/damage</th>
                </tr>
                </thead>
                <tbody>
                <tr class="text-center">
                    <td></td>
                    <td width="16%"><p>Insignificant damage to Property, Equipment or Minor Injury</p></td>
                    <td width="16%"><p>Non-Reportable Injury, minor loss of Process or slight damage to Property</p></td>
                    <td width="16%"><p>Reportable Injury moderate loss of Process or limited damage to Property</p></td>
                    <td width="16%"><p>Major Injury, Single Fatality critical loss of Process/damage to Property</p></td>
                    <td width="16%"><p>Multiple Fatalities Castastrophic Loss of Business</p></td>
                </tr>
                <tr class="text-center">
                    <td><b>Likelihood of the hazard happening</b></td>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                </tr>

                <tr class="text-center number">
                    <td class="sideTitle">Almost Certain<br/>50</td>
                    <td class="green-btn">50</td>
                    <td class="yellow-btn">100</td>
                    <td class="red-btn">150</td>
                    <td class="majordanger-btn">200</td>
                    <td class="majordanger-btn">250</td>
                </tr>
                <tr class="text-center number"> 
                    <td class="sideTitle">Will probably occur<br/>40</td>
                    <td class="green-btn">40</td>
                    <td class="yellow-btn">80</td>
                    <td class="red-btn">120</td>
                    <td class="majordanger-btn">160</td>
                    <td class="majordanger-btn">250</td>
                </tr>
                <tr class="text-center number">
                    <td class="sideTitle">Possible occur<br/>30</td>
                    <td class="green-btn">30</td>
                    <td class="yellow-btn">60</td>
                    <td class="yellow-btn">90</td>
                    <td class="red-btn">120</td>
                    <td class="red-btn">150</td>
                </tr>
                <tr class="text-center number">
                    <td class="sideTitle">Remote possibility<br/>20</td>
                    <td class="green-btn">20</td>
                    <td class="green-btn">40</td>
                    <td class="yellow-btn">60</td>
                    <td class="yellow-btn">80</td>
                    <td class="yellow-btn">100</td>
                </tr>
                <tr id="extreme" class="text-center number">
                    <td class="sideTitle">Extremely Unlikely<br/>100</td>
                    <td class="green-btn">10</td>
                    <td class="green-btn">20</td>
                    <td class="green-btn">30</td>
                    <td class="green-btn">40</td>
                    <td class="green-btn">50</td>
                </tr>
                </tbody>
                </table>
                </div>
                <div class="form-group">
                    <div class="col-sm-8">
                    <button type="button" id="riskAssessmentBtn" class="btn btn-success">Add Score</button>
                    </div>
                </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->

<!-- SHEQF Notes Modal -->
  <div class="modal fade" id="sheqfNotes-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">SHEQF Notes</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="sheqfNotesForm" action="<?php echo BASE_URL.'/index.php?action=post_sheqf_notes&module=risk_assessment&start_id='.$start_id;?>">
                <input type="hidden" readonly="true" name="request_id" value="<?php echo $start_id;?>"/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="group" class="form-control">
                    <option value selected disabled>Please select a group</option>
                       <?php
                        foreach($data['allSheqTeams'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['sheqteam_name']}</option>";
                        }?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="topic">Topic</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="topic" name="topic" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notes">Notes</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" id="notes" name="notes" required placeholder=""></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="sheqfNotesBtn" name="sheqfNotesBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Risk Assessment Submit Changes Modal -->
  <div class="modal fade" id="submitAssessment-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Revision</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="raSubmitChangesForm" action="<?php echo BASE_URL;?>/index.php?action=updateRiskAssessment&module=risk_assessment">  
             <input type="hidden" readonly="true" name="start_id" value="<?php echo $start_id;?>"/>  
              <input type="hidden" readonly="true" name="request_id" value="<?php echo $row[0]["request_id"];?>"/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="responsiblePerson">Responsible Person</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="revisedBy" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                        <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }
                            ?>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="raSubmitChangesBtn" name="raSubmitChangesBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- View Photo Modal -->
  <div class="modal fade" id="viewPhoto-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Risk Assessment Photo</h4>
        </div>
        <div class="modal-body">
            <img class="img-responsive" src="images/uncovered-wires.jpg" alt="wires">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<script>
$(function() {
    var selectedRow = 0;
    
    $("#controlSuggestedDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    $("#ControlSuggestedDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    $("#nextRevisionDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
   
     $('input[name="controlInEffect"]').click(function(){
            if($(this).attr("value")==="0"){
                $(".hidden-div").not(".control").hide();
                $(".control").show();
            }else{
                $(".control").hide();
            }
    });
    $('input[name="controlInEffect"]').click(function(){
            if($(this).attr("value")==="1"){
                $(".hidden-div2").not(".controlYes").hide();
                $(".controlYes").show();
            }else{
                $(".controlYes").hide();
            }
    });
    
    $('.possibleRisk').click(function() {
        $(".notRiskDiv").hide();
       $(this).removeClass("btn-default");
        $(this).addClass("btn-success");
        $('.notApplicable').removeClass("btn-warning");
        $('.notApplicable').addClass("btn-default");
    });

    $('.notApplicable').click(function() {
        $(".notRiskDiv").show();
       $(this).removeClass("btn-default");
        $(this).addClass("btn-warning");
        $('.possibleRisk').removeClass("btn-success");
        $('.possibleRisk').addClass("btn-default");
    });
});

$("#possibleRiskDiv").hide();
    
    $(".possibleRisk").click(function(){
    $("#possibleRiskDiv").show();
    });
    $(".notApplicable").click(function(){
    $("#possibleRiskDiv").hide();
    });

$("#riskAssessmentBtn").click(function(){
        $("#risk-assessment-score-modal").modal('toggle');
    });

    $('.green-btn, .yellow-btn, .red-btn, .majordanger-btn').click( function() {
      $(this).parents('table').find('td').each( function( index, element ) {
          $(element).removeClass('on');
      } );
      $(this).addClass('on');
    } );
    
    $( ".number td:not(.sideTitle)" ).click(function() {
    //alert($(this).text());
    $("#pscore").text($(this).text());
    });
    
 $(".check_risk_assessment").on("click",function()
 {
     var category = $(this).attr("category");
     var question = $(this).attr("question");
     var assess_id = $(this).attr("asses_id");
     $("#category").val(category);
     $("#question").val(question);
     $("#form_assessment_id").val(assess_id);
     $("#assessment_id").val(assess_id);
 }); 
 
 $( ".number td:not(.sideTitle)" ).click(function() {
    //alert($(this).text());
    $("#pscore").val($(this).text());
    });
    
    $("#riskAssessmentBtn").on("click",function()
    {
       var reduced_by = parseInt($("#controlYesReduction").val()); 
       calculate(reduced_by);
    });
     function calculate(result)
    {
          var reduction = parseInt(result);
          var score = parseInt($("#pscore").val());
          var new_score = (reduction * score / 100);
          $("#controlYesReduction").val(reduction + "%");
          $("#controlYesScore").val((score - new_score));
    }
    $(".get_control_reduction").on("change",function()
        {
            var id = this.value;
            var url = "<?php echo BASE_URL;?>/index.php?action=ajax_get_control_reduction&module=assets";
            $.ajax({
               type:'GET',
               url:url,
               data:{control_id:id},
               success:function(result)
               {
                    calculate(result);
               }
            });
            return false;
        }); 
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    