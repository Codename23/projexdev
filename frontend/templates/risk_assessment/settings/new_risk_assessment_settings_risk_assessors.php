<?php 
$title = 'Risk Assessors';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 	
<div class="col-lg-10">
  
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
                
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Upcoming</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=doRiskAssessment">Do Risk Assessment</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RequestedRiskAssessment">Requested</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RegisterRiskAssessment">Register</a></li>
        <!--<li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=ControlsRiskAssessment">Risk Controls</a></li>-->
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Setup</a></li>
    </ul>
    </div>
</div>  
<div class="row">
    <div class="col-lg-6">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li ><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Risk Assessment</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=new_risk_assessment_settings">Settings</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings_risk_assessors">Risk Assessors</a></li>
    </ul>
    </div>
</div> 
    
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#addRiskAssessor-modal" data-toggle="modal" data-target="#addRiskAssessor-modal"><button type="button" id="addRiskAssessor" class="btn btn-success">Add Risk Assessor</button></a>
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading">Risk Assessors</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Name Surname</th>
                    <th>Department</th>
                    <th>Date Added</th>
                    <th>Risk Assessed Departments</th>
                    <th>Group</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                  <?php
                 if($data['risk_assessors']): foreach($data['risk_assessors'] as $risk):   
                                ?>
                                <tr>
                                    <td><?php echo $risk["emp"];?></td>
                                    <td><?php echo $risk["department_name"];?></td>
                                    <td><?php echo $risk["date_created"];?></td>
                                    <td><?php echo $risk["risk_dep"];?></td>
                                    <td><?php echo $risk["groups_"];?></td>
                                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                </tr>
                                <?php endforeach;endif; 
                ?>   
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of panel-default-->
    
    <!-- Add Risk Assessor Modal -->
  <div class="modal fade" id="addRiskAssessor-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Risk Assessor</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addRiskAssessorForm" action="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=post_risk_assessor">
            <div class="form-group">
                <label class="control-label col-sm-4" for="dateCreated">Date Created</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="dateCreated" id="dateCreated" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="department">Department</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="risk_dep" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                         <?php
                        foreach($data['allDepartments'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                        }?>
                    </select> 
                </div>
            </div>   
            <div class="form-group">
                <label class="control-label col-sm-4" for="nameSurname">Name Surname</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="risk_emp" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a name and surname">
                    <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>
                    </select> 
                </div>
            </div><br/>
            <div class="form-group">
                <input type="hidden" id="risk_ass_dep" name="risk_ass_dep_val"/>
                <label class="control-label col-sm-4" for="riskAssessedDepartment">Risk Assessed Departments</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect risk_ass_dep" id="risk_ass_dep" name="risk_ass_dep" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                    <?php
                        foreach($data['allDepartments'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                        }?>
                    </select> 
                </div>
            </div>   
            <div class="form-group">
                <input type="hidden" id="risk_group" name="risk_group_val"/>
                <label class="control-label col-sm-4" for="groups">Groups</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="risk_group" id="risk_group" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more groups" data-header="Close">
                          <?php
                        foreach($data['allSheqTeams'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['sheqteam_name']}</option>";
                        }?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addRiskAssessorBtn" name="addRiskAssessorBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
   </div>
</div><!--End of container-fluid-->
<script>
    $(function() {
    $("#dateCreated").datepicker({
        dateFormat:"yy/mm/dd",
        maxDate: new Date(), 
        minDate: new Date()});
    $("#dateCreated").datepicker("setDate", new Date());
        $('.selectpicker').on('change', function(){              
            var selected = $(this," option:selected").val();
            focus_dropdown = $(this).parent().children('select').attr('id');
            switch(focus_dropdown)
            {
                case 'risk_ass_dep':
                    $("#risk_ass_dep").val(selected);
                    break;
               case 'risk_group':
                   $("#risk_group").val(selected);
                    break;  
            }
           // console.log(focus_dropdown);
        });
    });
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  