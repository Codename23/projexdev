<?php 
/*
 * Header file
 */
$title = 'Settings Risk';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
                
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Upcoming</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=doRiskAssessment">Do Risk Assessment</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RequestedRiskAssessment">Requested</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RegisterRiskAssessment">Register</a></li>
        <!--<li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=ControlsRiskAssessment">Risk Controls</a></li>-->
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Setup</a></li>
    </ul>
    </div>
</div>  
<div class="row">
    <div class="col-lg-6">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li ><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Risk Assessment</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=new_risk_assessment_settings">Settings</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings_risk_assessors">Risk Assessors</a></li>
    </ul>
    </div>
</div> 
<div class="row">
    <div class="col-lg-6">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li ><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=new_risk_assessment_settings">Hazards</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings_risk">Risk</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings_controls">Controls</a></li>        
    </ul>
    </div>
</div>  
<!--End of sub menu-->

<div class="panel panel-default">
<div class="panel-body">
    <a href="#addRiskTypes-modal" data-toggle="modal" data-target="#addRiskTypes-modal"><button type="button" id="addRiskTypes" class="btn btn-success">Add Risk Types</button></a>
</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Risks</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Group</th>
                    <th>Risk Types</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                  <?php
                 if($data['risk']): foreach($data['risk'] as $risk):   
                                ?>
                                <tr>
                                    <td><?php echo $risk["group_id"];?></td>
                                    <td><?php echo $risk["risk"];?></td>
                                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                </tr>
                                <?php endforeach;endif; 
                ?>    
            </tbody>
        </table>    
    </div>
    </div>
    </div>

<!-- Add Risk Types Modal -->
  <div class="modal fade" id="addRiskTypes-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Risk Type</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addRiskTypesForm" action="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=post_risk_settings">
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="group_risk" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more groups" data-header='Close'>
                        <option value="1">OHS (Health & Safety)</option>
                        <option value="2">E (Environment)</option>
                        <option value="3">Q (Quality)</option>
                        <option value="4">F (Food)</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="risk">Risk</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="risk" id="risk" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addRiskTypesBtn" name="addRiskTypesBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    