<?php 
$title = 'Setup';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
$id = (int)$_GET["temp_id"];
?> 		
<div class="col-lg-10">
  
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
  <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Upcoming</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=doRiskAssessment">Do Risk Assessment</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RequestedRiskAssessment">Requested</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RegisterRiskAssessment">Register</a></li>
        <!--<li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=ControlsRiskAssessment">Risk Controls</a></li>-->
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Setup</a></li>
    </ul>
    </div>
</div>  

<div class="row">
    <div class="col-lg-6">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Risk Assessment</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=new_risk_assessment_settings">Settings</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings_risk_assessors">Risk Assessors</a></li>
    </ul>
    </div>
</div>  
    <div class="panel panel-default">
        <div class="panel-body">
        <a href="#addCategories-modal" data-toggle="modal" data-target="#addCategories-modal"><button type="button" id="addCategories" class="btn btn-success">Add Categories</button></a>
        </div>
    </div> 
    
    <div class="panel panel-default">
    <div class="panel-heading">Critical Categories</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Category</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
               <?php   
                             $counter = 0;
                        if($data['allCategories']): foreach($data['allCategories'] as $category):
                            $counter++;
                            ?>
                                     <tr>
                                         <td><?php echo $category['category']; ?></td>                                
                                         <td><span class="glyphicon glyphicon-pencil"></span></td>                                                             
                                         <td><span class="glyphicon glyphicon-trash"></span></td>                                                             
                            </tr>
                        <?php endforeach;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of panel-default-->
    
    <div class="panel panel-default">
        <div class="panel-body">
        <a href="#addQuestion-modal" data-toggle="modal" data-target="#addQuestion-modal"><button type="button" id="addQuestion" class="btn btn-success">Add Question</button></a>
        </div>
    </div> 
    
    <div class="panel panel-default">
    <div class="panel-heading">Questions</div>
    <div class="panel-body"> 
        <div class="table-responsive">
         <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>No</th>
                                    <th>Questions</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                     <?php   
                                     $counter = 0;
                        if($data['allCategories']): foreach($data['allCategories'] as $category):
                            ?>  <tr>
                                    <td><?php echo $category["category"];?></td>                                 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                  <?php   
                                 if($data['allQuestions']): foreach($data['allQuestions'] as $questions):
                                     if($questions["category_id"] == $category["id"])
                                     {
                                         $counter++;
                                ?>
                                <tr>
                                    <td></td>
                                    <td><?php echo $counter;?></td>
                                    <td><?php echo $questions["question"];?></td>
                                    <td><span class="glyphicon glyphicon-trash"></span></td>
                                </tr>  
                                <?php 
                                     }
                                endforeach;endif; ?>
                                
                        <?php endforeach;endif; ?>
                                                         
                            </tbody>
                        </table>   
    </div>
    </div>
    </div>
    <!--End of panel-default-->
    
    <!-- Add Categories Modal -->
  <div class="modal fade" id="addCategories-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Category</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="createCriticalCategoriesForm" action="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=create_categories">
            <input type="hidden" name="template_id" value="<?php echo $_GET["temp_id"];?>"/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="category">Category</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="category_title" id="category_title" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addCategoriesBtn" name="addCategoriesBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

    <!-- Add Question Modal -->
  <div class="modal fade" id="addQuestion-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Question</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="createQuestionsForm" action="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=create_risk_question">
            <input type="hidden" name="template_id" value="<?php echo $_GET["temp_id"];?>"/>
            <div class="form-group">
                    <label class="control-label col-sm-4" for="category_list">Category</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="form-control" name="category_list" id="category_list">
                                 <?php   
                        if($data['allCategories']): foreach($data['allCategories'] as $category):
                            ?>     
                                         <option value="<?php echo $category['id']; ?>"><?php echo $category['category']; ?></option>                                                                                      
                        <?php endforeach;endif; ?>
                        </select>
                    </div>
           </div>    
            <div class="form-group">
                <label class="control-label col-sm-4" for="question">Question</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="category_quest" id="category_quest" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="createQuestion" name="createQuestion" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
 <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" method="post" name="markFormAsActive" action="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=mark_risk_template_active">
            <input type="hidden" name="template_id" value="<?php echo $_GET["temp_id"];?>"/>
            <button type="submit" id="markAsActive" name="markAsActive" class="btn btn-success">Mark as Active</button>
            </form>
        </div>
    </div> 
   </div>
</div><!--End of container-fluid-->
<script>
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');

?>  