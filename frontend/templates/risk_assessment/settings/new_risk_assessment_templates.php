<?php 
$title = 'Setup';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 		
<div class="col-lg-10">
  
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
  <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Upcoming</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=doRiskAssessment">Do Risk Assessment</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RequestedRiskAssessment">Requested</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RegisterRiskAssessment">Register</a></li>
        <!--<li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=ControlsRiskAssessment">Risk Controls</a></li>-->
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Setup</a></li>
    </ul>
    </div>
</div>  

<div class="row">
    <div class="col-lg-6">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Risk Assessment</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=new_risk_assessment_settings">Settings</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings_risk_assessors">Risk Assessors</a></li>
    </ul>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Risk Assessment Types</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=new_risk_assessment_templates">Risk Assessment Templates</a></li>        
    </ul>
    </div>
</div>      
   <div class="panel panel-default">
<div class="panel-body">
    <a href="#addRiskTemplate-modal" data-toggle="modal" data-target="#addRiskTemplate-modal"><button type="button" id="addRiskTypes" class="btn btn-success">Add Risk Assessment Templates</button></a>
</div>
</div> 
    <div class="panel panel-default">
    <div class="panel-heading">Risk Assessment Templates</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Templates</th>
                    <th>Setup</th>
                </tr>
            </thead>
            <tbody>
                
                
                <?php
                 if($data['risk_templates']): foreach($data['risk_templates'] as $template): 
                     $default = $template["is_draft"];
                     $class = "";
                     if($default == 0)
                     {
                         $default = "Active";
                         $class = "bg-success text-success";
                     }
                     else
                     {
                         $default = "Draft";
                         $class = "bg-danger text-danger";
                     }
                ?>
                    <tr>
                             <td><?php echo $template["template"];?></td>
                             <td class="<?php echo $class;?>"><a href="<?php echo BASE_URL.'/index.php?action=new_risk_assessment_setup_view&module=risk_assessment&temp_id='.$template["id"];?>"><?php echo $default;?></a></td>
                    </tr>
                  <?php endforeach;endif; ?>                    
            </tbody>
        </table>     
    </div>
    </div>
    </div>
    <!--End of panel-default-->
    
<!-- Add Hazard Types Modal -->
  <div class="modal fade" id="addRiskTemplate-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Risk Assessment Templates</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addRiskTypes" action="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=post_risk_templates">
                <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" name="group_types" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more groups" data-header='Close'>
                        <option value="1">OHS (Health & Safety)</option>
                        <option value="2">E (Environment)</option>
                        <option value="3">Q (Quality)</option>
                        <option value="4">F (Food)</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Risk Assessment</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" id="risk_assesment" name="risk_assesment" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more groups" data-header='Close'>
                            <?php
                                      foreach($data["risk_types"] as $types)
                                     {
                                          echo  "<option value='{$types["id"]}'>{$types['type_name']}</option>";
                                     }
                            ?>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="template">Template</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="template" id="template" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addRiskTemplate" name="addRiskTemplate" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
   </div>
</div><!--End of container-fluid-->

<script>
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');

?>  