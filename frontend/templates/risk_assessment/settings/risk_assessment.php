<?php 
$title = 'Setup';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 		
<div class="col-lg-10">
  
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
  <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Upcoming</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=doRiskAssessment">Do Risk Assessment</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RequestedRiskAssessment">Requested</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RegisterRiskAssessment">Register</a></li>
        <!--<li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=ControlsRiskAssessment">Risk Controls</a></li>-->
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Setup</a></li>
    </ul>
    </div>
</div>  

<div class="row">
    <div class="col-lg-6">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Risk Assessment</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=new_risk_assessment_settings">Settings</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings_risk_assessors">Risk Assessors</a></li>
    </ul>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Risk Assessment Types</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=new_risk_assessment_templates">Risk Assessment Templates</a></li>        
    </ul>
    </div>
</div>      
   <div class="panel panel-default">
<div class="panel-body">
    <a href="#addRiskTypes-modal" data-toggle="modal" data-target="#addRiskTypes-modal"><button type="button" id="addRiskTypes" class="btn btn-success">Add Risk Assessment Types</button></a>
</div>
</div> 
    <div class="panel panel-default">
    <div class="panel-heading">Risk Assessment</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Types</th>
                    <th>Default Type</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                
                
                <?php
                 if($data['risk_types']): foreach($data['risk_types'] as $risk): 
                     $default = $risk["is_default"];
                     if($default == "")
                     {
                         $default = "Custom";
                     }
                ?>
                    <tr>
                             <td><?php echo $risk["type_name"];?></td>
                            <td><?php echo $default;?></td>
                            <td><span class="glyphicon glyphicon-pencil"></span></td>
                    </tr>
                  <?php endforeach;endif; ?>                    
            </tbody>
        </table>     
    </div>
    </div>
    </div>
    <!--End of panel-default-->
    
<!-- Add Hazard Types Modal -->
  <div class="modal fade" id="addRiskTypes-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Risk Assessment Type</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addRiskTypes" action="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=post_risk_types">
            
            <div class="form-group">
                <label class="control-label col-sm-4" for="types">Types</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="types" id="types" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addHazardTypesBtn" name="addRiskTypes" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
   </div>
</div><!--End of container-fluid-->

<script>
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');

?>  