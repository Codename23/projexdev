<?php 
/*
 * Header file
 */
$title = 'Risk Controls';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
<li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=doRiskAssessment">Do Risk Assessment</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RequestedRiskAssessment">Requested</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RegisterRiskAssessment">Register</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=ControlsRiskAssessment">Risk Controls</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Setup</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

<div class="panel panel-default">
    <div class="panel-heading">Risk Controls</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>RA</th>
                    <th>Risk Assessment Date</th>
                    <th>Risk Assessment Type</th>
                    <th>Details</th>
                    <th>Risk Assessor</th>
                    <th>Control Due Date</th>
                    <th>Notes</th>
                    <th>Responsible Person</th>
                    <th>All Controls Active</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>12313</td>
                    <td>12/03/2017</td>
                    <td>Occupational</td>
                    <td>Welder</td>
                    <td>James Bing</td>
                    <td>13/03/2017</td>
                    <td><a href=''>View</a></td>
                    <td>Nick</td>
                    <td class="majordanger-table-bg">No</td>
                    <td><a href='new_risk_assessment_risk_controls_view.php'>View Risk Assessment</a></td>
                </tr>
            </tbody>
        </table>    
    </div>
    </div>
    </div>
</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    