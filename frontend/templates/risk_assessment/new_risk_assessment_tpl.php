<?php 
/*
 * Header file
 */
$title = 'Upcoming';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
global $objRiskAssessment;
?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=doRiskAssessment">Do Risk Assessment</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RequestedRiskAssessment">Requested</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=RegisterRiskAssessment">Register</a></li>       
        <!--<li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=ControlsRiskAssessment">Risk Controls</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=view_settings">Setup</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->
<div class="panel panel-default">
    <div class="panel-heading">Search Upcoming Risk Assessment</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Risk Assessment Types</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types"data-header="Close">
                <option value="1">Maintenance</option>
                <option value="2">Occupational</option>
                <option value="3">Resource</option>
                <option value="4">Department Baseline</option>
                <option value="5">Process</option>
                <option value="6">Contractor</option>
                <option value="7">Emergency</option>
            </select>
        </div>
        </div>

        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>I.T</option>
                <option>Human Resources</option>
                <option>Marketing</option>
                <option>Support and Services</option>
            </select>
        </div>
        </div>
            
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Risk Assessor</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more risk assessor"data-header="Close">
                <option>Nick Botha</option>
                <option>Sunny Mathole</option>
                <option>Helene Nel</option>
            </select>
        </div>
        </div>
            
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Group</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more groups"data-header="Close">
                <option value="1">OHS (Health & Safety)</option>
                <option value="2">E (Environment)</option>
                <option value="3">Q (Quality)</option>
                <option value="4">F (Food)</option>
            </select>
        </div>
        </div>

        </div>
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
 </div>

<div class="panel panel-default">
    <div class="panel-heading">Upcoming</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Due Date</th>
                    <th>RA Type</th>
                    <th>Risk Assessor</th>
                    <th>Priority</th>
                    <th>Request Details</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
               if($data['allRequestedUpcoming']): foreach($data['allRequestedUpcoming'] as $nc):
                   ?>
                            <tr>
                                <td><?php echo $nc['upcoming_date']; ?></td>
                                <td><?php echo $nc['template_id']; ?></td>                                                         
                                <td><?php echo $nc['requested_by']; ?></td>
                                 <td><?php echo $nc['priority']; ?></td>  
                                <td><?php echo $nc['notes']; ?></td>
                                <!-- check if there's a pending schedule -->
                                <?php
                                if(isset($nc["start_id"]) == null)
                                {
                                ?>
                                <td class="green-table-bg"><a class="request_id" style="color:#fff;" ncid="<?php echo $nc["id"];?>" href="#startRiskAssessment-modal" data-toggle="modal" data-target="#startRiskAssessment-modal">Start</a></td>
                                <?php
                                }
                                else
                                {
                                    $longStr = "start_id=".$nc["start_id"];//."&resources_list=".$nc[""];
                                    ?>
                                 <td class="red-table-bg"><a class="request_id" style="color:#fff;" ncid="<?php echo $nc["id"];?>" href="<?php echo BASE_URL.'/index.php?action=DoStartRiskAssessment&module=risk_assessment&'.$longStr;?>">Incomplete</a></td>   
                                 <?php
                                }
                                ?>
                                <!-- ./end check pending request -->
                                <td><a class="reschedule_id" ncid="<?php echo $nc["id"];?>" href='#Reschedule' data-toggle="modal" data-target="#RescheduleRiskAssessment-modal">Reschedule</a></td>
                                <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                            </tr>
                 <?php endforeach;endif;                  
              ?>   
            </tbody>
        </table>    
    </div>
    </div>
    </div>

<!-- Start Risk Assessment Modal -->
  <div class="modal fade" id="startRiskAssessment-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Start Risk Assessment</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="startRiskAssessmentForm" action="<?php echo BASE_URL;?>/index.php?action=StartRiskAssessment&module=risk_assessment">
                <input type="hidden" id="risk_requested_id" name="risk_requested_id"/>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="riskAssessmentType">Risk Assessor</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" id="riskAssessmentType" name="riskAssessmentType" data-live-search="true" required="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                            <?php
                              foreach($data["risk_types"] as $types)
                             {
                                  echo  "<option value='{$types["id"]}'>{$types['type_name']}</option>";
                             }
                            ?>                 
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="riskAssessmentTemplate">Risk Assessment Template</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" id="riskAssessmentTemplate" name="riskAssessmentTemplate" required="true" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a template">
                            
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="description">Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="description" id="description" required placeholder=""></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="startingDate">Starting Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="startingDate" id="startingDate" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="nextRevisionDate">Next Revision Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="nextRevisionDate" id="nextRevisionDate" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="frequency">Frequency</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" id="frequency" name="frequency" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a frequency">
                            <option>Monthly</option>
                            <option>Every 6 Months</option>
                            <option>Annually</option>
                            <option>Every 2 Years</option>
                        </select>
                    </div>
                </div>                
                <div class="modal-custom-h5"><span><h5>Location</h5></span></div>
                <div class="form-group">
                    <input type="hidden" name="department_selected" id="department_selected"/>
                    <label class="control-label col-sm-4" for="department">Department</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" id="search_dep" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                            <?php
                        foreach($data['allDepartments'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                        }?>                            
                        </select> 
                    </div>
                </div>
                
                <div class="modal-custom-h5"><span><h5>Links</h5></span></div>
                <div class="form-group">
                    <input type="hidden" name="resources_selected" id="resources_selected"/>
                    <label class="control-label col-sm-4" for="resources">Resources</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" id="search_res" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more resources" data-header="Close">
                               <?php
                            foreach($data['allResources'] as $group)
                            {
                                echo "<option value='{$group["id"]}'>{$group['as_name']}</option>";
                            }?>                            
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="occupations_selected" id="occupations_selected"/>
                    <label class="control-label col-sm-4" for="occupations">Occupations</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" id="search_oc" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more occupations" data-header="Close">
                             <?php
                            foreach($data['companyOccupations'] as $group)
                            {
                                ?>
                                    <option value="<?php echo $group["id"];?>"><?php echo $group["name"];?></option>
                                <?php
                            }?>                                   
                        </select> 
                    </div>
                </div>                
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="startRiskAssessmentBtn" name="startRiskAssessmentBtn" class="btn btn-success">Start</button>
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
<!-- Start Reschedule Modal -->
  <div class="modal fade" id="RescheduleRiskAssessment-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reschedule Risk Assessment</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="rescheduleRiskAssessmentForm" action="<?php echo BASE_URL;?>/index.php?action=RescheduleRiskAssessment&module=risk_assessment">
             <input type="hidden" id="reschedule_requested_id" name="reschedule_requested_id"/>
            <div class="form-group">
                    <label class="control-label col-sm-4" for="newDate">Reschedule Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="newDate" id="newDate" required placeholder=""> 
                    </div>
             </div>               
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="rescheduleRiskAssessmentBtn" name="rescheduleRiskAssessmentBtn" class="btn btn-success">Reschedule</button>
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->

<script>
$(function() {
    $("#startingDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datepicker();
    $("#nextRevisionDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datepicker();
    $("#newDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datepicker();
    $(".request_id").on("click",function()
    {
        id = $(this).attr("ncid");        
        $("#risk_requested_id").val(id);
    });
    
    $(".reschedule_id").on("click",function()
    {
        id = $(this).attr("ncid");        
        $("#reschedule_requested_id").val(id);
    });
    
    $("#riskAssessmentType").on("change",function()
    {
      getMoreInfoDropDownList(this,"#riskAssessmentTemplate","<?php echo BASE_URL;?>/index.php?module=risk_assessment&action=get_list_of_templates");
    });
    $('.selectpicker').on('change', function(){
              
        var selected = $(this," option:selected").val();
        focus_dropdown = $(this).parent().children('select').attr('id');
        switch(focus_dropdown)
        {
            case 'search_dep':
                $("#department_selected").val(selected);
                break;
           case 'search_res':
               $("#resources_selected").val(selected);
                break;                
            case 'search_oc':
                $("#occupations_selected").val(selected);
                break;
        }
        console.log( $("#resources_selected").val());
    });
});
</script>

<?php
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    