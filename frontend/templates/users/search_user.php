<?php
/**
 * Ajax script to search resource type
 * 
 * @package sheqonline
 * @author Sunny Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
//Include the common file
require_once '../../../config.php';

processAjax();

function processAjax() 
{
    global $db;
    $serchterm = $_POST["searchterm"];   

    $sql = "SELECT * FROM tbl_users WHERE firstname LIKE '%" . $serchterm ."%' OR lastname LIKE '%" . $serchterm ."%' OR username LIKE '%" . $serchterm ."%' LIMIT 20";
    $response = $db->getall($sql);
    
    if(!empty($response)){
    ?>
        <div class="table-responsive">          
            <table class="table">
            <thead>
                <tr>
                  <th>Firstname</th>
                  <th>Lastname</th>
                  <th>Username</th>
                  <th>Date created</th>
                  <th></th>
                </tr>
            </thead>
        <?php
        foreach($response as $userDetailArr){
           echo '<tr>
                    <td>' . $userDetailArr['firstname'] . '</td>
                    <td>' . $userDetailArr['lastname'] . '</td>
                    <td>' . $userDetailArr['username'] . '</td>
                    <td>' . date("D, d M y H:i:s O", strtotime($userDetailArr['date_created'])) . '</td>';        
                    if($userDetailArr['is_active']){
                        echo '<td><a data-toggle="tooltip" title="Deactivate user" href="index.php?module=users&action=deactivateUser&id=' . $userDetailArr['id'] . '"><span style="color:red" class="glyphicon glyphicon-remove"></span></td>';    
                    }else{
                        echo '<td><a data-toggle="tooltip" title="Activate user" href="index.php?module=users&action=activateUser&id=' . $userDetailArr['id'] . '"><span style="color:green" class="glyphicon glyphicon-ok"></span></td>'; 
                    }           
                echo "</tr>";
        }
        ?>
            </tbody>
            </table>
        </div>      
        <?php
    }else{
        echo "<p>No matched found for <b>$serchterm</b></p>";
    }
     
}
?> 