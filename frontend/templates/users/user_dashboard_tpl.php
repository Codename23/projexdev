<?php 
$title = 'Workspace';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/headers/menus/main-menu.php'); 
?>
<!--End of navigation-->   

<h1 class="heading-title">Workspace </h1>

<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/headers/menus/side-menu.php'); 
?> 

<div class="col-lg-10">
    
    <!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li class="active"><a href="workspace.php">Master Dashboard</a></li>
            <li><a href="dashboard_corrective_actions.php">My Corrective Actions</a></li>
            <li><a href="dashboard_company_calendar.php">Company Calendar</a></li>  
        </ul>
        </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-clock-o fa-fw"></i> Dashboard
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
<div class="col-lg-6">
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>
        <div class="col-lg-6">
<div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

    </div>
    <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
    
<div class="panel panel-default">
    <div class="panel-heading">Reported Incidents</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Incident Number</th>
                    <th>Date & Time</th>
                    <th>Report Type</th>
                    <th>Incident Type</th>
                    <th>Department</th>
                    <th>Investigator</th>
                    <th>Investigation Due Date</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>1254</td>
                    <td>15 June 2015 15:00</td>
                    <td>Near miss</td>
                    <td><a href="incident_view.php">Health and Safety</a></td>
                    <td>Information and Technology</td>
                    <td>Nick Botha</td>
                    <td>15 November 2016</td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                     <td>1</td>
                    <td>1254</td>
                    <td>15 June 2015 15:00</td>
                    <td>Near miss</td>
                    <td>Health and Safety</td>
                    <td>Information and Technology</td>
                    <td>Nick Botha</td>
                    <td>15 November 2016</td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1254</td>
                    <td>15 June 2015 15:00</td>
                    <td>Near miss</td>
                    <td>Health and Safety</td>
                    <td>Information and Technology</td>
                    <td>Nick Botha</td>
                    <td>15 November 2016</td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading">Upcoming SHEQ Plan Events</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Due Date</th>
                    <th>Frequency</th>
                    <th>Department</th>
                    <th>Responsible Person</th>
                    <th>Appointment</th>
                    <th>Relevant Form</th>
                    <th>Print Document</th>
                    <th>Schedule Event</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td>15 June 2015 15:00</td>
                    <td></td>
                    <td>Information and Technology</td>
                    <td>Nick Botha</td>
                    <td>15 November 2016</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>15 June 2015 15:00</td>
                    <td></td>
                    <td>Information and Technology</td>
                    <td>Nick Botha</td>
                    <td>15 November 2016</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>15 June 2015 15:00</td>
                    <td></td>
                    <td>Information and Technology</td>
                    <td>Nick Botha</td>
                    <td>15 November 2016</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>


    </div><!--End of row-->
    
    
    
</div><!--End of container-fluid-->


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
		
		<script type="text/javascript">
$(function () {
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Incident Statistic'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                'Minor',
                'Near Miss',
                'Serious',
                'Illness'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Series 1',
            data: [49.9, 71.5, 106.4, 129.2]

        }, {
            name: 'Series 2',
            data: [83.6, 78.8, 98.5, 93.4]

        }, {
            name: 'Series 3',
            data: [{y:48.9,color:'#5cb85c'}, {y:38.8,color:'#5cb85c'}, {y:39.3,color:'#5cb85c'}, {y:41.4,color:'#5cb85c'}]

        }, {
            name: 'Series 4',
            data: [{y:42.4,color:'red'}, {y:33.2,color:'red'}, {y:34.5,color:'red'}, {y:39.7,color:'red'}]

        }]
    });
    
});

Highcharts.setOptions({
     colors: ['rgb(67, 67, 72)', '#337ab7', '#5cb85c']
    });
var chart;

var options = {
    chart: {
        reflow: false,
        renderTo: 'container2',
        type: 'pie'
    },
    title: {
            text: 'Improvements',
            x: -20 //center
    },
    series: [{
        data: [["Training",33], ["PPE",33], ["Suppliers",33]]
    }]
};

    function createChart() {
        var chart = new Highcharts.Chart(options);
    }

jQuery(document).ready(function ($) {
    createChart();
    var windowWidth = $(window).width();

    $(window).resize(function () {
        if ($(window).width() !== windowWidth) {
            windowWidth = $(window).width();
            createChart();
        }
    });

    $('#button').on('click', function () {
        createChart();
    });
    var chart = $('#container2').highcharts();
    $('#chart-modal2').on('show.bs.modal', function() {
        $('#container2').css('visibility', 'hidden');
    });
    $('#chart-modal2').on('shown.bs.modal', function() {
        $('#container2').css('visibility', 'initial');
        chart.reflow();
    }); 
});
		</script>

<?php 
/*
 * 
 */
include_once('includes/footer.php');
?>  