<?php
/**
 * Created by PhpStorm.
 * User: Innovatorshill
 * Date: 3/16/2017
 * Time: 1:57 PM
 */
?>
<!-- Make Appointment Modal -->
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Appointment</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" name="makeAppointmentForm" action="<?php echo BASE_URL; ?>/index.php?module=sheq_teams&action=editAppointmentAssigment">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="appointmentGroup">Group</label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <select name="appointmentGroup" id="editAppointmentGroup" class="form-control">

                                <?php if($data['appointmentGroups']): foreach ($data['appointmentGroups'] as $group): ?>
                                    <?php if($data['appointmentAssignmentInfo']['group_id'] == $group['id']): ?>
                                        <option value="<?php echo $group['id']; ?>" selected="selected"><?php echo $group['sheqteam_name']; ?></option>
                                    <?php else: ?>
                                        <option value="<?php echo $group['id']; ?>"><?php echo $group['sheqteam_name']; ?></option>
                                    <?php endif; ?>
                                <?php endforeach;endif;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label col-sm-4" for="selectAppointment">Select Appointment</label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <select id="edit-group-appointment" name="appointmentSetting" class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="List of appointment types">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="sheqAppointmentDate">Appointment Date</label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <input type="text" class="form-control" name="sheqAppointmentDate" id="editSheqAppointmentDate" value="<?php echo $data['appointmentAssignmentInfo']['appointment_date']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="appointmentExpiryDate">Expiry Date</label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <input type="text" class="form-control" name="appointmentExpiryDate" id="editAppointmentExpiryDate"  value="<?php echo $data['appointmentAssignmentInfo']['appointment_expiry_date']; ?>" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="appointmentFullname">Name Surname</label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <select name="appointmentEmployee" class="selectpicker"  data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people" data-header="Close">
                                <?php if($data['appointmentEmployees']): foreach ($data['appointmentEmployees'] as $employee): ?>
                                    <?php if($data['appointmentAssignmentInfo']['appointed_employee_id'] == $employee['user_id']): ?>
                                        <option value="<?php echo $employee['user_id']; ?>" selected><?php echo $employee['employee_name']; ?></option>
                                    <?php else: ?>
                                        <option value="<?php echo $employee['user_id']; ?>"><?php echo $employee['employee_name']; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="appointmentId" value="<?php echo $data['appointmentAssignmentInfo']['id']; ?>" />
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" id="makeAppointmentBtn" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

<!--End Of Modal -->
<script>

    $(document).ready(function() {

        var default_group_id;

        var selected_otption = "";
        var  selectedOp;

        selectedOp =  <?php echo $data['appointmentAssignmentInfo']['settings_id'] ?>;
        default_group_id =  $('#editAppointmentGroup').find(":selected").val();

        $.post('<?php echo BASE_URL . "/index.php?module=ajax_request&action=ajaxGroupAppointments"; ?>', {'groupId':default_group_id}, function(data) {
            $.each(JSON.parse(data), function (key, elem) {
                if(selectedOp ==elem.id){
                    selected_otption = "selected"
                }else{
                    selected_otption = ""
                }
                $('#edit-group-appointment').append('<option  value="' + elem.id + '" ' + selected_otption+'>' + elem.name + '</option>');

                $('.selectpicker').selectpicker('refresh');
            });
        });


    $('.selectpicker').selectpicker('refresh');
    var selectedAppointment;
    $('#editSheqAppointmentDate').datepicker( {
    onSelect: function(date) {
    var exp_date;
    selectedAppointment = $( "#edit-group-appointment option:selected" ).val();

    $.post('<?php echo BASE_URL . "/index.php?module=ajax_request&action=ajaxNextOccurrence"; ?>', {'appointmentId':selectedAppointment,'appointmentDate':date}, function(data){
    exp_date = JSON.parse(data);
    $("#editAppointmentExpiryDate").val(exp_date[0].expiry_date);

    });

    },
    dateFormat:"yy/mm/dd",
    minDate: new Date(),
    changeYear:true,
    changeMonth:true,
    yearRange:"1900:3000"
    });


    $('#editAppointmentGroup').on('change', function() {
    $('#edit-group-appointment').empty();
    var group_id;

    group_id = $(this).find("option:selected").val();

    $.post('<?php echo BASE_URL . "/index.php?module=ajax_request&action=ajaxGroupAppointments"; ?>', {'groupId':group_id}, function(data){
    $.each(JSON.parse(data), function(key, elem) {
    //console.log(value+" - "+key);

    if(selectedOp ===elem.id ){
            selected_otption = "selected"
    }
    $('#edit-group-appointment').append('<option  value="' + elem.id + '"' + selected_otption+'>' + elem.name + '</option>');
    //options.append(new Option(elem.id, elem.name ));
    $('.selectpicker').selectpicker('refresh');
    });
    });
    });


    });



</script>
