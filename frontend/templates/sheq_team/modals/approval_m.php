<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017/03/15
 * Time: 11:36 AM
 */
?>

<!--Formal Appointment Modal -->
<div class="modal fade" id="formalAppointment-modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Formal Appointment</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" name="formalAppointmentForm" action="sheqteams.php">

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="uploadSignedAppointment"></label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <div class="fileUpload btn btn-primary">
                                <span><span class="glyphicon glyphicon-upload"></span> Upload Unsigned Appointments</span>
                                <input type="file" class="upload" />
                            </div>
                        </div>
                    </div><br/>

                    <div class="form-group">
                        <span class="control-label col-sm-3" for="signedInFile"></span>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <p><b>Employee acknowledges the signed & placed in file document</b></p>
                            <label class="radio-inline"><input type="radio" name="signedInFile" id="signedInFile1" value="0" required>Yes</label>
                            <label class="radio-inline"><input type="radio" name="signedInFile" id="signedInFile2" value="1" >No</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                            <button type="submit" class="btn btn-success">Approve</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!--End Of Modal -->
