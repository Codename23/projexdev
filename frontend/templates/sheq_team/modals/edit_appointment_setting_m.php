<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017/03/14
 * Time: 1:59 PM
 */
?>
<!-- Set Up Appointment Modal -->
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Appointment</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" name="setupAppointmentForm" action="<?php echo BASE_URL; ?>/index.php?module=sheq_teams&action=editSheqTeamSettings">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="appointmentGroup">Group</label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <select name="appointmentGroup" class="form-control">
                                <option value selected disabled>Please select a group</option>
                                <?php if($data['appointmentGroups']): foreach ($data['appointmentGroups'] as $group): ?>
                                    <?php if($data['appointmentSettingInfo']['appointment_group_id'] == $group['id']): ?>
                                        <option value="<?php echo $group['id']; ?>" selected><?php echo $group['sheqteam_name']; ?></option>
                                    <?php else: ?>
                                        <option value="<?php echo $group['id']; ?>"><?php echo $group['sheqteam_name']; ?></option>
                                    <?php endif; ?>
                                <?php endforeach;endif;?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="appointmentName">Appointment Name</label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <input type="text" class="form-control" id="appointmentName"  name="appointmentName" value="<?php echo $data['appointmentSettingInfo']['appointment_name']  ; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="appointmentDocLink">Appointment Document Link</label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <select name="appointmentDocLink" class="form-control">
                                <option value="" selected disabled>Please select a document</option>
                                <?php if($data['appointmentDocuments'] ): foreach($data['appointmentDocuments'] as $appointmentDocuments) : ?>
                                    <?php if($data['appointmentSettingInfo']['doc_id'] == $appointmentDocuments['id']): ?>
                                        <option value="<?php echo $appointmentDocuments['id']; ?>" selected><?php echo $appointmentDocuments['filename']; ?></option>
                                    <?php else: ?>
                                        <option value="<?php echo $appointmentDocuments['id']; ?>"><?php echo $appointmentDocuments['filename']; ?></option>
                                    <?php endif; ?>
                                <?php endforeach;endif;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="renewalFrequency">Renewal Frequency</label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <select name="renewalFrequency" class="form-control">
                                <option value="" selected disabled>Please select a frequency</option>
                                <?php if($data['frequencies']): foreach ($data['frequencies'] as $frequency ): ?>

                                    <?php if($data['appointmentSettingInfo']['renewal_frequency'] == $frequency['id']): ?>
                                        <option value="<?php echo $frequency['id']; ?> " selected><?php echo $frequency['frequency_description']; ?> </option>
                                    <?php else: ?>
                                        <option value="<?php echo $frequency['id']; ?> "><?php echo $frequency['frequency_description']; ?> </option>
                                    <?php endif; ?>
                                <?php endforeach;endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="settingId" value="<?php echo $data['appointmentSettingInfo']['id']; ?>" />
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" id="setupAppointmentBtn" class="btn btn-success">Update</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
<!--End Of Modal -->
