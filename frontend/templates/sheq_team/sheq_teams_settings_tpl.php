<?php
/*
 * Header file
 */
$title = 'Sheq Team Settings';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
    <div class="container-fluid">
        <!--navigation-->
        <?php
        /*
         * Include  main menu from the include file
         */
        include_once('frontend/templates/menus/main-menu.php');
        ?>
        <?php
        /*
         * Include side menu from the include file
         */
        include_once('frontend/templates/menus/side-menu.php');
        ?>
        <!--End of navigation-->
        <div class="col-lg-10">
            <!--sub menu-->
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-pills nav-justified topbar-menu">
                        <li><a href="index.php?module=sheq_teams&action=viewAllSheqTeamMembers">All SHEQ Team Memebers</a></li>
                        <li class="active"><a href="index.php?module=sheq_teams&action=viewAllSheqTeamSettings">Settings</a></li>
                    </ul>
                </div>
            </div>
            <!--End of sub menu-->
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-inline" method="post" action="">
                        <div class="form-group">
                            <a href="#setupAppointment-modal"   data-toggle="modal" data-target="#setupAppointment-modal"><button type="button" id="setupAppointment" class="btn btn-success">Set Up Appointment</button></a>
                        </div>
                    </form>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Settings</div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                            <tr>
                                <th>Group</th>
                                <th>Appointment</th>
                                <th>Link to Appointment Document</th>
                                <th>Frequency</th>

                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if($data['sheqTeamSettings']): foreach ($data['sheqTeamSettings'] as $sheqSettings): ?>
                            <tr>
                                <td><?php echo $sheqSettings['sheqteam_name']; ?></td>
                                <td><?php echo $sheqSettings['appointment_name']; ?></td>
                                <td><?php echo $sheqSettings['filename']; ?></td>
                                <td><?php echo $sheqSettings['frequency_description']; ?></td>

                                <td>
                                    <a data-toggle="sheqModal"  href="index.php?action=modalEditAppointmentSettings&module=modal_request&settingId=<?php echo $sheqSettings['id']; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                </td>
                            </tr>
                            <?php endforeach; endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- Set Up Appointment Modal -->
                <div class="modal fade" id="setupAppointment-modal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Set Up Appointment</h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal" method="post" name="setupAppointmentForm" action="<?php echo BASE_URL; ?>/index.php?module=sheq_teams&action=addSheqTeamSettings">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="appointmentGroup">Group</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select name="appointmentGroup" class="form-control">
                                                <option value selected disabled>Please select a group</option>
                                                <?php if($data['appointmentGroups']): foreach ($data['appointmentGroups'] as $group): ?>
                                                <option value="<?php echo $group['id']; ?>"><?php echo $group['sheqteam_name']; ?></option>
                                             <?php endforeach;endif;?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="appointmentName">Appointment Name</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <input type="text" class="form-control" id="appointmentName"  name="appointmentName" required placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="appointmentDocLink">Appointment Document Link</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select name="appointmentDocLink" class="form-control">
                                                <option value="" selected disabled>Please select a document</option>
                                                <?php if($data['appointmentDocuments'] ): foreach($data['appointmentDocuments'] as $appointmentDocuments) : ?>
                                                <option value="<?php echo $appointmentDocuments['id']; ?>"><?php echo $appointmentDocuments['filename']; ?></option>
                                                <?php endforeach;endif;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="renewalFrequency">Renewal Frequency</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select name="renewalFrequency" class="form-control">
                                                <option value="" selected disabled>Please select a frequency</option>
                                                <?php if($data['frequencies']): foreach ($data['frequencies'] as $frequency ): ?>
                                                <option value="<?php echo $frequency['id']; ?> "><?php echo $frequency['frequency_description']; ?> </option>
                                                <?php endforeach;endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-4 col-sm-8">
                                            <button type="submit" id="setupAppointmentBtn" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End Of Modal -->


            </div>
        </div><!--End of row-->
    </div><!--End of container-fluid-->

<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>
<script>

    $(document).on('click','[data-toggle="sheqModal"]',function(e){

        $('#sheqModal').remove();
        $('.modal-backdrop').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),

            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    });
</script>
