<?php
/*
 * Header file
 */
$title = 'Sheq Team';
include_once('frontend/templates/headers/default_header_tpl.php');

?>
<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */
    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <?php
    /*
     * Include side menu from the include file
     */
    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->
    <div class="col-lg-10">
        <!--sub menu-->
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-justified topbar-menu">
                    <li class="active"><a href="index.php?module=sheq_teams&action=viewAllSheqTeamMembers">All SHEQ Team Memebers</a></li>
                    <li><a href="index.php?module=sheq_teams&action=viewAllSheqTeamSettings">Settings</a></li>
                </ul>
            </div>
        </div>
        <!--End of sub menu-->
        <div class="panel panel-default">
            <div class="panel-body">
                <a href="#makeAppointment-modal"   data-toggle="modal" data-target="#makeAppointment-modal"><button type="button" id="makeAppointment" class="btn btn-success">Make Appointment</button></a>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Search SHEQ Team Details</div>
            <div class="panel-body">
                <form class="form-inline" method="post" name="search" action="<?php echo BASE_URL; ?>/index.php?module=sheq_teams&action=viewAllSheqTeamMembers">
                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <p><b>Appointment Types</b></p>
                                <select name="appointmentNameSearch[]" class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="List of appointment types" data-header="Close">
                                   <?php if($data["sheqTeamSettings"]) : foreach ($data['sheqTeamSettings'] as $appointment): ?>
                                    <option value="<?php echo $appointment['id']; ?>"><?php echo $appointment['appointment_name']; ?></option>
                                   <?php endforeach;endif;?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <p><b>Group Types</b></p>
                                <select name="groupSearch[]" class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more group types" data-header="Close">
                                    <option value="" disabled>Please select a group</option>
                                    <?php if($data['appointmentGroups']): foreach ($data['appointmentGroups'] as $group): ?>
                                        <option value="<?php echo $group['id']; ?>"><?php echo $group['sheqteam_name']; ?></option>
                                    <?php endforeach;endif;?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <p><b>Status</b></p>
                                <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more status types" data-header="Close">
                                    <option value="1">Due in a month</option>
                                    <option value="2">Due in 2 months</option>
                                    <option value="3">Overdue</option>
                                    <option value="4">Good</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div style="margin-left: 30px;">
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Search">
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <a href=""><h6 style="color: #5cb85c;"><span class="glyphicon glyphicon-print"></span> <b>Multiple Print</b></h6></a>

        <div class="panel panel-default">
            <div class="panel-heading">SHEQ Team Details</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" class="table table-hover" id="sheqTeamTbl">
                        <thead>
                        <tr>
                            <th><input type="checkbox" class="chkbox"></th>
                            <th>Group</th>
                            <th>Appointment</th>
                            <th>Name Surname</th>
                            <th>Date Of Appointment</th>
                            <th>Expiry Date</th>
                            <th>Document Name</th>
                            <th>Status</th>
                            <th>Approval</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($data['assignedAppointments']): foreach ($data['assignedAppointments'] as $appointment): ?>
                            <?php global $objEmployee;

                            $appointment_date = strtotime($appointment['appointment_date']);
                            $expiry_date = strtotime($appointment['appointment_expiry_date']);
                            ?>
                        <tr>
                            <td><input type="checkbox" class="chkbox"></td>
                            <td><?php echo $appointment['sheqteam_name']; ?></td>
                            <td><?php echo $appointment['appointment_name']; ?></td>
                            <td><?php echo $objEmployee->getEmployeeNames($appointment['appointed_employee_id']); ?></td>
                            <td><?php echo date("j F Y", $appointment_date); ?></td>
                            <td><?php echo date("j F Y", $expiry_date); ?></td>
                            <td><?php echo $appointment['filename']; ?></td>

                            <td>
                            <?php
                            if (time() > strtotime($appointment['appointment_expiry_date']) && $appointment['is_approved'] == 0 ){ ?>
                                <span class="label label-important status-overdue" title="Overdue" data-toggle="tooltip" data-original-title="Overdue">
                                 Overdue
                            </span>
                            <?php }elseif((date("m",strtotime('+1 month',$appointment_date)) == date("m",$expiry_date) && date("m") == date("m",$appointment_date)) && $appointment['frequency_name'] != '7D' && $appointment['is_approved'] == 0 ){ ?>
                                <span class="label label-important status-onemonth" title="Due in a month" data-toggle="tooltip" data-original-title="Due in a month">
                                    Due in a month
                                </span>
                           <?php }elseif((date("m",strtotime('+2 month',$appointment_date)) == date("m",$expiry_date) && date("m") == date("m",$appointment_date)) && $appointment['frequency_name'] != '7D' && $appointment['is_approved'] == 0 ){ ?>

                                <span class="label label-important status-twomonths" title="Due in two months" data-toggle="tooltip" data-original-title="Due in two months">
                                    Due in two months
                                 </span>

                           <?php }else{ ?>
                                <span class="label label-important status-good" title="Good" data-toggle="tooltip" data-original-title="Good">
                                 GOOD
                                </span>
                            <?php } ?>

                           </td>
                            <td>
                                <?php if($appointment['is_approved'] == 0 ){
                                    echo "Not Appointed";
                                }else{
                                    echo "Appointed";
                                }
                                ?>



                            </td>
                            <td><a href=""><span class="glyphicon glyphicon-print"></span> Print</a></td>
                            <td><a href="#formalAppointment-modal" class="approve_appointment_m" data-toggle="modal" data-id="<?php echo $appointment['id']; ?>" data-target="#formalAppointment-modal">Approve/Upload</a></td>
                            <td><a href="">View</a></td>

                            <td>
                                <a data-toggle="sheqModal"  href="index.php?action=modalEditAppointmentAssignment&module=modal_request&appointmentId=<?php echo $appointment['id']; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                        </tr>
                        <?php endforeach;endif; ?>

                        </tbody>
                    </table>

            </div>
        </div>

        <!-- Make Appointment Modal -->
        <div class="modal fade" id="makeAppointment-modal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Make Appointment</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" method="post" name="makeAppointmentForm" action="<?php echo BASE_URL; ?>/index.php?module=sheq_teams&action=addAppointment">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="appointmentGroup">Group</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <select name="appointmentGroup" id="appointmentGroup" class="form-control">
                                        <option value selected disabled>Please select a group</option>
                                        <?php if($data['appointmentGroups']): foreach ($data['appointmentGroups'] as $group): ?>
                                            <option value="<?php echo $group['id']; ?>"><?php echo $group['sheqteam_name']; ?></option>
                                        <?php endforeach;endif;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label col-sm-4" for="selectAppointment">Select Appointment</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <select id="group-appointment" name="appointmentSetting" class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="List of appointment types">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="sheqAppointmentDate">Appointment Date</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="sheqAppointmentDate" id="sheqAppointmentDate" required placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="appointmentExpiryDate">Expiry Date</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="appointmentExpiryDate" id="appointmentExpiryDate" required readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="appointmentFullname">Name Surname</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <select name="appointmentEmployee[]" class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people" data-header="Close">
                                        <?php if($data['appointmentEmployees']): foreach ($data['appointmentEmployees'] as $employee): ?>
                                        <option value="<?php echo $employee['user_id']; ?>"><?php echo $employee['employee_name']; ?></option>
                                       <?php endforeach; endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" id="makeAppointmentBtn" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--End Of Modal -->

            <!--Formal Appointment Modal -->
            <div class="modal fade" id="formalAppointment-modal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Formal Appointment</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" method="post" name="makeAppointmentForm" action="<?php echo BASE_URL; ?>/index.php?module=sheq_teams&action=approveAppointment">
                                <!--<div class="form-group">
                                    <label class="control-label col-sm-4" for="printAppoinment"></label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                    <button type="button" id="printAppoinment" class="btn btn-primary"><span class="glyphicon glyphicon-print"></span> Print Appointment</button>
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="uploadSignedAppointment"></label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <div class="fileUpload btn btn-primary">
                                            <span><span class="glyphicon glyphicon-upload"></span> Upload Unsigned Appointments</span>
                                            <input type="file" class="upload" />
                                        </div>
                                    </div>
                                </div><br/>

                                <div class="form-group">
                                    <span class="control-label col-sm-3" for="signedInFile"></span>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <p><b>Employee acknowledges the signed & placed in file document</b></p>
                                        <label class="radio-inline"><input type="radio" name="signedInFile" id="signedInFile1" value="1" required>Yes</label>
                                        <label class="radio-inline"><input type="radio" name="signedInFile" id="signedInFile2" value="0" >No</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="appointmentId" id="appointmentId" />
                                    <div class="col-sm-offset-3 col-sm-8">
                                        <button type="submit" class="btn btn-success">Approve</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
            <!--End Of Modal -->


    </div>
</div><!--End of container-fluid-->
<script src="js/jquery.tablecheckbox.js"></script>
<script src="js/bootstrap-select.js"></script>
<script>

    $(document).ready(function() {

        var selectedAppointment;
        $('#sheqAppointmentDate').datepicker( {
            onSelect: function(date) {
                var exp_date;
                selectedAppointment = $( "#group-appointment option:selected" ).val();

                $.post('<?php echo BASE_URL . "/index.php?module=ajax_request&action=ajaxNextOccurrence"; ?>', {'appointmentId':selectedAppointment,'appointmentDate':date}, function(data){
                    exp_date = JSON.parse(data);
                    $("#appointmentExpiryDate").val(exp_date[0].expiry_date);

                });

            },
            dateFormat:"yy/mm/dd",
            minDate: new Date(),
            changeYear:true,
            changeMonth:true,
            yearRange:"1900:3000"
        });


        $('#appointmentGroup').on('change', function() {
            $('#group-appointment').empty();
            var group_id;
            group_id = $(this).find("option:selected").val()
            $.post('<?php echo BASE_URL . "/index.php?module=ajax_request&action=ajaxGroupAppointments"; ?>', {'groupId':group_id}, function(data){
                $.each(JSON.parse(data), function(key, elem) {
                    //console.log(value+" - "+key);
                    $('#group-appointment').append('<option value="' + elem.id + '">' + elem.name + '</option>');
                    //options.append(new Option(elem.id, elem.name ));
                    $('.selectpicker').selectpicker('refresh');
                });
            });
        });


        $(document).on("click", ".approve_appointment_m", function () {
            var appointmentId = $(this).data('id');
            $(".modal-body #appointmentId").val( appointmentId );

        });
    });



</script>
<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>
<script>

        $(document).on('click','[data-toggle="sheqModal"]',function(e){

            $('#sheqModal').remove();
            $('.modal-backdrop').remove();
            e.preventDefault();
            var $this=$(this),
                $remote=$this.data('remote')||$this.attr('href'),

                $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
            $('body').append($modal);
            //$modal.modal();
            $modal.modal({backdrop: 'static', keyboard: false});
            $modal.load($remote);
        });
 </script>
