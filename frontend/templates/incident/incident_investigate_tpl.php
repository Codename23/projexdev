<?php 
$title = 'Incident Investigate';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php include_once('frontend/templates/menus/main-menu.php');  ?>
<!--End of navigation-->    

<?php include_once('frontend/templates/menus/side-menu.php'); ?>
	
<div class="col-lg-10">
<ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="incident.php">Report</a></li>
        <li class="active"><a href="incident_investigate.php">Open Investigations</a></li>
        <li><a href="">Report History</a></li>
        <li><a href="">Research</a></li>
</ul>
<div class="panel-group">
    <form  enctype="multipart/form-data" class="form-horizontal" name="incidentAddForm" id="incidentAddForm" method="post" action="">
    <div class="panel panel-default">
    <div class="panel-heading">Investigate</div>
    <div class="panel-body"> 
        <div class="form-group">
            <label class="control-label col-sm-4" for="leadInvestigate">Lead Investigate</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" id="incidentDepartment" name="leadInvestigate">
                <option value="1">Sunnyboy Mathole</option>
                <option value="2">Warren Windvogel</option>
                <option value="3">Nick Botha</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="assistanceInvestigate">Assistance Investigate</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" id="incidentDepartment" name="leadInvestigate">
                <option value="1">Sunnyboy Mathole</option>
                <option value="2">Warren Windvogel</option>
                <option value="3">Nick Botha</option>
            </select>
            </div>
        </div>
    </div>
    </div>
    <!--End of panel panel-default-->
    <div class="panel panel-default">
    <div class="panel-heading">Interviews</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-4" for="witnessName">Witness 1</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="form-control" id="witnessName" name="witnessName">
                <option value="1">Sunnyboy Mathole</option>
                <option value="2">Warren Windvogel</option>
                <option value="3">Nick Botha</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="witnessOpinionEvent">Witness Opinion of Event</label>
            <div class="col-lg-6 col-md-6 col-sm-8">
            <textarea class="form-control tinyMCEselector" rows="5" id="witnessOpinionEvent" name="witnessOpinionEvent"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="investigatorQuestion">Investigator Question</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="investigatorQuestion" name="investigatorQuestion" placeholder="Please enter investigation question" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="witnessAnswer">Witness Answer</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" id="witnessAnswer" name="witnessAnswer" placeholder="Please enter witness answer" required>
            </div>
        </div>
    </div>
    </div>
    <!--End of panel panel-default-->
    <!--End of panel panel-default-->
    <div class="panel panel-default">
    <div class="panel-heading">Upload or View Documents or Evidence</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-4" for="evidencePhotos">Photos of Incident</label>
            <div class="col-lg-2 col-md-2 col-sm-8">
            <label class="control-label"><a href="#">view</a></label>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="file" id="evidencePhotos" name="evidencePhotos" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="evidenceDocuments">Documents</label>
            <div class="col-lg-2 col-md-2 col-sm-8">            
            <label  class="control-label"><a href="#">view</a></label>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="file" id="evidenceDocuments" name="evidenceDocuments" required>
            </div>

        </div>
    </div>
    </div>
    <!--End of panel panel-default-->
    <div class="panel panel-default">
    <div class="panel-heading">Cause Analyses</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-4" for="employeeName">Employee Name</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
              <select class="form-control" id="witnessName" name="employeeName">
                <option value="1">Sunnyboy Mathole</option>
                <option value="2">Warren Windvogel</option>
                <option value="3">Nick Botha</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="causeAnalyses"></label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" id="causeAnalyses" name="causeAnalyses">
                <option value="1">Human Error</option>
                <option value="2">Task Experience</option>
                <option value="3">Organizational</option>
                <option value="4">Personal Capabilities</option>
                <option value="5">Training and Educational</option>
                <option value="6">Supervision</option>
                <option value="7">Behaviour</option>
             </select>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" id="optHumanError" name="subCauseAnalyses">
                <option value="1">Operation without Authority</option>
                <option value="2">Operation at unsafe Speed</option>
            </select>
            <select class="form-control" id="optTaskExperience" name="subCauseAnalyses">
                <option value="1">Non-Routine task</option>
                <option value="2">New employee</option>
            </select>
            <select class="form-control" id="optOrganizational" name="subCauseAnalyses">
                <option value="1">Long work hours</option>
                <option value="2">Task load</option>
            </select>
            <select class="form-control" id="optPersonalCapabilities" name="subCauseAnalyses">
                <option value="1">Lack of knowledge</option>
                <option value="2">Physical or mental defect</option>
            </select>
            <select class="form-control" id="optTrainignEducation" name="subCauseAnalyses">
                <option value="1">Inadequate Induction</option>
                <option value="2">Inadequate Task Training</option>
            </select>
            <select class="form-control" id="optSupervisions" name="subCauseAnalyses">
                <option value="1">Generic Supervision not available</option>
                <option value="2">Specific supervision not available</option>
            </select>
            <select class="form-control" id="optBehaiour" name="subCauseAnalyses">
                <option value="1">Inadequate critical behavioural analysis</option>
                <option value="2">Inadequate behavioural training</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="resourceOptions"></label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" id="resourceOptions" name="resourceOptions">
                <option value="1">Process induced by human error</option>
                <option value="2">Design</option>
                <option value="3">Acquisition</option>
                <option value="4">Department</option>
                <option value="5">Maintenance</option>
                <option value="6">Supervision</option>
             </select>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" id="optInducedhumanError" name="subResourceOptions">
                <option value="1">Sub-Standard Tools</option>
                <option value="2">Sub-Standard Parts</option>
            </select>
            <select class="form-control" id="optDesign" name="subCauseAnalyses">
                <option value="1">Task/Processed Hazards not identified</option>
                <option value="2">Interphasing task hazard not identified</option>
            </select>
            <select class="form-control" id="optAcqusition" name="subCauseAnalyses">
                <option value="1">Sub-Standard Tools</option>
                <option value="2">Sub-Standard Parts</option>
            </select>
            <select class="form-control" id="optDepartment" name="subCauseAnalyses">
                <option value="1">Inadequate SHEQ deployment</option>
                <option value="2">Inadequate ownership development</option>
            </select>
            <select class="form-control" id="optSupervision" name="subCauseAnalyses">
                <option value="1">Generic  Supervision not available</option>
                <option value="2">Specific supervision not available</option>
            </select>
            <select class="form-control" id="optMaintenance" name="subCauseAnalyses">
                <option value="1">Inadequate PM</option>
                <option value="2">Inadequate maintenance of RC measures</option>
            </select>
            </div>
        </div>
    </div>
    </div>
    <!--End of panel panel-default-->
    <div class="panel panel-default">
    <div class="panel-heading">Findings</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-4" for="findingsByLeadInvestigator">By Lead Investigator</label>
            <div class="col-lg-6 col-md-6 col-sm-8">
            <textarea class="form-control tinyMCEselector" name="findingsByLeadInvestigator" id="findingsByLeadInvestigator"></textarea>
            </div>
        </div>
    </div>
    </div>
    <!--End of panel panel-default-->
    <div class="panel panel-default">
    <div class="panel-heading">Recommendation</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-4" for="recommendationByLeadInvestigator">By Lead Investigator</label>
            <div class="col-lg-6 col-md-6 col-sm-8">
            <textarea class="form-control tinyMCEselector" name="recommendationByLeadInvestigator" id="recommendationByLeadInvestigator"></textarea>
            </div>
        </div>
    </div>
    </div>
    <!--End of panel panel-default-->
    </form>
</div>
<!--End of panel-group-->

   </div>
  </div><!--End of row-->
</div><!--End of container-fluid-->

<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>   