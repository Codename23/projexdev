<?php
/**
 * Ajax script to search resource type
 * 
 * @package sheqonline
 * @author Sunny Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
//Include the common file
require_once '../../../config.php';

processAjax();

function processAjax() 
{
    global $db;
    $categoryid = $_POST["categoryid"];   
    
    $sql = "SELECT * FROM tbl_incident_type WHERE category_id = " . $db->sqs($categoryid);
    $response = $db->getall($sql);
    
    if(!empty($response)){
    ?>
    <select class="form-control" id="resourceType" name="resourceType" required>
    <?php
    foreach($response as $resourceTypeDetailsArr){
       echo '<option value="' . $resourceTypeDetailsArr['id'] . '">' . $resourceTypeDetailsArr['type_name'] . '</option>';        
    }
    ?>
    </select>     
    <?php
    }
     
}
?> 