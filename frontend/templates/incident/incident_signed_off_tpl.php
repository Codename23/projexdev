<?php 
$title = 'Active Investigations';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php include_once('frontend/templates/menus/main-menu.php');  ?>
<!--End of navigation-->    

<?php include_once('frontend/templates/menus/side-menu.php'); ?>	
<div class="col-lg-10">
  
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
            <li><a href="index.php?module=incidents&action=incidentManagement">All Incidents</a></li>
            <li><a href="index.php?module=incidents&action=reportIncident">Report Incident</a></li>
            <li><a href="index.php?module=incidents&action=incidentInvestigation">Active Investigations</a></li>
            <li class="active"><a href="index.php?module=incidents&action=incidentSignoff">Signed Off</a></li>
            <li><a href="#">Research</a></li>
            <li><a href="#">Settings</a></li>
    </ul>
    </div>
</div>  

    <div class="panel panel-default">
    <div class="panel-heading">Signed Off</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Incident Number</th>
                    <th>Date & Time</th>
                    <th>Department</th>
                    <th>Incident Type</th>
                    <th>Category</th>
                    <th>Investigator</th>
                    <th>Date Completed</th>
                    <th>View Investigation</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>1254</td>
                    <td>15 June 2015 15:00</td>
                    <td>Information and Technology</td>
                    <td>Near miss</td>
                    <td></td>
                    <td>Nick Botha</td>
                    <td>15 November 2016</td>
                    <td></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1254</td>
                    <td>15 June 2015 15:00</td>
                    <td>Information and Technology</td>
                    <td>Near miss</td>
                    <td></td>
                    <td>Nick Botha</td>
                    <td>15 November 2016</td>
                    <td></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1254</td>
                    <td>15 June 2015 15:00</td>
                    <td>Information and Technology</td>
                    <td>Near miss</td>
                    <td></td>
                    <td>Nick Botha</td>
                    <td>15 November 2016</td>
                    <td></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of panel-default-->
 
   </div>
  </div><!--End of row-->
</div><!--End of container-fluid-->

<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>  