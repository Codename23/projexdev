<?php
$title = 'Incident report';
include_once('frontend/templates/headers/default_header_tpl.php');
global  $objLanguage;
?> 
<div class="container-fluid">

    <!--navigation--> 
    <?php include_once('frontend/templates/menus/main-menu.php'); ?>
    <!--End of navigation-->   

    <?php include_once('frontend/templates/menus/side-menu.php'); ?>

    <div class="col-lg-10">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li class="active"><a href="index.php?module=incidents&action=incidentManagement">All Incidents</a></li>
            <li><a href="index.php?module=incidents&action=reportIncident">Report Incident</a></li>
            <li><a href="index.php?module=incidents&action=incidentInvestigation">Active Investigations</a></li>
            <li><a href="index.php?module=incidents&action=incidentSignoff">Signed Off</a></li>
            <li><a href="#">Research</a></li>
            <li><a href="#">Settings</a></li>
        </ul>
        <div class="panel-group">
            <form  enctype="multipart/form-data" class="form-horizontal" name="incidentAddForm" id="incidentAddForm" method="post" action="index.php?action=updateIncident&module=incidents">
                <div class="panel panel-default">
                    <div class="panel-heading">New Incident</div>
                    <div class="panel-body"> 
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentCategory">Incident Category</label>
                            <div class="col-lg-4 col-md-4 col-sm-8"> 
                                <select class="form-control" id="incidentCategory" name="incidentCategory" required>
                                    <?php
                                    foreach ($data["getAllIncidentCategory"] as $incidentCategoryDetails) {
                                        echo '<option value="' . $incidentCategoryDetails["id"] . '">' . $incidentCategoryDetails["category_name"] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentType">Incident Type</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select class="form-control" id="incidentType" name="incidentType" required> 
                                </select>
                            </div>
                        </div>
                        <div id="showhideincident" class="form-group hide">
                            <label class="control-label col-sm-4" for="incidentNumber">Incident Number</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input disabled type="text" class="form-control" id="incidentNumber" name="incidentNumber">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentDate">Date & Time of Incident</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="incidentDate" name="incidentDate" required >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentDepartment">Department</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select class="form-control" id="incidentDepartment" name="incidentDepartment">
                                    <?php
                                    if (!empty($data["getAllDepartment"])) {
                                        foreach ($data["getAllDepartment"] as $departmentDetailsArr) {
                                            echo '<option value="' . $departmentDetailsArr["id"] . '">' . $departmentDetailsArr["department_name"] . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentDepartmentManager">Department Manager</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select class="form-control" name="incidentDepartmentManager" id="incidentDepartmentManager">
                                    <option value="1">Nick Botha</option>
                                    <option value="2">Warren Windvogel</option>
                                    <option value="3">Lerato Chiloane</option>
                                    <option value="4">Sunnyboy Mathole</option>
                                    <option value="5">Bill Blurr</option>
                                    <option value="6">Nango Kala</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentShift">Shift</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select class="form-control" id="incidentShift" name="incidentShift">
                                    <option selected disabled>Shift type</option>
                                    <option value="daytime">Daytime</option>
                                    <option value="night">Night</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                        </div> 
                         <div id="hideincidentShift" class="form-group hidden">
                            <label class="control-label col-sm-4" for="otherincidentShift">Other Shift</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="otherincidentShift" id="otherincidentShift" placeholder="Please enter shift">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="reportedPerson">Reported Person</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select class="form-control" id="reportedPersonDepartment" name="reportedPersonDepartment">
                                    <option selected disabled>Department</option>
                                    <?php
                                    if (!empty($data["getAllDepartment"])) {
                                        foreach ($data["getAllDepartment"] as $departmentDetailsArr) {
                                            echo '<option value="' . $departmentDetailsArr["id"] . '">' . $departmentDetailsArr["department_name"] . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="control-label col-sm-4"></span>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="hidden" name="reportedPersonID" id="reportedPersonID" value="">
                                <input type="text" class="form-control" name="reportedPerson" id="reportedPerson" placeholder="Please search a name & surname">
                            </div>
                        </div>
                        <!--Show Search Reported person -->
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="reportedPersonResults"></label>
                            <div class="col-lg-6 col-md-6 col-sm-8">
                            <table id="reportedPersonResults" class="table table-hover center hidden">
                               <tr>
                                   <th>Branch Name</th>
                                   <th>Department</th>
                                   <th>Fullname</th>
                                   <th></th>
                                </tr>
                            </table>
                            <div id="reportedPersonNotFound" class="hidden">
                                <a href="#addNewReportedPerson-modal" data-toggle="modal" data-target="#addNewReportedPerson-modal">
                                    <button type="button" id="addNewReportedPerson" class="btn btn-success">Add New Employee</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        <!--show reported Person -->
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="showReportedPerson"></label>
                            <div class="col-lg-6 col-md-6 col-sm-8">
                                <table id="showReportedPerson" style="margin-bottom:0;" class="table hidden">
                                    <tr>
                                        <th>Firstname</th>
                                        <th>Lastname</th>
                                        <th>Delete</th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--End of Reported Person -->
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentTask">Task while incident occurred</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <textarea class="form-control" rows="5" id="incidentTask" name="incidentTask"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentDescription">Incident Description</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <textarea class="form-control" rows="5" id="incidentDescription" name="incidentDescription"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4 col-xs-12" for="reportToAuthorities">Report to authorities</label>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="checkbox"><label><input type="checkbox" class="reportToAuthorities" name="reportToAuthorities[]" value="1">SAPS</label></div>
                                <div class="checkbox"><label><input type="checkbox" class="reportToAuthorities" name="reportToAuthorities[]" value="2">Compensation commissioner</label></div>
                                <div class="checkbox"><label><input type="checkbox" class="reportToAuthorities" name="reportToAuthorities[]" value="3">Department of Labor</label></div>
                                <div class="checkbox"><label><input type="checkbox" class="reportToAuthorities" name="reportToAuthorities[]" value="4">Fire Department</label></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="uploadPhotos">Upload Photos</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <input type="file" id="uploadPhotos" name="uploadPhotos">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="supportingDocs">Supporting Documents</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="file" id="supportingDocs" name="supportingDocs">
                            </div>
                        </div> 
                       <!--Ajax display added person-->
                       <div class="form-group displayAddedPerson">
                            <div class="col-lg-6 col-md-4 col-sm-8">
   
                            </div>
                        </div>
                       <!--Ajax display added person-->
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="addPerson">Involved Parties</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <a href="#addperson-modal" data-backdrop="static" data-toggle="modal" data-target="#addperson-modal">
                                    <button type="button" id="addPerson" class="btn btn-success">Add Person</button>
                                </a>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="addAsset">Involved Resources</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <a href="#addAsset-modal" data-backdrop="static" data-toggle="modal" data-target="#addAsset-modal"><button type="button" id="addAsset" class="btn btn-success">Add resource</button></a>
                            </div>
                        </div>                        
                       <!--Show Asset Involved -->
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="showAssetInvolved"></label>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <table id="showAssetInvolved" style="margin-bottom:0;" class="table hidden">
                                    <tr>
                                        <th>Resource Department</th>
                                        <th>Resource Category</th>
                                        <th>Resource Type</th>
                                        <th>Resource Description</th>
                                        <th>Resource ID Number</th>
                                        <th></th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--End of the Asset Involved -->
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="addWitness"> Involved Witnesses</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <a href="#addwitness-modal" data-backdrop="static" data-toggle="modal" data-target="#addwitness-modal"><button type="button" id="addWitness" class="btn btn-success">Add Witness</button></a>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="addParty"> 3rd Party Resource</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <a href="#addParty-modal" data-backdrop="static" data-toggle="modal" data-target="#addParty-modal">
                                    <button type="button" id="addParty" class="btn btn-success">Add 3rd Party Resource</button>
                                </a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="addParty"></label>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                            <table id="showThirdPartyAsset" class="table hidden">
                                <tr>
                                    <th>Type</th>
                                    <th>Description</th>
                                    <th>Resource Number</th>
                                    <th></th>
                                </tr>
                            </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="assignInvestigator">Assign Investigator</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select class="form-control" name="assignInvestigator" id="assignInvestigator">
                                    <option value="1">Sunnyboy Mathole</option>
                                    <option value="2">Warren Windvogel</option>
                                    <option value="3">Nick Botha</option>
                                    <option value="4">Zaakirah Hoosain</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="investigationDate">Investigation Due Date</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="datetime" class="form-control" name="investigationDate" id="investigationDate">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <button type="reset" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!--End of panel-group-->


        <!-- Add Person Modal -->
        <div class="modal fade" id="addperson-modal" role="dialog">
            <div class="modal-dialog">  
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Person Involved</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" method="post" id="incendentReportAddPersonForm" name="incendentReportAddPersonForm" action="incident_report.php">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="personType">Person Type</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="hidden" name="personInvolvedID" id="personInvolvedID">
                                    <label class="radio-inline"><input type="radio" class="personInvolvedType" name="personType" value="1">Employee</label>
                                    <label class="radio-inline"><input type="radio" class="personInvolvedType" name="personType" value="2">Customer</label>
                                </div>
                                <div class="col-lg-6 col-lg-offset-4 col-md-4 col-sm-8">
                                    <label class="radio-inline"><input type="radio" class="personInvolvedType" name="personType" value="3">Service Provider</label>
                                    <label class="radio-inline"><input type="radio" class="personInvolvedType" name="personType" value="4">Supplier</label>
                                </div>
                                <div class="col-lg-6 col-lg-offset-4 col-md-4 col-sm-8">
                                    <label class="radio-inline"><input type="radio" class="personInvolvedType" name="personType" value="5">Visitor</label>
                                    <label class="radio-inline"><input type="radio" class="personInvolvedType" name="personType" value="6">Public</label> 
                                </div>
                            </div>

                            <!--Input Fields For Employee-->
                            <div class="incidentEmployee incidentBox">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="Department">Department</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <select class="form-control" id="personInvolvedDepartment" name="personInvolvedDepartment">
                                            <?php
                                            if (!empty($data["getAllDepartment"])) {
                                                foreach ($data["getAllDepartment"] as $departmentDetailsArr) {
                                                    echo '<option value="' . $departmentDetailsArr["id"] . '">' . $departmentDetailsArr["department_name"] . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="personInvolvedFullname">Name Surname</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="personInvolvedFullname" id="personInvolvedFullname" placeholder="Please search a name & surname">
                                    </div>
                                </div>
                                
                            </div>
                            <!--Search Employee-->
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1 col-sm-8">
                                    <table id="personInvolvedResults" class="table hidden">
                                        <thead>
                                            <tr>
                                              <th>Branch Name</th>
                                              <th>Department</th>
                                              <th>Full name</th>
                                              <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div id="EmployeeNotFound" class="form-group hidden">
                                <label class="control-label col-sm-4" for="resultsNotFound">Result not found :</label>
                                <div  class="col-lg-6 col-md-4 col-sm-8"> 
                                    <a href="#addNewReportedPerson-modal" data-toggle="modal" data-target="#addNewReportedPerson-modal">
                                    <button type="button" id="addNewReportedPerson" class="btn btn-success">Add New Employee</button>
                                    </a>
                                </div>
                            </div>
                            <!--End of search employee-->
                             
                            <!--Input Fields For Customer-->
                            <div class="incidentCustomer incidentBox">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="companyIndividual">Company/Individual</label>
                                    <div class="col-lg-6 col-md-6 col-sm-8">
                                        <select class="form-control" name="companyIndividual" id="customerCompanyType" required>
                                            <option value="" selected disabled>Please select a customer</option>
                                            <?php if($data['companyTypes']): foreach ($data['companyTypes'] as $companyTypes): ?>
                                            <option value="<?php echo strtolower($companyTypes['type_name']); ?>"><?php echo $companyTypes['type_name']; ?></option>
                                            <?php endforeach;endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group customerCompanyName">
                                    <label class="control-label col-sm-4" for="customerCompanyName">Company Name</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <select class="form-control" name="customerCompanyName" id="customerCompanyName">
                                        </select>
                                    </div>
                                </div>      
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="customerNameSurname">Name Surname</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="customerNameSurname" id="customerNameSurname" placeholder="Please search a name & surname">
                                    </div>
                                </div>
                                <div class="form-group hidden" id="customerPersonInvolvedNotFound">
                                    <span class="control-label col-sm-4" for="addNewCustomer"></span>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <a href="#addNewCustomer-modal" data-toggle="modal" data-target="#addNewCustomer-modal">
                                            <button type="button" id="addNewCustomer" class="btn btn-success">Add New Customer</button></a>
                                    </div>
                                </div>
                                 <!--Search Customer-->
                                 <div class="form-group">
                                    <label class="control-label col-sm-4" for="searchCustomerPersonInvolved"></label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <table id="searchCustomerPersonInvolved" class="table hidden">
                                            <thead>
                                                <tr>
                                                  <th>Company Name</th>
                                                  <th>Fullname</th>
                                                  <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                 </div>
                            </div>
                            <!--Input Fields For Service Provider-->
                            <div class="incidentServiceProvider incidentBox">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="serviceproviderCompanyName">Company Name</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <select class="form-control" name="serviceproviderCompanyName"  id="serviceproviderCompanyName"></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="serviceproviderNameSurname">Name Surname</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="serviceproviderNameSurname" id="serviceproviderNameSurname" placeholder="Please search a name & surname">
                                    </div>
                                </div>
                                <!--Search Provider-->
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="searchProviderPersonInvolved"></label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <table id="searchProviderPersonInvolved" class="table hidden">
                                            <thead>
                                                <tr>
                                                  <th>Company Name</th>
                                                  <th>Fullname</th>
                                                  <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                 </div>
                                <div class="form-group hidden" id="providerPersonInvolvedNotFound">
                                    <span class="control-label col-sm-4" for="addNewServiceProvider"></span>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <a href="#addNewServiceProvider-modal" data-toggle="modal" data-target="#addNewServiceProvider-modal">
                                            <button type="button" id="addNewServiceProvider" class="btn btn-success">Add New Service Provider</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--Input Fields For Supplier-->
                            <div class="incidentSupplier incidentBox">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="supplierCompanyName">Company Name</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <select class="form-control" name="supplierCompanyName" id="supplierCompanyName"></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="supplierNameSurname">Name Surname</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="supplierNameSurname" id="supplierNameSurname" placeholder="Please search a name & surname">
                                    </div>
                                </div>
                                <!--Search Supplier-->
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="searchSupplierPersonInvolved"></label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <table id="searchSupplierPersonInvolved" class="table hidden">
                                            <thead>
                                                <tr>
                                                  <th>Company Name</th>
                                                  <th>Fullname</th>
                                                  <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group hidden" id="supplierPersonInvolvedNotFound">
                                    <span class="control-label col-sm-4" for="addNewSupplier"></span>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <a href="#addNewSupplier-modal"   data-toggle="modal" data-target="#addNewSupplier-modal">
                                            <button type="button" id="addNewSupplier" class="btn btn-success">Add New Supplier</button></a>
                                    </div>
                                </div>

                            </div>
                            <!--Input Fields For Visitor-->
                            <div class="incidentVisitor incidentBox">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="visitorFullname">Name Surname</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="visitorFullname" id="visitorFullname" placeholder="Please enter name & surname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="visitorIDNumber">Id Number</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="visitorIDNumber" id="visitorIDNumber" placeholder="Please enter your Id">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="visitorContactNumber">Contact Number</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="visitorContactNumber" id="visitorContactNumber" placeholder="Please enter your contact number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="visitorEmail">E-mail</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="email" class="form-control" name="visitorEmail" id="visitorEmail" placeholder="Please enter your email address">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="visitorCompanyName">Company name if applicable</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="visitorCompanyName" id="visitorCompanyName" placeholder="Please enter company name">
                                    </div>
                                </div>
                            </div>

                            <!--Input Fields For Public-->
                            <div class="incidentPublic incidentBox">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="publicFullname">Name Surname</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="publicFullname" id="publicFullname" placeholder="Please enter name & surname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="publicIDNumber">Id Number</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="publicIDNumber" id="publicIDNumber" placeholder="Please enter your Id">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="publicContactNumber">Contact Number</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="publicContactNumber" id="publicContactNumber" placeholder="Please enter your contact number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="publicEmail">E-mail</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="email" class="form-control" name="publicEmail" id="publicEmail" placeholder="Please enter your email address">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="publicCompanyName">Company name if applicable</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="publicCompanyName" id="publicCompanyName" placeholder="Please enter company name">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4" for="personInjured">Was the person injured or ill?</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <label class="radio-inline"><input type="radio" name="personInjured" id="personInjured1" value="1" required>Yes</label>
                                    <label class="radio-inline"><input type="radio" name="personInjured" id="personInjured2" value="0">No</label>
                                </div>
                            </div>

                            <div class="injuries incidentBox2">
                                <div class="form-group">
                                    <label class="control-label col-sm-4 col-xs-12" for="effectedBodyPart">PreportToAuthorities</label>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="checkbox"><label><input type="checkbox" name="effectedBodyPart[]" value="1">Head or Neck</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectedBodyPart[]" value="2">Foot</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectedBodyPart[]" value="3">Eye</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectedBodyPart[]" value="4">Trunk</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectedBodyPart[]" value="5">Internal</label></div>
                                    </div>
                                    <span class="col-xs-4"></span>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="checkbox"><label><input type="checkbox" name="effectedBodyPart[]" value="6">Arm</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectedBodyPart[]" value="7">Finger</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectedBodyPart[]" value="8">Leg</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectedBodyPart[]" value="9">Hand</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectedBodyPart[]" value="10">Multiple</label></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4 col-xs-12" for="effectPerson">Effect on person</label>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="checkbox"><label><input type="checkbox" name="effectPerson[]" value="1">Sprain or Strain</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectPerson[]" value="2">Amputation</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectPerson[]" value="3">Electrical Shock</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectPerson[]" value="4">Occupational Disease</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectPerson[]" value="5">Asphyxiation</label></div>
                                    </div>
                                    <span class="col-xs-4"></span>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="checkbox"><label><input type="checkbox" name="effectPerson[]" value="6">Fractures</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectPerson[]" value="7">Burns</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectPerson[]" value="8">Unconsciousness</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectPerson[]" value="9">Contusion of Wounds</label></div>
                                        <div class="checkbox"><label><input type="checkbox" name="effectPerson[]" value="10">Poisoning</label></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="descriptionDisease">Description Of Occupational Disease</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <textarea class="form-control" rows="5" name="descriptionDisease" id="descriptionDisease"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="daysExpected">How many days of work expected</label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="number" class="form-control" id="daysExpected" name="daysExpected" min="1" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="button" data-dismiss="modal" name="personInvolvedEmployeeBtn" id="personInvolvedEmployeeBtn" class="btn btn-success">Save</button>
                                    <button type="button" data-dismiss="modal" name="personInvolvedCustomerBtn" id="personInvolvedCustomerBtn" class="btn btn-success">Save</button>
                                    <button type="button" data-dismiss="modal" name="personInvolvedServiceProviderBtn" id="personInvolvedServiceProviderBtn" class="btn btn-success">Save</button>
                                    <button type="button" data-dismiss="modal" name="personInvolvedSupplierBtn" id="personInvolvedSupplierBtn" class="btn btn-success">Save</button>
                                    <button type="button" data-dismiss="modal" name="personInvolvedVisitorBtn" id="personInvolvedVisitorBtn" class="btn btn-success">Save</button>
                                    <button type="button" data-dismiss="modal" name="personInvolvedPublicBtn" id="personInvolvedPublicBtn" class="btn btn-success">Save</button>
                                </div>
                            </div>
                        </form> 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!--End Of Modal -->
        
        <!-- Add New Reported Person -->
        <div class="modal fade" id="addNewReportedPerson-modal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content largeModal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add New Employee</h4>
                    </div>
                    <div class="modal-body">
                        <?php include 'forms/employee_modal_form.php'; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--End Of Modal -->

        <!-- Add Resource Modal -->
        <div class="modal fade" id="addAsset-modal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content largeModal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Resource Involved</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" method="post" id="addAssetFormInvolved" name="addAssetFormInvolved" action="incident_report.php">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="incidentAssetDepartment">Resource Department</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <select class="form-control" id="incidentAssetDepartment" name="incidentAssetDepartment" required>
                                        <?php
                                        if (!empty($data["getAllDepartment"])) {
                                            foreach ($data["getAllDepartment"] as $departmentDetailsArr) {
                                                echo '<option value="' . $departmentDetailsArr["id"] . '">' . $departmentDetailsArr["department_name"] . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="incidentAssetCategory">Resource</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <select class="form-control" name="incidentAssetCategory" id="incidentAssetCategory" required>
                                        <?php
                                        foreach ($data['getAllAssetCategory'] as $assetCategoryArr) {
                                            echo '<option value="' . $assetCategoryArr['id'] . '">' . $assetCategoryArr['name'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group"> 
                                <label class="control-label col-sm-4" for="incidentAssetType">Type</label>
                                <div  class="col-lg-6 col-md-4 col-sm-8">
                                    <select class="form-control incidentAssetType" id="incidentAssetType" name="incidentAssetType" required>
                                    </select> 
                                </div>   
                            </div>    
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="incidentAssetDescription">Resource Description</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="hidden" name="assetInvolvedID" id="assetInvolvedID" value="">
                                    <input type="text" class="form-control" name="incidentAssetDescription"  required id="incidentAssetDescription" placeholder="Please search for a resource" required>
                                </div>
                            </div>
                            <div class="form-group">   
                                <div id="assetInvolvedResults"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="involvementType">Involvement Type</label>
                                <div class="col-lg-8 col-md-4 col-sm-8">
                                    <label class="radio-inline"><input type="radio"  name="involvementType" value="0">Cause of incident</label>
                                    <label class="radio-inline"><input type="radio" name="involvementType" value="1">Damage due to incident</label> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="effectAsset">Effect on Resource</label>
                                <div class="col-lg-8 col-md-4 col-sm-8">
                                    <label class="radio-inline"><input type="radio" name="effectAsset" value="0">Damage Beyond Repair</label>
                                    <label class="radio-inline"><input type="radio" name="effectAsset" value="1">Repairable</label>
                                    <label class="radio-inline"><input type="radio" name="effectAsset" value="2">No Damage</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-xs-12" for="cause">Cause</label>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="checkbox"><label><input type="checkbox" name="cause[]" value="1">Fire</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="cause[]" value="2">Stolen</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="cause[]" value="3">Explode</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="cause[]" value="4">Excessive Noise</label></div>
                                </div>
                                <span class="col-xs-4"></span>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="checkbox"><label><input type="checkbox" name="cause[]" value="5">Breakdown</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="cause[]" value="6">Fall</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="cause[]" value="7">Excessive Vibration</label></div>
                                    <div class="checkbox"><label><input type="checkbox" name="cause[]" value="8">Electrical short</label></div>   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="numberHours">Expected Down Time</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="number" class="form-control" name="numberHours" id="numberHours" required min="1">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" data-dismiss="modal" name="addAssetInvolvedBtn" id="addAssetInvolvedBtn" class="btn btn-success">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Of Modal -->
    <!-- Add New Resource Modal -->
    <div class="modal fade" id="addNewAsset-modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Resource</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="incendentNewAssetForm" method="post" name="incendentNewAssetForm" >
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="departmentid">Department</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <select class="form-control" id="departmentid" name="departmentid">
                                    <?php
                                    if (!empty($data["getAllDepartment"])) {
                                        foreach ($data["getAllDepartment"] as $departmentDetailsArr) {
                                            echo '<option value="' . $departmentDetailsArr["id"] . '">' . $departmentDetailsArr["department_name"] . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="categoryType">Resource Category</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <select class="form-control" name="categoryType" id="categoryType">
                                    <?php
                                    foreach ($data['getAllAssetCategory'] as $assetCategoryArr) {
                                        echo '<option value="' . $assetCategoryArr['id'] . '">' . $assetCategoryArr['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">   
                            <label class="control-label col-sm-4" for="assetType">Type</label>
                               <div  class="col-lg-6 col-md-4 col-sm-8">
                                <select class="incidentAssetType form-control" id="assetType" name="assetType">
                                </select> 
                            </div>   
                        </div>   
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="resourceName">Description Name</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="resourceName" id="resourceName">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="companyAssetNumber">Resource ID Number</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="companyAssetNumber" id="companyAssetNumber" placeholder="Please enter resource Id number" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="button" id="addAssetBtn"  data-dismiss="modal" class="btn btn-success">Add Resource</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--End Of Modal -->

    <!-- Add Witness Modal -->
    <div class="modal fade" id="addwitness-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Witness Involved</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" id="incendentReportAddWitnessForm" name="incendentReportAddWitnessForm" action="incident_report.php">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="personType">Type</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <label class="radio-inline"><input type="radio"  class="witnessType" name="personType" value="1">Employee</label><input type="hidden" name="witnessID" id="witnessID">
                                <label class="radio-inline"><input type="radio"  class="witnessType" name="personType" value="2">Customer</label>
                            </div>
                            <div class="col-lg-6 col-lg-offset-4 col-md-4 col-sm-8">    
                                <label class="radio-inline"><input type="radio"  class="witnessType" name="personType" value="3">Service Provider</label> 
                                <label class="radio-inline"><input type="radio"  class="witnessType" name="personType" value="4">Supplier</label>
                            </div>
                            <div class="col-lg-6 col-lg-offset-4 col-md-4 col-sm-8">    
                                <label class="radio-inline"><input type="radio"  class="witnessType" name="personType" value="5">Visitor</label>
                                <label class="radio-inline"><input type="radio"  class="witnessType" name="personType" value="6">Public</label>
                            </div>
                        </div>
                        <!--Input Fields For Employee-->
                        <div class="incidentEmployee incidentBox">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessEmployeeDepartment">Department</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <select class="form-control" id="witnessEmployeeDepartment" name="witnessEmployeeDepartment">
                                        <?php
                                        if (!empty($data["getAllDepartment"])) {
                                            foreach ($data["getAllDepartment"] as $departmentDetailsArr) {
                                                echo '<option value="' . $departmentDetailsArr["id"] . '">' . $departmentDetailsArr["department_name"] . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessEmployeeFullname">Name Surname</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="witnessEmployeeFullname" id="witnessEmployeeFullname" required placeholder="Please search a name & surname">
                                </div>
                            </div>
                            <!--Search Employee-->
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-1 col-sm-8">
                                    <table id="witnessEmployeeResults" class="table hidden">
                                        <thead>
                                            <tr>
                                              <th>Branch Name</th>
                                              <th>Department</th>
                                              <th>Full name</th>
                                              <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div id="WitnessEmployeeNotFound" class="form-group hidden">
                                <label class="control-label col-sm-4" for="resultsNotFound">Result not found :</label>
                                <div  class="col-lg-6 col-md-4 col-sm-8"> 
                                    <a href="#addNewReportedPerson-modal" data-toggle="modal" data-target="#addNewReportedPerson-modal">
                                    <button type="button" id="addNewReportedPerson" class="btn btn-success">Add New Employee</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--Input Fields For Customer-->
                        <div class="incidentCustomer incidentBox">
                             <div class="form-group">
                                    <label class="control-label col-sm-4" for="companyIndividual">Company/Individual</label>
                                    <div class="col-lg-6 col-md-6 col-sm-8">
                                        <select class="form-control" name="companyIndividual" id="companyIndividual" required>
                                            <option value="" selected disabled>Please select a customer</option>
                                            <?php if($data['companyTypes']): foreach ($data['companyTypes'] as $companyTypes): ?>
                                            <option value="<?php echo strtolower($companyTypes['type_name']); ?>"><?php echo $companyTypes['type_name']; ?></option>
                                            <?php endforeach;endif; ?>
                                        </select>
                                    </div>
                                </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessCustomerCompanyName">Company Name</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <select class="form-control" name="witnessCustomerCompanyName" id="witnessCustomerCompanyName"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessCustomerFullname">Name Surname</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="witnessCustomerFullname" id="witnessCustomerFullname" placeholder="Please enter full name">
                                </div>
                            </div>
                            <div class="form-group hidden" id="customerWitnessNotFound">
                                <span class="control-label col-sm-4" for="addNewCustomer"></span>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <a href="#addNewCustomer-modal" data-toggle="modal" data-target="#addNewCustomer-modal">
                                        <button type="button" id="addNewCustomer" class="btn btn-success">Add New Customer</button></a>
                                </div>
                            </div>
                             <!--Search Customer-->
                             <div class="form-group">
                                <label class="control-label col-sm-4" for="searchWitnessCustomer"></label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <table id="searchWitnessCustomer" class="table hidden">
                                        <thead>
                                            <tr>
                                              <th>Company Name</th>
                                              <th>Fullname</th>
                                              <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                             </div>
       
                        </div>
                        <!--Input Fields For Service Provider-->
                        <div class="incidentServiceProvider incidentBox">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="serviceproviderCompanyName">Company Name</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <select class="form-control" name="serviceproviderCompanyName" id="witnessproviderCompanyName"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="serviceproviderNameSurname">Name Surname</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="serviceproviderNameSurname" id="witnessProviderNameSurname" placeholder="Please search a name & surname">
                                </div>
                            </div>
                             <!--Search Provider-->
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="searchProviderPersonInvolved"></label>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <table id="searchWitnessProvider" class="table hidden">
                                            <thead>
                                                <tr>
                                                  <th>Company Name</th>
                                                  <th>Fullname</th>
                                                  <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                 </div>
                                <div class="form-group hidden" id="witnessProviderNotFound">
                                    <span class="control-label col-sm-4" for="addNewServiceProvider"></span>
                                    <div class="col-lg-6 col-md-4 col-sm-8">
                                        <a href="#addNewServiceProvider-modal" data-toggle="modal" data-target="#addNewServiceProvider-modal">
                                            <button type="button" id="addNewServiceProvider" class="btn btn-success">Add New Service Provider</button>
                                        </a>
                                    </div>
                                </div>
                        </div>
                        <!--Input Fields For Supplier-->
                        <div class="incidentSupplier incidentBox">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessSupplierCompanyName">Company Name</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <select class="form-control" name="witnessSupplierCompanyName" id="witnessSupplierCompanyName"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessSupplierName">Name Surname</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="witnessSupplierName" id="witnessSupplierName" required placeholder="Please search a name & surname">
                                </div>
                            </div>
                         <div class="form-group">
                            <label class="control-label col-sm-4" for="searchWitnessSupplier"></label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <table id="searchWitnessSupplier" class="table hidden">
                                    <thead>
                                        <tr>
                                          <th>Company Name</th>
                                          <th>Fullname</th>
                                          <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="form-group hidden" id="witnessSupplierNotFound">
                            <span class="control-label col-sm-4" for="addNewSupplier"></span>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <a href="#addNewSupplier-modal"   data-toggle="modal" data-target="#addNewSupplier-modal">
                                    <button type="button" id="addNewSupplier" class="btn btn-success">Add New Supplier</button></a>
                            </div>
                        </div>
                        </div>
                        <!--Input Fields For Visitor-->
                        <div class="incidentVisitor incidentBox">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessVisitorFullname">Name Surname</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="witnessVisitorFullname" id="witnessVisitorFullname" required placeholder="Please enter name & surname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessVisitorIDNumber">Id Number</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="witnessVisitorIDNumber" id="witnessVisitorIDNumber" required placeholder="Please enter your Id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessVisitorContactNumber">Contact Number</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="witnessVisitorContactNumber" id="witnessVisitorContactNumber" required placeholder="Please enter your contact number">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessVisitorEmail">E-mail</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="email" class="form-control" name="witnessVisitorEmail" id="witnessVisitorEmail" required placeholder="Please enter your email address">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessVisitorCompany">Company name if applicable</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="witnessVisitorCompany" id="witnessVisitorCompany" required placeholder="Please enter company name">
                                </div>
                            </div><br/>
                        </div>
                        <!--Input Fields For Public-->
                        <div class="incidentPublic incidentBox">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessPublicFullname">Name Surname</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="witnessPublicFullname" id="witnessPublicFullname" required placeholder="Please enter name & surname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessPublicIDNumber">Id Number</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="witnessPublicIDNumber" id="witnessPublicIDNumber" required placeholder="Please enter your Id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessPublicContactNumber">Contact Number</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="witnessPublicContactNumber" id="witnessPublicContactNumber" required placeholder="Please enter your contact number">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessPublicEmail">E-mail</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="email" class="form-control" name="witnessPublicEmail" id="witnessPublicEmail" required placeholder="Please enter your email address">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="witnessPublicCompanyName">Company name if applicable</label>
                                <div class="col-lg-6 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="witnessPublicCompanyName" id="witnessPublicCompanyName" required placeholder="Please enter company name">
                                </div>
                            </div><br/>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="witnessIncidentType">Witness Type</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <label class="radio-inline"><input type="radio" name="witnessIncidentType" id="onScene" value="1">On Scene</label>
                                <label class="radio-inline"><input type="radio" name="witnessIncidentType" id="inVicinity" value="2">In Vicinity</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="button" data-dismiss="modal" name="witnessEmployeeBtn" id="witnessEmployeeBtn" class="btn btn-success">Save</button>
                                <button type="button" data-dismiss="modal" name="witnessCustomerBtn" id="witnessCustomerBtn" class="btn btn-success">Save</button>
                                <button type="button" data-dismiss="modal" name="witnessServiceBtn" id="witnessServiceBtn" class="btn btn-success">Save</button>
                                <button type="button" data-dismiss="modal" name="witnessSupplierBtn" id="witnessSupplierBtn" class="btn btn-success">Save</button>
                                <button type="button" data-dismiss="modal" name="witnessVisitorBtn" id="witnessVisitorBtn" class="btn btn-success">Save</button>
                                <button type="button" data-dismiss="modal" name="witnessPublicBtn" id="witnessPublicBtn" class="btn btn-success">Save</button> 
                            </div>
                        </div>
                    </form> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--End Of Modal -->

    <!-- Add New Employee Modal -->
    <div class="modal fade" id="addNewEmployee-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Employee</h4>
                </div>
                <div class="modal-body">
                    <?php include 'forms/employee_modal_form.php'; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--End Of Modal -->

    <!-- Add New Customers Modal -->
    <div class="modal fade" id="addNewCustomer-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Customer</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="AddNewCustomerForm" name="AddNewCustomerInvolvedForm" >
                        <?php include 'forms/customer_modal_form.php'; ?>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="button" data-dismiss="modal" id="addNewCustomerBtn" class="btn btn-success">Add Customer</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--End Of Modal -->

    <!-- Add New Service Provider Modal -->
    <div class="modal fade" id="addNewServiceProvider-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Service Provider</h4>
                </div>
                <div class="modal-body">
                    <?php include 'forms/service_provider_modal_form.php'; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--End Of Modal -->

    <!-- Add New Suppliers Modal -->
    <div class="modal fade" id="addNewSupplier-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Supplier</h4>
                </div>
                <div class="modal-body">
                    <?php include 'forms/supplier_modal_form.php'; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--End Of Modal -->

    <!-- Add 3rd Party Modal -->
    <div class="modal fade" id="addParty-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add 3rd Party Resource</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" action="" id="addPartyResourceForm" name="addPartyResourceForm">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="partyResourceType">Type</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="partyResourceType" id="partyResourceType" placeholder="Please enter resource type" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="partyResourceDescription">Resource Description</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="partyResourceDescription" id="partyResourceDescription" placeholder="Please enter resource description">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="partyResourceNumber">Resource Number</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="partyResourceNumber" id="partyResourceNumber" placeholder="Please enter resource number" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="partyResourceEffectAsset">Effect on Resource</label>
                            <div class="col-lg-8 col-md-4 col-sm-8">
                                <label class="radio-inline"><input type="radio" name="partyResourceEffectAsset" value="0">Damage Beyond Repair</label>
                                <label class="radio-inline"><input type="radio" name="partyResourceEffectAsset" value="1">Repairable</label>
                                <label class="radio-inline"><input type="radio" name="partyResourceEffectAsset" value="2">No Damage</label>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-sm-4 col-xs-4" for="partyCause">Cause</label>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="checkbox"><input type="checkbox" name="partyResourceCause[]" value="1">Fire</div>
                                <div class="checkbox"><input type="checkbox" name="partyResourceCause[]" value="2">Stole</div>
                                <div class="checkbox"><input type="checkbox" name="partyResourceCause[]" value="3">Explode</div>
                                <div class="checkbox"><input type="checkbox" name="partyResourceCause[]" value="4">Breakdown</div>
                                <div class="checkbox"><input type="checkbox" name="partyResourceCause[]" value="5">Electrical short circuit</div>
                                <div class="checkbox"><input type="checkbox" name="partyResourceCause[]" value="6">Fall - fall over</div>
                                <div class="checkbox"><input type="checkbox" name="partyResourceCause[]" value="7">Fall from height</div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="checkbox"><input type="checkbox" name="partyResourceCause[]" value="8">Excessive dust</div>
                                <div class="checkbox"><input type="checkbox" name="partyResourceCause[]" value="9">Excessive noise</div>
                                <div class="checkbox"><input type="checkbox" name="partyResourceCause[]" value="10">Excessive vibration</div>
                                <div class="checkbox"><input type="checkbox" name="partyResourceCause[]" value="11">Excessive heat</div>
                                <div class="checkbox"><input type="checkbox" name="partyResourceCause[]" value="12">Excessive radiation</div>
                                <div class="checkbox"><input type="checkbox" name="partyResourceCause[]" value="13">Excessive gas emission</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="button" id="addPartyResourceBtn"  data-dismiss="modal" class="btn btn-success">Add 3rd Party Resource</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--End Of Modal -->

</div>
</div><!--End of row-->
</div><!--End of container-fluid-->

<script src="frontend/js/incidents.js"></script>
<?php include_once('frontend/templates/footers/branch_tpl.php'); ?>
<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>