<?php 
$title = 'Incident near miss';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php include_once('frontend/templates/menus/main-menu.php');  ?>
<!--End of navigation-->    

<?php include_once('frontend/templates/menus/side-menu.php'); ?>
	
<div class="col-lg-10">
<ul class="nav nav-pills nav-justified topbar-menu">
    <li class="active"><a href="incident.php">Incident Log</a></li>
    <li><a href="incident_investigate.php">Investigations</a></li>
</ul>
<div class="panel-group">
    <form  enctype="multipart/form-data" class="form-horizontal" name="incidentAddForm" id="incidentAddForm" method="post" action="">
    <div class="panel panel-default">
    <div class="panel-heading">Near Miss Incident</div>
    <div class="panel-body"> 
        <div class="form-group">
            <label class="control-label col-sm-4" for="incidentDate">Date of Incident</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" id="incidentDate" name="incidentDate" required >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="incidentTime">Time of Incident</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" id="incidentTime" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="incidentDepartment">Department</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="form-control" id="incidentDepartment" name="incidentDepartment">
                    <option value="1">Human Resource</option>
                    <option value="2">Finance</option>
                    <option value="3">Information Technology</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="incidentShift">Shift</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" id="incidentShift" required placeholder="Please enter shift name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="reportedPerson">Reported Person</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="form-control" id="reportedPerson" name="reportedPerson">
                    <option value="1">Sunnyboy Mathole</option>
                    <option value="2">Nick Botha</option>
                    <option value="3">Warren Windvogel</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="incidentCategory">Incident Category</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="form-control" id="incidentCategory" name="incidentCategory">
                    <option value="1">Health and Safety</option>
                    <option value="2">Environmental</option>
                    <option value="3">Quality</option>
                    <option value="4">Other Incidents</option>
                </select>
            </div>
        </div>
        <div class="form-group" id="incidentSubCategory">
            <label class="control-label col-sm-4" for="incidentSubCategory">Incident Category</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="form-control" id="healthandsafety" name="incidentSubCategory">
                    <option value="1">First Aid</option>
                    <option value="2">Medical Treatment</option>
                    <option value="3">Serious Injury</option>
                </select>
                <select class="form-control" id="environmental" name="incidentSubCategory">
                    <option value="1">Soil Pollution</option>
                    <option value="2">Air Pollution</option>
                    <option value="3">Spill</option>
                </select>
                <select class="form-control" id="quality" name="incidentSubCategory">
                    <option value="1">Property Damage</option>
                    <option value="2">Product Damage</option>
                    <option value="3">Process Error</option>
                </select>
                <select class="form-control" id="otherIncidents" name="incidentSubCategory">
                    <option value="1">Vehicle Accident</option>
                    <option value="2">Crime</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="reportedDOL">Reported to DOL</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <label class="radio-inline"><input type="radio" name="reportedDO" id="reportedDOLYes" value="1">Yes</label>
            <label class="radio-inline"><input type="radio" name="reportedDO" id="reportedDOLNo" value="2">No</label>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="incidentDescription">Description</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <textarea class="form-control" rows="5" name="incidentDescription" id="incidentDescription" required></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="uploadPhotos">Upload Photos</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <button type="button" id="uploadPhotos" class="btn btn-success">Upload Photos</button>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="addPerson">Person Involvement</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <button type="button" id="addPerson" class="btn btn-success">Add Person</button>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="addResource">Resource Involvement</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <button type="button" id="addResource" class="btn btn-success">Add resource</button>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="addWitness">Add Witness</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <button type="button" id="addWitness" class="btn btn-success">Add Witness</button>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="appointInvestigator">Appoint Investigator</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <select class="form-control" name="appointInvestigator" id="appointInvestigator">
                <option value="1">Sunnyboy Mathole</option>
                <option value="2">Warren Windvogel</option>
                <option value="3">Nick Botha</option>
                <option value="4">Zaakirah Hoosain</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="investigationDate">Investigation Due Date</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="datetime" class="form-control" id="investigationDate" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="reset" class="btn btn-default">Reset</button>
            </div>
        </div>
    </div>
    </div>
    <!--End of panel panel-default-->
    <div class="panel panel-default" id="addPersonForm">
    <div class="panel-heading">Add Person</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-4" for="name">Name</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <input type="text" class="form-control" id="name" required placeholder="Please enter name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="surname">Surname</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <input type="text" class="form-control" id="surname" required placeholder="Please enter surname">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="personType">Person Type</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <label class="radio-inline"><input type="radio" name="personType" value="1">Employee</label>
            <label class="radio-inline"><input type="radio" name="personType"value="2">Contractor</label>
            <label class="radio-inline"><input type="radio" name="personType" value="3">Visitor</label> 
            </div>
        </div>
    </div>
    </div>
    <!--End of panel panel-default-->
    <div class="panel panel-default" id="addResourceForm">
    <div class="panel-heading">Add Resource</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-4" for="resourceType">Resource Type</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <input type="text" class="form-control" id="resourceType" required placeholder="Please resource type">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="resourceDepartment">Resource Department</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <select class="form-control" id="quality" name="resourceDepartment">
                <option value="1">Human Resource</option>
                <option value="2">Developers</option>
                <option value="3">Finance</option>
             </select>
            </div>
        </div>
    </div>
    </div>
    <!--End of panel panel-default-->
    <div class="panel panel-default" id="addWitnessForm">
    <div class="panel-heading">Add Witness</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-4" for="witnessName">Name</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <input type="text" class="form-control" id="witnessName" required placeholder="Please enter witness name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="witnessSurname">Surname</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <input type="text" class="form-control" id="witnessSurname" required placeholder="Please enter witness surname">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="witnessType">Witness Type</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <label class="radio-inline"><input type="radio" name="witnessType" id="employee" value="1">Employee</label>
            <label class="radio-inline"><input type="radio" name="witnessType" id="contractor" value="2">Contractor</label>
            <label class="radio-inline"><input type="radio" name="witnessType" id="visitor" value="3">Visitor</label> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="witnessContactNumber">Contact Number</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <input type="tel" class="form-control" id="witnessContactNumber" required placeholder="Please enter witness contact nuumber">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="witnessEmail">E-mail</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <input type="email" class="form-control" id="witnessEmail" required placeholder="Please enter witness email address">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="witnessDepartment">Department</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <select class="form-control" name="witnessDepartment" id="witnessDepartment">
                <option value="1">Finance</option>
                <option value="2">Human Resource</option>
                <option value="3">Project Management</option>
                <option value="4">Information Technology</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="witnessIncidentType">Witness Type</label>
            <div class="col-lg-6 col-md-4 col-sm-8">
            <label class="radio-inline"><input type="radio" name="witnessIncidentType" id="onScene" value="1">On the Scene</label>
            <label class="radio-inline"><input type="radio" name="witnessIncidentType" id="nearIncident" value="2">Near Incident</label>
            </div>
        </div>
    </div>
    </div>
    <!--End of panel panel-default-->
    </form>
</div>
<!--End of panel-group-->

   </div>
  </div><!--End of row-->
</div><!--End of container-fluid-->

<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>   