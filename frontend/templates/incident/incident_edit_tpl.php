<?php
$title = 'Incident report';
include_once('frontend/templates/headers/default_header_tpl.php');
global  $objLanguage;
?> 
<div class="container-fluid">

    <!--navigation--> 
    <?php include_once('frontend/templates/menus/main-menu.php'); ?>
    <!--End of navigation-->   

    <?php include_once('frontend/templates/menus/side-menu.php'); ?>

    <div class="col-lg-10">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li class="active"><a href="index.php?module=incidents&action=incidentManagement">All Incidents</a></li>
            <li><a href="index.php?module=incidents&action=reportIncident">Report Incident</a></li>
            <li><a href="index.php?module=incidents&action=incidentInvestigation">Active Investigations</a></li>
            <li><a href="index.php?module=incidents&action=incidentSignoff">Signed Off</a></li>
            <li><a href="#">Research</a></li>
            <li><a href="#">Settings</a></li>
        </ul>
        <div class="panel-group">
            <form  enctype="multipart/form-data" class="form-horizontal" name="incidentAddForm" id="incidentAddForm" method="post" action="index.php?action=updateIncident&module=incidents">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Incident</div>
                    <div class="panel-body"> 
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentCategory">Incident Category</label>
                            <div class="col-lg-4 col-md-4 col-sm-8"> 
                                <select class="form-control" id="incidentCategory" name="incidentCategory" required>
                                    <?php
                                    foreach ($data["getAllIncidentCategory"] as $incidentCategoryDetails) {
                                        echo '<option value="' . $incidentCategoryDetails["id"] . '">' . $incidentCategoryDetails["category_name"] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentType">Incident Type</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select class="form-control" id="incidentType" name="incidentType" required> 
                                </select>
                            </div>
                        </div>
                        <div id="showhideincident" class="form-group hide">
                            <label class="control-label col-sm-4" for="incidentNumber">Incident Number</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input disabled type="text" class="form-control" id="incidentNumber" name="incidentNumber">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentDate">Date & Time of Incident</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="incidentDate" name="incidentDate" required >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentDepartment">Department</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select class="form-control" id="incidentDepartment" name="incidentDepartment">
                                    <?php
                                    if (!empty($data["getAllDepartment"])) {
                                        foreach ($data["getAllDepartment"] as $departmentDetailsArr) {
                                            echo '<option value="' . $departmentDetailsArr["id"] . '">' . $departmentDetailsArr["department_name"] . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentDepartmentManager">Department Manager</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select class="form-control" name="incidentDepartmentManager" id="incidentDepartmentManager">
                                    <option value="1">Nick Botha</option>
                                    <option value="2">Warren Windvogel</option>
                                    <option value="3">Lerato Chiloane</option>
                                    <option value="4">Sunnyboy Mathole</option>
                                    <option value="5">Bill Blurr</option>
                                    <option value="6">Nango Kala</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentShift">Shift</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="incidentShift" id="incidentShift" required placeholder="Please enter shift">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="reportedPerson">Reported Person</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select class="form-control" id="reportedPersonDepartment" name="reportedPersonDepartment">
                                    <option selected disabled>Department</option>
                                    <?php
                                    if (!empty($data["getAllDepartment"])) {
                                        foreach ($data["getAllDepartment"] as $departmentDetailsArr) {
                                            echo '<option value="' . $departmentDetailsArr["id"] . '">' . $departmentDetailsArr["department_name"] . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="control-label col-sm-4"></span>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="hidden" name="reportedPersonID" id="reportedPersonID" value="">
                                <input type="text" class="form-control" name="reportedPerson" id="reportedPerson" placeholder="Please search a name & surname">
                            </div>
                        </div>
                        <!--Show Search Reported person -->
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="reportedPersonResults"></label>
                            <div class="col-lg-6 col-md-6 col-sm-8">
                            <table id="reportedPersonResults" class="table table-hover center hidden">
                                <thead>
                                  <tr>
                                    <th>Branch Name</th>
                                    <th>Department</th>
                                    <th>Fullname</th>
                                    <th></th>
                                  </tr>
                                </thead>
                            </table>
                            <div id="reportedPersonNotFound" class="hidden">
                                <a href="#addNewReportedPerson-modal" data-toggle="modal" data-target="#addNewReportedPerson-modal">
                                    <button type="button" id="addNewReportedPerson" class="btn btn-success">Add New Employee</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        <!--show reported Person -->
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="showReportedPerson"></label>
                            <div class="col-lg-6 col-md-6 col-sm-8">
                                <table id="showReportedPerson" style="margin-bottom:0;" class="table hidden">
                                    <tr>
                                        <th>Firstname</th>
                                        <th>Lastname</th>
                                        <th>Delete</th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--End of Reported Person -->
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentTask">Task while incident occurred</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <textarea class="form-control" rows="5" id="incidentTask" name="incidentTask"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="incidentDescription">Incident Description</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <textarea class="form-control" rows="5" id="incidentDescription" name="incidentDescription"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4 col-xs-12" for="reportToAuthorities">Report to authorities</label>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="checkbox"><label><input type="checkbox" class="reportToAuthorities" name="reportToAuthorities[]" value="1">SAPS</label></div>
                                <div class="checkbox"><label><input type="checkbox" class="reportToAuthorities" name="reportToAuthorities[]" value="2">Compensation commissioner</label></div>
                                <div class="checkbox"><label><input type="checkbox" class="reportToAuthorities" name="reportToAuthorities[]" value="3">Department of Labor</label></div>
                                <div class="checkbox"><label><input type="checkbox" class="reportToAuthorities" name="reportToAuthorities[]" value="4">Fire Department</label></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="uploadPhotos">Upload Photos</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <input type="file" id="uploadPhotos" name="uploadPhotos">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="supportingDocs">Supporting Documents</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="file" id="supportingDocs" name="supportingDocs">
                            </div>
                        </div> 
                       <!--Ajax display added person-->
                       <div class="form-group displayAddedPerson">
                            <div class="col-lg-6 col-md-4 col-sm-8">
   
                            </div>
                        </div>
                       <!--Ajax display added person-->
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="addPerson">Involved Parties</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <a href="#addperson-modal" data-backdrop="static" data-toggle="modal" data-target="#addperson-modal">
                                    <button type="button" id="addPerson" class="btn btn-success">Add Person</button>
                                </a>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="addAsset">Involved Resources</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <a href="#addAsset-modal" data-backdrop="static" data-toggle="modal" data-target="#addAsset-modal"><button type="button" id="addAsset" class="btn btn-success">Add resource</button></a>
                            </div>
                        </div>                        
                       <!--Show Asset Involved -->
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="showAssetInvolved"></label>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <table id="showAssetInvolved" style="margin-bottom:0;" class="table hidden">
                                    <tr>
                                        <th>Resource Department</th>
                                        <th>Resource Category</th>
                                        <th>Resource Type</th>
                                        <th>Resource Description</th>
                                        <th>Resource ID Number</th>
                                        <th></th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--End of the Asset Involved -->
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="addWitness"> Involved Witnesses</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <a href="#addwitness-modal" data-backdrop="static" data-toggle="modal" data-target="#addwitness-modal"><button type="button" id="addWitness" class="btn btn-success">Add Witness</button></a>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="addParty"> 3rd Party Resource</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <a href="#addParty-modal" data-backdrop="static" data-toggle="modal" data-target="#addParty-modal">
                                    <button type="button" id="addParty" class="btn btn-success">Add 3rd Party Resource</button>
                                </a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="addParty"></label>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                            <table id="showThirdPartyAsset" class="table hidden">
                                <tr>
                                    <th>Type</th>
                                    <th>Description</th>
                                    <th>Resource Number</th>
                                    <th></th>
                                </tr>
                            </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="assignInvestigator">Assign Investigator</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select class="form-control" name="assignInvestigator" id="assignInvestigator">
                                    <option value="1">Sunnyboy Mathole</option>
                                    <option value="2">Warren Windvogel</option>
                                    <option value="3">Nick Botha</option>
                                    <option value="4">Zaakirah Hoosain</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="investigationDate">Investigation Due Date</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="datetime" class="form-control" name="investigationDate" id="investigationDate">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <button type="reset" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!--End of panel-group-->
        
</div><!--End of row-->
</div><!--End of container-fluid-->

<script src="frontend/js/incidents.js"></script>
<?php include_once('frontend/templates/footers/branch_tpl.php'); ?>
<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>






