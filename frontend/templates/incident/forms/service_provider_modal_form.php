            <form class="form-horizontal" id="addServiceProviderForm" name="addServiceProviderForm" method="post">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="companyName"><?php echo $objLanguage->languageText('COMPANY_NAME') ;?></label>
                            <div class="col-lg-6 col-md-6 col-sm-8">
                                <input type="text" class="form-control" id="companyName" name="companyName" placeholder="Please provide company name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="contactPerson"><?php echo $objLanguage->languageText('CONTACT_PERSON') ; ?></label>
                            <div class="col-lg-6 col-md-6 col-sm-8">
                                <input type="text" class="form-control" id="contactPerson" name="contactPerson" placeholder="Please enter name and surname" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="contactNumber"><?php echo $objLanguage->languageText('CONTACT_NUMBER') ; ?></label>
                            <div class="col-lg-6 col-md-6 col-sm-8">
                                <input type="text" class="form-control" id="contactNumber" name="contactNumber" placeholder="Please provide your contact number"  required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email"><?php echo $objLanguage->languageText('EMAIL') ; ?></label>
                            <div class="col-lg-6 col-md-6 col-sm-8">
                                <input type="email" class="form-control" id="email" name="email" required>
                                <input type="hidden" name="formType" value="ajaxForm">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="serviceType"><?php echo $objLanguage->languageText('SERVICE_TYPE');?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select name="serviceType" class="form-control" id="serviceType">

                                    <?php if(count($data['serviceTypes'])) : foreach($data['serviceTypes'] as $serviceType): ?>
                                    <option value="<?php echo $serviceType['id']; ?>"><?php echo $serviceType['service_name']; ?></option>
                                  <?php endforeach;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="button" id="addServiceProvider" data-dismiss="modal" class="btn btn-success"><?php echo $objLanguage->languageText('ADD_SERVICE_PROVIDER'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End of the panel panel-default-->
            </form>