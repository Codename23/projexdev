<div class="form-group">
    <label class="control-label col-sm-4" for="companyIndividual">Company/Individual</label>
    <div class="col-lg-4 col-md-4 col-sm-8">
        <select class="form-control" name="companyIndividual" id="companyIndividual" required>
            <option value="" selected disabled>Please select a customer</option>
            <?php if($data['companyTypes']): foreach ($data['companyTypes'] as $companyTypes): ?>
            <option value="<?php echo strtolower($companyTypes['type_name']); ?>"><?php echo $companyTypes['type_name']; ?></option>
            <?php endforeach;endif; ?>
        </select>
    </div>
</div>
<div id="company">
<div class="form-group">
    <label class="control-label col-sm-4" for="companyName">Company Name</label>
    <div class="col-lg-6 col-md-4 col-sm-8">
    <input type="text" class="form-control" name="companyName" id="companyName" placeholder="Please provide company name" required>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-sm-4" for="contactPerson">Contact person</label>
    <div class="col-lg-6 col-md-4 col-sm-8">
    <input type="text" class="form-control" name="contactPerson" id="contactPerson" placeholder="Please enter name and surname" required>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-sm-4" for="contactNumber">Contact Number</label>
    <div class="col-lg-6 col-md-4 col-sm-8">
    <input type="number" class="form-control" name="contactNumber" id="contactNumber" placeholder="Please provide your contact number"  required>
    </div>
</div> 
<div class="form-group">
    <label class="control-label col-sm-4" for="email">E-mail</label>
    <div class="col-lg-6 col-md-4 col-sm-8">
    <input type="email" class="form-control" name="email" id="email" required>
    </div>
</div>
</div>

<div id="individual">
<div class="form-group">
    <label class="control-label col-sm-4" for="individualName">Name</label>
    <div class="col-lg-6 col-md-4 col-sm-8">
    <input type="text" class="form-control" name="individualName" id="individualName" placeholder="Please provide a name" required>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-sm-4" for="individualSurname">Surname</label>
    <div class="col-lg-6 col-md-4 col-sm-8">
    <input type="text" class="form-control" name="individualSurname" id="individualSurname" placeholder="Please provide a surname" required>
    </div>
</div>
 <div class="form-group">
    <label class="control-label col-sm-4" for="individualGender">Gender</label>
    <div class="col-lg-4 col-md-4 col-sm-8">
        <select class="form-control" name="individualGender" id="individualGender" required>
            <?php if($data['gender']): foreach ($data['gender'] as $gender):?>
                    <option value="<?php echo $gender['id']; ?>" ><?php echo $gender['name']; ?></option>
            <?php endforeach;endif;?>
        </select>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-sm-4" for="individualContactNumber">Contact Number</label>
    <div class="col-lg-6 col-md-4 col-sm-8">
    <input type="hidden" name="formType" value="ajaxForm">
    <input type="number" class="form-control" name="individualContactNumber" id="individualContactNumber" placeholder="Please provide your contact number"  required>
    </div>
</div>
</div>