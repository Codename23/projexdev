<?php 
$title = 'Incident view';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php include_once('frontend/templates/menus/main-menu.php');  ?>
<!--End of navigation-->    

<?php include_once('frontend/templates/menus/side-menu.php'); ?>
	
<div class="col-lg-10">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="index.php?module=incidents&action=incidentManagement">All Incidents</a></li>
        <li><a href="index.php?module=incidents&action=reportIncident">Report Incident</a></li>
        <li><a href="index.php?module=incidents&action=incidentInvestigation">Active Investigations</a></li>
        <li><a href="index.php?module=incidents&action=incidentSignoff">Signed Off</a></li>
        <li><a href="#">Research</a></li>
        <li><a href="#">Settings</a></li>
    </ul>
    
<div class="panel-group">
     
    <div class="panel panel-default">
    <div class="panel-heading"><h4>Incident</h4></div>
    <div class="panel-body"> 
        <div class="table-responsive">          
            <table class="table">
              <tbody>
                <tr>
                  <td><strong>Category</strong></td>
                  <td colspan="2"><?php echo $data['viewincident']['category_name']; ?></td>
                </tr>
                <tr>
                  <td><strong>Type</strong></td>
                  <td colspan="2"><?php echo $data['viewincident']['type_name']; ?></td>
                </tr>
                <tr>
                  <td><strong>Incident Number</strong></td>
                  <td colspan="2"><?php echo $data['viewincident']['incident_number']; ?></td>
                </tr>
                <tr>
                  <td><strong>Date and Time of Incident</strong></td>
                  <td colspan="2"><?php echo $data['viewincident']['incident_date']; ?></td>
                </tr>
                <tr>
                  <td><strong>Department</strong></td>
                  <td colspan="2"><?php echo $data['viewincident']['department_name']; ?></td>
                </tr>
                <tr>
                  <td><strong>Department Manager</strong></td>
                  <td colspan="2"><?php  ?></td>
                </tr>
                <tr>
                  <td><strong>Shift</strong></td>
                  <td colspan="2"><?php echo $data['viewincident']['shift']; ?></td>
                </tr>
                <tr>
                  <td><strong>Reported person</strong></td>
                  <td colspan="2"></td>
                </tr>
                <tr>
                  <td><strong>Task while incident occurred</strong></td>
                  <td colspan="2"><?php echo $data['viewincident']['incident_task'] ; ?></td>
                </tr>
                <tr>
                  <td><strong>Incident Description</strong></td>
                  <td colspan="2"><?php echo $data['viewincident']['description']; ?></td>
                </tr>           
                <tr>
                  <td><strong>Reported to authorities</strong></td>
                  <td colspan="2"></td>
                </tr>
                <?php 
                   $Authorities = explode(',' , $data['viewincident']['report_authorities']);
                   $count = 1;
                   foreach($data['authorities'] as $AuthoritiesDetsArr){
                       echo '<tr>
                               <td></td>
                               <td>' . $AuthoritiesDetsArr['label_name'] . '</td>
                               <td><span style="color:green" class="glyphicon glyphicon-ok"></span> Yes</td>';
                       echo '</tr>';
                       $count ++;
                   } 
                  ?>
                <tr>
                  <td><strong>Assign Investigator</strong></td>
                  <td colspan="2"><?php echo $data['viewincident']['investigator_id']; ?></td>
                </tr> 
                <tr>
                  <td><strong>Investigation Due Date</strong></td>
                  <td colspan="2"><?php echo $data['viewincident']['due_date']; ?></td>
                </tr>  
              </tbody>
              </table>
        </div>                    
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading"><h4>Resource Involved</h4></div> 
    <div class="panel-body"> 
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Department</th>
                <th>Category</th>
                <th>Type</th>
                <th>Resource Name</th>
                <th>Resource Number</th>
                <th>Involvement Type</th>
            </tr>
          <tbody>
               <?php
              foreach($data['assetInvolved'] as $assetInvolvedDetails){
                echo '
                <tr>
                    <td>' .$assetInvolvedDetails['department_name']. '</td>
                    <td>' .$assetInvolvedDetails['category_name']. '</td>
                    <td>' .$assetInvolvedDetails['assetType']. '</td>
                    <td><a href="index.php?module=assets&action=view&id=' . $assetInvolvedDetails['id']  .'">' .$assetInvolvedDetails['name']. '</a></td>
                    <td>' .$assetInvolvedDetails['company_asset_number']. '</td>';
                    if($assetInvolvedDetails['involvement_type'] == 0){
                        echo '<td>Cause of incident</td>';
                    }else{
                        echo '<td>Damage due to incident</td>';
                    }
                echo '</tr>'; 
              }
              ?>   
          </tbody>
        </table>
        </div>                    
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading"><h4>Third Party Resource Involved</h4></div> 
    <div class="panel-body"> 
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Resource Type</th>
                <th>Description</th>
                <th>Resource Number</th>
                <th>Effect on Resource</th>
            </tr>
          <tbody>
               <?php
              foreach($data['thirdpartyAssetInvolved'] as $thirdPartyDetsArr){
                echo '
                <tr>
                    <td>' .$thirdPartyDetsArr['assetType']. '</td>
                    <td>' .$thirdPartyDetsArr['description']. '</td>
                    <td>' .$thirdPartyDetsArr['company_asset_number']. '</td>';
                    if($thirdPartyDetsArr['effect_on_asset'] == 0){
                        echo '<td>Cause of incident</td>';
                    }elseif($thirdPartyDetsArr['effect_on_asset'] == 1){
                        echo '<td>Damage Beyond Repair</td>';
                    }elseif($thirdPartyDetsArr['effect_on_asset'] == 2){
                        echo '<td>Repairable</td>';
                    }else{
                        echo '<td>No Damage</td>';
                    }
                echo '</tr>'; 
              }
              ?>   
          </tbody>
        </table>
        </div>                    
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading"><h4>Involved Parties</h4></div> 
    <div class="panel-body"> 
        <?php
        if(!empty($data['employee'])){ ?>
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Employee Number</th>
                <th>Employee Name</th>
                <th>Contact Number</th>
                <th>Email</th>
            </tr>
           <?php
            foreach($data['employee'] as $employeeDetsArr){
                switch($employeeDetsArr['field_id']){
                    case 1:
                    echo '<tr><td>' . $employeeDetsArr['field_data'].'</td>';
                    break;
                    case 2:
                    echo '<td><a href="index.php?module=employee&action=viewEmployeeInfo&id=' . $employeeDetsArr['user_id'] .'">' . $employeeDetsArr['field_data'].'</a></td>';
                    break; 
                    case 9:
                    echo '<td>' . $employeeDetsArr['field_data'].'</td>';
                    break;
                    case 10:
                    echo '<td>' . $employeeDetsArr['field_data'].'</td></tr>';
                    break;
                }
            }  
   
           ?>
        </table>
        </div> 
        <?php } 
        if(!empty($data['customer'])){ ?>
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Customer Name</th>
                <th>Company Name</th>
                <th>Email</th>
                <th>Contact Number</th>
            </tr>
           <?php
            foreach($data['customer'] as $customerDetsArr){
                echo '<tr>
                        <td><a href="">' . $customerDetsArr['contact_person']. '</a></td>
                        <td>' . $customerDetsArr['company_name']. '</td>
                        <td>' . $customerDetsArr['email']. '</td>
                        <td>' . $customerDetsArr['company_contact_number']. '</td>
                      </tr>';
            }  
   
           ?>
        </table>
        </div> 
        <?php }
        
        if(!empty($data['serviceprovider'])){ ?>
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Service Provider Name</th>
                <th>Company Name</th>
                <th>Email</th>
                <th>Contact Number</th>
            </tr>
           <?php
            foreach($data['serviceprovider'] as $serviceProviderDetsArr){
                echo '<tr>
                        <td><a href="">' . $serviceProviderDetsArr['contact_person']. '</a></td>
                        <td>' . $serviceProviderDetsArr['provider_name']. '</td>
                        <td>' . $serviceProviderDetsArr['provider_email']. '</td>
                        <td>' . $serviceProviderDetsArr['cell_number']. '</td>
                      </tr>';
            }  
   
           ?>
        </table>
        </div> 
        <?php } 
        if(!empty($data['supplier'])){ ?>
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Supplier Name</th>
                <th>Company Name</th>
                <th>Email</th>
                <th>Contact Number</th>
            </tr>
           <?php
            foreach($data['supplier'] as $supplierDetsArr){
                echo '<tr>
                        <td><a href="">' . $supplierDetsArr['contact_person']. '</a></td>
                        <td>' . $supplierDetsArr['supplier_name']. '</td>
                        <td>' . $supplierDetsArr['supplier_email']. '</td>
                        <td>' . $supplierDetsArr['cell_number']. '</td>
                      </tr>';
            }  
   
           ?>
        </table>
        </div> 
        <?php } 
        if(!empty($data['visitor'])){ ?>
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Visitor Name</th>
                <th>Company Name</th>
                <th>Email</th>
                <th>Contact Number</th>
            </tr>
           <?php
            foreach($data['visitor'] as $visitorDetsArr){
                echo '<tr>
                        <td><a href="">' . $visitorDetsArr['first_name'].' '.$visitorDetsArr['lastname'].'</a></td>
                        <td>' . $visitorDetsArr['company_name']. '</td>
                        <td>' . $visitorDetsArr['email']. '</td>
                        <td>' . $visitorDetsArr['contact_number']. '</td>
                      </tr>';
            }  
           ?>
        </table>
        </div> 
        <?php }
        if(!empty($data['publicperson'])){ ?>
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Visitor Name</th>
                <th>Company Name</th>
                <th>Email</th>
                <th>Contact Number</th>
            </tr>
           <?php
            foreach($data['publicperson'] as $publicpersonDetsArr){
                echo '<tr>
                        <td><a href="">' . $publicpersonDetsArr['first_name'].' '.$publicpersonDetsArr['lastname'].'</a></td>
                        <td>' . $publicpersonDetsArr['company_name']. '</td>
                        <td>' . $publicpersonDetsArr['email']. '</td>
                        <td>' . $publicpersonDetsArr['contact_number']. '</td>
                      </tr>';
            }  
           ?>
        </table>
        </div> 
        <?php } ?>
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading"><h4>Involved Witnesses</h4></div> 
    <div class="panel-body"> 
         <?php
        if(!empty($data['witnessEmployee'])){ ?>
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Employee Number</th>
                <th>Employee Name</th>
                <th>Contact Number</th>
                <th>Email</th>
            </tr>
           <?php
            foreach($data['witnessEmployee'] as $employeeDetsArr){
                switch($employeeDetsArr['field_id']){
                    case 1:
                    echo '<tr><td>' . $employeeDetsArr['field_data'].'</td>';
                    break;
                    case 2:
                    echo '<td><a href="index.php?module=employee&action=viewEmployeeInfo&id=' . $employeeDetsArr['user_id'] .'">' . $employeeDetsArr['field_data'].'</a></td>';
                    break; 
                    case 9:
                    echo '<td>' . $employeeDetsArr['field_data'].'</td>';
                    break;
                    case 10:
                    echo '<td>' . $employeeDetsArr['field_data'].'</td></tr>';
                    break;
                }
            }  
   
           ?>
        </table>
        </div> 
        <?php } 
        if(!empty($data['witnessCustomer'])){ ?>
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Customer Name</th>
                <th>Company Name</th>
                <th>Email</th>
                <th>Contact Number</th>
            </tr>
           <?php
            foreach($data['witnessCustomer'] as $customerDetsArr){
                echo '<tr>
                        <td><a href="">' . $customerDetsArr['contact_person']. '</a></td>
                        <td>' . $customerDetsArr['company_name']. '</td>
                        <td>' . $customerDetsArr['email']. '</td>
                        <td>' . $customerDetsArr['company_contact_number']. '</td>
                      </tr>';
            }  
   
           ?>
        </table>
        </div> 
        <?php } 
                
        if(!empty($data['witnessServiceprovider'])){ ?>
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Service Provider Name</th>
                <th>Company Name</th>
                <th>Email</th>
                <th>Contact Number</th>
            </tr>
           <?php
            foreach($data['witnessServiceprovider'] as $serviceProviderDetsArr){
                echo '<tr>
                        <td><a href="">' . $serviceProviderDetsArr['contact_person']. '</a></td>
                        <td>' . $serviceProviderDetsArr['provider_name']. '</td>
                        <td>' . $serviceProviderDetsArr['provider_email']. '</td>
                        <td>' . $serviceProviderDetsArr['cell_number']. '</td>
                      </tr>';
            }  
   
           ?>
        </table>
        </div> 
        <?php }
         if(!empty($data['witnessSupplier'])){ ?>
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Supplier Name</th>
                <th>Company Name</th>
                <th>Email</th>
                <th>Contact Number</th>
            </tr>
           <?php
            foreach($data['witnessSupplier'] as $supplierDetsArr){
                echo '<tr>
                        <td><a href="">' . $supplierDetsArr['contact_person']. '</a></td>
                        <td>' . $supplierDetsArr['supplier_name']. '</td>
                        <td>' . $supplierDetsArr['supplier_email']. '</td>
                        <td>' . $supplierDetsArr['cell_number']. '</td>
                      </tr>';
            }  
   
           ?>
        </table>
        </div> 
        <?php } 
        if(!empty($data['witnessVisitor'])){ ?>
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Visitor Name</th>
                <th>Company Name</th>
                <th>Email</th>
                <th>Contact Number</th>
            </tr>
           <?php
            foreach($data['witnessVisitor'] as $visitorDetsArr){
                echo '<tr>
                        <td><a href="">' . $visitorDetsArr['first_name'].' '.$visitorDetsArr['lastname'].'</a></td>
                        <td>' . $visitorDetsArr['company_name']. '</td>
                        <td>' . $visitorDetsArr['email']. '</td>
                        <td>' . $visitorDetsArr['contact_number']. '</td>
                      </tr>';
            }  
           ?>
        </table>
        </div> 
        <?php } 
        if(!empty($data['witnessPublicperson'])){ ?>
        <div class="table-responsive">          
        <table class="table">
            <tr>
                <th>Visitor Name</th>
                <th>Company Name</th>
                <th>Email</th>
                <th>Contact Number</th>
            </tr>
           <?php
            foreach($data['witnessPublicperson'] as $publicpersonDetsArr){
                echo '<tr>
                        <td><a href="">' . $publicpersonDetsArr['first_name'].' '.$publicpersonDetsArr['lastname'].'</a></td>
                        <td>' . $publicpersonDetsArr['company_name']. '</td>
                        <td>' . $publicpersonDetsArr['email']. '</td>
                        <td>' . $publicpersonDetsArr['contact_number']. '</td>
                      </tr>';
            }  
           ?>
        </table>
        </div> 
        <?php } ?>
    </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="post" name="addAssets" action="index.php?module=incidents&action=incidentManagement">
                <button type="submit" name="" class="btn btn-success">Back</button>
            </form>
        </div>
    </div>
</div>
<!--End of panel-group-->

  </div><!--End of row-->
</div><!--End of container-fluid-->

<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>  