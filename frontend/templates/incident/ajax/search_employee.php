<?php
/**
 * Ajax script to search resource type
 * 
 * @package sheqonline
 * @author Sunny Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
//Include the common file
require_once '../../../../config.php';

searchEmployee();

function searchEmployee() 
{
    global $db;

    $conditions = "";
    if(isset($_POST["searchterm"]) && isset($_POST["department"])){
        $conditions = "AND (tbl_users.firstname LIKE '%" . $_POST["searchterm"] . "%' OR tbl_users.lastname LIKE '%" . $_POST["searchterm"] . "%' ) AND tbl_departments.id = " . $db->sqs($_POST["department"]);
    }
    if(!isset($_POST["searchterm"]) && isset($_POST["department"])){
        $conditions = "AND tbl_departments.id = ".$db->sqs($_POST["department"]);
        
    }
    if(!isset($_POST["department"]) && isset($_POST["searchterm"])){
        $conditions = "AND tbl_users.firstname LIKE '%" . $_POST["searchterm"] . "%' OR tbl_users.lastname LIKE '%" . $_POST["searchterm"] . "%'";
    }
 
    $sql = "SELECT  tbl_branches.branch_name,
                    tbl_departments.department_name,
                    tbl_users.firstname,
                    tbl_users.lastname,
                    tbl_users.id
                    FROM tbl_users
                    LEFT JOIN tbl_department_occupations ON tbl_department_occupations.users_id  = tbl_users.id
                    LEFT JOIN tbl_branches  ON tbl_department_occupations.branch_id  = tbl_branches.id
                    LEFT JOIN tbl_departments ON tbl_department_occupations.department_id  = tbl_departments.id
                    WHERE tbl_branches.company_id = " . $db->sqs($_SESSION['company_id']) . " " . $conditions;
    $response = $db->getall($sql);

    if($response){
    ?>
    <div class="table-responsive">
    <table class="table table-hover center">
        <thead>
          <tr>
            <th>Branch Name</th>
            <th>Department</th>
            <th>Full name</th>
            <th></th>
          </tr>
        </thead>
        <tbody>   
        <?php
        foreach($response as $employeeDetailArr){
        echo
            '<tr>
                <td>' . $employeeDetailArr["branch_name"] .'</td>
                <td>' . $employeeDetailArr["department_name"] .'</td>
                <td><span class="reportedPersonName">' . $employeeDetailArr["firstname"] .' '. $employeeDetailArr["lastname"]. '</span></td> 
                <td><button type="button" value="' . $employeeDetailArr['id'] .'" class="reportedPersonBtn btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span></button></td>
            </tr>';
        }
        ?>      
        </tbody>
    </table>  
    </div>
 
    <?php
        }else{
            echo '
            <div class="form-group">
                <label class="control-label col-sm-4"></label>
                <div class="col-lg-6 col-md-4 col-sm-8"> Result not found <strong>'. $_POST["searchterm"] .'</strong><br/>
                    <a href="#addNewReportedPerson-modal" data-toggle="modal" data-target="#addNewReportedPerson-modal">
                    <button type="button" id="addNewReportedPerson" class="btn btn-success">Add New Employee</button>
                    </a>
                </div>
            </div>';
        }   
    }
    ?> 