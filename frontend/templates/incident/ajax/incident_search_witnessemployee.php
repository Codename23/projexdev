<?php
/**
 * Ajax script to search resource type
 * 
 * @package sheqonline
 * @author Sunny Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
//Include the common file
require_once '../../../../config.php';

searchWitnessEmployee();

function searchWitnessEmployee() 
{
    global $db;

    $witnessDepartment = $_POST["witnessDepartment"];
    $witnessFullname = $_POST["witnessFullname"];
    
    $conditions = "";
    
    if(!empty($witnessFullname) && !empty($witnessDepartment)){
        $conditions = "AND (tbl_users.firstname LIKE '%".$witnessFullname. "%' OR 
                       tbl_users.lastname LIKE '%".$witnessFullname. "%' )
                       AND tbl_departments.id = ".$db->sqs($witnessDepartment);
        
    }else if(!empty($witnessFullname) && empty($witnessDepartment)){
        $conditions = " AND tbl_departments.id = ".$db->sqs($witnessDepartment);
        
    }else if(!empty($witnessFullname) && empty($witnessDepartment)){
        $conditions = "AND tbl_departments.id = ".$db->sqs($witnessDepartment);
    }
    
    
    $sql = "SELECT branch_name,
                   department_name,
                   firstname,
                   lastname,
                   tbl_users.id
                   FROM tbl_users
                   LEFT JOIN tbl_department_occupations ON tbl_department_occupations.users_id  = tbl_users.id
                   LEFT JOIN tbl_branches  ON tbl_department_occupations.branch_id  = tbl_branches.id
                   LEFT JOIN tbl_departments ON tbl_department_occupations.department_id  = tbl_departments.id
                   LEFT JOIN tbl_occupations ON tbl_department_occupations.occupation_id  = tbl_occupations.id
                   WHERE tbl_branches.company_id = ".$db->sqs($_SESSION['company_id'] = 4). " " . $conditions;
    $response = $db->getall($sql);
    
    if($response){
    ?>
    <div id="personInvolvedEmployeeHide">
        <div class="col-lg-12">
            <table class="table">
            <thead>
              <tr>
                <th>Branch name</th>
                <th>Department</th>
                <th>Fullname</th>
                <th>Add</th>
              </tr>
            </thead>
            <tbody>
                <?php
                    foreach($response as $employeeDetailArr){
                    echo 
                    '<tr>
                        <td>' . $employeeDetailArr["branch_name"] .'</td>
                        <td>' . $employeeDetailArr["department_name"] .'</td>
                        <td><span class="witnessName">' . $employeeDetailArr["firstname"] .' '. $employeeDetailArr["lastname"]. '</span></td>
                        <td><button type="button" value="' . $employeeDetailArr['id'] .'" class="witnessInvolvedID btn btn-success btn-xs">
                            <span class="glyphicon glyphicon-plus"></span></button></td>
                     </tr>';
                    }
                ?> 
            </tbody>
            </table>
        </div>
    </div>   
    <?php } else {?>
            <label class="control-label col-sm-4"></label>
            <div class="col-lg-6 col-md-4 col-sm-8">Result not found <strong><?php echo $_POST["witnessFullname"]; ?></strong><br/>
                <a href="#addNewEmployee-modal" data-toggle="modal" data-target="#addNewEmployee-modal"><button type="button" id="addNewEmployee" class="btn btn-success">Add New Employee</button></a>
            </div>
    <?php
        }   
    }
    ?> 