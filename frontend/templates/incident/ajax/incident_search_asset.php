<?php
/**
 * Ajax script to search resource type
 * 
 * @package sheqonline
 * @author Sunny Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
//Include the common file
require_once '../../../../config.php';

incidentSearchAsset();

function incidentSearchAsset() 
{
    global $db;
    
    $conditions = "";
    
    if(isset($_POST["incidentAssetDepartment"])){
        $conditions .= " AND tbl_assets.department_id =  ". $db->sqs($_POST["incidentAssetDepartment"]);
    }
    
    if(isset($_POST["incidentAssetCategory"]) ){
        $conditions .=" AND categorytype_id =  ". $db->sqs($_POST["incidentAssetCategory"]);
        
    }
    
    if(isset($_POST["incidentAssetType"])){
        $conditions .= " AND resourcetype_id =  ". $db->sqs($_POST["incidentAssetType"]);
        
    }
    if(isset($_POST["incidentAssetDescription"])){
        $conditions .= " AND tbl_assets.name LIKE '%". $_POST["incidentAssetDescription"] ."%'";
    } 
    
    $sql = "SELECT tbl_assets.id,
                   tbl_assets.name,
                   tbl_assets.company_asset_number,
                   tbl_departments.department_name,
                   tbl_asset_category.name AS assetcategory_name,
                   tbl_asset_type.name AS assettype_name
                   FROM tbl_assets
                   LEFT JOIN tbl_department_assets ON tbl_department_assets.assets_id = tbl_assets.id
                   LEFT JOIN tbl_departments  ON tbl_department_assets.department_id = tbl_departments.id
                   LEFT JOIN tbl_asset_category ON tbl_assets.categorytype_id = tbl_asset_category.id
                   LEFT JOIN tbl_asset_type ON tbl_assets.resourcetype_id = tbl_asset_type.id
                   WHERE tbl_department_assets.company_id = ".$db->sqs($_SESSION['company_id']). " " . $conditions;
    $response = $db->getall($sql);
    if($response){
    ?>
    <div id="hideSearchAsset">
        <div class="col-lg-12">
            <table class="table">
            <thead>
              <tr>
                <th>Resource Name</th>
                <th>Resource Number</th>
                <th>Department</th>
                <th>Resource name</th>
                <th>Resource Type</th>
                <th>Add</th>
              </tr>
            </thead>
            <tbody>
                <?php
                    foreach($response as $assetDetailsArr){
                    echo 
                    '<tr>
                        <td class="assetName">' . $assetDetailsArr["name"] .'</td>
                        <td>' . $assetDetailsArr["company_asset_number"] .'</td>
                        <td>' . $assetDetailsArr["department_name"] .'</td>
                        <td>' . $assetDetailsArr["assetcategory_name"] .'</td>
                        <td>' . $assetDetailsArr["assettype_name"] .'</td>
                        <td><button type="button" value="' . $assetDetailsArr['id'] .'" class="assetInvolvedID btn btn-success btn-xs">
                            <span class="glyphicon glyphicon-plus"></span></button></td>
                     </tr>';
                    }
                ?> 
            </tbody>
            </table>
        </div>
    </div>   
    <?php } else {?>
            <label class="control-label col-sm-4"></label>
            <div class="col-lg-6 col-md-4 col-sm-8">Result not found <strong><?php echo $_POST["incidentAssetDescription"]; ?></strong><br/>
                <a href="#addNewAsset-modal" data-toggle="modal" data-target="#addNewAsset-modal"><button type="button" id="addNewAsset" class="btn btn-success">Add New Resource</button></a>
            </div>
    <?php
        }   
    }
    ?> 