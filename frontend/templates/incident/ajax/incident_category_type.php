<?php
/**
 * Ajax script to search resource type
 * 
 * @package sheqonline
 * @author Sunny Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
//Include the common file
require_once '../../../../config.php';


showIncidentByCategory();
function showIncidentByCategory() 
{
    global $db;
    global $objIncidents;
    
    $id = $_POST["incidentCategory"];   
    $incidentTypesDetailsArr = $objIncidents->getIncidentTypesByCategoryId($id);
     
    if(!empty($incidentTypesDetailsArr)){
        foreach($incidentTypesDetailsArr as $incidentTypesDetails){
            echo '<option value="' . $incidentTypesDetails['id'] . '">' . $incidentTypesDetails['type_name'] . '</option>';        
        }
    }   
}
?> 