<?php
/**
 * Ajax script to search resource type
 * 
 * @package sheqonline
 * @author Sunny Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
//Include the common file
require_once '../../../../config.php';

processAjax();

function processAjax() 
{
    global $db;

    $personInvolved = $_POST["employeeNameSurname"];
    $personInvolvedDepartment = $_POST["Department"];
    
    
    if(!empty($personInvolved) && !empty($personInvolvedDepartment)){
        $sql = "SELECT  branch_name,
                        department_name,
                        firstname,
                        lastname,
                        tbl_users.id
                        FROM tbl_users
                        LEFT JOIN tbl_department_occupations ON tbl_department_occupations.users_id  = tbl_users.id
                        LEFT JOIN tbl_branches  ON tbl_department_occupations.branch_id  = tbl_branches.id
                        LEFT JOIN tbl_departments ON tbl_department_occupations.department_id  = tbl_departments.id
                        LEFT JOIN tbl_occupations ON tbl_department_occupations.occupation_id  = tbl_occupations.id
                        WHERE tbl_branches.company_id = ".$db->sqs(4). " AND
                        (tbl_users.firstname LIKE '%".$personInvolved. "%' OR tbl_users.lastname LIKE '%".$personInvolved. "%' )
                        AND tbl_departments.id = ".$db->sqs($personInvolvedDepartment);
        $response = $db->getall($sql);
    }
    
    if(!empty($personInvolved) && empty($personInvolvedDepartment)){
        $sql = "SELECT branch_name,
                        department_name,
                        firstname,
                        lastname,
                        tbl_users.id
                        FROM tbl_users
                        LEFT JOIN tbl_department_occupations ON tbl_department_occupations.users_id  = tbl_users.id
                        LEFT JOIN tbl_branches  ON tbl_department_occupations.branch_id  = tbl_branches.id
                        LEFT JOIN tbl_departments ON tbl_department_occupations.department_id  = tbl_departments.id
                        LEFT JOIN tbl_occupations ON tbl_department_occupations.occupation_id  = tbl_occupations.id
                        WHERE tbl_branches.company_id = ".$db->sqs(4). " AND tbl_departments.id = ".$db->sqs($personInvolvedDepartment);
        $response = $db->getall($sql);
    }
    
    if(!empty($personInvolvedDepartment) && empty($personInvolved)){
        $sql = "SELECT branch_name,
                        department_name,
                        firstname,
                        lastname,
                        tbl_users.id
                        FROM tbl_users
                        LEFT JOIN tbl_department_occupations ON tbl_department_occupations.users_id  = tbl_users.id
                        LEFT JOIN tbl_branches  ON tbl_department_occupations.branch_id  = tbl_branches.id
                        LEFT JOIN tbl_departments ON tbl_department_occupations.department_id  = tbl_departments.id
                        LEFT JOIN tbl_occupations ON tbl_department_occupations.occupation_id  = tbl_occupations.id
                        WHERE tbl_branches.company_id = ".$db->sqs(4). " AND
                        tbl_departments.id = ".$db->sqs($personInvolvedDepartment);
        $response = $db->getall($sql);
    }
    
    if($response){
    ?>
    <div class="form-group" id="personInvolvedEmployeeHide">
        <div class="col-lg-8 col-lg-offset-2 col-md-6 col-md-6 col-sm-12">
            <table class="table">
            <thead>
              <tr>
                <th>Branch name</th>
                <th>Department</th>
                <th>Fullname</th>
                <th>Add</th>
              </tr>
            </thead>
            <tbody>
                <?php
                    foreach($response as $employeeDetailArr){
                    echo 
                    '<tr>
                        <td>' . $employeeDetailArr["branch_name"] .'</td>
                        <td>' . $employeeDetailArr["department_name"] .'</td>
                        <td><span class="involvedPersonName">' . $employeeDetailArr["firstname"] .' '. $employeeDetailArr["lastname"]. '</span></td>
                        <td><button type="button" value="' . $employeeDetailArr['id'] .'" class="personInvolvedID btn btn-success btn-xs">
                            <span class="glyphicon glyphicon-plus"></span></button></td>
                     </tr>';
                    }
                ?> 
            </tbody>
            </table>
        </div>
    </div>   
    <?php }else{?>
            <div class="form-group">
            <label class="control-label col-sm-4"></label>
            <div class="col-lg-6 col-md-4 col-sm-8">
                Result not found <strong><?php echo $personInvolved; ?></strong><br/>
                <a href="#addNewEmployee-modal" data-toggle="modal" data-target="#addNewEmployee-modal">
                <button type="button" id="addNewEmployee" class="btn btn-success">Add New Employee</button></a>
            </div>
            </div>
    <?php
        }   
    }
    ?> 