<?php
/**
 * Ajax script to update incident
 * 
 * @package sheqonline
 * @author Sunny Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

//Include the common file
require_once '../../../../config.php';

updateIncidentAjax();

function updateIncidentAjax() 
{
    if(isset($_POST['incidentid'])){
        global $db;
        $sql = "UPDATE tbl_incident SET incident_task = ". $db->sqs($_POST['incidentTask']). ",
                                        description = ". $db->sqs($_POST['incidentDescription']). ",
                                        report_authorities = ". $db->sqs($_POST['incidentReportAuthorities']). "
                                        WHERE id = ". $db->sqs($_POST['incidentid']);
        $response = $db->query($sql);
        if($response){
            return TRUE;
        }else{
            return FALSE;
        }   
    }else{
        return 'Required information is missing';
    }
}
?> 