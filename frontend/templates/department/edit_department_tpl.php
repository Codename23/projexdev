<?php 
$title = 'Department';
include_once('frontend/templates/headers/default_header_tpl.php');
?>

<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
 include_once('frontend/templates/menus/main-menu.php');
?>
<!--End of navigation-->     
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?>
<div class="col-lg-10">     
<?php 
/*
 * Include sub menu from the include file
 */
//include_once('frontend/templates/menus/sub-menu.php');
?>
<div class="panel panel-default" id="departmentsforms">
<div class="panel-heading">Department details</div>
<div class="panel-body">
<form class="form-horizontal" name="departmentsform" method="post" action="<?php echo BASE_URL; ?>/index.php?module=companies&action=editDepartment">
<?php  foreach($data["editDepartmentInfo"] as $departmentInfo): ?>
    <div class="form-group">
        <label class="control-label col-sm-4" for="departmentName">Department Name</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <input type="text" class="form-control" name="departmentName" required placeholder="Enter department name" value="<?php echo $departmentInfo['department_name']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="buildingName">Building Name</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <input type="text" class="form-control" name="buildingName" required placeholder="Enter building name" value="<?php echo $departmentInfo['department_building']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="floorName">Floor Name</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <input type="text" class="form-control" name="floorName" required placeholder="Enter floor name" value="<?php echo $departmentInfo['department_floor']; ?>">
        </div>
    </div>

    <div class="form-group">
	    <input type="hidden" name="id" value="<?php echo $departmentInfo['id']; ?>" >
		<input type="hidden" name="branchId" value="<?php echo $departmentInfo['branch_id']; ?>" >
        <input type="hidden" name="companyId" value="<?php echo $departmentInfo['company_id']; ?>" >
        <input type="hidden" name="is_active" value="tab_department" />
        <div class="col-sm-offset-4 col-sm-8">
        <button type="submit" class="btn btn-success">Save</button>
        <button type="reset" class="btn btn-default">Reset</button>
        </div>
    </div>
	<?php endforeach; ?>
</form>
</div>
</div>
<!--End of panel panel-default-->
    </div>
   </div><!--End of row -->
</div><!--End of container-fluid -->
<?php 
/*
 * 
 * 
 * 
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
    