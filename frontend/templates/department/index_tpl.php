<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">Departments</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" class="table table-hover" id="dataTables">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Department Name</th>
                            <th>Branch Name</th>
                            <th>Building Name</th>
                            <th>Floor Name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($data['allCompanyDepartments']): foreach($data['allCompanyDepartments'] as $department): ?>
                            <tr>
                                <td><?php echo $department['id']; ?></td>
                                <td><?php echo $department['department_name']; ?></td>
                                <td><?php echo $department['branch_name']; ?></td>
                                <td><?php echo $department['department_building']; ?></td>
                                <td><?php echo $department['department_floor']; ?></td>
                                <td><a href="<?php echo BASE_URL . "/index.php?module=companies&action=editDepartment&id=" .$department['id']; ?>">Edit</a></td>
                                <td class="danger"><a href="#">Delete</a></td>
                            </tr>
                        <?php endforeach;endif; ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    $('#dataTables').DataTable({
        responsive: true
    });
</script>