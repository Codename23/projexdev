<?php 
$title = 'Projex';
include_once('frontend/templates/headers/default_header_tpl.php');

include_once('frontend/templates/menus/side-menu.php'); 

include_once('frontend/templates/menus/main-menu.php'); 
?> 
<div class="page-content">
    <div class="column" style="width:100%;">
        <div class="portlet" >
        <div class="portlet-header">Incidents</div>
        <div class="portlet-content">
            <div style="width:100%;">
            <canvas id="canvas"></canvas>
        </div>
        </div>
        </div>
    </div>

    <div class="column">
        <div class="portlet">
        <div class="portlet-header">New Resources</div>
        <div class="portlet-content"><li>Forklift</li><li>Grinder</li><li>Wood</li><li>Fire Extinguishers</li></div>
        </div>

        <div class="portlet">
        <div class="portlet-header">Corrective Actions</div>
        <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
        </div>
    </div>

    <div class="column">
        <div class="portlet">
        <div class="portlet-header">Non-Conformances</div>
        <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
        </div>

        <div class="portlet">
        <div class="portlet-header">Inspections</div>
        <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
        </div>
    </div>
</div>
<!--END CONTENT-->

<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>

<script>
var config = {
    type: 'line',
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            label: "Unfilled",
            fill: false,
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }, {
            label: "Dashed",
            fill: false,
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            borderDash: [5, 5],
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }, {
            label: "Filled",
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.red,
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
            fill: true,
        }]
    },
    options: {
        responsive: true,
        title:{
            display:true,
            text:'Chart.js Line Chart'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Month'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Value'
                }
            }]
        }
    }
};
window.onload = function() {
    var ctx = document.getElementById("canvas").getContext("2d");
    window.myLine = new Chart(ctx, config);
};

$( ".column" ).sortable({
  connectWith: ".column",
  handle: ".portlet-header",
  cancel: ".portlet-toggle",
  placeholder: "portlet-placeholder ui-corner-all"
});

$( ".portlet" )
  .addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
  .find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" )
    .prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

$( ".portlet-toggle" ).on( "click", function() {
  var icon = $( this );
  icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
  icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
});
</script>  