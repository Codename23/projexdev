<?php  global $objDepartment; ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Branch list</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" class="table table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Branch Name</th>
                            <th>Office Address</th>
                            <th>Office Telephone</th>
                            <th>Fax Number</th>
                            <th>E-mail Address</th>
                            <th>Number of Departments</th>
                            <th>Add Department</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($data['allCompanyBranches'])): foreach($data['allCompanyBranches'] as $branch):  ?>
                            <tr>
                                <td><?php echo $branch['id']; ?></td>
                                <td><a href="#"><?php echo $branch['branch_name']; ?></a></td>
                                <td><?php echo $branch['branch_address']; ?></td>
                                <td><?php echo $branch['branch_tel_number']; ?></td>
                                <td><?php echo $branch['branch_fax_number']; ?></td>
                                <td><?php echo $branch['branch_email']; ?></td>
                                <td><?php echo $objDepartment->getNumberOfBranchDepartments($branch['id']); ?></td>
                                <td><a href="#department-modal" data-toggle="modal" data-target="#department-modal" data-id="<?php echo $branch['id'];?>" class="department-modal">Add</a></td>
                                <td><a href="<?php echo BASE_URL . "/index.php?module=companies&action=editBranch&id=" .$branch['id']; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                <td class="danger"><a href="<?php echo BASE_URL . "/index.php?module=companies&action=deleteBranch&id=" .$branch['id']; ?>"><span class="glyphicon glyphicon-trash"></span><a></td>
                            </tr>
                        <?php endforeach; endif;?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!--End of panel panel-default-->


</div>
</div>

<!-- Modal -->
<div class="modal fade" id="department-modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Department Details</h4>
            </div>
            <div class="modal-body">
                    <form class="form-horizontal" name="departmentsform"  method="post" action="<?php echo BASE_URL; ?>/index.php?module=companies&action=addDepartment">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="departmentName">Department Name</label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <input type="text" class="form-control" id="departmentName" name="departmentName" required placeholder="Enter department name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="buildingName">Building Name</label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <input type="text" class="form-control" id="officeAddress"  name="buildingName" required placeholder="Enter building name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="floorName">Floor Name</label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                            <input type="text" class="form-control" id="floorName"  name="floorName"  required placeholder="Enter floor name">
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="hidden" class="form-control" id="branchId" name="branchId">
                        <input type="hidden" class="form-control" id="companyId" name="companyId" value="<?php echo $_GET['id']; ?>">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" class="btn btn-success">Save</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!--End Of Modal -->
<script>
    $(document).on("click", ".department-modal", function () {
        var branchId = $(this).data('id');
        $(".modal-body #branchId").val(branchId);
    });
    $('#dataTables-example').DataTable({
        responsive: true
    });
</script>