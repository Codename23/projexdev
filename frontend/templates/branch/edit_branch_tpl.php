<?php
$title = 'Company Information';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
    <div class="container-fluid">
        <!--navigation-->
        <?php
        /*
         * Include  main menu from the include file
         */

        include_once('frontend/templates/menus/main-menu.php');
        ?>
        <!--End of navigation-->

        <?php
        /*
         * Include side menu from the include file
         */

        include_once('frontend/templates/menus/side-menu.php');
        ?>

        <div class="col-lg-10">
            <?php
            /*
             * Include sub menu from the include file
             */

            //include_once('frontend/templates/menus/sub-menu.php');
            ?>

<!--End of panel panel-default-->
<div class="panel panel-default" id="additionalbranchforma">
<form enctype="multipart/form-data" class="form-horizontal" name="additionalbranchForm" id="additionalbranchForms" method="post" action="<?php echo BASE_URL; ?>/index.php?module=companies&action=editBranch">

<div class="panel-heading">Branch details</div>
<div class="panel-body">

	 <div class="form-group">
        <label class="control-label col-sm-4" for="officeAddress">Branch Name</label>
        <div class="col-lg-6 col-md-4 col-sm-8">
        <input type="text" class="form-control" name="branchName"  id="branchName" value="<?php echo $data["editBranchInfo"]['branch_name']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="officeAddress">Office Address</label>
        <div class="col-lg-6 col-md-4 col-sm-8">
        <textarea class="form-control" name="officeAddress"  id="officeAddress"><?php echo $data["editBranchInfo"]['branch_address']; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="officeNumber">Office Number</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <input type="tel" class="form-control" name="officeNumber" id="officeNumber"  placeholder="Enter office number" value="<?php echo $data["editBranchInfo"]['branch_tel_number']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="faxNumber">Fax Number</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <input type="tel" class="form-control" name="faxNumber" id="faxNumber" placeholder="Enter fax number" value="<?php echo $data["editBranchInfo"]['branch_fax_number']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="officeEmail">Office E-Mail Address</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <input type="email" class="form-control" name="officeEmail" id="officeEmail" required placeholder="Enter e-mail address" value="<?php echo $data["editBranchInfo"]['branch_email']; ?>">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4" for="branchCountryName">Country</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" name="branchCountryName" id="branchCountryName">
                <?php foreach ($data['countries'] as $country): ?>
                    <?php if($data["editBranchInfo"]['country_id'] == $country['iso']): ?>
                        <option value="<?php echo $country['iso']; ?>" selected><?php echo $country['name']; ?></option>
                    <?php else: ?>
                        <option value="<?php echo $country['iso']; ?>"><?php echo $country['name']; ?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="branchRegionName">State/Province</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" name="branchRegionName" id="branchRegionName">
                <?php foreach ($data['companyRegions'] as $region): ?>
                    <?php if($data["editBranchInfo"]['region_id'] == $region['id']): ?>
                        <option value="<?php echo $region['id']; ?>" selected><?php echo $region['name']; ?></option>
                        <?php else: ?>
                        <option value="<?php echo $region['id']; ?>"><?php echo $region['name']; ?></option>
                        <?php endif; ?>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-8">
		<input type="hidden" name="id" value="<?php echo $data["editBranchInfo"]['id']; ?>">
        <input type="hidden" name="companyId" value="<?php echo $data["editBranchInfo"]['company_id']; ?>">
         <input type="hidden" name="is_active" value="tab_branch" />
        <button type="submit" class="btn btn-success">Save</button>
        <button type="reset" class="btn btn-default">Reset</button>
        </div>
    </div>
</div>

</form>
</div>
<!--End of panel panel-default-->
<?php
/*
 *
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
include_once('frontend/templates/footers/regions_ajax_tpl.php');
?>