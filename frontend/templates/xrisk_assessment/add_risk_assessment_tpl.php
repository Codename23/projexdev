<?php
/*
 * Header file
 */
$title = 'Add Risk Assessment';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */
    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <?php
    /*
     * Include side menu from the include file
     */
    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-justified topbar-menu">
                    <li><a href="risk_assessment_dashboard.php">Dashboard</a></li>
                    <li><a href="risk_assessment_startra.php">Start R/A</a></li>
                    <li class="active"><a href="risk_assessment_schedulera.php">Schedule R/A</a></li>
                    <li><a href="risk_assessment_openra.php">Open R/A</a></li>
                    <li><a href="risk_assessment_recommendations.php">Recommendations</a></li>
                    <li><a href="risk_assessment_rahistory.php">R/A History</a></li>
                    <li><a href="">Risk Register</a></li>
                </ul>
            </div>
        </div>

        <form class="form-horizontal" name="riskAssessmentForm" method="post" action="<?php echo BASE_URL . "/index.php?module=risk_assessment&action=addRiskAssessment"; ?>">
            <div class="panel-group assessmentHeader">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <?php echo $data['riskAssessmentHeaderInfo']['group_name']; ?>
                        </h4>
                    </div>
                    <div class="panel-body">
                        <?php
                        $riskAssessmentType= $data['riskAssessmentType'];
                        $departments = $data['departments'];
                        $categoryTypes = $data['categoryTypes'];
                        $assetTypes = $data['assetTypes'];
                        $riskAutoNo = $data['riskAutoNo'];
                        ?>
                        <?php include_once(TEMPLATES.'/risk_assessment/header_template/'.strtolower($data['riskAssessmentHeaderInfo']['group_name']).'_header_tpl.php'); ?>
                    </div>
                </div>
            </div>
            <div class="load_groups"></div>

        </form>

        <!--End of panel panel-default-->

    </div>
</div><!--End of row-->
</div><!--End of container-fluid-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>
<?php include_once('frontend/templates/footers/risk_ajax_tpl.php'); ?>



<script>
    $(function() {
        $("#date").datepicker({
            dateFormat:"yy/mm/dd",
            maxDate: new Date(),
            minDate: new Date()});
        $("#date").datepicker("setDate", new Date());
    });

</script>


