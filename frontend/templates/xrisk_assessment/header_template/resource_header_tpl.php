<div class="row">
    <div class="form-group">
        <label class="control-label col-sm-4" for="date">Date</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" id="date" name="riskAssessmentDate" required>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="assessor">Name of Assessor</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" id="assessor" value="<?php echo $_SESSION['firstname']." ". $_SESSION['username']; ?> ">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="riskNo">Risk No.</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" name="riskNo"  id="riskNo" value="<?php echo $riskAutoNo; ?>" readonly>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="departmentName">Department Name</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="selectpicker form-control" name="departmentName[]" id="RiskDepartmentName" data-header="Close"  multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments">

                <?php if($departments): foreach($departments as $department) : ?>
                    <option value="<?php echo $department['id'] ; ?>"><?php echo $department['department_name'] ; ?></option>
                <?php endforeach; endif; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="resource">Resource Category</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" name="resourceCategory" id="resourceCategory" required>
                <option disabled selected value="0">Select Category </option>
                <?php if($categoryTypes): foreach($categoryTypes as $resourceCategory) : ?>
                    <option value="<?php echo $resourceCategory['id'] ; ?>"><?php echo $resourceCategory['name'] ; ?></option>
                <?php endforeach; endif; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="type">Resource Type</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" name="resourceType" id="resourceType" required>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="selectResource">Select Resource</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="selectpicker form-control"  name="selectResource[]" id="selectResource"  data-header="Close"   multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more resources" required>

            </select>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
        <button type="submit"  id="btn_add_Risk_Assessments" class="btn btn-success">Add Risk Assessment</button>
    </div>
</div>
<input type="hidden" name="riskAssessmentType" id="riskAssessmentType"  value="<?php echo $riskAssessmentType; ?>"  />