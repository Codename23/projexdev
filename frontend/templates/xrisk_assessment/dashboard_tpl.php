<?php
/*
 * Header file
 */
$title = 'Risk Assessment';
include_once('frontend/templates/headers/default_header_tpl.php');

?>
<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */

    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <?php
    /*
     * Include side menu from the include file
     */
    include_once('frontend/templates/menus/side-menu.php');

    ?>
    <!--End of navigation-->
    <div class="col-lg-10">
        <!--sub menu-->
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-justified topbar-menu">
                    <li class="active"><a href="index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Dashboard</a></li>
                    <li><a href="index.php?action=startRiskAssessment&module=risk_assessment">Start R/A</a></li>
                    <li><a href="risk_assessment_schedulera.php">Schedule R/A</a></li>
                    <li><a href="risk_assessment_openra.php">Open R/A</a></li>
                    <li><a href="risk_assessment_recommendations.php">Recommendations</a></li>
                    <li><a href="risk_assessment_rahistory.php">R/A History</a></li>
                    <li><a href="">Risk Register</a></li>
                </ul>
            </div>
        </div>
        <!--End of sub menu-->
        <div class="panel panel-default">
            <div class="panel-heading">Upcoming Risk Assessments</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" class="table table-hover">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Type</th>
                            <th>Department</th>
                            <th>Risk Assessor</th>
                            <th>Occupation</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><a href="#">Start</a></td>
                            <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><a href="#">Start</a></td>
                            <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><a href="#">Start</a></td>
                            <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--End of the panel panel-default-->
        <div class="panel panel-default">
            <div class="panel-heading">Open Risk Assessments</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" class="table table-hover">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Assessment No</th>
                            <th>Type</th>
                            <th>Department</th>
                            <th>Risk Assessor</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($data['inCompleteRiskAssessments']): foreach ($data['inCompleteRiskAssessments'] as $riskAssessment): ?>
                        <tr>
                            <td><?php echo $riskAssessment['risk_assessment_date']; ?></td>
                            <td><?php echo $riskAssessment['risk_assessment_no']; ?></td>
                            <td><?php echo $riskAssessment['type_name']; ?></td>
                            <td></td>
                            <td><?php echo $riskAssessment['firstname']. " ". $riskAssessment['lastname'] ; ?></td>
                            <td><a href="<?php echo BASE_URL . "/index.php?module=risk_assessment&action=completeRiskAssessment&id=" . $riskAssessment['id'] ; ?>">Complete</a></td>
                            <td><a href="<?php echo BASE_URL . "/index.php?module=risk_assessment&action=editRiskAssessmentData&id=" . $riskAssessment['id']."&riskType=". $riskAssessment['assessment_type_id']; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        </tr>

                        <?php endforeach;endif; ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--End of the panel panel-default-->
    </div>
</div><!--End of row-->
</div><!--End of container-fluid-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>
