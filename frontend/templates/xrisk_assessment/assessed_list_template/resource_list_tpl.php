<?php
/**
 * List assessed items
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */
?>
<div class="panel panel-default">
    <div class="panel-heading">Resource Listing</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table width="100%" class="table table-hover">
                <thead>
                <tr>
                    <th>Department</th>
                    <th>Resource No</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                <?php if($assessedItemsInfo): foreach ($assessedItemsInfo as $assetInfo) : ?>
                <tr>
                    <td><?php echo $assetInfo['department_name']; ?></td>
                    <td><?php echo $assetInfo['company_asset_number']; ?></td>
                    <td><?php echo $assetInfo['asset_name']; ?></td>
                </tr>
                <?php endforeach;endif; ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
