<?php
/**
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

?>
<div class="col-lg-12">
    <?php if($data['allEmployeeResults']) : ?>
    <table class="table">
            <thead>
            <tr>
                <th>Branch name</th>
                <th>Department</th>
                <th>Fullname</th>
                <th>Add</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data['allEmployeeResults'] as $employee) : ?>
                    <tr>
                        <td><?php echo  $employee["branch_name"]; ?></td>
                        <td><?php echo  $employee["department_name"]; ?></td>
                        <td><span class="assistanceName"><?php echo $employee["firstname"] .' '. $employee["lastname"]; ?></span></td>
                        <td><button type="button" value="<?php echo $employee['id'] ; ?>" class="addAssistanceID btn btn-success btn-xs">
                            <span class="glyphicon glyphicon-plus"></span></button></td>
                     </tr>
         <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
    <div class="form-group">
        <div class="col-lg-12 col-md-12 col-sm-12">Result not found <strong><?php echo $data['searchedEmployee']; ?></strong><br />
            <?php if($_SESSION['usertype_id'] == 1): ?>
                <a data-toggle="sheqModal"  href="index.php?action=modalAddEmployee&module=modal_request"><button type="button" id="addInternalPerson" class="btn btn-success">Add Internal Person</button></a>
                <a data-toggle="sheqModal"  href="index.php?action=modalAddServiceProvider&module=modal_request"><button type="button" id="addExternalPerson" class="btn btn-success">Add External Person</button></a>
            <?php endif;?>
        </div>
    </div>
    <?php endif; ?>
 </div>

