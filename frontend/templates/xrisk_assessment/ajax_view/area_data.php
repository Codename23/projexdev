<?php
/**
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */
?>

    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
            <tr>
                <th>
                    Activity No</th>
                <th></th>
                <th>Hazard</th>
                <th>Risk</th>
                <th>Score</th>
                <th>View Recommendations</th>
                <th>Final Score</th>
                <th>Photo</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            <?php if($data['riskAssessmentAreas']): foreach($data['riskAssessmentAreas']  as $areas => $area): ?>
                <?php if($area['areas_data']): foreach($area['areas_data'] as $data): ?>
                   <tr>
                    <td><?php echo ($data['r_data_id']) ? 'RA'.$data['r_data_id'] : ''; ?></td>
                    <td><?php echo $data['area_name']; ?></td>
                    <td><?php  echo $data['hazard']; ?></td>
                    <td><?php  echo $data['risk']; ?></td>
                    <td><?php  echo $data['score']; ?></td>
                    <td>
                        <?php if($data['noOfRecommendations'] > 0): ?>
                        <a data-toggle="sheqModal"  href="index.php?action=modalEditRiskRecommendations&module=modal_request&recommendationId=<?php echo $data['r_data_id']; ?> "><?php echo $data['noOfRecommendations'] ;?> Recommendations</a>
                        <?php endif; ?>
                    </td>
                      <td><?php echo $data['final_score']; ?></td>
                    <td>no photo</td>
                       <td>
                           <?php if($data['r_data_id']): ?>
                             <a data-toggle="sheqModal"  href="index.php?action=modalEditRiskAssessment&module=modal_request&id=<?php echo $data['r_data_id']; ?> "><span class="glyphicon glyphicon-pencil"></a>
                        <?php endif; ?>

                        </td>
                       <td><a href="#risk-assessment-modal" data-toggle="modal" data-target="#risk-assessment-modal" class="add_risk_assessment"
                              data-group_id="<?php echo $data['groupId'];  ?>" data-area_id="<?php echo $data['area_id']; ?>" data-area_name="<?php echo $data['area_name']; ?>" data-risk_id="<?php echo $_SESSION['riskNumber']; ?>" >
                               <span class="glyphicon glyphicon-plus"></span></a></td>
                          </tr>
                            <?php endforeach;endif; ?>




            <?php endforeach;endif; ?>

            </tbody>
        </table>
    </div>



<script>

    $(document).on('click','[data-toggle="sheqModal"]',function(e){
        $('.modal-backdrop').remove();
        $("#risk-assessment-score-modal").modal('hide');
        $('#risk-assessment-modal').modal('hide');
        $('.modal-backdrop').hide();
        $('#sheqModal').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),
            //$modal=$('<div class="modal" id="sheqModal"><div class="modal-body"></div></div>');
        $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-dialog" ><div class="modal-body"></div></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    });

</script>