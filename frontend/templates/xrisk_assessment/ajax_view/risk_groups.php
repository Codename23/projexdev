<?php
/**
 * Group risks
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */
?>
<?php echo  $_SESSION['riskNumber']; ?>
<div class="accordion_data">
<div class="panel-group" id="accordion">
    <?php if($data['riskAssessmentGroups']): foreach ($data['riskAssessmentGroups'] as $group): ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $group['id']; ?>"><?php echo $group['group_name']; ?></a>
                </h4>
            </div>
            <div id="collapse_<?php echo $group['id']; ?>" data="<?php echo $group['id']; ?>" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="area_data"></div>
                </div>
            </div>
        </div>
    <?php endforeach;endif;?>
</div>
</div>
<!--Risk Assessment Modal -->
<div class="modal fade" id="risk-assessment-modal" role="dialog">
    <div class="modal-dialog risk-assessment-dialog-modal">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Risk Assessment</h4>
            </div>
            <div class="modal-body table-responsive">
                <form class="form-horizontal" name="riskAssessmentForm" id="riskAssessmentForm" enctype="multipart/form-data" method="post" action="<?php echo BASE_URL; ?>/index.php?module=risk_assessment&action=addRiskAssessmentData">

                    <table width="100%" class="table table-hover">
                        <thead>
                        <tr>
                            <th colspan="2">Risk Assessment</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td width="25%">Safety</td>
                            <td colspan="3">

                            </td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Hazard</td>
                            <td colspan="3">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control risk-input" name="hazard" id="hazard" required placeholder="Enter a hazard">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Risk</td>
                            <td colspan="3">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control risk-input" name="risk" id="risk" required placeholder="Enter a risk">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Score</td>
                            <td colspan="2">

                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control pscore-input" name="pscore" id="pscore" style="border: 0px;"  readonly="readonly">
                                    <span>
                                        <a href="#risk-assessment-score-modal" data-toggle="modal" data-target="#risk-assessment-score-modal"><button type="button" class="btn btn-success">Add Score</button></a>
                                            </span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4"><b>Recommendation & Reduction</b></td>
                        </tr>
                        <?php if(count($data['riskAssessmentRecommendations'])): foreach ($data['riskAssessmentRecommendations'] as $recommendation): ?>
                        <tr>
                            <td class="risk-border-right">-<?php echo $recommendation['percentage']; ?>%</td>
                            <td width="25%" class="risk-border-right"><?php echo $recommendation['type_name']; ?></td>
                            <td class="risk-border-right">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control risk-input" name="riskRecommendation[<?php echo $recommendation['id']; ?>][<?php echo $recommendation['percentage']; ?>]" id="elimination" required placeholder="Enter a recommendation">
                                </div>
                            </td>
                            <td width="7%">
                                <div class="form-group risk-group-input">
                                    <div class="col-lg-12 col-md-4 col-sm-8">
                                        <select class="form-control risk-input" name="P1-4" id="P1-4" required>
                                            <option value="1">P1</option>
                                            <option value="2">P2</option>
                                            <option value="3">P3</option>
                                            <option value="4">P4</option>
                                            <option value="5">P5</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; endif;?>

                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Final Score</td>
                            <td colspan="3">50</td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Upload/Take Photo</td>
                            <td colspan="3">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="file" id="photo" name="photo">
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="form-group">
                        <input type="hidden" name="risk_group_id" id="risk_group_id" />
                        <input type="hidden" name="risk_area_id" id="risk_area_id" />
                        <input type="hidden" name="risk_risk_id" id="risk_risk_id" />
                        <div class="col-sm-8">
                            <button type="button" id="addRiskDataModal" class="btn btn-success">Add Risk Assessment</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">


                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!--End Of Modal -->

<!-- Modal -->
<div class="modal fade" id="risk-assessment-score-modal" role="dialog">
    <div class="modal-dialog risk-assessment-score-dialog-modal">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Risk Assessment Score</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" name="riskAssessmentScoreForm" >
                    <div class="table-responsive">
                        <table width="100%" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th class="text-center" colspan="6">Severity of the potential injury/damage</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="text-center">
                                <td></td>
                                <td width="16%"><p>Insignificant damage to Property, Equipment or Minor Injury</p></td>
                                <td width="16%"><p>Non-Reportable Injury, minor loss of Process or slight damage to Property</p></td>
                                <td width="16%"><p>Reportable Injury moderate loss of Process or limited damage to Property</p></td>
                                <td width="16%"><p>Major Injury, Single Fatality critical loss of Process/damage to Property</p></td>
                                <td width="16%"><p>Multiple Fatalities Castastrophic Loss of Business</p></td>
                            </tr>
                            <tr class="text-center">
                                <td><b>Likelihood of the hazard happening</b></td>
                                <td>1</td>
                                <td>2</td>
                                <td>3</td>
                                <td>4</td>
                                <td>5</td>
                            </tr>

                            <tr class="text-center number">
                                <td class="sideTitle">Almost Certain<br/>50</td>
                                <td class="green-btn">50</td>
                                <td class="yellow-btn">100</td>
                                <td class="red-btn">150</td>
                                <td class="majordanger-btn">200</td>
                                <td class="majordanger-btn">250</td>
                            </tr>
                            <tr class="text-center number">
                                <td class="sideTitle">Will probably occur<br/>40</td>
                                <td class="green-btn">40</td>
                                <td class="yellow-btn">80</td>
                                <td class="red-btn">120</td>
                                <td class="majordanger-btn">160</td>
                                <td class="majordanger-btn">250</td>
                            </tr>
                            <tr class="text-center number">
                                <td class="sideTitle">Possible occur<br/>30</td>
                                <td class="green-btn">30</td>
                                <td class="yellow-btn">60</td>
                                <td class="yellow-btn">90</td>
                                <td class="red-btn">120</td>
                                <td class="red-btn">150</td>
                            </tr>
                            <tr class="text-center number">
                                <td class="sideTitle">Remote possibility<br/>20</td>
                                <td class="green-btn">20</td>
                                <td class="green-btn">40</td>
                                <td class="yellow-btn">60</td>
                                <td class="yellow-btn">80</td>
                                <td class="yellow-btn">100</td>
                            </tr>
                            <tr id="extreme" class="text-center number">
                                <td class="sideTitle">Extremely Unlikely<br/>100</td>
                                <td class="green-btn">10</td>
                                <td class="green-btn">20</td>
                                <td class="green-btn">30</td>
                                <td class="green-btn">40</td>
                                <td class="green-btn">50</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8">
                            <button type="button" id="riskAssessmentBtn" class="btn btn-success">Add Score</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!--End Of Modal -->
<script>



    $('.modal-backdrop').hide();
    $('#risk-assessment-modal').modal('hide');

    $('.accordion_data #accordion').on('show.bs.collapse', function (e) {
      //  e.preventDefault();
       var group_id  = e.target.id;
        //var group_id = $(this).attr("data");
        //alert("asds"+group_id);
        $.post('<?php echo BASE_URL . "/index.php?module=risk_assessment&action=viewGroupData"; ?>', {'groupId':group_id}, function(data){
            $('.area_data').html(data);
        });

    });
    /*$('.collapse').on('show.bs.collapse', function (e) {
        var clicked = e.target;
        var jQueryClicked = $(e.target);
    });

    $('#accordion').on('show.bs.collapse', function (e) {
        var tId = e.target.id;
        var clicked  = $('#accordion a[href="#'+tId+'"]');
        alert("asds"+tId);
        console.log(clicked)
        // do something…
    });*/
    $("#riskAssessmentBtn").click(function(){
        $("#risk-assessment-score-modal").modal('toggle');
    });

    $('.green-btn, .yellow-btn, .red-btn, .majordanger-btn').click( function() {
        $(this).parents('table').find('td').each( function( index, element ) {
            $(element).removeClass('on');
        } );
        $(this).addClass('on');
    } );

    $( ".number td:not(.sideTitle)" ).click(function() {
        //alert($(this).text());
        $("#pscore").val($(this).text());
    });


    $(document).on("click", ".add_risk_assessment", function () {
        var group_id = $(this).data('group_id');
        var area_id= $(this).data('area_id');
        var risk_id= $(this).data('risk_id');

        $(".modal-body #risk_group_id").val(group_id);
        $(".modal-body #risk_area_id").val(area_id);
        $(".modal-body #risk_risk_id").val(risk_id);


    });

    $('#addRiskDataModal').click(function() {

        $("#risk-assessment-score-modal").modal('hide');
        $('#risk-assessment-modal').modal('hide');
        $('.modal-backdrop').hide();
        $.post('<?php echo BASE_URL . "/index.php?module=risk_assessment&action=addRiskAssessmentData"; ?>',$("#riskAssessmentForm").serialize() )
            .done(function(data){
                $('.load_groups').html(data);
            });
    });
</script>




