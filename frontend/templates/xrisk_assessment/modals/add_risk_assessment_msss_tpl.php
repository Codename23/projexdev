<?php
/**
 * Risk Assessment modal
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */
?>
<!--Risk Assessment Modal -->
    <div class="modal-dialog risk-assessment-dialog-modal">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Risk Assessment</h4>
            </div>
            <div class="modal-body table-responsive">
                <form class="form-horizontal" name="riskAssessmentForm" method="post" action="">

                    <table width="100%" class="table table-hover">
                        <thead>
                        <tr>
                            <th colspan="2">Risk Assessment</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td width="25%">Safety</td>
                            <td colspan="3">

                            </td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Hazard</td>
                            <td colspan="3">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control risk-input" name="hazard" id="hazard" required placeholder="Enter a hazard">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Risk</td>
                            <td colspan="3">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control risk-input" name="risk" id="risk" required placeholder="Enter a risk">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Score</td>
                            <td colspan="2">
                                <p id="pscore"></p>
                                <a href="#risk-assessment-score-modal" data-toggle="modal" data-target="#risk-assessment-score-modal"><button type="button" class="btn btn-success">Add Score</button></a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4"><b>Recommendation & Reduction</b></td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">-100%</td>
                            <td width="25%" class="risk-border-right">Elimination</td>
                            <td class="risk-border-right">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control risk-input" name="elimination" id="elimination" required placeholder="Enter a recommendation">
                                </div>
                            </td>
                            <td width="7%">
                                <div class="form-group risk-group-input">
                                    <div class="col-lg-12 col-md-4 col-sm-8">
                                        <select class="form-control risk-input" name="P1-4" id="P1-4" required>
                                            <option value="1">P1</option>
                                            <option value="2">P2</option>
                                            <option value="3">P3</option>
                                            <option value="4">P4</option>
                                            <option value="5">P5</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">-80%</td>
                            <td class="risk-border-right">Substitution</td>
                            <td class="risk-border-right">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control risk-input" name="substitution" id="substitution" required placeholder="Enter a recommendation">
                                </div>
                            </td>
                            <td>
                                <div class="form-group risk-group-input">
                                    <div class="col-lg-12 col-md-4 col-sm-8">
                                        <select class="form-control risk-input" name="P1-4" id="P1-4" required>
                                            <option value="1">P1</option>
                                            <option value="2">P2</option>
                                            <option value="3">P3</option>
                                            <option value="4">P4</option>
                                            <option value="5">P5</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">-50%</td>
                            <td class="risk-border-right">Engineering Control</td>
                            <td class="risk-border-right">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control risk-input" name="engineering" id="engineering" required placeholder="Enter a recommendation">
                                </div>
                            </td>
                            <td>
                                <div class="form-group risk-group-input">
                                    <div class="col-lg-12 col-md-4 col-sm-8">
                                        <select class="form-control risk-input" name="P1-4" id="P1-4" required>
                                            <option value="1">P1</option>
                                            <option value="2">P2</option>
                                            <option value="3">P3</option>
                                            <option value="4">P4</option>
                                            <option value="5">P5</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">-25%</td>
                            <td class="risk-border-right">Administrative Controls</td>
                            <td class="risk-border-right">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control risk-input" name="administrative" id="administrative" required placeholder="Enter a recommendation">
                                </div>
                            </td>
                            <td>
                                <div class="form-group risk-group-input">
                                    <div class="col-lg-12 col-md-4 col-sm-8">
                                        <select class="form-control risk-input" name="P1-4" id="P1-4" required>
                                            <option value="1">P1</option>
                                            <option value="2">P2</option>
                                            <option value="3">P3</option>
                                            <option value="4">P4</option>
                                            <option value="5">P5</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">-15%</td>
                            <td class="risk-border-right">PPE</td>
                            <td class="risk-border-right">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control risk-input" name="ppe" id="ppe" required placeholder="Enter a recommendation">
                                </div>
                            </td>
                            <td>
                                <div class="form-group risk-group-input">
                                    <div class="col-lg-12 col-md-4 col-sm-8">
                                        <select class="form-control risk-input" name="P1-4" id="P1-4" required>
                                            <option value="1">P1</option>
                                            <option value="2">P2</option>
                                            <option value="3">P3</option>
                                            <option value="4">P4</option>
                                            <option value="5">P5</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Final Score</td>
                            <td colspan="3">50</td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Upload/Take Photo</td>
                            <td colspan="3">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="file" id="photo" name="photo">
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="form-group">
                        <div class="col-sm-8">
                            <button type="submit" class="btn btn-success">Add Risk Assessment</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
<!--End Of Modal -->
