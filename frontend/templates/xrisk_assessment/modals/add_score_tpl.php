<!-- Modal -->
<div class="modal-dialog risk-assessment-score-dialog-modal" id="risk-assessment-score-modal">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Risk Assessment Score</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" name="riskAssessmentScoreForm" >
                    <div class="table-responsive">
                        <table width="100%" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th class="text-center" colspan="6">Severity of the potential injury/damage</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="text-center">
                                <td></td>
                                <td width="16%"><p>Insignificant damage to Property, Equipment or Minor Injury</p></td>
                                <td width="16%"><p>Non-Reportable Injury, minor loss of Process or slight damage to Property</p></td>
                                <td width="16%"><p>Reportable Injury moderate loss of Process or limited damage to Property</p></td>
                                <td width="16%"><p>Major Injury, Single Fatality critical loss of Process/damage to Property</p></td>
                                <td width="16%"><p>Multiple Fatalities Castastrophic Loss of Business</p></td>
                            </tr>
                            <tr class="text-center">
                                <td><b>Likelihood of the hazard happening</b></td>
                                <td>1</td>
                                <td>2</td>
                                <td>3</td>
                                <td>4</td>
                                <td>5</td>
                            </tr>

                            <tr class="text-center number">
                                <td class="sideTitle">Almost Certain<br/>50</td>
                                <td class="green-btn">50</td>
                                <td class="yellow-btn">100</td>
                                <td class="red-btn">150</td>
                                <td class="majordanger-btn">200</td>
                                <td class="majordanger-btn">250</td>
                            </tr>
                            <tr class="text-center number">
                                <td class="sideTitle">Will probably occur<br/>40</td>
                                <td class="green-btn">40</td>
                                <td class="yellow-btn">80</td>
                                <td class="red-btn">120</td>
                                <td class="majordanger-btn">160</td>
                                <td class="majordanger-btn">250</td>
                            </tr>
                            <tr class="text-center number">
                                <td class="sideTitle">Possible occur<br/>30</td>
                                <td class="green-btn">30</td>
                                <td class="yellow-btn">60</td>
                                <td class="yellow-btn">90</td>
                                <td class="red-btn">120</td>
                                <td class="red-btn">150</td>
                            </tr>
                            <tr class="text-center number">
                                <td class="sideTitle">Remote possibility<br/>20</td>
                                <td class="green-btn">20</td>
                                <td class="green-btn">40</td>
                                <td class="yellow-btn">60</td>
                                <td class="yellow-btn">80</td>
                                <td class="yellow-btn">100</td>
                            </tr>
                            <tr id="extreme" class="text-center number">
                                <td class="sideTitle">Extremely Unlikely<br/>100</td>
                                <td class="green-btn">10</td>
                                <td class="green-btn">20</td>
                                <td class="green-btn">30</td>
                                <td class="green-btn">40</td>
                                <td class="green-btn">50</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8">
                            <button type="button" id="riskAssessmentBtn" class="btn btn-success">Add Score</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

</div>

<script>

    $('.green-btn, .yellow-btn, .red-btn, .majordanger-btn').click( function() {
        $(this).parents('table').find('td').each( function( index, element ) {
            $(element).removeClass('on');
        } );
        $(this).addClass('on');
    } );

    $( ".number td:not(.sideTitle)" ).click(function() {
        //alert($(this).text());
        $("#pscoreEdit").val($(this).text());
    });

    $(document).on('click','#riskAssessmentBtn',function(e){
        //$('.modal-backdrop').remove();
        $('#sheq_Modal').remove();
    });

</script>
