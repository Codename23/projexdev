<?php
/**
 * Risk Assessment modal
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */
?>
<div class="modal-dialog risk-assessment-dialog-modal">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Risk Assessment</h4>
        </div>
        <div class="modal-body table-responsive">
            <form class="form-horizontal" name="editRiskAssessmentForm" id="editRiskAssessmentForm" enctype="multipart/form-data" method="post" action="<?php echo BASE_URL . "/index.php?module=risk_assessment&action=editRiskAssessmentData"; ?>">

                <input type="hidden" class="form-control risk-input" name="data_id" id="data_id" value="<?php echo $data['riskAssessmentInfo']['id']; ?>">
                <table width="100%" class="table table-hover">
                    <thead>
                    <tr>
                        <th colspan="2">Risk Assessment</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td width="25%">Safety</td>
                        <td colspan="3">

                        </td>
                    </tr>
                    <tr>
                        <td class="risk-border-right">Hazard</td>
                        <td colspan="3">
                            <div class="col-lg-12 col-md-4 col-sm-8">
                                <input type="text" class="form-control risk-input" name="hazard" id="hazard" value="<?php echo $data['riskAssessmentInfo']['hazard']; ?>">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="risk-border-right">Risk</td>
                        <td colspan="3">
                            <div class="col-lg-12 col-md-4 col-sm-8">
                                <input type="text" class="form-control risk-input" name="risk" id="risk" value="<?php echo $data['riskAssessmentInfo']['risk']; ?>">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="risk-border-right">Score</td>
                        <td colspan="2">

                            <div class="col-lg-12 col-md-4 col-sm-8">
                                <input type="text" class="form-control pscore-input" name="pscore" id="pscoreEdit" value="<?php echo $data['riskAssessmentInfo']['score']; ?>" style="border: 0px;" readonly>
                                <span>
                                    <a data-toggle="sheq_Modal"  href="index.php?action=modalEditAddScore&module=modal_request"  class="btn btn-success">Add Score</a>
                                    <!--
                                        <a href="#risk-assessment-score-modal" data-toggle="modal"   data-target="#risk-assessment-score-modal" ><button type="button" class="btn btn-success add_risk_assessment_score">Add Score</button></a>
                                        -->
                                </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4"><b>Recommendation & Reduction</b></td>
                    </tr>
                    <?php if(count($data['riskAssessmentRecommendations'])): foreach ($data['riskAssessmentRecommendations'] as $recommendation): ?>
                        <tr>
                            <td class="risk-border-right">-<?php echo $recommendation['percentage']; ?>%</td>
                            <td width="25%" class="risk-border-right"><?php echo $recommendation['type_name']; ?></td>
                            <td class="risk-border-right">
                                <!--
                                <div class="col-lg-12 col-md-4 col-sm-8">

                                </div>
                                -->
                                <?php  $type_name = explode(' ',strtolower($recommendation['type_name']));

                                global  $objRiskAssessment;
                                $riskRecoo = $objRiskAssessment->getRecommendationByTypeDataId($data['riskAssessmentInfo']['id'],$recommendation['id'] );

                                ?>
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <div class="after-add-more-<?php echo strtolower($type_name[0]); ?>">
                                    <?php if($riskRecoo): foreach ($riskRecoo as $risk_recommendation) : //print_r($risk_recommendation); ?>

                                        <div class="input-group">
                                            <input type="text" class="form-control risk-input" name="<?php echo $type_name[0]; ?>[<?php echo $risk_recommendation['id'];?>][]" id="<?php echo strtolower($type_name[0]); ?>" value="<?php echo $risk_recommendation['message']; ?>">
                                            <span class="input-group-btn <?php echo $type_name[0]; ?>-glyphicon">
                                                        <button class="btn btn-link" type="button"><span class="glyphicon glyphicon-plus addglyphicon add-more-<?php echo strtolower($type_name[0]); ?>"></span></button>
                                                    </span>
                                        </div>

                                     <?php endforeach; ?>
                                    </div>

                            <?php  else:?>
                                        <div class="after-add-more-<?php echo strtolower($type_name[0]); ?>">
                                            <div class="input-group">
                                                <input type="text" class="form-control risk-input" name="<?php echo $type_name[0]; ?>[]" id="<?php echo strtolower($type_name[0]); ?>" required placeholder="Enter a <?php echo $type_name[0]; ?>">
                                                <span class="input-group-btn <?php echo $type_name[0]; ?>-glyphicon">
                                                        <button class="btn btn-link" type="button"><span class="glyphicon glyphicon-plus addglyphicon add-more-<?php echo strtolower($type_name[0]); ?>"></span></button>
                                                    </span>
                                            </div>
                                        </div>

                                    <?php endif; ?>
                                </div>
                            </td>
                            <td width="7%">
                                <div class="form-group risk-group-input">
                                    <div class="col-lg-12 col-md-4 col-sm-8">
                                        <select class="form-control risk-input" name="P1-4" id="P1-4" required>
                                            <option value="1">P1</option>
                                            <option value="2">P2</option>
                                            <option value="3">P3</option>
                                            <option value="4">P4</option>
                                            <option value="5">P5</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; endif;?>

                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="risk-border-right">Final Score</td>
                        <td colspan="3"><input type="text" id="finalScoreEdit" name="finalScore" readonly value="<?php echo $data['riskAssessmentInfo']['final_score']; ?>" style="border: 0px;" /></td>
                    </tr>
                    <tr>
                        <td class="risk-border-right">Upload/Take Photo</td>
                        <td colspan="3">
                            <div class="col-lg-12 col-md-4 col-sm-8">
                                <input type="file" id="photo" name="photo">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="form-group">
                    <input type="hidden" name="risk_group_id" id="risk_group_id" />
                    <input type="hidden" name="risk_area_id" id="risk_area_id" />
                    <input type="hidden" name="risk_risk_id" id="risk_risk_id" />
                    <div class="col-sm-8">
                        <button type="submit" id="editRiskDataModalsss" class="btn btn-success">Edit Risk Assessment</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">


            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>

<!--End Of Modal -->


<script>

    $(document).on("click", ".add_risk_assessment", function () {
        var group_id = $(this).data('group_id');
        var area_id= $(this).data('area_id');
        var risk_id= $(this).data('risk_id');

        $(".modal-body #risk_group_id").val(group_id);
        $(".modal-body #risk_area_id").val(area_id);
        $(".modal-body #risk_risk_id").val(risk_id);

    });



    $(document).ready(function(){
        var maxField = 10; //Input fields increment limitation

        //buttons
        var eliminationAddButton = $('.add-more-elimination'); //Add button selector
        var substitutionAddButton = $('.add-more-substitution'); //Add button selector
        var engineeringAddButton = $('.add-more-engineering'); //Add button selector
        var administrativeAddButton = $('.add-more-administrative'); //Add button selector
        var ppeAddButton = $('.add-more-ppe'); //Add button selector




        //wrapper
        var eliminationWrapper = $('.after-add-more-elimination'); //Input field wrapper
        var substitutionWrapper = $('.after-add-more-substitution'); //Input field wrapper
        var engineeringWrapper = $('.after-add-more-engineering'); //Input field wrapper
        var administrativeWrapper = $('.after-add-more-administrative'); //Input field wrapper
        var ppeWrapper = $('.after-add-more-ppe');  //Input field wrapper




        //fields
        var fieldEliminationHTML = '<div class="input-group"><input type="text" class="form-control risk-input" name="elimination[]" id="seliminations" required placeholder="Enter a recommendation"><span class="input-group-btn recommendation-glyphicon"> <button class="btn btn-link remove_elimination" type="button"><span class="glyphicon glyphicon-minus minusglyphicon"></span></button> </span> </div>';

        var fieldSubstitutionHTML = '<div class="input-group"><input type="text" class="form-control risk-input" name="substitution[]" id="ssubstitutions" required placeholder="Enter a substitution"> <span class="input-group-btn recommendation-glyphicon"> <button class="btn btn-link remove_substitution" type="button"><span class="glyphicon glyphicon-minus minusglyphicon"></span></button> </span> </div>';


        var fieldEngineeringHTML = '<div class="input-group"> <input type="text" class="form-control risk-input" name="engineering[]" id="sengineerings" required placeholder="Enter a engeneering"> <span class="input-group-btn recommendation-glyphicon"> <button class="btn btn-link remove_engineering" type="button"><span class="glyphicon glyphicon-minus minusglyphicon"></span></button> </span> </div>';

        var fieldAdministrativeHTML = '<div class="input-group"> <input type="text" class="form-control risk-input" name="administrative[]" id="sadministratives" required placeholder="Enter a administrative"> <span class="input-group-btn recommendation-glyphicon"> <button class="btn btn-link remove_administrative" type="button"><span class="glyphicon glyphicon-minus minusglyphicon"></span></button> </span> </div>';


        var fieldPpeHTML = ' <div class="input-group"> <input type="text" class="form-control risk-input" name="ppe[]" id="sppes" required placeholder="Enter a ppe"> <span class="input-group-btn recommendation-glyphicon"> <button class="btn btn-link remove_ppe" type="button"><span class="glyphicon glyphicon-minus minusglyphicon"></span></button> </span> </div> ';

        //var fieldHTML = '<div><input type="text" name="elimination[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="remove-icon.png"/></a></div>'; //New input field html
        var eX = 1; //Initial field counter is 1
        $(eliminationAddButton).click(function(){ //Once add button is clicked
            if(eX < maxField){ //Check maximum number of input fields
                eX++; //Increment field counter
                $(eliminationWrapper).append(fieldEliminationHTML); // Add field html
            }
        });
        $(eliminationWrapper).on('click', '.remove_elimination', function(e){ //Once remove button is clicked

            // e.preventDefault();
            $(this).parents('div .input-group').remove(); //Remove field html
            eX--; //Decrement field counter
        });

        //Substitution
        var sX = 1; //Initial field counter is 1
        $(substitutionAddButton).click(function(){ //Once add button is clicked
            if(sX < maxField){ //Check maximum number of input fields
                sX++; //Increment field counter
                $(substitutionWrapper).append(fieldSubstitutionHTML); // Add field html
            }
        });

        $(substitutionWrapper).on('click', '.remove_substitution', function(e){ //Once remove button is clicked

            // e.preventDefault();
            $(this).parents('div .input-group').remove(); //Remove field html
            sX--; //Decrement field counter
        });

        //Engineering
        var enX = 1; //Initial field counter is 1
        $(engineeringAddButton).click(function(){ //Once add button is clicked
            if(enX < maxField){ //Check maximum number of input fields
                enX++; //Increment field counter
                $(engineeringWrapper).append(fieldEngineeringHTML); // Add field html
            }
        });

        $(engineeringWrapper).on('click', '.remove_engineering', function(e){ //Once remove button is clicked

            // e.preventDefault();
            $(this).parents('div .input-group').remove(); //Remove field html
            enX--; //Decrement field counter
        });

        //Administrative
        var adX = 1; //Initial field counter is 1
        $(administrativeAddButton).click(function(){ //Once add button is clicked
            if(adX < maxField){ //Check maximum number of input fields
                adX++; //Increment field counter
                $(administrativeWrapper).append(fieldAdministrativeHTML); // Add field html
            }
        });

        $(administrativeWrapper).on('click', '.remove_administrative', function(e){ //Once remove button is clicked

            // e.preventDefault();
            $(this).parents('div .input-group').remove(); //Remove field html
            adX--; //Decrement field counter
        });

        //PPE
        var pX = 1; //Initial field counter is 1
        $(ppeAddButton).click(function(){ //Once add button is clicked
            if(pX < maxField){ //Check maximum number of input fields
                pX++; //Increment field counter
                $(ppeWrapper).append(fieldPpeHTML); // Add field html
            }
        });

        $(ppeWrapper).on('click', '.remove_ppe', function(e){ //Once remove button is clicked

            // e.preventDefault();
            $(this).parents('div .input-group').remove(); //Remove field html
            pX--; //Decrement field counter
        });

    });


    $(document).ready(function() {
        var elimination_score = 0;
        var substitution_score = 0;
        var engineering_score = 0;
        var administrative_score = 0;
        var ppe_score = 0;
        var final_score = 0;
        //$("body").on("keyup", "#elimination", function () {

        $(document).on('keyup', '#elimination', function () {

           var elimination_value = $(this).val();
            if (elimination_value.length > 0) {
                elimination_score = 100;
            } else {
                elimination_score = 0;

            }
            final_score = $("#pscoreEdit").val()  * ((100-elimination_score) / 100);
            $("#finalScoreEdit").val(final_score);


        });

        //$("body").on("keyup", "#substitution", function () {
        $(document).on('keyup', '#substitution', function () {
            var substitution_value = $(this).val();
            if (substitution_value.length > 0) {
                substitution_score = 80;
            } else {
                substitution_score = 0;

            }

            if (elimination_score == 100) {

                final_score = $("#pscoreEdit").val()  * ((100-elimination_score) / 100);
                $("#finalScoreEdit").val(final_score);

            } else if(substitution_score == 80 && elimination_score == 0) {

                final_score = $("#pscoreEdit").val()  * ((100-substitution_score) / 100);
                $("#finalScoreEdit").val(final_score);

            }

        });

        //$("body").on("blur", "#engineering", function () {
        $(document).on('keyup', '#engineering', function () {
            var engineering_value = $(this).val();
            if (engineering_value.length > 0) {
                engineering_score = 50;
            } else {
                engineering_score = 0;

            }

            if (elimination_score == 100) {

                final_score = $("#pscoreEdit").val()  * ((100-elimination_score) / 100);
                $("#finalScoreEdit").val(final_score);

            } else if (substitution_score == 80 && elimination_score == 0) {

                final_score = $("#pscoreEdit").val()  * ((100-substitution_score) / 100);
                $("#finalScoreEdit").val(final_score);

            } else if (engineering_score == 50 && substitution_score == 0 && elimination_score == 0) {

                final_score = $("#pscoreEdit").val()  * ((100-engineering_score) / 100);
                $("#finalScoreEdit").val(final_score);

            }

        });

        //$("body").on("keyup", "#administrative", function () {
        $(document).on('keyup', '#administrative', function () {
            var administrative_value = $(this).val();
            if (administrative_value.length > 0) {
                administrative_score = 25;
            } else {
                administrative_score = 0;

            }
            if (elimination_score == 100) {

                final_score = $("#pscoreEdit").val()  * ((100-elimination_score) / 100);
                $("#finalScoreEdit").val(final_score);

            } else if (substitution_score == 80 && elimination_score == 0) {

                final_score = $("#pscoreEdit").val()  * ((100-substitution_score) / 100);
                $("#finalScoreEdit").val(final_score);

            } else if (engineering_score == 50 && substitution_score == 0 && elimination_score == 0) {

                final_score = $("#pscoreEdit").val()  * ((100-engineering_score) / 100);
                $("#finalScoreEdit").val(final_score);

            } else if (administrative_score == 25 && engineering_score == 0 && substitution_score == 0 && elimination_score == 0) {

                final_score = $("#pscoreEdit").val()  * ((100-administrative_score) / 100);
                $("#finalScoreEdit").val(final_score);

            }

        });

        //$("body").on("keyup", "#ppe", function () {
        // $(document).on('keyup', '#ppe', function () {
          $(document).on('keyup', '#ppe', function () {
            var ppe_value = $(this).val();
            if (ppe_value.length > 0) {
                ppe_score = 15;
            } else {
                ppe_score = 0;

            }
            if (elimination_score == 100) {

                final_score = $("#pscoreEdit").val()  * ((100-elimination_score) / 100);
                $("#finalScoreEdit").val(final_score);

            } else if (substitution_score == 80 && elimination_score == 0) {

                final_score = $("#pscoreEdit").val()  * ((100-substitution_score) / 100);
                $("#finalScoreEdit").val(final_score);

            } else if (engineering_score == 50 && substitution_score == 0 && elimination_score == 0) {

                final_score = $("#pscoreEdit").val()  * ((100-engineering_score) / 100);
                $("#finalScoreEdit").val(final_score);

            } else if (administrative_score == 25 && engineering_score == 0 && substitution_score == 0 && elimination_score == 0) {

                final_score = $("#pscoreEdit").val()  * ((100-administrative_score) / 100);
                $("#finalScoreEdit").val(final_score);

            } else if (ppe_score == 15 && administrative_score == 0 && engineering_score == 0 && substitution_score == 0 && elimination_score == 0) {

                final_score = $("#pscoreEdit").val()  * ((100-ppe_score) / 100);
                $("#finalScoreEdit").val(final_score);
            }
        });


    });

    $(document).on('click','[data-toggle="sheq_Modal"]',function(e){
        $('#sheq_Modal').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),
            $modal=$('<div class="modal" id="sheq_Modal"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: false, keyboard: false});
        $modal.load($remote);
    });

</script>
