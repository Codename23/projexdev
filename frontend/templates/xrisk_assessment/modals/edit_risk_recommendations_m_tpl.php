<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017/02/14
 * Time: 1:40 PM
 */
?>
<div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Recommendations</h4>
            </div>
            <div class="modal-body table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                    <tr>
                        <th>R No</th>
                        <th>Recommendation Type</th>
                        <th>Recommendations</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                   <?php if($data['riskRecommendations']): foreach ($data['riskRecommendations'] as $recommendation): ?>
                    <tr>
                        <td>R<?php echo $recommendation['rec_id']; ?></td>
                        <td><?php echo $recommendation['type_name']; ?></td>
                        <td>
                            <p><?php echo $recommendation['message']; ?></p>
                        </td>
                        <td>
                            <a data-toggle="sheq_Modal"  href="index.php?action=modalUpdateRiskRecommendations&module=modal_request&id=<?php echo $recommendation['rec_id']; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                        </td>
                        <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>

                   <?php endforeach;endif; ?>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>

<script>

    $(document).on('click','[data-toggle="sheq_Modal"]',function(e){

        $('#sheq_Modal').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),
            $modal=$('<div class="modal" id="sheq_Modal"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: false, keyboard: false});
        $modal.load($remote);
    });

</script>


