<?php
/**
 * Employee modal
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */
?>
<?php global  $objLanguage; ?>
<!-- Add New Internal Person Modal-->
<div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content largeModal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Employee</h4>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" class="form-horizontal" id="frmAddEmployee" method="post" action="<?php echo BASE_URL; ?>/index.php?module=employee&action=addEmployee">

                                <?php foreach ($data['allEmployeeFields'] as $fields): ?>

                                    <?php if($fields['field_name'] == 'branch_name' ){?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="branchName"><?php  echo $objLanguage->languageText(strtoupper($fields['field_name'])); ?></label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control" name="<?php echo $fields['field_name']; ?>" id="branchName">
                                                    <option value="">None</option>
                                                    <?php foreach ($data['companyBranches'] as $branch): ?>
                                                        <?php  if( isset($_SESSION['branch_id']) && $_SESSION['branch_id'] == $branch['id']): ?>
                                                            <option value="<?php echo $branch['id']; ?>" selected><?php echo $branch['branch_name']; ?></option>
                                                        <?php else: ?>
                                                            <option value="<?php echo $branch['id']; ?>"><?php echo $branch['branch_name']; ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>


                                    <?php }elseif($fields['field_name'] == 'department_name' ){?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="departmentName"><?php  echo $objLanguage->languageText(strtoupper($fields['field_name'])); ?></label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control departmentAddOccupation" name="<?php echo $fields['field_name']; ?>" id="departmentName">
                                                    <option value="">None</option>
                                                    <?php foreach ($data['branchDepartments'] as $departments): ?>
                                                        <option value="<?php echo $departments['id']; ?>"><?php echo $departments['department_name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                    <?php }elseif($fields['field_name'] == 'occupation' ){?>
                                        <div class="form-group occupation_options">
                                            <label class="control-label col-sm-4" for="occupationName"><?php  echo $objLanguage->languageText(strtoupper($fields['field_name'])); ?></label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <div class="input-group">
                                             <span class="input-group-addon" >
                                                        <a title="Add Occupation"  class="info modalAddOccupation" style="display: none"
                                                           href="#occupation-modal" data-toggle="modal" data-target="#occupation-modal" ><span class="glyphicon glyphicon-plus"></span></a>
                                             </span>
                                                    <select class="form-control" name="<?php echo $fields['field_name']; ?>" id="occupationName">
                                                        <option value="">None-<?php echo $fields['field_name']; ?></option>


                                                    </select>
                                                </div>
                                            </div>
                                        </div>


                                    <?php }elseif($fields['field_name'] == 'employee_blood_type' ){?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="bloodtype"><?php  echo $objLanguage->languageText(strtoupper($fields['field_name'])); ?></label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control" name="<?php echo $fields['field_name']; ?>" id="<?php echo $fields['field_name']; ?>">
                                                    <option value="1">O</option>
                                                    <option value="2">A</option>
                                                    <option value="3">B</option>
                                                    <option value="4">AB</option>
                                                </select>
                                            </div>
                                        </div>
                                    <?php }elseif($fields['field_name'] == 'employee_allergies' ){?>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="allergies"><?php echo $objLanguage->languageText(strtoupper($fields['field_name'])); ?></label>
                                            <div class="col-lg-6 col-md-6 col-sm-8">
                                                <textarea class="form-control tinyMCEselector" name="<?php echo $fields['field_name']; ?>" id="<?php echo $fields['field_name']; ?>"></textarea>
                                            </div>
                                        </div>

                                    <?php }elseif($fields['field_name'] == 'employee_race'){?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="<?php echo $fields['field_name']; ?>"><?php  echo $objLanguage->languageText(strtoupper($fields['field_name'])); ?></label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control" name="<?php echo $fields['field_name']; ?>" id="<?php echo $fields['field_name']; ?>">
                                                    <?php if($data['gender']): foreach ($data['ethnic'] as $ethnic):?>
                                                        <option value="<?php echo $ethnic['id']; ?>" ><?php echo $ethnic['name']; ?></option>
                                                    <?php endforeach;endif;?>
                                                </select>
                                            </div>
                                        </div>

                                    <?php }elseif($fields['field_name'] == 'employee_gender'){?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="<?php echo $fields['field_name']; ?>"><?php  echo $objLanguage->languageText(strtoupper($fields['field_name'])); ?></label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control" name="<?php echo $fields['field_name']; ?>" id="<?php echo $fields['field_name']; ?>">
                                                    <?php if($data['gender']): foreach ($data['gender'] as $gender):?>
                                                        <option value="<?php echo $gender['id']; ?>" ><?php echo $gender['name']; ?></option>
                                                    <?php endforeach;endif;?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php }elseif($fields['field_name'] == 'employee_marital_status'){?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="maritalStatus"><?php  echo $objLanguage->languageText(strtoupper($fields['field_name'])); ?></label>
                                            <div class="col-lg-6 col-md-6 col-sm-8">
                                                <label class="radio-inline"><input type="radio" name="<?php echo $fields['field_name']; ?>" value="1">Married</label>
                                                <label class="radio-inline"><input type="radio" name="<?php echo $fields['field_name']; ?>" value="2">Single</label>
                                            </div>
                                        </div>
                                    <?php }elseif($fields['field_name'] == 'employee_language'){?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="homeLanguage"><?php  echo $objLanguage->languageText(strtoupper($fields['field_name'])); ?></label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control bfh-languages" name="<?php echo $fields['field_name']; ?>" data-language="en"></select>
                                            </div>
                                        </div>

                                    <?php }elseif($fields['field_name'] == 'second_language'){?>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="secondLanguage"><?php  echo $objLanguage->languageText(strtoupper($fields['field_name'])); ?></label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <select class="form-control bfh-languages" name="<?php echo $fields['field_name']; ?>" data-language="en"></select>
                                            </div>
                                        </div>

                                    <?php }else{?>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="<?php echo $fields['field_name']; ?>"><?php  echo $objLanguage->languageText(strtoupper($fields['field_name'])); ?></label>
                                            <div class="col-lg-4 col-md-4 col-sm-8">
                                                <input type="text" class="form-control" name="<?php echo $fields['field_name']; ?>" id="<?php echo $fields['field_name']; ?>" required placeholder="Enter <?php echo $objLanguage->languageText(strtolower($fields['field_name'])); ?>">
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php endforeach; ?>

                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="profileimage">Upload photo</label>
                                    <div class="col-lg-4 col-md-4 col-sm-8">
                                        <input type="file" id="profileimage" name="profileimage">
                                    </div>
                                </div>


                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
</div>
<?php

include_once('frontend/templates/footers/branch_tpl.php');
?>

<script>
    $(document).on("click", ".occupation-modal", function () {

        //var departmentId = $('.departmentAddOccupation option:selected').val();//$('#departmentName').val();
        //alert('department'+departmentId);
        $(".modal-body #occupationDepartmentName").val(departmentId);
    });

    $('#btnAddOccupation').click(function (e) {

        var departmentId = $('.departmentAddOccupation option:selected').val();
        var occupationName = $('.modal-body #occupationName').val();
        $.post('<?php echo BASE_URL . "/index.php?module=occupation&action=addOccupation"; ?>', {occupationName:occupationName,departmentName:departmentId}, function(data){

            $('.occupation-modal').modal('hide');
            location.reload();
            //$('#frmAddEmployee').hide().slideDown('slow');
        });


    })


</script>
