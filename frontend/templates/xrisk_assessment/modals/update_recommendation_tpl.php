<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017/02/14
 * Time: 1:40 PM
 */
?>
<div class="modal-dialog singleRecommendationModal">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Recommendations</h4>
        </div>
        <div class="modal-body table-responsive">
            <form class="form-horizontal" name="singleRecommendationEdit" id="singleRecommendationEditForm" method="post" action="">

                <div class="form-group">
                    <label class="control-label col-sm-4" for="contactPerson">Recommendation</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" id="recommendation" name="recommendation" value="<?php echo $data['recommendationInfo']['message']; ?>" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <input type="hidden" class="form-control" name="recommendationId" id="recommendationId" value="<?php echo $data['recommendationInfo']['id']; ?>" >
                        <button type="button" id="singleRecommendationEdit" class="btn btn-success">Update Recommendation</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>
<script>

    $('#singleRecommendationEdit').click(function() {

        $.post('<?php echo BASE_URL . "/index.php?module=risk_assessment&action=updateSingleRecommendation"; ?>',
            {'recommendation': $('#recommendation').val(),
                'recommendationId':$('#recommendationId').val()})
            .done(function(data){

                $("#singleRecommendationEditForm").get(0).reset();
                $("#sheq_Modal").modal('hide');
                $('.modal-backdrop').remove();
                var response = JSON.parse(data);
                if (response.type == 'success') {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                    toastr.success(response.message, "");
                }

                if (response.type == 'error') {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                    toastr.error(response.message, "");
                }
            });
    });

</script>