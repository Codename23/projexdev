<?php
/**
 * Employee modal
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */
?>
<!-- Add New External Person Modal -->
<div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Service Provider</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" name="addExternalPersonForm" action="risk_assessment_startra.php.php">
                    <?php  include_once(TEMPLATES.'/modal_forms/service_provider_modal_form.php'); ?>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" class="btn btn-success">Add Service Provider</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
</div>
<!--End Of Modal -->
