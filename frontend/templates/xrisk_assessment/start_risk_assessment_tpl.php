<?php
/*
 * Header file
 */
$title = 'Start Risk Assessment';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */
    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <?php
    /*
     * Include side menu from the include file
     */
    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->
    <div class="col-lg-10">
        <!--sub menu-->
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-justified topbar-menu">
                    <li><a href="index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Dashboard</a></li>
                    <li  class="active"><a href="index.php?action=startRiskAssessment&module=risk_assessment">Start R/A</a></li>
                    <li><a href="risk_assessment_schedulera.php">Schedule R/A</a></li>
                    <li><a href="risk_assessment_openra.php">Open R/A</a></li>
                    <li><a href="risk_assessment_recommendations.php">Recommendations</a></li>
                    <li><a href="risk_assessment_rahistory.php">R/A History</a></li>
                    <li><a href="">Risk Register</a></li>
                </ul>
            </div>
        </div>
        <!--End of sub menu-->
        <div class="panel-group">
            <form class="form-horizontal" name="riskAssessmentStartRaForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=risk_assessment&action=startRiskHeader">
                <div class="panel panel-default">
                    <div class="panel-heading">Start R/A</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="riskAssessmentType">Risk Assessment Type</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select name="riskAssessmentType" class="form-control" required>
                                    <option value="" selected disabled>Please select a risk assessment type</option>
                                    <?php if($data['riskAssessmentTypes']) : foreach ($data['riskAssessmentTypes'] as $riskType) : ?>
                                        <option value="<?php echo $riskType['id']; ?>"><?php echo $riskType['type_name']; ?></option>
                                    <?php endforeach;endif; ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-sm-4">Risk Assessor</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <div class="after-add-more-assistance">
                                    <input type="text" class="form-control" name="nameSurname" id="nameSurname" value="<?php echo $_SESSION['firstname']." ". $_SESSION['username']; ?>" readonly >
                                </div>
                                <br />
                                <!--
                                <div>
                                    <br />
                                    <a data-toggle="sheqModal"  href="index.php?action=modalAddEmployee&module=modal_request"><button type="button" id="addInternalPerson" class="btn btn-success">Add Internal Person</button></a>
                                    <a data-toggle="sheqModal"  href="index.php?action=modalAddServiceProvider&module=modal_request"><button type="button" id="addExternalPerson" class="btn btn-success">Add External Person</button></a>

                                </div>
                                <br/>
                                -->
                                <div>
                                    <button class="btn btn-success add-more-assistance" type="button"><i class="glyphicon glyphicon-plus"></i> Add Assistance</button>
                                </div>
                            </div>
                        </div>
                        <br/>

                        <div class="copy-assistance hide">
                            <div class="control-group" style="margin-top:30px">

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" name="assistanceNameSurname" id="assistanceNameSurname" placeholder="Search for a name and surname" onkeyup="search_func(this.value);">
                                    </div>
                                </div>
                                <div class="form-group" id="assistanceResults">
                                </div>
                                <div>
                                    <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <input type="hidden" class="form-control" name="assistanceUserId" id="assistanceUserId">
                                <button type="submit" class="btn btn-success">Start Risk Assessment</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End of panel panel-default-->
            </form>
        </div>


        <!--End of the panel panel-default-->
    </div>
</div><!--End of container-fluid-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>

<script>
    $(".add-more-assistance").click(function(){
        var html = $(".copy-assistance").html();
        $(".after-add-more-assistance").after(html);
    });

    /**
     * Search employee
     */
    function search_func(){
        $("#assistanceNameSurname").keyup(function(){
            if(this.value.length > 2 ){
                var assistancePerson = $(this).val();
                $.ajax({
                    type: "POST",
                    url : "<?php echo BASE_URL . "/index.php?module=ajax_request&action=ajaxSearchForEmployee"; ?>",
                    data :"employeeData="+assistancePerson,
                    success: function (data){
                        $("#assistanceResults").html(data);
                    }
                });
            }
        });
    }

    $('body').on('click', '.addAssistanceID', function(){
        var assistanceName = $(".assistanceName").text();
        $('#assistanceNameSurname').val(assistanceName);

        var witnessID = $(".addAssistanceID").val();
        $('#assistanceUserId').val(witnessID);
        $("#assistanceResults").hide();
    });

    $(document).on('click','[data-toggle="sheqModal"]',function(e){

        $('#sheqModal').remove();
        $('.modal-backdrop').remove();
        e.preventDefault();
        var $this=$(this),
        $remote=$this.data('remote')||$this.attr('href'),

        $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    });
</script>
