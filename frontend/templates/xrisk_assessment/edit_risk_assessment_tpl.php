<?php
/*
 * Header file
 */
$title = 'Add Risk Assessment';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<!--Risk Assessment Modal -->
<div class="modal fade" id="risk-assessment-modal" role="dialog">
    <div class="modal-dialog risk-assessment-dialog-modal">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Risk Assessment</h4>
            </div>
            <div class="modal-body table-responsive">
                <form class="form-horizontal" name="riskAssessmentForm" id="riskAssessmentForm" enctype="multipart/form-data" method="post" action="<?php echo BASE_URL."/index.php?module=risk_assessment&action=addRiskAssessmentData"; ?>">

                    <table width="100%" class="table table-hover">
                        <thead>
                        <tr>
                            <th colspan="2">Risk Assessment</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td width="25%"><p id="risk_area_name"></p></td>
                            <td colspan="3">

                            </td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Hazard</td>
                            <td colspan="3">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control risk-input" name="hazard" id="hazard" required placeholder="Enter a hazard">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Risk</td>
                            <td colspan="3">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control risk-input" name="risk" id="risk" required placeholder="Enter a risk">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Score</td>
                            <td colspan="2">

                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="text" class="form-control pscore-input" name="pscore" id="pscore" style="border: 0px;" readonly>
                                    <span>
                                        <a href="#risk-assessment-score-modal" data-toggle="modal" data-target="#risk-assessment-score-modal"><button type="button" class="btn btn-success">Add Score</button></a>
                                            </span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>

                        <tr>
                            <td colspan="4"><b>Recommendation & Reduction</b></td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">-100%</td>
                            <td width="25%" class="risk-border-right">Elimination</td>
                            <td class="risk-border-right">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <div class="after-add-more-elimination">
                                        <div class="input-group">
                                            <input type="text" class="form-control risk-input" name="elimination[]" id="elimination"  placeholder="Enter a recommendation">
                                            <span class="input-group-btn recommendation-glyphicon">
                                                <button class="btn btn-link" type="button"><span class="glyphicon glyphicon-plus addglyphicon add-more-elimination"></span></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td width="7%">
                                <div class="form-group risk-group-input">
                                    <div class="col-lg-12 col-md-4 col-sm-8">
                                        <select class="form-control risk-input" name="P1-4" id="P1-4" required>
                                            <option value="1">P1</option>
                                            <option value="2">P2</option>
                                            <option value="3">P3</option>
                                            <option value="4">P4</option>
                                            <option value="5">P5</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="risk-border-right">-80%</td>
                            <td class="risk-border-right">Substitution</td>
                            <td class="risk-border-right">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <div class="after-add-more-substitution">
                                        <div class="input-group">
                                            <input type="text" class="form-control risk-input" name="substitution[]" id="substitution"  placeholder="Enter a recommendation">
                                            <span class="input-group-btn recommendation-glyphicon">
                                                    <button class="btn btn-link" type="button"><span class="glyphicon glyphicon-plus addglyphicon add-more-substitution"></span></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group risk-group-input">
                                    <div class="col-lg-12 col-md-4 col-sm-8">
                                        <select class="form-control risk-input" name="P1-4" id="P1-4" required>
                                            <option value="1">P1</option>
                                            <option value="2">P2</option>
                                            <option value="3">P3</option>
                                            <option value="4">P4</option>
                                            <option value="5">P5</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="risk-border-right">-50%</td>
                            <td class="risk-border-right">Engineering Control</td>
                            <td class="risk-border-right">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <div class="after-add-more-engineering">
                                        <div class="input-group">
                                            <input type="text" class="form-control risk-input" name="engineering[]" id="engineering"  placeholder="Enter a recommendation">
                                            <span class="input-group-btn recommendation-glyphicon">
                                <button class="btn btn-link" type="button"><span class="glyphicon glyphicon-plus addglyphicon add-more-engineering"></span></button>
                            </span>
                                        </div>
                                    </div>
                                </div>

                            </td>
                            <td>
                                <div class="form-group risk-group-input">
                                    <div class="col-lg-12 col-md-4 col-sm-8">
                                        <select class="form-control risk-input" name="P1-4" id="P1-4" required>
                                            <option value="1">P1</option>
                                            <option value="2">P2</option>
                                            <option value="3">P3</option>
                                            <option value="4">P4</option>
                                            <option value="5">P5</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="risk-border-right">-25%</td>
                            <td class="risk-border-right">Administrative Controls</td>
                            <td class="risk-border-right">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <div class="after-add-more-administrative">
                                        <div class="input-group">
                                            <input type="text" class="form-control risk-input" name="administrative[]" id="administrative"  placeholder="Enter a recommendation">
                                            <span class="input-group-btn recommendation-glyphicon">
                                                        <button class="btn btn-link" type="button"><span class="glyphicon glyphicon-plus addglyphicon add-more-administrative"></span></button>
                                                    </span>
                                        </div>
                                    </div>
                                </div>

                            </td>
                            <td>
                                <div class="form-group risk-group-input">
                                    <div class="col-lg-12 col-md-4 col-sm-8">
                                        <select class="form-control risk-input" name="P1-4" id="P1-4" required>
                                            <option value="1">P1</option>
                                            <option value="2">P2</option>
                                            <option value="3">P3</option>
                                            <option value="4">P4</option>
                                            <option value="5">P5</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="risk-border-right">-15%</td>
                            <td class="risk-border-right">PPE</td>
                            <td class="risk-border-right">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <div class="after-add-more-ppe">
                                        <div class="input-group">
                                            <input type="text" class="form-control risk-input" name="ppe[]" id="ppe"  placeholder="Enter a recommendation">
                                            <span class="input-group-btn recommendation-glyphicon">
                                <button class="btn btn-link" type="button"><span class="glyphicon glyphicon-plus addglyphicon add-more-ppe"></span></button>
                            </span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group risk-group-input">
                                    <div class="col-lg-12 col-md-4 col-sm-8">
                                        <select class="form-control risk-input" name="P1-4" id="P1-4" required>
                                            <option value="1">P1</option>
                                            <option value="2">P2</option>
                                            <option value="3">P3</option>
                                            <option value="4">P4</option>
                                            <option value="5">P5</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Final Score</td>
                            <td colspan="3"><input type="text" id="finalScore" name="finalScore" readonly /></td>
                        </tr>
                        <tr>
                            <td class="risk-border-right">Upload/Take Photo</td>
                            <td colspan="3">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                    <input type="file" id="photo" name="photo">
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="form-group">
                        <input type="hidden" name="risk_group_id" id="risk_group_id" />
                        <input type="hidden" name="risk_area_id" id="risk_area_id" />
                        <input type="hidden" name="risk_risk_id" id="risk_risk_id" />
                        <div class="col-sm-8">
                            <button type="submit" id="addRiskDataModalssss" class="btn btn-success">Add Risk Assessment</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">


                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!--End Of Modal -->

<!-- Modal -->
<div class="modal fade" id="risk-assessment-score-modal" role="dialog">
    <div class="modal-dialog risk-assessment-score-dialog-modal">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Risk Assessment Score</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" name="riskAssessmentScoreForm" >
                    <div class="table-responsive">
                        <table width="100%" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th class="text-center" colspan="6">Severity of the potential injury/damage</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="text-center">
                                <td></td>
                                <td width="16%"><p>Insignificant damage to Property, Equipment or Minor Injury</p></td>
                                <td width="16%"><p>Non-Reportable Injury, minor loss of Process or slight damage to Property</p></td>
                                <td width="16%"><p>Reportable Injury moderate loss of Process or limited damage to Property</p></td>
                                <td width="16%"><p>Major Injury, Single Fatality critical loss of Process/damage to Property</p></td>
                                <td width="16%"><p>Multiple Fatalities Castastrophic Loss of Business</p></td>
                            </tr>
                            <tr class="text-center">
                                <td><b>Likelihood of the hazard happening</b></td>
                                <td>1</td>
                                <td>2</td>
                                <td>3</td>
                                <td>4</td>
                                <td>5</td>
                            </tr>

                            <tr class="text-center number">
                                <td class="sideTitle">Almost Certain<br/>50</td>
                                <td class="green-btn">50</td>
                                <td class="yellow-btn">100</td>
                                <td class="red-btn">150</td>
                                <td class="majordanger-btn">200</td>
                                <td class="majordanger-btn">250</td>
                            </tr>
                            <tr class="text-center number">
                                <td class="sideTitle">Will probably occur<br/>40</td>
                                <td class="green-btn">40</td>
                                <td class="yellow-btn">80</td>
                                <td class="red-btn">120</td>
                                <td class="majordanger-btn">160</td>
                                <td class="majordanger-btn">250</td>
                            </tr>
                            <tr class="text-center number">
                                <td class="sideTitle">Possible occur<br/>30</td>
                                <td class="green-btn">30</td>
                                <td class="yellow-btn">60</td>
                                <td class="yellow-btn">90</td>
                                <td class="red-btn">120</td>
                                <td class="red-btn">150</td>
                            </tr>
                            <tr class="text-center number">
                                <td class="sideTitle">Remote possibility<br/>20</td>
                                <td class="green-btn">20</td>
                                <td class="green-btn">40</td>
                                <td class="yellow-btn">60</td>
                                <td class="yellow-btn">80</td>
                                <td class="yellow-btn">100</td>
                            </tr>
                            <tr id="extreme" class="text-center number">
                                <td class="sideTitle">Extremely Unlikely<br/>100</td>
                                <td class="green-btn">10</td>
                                <td class="green-btn">20</td>
                                <td class="green-btn">30</td>
                                <td class="green-btn">40</td>
                                <td class="green-btn">50</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8">
                            <button type="button" id="riskAssessmentBtn" class="btn btn-success">Add Score</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!--End Of Modal -->

<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */
    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <?php
    /*
     * Include side menu from the include file
     */
    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->
    <div class="col-lg-10">
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-justified topbar-menu">
                    <li><a href="index.php?module=risk_assessment&action=viewRiskAssessmentDashboard">Dashboard</a></li>
                    <li><a href="index.php?action=startRiskAssessment&module=risk_assessment">Start R/A</a></li>
                    <li class="active"><a href="risk_assessment_schedulera.php">Schedule R/A</a></li>
                    <li><a href="risk_assessment_openra.php">Open R/A</a></li>
                    <li><a href="risk_assessment_recommendations.php">Recommendations</a></li>
                    <li><a href="risk_assessment_rahistory.php">R/A History</a></li>
                    <li><a href="">Risk Register</a></li>
                </ul>
            </div>
        </div>

        <form class="form-horizontal" name="assessmentFormDetails" method="post" action="">
            <div class="panel panel-default">
                <div class="panel-heading">Risk Assessment Details</div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                            <tr>
                                <td width="30%"><strong>Risk Assessment No</strong>:</td>
                                <td><?php echo $_SESSION['risk_assessment_no']; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Assessed Date</strong></td>
                                <td><?php echo $data['riskAssessmentInfo']['risk_assessment_date']; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Risk Assessor</strong></td>
                                <td><?php echo $data['riskAssessmentInfo']['firstname']." ".$data['riskAssessmentInfo']['lastname']; ?></strong></td>
                            </tr>
                            <tr>
                                <td><strong>Assistant</strong></td>
                                <td><?php echo ($data['riskAssistanceInfo']) ? $data['riskAssistanceInfo']['firstname']." ".$data['riskAssistanceInfo']['lastname'] : '---'; ?></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <?php $assessedItemsInfo  = $data['assessedItemsInfo']; ?>
            <?php include_once(TEMPLATES.'/risk_assessment/assessed_list_template/'.strtolower($data['riskAssessmentHeaderInfo']['group_name']).'_list_tpl.php'); ?>

                <div class="accordion_data">
                    <div class="panel-group" id="accordion">

                        <?php if($data['riskAssessmentGroups']): foreach ($data['riskAssessmentGroups'] as $key=>$value): ?>

                            <?php //echo  $key." -- ".$value['id'] ."<br />"; ?>
                                <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $value['id']; ?>"><?php echo $value['group_name']; ?></a>
                                    </h4>
                                </div>

                                <div id="collapse_<?php echo $value['id']; ?>" data="<?php echo $value['id']; ?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="area_data"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;endif;?>
                    </div>
                </div>



        </form>

        <!--End of panel panel-default-->

    </div>
</div><!--End of row-->
</div><!--End of container-fluid-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>
<?php include_once('frontend/templates/footers/risk_ajax_tpl.php'); ?>



<script>


    $(".panel-collapse:first").addClass('in');
    $.post('<?php echo BASE_URL . "/index.php?module=risk_assessment&action=viewGroupData"; ?>', {'groupId': $(".panel-collapse:first").attr("id")}, function(data){
        $('.area_data').html(data);
    });

    $(function() {
        $("#date").datepicker({
            dateFormat:"yy/mm/dd",
            maxDate: new Date(),
            minDate: new Date()});
        $("#date").datepicker("setDate", new Date());
    });


    $('.accordion_data #accordion').on('show.bs.collapse', function (e) {

        var group_id  = e.target.id;

        $.post('<?php echo BASE_URL . "/index.php?module=risk_assessment&action=viewGroupData"; ?>', {'groupId':group_id}, function(data){
            $('.area_data').html(data);
        });

    });


    $("#riskAssessmentBtn").click(function(){
        $("#risk-assessment-score-modal").modal('toggle');
    });

    $('.green-btn, .yellow-btn, .red-btn, .majordanger-btn').click( function() {
        $(this).parents('table').find('td').each( function( index, element ) {
            $(element).removeClass('on');
        } );
        $(this).addClass('on');
    } );

    $( ".number td:not(.sideTitle)" ).click(function() {
        $("#pscore").val($(this).text());
    });


    $(document).on("click", ".add_risk_assessment", function () {


        var group_id = $(this).data('group_id');
        var area_id= $(this).data('area_id');
        var risk_id= $(this).data('risk_id');
        var area_name = $(this).data('area_name');

        $(".modal-body #risk_group_id").val(group_id);
        $(".modal-body #risk_area_id").val(area_id);
        $(".modal-body #risk_risk_id").val(risk_id);
        $(".modal-body #risk_area_name").text(area_name);


    });

        $(document).ready(function(){
            var maxField = 10; //Input fields increment limitation

            //buttons
            var eliminationAddButton = $('.add-more-elimination'); //Add button selector
            var substitutionAddButton = $('.add-more-substitution'); //Add button selector
            var engineeringAddButton = $('.add-more-engineering'); //Add button selector
            var administrativeAddButton = $('.add-more-administrative'); //Add button selector
            var ppeAddButton = $('.add-more-ppe'); //Add button selector




            //wrapper
            var eliminationWrapper = $('.after-add-more-elimination'); //Input field wrapper
            var substitutionWrapper = $('.after-add-more-substitution'); //Input field wrapper
            var engineeringWrapper = $('.after-add-more-engineering'); //Input field wrapper
            var administrativeWrapper = $('.after-add-more-administrative'); //Input field wrapper
            var ppeWrapper = $('.after-add-more-ppe');  //Input field wrapper




            //fields
            var fieldEliminationHTML = '<div class="input-group"><input type="text" class="form-control risk-input" name="elimination[]" id="elimination" required placeholder="Enter a recommendation"><span class="input-group-btn recommendation-glyphicon"> <button class="btn btn-link remove_elimination" type="button"><span class="glyphicon glyphicon-minus minusglyphicon"></span></button> </span> </div>';

            var fieldSubstitutionHTML = '<div class="input-group"><input type="text" class="form-control risk-input" name="substitution[]" id="substitution" required placeholder="Enter a recommendation"> <span class="input-group-btn recommendation-glyphicon"> <button class="btn btn-link remove_substitution" type="button"><span class="glyphicon glyphicon-minus minusglyphicon"></span></button> </span> </div>';


            var fieldEngineeringHTML = '<div class="input-group"> <input type="text" class="form-control risk-input" name="engineering[]" id="engineering" required placeholder="Enter a recommendation"> <span class="input-group-btn recommendation-glyphicon"> <button class="btn btn-link remove_engineering" type="button"><span class="glyphicon glyphicon-minus minusglyphicon"></span></button> </span> </div>';

            var fieldAdministrativeHTML = '<div class="input-group"> <input type="text" class="form-control risk-input" name="administrative[]" id="administrative" required placeholder="Enter a recommendation"> <span class="input-group-btn recommendation-glyphicon"> <button class="btn btn-link remove_administrative" type="button"><span class="glyphicon glyphicon-minus minusglyphicon"></span></button> </span> </div>';


            var fieldPpeHTML = ' <div class="input-group"> <input type="text" class="form-control risk-input" name="ppe[]" id="ppe" required placeholder="Enter a recommendation"> <span class="input-group-btn recommendation-glyphicon"> <button class="btn btn-link remove_ppe" type="button"><span class="glyphicon glyphicon-minus minusglyphicon"></span></button> </span> </div> ';

            //var fieldHTML = '<div><input type="text" name="elimination[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="remove-icon.png"/></a></div>'; //New input field html
            var eX = 1; //Initial field counter is 1
            $(eliminationAddButton).click(function(){ //Once add button is clicked
                if(eX < maxField){ //Check maximum number of input fields
                    eX++; //Increment field counter
                    $(eliminationWrapper).append(fieldEliminationHTML); // Add field html
                }
            });
            $(eliminationWrapper).on('click', '.remove_elimination', function(e){ //Once remove button is clicked

               // e.preventDefault();
                $(this).parents('div .input-group').remove(); //Remove field html
                eX--; //Decrement field counter
            });

            //Substitution
            var sX = 1; //Initial field counter is 1
            $(substitutionAddButton).click(function(){ //Once add button is clicked
                if(sX < maxField){ //Check maximum number of input fields
                    sX++; //Increment field counter
                    $(substitutionWrapper).append(fieldSubstitutionHTML); // Add field html
                }
            });

            $(substitutionWrapper).on('click', '.remove_substitution', function(e){ //Once remove button is clicked

                // e.preventDefault();
                $(this).parents('div .input-group').remove(); //Remove field html
                sX--; //Decrement field counter
            });

            //Engineering
            var enX = 1; //Initial field counter is 1
            $(engineeringAddButton).click(function(){ //Once add button is clicked
                if(enX < maxField){ //Check maximum number of input fields
                    enX++; //Increment field counter
                    $(engineeringWrapper).append(fieldEngineeringHTML); // Add field html
                }
            });

            $(engineeringWrapper).on('click', '.remove_engineering', function(e){ //Once remove button is clicked

                // e.preventDefault();
                $(this).parents('div .input-group').remove(); //Remove field html
                enX--; //Decrement field counter
            });

            //Administrative
            var adX = 1; //Initial field counter is 1
            $(administrativeAddButton).click(function(){ //Once add button is clicked
                if(adX < maxField){ //Check maximum number of input fields
                    adX++; //Increment field counter
                    $(administrativeWrapper).append(fieldAdministrativeHTML); // Add field html
                }
            });

            $(administrativeWrapper).on('click', '.remove_administrative', function(e){ //Once remove button is clicked

                // e.preventDefault();
                $(this).parents('div .input-group').remove(); //Remove field html
                adX--; //Decrement field counter
            });

            //PPE
            var pX = 1; //Initial field counter is 1
            $(ppeAddButton).click(function(){ //Once add button is clicked
                if(pX < maxField){ //Check maximum number of input fields
                    pX++; //Increment field counter
                    $(ppeWrapper).append(fieldPpeHTML); // Add field html
                }
            });

            $(ppeWrapper).on('click', '.remove_ppe', function(e){ //Once remove button is clicked

                // e.preventDefault();
                $(this).parents('div .input-group').remove(); //Remove field html
                pX--; //Decrement field counter
            });

        });


    $(document).ready(function() {
        var elimination_score = 0;
        var substitution_score = 0;
        var engineering_score = 0;
        var administrative_score = 0;
        var ppe_score = 0;
        var final_score = 0;


        $(document).on('keyup paste change focus blur keydown', '#elimination', function () {

            var elimination_value = $(this).val();
            if (elimination_value.length > 0) {
                elimination_score = 100;
            } else {
                elimination_score = 0;

            }
            final_score = $("#pscore").val()  * ((100-elimination_score) / 100);
            $("#finalScore").val(final_score);


        });

        $(document).on('keyup paste change focus blur keydown', '#substitution', function () {
            var substitution_value = $(this).val();
            if (substitution_value.length > 0) {
                substitution_score = 80;
            } else {
                substitution_score = 0;

            }

            if (elimination_score == 100) {

                final_score = $("#pscore").val()  * ((100-elimination_score) / 100);
                $("#finalScore").val(final_score);

            } else if(substitution_score == 80 && elimination_score == 0) {

                final_score = $("#pscore").val()  * ((100-substitution_score) / 100);
                $("#finalScore").val(final_score);

            }

        });

        $(document).on('keyup paste change focus blur keydown', '#engineering', function () {
            var engineering_value = $(this).val();
            if (engineering_value.length > 0) {
                engineering_score = 50;
            } else {
                engineering_score = 0;

            }

            if (elimination_score == 100) {

                final_score = $("#pscore").val()  * ((100-elimination_score) / 100);
                $("#finalScore").val(final_score);

            } else if (substitution_score == 80 && elimination_score == 0) {

                final_score = $("#pscore").val()  * ((100-substitution_score) / 100);
                $("#finalScore").val(final_score);

            } else if (engineering_score == 50 && substitution_score == 0 && elimination_score == 0) {

                final_score = $("#pscore").val()  * ((100-engineering_score) / 100);
                $("#finalScore").val(final_score);

            }

        });


        $(document).on('keyup paste change focus blur keydown', '#administrative', function () {
            var administrative_value = $(this).val();
            if (administrative_value.length > 0) {
                administrative_score = 25;
            } else {
                administrative_score = 0;

            }
            if (elimination_score == 100) {

                final_score = $("#pscore").val()  * ((100-elimination_score) / 100);
                $("#finalScore").val(final_score);

            } else if (substitution_score == 80 && elimination_score == 0) {

                final_score = $("#pscore").val()  * ((100-substitution_score) / 100);
                $("#finalScore").val(final_score);

            } else if (engineering_score == 50 && substitution_score == 0 && elimination_score == 0) {

                final_score = $("#pscore").val()  * ((100-engineering_score) / 100);
                $("#finalScore").val(final_score);

            } else if (administrative_score == 25 && engineering_score == 0 && substitution_score == 0 && elimination_score == 0) {

                final_score = $("#pscore").val()  * ((100-administrative_score) / 100);
                $("#finalScore").val(final_score);

            }

        });

        $(document).on('keyup paste change focus blur keydown', '#ppe', function () {
            var ppe_value = $(this).val();
            if (ppe_value.length > 0) {
                ppe_score = 15;
            } else {
                ppe_score = 0;

            }
            if (elimination_score == 100) {

                final_score = $("#pscore").val()  * ((100-elimination_score) / 100);
                $("#finalScore").val(final_score);

            } else if (substitution_score == 80 && elimination_score == 0) {

                final_score = $("#pscore").val()  * ((100-substitution_score) / 100);
                $("#finalScore").val(final_score);

            } else if (engineering_score == 50 && substitution_score == 0 && elimination_score == 0) {

                final_score = $("#pscore").val()  * ((100-engineering_score) / 100);
                $("#finalScore").val(final_score);

            } else if (administrative_score == 25 && engineering_score == 0 && substitution_score == 0 && elimination_score == 0) {

                final_score = $("#pscore").val()  * ((100-administrative_score) / 100);
                $("#finalScore").val(final_score);

            } else if (ppe_score == 15 && administrative_score == 0 && engineering_score == 0 && substitution_score == 0 && elimination_score == 0) {

                final_score = $("#pscore").val()  * ((100-ppe_score) / 100);
                $("#finalScore").val(final_score);
            }
        });


    });

</script>


