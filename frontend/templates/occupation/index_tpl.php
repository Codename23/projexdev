<div class="row">
    <div class="col-md-12">


        <div class="panel panel-default">
            <div class="panel-heading">occupations</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" class="table table-hover" id="dataTables-examples">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Branch</th>
                            <th>Department</th>
                            <th>Occupati$data['companyOccupations']ons Name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php if($data['companyOccupations']): foreach($data['companyOccupations'] as $occupation): ?>
                            <tr>
                                <td><?php echo $occupation['id']; ?></td>
                                <td><?php echo $occupation['branch_name']; ?></td>
                                <td><?php echo $occupation['department_name']; ?></td>
                                <td><?php echo $occupation['name']; ?></td>
                                <td><a data="<?php echo $occupation['id']; ?>" href="" class="editOccupation" id="editOccupation">Edit</a></td>
                                <td class="danger"><a href="#">Delete</a></td>
                            </tr>
                        <?php endforeach;endif;?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    $('.addOccupation').show();
    $('#dataTables-examples').DataTable({
        responsive: true
    });
    $('.editOccupation').on("click",function(e){
        e.preventDefault();
        $('.addOccupation').hide();
        var occupationId = $(this).attr("data");
        $.post('<?php echo BASE_URL . "/index.php?module=occupation&action=editOccupation"?>', {id:occupationId}, function(data){
            $('.occupationResults').html(data);
        });
    });

</script>