 <form class="form-horizontal" method="post" name="addInvestigatorForm" action="<?php echo BASE_URL;?>/index.php?action=updateInvestigatePost&module=investigation"> 
                <input type="hidden" id="edit_investigate" name="edit_investigate"/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigatorNameSurname">Name Surname</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect person_select" disabled="true" id="investigatorNameSurname" required="true" name="investigatorNameSurname" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a name and surname">
                          <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>
                    </select> 
                </div>
            </div><br/>
            <div class="form-group">                
                <input type="hidden" name="departments_selected" id="departments_selected"/>
                <label class="control-label col-sm-4" for="investigationDepartment">Investigation Departments</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect"id="investigationDepartment"  required="true" name="investigationDepartment" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                           <?php
                            foreach($data['allDepartments'] as $group)
                            {                                
                                echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                            }?>
                    </select> 
                </div>
            </div>   
            <div class="form-group">
                <input type="hidden" name="groups_selected" id="groups_selected"/>
                <label class="control-label col-sm-4" for="investigationntGroups">Investigation Groups</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" id="investigationGroups" required="true" name="investigationGroups" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more groups" data-header="Close">
                     <?php
                        foreach($data['allSheqTeams'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['sheqteam_name']}</option>";
                        }?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" name="btnUpdateInvestigator" id="btnUpdateInvestigator" class="btn btn-success">Update Investigator</button>
                </div>
            </div>
        </form>