<?php 
$title = 'Improvement Why Investigation';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<div class="col-lg-10">
  
<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
                 <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Non-Conformance</a></li>
                    <li><a href="improvement_incident.php">Incidents</a></li>
                    <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Investigations</a></li>
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action">Corrective Actions</a></li>
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Improvement</a></li>
                    <li><a href="improvement_objective.php">Objective</a></li>
        </ul>
    </div>
</div> 
    <?php
   global $objNonConformance;
   $source = 0;
   $investigate_id = 0;
   if(isset($data["allNonConformance"]) && !empty($data["allNonConformance"]))
   {
        foreach($data["allNonConformance"] as $i)
        {
             $objNonConformance->get_id = $i["nc_id"];
             $objNonConformance->get_created_date = $i["nc_date_created"];
             $objNonConformance->get_group_name = $i["nc_group_field"];
             $objNonConformance->get_department_name = $i["department_name"];
             $objNonConformance->get_due_date = $i["nc_due_date"];
             $objNonConformance->get_responsible_person = $i["responsible"];
             $objNonConformance->get_responsible_person_id = $i["responsible_person"];
             $objNonConformance->get_person_involved = $i["person_involve"];
             $objNonConformance->get_resource_involved= $i["resource"];
             $objNonConformance->get_non_conformance_details = $i["non_conformance_details"];
             $objNonConformance->get_loggedBy = $i["loggedBy"];
             $objNonConformance->get_status = $i["status"];
             $objNonConformance->get_photo = $i["photo_name"];
             $objNonConformance->get_investigator = $i["investigator_person"];
             $objNonConformance->get_investigationType = $i["investigation_type"];
             $objNonConformance->get_investigate_due_date = $i["nc_invest_due_date"];
             $objNonConformance->get_location = $i["photo_name"];
             $investigate_id = $i["inv_id"];
             $source = $i["investigate_link_id"];
             break;
        }
   }
   if($objNonConformance->get_id == 0)
   {
       //error handeling information
      // header('Location: http://localhost/sheq/index.php?action=dashboard&module=user');
   }
    ?>
     <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Investigation Details</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                            <table width="100%" class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td width="30%">No</td>
                                        <td><?php echo $objNonConformance->get_id;?></td>
                                    </tr>
                                    <tr>
                                        <td>Date</td>
                                        <td><?php echo $objNonConformance->get_created_date;?></td>
                                    </tr>
                                    <tr>
                                        <td>Type</td>
                                        <td>5 Why Investigation</td>
                                    </tr>
                                    <tr>
                                        <td>Investigator</td>
                                        <td><?php echo $objNonConformance->get_investigator;?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Source Information</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Type</td>
                                    <td>Non-Conformance</td>
                                </tr>
                                <tr>
                                    <td width="30%">No</td>
                                    <td>123</td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td>2017/05/12</td>
                                </tr>
                                <tr>
                                    <td>Group</td>
                                    <td>OHS</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>PPE</td>
                                </tr>
                                <tr>
                                    <td>Reporting Person</td>
                                    <td>Pieter</td>
                                </tr>
                                <tr>
                                    <td>Non-Conformance</td>
                                    <td>Walk on roof without fall protection</td>
                                </tr>
                                <tr>
                                    <td>Link</td>
                                    <td>Non-Conformance</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Involvement</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td>Persons Involved</td>
                                    <td>Sunny Mathole</td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Supplier Involved</td>
                                    <td>Supplier 1</td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Resources involved</td>
                                    <td>Conveyor Belt</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Images</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Photo</td>
                                    <td>
                                    <?php
                    if($objNonConformance->get_photo != "")
                    {
                        echo "<img width='250px' class='img-responsive' src='".BASE_URL."/frontend/media/uploads/".$objNonConformance->get_photo."' alt='non-conformance image'/>";
                    }
                    else
                    {
                        echo "None";
                    }
?></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of panel-default-->
    
    <?php
    global $objInvestigations;
    if($objInvestigations->hasFive == false)
    {
    ?>
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#whyInvestigation-modal"   data-toggle="modal" data-target="#whyInvestigation-modal"><button type="button" id="whyInvestigation" class="btn btn-success">Start 5 Why Investigation Method</button></a>
    </div>
    </div>
    <?php
    }
    else
    {
       ?>
        <div class="panel panel-default">
        <div class="panel-body">
            <a href="#EditwhyInvestigation-modal"   data-toggle="modal" data-target="#EditwhyInvestigation-modal"><button type="button" id="EditwhyInvestigation" class="btn btn-success">Edit 5 Why Investigation Method</button></a>
        </div>
        </div>
     <?php   
    }
    ?>
    <div class="panel panel-default">
    <div class="panel-heading">5 Why Investigation</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Why</th>
                    <th>Type</th>
                    <th>Description of Non-Conformance</th>
                    <th>Recommended Improvement</th>
                    <th>Priority</th>
                    <th>Action Taken</th>
                    <th>Reference No</th>
                    <th>Corrective Action</th>
                    <th>Improvement</th>
                </tr>
            </thead>
            <tbody>
                <?php
                 if(isset($data["allFiveWhyDetails"]) && !empty($data["allFiveWhyDetails"]))
                    {
                         foreach($data["allFiveWhyDetails"] as $why)
                         {                           
                                 ?>
                                 <tr>
                                     <td><?php echo $why['why'];?></td>
                                     <td><?php echo $why['type'];?></td>
                                     <td><?php echo $why['description'];?></td>
                                     <td><?php echo $why['recomm'];?></td>
                                     <td><?php echo $why['priority'];?></td>
                                     <td></td>
                                     <td></td>
                                     <?php
                                     if($why['type'] > 0)
                                     {
                                        if(empty($why['improveId']))
                                        {
                                           if(empty($why['correctiveId']))
                                           {
                                               $objInvestigations->hasAddedImprovementOrCorrectiveAction = false;
                                               //check the source
                                               //check investigation type
                                               $added_url = "";
                                               switch($objNonConformance->get_investigationType)
                                               {
                                                   case 1:
                                                       $added_url = "ncid=".$source."&why_id=".$why["id"];
                                                       break;
                                                   case 2:
                                                       //from corrective action
                                                       break;
                                               }                                      
                                              ?>
                                              <td><a href="<?php echo BASE_URL."/index.php?".$added_url."&action=addCorrectiveAction&module=corrective_action"?>">Add Corrective Action</a></td>
                                              <?php
                                           }
                                           else
                                           { 
                                               $objInvestigations->hasAddedImprovementOrCorrectiveAction = true;
                                              ?>
                                              <td>View Corrective Action</td>
                                              <?php
                                           }
                                        }
                                        else
                                        {
                                            echo "<td></td>";
                                        }
                                     }
                                     ?>
                                     <?php
                                     if($why['type'] > 0)
                                     {
                                        if(empty($why['correctiveId']))
                                        {
                                            if(empty($why['improveId']))
                                            {
                                               ?>
                                               <td><a href="#addImprovement-modal" data-toggle="modal" data-target="#addImprovement-modal">Add Improvement</a></td>
                                               <?php
                                            }
                                            else
                                            {
                                                   $objInvestigations->hasAddedImprovementOrCorrectiveAction = true;
                                                  ?>
                                                  <td>View improvement</td>
                                               <?php                                         
                                            }
                                        }
                                        else
                                        {
                                            //not empty
                                            echo "<td></td>";
                                        }
                                     }
                                     else
                                     {
                                         echo "<td></td>";
                                     }
                                     ?>
                                 </tr>
                 <?php
                         }
                    }
                 ?>                
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of panel-default-->
<?php
    if($objInvestigations->hasAddedImprovementOrCorrectiveAction)
    {
        ?>
        <div class="panel panel-default">
        <div class="panel-body">
            <a href="#investigatorSignoff-modal" data-toggle="modal" data-target="#investigatorSignoff-modal"><button type="button" id="investigatorSignoff" class="btn btn-success">Investigator Sign Off</button></a>
        </div>
        </div>
        <?php
    }
    ?>
   
    
    <!--Why Investigation Modal -->
        <div class="modal fade" id="whyInvestigation-modal" role="dialog">
          <div class="modal-dialog risk-assessment-dialog-modal">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Why Investigation</h4>
              </div>
              <div class="modal-body table-responsive">
                  <form class="form-horizontal" name="whyInvestigationForm" method="post" action="<?php echo BASE_URL;?>/index.php?action=addWhyInvestigation&module=investigation">          
  <table width="100%" class="table table-hover">
      <input type="hidden" name="ncid" id="ncid" value="<?php echo $objNonConformance->get_id;?>"
                    <thead>
                    <tr>
                        <th>Why</th>
                        <th>Type</th>
                        <th>Description of Non-Conformance</th>
                        <th>Recommended Improvement</th>
                        <th>Priority</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    for($i = 1; $i < 6;$i++)
                    {
                    ?>
                    <tr>
                        <td class="custom-hidden-border-right" width="8%">Why <?php echo $i?></td>
                        <td class="custom-hidden-border-right" width="15%">
                            <div class="form-group custom-hidden-group-input">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                <select class="form-control custom-hidden-input" name="type[]" id="type"
                                        <?php
                                        if($i == 1)
                                        {
                                            echo 'Required';
                                        }
                                        ?>>
                                    <?php
                                    $priority = array("Select a type","Human","Condition","System");
                                     if(isset($_POST['type'][($i - 1)]))
                                     {
                                         //get the selected value
                                         $counter = 0;
                                         foreach($priority as $pro)
                                         {
                                             $counter++;
                                             if($counter == $_POST['type'][($i - 1)])
                                             {
                                                 echo "<option selected value='$counter'>$pro</option>";
                                             }
                                             else
                                             {
                                                 echo "<option value='$counter'>$pro</option>";
                                             }
                                         }
                                     }
                                     else
                                     {
                                    ?>
                                    <option value="0" selected>Select a type</option>
                                    <option value="1">Human</option>
                                    <option value="2">Condition</option>
                                    <option value="3">System</option>
                                    <?php
                                     }
                                    ?>
                                </select>
                                </div>
                            </div>
                        </td>
                        <td class="custom-hidden-border-right">
                            <div class="col-lg-12 col-md-4 col-sm-8">
                                <textarea rows="2" class="form-control custom-hidden-input" name="description[]" id="description" placeholder="Enter a description"><?php
                                if(isset($_POST['description'][($i - 1)]))
                                {
                                    echo $_POST['description'][($i - 1)];
                                }
                                ?></textarea>
                            </div>
                        </td>
                        <td class="custom-hidden-border-right">
                            <div class="col-lg-12 col-md-4 col-sm-8">
                                <textarea rows="2" class="form-control custom-hidden-input" name="recommendedImprovement[]" id="recommendedImprovement" placeholder="Enter a recommended improvement"><?php
                                if(isset($_POST['recommendedImprovement'][($i - 1)]))
                                {
                                    echo $_POST['recommendedImprovement'][($i - 1)];
                                }
                                ?></textarea>
                            </div>
                        </td>
                        <td class="custom-hidden-border-right">
                            <div class="form-group custom-hidden-group-input">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                <select class="form-control custom-hidden-input" name="priority[]" id="priority">
                                    <?php
                                    $priority = array("High","Medium","Low");
                                     if(isset($_POST['priority'][($i - 1)]))
                                     {
                                         //get the selected value
                                         $counter = 0;
                                         foreach($priority as $pro)
                                         {
                                             $counter++;
                                             if($counter == $_POST['priority'][($i - 1)])
                                             {
                                                 echo "<option selected value='$counter'>$pro</option>";
                                             }
                                             else
                                             {
                                                 echo "<option value='$counter'>$pro</option>";
                                             }
                                         }
                                     }
                                     else
                                     {
                                        ?>
                                        <option value="1">High</option>
                                        <option value="2">Medium</option>
                                        <option value="3">Low</option>
                                        <?php
                                     }
                                    ?>
                                </select>
                                </div>
                            </div>
                        </td>
                    </tr>   
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
                <div class="form-group">
                    <div class="col-sm-8">
                    <button type="submit" name="btnSubmitFiveWhy"  class="btn btn-success">Submit</button>
                    </div>
                </div>
                </form>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>
      <!--End Of Modal -->
     
      
      <!-- Edit 5 Why Investigation -->
       <!--Why Investigation Modal -->
       <?php
       if($objInvestigations->hasFive == true)
       {
       ?>
        <div class="modal fade" id="EditwhyInvestigation-modal" role="dialog">
          <div class="modal-dialog risk-assessment-dialog-modal">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Why Investigation</h4>
                <?php
                //echo $data[array_keys($data["allFiveWhyDetails"])[2]];
                ?>
              </div>
              <div class="modal-body table-responsive">
                  <form class="form-horizontal" name="whyInvestigationForm" method="post" action="<?php echo BASE_URL;?>/index.php?action=addWhyInvestigation&module=investigation">          
  <table width="100%" class="table table-hover">
      <input type="hidden" name="ncid" id="ncid" value="<?php echo $objNonConformance->get_id;?>"
                    <thead>
                    <tr>
                        <th>Why</th>
                        <th>Type</th>
                        <th>Description of Non-Conformance</th>
                        <th>Recommended Improvement</th>
                        <th>Priority</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    foreach($data["allFiveWhyDetails"] as $why)
                    {
                        $i++;
                    ?>
                    <tr>
                        <td class="custom-hidden-border-right" width="8%">Why <?php echo $i?></td>
                        <td class="custom-hidden-border-right" width="15%">
                            <div class="form-group custom-hidden-group-input">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                <select class="form-control custom-hidden-input" name="type[]" id="type"
                                        <?php
                                        if($i == 1)
                                        {
                                            echo 'Required';
                                        }
                                        ?>>
                                    <?php
                                    $priority = array("Select a type","Human","Condition","System");
                                     if(isset($_POST['type'][($i - 1)]))
                                     {
                                         //get the selected value
                                         $counter = 0;
                                         foreach($priority as $pro)
                                         {
                                             $counter++;
                                             if($counter == $_POST['type'][($i - 1)])
                                             {
                                                 echo "<option selected value='$counter'>$pro</option>";
                                             }
                                             else
                                             {
                                                 echo "<option value='$counter'>$pro</option>";
                                             }
                                         }
                                     }
                                     else
                                     {
                                         //get the selected id   
                                         if(isset($data["allFiveWhyDetails"]))
                                         {
                                            //$counter = 0;
                                            foreach($priority as $pro)
                                            {
                                                $counter++;
                                                if($counter == $why['type'][($i - 1)])
                                                {
                                                    echo "<option selected value='$counter'>$pro</option>";
                                                }
                                                else
                                                {
                                                    echo "<option value='$counter'>$pro</option>";
                                                }
                                            }
                                         }
                                     }
                                    ?>
                                </select>
                                </div>
                            </div>
                        </td>
                        <td class="custom-hidden-border-right">
                            <div class="col-lg-12 col-md-4 col-sm-8">
                                <textarea rows="2" class="form-control custom-hidden-input" name="description[]" id="description" placeholder="Enter a description"><?php
                                if(isset($_POST['description'][($i - 1)]))
                                {
                                    echo $_POST['description'][($i - 1)];
                                }
                                else
                                {
                                    //get the current
                                    if(isset($data["allFiveWhyDetails"]))
                                    {
                                         echo $why['description'];
                                    }
                                }
                                ?></textarea>
                            </div>
                        </td>
                        <td class="custom-hidden-border-right">
                            <div class="col-lg-12 col-md-4 col-sm-8">
                                <textarea rows="2" class="form-control custom-hidden-input" name="recommendedImprovement[]" id="recommendedImprovement" placeholder="Enter a recommended improvement"><?php
                                if(isset($_POST['recommendedImprovement'][($i - 1)]))
                                {
                                    echo $_POST['recommendedImprovement'][($i - 1)];
                                }
                                else
                                {
                                    if(isset($data["allFiveWhyDetails"]))
                                    {
                                         echo $why['recomm'];
                                    }
                                }
                                ?></textarea>
                            </div>
                        </td>
                        <td class="custom-hidden-border-right">
                            <div class="form-group custom-hidden-group-input">
                                <div class="col-lg-12 col-md-4 col-sm-8">
                                <select class="form-control custom-hidden-input" name="priority[]" id="priority">
                                    <?php
                                    $priority = array("High","Medium","Low");
                                     if(isset($_POST['priority'][($i - 1)]))
                                     {
                                         //get the selected value
                                         $counter = 0;
                                         foreach($priority as $pro)
                                         {
                                             $counter++;
                                             if($counter == $_POST['priority'][($i - 1)])
                                             {
                                                 echo "<option selected value='$counter'>$pro</option>";
                                             }
                                             else
                                             {
                                                 echo "<option value='$counter'>$pro</option>";
                                             }
                                         }
                                     }
                                     else
                                     {
                                            if(isset($data["allFiveWhyDetails"]))
                                            {
                                               //$counter = 0;
                                               foreach($priority as $pro)
                                               {
                                                   $counter++;
                                                   if($counter == $why['priority'][($i - 1)])
                                                   {
                                                       echo "<option selected value='$counter'>$pro</option>";
                                                   }
                                                   else
                                                   {
                                                       echo "<option value='$counter'>$pro</option>";
                                                   }
                                               }
                                            }
                                     }
                                    ?>
                                </select>
                                </div>
                            </div>
                        </td>
                    </tr>   
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
                <div class="form-group">
                    <div class="col-sm-8">
                    <button type="submit" name="btnSubmitEditFiveWhy"  class="btn btn-success">Submit</button>
                    </div>
                </div>
                </form>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>
      <!--End Of Modal -->
       <?php }?>

       <!-- End of 5 Why Investigation -->      
        <!-- Add Improvement Modal -->
  <div class="modal fade" id="addImprovement-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Improvement</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addImprovementForm" action="<?php echo BASE_URL;?>/index.php?action=addImprovement&module=improvement">
                <input type="hidden" name="action" value="Investigation"/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="dateCreated">Date Created</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="dateCreated" id="dateCreated"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="group" class="form-control">
                    <option value selected disabled>Please select a group</option>
                    <option value="1">OHS (Health & Safety)</option>
                    <option value="2">E (Environment)</option>
                    <option value="3">Q (Quality)</option>
                    <option value="4">F (Food)</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="improvementType">Improvement Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="improvementType" id="improvementType"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="improvementDescription">Improvement Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="improvementDescription" id="improvementDescription"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="responsiblePerson">Responsible Person</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people">
                    <option>Nick Botha</option>
                    <option>Sunny Mathole</option>
                    <option>Warren Windvogel</option>
                    <option>Brian Douglas</option>
                </select>
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Approval</h5></span></div>  
            <div class="form-group">
                <label class="control-label col-sm-4" for="managerialApproval">Need Managerial Approval?</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <label class="radio-inline"><input type="radio" name="managerialApproval" id="managerialApproval1" value="0" required>Yes</label>
                    <label class="radio-inline"><input type="radio" name="managerialApproval" id="managerialApproval2" value="1" >No</label>
                </div>
            </div>

            <div class="managerial hidden-div">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="managerialDueDate">Due Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="managerialDueDate" name="managerialDueDate" required >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="branchLevel">Branch Level</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                        <option>Nick Botha</option>
                        <option>Brian Douglas</option>
                        <option>Sunny Mathole</option>
                    </select> 
                    </div>
               </div>
               <div class="form-group">
                    <label class="control-label col-sm-4" for="groupLevel">Group Level</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                        <option>Nick Botha</option>
                        <option>Brian Douglas</option>
                        <option>Sunny Mathole</option>
                    </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="motivation">Motivation/Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" id="motivation" name="motivation"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="estimateCost">Estimate Cost</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="estimateCost" name="estimateCost" required value="R">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="uploadDocs">Upload Documents</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="file" id="uploadDocs" name="uploadDocs">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-4" for="sheqfApproval">Need SHEQF Approval?</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <label class="radio-inline"><input type="radio" name="sheqfApproval" id="sheqfApproval1" value="0" required>Yes</label>
                    <label class="radio-inline"><input type="radio" name="sheqfApproval" id="sheqfApproval2" value="1" >No</label>
                </div>
            </div>

            <div class="sheqf hidden-div2">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="sheqfDueDate">Due Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="sheqfDueDate" name="sheqfDueDate" required >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="committee">Committee</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="form-control" name="committee" id="committee" required>
                            <option value selected disabled>Please select a committee</option>
                            <option>Health</option>
                            <option>Safety</option>
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="sheqPersons">SHEQ Persons</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple="" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people" data-header="Close">
                        <option>Nick Botha</option>
                        <option>Brian Douglas</option>
                        <option>Sunny Mathole</option>
                    </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="sheqfMotivation">Motivation/Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" id="sheqfMotivation" name="sheqfMotivation"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addImprovementBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
      
<!--End Of Modal -->
<!-- View Improvement Modal -->
  <div class="modal fade" id="viewImprovement-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">View Improvement Details</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
            <table width="100%" class="table table-hover">
                <thead>
                    <tr>
                        <th colspan="2">Improvement Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="40%">No</td>
                        <td>123456789</td>
                    </tr>
                    <tr>
                        <td>Date Created</td>
                        <td>2017/04/01</td>
                    </tr>
                    <tr>
                        <td>Group</td>
                        <td>OHS</td>
                    </tr>
                    <tr>
                        <td>Improvement Type</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Improvement Description</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Responsible Person</td>
                        <td>Nick Botha</td>
                    </tr>
                </tbody>
            </table>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
<!-- View Corrective Action Modal -->
  <div class="modal fade" id="viewCorrectiveAction-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">View Corrective Action Details</h4>
        </div>
        <div class="modal-body">
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th colspan="2">Corrective Action Details</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td width="50%">No</td>
                    <td>234524</td>
                </tr>
                <tr>
                    <td>Department</td>
                    <td>Finance</td>
                </tr>
                <tr>
                    <td>System Element</td>
                    <td>Training</td>
                </tr>
                <tr>
                    <td>Non-Conformance Description</td>
                    <td>This is a description</td>
                </tr>
                <tr>
                    <td>Description of corrective action</td>
                    <td>This is a description</td>
                </tr>
                <tr>
                    <td>Department Manager</td>
                    <td>Nick Botha</td>
                </tr>
                <tr>
                    <td>Person who lodged corrective action</td>
                    <td>Sunny</td>
                </tr>
            </tbody>
        </table>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Investigator Sign Off List Modal -->
  <div class="modal fade" id="investigatorSignoff-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Investigator Sign Off</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="investigatorSignoffForm" action="<?php echo BASE_URL;?>/index.php?action=signedOffInvestigation&module=investigation"> 
                <input type="hidden" name="txtInvestigationId" id="txtInvestigationId" value="<?php echo $investigate_id;?>"/>
                <input type="hidden" name="txtMaster" id="txtMaster" value="Master"/>
                <input type="hidden" name="txtSignOffInvestigation" id="txtSignOffInvestigation" value="Investigation"/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="signoffUsername">Username</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="signoffUsername" disabled="disabled" value="<?php echo $_SESSION['firstname'] . ' '. $_SESSION['lastname']; ?>" id="signoffUsername" required=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="signoffPassword">Password</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="password" class="form-control" name="signoffPassword" id="signoffPassword"> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="investigatorSignoffBtn" name="investigatorSignoffBtn" class="btn btn-success">Sign Off</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
  </div>
<!--End Of Modal -->
   </div>
</div><!--End of container-fluid-->
<script>
$(document).ready(function(){
    $("#dateCreated").datepicker({
        dateFormat:"yy/mm/dd",
        maxDate: new Date(), 
        minDate: new Date()});
    $("#dateCreated").datepicker("setDate", new Date());
    
    $("#managerialDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    $("#sheqfDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
       $('input[name="sheqfApproval"]').click(function(){
        if($(this).attr("value")==="0"){
            $(".hidden-div2").not(".sheqf").hide();
            $(".sheqf").show();
        }else{
            $(".sheqf").hide();
        }
    });
    $('input[name="managerialApproval"]').click(function(){
            if($(this).attr("value")==="0"){
                $(".hidden-div").not(".managerial").hide();
                $(".managerial").show();
            }else{
                $(".managerial").hide();
            }
    });
});
</script>
<?php 
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  