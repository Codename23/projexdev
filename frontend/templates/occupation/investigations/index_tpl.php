<?php 
$title = 'Improvement';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<div class="col-lg-10">
  
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
               <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Non-Conformance</a></li>
                <li><a href="improvement_incident.php">Incidents</a></li>
                <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Investigations</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action">Corrective Actions</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Improvement</a></li>
                <li><a href="improvement_objective.php">Objective</a></li>
    </ul>
    </div>
</div> 
    
<div class="row">
    <div class="col-lg-6">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Active & Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=viewInvestigators&module=investigation">Investigators</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigationsHistory&module=investigation">History</a></li>
    </ul>
    </div>
</div>  

<div class="row">
    <div class="col-lg-4">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="improvement_investigations.php">Non-Conformance</a></li>
        <li><a href="improvement_investigations_incident.php">Incident</a></li>
    </ul>
    </div>
</div> 
    
    <div class="panel panel-default">
        <div class="panel-heading">Search Non-Conformance Investigations</div>
        <div class="panel-body">
        <form class="form-inline" method="post" name="search" action="">
            <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Group</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more groups"data-header="Close">
                    <option value="1">OHS (Health & Safety)</option>
                    <option value="2">E (Environment)</option>
                    <option value="3">Q (Quality)</option>
                    <option value="4">F (Food)</option>
                </select>
            </div>
            </div>

            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Select Department</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                    <option>Receiving</option>
                    <option>Welding</option>
                    <option>Support and Services</option>
                </select>
            </div>
            </div>
                
            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Investigator</b></p>
                <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more investigators" data-header="Close">
                    <option value="1">Nick Botha</option>
                    <option value="2">Sunny Mathole</option>
                    <option value="3">Brian Douglas</option>
                </select> 
            </div>
            </div>
                
            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Status</b></p>
                <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more status types" data-header="Close">
                    <option value="1">Due in a month</option>
                    <option value="2">Due in 2 months</option>
                    <option value="3">Overdue</option>
                    <option value="4">Good</option>
                </select> 
            </div>
            </div>
            </div>
            <div style="margin-left: 30px;">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Search">
            </div>
            </div>
        </form>
        </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading">Non-Conformance Investigations</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Date Lodged</th>
                    <th>Group</th>
                    <th>Department</th>
                    <th>Non-Conformance Details</th>
                    <th>Investigator</th>
                    <th>Investigation Due Date</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                 <?php if($data['allInvestigations']): foreach($data['allInvestigations'] as $nc): ?>
                            <tr>
                                <td><?php echo $nc['nc_id']; ?></td>
                                <td><?php echo $nc['nc_date_created']; ?></td>                                
                                <td><?php echo $nc['nc_group_field']; ?></td>
                                <td><?php echo $nc['department_name']; ?></td>
                                <td><?php echo $nc['non_conformance_details']; ?></td>
                                <td><?php echo $nc['investigator']; ?></td>
                                <td><?php echo $nc['investigator_due_date']; ?></td>                                
                                <td><?php echo $nc['investigator_status']; ?></td>
                                <td><a class="investigate_item" link="<?php echo BASE_URL."/index.php?ncid={$nc['nc_id']}&action=viewSingleInvestigation&module=investigation";?>" attr="<?php echo $nc['tid'];?>" data-toggle="modal" data-target="#investigationType-modal">Investigate</a></td>
                            </tr>
                        <?php endforeach;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of panel-default-->
    
    <!-- Investigation Type Modal -->
    <div class="modal fade" id="investigationType-modal" role="dialog">
      <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Choose An Investigation</h4>
          </div>
          <div class="modal-body">
              <form class="form-horizontal" method="post" name="addInvestigatorForm">
              <div class="form-group">
                <span class="col-sm-3"></span>
                <div class="col-lg-8 col-md-4 col-sm-8">
                    <a href="<?php echo BASE_URL;?>/improvement_investigate_fast_root.php?action=getAllInvestigations&module=investigation"><button type="button" id="fastRoot" class="btn btn-success">Fast Root</button></a>
                    <a href="<?php echo BASE_URL;?>/improvement_investigate_root_cause.php?action=getAllInvestigations&module=investigation"><button type="button" id="rootCause" class="btn btn-success">Root Cause</button></a>
                    <a href="#"><button type="button" id="rootCause" class="btn btn-success why_investigate">5 Why Investigation</button></a>
                </div>
              </div>
          </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          </div>
      </div>
    </div>
  <!--End Of Modal -->
    
    <!--<div class="panel panel-default">
    <div class="panel-heading">Active Investigations</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Incident Number</th>
                    <th>Date & Time</th>
                    <th>Department</th>
                    <th>Incident Type</th>
                    <th>Category</th>
                    <th>Investigator</th>
                    <th>Investigation Due Date</th>
                    <th>Investigate Now</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>1254</td>
                    <td>15 June 2015 15:00</td>
                    <td>Information and Technology</td>
                    <td>Near miss</td>
                    <td></td>
                    <td>Nick Botha</td>
                    <td>15 November 2016</td>
                    <td></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1254</td>
                    <td>15 June 2015 15:00</td>
                    <td>Information and Technology</td>
                    <td>Near miss</td>
                    <td></td>
                    <td>Nick Botha</td>
                    <td>15 November 2016</td>
                    <td></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1254</td>
                    <td>15 June 2015 15:00</td>
                    <td>Information and Technology</td>
                    <td>Near miss</td>
                    <td></td>
                    <td>Nick Botha</td>
                    <td>15 November 2016</td>
                    <td></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of panel-default-->
 
   </div>
</div>
<script>
    $(document).ready(function()
    {
        var url_click = 0;
        
       $(".investigate_item").click(function()
       {
           url_click = $(this).attr('link');                      
       });
       $(".why_investigate").click(function()
       {
          if(url_click != 0)
          {
            window.location = url_click; 
          }
       });       
    });
</script>
<?php 
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  