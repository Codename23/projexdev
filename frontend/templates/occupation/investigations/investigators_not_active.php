<?php 
$title = 'Improvement';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<div class="col-lg-10">
  
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
               <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Non-Conformance</a></li>
                <li><a href="improvement_incident.php">Incidents</a></li>
                <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Investigations</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action">Corrective Actions</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Improvement</a></li>
                <li><a href="improvement_objective.php">Objective</a></li>
    </ul>
    </div>
</div> 
    
<div class="row">
    <div class="col-lg-6">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Active & Upcoming</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=viewInvestigators&module=investigation">Investigators</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigationsHistory&module=investigation">History</a></li>
    </ul>
    </div>
</div>  


<div class="row">
    <div class="col-lg-4">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=viewInvestigators&module=investigation">Active</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=viewInvestigatorsNotActive&module=investigation">Not Active</a></li>
    </ul>
    </div>
</div> 
    
    <div class="panel panel-default">
    <div class="panel-heading">Not Active Investigators</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Name Surname</th>
                    <th>Department</th>
                    <th>Date Added</th>
                    <th>Investigation Departments</th>
                    <th>Group</th>
                    <th>Date Deactivated</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                 <?php if($data['allInvestigations']): foreach($data['allInvestigations'] as $nc): ?>
                            <tr>                              
                                <td><?php echo $nc['fullname']; ?></td>
                                <td><?php echo $nc['department_name']; ?></td>
                                <td><?php echo $nc['date_created']; ?></td>
                                <td><?php echo $nc['investigation_dep']; ?></td>                                
                                <td><?php echo $nc['investigation_group']; ?></td>                                
                                <td><?php echo $nc['iv_date_modified']; ?></td>
                                <td><a href="<?php echo BASE_URL;?>/index.php?ivd=<?php echo $nc['invest_id'];?>&action=restoreInvestigator&module=investigation">Restore</a></td>
                            </tr>
                        <?php endforeach;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of panel-default-->
     <!-- Add Investigator Modal -->
  <div class="modal fade" id="addInvestigator-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Investigator</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addInvestigatorForm" action="<?php echo BASE_URL;?>/index.php?action=addInvestigatorsPost&module=investigation"">
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigatorDateCreated">Date Created</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="investigatorDateCreated" id="investigatorDateCreated" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigatorDepartment">Department</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect dep_select" required="true" name="investigatorDepartment" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                          <?php
                            foreach($data['allDepartments'] as $group)
                            {
                                echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                            }?>
                    </select> 
                </div>
            </div>   
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigatorNameSurname">Name Surname</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect person_select" id="investigatorNameSurname" required="true" name="investigatorNameSurname" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a name and surname">
                          <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>
                    </select> 
                </div>
            </div><br/>
            <div class="form-group">                
                <input type="hidden" name="departments_selected" id="departments_selected"/>
                <label class="control-label col-sm-4" for="investigationDepartment">Investigation Departments</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect"id="investigationDepartment"  required="true" name="investigationDepartment" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                           <?php
                            foreach($data['allDepartments'] as $group)
                            {
                                echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                            }?>
                    </select> 
                </div>
            </div>   
            <div class="form-group">
                <input type="hidden" name="groups_selected" id="groups_selected"/>
                <label class="control-label col-sm-4" for="investigationntGroups">Investigation Groups</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" id="investigationGroups" required="true" name="investigationGroups" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more groups" data-header="Close">
                     <?php
                        foreach($data['allSheqTeams'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['sheqteam_name']}</option>";
                        }?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" name="btnAddInvestigator" id="addInvestigatorBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>     
 
   </div>
</div>
<script>
    $(document).ready(function()
    {
         $("#investigatorDateCreated").datepicker({
        dateFormat:"yy/mm/dd",
        maxDate: new Date(), 
        minDate: new Date()});
        $("#investigatorDateCreated").datepicker("setDate", new Date());
      
        $('.selectpicker').on('change', function(){              
        var selected = $(this," option:selected").val();
        focus_dropdown = $(this).parent().children('select').attr('id');
        switch(focus_dropdown)
        {
            case 'investigationGroups':
                $("#groups_selected").val(selected);
                break;
           case 'investigationDepartment':
               $("#departments_selected").val(selected);
                break;                
        }
       
    });
       
        
        $(".dep_select").on("change",function()
        {
            /*var selected_dep = $(this).val();
            var url_ = '<?php //echo BASE_URL . "/index.php?module=investigation&action=getEmployeeByDepartment";?>';
            $.ajax({
               url:url_,
               type:'GET',
               dataType: 'json',
               success:function(result)
               {
                    var rs = JSON.stringify(result);
                    var emp = JSON.parse(rs); 
                    var $select = $(".person_select");
                    $($select).empty();
                    $(emp).each(function(key,value)
                    {                          
                        $select.append($('<option>', {
                            value: value.id,
                            text: value.fullname
                        }));
                    });        
               }
            });
            return false;*/
        });
    });
</script>
<?php 
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  