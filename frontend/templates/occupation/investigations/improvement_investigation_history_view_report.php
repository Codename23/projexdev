<?php 
$title = 'Improvement Why Investigation';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
global $objInvestigations;
$objInvestigations->getInvestigationReport($_GET["tid"]);
?> 
<div class="col-lg-10">
    
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Investigation Details</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">No</td>
                                    <td><?php echo $objInvestigations->investigation_id;?></td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td><?php echo $objInvestigations->investigation_date;?></td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>5 Why Investigation</td>
                                </tr>
                                <tr>
                                    <td>Investigator</td>
                                    <td><?php echo $objInvestigations->investigator;?></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Source Information</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Type</td>
                                     <td><?php echo $objInvestigations->source_description;?></td>
                                </tr>
                                <tr>
                                    <td width="30%">No</td>
                                     <td><?php echo $objInvestigations->investigation_source_id;?></td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td><?php echo $objInvestigations->source_date;?></td>
                                </tr>
                                <tr>
                                    <td>Group</td>
                                     <td><?php echo $objInvestigations->source_group;?></td>
                                </tr>
                                <tr>
                                    <td>Department</td>
                                      <td><?php echo $objInvestigations->source_dep;?></td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                     <td><?php ?></td>
                                </tr>
                                <tr>
                                    <td>Reporting Person</td>
                                     <td><?php echo $objInvestigations->source_reporting_person;?></td>
                                </tr>
                                <tr>
                                    <td>Non-Conformance</td>
                                     <td><?php echo $objInvestigations->source_details;?></td>
                                </tr>
                                <tr>
                                    <td>Link</td>
                                    <td><a href="<?php echo BASE_URL;?>/index.php?ncid=<?php echo $objInvestigations->investigation_source_id;?>&action=view_non_conformance_details&module=non_conformance">View Details</a></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Involvement</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td>Persons Involved</td>
                                   <td><?php echo $objInvestigations->person_involved;?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Supplier Involved</td>
                                    <td><?php echo $objInvestigations->suppler_involved;?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Resources involved</td>
                                 <td><?php echo $objInvestigations->resources_involved;?></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Images</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Photo</td>
                                    <td><img width="250px" class="img-responsive" src="<?php echo HOSTNAME;?>/frontend/media/uploads/<?php echo $objInvestigations->images;?>" alt="non-conformance image"></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Investigation</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                            <table width="100%" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Why</th>
                                        <th>Type</th>
                                        <th>Description of Non-Conformance</th>
                                        <th>Recommended Improvement</th>
                                        <th>Priority</th>
                                        <th>Action Taken</th>
                                        <th>Reference No</th>
                                        <th>Corrective Action</th>
                                        <th>Improvement</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                 if(isset($data["allFiveWhyDetails"]) && !empty($data["allFiveWhyDetails"]))
                                    {
                                         foreach($data["allFiveWhyDetails"] as $why)
                                         {                           
                                                 ?>
                                                 <tr>
                                                     <td><?php echo $why['why'];?></td>
                                                     <td><?php echo $why['type'];?></td>
                                                     <td><?php echo $why['description'];?></td>
                                                     <td><?php echo $why['recomm'];?></td>
                                                     <td><?php echo $why['priority'];?></td>
                                                     <td><?php echo $why['priority'];?></td>
                                                     <td><?php echo $why['priority'];?></td>
                                                     <td><?php echo $why['priority'];?></td>
                                                     <td><?php echo $why['priority'];?></td> 
                                                 </tr>
                                 <?php
                                         }
                                    }
                                 ?>                
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of panel-default-->
   </div>
</div><!--End of container-fluid-->
<?php 
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  