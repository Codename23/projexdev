
            <div class="col-lg-10">
                <?php
                /*
                 * Include sub menu from the include file
                 */


                ?>

            <!--End of the panel panel-default-->
            <div class="panel panel-default" id="editOccupationForms">
                <div class="panel-heading">Occupation Details</div>
                <div class="panel-body">
                    <form class="form-horizontal" name="occupationForms"  method="post" action="<?php echo BASE_URL; ?>/index.php?module=occupation&action=editOccupation">
                        <?php  foreach($data["editOccupationInfo"] as $occupationInfo): ?>
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="occupationName">Occupations Name</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="occupationName" id="occupationName" placeholder="Enter occupation name" value="<?php echo $occupationInfo['name']; ?>" />
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="hidden" name="id" value="<?php echo $occupationInfo['id']; ?>" />
                                <input type="hidden" name="companyId" value="<?php echo $occupationInfo['company_id']; ?>" />
                                <input type="hidden" name="is_active" value="tab_occupation" />
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </form>
                </div>
            </div>
            <!--End of panel panel-default-->
        </div>

