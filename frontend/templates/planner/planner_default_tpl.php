<?php 
$title = 'Dashboard Planner';
include_once('frontend/templates/headers/default_header_tpl.php');

?>
<div class="container-fluid"> 
<!--navigation--> 
<?php include_once('frontend/templates/menus/main-menu.php'); 
      include_once('frontend/templates/menus/side-menu.php'); ?>

<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li><a>Statistics & Performance</a></li>
            <li class="active"><a href="index.php?module=planner&action=defaultAction">Planner</a></li>
            <li><a>Scheduled</a></li>
            <li><a>Corrective Actions</a></li>
            <li><a>Improvements</a></li>
        </ul>
        </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Search Planner</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Module</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more modules"data-header="Close">
                <option>Training</option>
                <option>Inspection</option>
                <option>PPE</option>
                <option>Health</option>
                <option>Documents</option>
                <option>Audit</option>
            </select>
        </div>
        </div>
        
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Category</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more categories"data-header="Close">
                <option>Category 1</option>
                <option>Category 2</option>
                <option>Category 3</option>
                <option>Category 4</option>
            </select>
        </div>
        </div>
        
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Departments</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>I.T</option>
                <option>Finance</option>
                <option>HR</option>
            </select>
        </div>
        </div>
        
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Responsible Person</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more people"data-header="Close">
                <option>Nick</option>
                <option>Sunny</option>
                <option>Douglas</option>
                <option>Warren</option>
            </select>
        </div>
        </div>
        </div>
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Planner</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Module</th>
                    <th>Category</th>
                    <th>Description</th>
                    <th>Department</th>
                    <th>Due Date</th>
                    <th>Status</th>
                    <th>Responsible Person</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Training</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>15 June 2015 15:00</td>
                    <td></td>
                    <td>Nick Botha</td>
                    <td><a href="#">Go to Module</a></td>
                </tr>
                <tr>
                    <td>Training</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>15 June 2015 15:00</td>
                    <td></td>
                    <td>Nick Botha</td>
                    <td><a href="#">Go to Module</a></td>
                </tr>
                <tr>
                    <td>Training</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>15 June 2015 15:00</td>
                    <td></td>
                    <td>Nick Botha</td>
                    <td><a href="#">Go to Module</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
</div>
</div><!--End of container-fluid-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php'); ?>