<?php
/*
 * Header file
 */
$title = "Add Service Provider";
include_once('frontend/templates/headers/default_header_tpl.php');
global $objLanguage;
?>
<div class="container-fluid">
    <!--navigation-->
    <?php

    /*
     * Include  main menu from the include file
     */
    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <?php
    /*
     * Include side menu from the include file
     */

    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->
    <div class="col-lg-10">
        <!--sub menu-->
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-justified topbar-menu">
                    <li><a href="index.php?module=provider&action=viewAllProviders">All Service Providers</a></li>
                    <li><a>Upcoming & Active</a></li>
                    <li><a>Schedule</a></li>
                    <li><a>Request Quote</a></li>
                    <li><a>Audit</a></li>
                    <li><a>Non-Conformances</a></li>
                    <li><a>Settings</a></li>
                </ul>
            </div>
        </div>
        <!--End of sub menu-->
        <div class="panel-group">
            <form class="form-horizontal" name="addServiceProviderForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=provider&action=addProvider">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $objLanguage->languageText('NEW_SERVICE_PROVIDER'); ?></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="companyName"><?php echo $objLanguage->languageText('COMPANY_NAME') ;?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="companyName" name="companyName" placeholder="Please provide company name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="ptyNumber"><?php echo $objLanguage->languageText('COMPANY_PTY_NUMBER') ;?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="ptyNumber" name="ptyNumber" placeholder="Please provide PTY number" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="vatNumber"><?php echo $objLanguage->languageText('COMPANY_VAT_NUMBER') ;?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="vatNumber" name="vatNumber" placeholder="Please provide VAT number" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="officeNumber"><?php echo $objLanguage->languageText('OFFICE_NUMBER') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="officeNumber" name="officeNumber" placeholder="Please provide office contact number" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="contactPerson"><?php echo $objLanguage->languageText('CONTACT_PERSON') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="contactPerson" name="contactPerson" placeholder="Please enter name and surname" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="contactNumber"><?php echo $objLanguage->languageText('CONTACT_NUMBER') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="contactNumber" name="contactNumber" placeholder="Please provide your contact number"  required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email"><?php echo $objLanguage->languageText('EMAIL') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="email" class="form-control" id="email" name="email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="website"><?php echo $objLanguage->languageText('WEBSITE') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="url" class="form-control" id="website" name="website" placeholder="Please enter your company website address"  required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="beeCertificate"><?php echo $objLanguage->languageText('BEE_CERTIFICATE') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="file" id="beeCertificate" name="beeCertificate">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="headOfficeDetails"><?php echo $objLanguage->languageText('RSA_HEAD_OFFICE_DETAILS') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="headOfficeDetails" name="headOfficeDetails" placeholder="Please provide your street address"  required>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="control-label col-sm-4"></span>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="headOfficeTown" name="headOfficeTown" placeholder="Please provide your town"  required>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="control-label col-sm-4"></span>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select name="headOfficeProvince" class="form-control" id="headOfficeProvince">
                                    <option>Western Cape</option>
                                    <option>Northern Cape</option>
                                    <option>Gauteng</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End of the panel panel-default-->
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $objLanguage->languageText('SERVICE_DETAILS') ; ?></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="serviceType"><?php echo $objLanguage->languageText('SERVICE_TYPE');?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select name="serviceType" class="form-control" id="serviceType">

                                    <?php if(count($data['serviceTypes'])) : foreach($data['serviceTypes'] as $serviceType): ?>
                                    <option value="<?php echo $serviceType['id']; ?>"><?php echo $serviceType['service_name']; ?></option>
                                  <?php endforeach;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="coreBusinessDescription"><?php echo $objLanguage->languageText('CORE_BUSINESS_DESCRIPTION') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="providerDetails" name="providerDetails">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="serviceFrequency"><?php echo $objLanguage->languageText('SERVICE_FREQUENCY') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select name="serviceFrequency" class="form-control" id="serviceFrequency">
                                    <option>Once Off</option>
                                    <option>As Required</option>
                                    <option>Daily</option>
                                    <option>Weekly</option>
                                    <option>Monthly</option>
                                    <option>Quarterly</option>
                                    <option>Biannually</option>
                                    <option>Annually</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="uploadDocuments"><?php echo $objLanguage->languageText('UPLOAD_SUPPORTING_DOCUMENTS') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="file" id="uploadDocuments" name="uploadDocuments">
                            </div>
                        </div>
                    </div>
                </div>
                <!--End of the panel panel-default-->
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $objLanguage->languageText('COMPLIANCE') ; ?></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="complianceConstruction"><?php echo $objLanguage->languageText('CONSTRUCTION') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <label class="radio-inline"><input type="radio" name="complianceConstruction" id="complianceConstruction1" value="0" required>Yes</label>
                                <label class="radio-inline"><input type="radio" name="complianceConstruction" id="complianceConstruction2" value="1" >No</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="complianceHighRisk"><?php echo $objLanguage->languageText('HIGH_RISK') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <label class="radio-inline"><input type="radio" name="complianceHighRisk" id="complianceHighRisk1" value="0" required>Yes</label>
                                <label class="radio-inline"><input type="radio" name="complianceHighRisk" id="complianceHighRisk2" value="1" >No</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="complianceDescription"><?php echo $objLanguage->languageText('DESCRIPTION') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <textarea class="form-control" rows="5" id="complianceDescription" name="complianceDescription"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End of the panel panel-default-->
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $objLanguage->languageText('SERVICE_PROVIDER_AUDIT') ; ?></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="applicableServiceProvider"><?php echo $objLanguage->languageText('WILL_APPLICABLE_SERVICE_PROVIDER_BE_AUDITED'); ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <label class="radio-inline"><input type="radio" name="applicableServiceProvider" id="applicableServiceProvider1" value="0" required>Yes</label>
                                <label class="radio-inline"><input type="radio" name="applicableServiceProvider" id="applicableServiceProvider2" value="1" >No</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="auditType"><?php echo $objLanguage->languageText('AUDIT_TYPE') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <select name="auditType" class="form-control" id="auditType">
                                    <option>Food Safety</option>
                                    <option>Quality</option>
                                    <option>Safety</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="auditFrequency"><?php echo $objLanguage->languageText('AUDIT_FREQUENCY') ; ?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="auditFrequency" name="auditFrequency">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="nextAuditMonth"><?php echo $objLanguage->languageText('NEXT_AUDIT_MONTH') ;?></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="nextAuditMonth" name="nextAuditMonth">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-success"><?php echo $objLanguage->languageText('ADD_SERVICE_PROVIDER'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End of the panel panel-default-->
            </form>
        </div>
        <!--End of the panel-group-->
    </div>
</div><!--End of row-->
</div><!--End of container-fluid-->
<?php
/*
 *
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
