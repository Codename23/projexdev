<?php
/*
 * Header file
 */
$title = "Service Provider";
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */
    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <?php
    /*
     * Include side menu from the include file
     */

    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->
    <div class="col-lg-10">
        <!--sub menu-->
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-justified topbar-menu">
                    <li class="active"><a href="index.php?module=provider&action=viewAllProviders">All Service Providers</a></li>
                    <li><a>Upcoming & Active</a></li>
                    <li><a>Schedule</a></li>
                    <li><a>Request Quote</a></li>
                    <li><a>Audit</a></li>
                    <li><a>Non-Conformances</a></li>
                    <li><a>Settings</a></li>
                </ul>
            </div>
        </div>
        <!--End of sub menu-->
        <!--
        <div class="panel panel-default">
            <div class="panel-body">
                <button type="submit" onclick="window.location.href='service_provider_add.php'" id="addServiceProvider" class="btn btn-success">Add Service Provider</button>
            </div>
        </div>
        -->
        <div class="panel panel-default">
            <div class="panel-body">
                <form class="form-inline" method="post" action="add_employees.php">
                    <div class="form-group">
                        <a href="<?php echo BASE_URL . "/index.php?module=provider&action=addProvider"; ?>" id="addServiceProvider" class="btn btn-success">Add Service Provider</a>
                    </div>
                </form>
            </div>
        </div>
        <!--End of panel panel-default-->

        <div class="panel panel-default">
            <div class="panel-heading">Service Provider Search</div>
            <div class="panel-body">
                <form class="form-inline" method="post" name="search" action="">
                    <div class="form-group">
                        <label for="companyName">Name </label>
                        <input type="text" class="form-control" id="companyName" placeholder="Search company name">
                    </div>
                    <div class="form-group">
                        <label for="contractorType">Type </label>
                        <input type="text" class="form-control" id="contractorType" placeholder="Search contractor type">
                    </div>
                    <div class="form-group">
                        <label for="operatingArea">Operating Area </label>
                        <select class="form-control" name="operatingArea">
                            <option>Western Cape</option>
                            <option>Northern Cape</option>
                            <option>Gauteng</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Search">
                    </div>
                </form>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Suppliers</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>


                        <tr>
                            <th>#</th>
                            <th>Company Name</th>
                            <th>Operational Area</th>
                            <th>Contractor Type</th>
                            <th>Approval</th>
                            <th>Rating</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if($data['serviceProviders']) : foreach ($data['serviceProviders'] as $serviceProvider): ?>
                        <tr>
                            <td><?php echo $serviceProvider['id']; ?></td>
                            <td><a href="<?php echo BASE_URL . "/index.php?module=provider&action=viewProviderInfo&id=" . $serviceProvider['id'] ; ?>"><?php echo $serviceProvider['provider_name']; ?></a></td>
                            <td><?php echo $serviceProvider['province']; ?></td>
                            <td>Emergency Equipment</td>
                            <td>Approved</td>
                            <td>5.5</td>
                            <td><a href="<?php echo BASE_URL . "/index.php?module=provider&action=viewProviderInfo&id=" . $serviceProvider['id'] ; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
                            <td class="danger"><a href="<?php echo BASE_URL . "/index.php?module=provider&action=deleteProvider&id=" .$serviceProvider['id']; ?>"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>
                        <?php endforeach;endif; ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--End of the panel panel-default-->

    </div>
</div><!--End of row-->
</div><!--End of container-fluid-->
<?php
/*
 *
 */

include_once('frontend/templates/footers/default_footer_tpl.php');
?>
