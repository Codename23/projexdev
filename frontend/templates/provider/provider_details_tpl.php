<?php
$title = 'Company Information';
global $objLanguage;
include_once('frontend/templates/headers/default_header_tpl.php');
?>

<div class="container-fluid">
    <!--navigation-->
    <?php

    /*
     * Include  main menu from the include file
     */

    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <!--End of navigation-->

    <?php
    /*
     * Include side menu from the include file
     */

    include_once('frontend/templates/menus/side-menu.php');
    ?>

    <div class="col-lg-10">
        <?php
        /*
         * Include sub menu from the include file
         */

        //include_once('frontend/templates/menus/sub-menu.php');
        ?>
        <div class="row company">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                     <h2><span class="glyphicon glyphicon-user" aria-hidden="true"></span><?php echo $data['providerInfo']['provider_name']; ?></h2>
                    </div>
                </div>
                <!--BEGIN TABS-->
                <div class="tabbable tabbable-custom tabbable-full-width">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#provider_details" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('SERVICE_PROVIDER_DETAILS') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#provider_schedule" data-toggle="tab">


                            </a>
                        </li>
                        <li>
                            <a href="#provider_quote" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('REQUEST_QUOTATION') ;?>

                            </a>
                        </li>
                        <li>
                            <a href="#provider_compliance" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('SHEQ_COMPLIANCE') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#provider_audit" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('AUDIT') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#provider_conformance" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('NON_CONFORMANCE') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#provider_history" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('HISTORY') ;?>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="provider_details">

                            <div class="panel-group">
                                <form class="form-horizontal" name="addSupplierForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=provider&action=editProviderInfo">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php  echo $objLanguage->languageText('SERVICE_PROVIDER_DETAILS') ;?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="companyName"><?php echo $objLanguage->languageText('COMPANY_NAME') ;?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="companyName" name="companyName" value="<?php echo $data['providerInfo']['provider_name']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="ptyNumber"><?php echo $objLanguage->languageText('COMPANY_PTY_NUMBER') ;?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="ptyNumber" name="ptyNumber"  value="<?php echo $data['providerInfo']['pty_number']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="vatNumber"><?php echo $objLanguage->languageText('COMPANY_VAT_NUMBER') ;?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="vatNumber" name="vatNumber" value="<?php echo $data['providerInfo']['vat_number']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="officeNumber"><?php echo $objLanguage->languageText('OFFICE_NUMBER') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="officeNumber" name="officeNumber" value="<?php echo $data['providerInfo']['office_number']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="contactPerson"><?php echo $objLanguage->languageText('CONTACT_PERSON') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="contactPerson"  name="contactPerson" value="<?php echo $data['providerInfo']['contact_person']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="contactNumber"><?php echo $objLanguage->languageText('CONTACT_NUMBER') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="contactNumber" name="contactNumber" value="<?php echo $data['providerInfo']['cell_number']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="email"><?php echo $objLanguage->languageText('EMAIL') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="email" class="form-control" id="email" name="email" value="<?php echo $data['providerInfo']['provider_email']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="website"><?php echo $objLanguage->languageText('WEBSITE') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="url" class="form-control" id="website"  name="website" value="<?php echo $data['providerInfo']['website']; ?>" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="beeCertificate"><?php echo $objLanguage->languageText('BEE_CERTIFICATE') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <button class="btn btn-success">View</button>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="headOfficeDetails"><?php echo $objLanguage->languageText('RSA_HEAD_OFFICE_DETAILS') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                <textarea row="30" cols="10" class="form-control" id="headOfficeDetails" name="headOfficeDetails"><?php echo $data['providerInfo']['provider_address']; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <span class="control-label col-sm-4"></span>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="headOfficeTown" name="headOfficeTown" value="<?php echo $data['providerInfo']['provider_town']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <span class="control-label col-sm-4"></span>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <select name="headOfficeProvince" class="form-control" id="headOfficeProvince">
                                                        <option>Western Cape</option>
                                                        <option>Northern Cape</option>
                                                        <option>Gauteng</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--End of the panel panel-default-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $objLanguage->languageText('SERVICE_DETAILS') ; ?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="serviceType"><?php echo $objLanguage->languageText('SERVICE_TYPE');?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <select name="serviceType" class="form-control" id="serviceType">

                                                        <?php if(count($data['serviceTypes'])) : foreach($data['serviceTypes'] as $serviceType): ?>
                                                            <?php if($serviceType['id'] == $data['providerInfo']['service_type']): ?>
                                                            <option value="<?php echo $serviceType['id']; ?>" selected><?php echo $serviceType['service_name']; ?></option>
                                                                <?php else: ?>
                                                                <option value="<?php echo $serviceType['id']; ?>"><?php echo $serviceType['service_name']; ?></option>
                                                                <?php endif; ?>
                                                        <?php endforeach;endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="coreBusiness"><?php echo $objLanguage->languageText('CORE_BUSINESS_DESCRIPTION') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <textarea row="30" cols="10" class="form-control" id="providerDetails" name="providerDetails"><?php echo $data['providerInfo']['business_description']; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="serviceFrequency"><?php echo $objLanguage->languageText('SERVICE_FREQUENCY') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <select name="serviceFrequency" class="form-control" id="serviceFrequency">
                                                        <option>Once Off</option>
                                                        <option>As Required</option>
                                                        <option>Daily</option>
                                                        <option>Weekly</option>
                                                        <option>Monthly</option>
                                                        <option>Quarterly</option>
                                                        <option>Biannually</option>
                                                        <option>Annually</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="uploadDocuments"><?php echo $objLanguage->languageText('UPLOAD_SUPPORTING_DOCUMENTS') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="file" id="uploadDocuments" name="uploadDocuments">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--End of the panel panel-default-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $objLanguage->languageText('COMPLIANCE') ; ?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="complianceConstruction"><?php echo $objLanguage->languageText('CONSTRUCTION') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <label class="radio-inline"><input type="radio" name="complianceConstruction" id="complianceConstruction1" <?php echo ($data['providerInfo']['is_construction'] == 1) ? 'checked' : ''; ?> value="1" required>Yes</label>
                                                    <label class="radio-inline"><input type="radio" name="complianceConstruction" id="complianceConstruction2" <?php echo ($data['providerInfo']['is_construction'] == 0) ? 'checked' : ''; ?> value="0" >No</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="complianceHighRisk"><?php echo $objLanguage->languageText('HIGH_RISK') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <label class="radio-inline"><input type="radio" name="complianceHighRisk" id="complianceHighRisk1" <?php echo ($data['providerInfo']['is_high_risk'] == 1) ? 'checked' : ''; ?> value="1"  required>Yes</label>
                                                    <label class="radio-inline"><input type="radio" name="complianceHighRisk" id="complianceHighRisk2" <?php echo ($data['providerInfo']['is_high_risk'] == 0) ? 'checked' : ''; ?> value="0" >No</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="complianceDescription"><?php echo $objLanguage->languageText('DESCRIPTION') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <textarea class="form-control" rows="5" id="complianceDescription" name="complianceDescription"><?php echo $data['providerInfo']['compliance_description'];?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--End of the panel panel-default-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $objLanguage->languageText('SERVICE_PROVIDER_AUDIT') ; ?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="applicableServiceProvider"><?php echo $objLanguage->languageText('WILL_APPLICABLE_SERVICE_PROVIDER_BE_AUDITED'); ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <label class="radio-inline"><input type="radio" name="applicableServiceProvider" id="applicableServiceProvider1" <?php echo ($data['providerInfo']['is_audited'] == 1) ? 'checked' : ''; ?> value="1" required>Yes</label>
                                                    <label class="radio-inline"><input type="radio" name="applicableServiceProvider" id="applicableServiceProvider2" <?php echo ($data['providerInfo']['is_audited'] == 0) ? 'checked' : ''; ?> value="0" >No</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="auditType"><?php echo $objLanguage->languageText('AUDIT_TYPE') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <select name="auditType" class="form-control" id="auditType">
                                                        <option>Food Safety</option>
                                                        <option>Quality</option>
                                                        <option>Safety</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="auditFrequency"><?php echo $objLanguage->languageText('AUDIT_FREQUENCY') ; ?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="auditFrequency" name="auditFrequency" value="Monthly">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="nextAuditMonth"><?php echo $objLanguage->languageText('NEXT_AUDIT_MONTH') ;?></label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="text" class="form-control" id="nextAuditMonth" name="nextAuditMonth" value="15 January 2017">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input type="hidden" name="providerId" value="<?php echo $data['providerInfo']['id']; ?>" />
                                                <div class="col-sm-offset-4 col-sm-8">
                                                    <button type="submit" class="btn btn-success"><?php echo $objLanguage->languageText('UPDATE'); ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--End of the panel panel-default-->
                                </form>
                            </div>
                            <!--End of the panel-group-->

                        </div>
                        <!--tab_1_2-->
                        <div class="tab-pane" id="provider_schedule">
                            <div class="scheduleResults"></div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane" id="provider_quote">
                            <div class="quoteResults"></div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane" id="provider_compliance">
                            <div class="complianceResults"></div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane" id="provider_audit">
                            <div class="auditResults"></div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane" id="provider_conformance">
                            <div class="conformanceResults"></div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane" id="provider_history">
                            <div class="historyResults"></div>
                        </div>
                        <!--end tab-pane-->
                    </div><!--End of row -->
                </div><!--End of container-fluid -->
            </div>

        </div>
    </div>
    <!--END TABS-->
</div>
</div>
<!-- END PAGE CONTENT-->

</div>
</div>
<?php

include_once('frontend/templates/footers/default_footer_tpl.php');
?>

<script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {


        var target = $(e.target).attr("href");

        if ((target == '#provider_schedule')) {
            $.post('<?php echo BASE_URL . "" .$data["providerInfo"]['id']; ?>', {}, function(data){
                $('.scheduleResults').html(data);
            });
        }

        if ((target == '#provider_quote')) {
            $.post('<?php echo BASE_URL . "" .$data["providerInfo"]['id']; ?>', {}, function(data){
                $('.quoteResults').html(data);
            });
        }

    });


</script>