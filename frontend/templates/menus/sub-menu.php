<?php
/*
*This is a side bar menu 
*/

$currentUrl = $_SERVER["PHP_SELF"];
$parts = explode('/', $currentUrl);
$pageName = $parts[count($parts) - 1];
$pageNameLists = array(
    array("company_info.php","Company Informantion"), 
    array("branch.php", "Branches"), 
    array("department.php", "Departments"), 
    array("occupation.php", "Occupations"));

?>
   
<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <?php
            //create menu
            foreach($pageNameLists as $pageUrl){
               if($pageUrl[0]== $pageName){
                   echo '<li class="active"><a href="'. HOSTNAME .'/' . $pageUrl[0] . '">' . $pageUrl[1].'</a></li>';   
               }else{
                   echo '<li><a href="'. HOSTNAME .'/' . $pageUrl[0]. '">' . $pageUrl[1].'</a></li>';            
               }
            }
            ?>
        </ul>
    </div>
</div>
<?php //End of the file ?>     
