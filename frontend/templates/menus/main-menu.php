<!--BEGIN PAGE WRAPPER-->
<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">

        <div class="page-header pull-left">
            <div class="page-title">
                <ul class="nav-header pull-left">
                <li>
                <a id="menu-toggle" href="#"><button class="btn btn-default btn-custom-header" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                <i class="fa fa-ellipsis-v"></i>
                </button></a>
                </li>
                <li>
                <button class="btn btn-default btn-custom-header" data-toggle="modal" data-target="#action-modal" type="button">
                <i class="fa fa-th"></i>
                </button>
                </li>
                </ul>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="action-modal" role="dialog">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header" style="background-color: #2c343f; color: white;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Actions</h4>
              </div>
              <div class="modal-body row">
                <div class="col-lg-12">  
                <div class="col-lg-4 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading dark-blue">
                                <i class="fa fa-thumbs-down fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content dark-blue">
                            <div class="circle-tile-description text-faded">
                                Non-Conformance
                            </div>
                            <div class="circle-tile-number text-faded">
                                265
                                <span id="sparklineA"></span>
                            </div>
                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading green">
                                <i class="fa fa-exclamation fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content green">
                            <div class="circle-tile-description text-faded">
                                Incident
                            </div>
                            <div class="circle-tile-number text-faded">
                                50
                            </div>
                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading orange">
                                <i class="fa fa-bell fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content orange">
                            <div class="circle-tile-description text-faded">
                                Near Miss
                            </div>
                            <div class="circle-tile-number text-faded">
                                19
                            </div>
                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                </div>

                  <div class="col-lg-12">  
                <div class="col-lg-4 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading dark-blue">
                                <i class="fa fa-cubes fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content dark-blue">
                            <div class="circle-tile-description text-faded">
                                Resource
                            </div>
                            <div class="circle-tile-number text-faded">
                                30 New
                                <span id="sparklineA"></span>
                            </div>
                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading green">
                                <i class="fa fa-truck fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content green">
                            <div class="circle-tile-description text-faded">
                                Supplier
                            </div>
                            <div class="circle-tile-number text-faded">
                                500 New
                            </div>
                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading orange">
                                <i class="fa fa-male fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content orange">
                            <div class="circle-tile-description text-faded">
                                Employees
                            </div>
                            <div class="circle-tile-number text-faded">
                                1000
                            </div>
                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                </div>



              </div>

            </div>
          </div>
        </div>

           <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">
           <li class="dropdown_company">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo (isset($_SESSION['defaultCompany'])) ? $_SESSION['defaultCompany']: 'Select Company'; ?><span class="caret"></span></a>
                  <?php
                  global $objUsers;
                  global $objCompanies;
                  // all companies assigned to logged user
                  $companies = $objUsers->getUserCompanies($_SESSION['user_id']);
                  if(!empty($companies)){
                  if(count($companies)):
                  ?>
                  <ul class="dropdown-menu user-companies">
                       <?php 
                       
                        foreach($companies as $company): ?>
                               <li><a href="#" data-value="<?php echo $company['id']; ?>"><?php echo $company['company_name']; ?></a></li>
                        <?php endforeach;  
                       ?>
                  </ul>
                  <?php endif; 
                  } ?>
             
           </li>
           <?php if(isset($_SESSION['company_id'])): ?>
                  <?php  $companyBranch = $objCompanies->getCompanyBranches($_SESSION['company_id']); ?>
                    <?php if($companyBranch): ?>
                  <li class="user-branch">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo (isset($_SESSION['defaultBranch'])) ? $_SESSION['defaultBranch']:'Branch' ; ?><span class="caret"></span></a>
                      <ul class="dropdown-menu user-allowed-branch">
                          <?php  foreach($companyBranch as $branch): ?>
                              <?php if($branch['id'] == $_SESSION['branch_id']) :?>
                                <li class="active"><a href="#" data-value="<?php echo $branch['id']; ?>"><?php echo $branch['branch_name']; ?></a></li>
                              <?php else: ?>
                                  <li><a href="#" data-value="<?php echo $branch['id']; ?>" ><?php echo $branch['branch_name']; ?></a></li>

                                  <?php endif;?>
                          <?php endforeach;  ?>
                      </ul>
                  </li>
              <?php endif; ?>
          <?php endif; ?>
           <li class="dropdown">                          
               <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                   <span class="thumb-sm avatar pull-left"> <img class="thumbnail-img" src="images/images.jpeg" alt="..."> </span> <?php echo $_SESSION['firstname'] . ' '. $_SESSION['lastname']; ?> <b class="caret"></b> 
               </a> 
               <ul id="dropdown-menu" class="dropdown-menu dropdown-lg animated fadeInRight"> 
                   <li> 
                       <div style="float:left; padding-left: 1px;" class="col-md-4"><img class="dropdown-img" src="images/images.jpeg" alt="..."></div>
                       <div style="float:left;" class="col-md-8"><span><p>
                              <?php echo $_SESSION['firstname'] . ' '. $_SESSION['lastname']; ?>
                               </p><p>
                                   
                               </p><p><a href="#">Edit Profile</a></p></span></div>
                           <span class="arrow2 top"></span></li> 
                   <li> <a href="#">Settings</a> </li> 
                   <li> <a href="#">Profile</a> </li> 
                   <li><a class="notification_popup_clicked" href="#notificationDisplay-modal" data-toggle="modal" data-target="#notificationDisplay-modal"><span class="glyphicon glyphicon-globe"></span> Notifications <span class="badge bg-danger pull-right">3</span></a></li>
                   <li> <a href="#">Help</a> </li> 
                   <li class="divider"></li> 
                   <li><a href="index.php?action=logout&module=user">LogOut</a></li>
               </ul> 
           </li>
           </ul>

           <!--<div id="header-nav-left">
           <a class="header-btn" id="logout-btn" href="lockscreen-3.html" title="Lockscreen page example"><i class="glyph-icon icon-linecons-lock"></i></a><div class="user-account-btn dropdown"><a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown" aria-expanded="false"><img width="28" src="images/images.jpeg" alt="Profile image"> <span>Sarah Lee</span> <i class="glyph-icon icon-angle-down"></i></a><div class="dropdown-menu float-right" style="display: none;"><div class="box-sm"><div class="login-box clearfix"><div class="user-img"><a href="#" title="" class="change-img">Change photo</a> <img src="../../assets/image-resources/gravatar.jpg" alt=""></div><div class="user-info"><span>Michael Lee <i>UX/UI developer</i></span> <a href="#" title="Edit profile">Edit profile</a> <a href="#" title="View notifications">View notifications</a></div></div><div class="divider"></div><ul class="reset-ul mrg5B"><li><a href="#">View login page example <i class="glyph-icon float-right icon-caret-right"></i></a></li><li><a href="#">View lockscreen example <i class="glyph-icon float-right icon-caret-right"></i></a></li><li><a href="#">View account details <i class="glyph-icon float-right icon-caret-right"></i></a></li></ul><div class="button-pane button-pane-alt pad5L pad5R text-center"><a href="#" class="btn btn-flat display-block font-normal btn-danger"><i class="glyph-icon icon-power-off"></i> Logout</a></div></div></div></div></div>-->

        <!--<ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="hidden"><a href="#">Dashboard</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Dashboard</li>
        </ol>-->
        <div class="clearfix">
        </div>
    </div>

    <!--END TITLE & BREADCRUMB PAGE-->
    
    <!-- major modals -->
    <div class="modal fade" id="notificationDisplay-modal" role="dialog">
    <div class="modal-dialog" style="width:90% !important;">
        <!-- Modal content-->
        <div class="modal-content">
            <style>
	.mail-box {
    border-collapse: collapse;
    border-spacing: 0;
    display: table;
    table-layout: fixed;
    width: 100%;
}
.mail-box aside {
    display: table-cell;
    float: none;
    height: 100%;
    padding: 0;
    vertical-align: top;
}
.mail-box .sm-side {
    background: none repeat scroll 0 0 #e5e8ef;
    border-radius: 4px 0 0 4px;
    width: 25%;
}
.mail-box .lg-side {
    background: none repeat scroll 0 0 #fff;
    border-radius: 0 4px 4px 0;
    width: 75%;
}
.mail-box .sm-side .user-head {
    background: none repeat scroll 0 0 #00a8b3;
    border-radius: 4px 0 0;
    color: #fff;
    min-height: 80px;
    padding: 10px;
}
.user-head .inbox-avatar {
    float: left;
    width: 65px;
}
.user-head .inbox-avatar img {
    border-radius: 4px;
}
.user-head .user-name {
    display: inline-block;
    margin: 0 0 0 10px;
}
.user-head .user-name h5 {
    font-size: 14px;
    font-weight: 300;
    margin-bottom: 0;
    margin-top: 15px;
}
.user-head .user-name h5 a {
    color: #fff;
}
.user-head .user-name span a {
    color: #87e2e7;
    font-size: 12px;
}
a.mail-dropdown {
    background: none repeat scroll 0 0 #80d3d9;
    border-radius: 2px;
    color: #01a7b3;
    font-size: 10px;
    margin-top: 20px;
    padding: 3px 5px;
}
.inbox-body {
    padding: 20px;
}
.btn-compose {
    background: none repeat scroll 0 0 #ff6c60;
    color: #fff;
    padding: 12px 0;
    text-align: center;
    width: 100%;
}
.btn-compose:hover {
    background: none repeat scroll 0 0 #f5675c;
    color: #fff;
}
ul.inbox-nav {
    display: inline-block;
    margin: 0;
    padding: 0;
    width: 100%;
}
.inbox-divider {
    border-bottom: 1px solid #d5d8df;
}
ul.inbox-nav li {
    display: inline-block;
    line-height: 45px;
    width: 100%;
}
ul.inbox-nav li a {
    color: #6a6a6a;
    display: inline-block;
    line-height: 45px;
    padding: 0 20px;
    width: 100%;
}
ul.inbox-nav li a:hover, ul.inbox-nav li.active a, ul.inbox-nav li a:focus {
    background: none repeat scroll 0 0 #d5d7de;
    color: #6a6a6a;
}
ul.inbox-nav li a i {
    color: #6a6a6a;
    font-size: 16px;
    padding-right: 10px;
}
ul.inbox-nav li a span.label {
    margin-top: 13px;
}
ul.labels-info li h4 {
    color: #5c5c5e;
    font-size: 13px;
    padding-left: 15px;
    padding-right: 15px;
    padding-top: 5px;
    text-transform: uppercase;
}
ul.labels-info li {
    margin: 0;
}
ul.labels-info li a {
    border-radius: 0;
    color: #6a6a6a;
}
ul.labels-info li a:hover, ul.labels-info li a:focus {
    background: none repeat scroll 0 0 #d5d7de;
    color: #6a6a6a;
}
ul.labels-info li a i {
    padding-right: 10px;
}
.nav.nav-pills.nav-stacked.labels-info p {
    color: #9d9f9e;
    font-size: 11px;
    margin-bottom: 0;
    padding: 0 22px;
}
.inbox-head {
    background: none repeat scroll 0 0 #0a05ff;
    border-radius: 0 4px 0 0;
    color: #fff;
    min-height: 80px;
    padding: 20px;
}
.inbox-head h3 {
    display: inline-block;
    font-weight: 300;
    margin: 0;
    padding-top: 6px;
}
.inbox-head .sr-input {
    border: medium none;
    border-radius: 4px 0 0 4px;
    box-shadow: none;
    color: #8a8a8a;
    float: left;
    height: 40px;
    padding: 0 10px;
}
.inbox-head .sr-btn {
    background: none repeat scroll 0 0 #00a6b2;
    border: medium none;
    border-radius: 0 4px 4px 0;
    color: #fff;
    height: 40px;
    padding: 0 20px;
}
.table-inbox {
    border: 1px solid #d3d3d3;
    margin-bottom: 0;
}
.table-inbox tr td {
    padding: 12px !important;
}
.table-inbox tr td:hover {
    cursor: pointer;
}
.table-inbox tr td .fa-star.inbox-started, .table-inbox tr td .fa-star:hover {
    color: #f78a09;
}
.table-inbox tr td .fa-star {
    color: #d5d5d5;
}
.table-inbox tr.unread td {
    background: none repeat scroll 0 0 #f7f7f7;
    font-weight: 600;
}
ul.inbox-pagination {
    float: right;
}
ul.inbox-pagination li {
    float: left;
}
.mail-option {
    display: inline-block;
    margin-bottom: 10px;
    width: 100%;
}
.mail-option .chk-all, .mail-option .btn-group {
    margin-right: 5px;
}
.mail-option .chk-all, .mail-option .btn-group a.btn {
    background: none repeat scroll 0 0 #fcfcfc;
    border: 1px solid #e7e7e7;
    border-radius: 3px !important;
    color: #afafaf;
    display: inline-block;
    padding: 5px 10px;
}
.inbox-pagination a.np-btn {
    background: none repeat scroll 0 0 #fcfcfc;
    border: 1px solid #e7e7e7;
    border-radius: 3px !important;
    color: #afafaf;
    display: inline-block;
    padding: 5px 15px;
}
.mail-option .chk-all input[type="checkbox"] {
    margin-top: 0;
}
.mail-option .btn-group a.all {
    border: medium none;
    padding: 0;
}
.inbox-pagination a.np-btn {
    margin-left: 5px;
}
.inbox-pagination li span {
    display: inline-block;
    margin-right: 5px;
    margin-top: 7px;
}
.fileinput-button {
    background: none repeat scroll 0 0 #eeeeee;
    border: 1px solid #e6e6e6;
}
.inbox-body .modal .modal-body input, .inbox-body .modal .modal-body textarea {
    border: 1px solid #e6e6e6;
    box-shadow: none;
}
.btn-send, .btn-send:hover {
    background: none repeat scroll 0 0 #00a8b3;
    color: #fff;
}
.btn-send:hover {
    background: none repeat scroll 0 0 #009da7;
}
.modal-header h4.modal-title {
    font-family: 'Open Sans', sans-serif !important;
    font-weight: 300;
}
.modal-body label {
    font-family: 'Open Sans', sans-serif !important;
    font-weight: 400;
}
.heading-inbox h4 {
    border-bottom: 1px solid #ddd;
    color: #444;
    font-size: 18px;
    margin-top: 20px;
    padding-bottom: 10px;
}
.sender-info {
    margin-bottom: 20px;
}
.sender-info img {
    height: 30px;
    width: 30px;
}
.sender-dropdown {
    background: none repeat scroll 0 0 #eaeaea;
    color: #777;
    font-size: 10px;
    padding: 0 3px;
}

            </style>
        <div class="modal-body" style="padding:0px;">
            <form class="form-horizontal" method="post">
                <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
 <div class="mail-box">
                  <aside class="sm-side" style="margin:0px;">
                      <ul class="inbox-nav inbox-divider">
                          <li class="active loader_item" id="inbox">
                              <a href="#"><i class="fa fa-inbox"></i> Inbox <span class="label label-primary pull-right inbox_total"></span></a>
                          </li>
                          <li class="loader_item" id="outbox">
                              <a href="#"><i class="fa fa-envelope-o"></i> Sent Request <span class="label label-primary pull-right outbox_total"></span></a>
                          </li>
                          <li class="loader_item" id="trash">
                              <a href="#"><i class=" fa fa-trash-o"></i> Trash <span class="label label-primary pull-right trash_total"></span></a>
                          </li>
                      </ul>
                      <ul class="nav nav-pills nav-stacked labels-info inbox-divider">
                          <li> <h4>Work Space</h4> </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-danger"></i> Non-Conformance </a> </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-success"></i> Incident </a> </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-info "></i> Corrective Action </a>
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-warning "></i> Maintenance </a>
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-primary "></i> Investigation </a>
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-primary "></i> Improvement </a>
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-primary "></i> Online Inspection </a>
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-primary "></i> Objective </a>
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-primary "></i> Training </a>    
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-primary "></i> Communication </a>    
                          </li>
                      </ul>
                      <div class="inbox-body text-center" style="display:none;">
                          <div class="btn-group">
                              <a class="btn mini btn-primary" href="javascript:;">
                                  <i class="fa fa-plus"></i>
                              </a>
                          </div>
                          <div class="btn-group">
                              <a class="btn mini btn-success" href="javascript:;">
                                  <i class="fa fa-phone"></i>
                              </a>
                          </div>
                          <div class="btn-group">
                              <a class="btn mini btn-info" href="javascript:;">
                                  <i class="fa fa-cog"></i>
                              </a>
                          </div>
                      </div>

                  </aside>
                  <aside class="lg-side" style="margin:0px;">
                      <div class="inbox-body mail_body" style="padding:0px;">
                                                    
                      </div>
                  </aside>
       </div>
     </form>
            
</div>
            
        </div>
        </div>
    </div>
    <script>
</script>