<?php
/*
*
*This is a side bar menu 
*
*/

$currentUrl = $_SERVER["PHP_SELF"];
$parts = explode('/', $currentUrl);
$pageUrl = $parts[count($parts) - 1];

$menu = array(
        array('text'=>'Company Info','url'=>'company_info.php'), 
        array('text'=>'Suppliers', 'url'=>'supplier.php'),
        array('text'=>'Asset', 'url'=>'asset.php'),
        array('text'=>'SHEQ Standards', 'url'=>'sheqstandard.php'),
        array('text'=>'SHEQ Teams', 'url'=>'sheqteams.php'),
        array('text'=>'Committees', 'url'=>'committees.php'),
        array('text'=>'Legal', 'url'=>'legal.php'));

$company = array('branch.php','department.php','occupation.php');

$employee = array('employee_info.php',
                'add_employees.php',
                'employees_sheqteam.php',
                'employees_emergency.php',
                'employees_occupation.php',
                'employees_training_competence.php',
                'employees_health.php',
                'employees_ppe.php',
                'employees_performance.php');

$suppliers = array('supplier_add.php',
                    'supplier_request_quote.php',
                    'supplier_schedule.php',
                    'supplier_history.php',
                    'supplier_sheq_compliance.php');

$asset = array('asset_add.php',
                'asset_info.php',
                'asset_info.php',
                'asset_sheq.php',
                'asset_sheq_healthandsafety.php',
                'asset_sheq_environmental.php',
                'asset_sheq_quality.php',
                'asset_statutory_history.php',
                'asset_maintenance_record.php');

$committees = array('committees_schedulemeeting.php','committees_create_agenda.php');
$legal = array('');

?>
<div class="row">
<div class="col-lg-2">
    <ul class="nav nav-pills nav-stacked side-bar">
        <?php
      //create menu
        $class = ''; 
        
        if($pageUrl == 'workspace.php'){
            echo '<li class="active" ><a href="'. HOSTNAME .'/workspace.php">Dashboard</a></li>';  
        }
        
        foreach($menu as $item){ 
            if((in_array($pageUrl, $company, true)) &&($item['url']=='company_info.php')) {
               $class = 'class="active"';   
            }
            if((in_array($pageUrl, $employee, true)) &&($item['url']=='employees.php')) {
               $class = 'class="active"';   
            }
            if((in_array($pageUrl, $suppliers, true)) &&($item['url']=='supplier.php')) {
               $class = 'class="active"';   
            }
            if((in_array($pageUrl, $asset, true)) &&($item['url']=='asset.php')) {
               $class = 'class="active"';   
            }
            if((in_array($pageUrl, $committees, true)) &&($item['url']=='committees.php')) {
               $class = 'class="active"';   
            }
            
            if($item['url']== $pageUrl){
               $class = 'class="active"';
            }

            echo '<li ' . $class. ' ><a href="'. HOSTNAME .'/' . $item['url'] . '">' . $item['text'].'</a></li>';  
            
            $class = '';
        }
       
        ?>
    </ul>
</div>
<?php //End of the file ?>     
