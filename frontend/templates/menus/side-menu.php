    <div>
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        
        <div id="wrapper"><?php
/**
 *
 *This is a side bar menu
 *
 */

$currentUrl = $_SERVER["PHP_SELF"];
$parts = explode('/', $currentUrl);
$pageUrl = $parts[count($parts) - 1];

$menu = array(
    array('text'=>'Company Info','url'=>'index.php?module=companies&action=viewAllCompanies'),
    array('text'=>'System Users', 'url'=>'index.php?action=view_system_users&module=user'),
    array('text'=>'Process', 'url'=>'index.php?module=process&action=defaultAction'),
    array('text'=>'Critical Control Point', 'url'=>'index.php?module=critical_control&action=critical_control_point'),
    array('text'=>'Statutory', 'url'=>'index.php?module=assets&action=statutory_maintenance_plan'),
    array('text'=>'Maintenance Plan', 'url'=>'index.php?action=maintenance_view&module=maintenance'),
    array('text'=>'Active Maintenance', 'url'=>'index.php?action=view_requested_maintenance&module=maintenance'),
    array('text'=>'Online Inspection', 'url'=>'index.php?module=online_inspection&action=inspection_online'),
    array('text'=>'Employee', 'url'=>'index.php?module=employee&action=viewAllEmployees'),
    array('text'=>'Suppliers/Clients', 'url'=>'index.php?module=supplier_client&action=viewDashboard'),
    array('text'=>'Resources', 'url'=>'index.php?module=assets&action=assetmanagement'),
    array('text'=>'Material', 'url'=>'index.php?action=defaultAction&module=material'),
    array('text'=>'Document Control', 'url'=>'index.php?action=defaultAction&module=documents'),
    array('text'=>'SHEQ Teams', 'url'=>'index.php?action=viewAllSheqTeamMembers&module=sheq_teams'),
    array('text'=>'Committees', 'url'=>'index.php?action=defaultAction&module=default'),
    array('text'=>'Legal', 'url'=>'index.php?action=defaultAction&module=default'),
    array('text'=>'Training Management', 'url'=>'index.php?action=defaultAction&module=default'),
    array('text'=>'Waste Management', 'url'=>'index.php?action=defaultAction&module=default'),
    array('text'=>'Behaviour', 'url'=>'index.php?action=defaultAction&module=default'),
    array('text'=>'Inspections', 'url'=>'index.php?action=defaultAction&module=default'),
    array('text'=>'Risk Assessment', 'url'=>'index.php?module=risk_assessment&action=viewRiskAssessmentDashboard'),
    array('text'=>'Incident Management','url'=>'index.php?module=incidents&action=incidentManagement'),
    array('text'=>'Audit', 'url'=>'index.php?action=defaultAction&module=default'),
    array('text'=>'Task Manager', 'url'=>'index.php?action=defaultAction&module=default'),
    array('text'=>'SHEQ Plan', 'url'=>'index.php?action=defaultAction&module=default'),
    array('text'=>'SHEQ Objectives', 'url'=>'index.php?action=defaultAction&module=default'),
    array('text'=>'Communication', 'url'=>'index.php?action=defaultAction&module=default'),
    array('text'=>'Health', 'url'=>'index.php?action=defaultAction&module=default'),
    array('text'=>'PPE Management', 'url'=>'index.php?action=defaultAction&module=default'));
    

$company = array('branch.php','department.php','occupation.php');

$employee = array('index.php?action=defaultAction&module=default',
    'index.php?action=defaultAction&module=default',
    'index.php?action=defaultAction&module=defaultp',
    'index.php?action=defaultAction&module=default',
    'index.php?action=defaultAction&module=default',
    'index.php?action=defaultAction&module=default',
    'index.php?action=defaultAction&module=defaultp',
    'index.php?action=defaultAction&module=default',
    'index.php?action=defaultAction&module=default');

$suppliers = array('supplier_add.php',
    'supplier_request_quote.php',
    'supplier_schedule.php',
    'supplier_history.php',
    'supplier_sheq_compliance.php');

$asset = array('asset_add.php',
    'asset_info.php',
    'asset_info.php',
    'asset_sheq.php',
    'asset_sheq_healthandsafety.php',
    'asset_sheq_environmental.php',
    'asset_sheq_quality.php',
    'asset_statutory_history.php',
    'asset_maintenance_record.php');

$committees = array('committees_schedulemeeting.php','committees_create_agenda.php');
$incidents = array('incident_nearmiss.php', 'incident_report.php','incident_investigate.php', 'incident_view.php');
$legal = array('');

?>
<!--BEGIN SIDEBAR MENU-->
<nav id="sidebar" role="navigation" data-step="2" data-intro="Template has &lt;b&gt;many navigation styles&lt;/b&gt;"
    data-position="right" class="navbar-default navbar-static-side">
    <div class="sidebar-collapse menu-scroll">
        <ul id="side-menu" class="nav">
            <div class="navbar nav_title" style="border: 0;">
            <li><a href="index.php" class="site_title"><img width="35" src="images/logo2.png" alt="..."> <span class="">ProjeX</span></a></li>
            </div>
            <div class="clearfix"></div>
            
            <li><a href="index.php"><i class="fa fa-tachometer fa-fw">
                <div class="icon-bg bg-orange"></div>
            </i><span class="menu-title">Dashboard</span></a></li>
            
            <li><a href="#"><i class="fa fa-desktop fa-fw">
                <div class="icon-bg bg-pink"></div>
            </i><span class="menu-title">Projects</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="projects.php"><i class="fa fa-align-left"></i><span class="submenu-title">All</span></a></li>
                    <li><a href="task_setup.php"><i class="fa fa-align-left"></i><span class="submenu-title">Task Setup</span></a></li>
                </ul>
            </li>
            
            <li><a href="#"><i class="fa fa-send-o fa-fw">
                <div class="icon-bg bg-green"></div>
            </i><span class="menu-title">Financial</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href=""><i class="fa fa-briefcase"></i><span class="submenu-title">Cost Calculation</span></a></li>
                    <li><a href=""><i class="fa fa-th-large"></i><span class="submenu-title">Quote</span></a></li>
                    <li><a href=""><i class="fa fa-hand-o-up"></i><span class="submenu-title">Invoices</span></a></li>
                </ul>
            </li>
            
            <li><a href="customers.php"><i class="fa fa-th-list fa-fw">
                <div class="icon-bg bg-blue"></div>
            </i><span class="menu-title">Customers</span></a>
            </li>
            
            <li><a href="#"><i class="fa fa-file-o fa-fw">
                <div class="icon-bg bg-yellow"></div>
            </i><span class="menu-title">HR</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="employees.php"><i class="fa fa-suitcase"></i><span class="submenu-title">Employees</span></a></li>
                    <li><a href="employees_occupations.php"><i class="fa fa-steam"></i><span class="submenu-title">Occupations</span></a></li>
                    <li><a href="employees_leadership.php"><i class="fa fa-puzzle-piece"></i><span class="submenu-title">Leadership</span></a></li>
                    <li><a href="projects_team_builder.php"><i class="fa fa-puzzle-piece"></i><span class="submenu-title">Teams</span></a></li>
                    <li><a href=""><i class="fa fa-puzzle-piece"></i><span class="submenu-title">Disciplinary</span></a></li>
                    <li><a href="sheqteams.php"><i class="fa fa-puzzle-piece"></i><span class="submenu-title">Appointments</span></a></li>
                    <li><a href="employees_absenteeism.php"><i class="fa fa-puzzle-piece"></i><span class="submenu-title">Absenteeism</span></a></li>
                    <li><a href="training_management.php"><i class="fa fa-puzzle-piece"></i><span class="submenu-title">Training</span></a></li>
                </ul>
            </li>
            
            <li><a href="#"><i class="fa fa-gift fa-fw">
                <div class="icon-bg bg-grey"></div>
            </i><span class="menu-title">Resources</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="resource.php"><i class="fa fa-user"></i><span class="submenu-title">Assets</span></a></li>
                    <li><a href="material.php"><i class="fa fa-sign-in"></i><span class="submenu-title">Materials</span></a></li>
                    <li><a href="statutory_maintenance.php"><i class="fa fa-sign-out"></i><span class="submenu-title">Statutory Maintenance</span></a></li>
                    <li><a href=""><i class="fa fa-lock"></i><span class="submenu-title">Calibrations</span></a></li>
                </ul>
            </li>
            
            <li><a href="#"><i class="fa fa-send-o fa-fw">
                <div class="icon-bg bg-green"></div>
            </i><span class="menu-title">Suppliers</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href=""><i class="fa fa-briefcase"></i><span class="submenu-title">All Suppliers</span></a></li>
                </ul>
            </li>
            
            <li><a href="#"><i class="fa fa-envelope-o">
                <div class="icon-bg bg-primary"></div>
            </i><span class="menu-title">SHEQ</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href=""><i class="fa fa-inbox"></i><span class="submenu-title">Incidents</span></a></li>
                    <li><a href=""><i class="fa fa-pencil"></i><span class="submenu-title">Risk Assessments</span></a></li>
                    <li><a href="inspection_online.php"><i class="fa fa-eye"></i><span class="submenu-title">Online Inspection</span></a></li>
                    <li><a href="document_control.php"><i class="fa fa-eye"></i><span class="submenu-title">Documents</span></a></li>
                    <li><a href="method_statements.php"><i class="fa fa-eye"></i><span class="submenu-title">Method Statements</span></a></li>
                    <li><a href="fall_protection_plan.php"><i class="fa fa-eye"></i><span class="submenu-title">Fall Protection Plans</span></a></li>
                    <li><a href="safety_plan.php"><i class="fa fa-eye"></i><span class="submenu-title">Safety Plans</span></a></li>
                    <li><a href=""><i class="fa fa-eye"></i><span class="submenu-title">Compliance Register</span></a></li>
                    <li><a href=""><i class="fa fa-eye"></i><span class="submenu-title">Health</span></a></li>
                    <li><a href=""><i class="fa fa-eye"></i><span class="submenu-title">HCS</span></a></li>
                    <li><a href=""><i class="fa fa-eye"></i><span class="submenu-title">PPE</span></a></li>
                    <li><a href=""><i class="fa fa-eye"></i><span class="submenu-title">Committee</span></a></li>
                    <li><a href="awareness.php"><i class="fa fa-eye"></i><span class="submenu-title">Awareness</span></a></li>
                </ul>
            </li>
            
            <li><a href="#"><i class="fa fa-envelope-o">
                <div class="icon-bg bg-primary"></div>
            </i><span class="menu-title">Health</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="health_medicals.php"><i class="fa fa-inbox"></i><span class="submenu-title">Medicals</span></a></li>
                    <li><a href="health_surveys.php"><i class="fa fa-pencil"></i><span class="submenu-title">Surveys</span></a></li>
                    <li><a href="health_hazardous_material.php"><i class="fa fa-eye"></i><span class="submenu-title">Hazardous Material</span></a></li>
                </ul>
            </li>
            
            <li><a href="#"><i class="fa fa-envelope-o">
                <div class="icon-bg bg-primary"></div>
            </i><span class="menu-title">Improvements</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance"><i class="fa fa-inbox"></i><span class="submenu-title">Non-Conformance</span></a></li>
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action"><i class="fa fa-pencil"></i><span class="submenu-title">Corrective Action</span></a></li>
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation"><i class="fa fa-eye"></i><span class="submenu-title">Investigation</span></a></li>
                </ul>
            </li>
            
            <li><a href="#"><i class="fa fa-file-o fa-fw">
                <div class="icon-bg bg-yellow"></div>
            </i><span class="menu-title">Settings</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href=""><i class="fa fa-suitcase"></i><span class="submenu-title">Users</span></a></li>
                    <li><a href="company.php"><i class="fa fa-steam"></i><span class="submenu-title">Company</span></a></li>
                    <li><a href=""><i class="fa fa-puzzle-piece"></i><span class="submenu-title">Financial</span></a></li>
                    <li><a href=""><i class="fa fa-puzzle-piece"></i><span class="submenu-title">System</span></a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<!--END SIDEBAR MENU-->
 