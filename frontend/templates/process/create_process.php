<?php 
$title = 'Create Process';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of col-lg-2 side-bar --> 
<div class="col-lg-10">
    <!-- progress percentage -->   

<!-- Steps Progress and Details - START -->
<div class="col-sm-12">
    <div class="row">
        <div class="progress" id="progress1">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 10%;">
            </div>
            <span class="progress-type">Process Progress</span>
            <span class="progress-completed">20%</span>
        </div>
    </div>
    <div class="row">
         <div class="btn-group btn-group-justified">
             <a onclick="javascript: resetActive(event, 10, 'step-1')" class="btn btn-default">Step 1: Product & Services</a>
            <a onclick="javascript: resetActive(event, 18, 'step-2')" class="btn btn-default">Step 2: Process Description</a>
            <a onclick="javascript: resetActive(event, 30, 'step-3')" class="btn btn-default">Step 3: Materials</a>
            <a onclick="javascript: resetActive(event, 40, 'step-4')" class="btn btn-default">Step 4: Risk Assessment</a>
            <a onclick="javascript: resetActive(event, 60, 'step-5')" class="btn btn-default">Step 5: Process Stoppers</a>
            <a onclick="javascript: resetActive(event, 80, 'step-6')" class="btn btn-default">Step 6: Control Point</a>
            <a onclick="javascript: resetActive(event, 100, 'step-7')" class="btn btn-default">Step 7: Leadership</a>            
        </div>   
    </div>
    <div class="row setup-content step activeStepInfo" id="step-1">
           <div class="panel panel-default">
              <div class="panel-heading text-left">Step 1: Product & Services</div>
                <div class="panel-body text-left">
                    <div class="col-xs-6">
                    
                      <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="product_service">Do you want to use an existing product or service?</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <label class="radio-inline"><input type="radio" name="product_service" id="product_service1" value="1" required>Yes</label>
                                    <label class="radio-inline"><input type="radio" name="product_service" id="product_service2" value="0" >No</label>
                                </div>
                            </div>
                      </div>
                       
                    <!-- existing product or service -->    
                    <div class="existing_product_div">
                        <div class="row">
                        <div class="form-group">                       
                            <h5 class="text-center">Choose an existing product or service</h5>
                            <input type="hidden" name="existing_pros" id="existing_pros"/>
                            <label class="control-label col-sm-4" for="product">Product or service</label>                                   
                               <div class="col-lg-6 col-md-4 col-sm-8">                                   
                                    <select class="selectpicker" multiple data-live-search="true" name="existing_products_service"  id="existing_products_service" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more products" data-header="Close">
                                           <?php
                                           foreach($data['allProductsServices'] as $pro)
                                           {
                                               echo "<option value='{$pro["id"]}'>{$pro['prod_service']}</option>";
                                           }?>
                                   </select>
                               </div>
                        </div>                       
                        </div>                         
                     </div>
                     
                     <!-- new product or service -->
                     <div class="new_product_service">
                        <div class="row">
                            <form class="form-horizontal" method="post" name="addProductServiceForm" action="<?php echo BASE_URL;?>/index.php?action=post_product_service&module=process">
                                <h5 class="text-center">Create new product or service</h5>
                               <div class="form-group">
                                   <label class="control-label col-sm-4" for="product">Product</label>
                                   <div class="col-lg-6 col-md-4 col-sm-8">
                                   <input type="text" class="form-control" name="new_product" id="new_product" required placeholder=""> 
                                   </div>
                               </div>
                               <div class="form-group">
                                   <label class="control-label col-sm-4" for="description">Description of final product/service</label>
                                   <div class="col-lg-6 col-md-4 col-sm-8">
                                   <input type="text" class="form-control" name="new_product_description" id="new_product_description" required placeholder=""> 
                                   </div>
                               </div>
                               <div class="form-group">
                                   <div class="col-sm-offset-4 col-sm-8">
                                   <button type="button" id="addProductServiceBtn" name="" name="addProductServiceBtn" class="btn btn-success">Add</button>
                                   </div>
                               </div>
                           </form> 
                           </div>
                     </div>
                     </div>
                    <div class="col-xs-6">
                        <img src=""/>
                    </div>
                </div>
              <div class="panel-footer text-left ">
                  <button type="button" onclick="javascript: resetActive(event, 18, 'step-2')" id="addProductServiceBtn" class="btn btn-success">Next</button>
              </div>
           </div>
    </div>
    <div class="row setup-content step hiddenStepInfo" id="step-2">
         <div class="panel panel-default">
              <div class="panel-heading text-left">Step 2: Process Description</div>
                <div class="panel-body text-left">
                    <div class="col-xs-6">
                    <!-- existing product or service -->    
                        <div class="row">
                        <div class="form-group">
                               <label class="control-label col-sm-4" for="product">Description</label>
                               <div class="col-lg-6 col-md-4 col-sm-8">
                               <input type="text" class="form-control" name="product" id="product" required placeholder=""> 
                               </div>
                         </div>  
                        </div>
                    <br/>
                    <div class="row">
                        <div class="form-group">                       
                            <input type="hidden" name="existing_pros" id="existing_pros"/>
                            <label class="control-label col-sm-4" for="product">Department</label>                                   
                               <div class="col-lg-6 col-md-4 col-sm-8">                                   
                                    <select class="selectpicker" multiple data-live-search="true" name="existing_products_service"  id="existing_products_service" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more products" data-header="Close">
                                           <?php
                                           foreach($data['allProductsServices'] as $pro)
                                           {
                                               echo "<option value='{$pro["id"]}'>{$pro['prod_service']}</option>";
                                           }?>
                                   </select>
                               </div>
                        </div>>
                        </div>                         
                     
                     </div>
                    <div class="col-xs-6">
                        <img src=""/>
                    </div>
                </div>
                <div class="panel-footer text-left ">
                   <button type="button" onclick="javascript: resetActive(event, 10, 'step-1')" id="addProductServiceBtn" class="btn btn-primary">Previous</button>
                   <button type="button" onclick="javascript: resetActive(event, 30, 'step-3')" id="addProductServiceBtn" class="btn btn-success">Next</button>
              </div>
           </div>
    </div>
    <div class="row setup-content step hiddenStepInfo" id="step-3">
                 <div class="panel panel-default">
              <div class="panel-heading text-left">Step 3: Materials</div>
                <div class="panel-body text-left">
                    <div class="col-xs-6">
                    <!-- existing product or service -->    
                               <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="material_service">Do you want to use existing materials?</label>
                                <div class="col-lg-4 col-md-4 col-sm-8">
                                    <label class="radio-inline"><input type="radio" name="material_service" id="material_service1" value="1" required>Yes</label>
                                    <label class="radio-inline"><input type="radio" name="material_service" id="material_service2" value="0" >No</label>
                                </div>
                            </div>
                      </div>
                       
                    <!-- existing product or service -->    
                    <div class="existing_material_div">
                        <div class="row">
                        <div class="form-group">                       
                            <h5 class="text-center">Choose existing materials</h5>
                            <input type="hidden" name="existing_pros" id="existing_pros"/>
                            <label class="control-label col-sm-4" for="product">Materials</label>                                   
                               <div class="col-lg-6 col-md-4 col-sm-8">                                   
                                    <select class="selectpicker" multiple data-live-search="true" name="existing_products_service"  id="existing_products_service" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more products" data-header="Close">
                                           <?php
                                           foreach($data['AllMaterials'] as $pro)
                                           {
                                               echo "<option value='{$pro["id"]}'>{$pro['description']}</option>";
                                           }?>
                                   </select>
                               </div>
                        </div>
                        <div class="form-group">
                                   <div class="col-sm-offset-4 col-sm-8">
                                       <br/>
                                   <button type="button" onclick="javascript: resetActive(event, 18, 'step-2')" id="addProductServiceBtn" class="btn btn-success">Next</button>
                                   </div>
                        </div>
                        </div>                         
                     </div>
                     
                     <!-- new product or service -->
                     <div class="new_material_service">
                        <div class="row">
                                       <form class="form-horizontal" method="post" name="addMaterialForm" action="<?php echo BASE_URL;?>/index.php?action=post_material&module=material">
                         <h5 class="text-center">Add Material Details</h5>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="category">Category</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <select class="selectpicker form-multiselect" name="categories" required data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a category">
                                    <?php
                                      foreach($data["allMaterialCategories"] as $categories)
                                     {
                                          echo  "<option value='{$categories["id"]}'>{$categories['category']}</option>";
                                     }?>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="type">Type</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <select class="selectpicker form-multiselect" name="types" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                                  <?php
                                      foreach($data["allMaterialTypes"] as $types)
                                     {
                                          echo  "<option value='{$types["id"]}'>{$types['type']}</option>";
                                     }?>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="description">Description</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                            <input type="text" class="form-control"  name="description" id="description" required placeholder=""> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="inventoryNo">Inventory No</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                            <input type="text" class="form-control" name="inventoryNo" id="inventoryNo" required placeholder=""> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="departmentsToBeUsed">Departments To Be Used</label>
                            <input type="hidden" name="departments_selected" id="departments_selected"/>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <select class="selectpicker form-multiselect" id="departmentsToBeUsed" name="departmentsToBeUsed" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                                <?php
                                   foreach($data['allDepartments'] as $group)
                                   {
                                       echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                                   }
                                 ?>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="manufacturer">Manufacturer</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="manufacturer" id="manufacturer" value=""> 
                            </div>
                        </div>                                                       
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="addMaterialBtn"></label>
                            <div class="col-lg-6 text-left">                                
                             <button type="submit" id="addMaterialBtn" name="addMaterialBtn" class="btn btn-success">Add material and continue</button>
                            </div>
                        </div>
            </form>
                           </div>
                     </div>                 
                     
                     </div>
                    <div class="col-xs-6">
                        <img src=""/>
                    </div>
                </div>
                <div class="panel-footer text-left ">
                   <button type="button" onclick="javascript: resetActive(event, 18, 'step-2')" id="addProductServiceBtn" class="btn btn-primary">Previous</button>
                   <button type="button" onclick="javascript: resetActive(event, 40, 'step-4')" id="addProductServiceBtn" class="btn btn-success">Next</button>
              </div>
           </div>
    </div>
    
    
    <!-- Risk Assessment -->
    <div class="row setup-content step hiddenStepInfo" id="step-4">
                <div class="panel panel-default">
              <div class="panel-heading text-left">Step 4: Risk Assessment</div>
                <div class="panel-body text-left">
                <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#requestAssessment-modal" data-toggle="modal" data-target="#requestAssessment-modal"><button type="button" id="requestAssessment" class="btn btn-success">Request SHEQF Assessment</button></a>
                        </div>
                    </div>
                </div>
              <div class="panel-footer text-left">
                   <button type="button" onclick="javascript: resetActive(event, 30, 'step-3')" id="addProductServiceBtn" class="btn btn-primary">Previous</button>
                   <button type="button" onclick="javascript: resetActive(event, 60, 'step-5')" id="addProductServiceBtn" class="btn btn-success">Next</button>
              </div>
                </div>
    </div>
    
    
    <div class="row setup-content step hiddenStepInfo" id="step-5">
        <div class="panel panel-default">
              <div class="panel-heading text-left">Step 5: Process Stoppers</div>
                <div class="panel-body text-left">
                <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#requestAssessment-modal" data-toggle="modal" data-target="#requestAssessment-modal"><button type="button" id="requestAssessment" class="btn btn-success">Request SHEQF Assessment</button></a>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-left">
                   <button type="button" onclick="javascript: resetActive(event, 40, 'step-4')" id="addProductServiceBtn" class="btn btn-primary">Previous</button>
                   <button type="button" onclick="javascript: resetActive(event, 80, 'step-6')" id="addProductServiceBtn" class="btn btn-success">Next</button>
              </div>
                </div>
    </div>
    <div class="row setup-content step hiddenStepInfo" id="step-6">
         <div class="panel panel-default">
              <div class="panel-heading text-left">Step 6: Control Point</div>
                <div class="panel-body text-left">
                <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#requestAssessment-modal" data-toggle="modal" data-target="#requestAssessment-modal"><button type="button" id="requestAssessment" class="btn btn-success">Request SHEQF Assessment</button></a>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-left">
                   <button type="button" onclick="javascript: resetActive(event, 60, 'step-5')" id="addProductServiceBtn" class="btn btn-primary">Previous</button>
                   <button type="button" onclick="javascript: resetActive(event, 100, 'step-7')" id="addProductServiceBtn" class="btn btn-success">Next</button>
              </div>
                </div>
    </div>
    <div class="row setup-content step hiddenStepInfo" id="step-7">
        <form method="post" action="#">
            <!-- selected products and services -->
            <input type="hidden" id="selected_pr_service" name="selected_pr_service"/>
            <button type="button" onclick="javascript: resetActive(event, 80, 'step-6')" id="addProductServiceBtn" class="btn btn-primary">Back</button>
            <input type="submit" name="btnSubmitProcess" class="btn btn-success" value="Submut Process"/>
        </form>
        <br/>
    </div>
    
    
    <!-- Added information -->
    <div class="row"style="display:none;">
     <div class="panel panel-default">
            <div class="panel-heading text-left">Proccessers</div>
            <div class="panel-body text-left" style="border:none;">
              <div class="modal-body table-responsive">
                <table width="100%" class="table table-hover person_table">
                    <thead>
                    <tr>
                        <th>Steps Taken</th>
                        <th>Action Taken</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr class="bg-success step1">
                            <td>Step 1: Product</td>
                            <td>Coce Cola</td>
                            <td class=""><b><span class="glyphicon glyphicon-check"></span> Complete</b></td>
                        </tr>
                       <tr>
                            <td>Step 2: Process</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
                            <td><span class="glyphicon glyphicon-ban-circle"></span> Incomplete</td>
                        </tr>
                        <tr>
                            <td>Step 3: Materials</td>
                            <td>Wood,Machines, Computers</td>
                            <td><span class="glyphicon glyphicon-ban-circle"></span> Incomplete</td>
                        </tr>
                        <tr>
                            <td>Step 4: Risk Assessment</td>
                            <td>View Risk Assessment Steps</td>
                            <td><span class="glyphicon glyphicon-ban-circle"></span> Incomplete</td>
                        </tr>
                        <tr >
                            <td>Step 5: Process Stoppers</td>
                            <td>View Process Stoppers</td>
                            <td><span class="glyphicon glyphicon-ban-circle"></span> Incomplete</td>
                        </tr>
                        <tr >
                            <td>Step 6: Control Point</td>
                            <td>Bit Coint</td>
                            <td><span class="glyphicon glyphicon-ban-circle"></span> Incomplete</td>
                        </tr>
                    </tbody>
                </table>
                </div> 
            </div>
     </div>
    </div>    
</div>

<style>
    /*
    Product and Service styling
    */
    .new_product_service,.existing_product_div,.existing_material_div,.new_material_service
    {
        display:none;
    }
.hiddenStepInfo {
    display: none;
}

.activeStepInfo {
    display: block !important;
}

.underline {
    text-decoration: underline;
}

.step {
    margin-top: 27px;
}

.progress {
    position: relative;
    height: 25px;
}

.progress > .progress-type {
    position: absolute;
    left: 0px;
    font-weight: 800;
    padding: 3px 30px 2px 10px;
    color: rgb(255, 255, 255);
    background-color: rgba(25, 25, 25, 0.2);
}

.progress > .progress-completed {
    position: absolute;
    right: 0px;
    font-weight: 800;
    padding: 3px 10px 2px;
}

.step {
    text-align: center;
}

.step .col-md-2 {
    background-color: #fff;
    border: 1px solid #C0C0C0;
    border-right: none;
}

.step .col-md-2:last-child {
    border: 1px solid #C0C0C0;
}

.step .col-md-2:first-child {
    border-radius: 5px 0 0 5px;
}

.step .col-md-2:last-child {
    border-radius: 0 5px 5px 0;
}

.step .col-md-2:hover {
    color: #F58723;
    cursor: pointer;
}

.step .activestep {
    color: #F58723;
    height: 100px;
    margin-top: -7px;
    padding-top: 7px;
    border-left: 6px solid #5CB85C !important;
    border-right: 6px solid #5CB85C !important;
    border-top: 3px solid #5CB85C !important;
    border-bottom: 3px solid #5CB85C !important;
    vertical-align: central;
}

.step .fa {
    padding-top: 15px;
    font-size: 40px;
}
</style>

<script type="text/javascript">
   function displayMessage(result,message)
    {
        toastr.options = {
           "closeButton": true,
           "debug": false,
           "positionClass": "toast-top-right",
           "onclick": null,
           "showDuration": "300",
           "hideDuration": "1000",
           "timeOut": "5000",
           "extendedTimeOut": "1000",
           "showEasing": "swing",
           "hideEasing": "linear",
           "showMethod": "fadeIn",
           "hideMethod": "fadeOut"
       }
       if(result == "success")
       {
            toastr.success(message, "<?php echo "Message"; ?>");
       }
       else{
            toastr.warning(message, "<?php echo "Message"; ?>");
       }
    }    
    
     $('input[name="product_service"]').click(function(){
            if($(this).attr("value")==="0"){
                $(".existing_product_div").hide();
                $(".new_product_service").show();
            }else{
                
            /*$.ajax({
                    url:'<?php echo BASE_URL;?>',
                    type:'GET',
                    success:function(result)
                    {
                        
                    }
                });*/
                
                $(".existing_product_div").show();
                $(".new_product_service").hide();
            }
      });
       $('input[name="material_service"]').click(function(){
            if($(this).attr("value")==="0"){
                $(".existing_material_div").hide();
                $(".new_material_service").show();
            }else{
                $(".existing_material_div").show();
                $(".existing_products_service").html();
                $(".new_material_service").hide();
            }
      });
    function resetActive(event, percent, step) {
        $(".progress-bar").css("width", percent + "%").attr("aria-valuenow", percent);
        $(".progress-completed").text(percent + "%");
        $(".btn-default").removeClass("active");
        $(this).addClass("active");
        hideSteps();
        showCurrentStepInfo(step);
    }

    function hideSteps() {
        $("div").each(function () {
            if ($(this).hasClass("activeStepInfo")) {
                $(this).removeClass("activeStepInfo");
                $(this).addClass("hiddenStepInfo");
            }
        });
    }
    //////// Emmanuel : Add Product or service using ajax /////////
    $("#addProductServiceBtn").on("click",function(result)
    {
        var ur = "<?php echo BASE_URL;?>/index.php?action=post_product_service&module=process";
        if($("#new_product_description").val() == "" || $("#new_product").val() == "")
        {
            alert("Please fill in product details");
            return false;
        }
        $.ajax({
          url:  ur,
          type:"POST",
          data:{ajaxPostProductService:"REQUEST",product:$("#new_product").val(),description:$("#new_product_description").val()},
          success:function(result)
          {
              //switch based on the result
              if(result == "OK")
              {
                  displayMessage("success","Product or service successfully created");
              }
              else
              {
                  displayMessage("danger","Failed to create product or service");
              }
          }
        });
    });
    $('.selectpicker').on('change', function(){              
        var selected = $(this," option:selected").val();
        //var selected = this.innerHTML;
        //alert(selected);
        focus_dropdown = $(this).parent().children('select').attr('id');
        switch(focus_dropdown)
        {
            //step 1. Products and services
            case 'existing_products_service':    
                if(selected != "")
                {
                    $("#selected_pr_service").val(selected);                    
                    $(".step1 td:nth-child(2)").html($("#selected_pr_service").val()); 
                    //choice
                }
                break;
           case 'search_departments':
               $("#departments_selected").val(selected);
                break;                
            case 'search_status':
                $("#status_selected").val(selected);
                break;
        }
       // console.log(focus_dropdown);
    });
    function showCurrentStepInfo(step) {        
        var id = "#" + step;
        $(id).addClass("activeStepInfo");
    }
</script>

<!-- Steps Progress and Details - END -->
</div>
<?php 
/*
 * 
 * 
 * 
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    