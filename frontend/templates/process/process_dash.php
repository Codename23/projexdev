<?php 
$title = 'Process';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of col-lg-2 side-bar --> 
<div class="col-lg-10">
    <!--sub menu-->
   
    <!--End of sub menu-->
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="<?php echo BASE_URL;?>/index.php?action=view_create_process&module=process" data-toggle="modal"><button type="button" id="addProcessStep" class="btn btn-danger">Add Process</button></a>
        <a href="#addProcessStep-modal" data-toggle="modal" data-target="#addProcessStep-modal"><button type="button" id="addProcessStep" class="btn btn-success">Add Process</button></a>
        <a href="#addProcessStep2-modal" data-toggle="modal" data-target="#addProcessStep2-modal"><button type="button" id="addProcessStep" class="btn btn-primary">Add Process Steps</button></a>
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading">Process Selection</div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2" for="productsService">Products / Service</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="productsService" id="productsService" class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a product / service">
                        <option>Wood Shutters</option>
                        <option>Fire Extinguishers</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="department">Department</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="department" id="department" class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                        <option>Wood Department</option>
                        <option>IT</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" id="viewProcess" class="btn btn-success">View</button>
                </div>
            </div>
        </form>
    </div>
    </div>
    
      <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=process&action=defaultAction">Process Dash</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_stock_supply">Stock / Supply</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_risk_controls">Risk / Controls</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_rps">RPS</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_control_sample_point">Control / Sample Point</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_products_services">Products / Service</a></li>
        </ul>
        </div>
    </div> 

    
    <!-- process modal -->
    <div id="addProcessStep2-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Create a new process</h3>
        </div>
        <div class="modal-body" id="myWizard">
          
         <div class="progress">
           <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 20%;">
             Step 1 of 5
           </div>
         </div>
        
         <div class="navbar">
            <div class="navbar-inner">
                  <ul class="nav nav-pills">
                     <li class="active"><a href="#step1" data-toggle="tab" data-step="1">Step 1 (Products & Services)</a></li>
                     <li><a href="#step2" data-toggle="tab" data-step="2">Step 2</a></li>
                     <li><a href="#step3" data-toggle="tab" data-step="3">Step 3</a></li>
                     <li><a href="#step4" data-toggle="tab" data-step="4">Step 4</a></li>
                  </ul>
            </div>
         </div>
         <div class="tab-content">
            <div class="tab-pane fade in active" id="step1">
               <form class="form-horizontal" method="post" name="addProductServiceForm" action="<?php echo BASE_URL;?>/index.php?action=post_product_service&module=process">
             <div class="form-group">
                <label class="text-center col-sm-12" for="product">Use an existing product or service</label>               
            </div>      
            <div class="form-group">
                <label class="control-label col-sm-4" for="product">Product</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="product" id="product" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="description">Description of final product/service</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="description" id="description" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addProductServiceBtn" name="addProductServiceBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
              <!--<a class="btn btn-default next" href="#">Continue</a>-->
              
              
            </div>
            <div class="tab-pane fade" id="step2">
               
                <!-- step 1.0 section -->
                
                
                <!-- ./step 1.1 section -->
                
            </div>
            <div class="tab-pane fade" id="step3">
              <div class="well"> <h2>Step 3</h2> Add another step here..</div>
               <a class="btn btn-default next" href="#">Continue</a>
            </div>
            <div class="tab-pane fade" id="step4">
              <div class="well"> <h2>Step 4</h2> You're Done!</div>
               <a class="btn btn-success first" href="#">Start over</a>
            </div>
         </div>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
          <button class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
    <!-- ./end of new process modal -->
    
    <div class="panel panel-default">
    <div class="panel-heading">Process Dashboard</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Process Description</th>
                    <th>Department</th>
                    <th>Product Service</th>
                    <th>Expected Outcomes</th>
                    <th>Process Flow</th>
                    <th>Occupations</th>
                    <th>Assets</th>
                    <th>RPS</th>
                    <th>Material & Stock</th>
                    <th>R/A</th>
                    <th>Leadership</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Receiving</td>
                    <td>Warehouse</td>
                    <td><a href="">View</a></td>
                    <td><a href="">View</a></td>
                    <td><a href="#processFlow-modal" data-toggle="modal" data-target="#processFlow-modal">View</a></td>
                    <td><a href="#occupations-modal" data-toggle="modal" data-target="#occupations-modal">View</a></td>
                    <td><a href="#assets-modal" data-toggle="modal" data-target="#assets-modal">View</a></td>
                    <td><a href="process_rps.php">View</a></td>
                    <td><a href="process_stock_supply.php">View</a></td>
                    <td><a href="process_risk_controls.php">View</a></td>
                    <td><a href="#leadership-modal" data-toggle="modal" data-target="#leadership-modal">View</a></td>
                    <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!-- Add Process Step Modal -->
  <div class="modal fade" id="addProcessStep-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Process Step</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addProcessStepForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="processDescription">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="processDescription" id="processDescription" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="department">Department</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="department" id="department" class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                        <option>Wood Department</option>
                        <option>IT</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Core Function</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="productServiceLink">Product / Service Link</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="productServiceLink" id="productServiceLink" class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a link">
                        <option>Link 1</option>
                        <option>Link 2</option>
                        <option>Link 3</option>
                        <option>Link 4</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="descriptionOfActions">Description Of Action</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" name="descriptionOfActions" id="descriptionOfActions" required placeholder=""></textarea>
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Process</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="summaryOfProcess">Summary Of Process</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" name="summaryOfProcess" id="summaryOfProcess" required placeholder=""></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="fullDescription">Full Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" name="fullDescription" id="fullDescription" required placeholder=""></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="addDoc">Add Document</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="file" name="addDoc" id="addDoc"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="addPhoto">Add Photo</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="file" name="addPhoto" id="addPhoto"> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addProcessStepBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Process Flow Modal -->
  <div class="modal fade" id="processFlow-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Process Flow</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Product / Service</th>
                            <th colspan="4">(A) Wood Shutters</th>
                        </tr>
                        <tr>
                            <th>No</th>
                            <th>Process Description</th>
                            <th>Department</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Receiving</td>
                            <td>Warehouse</td>
                            <td><a href=""><span class="glyphicon glyphicon-arrow-up"></span></a></td>
                            <td><a href=""><span class="glyphicon glyphicon-arrow-down"></span></a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Occupations Modal -->
  <div class="modal fade" id="occupations-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">View Occupations</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Department</th>
                            <th>Occupation</th>
                            <th>Employee Name Surname</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Receiving</td>
                            <td>Forklift Driver</td>
                            <td>John Spies</td>
                            <td><a href="">View Details</a></td>
                        </tr>
                        <tr>
                            <td>Receiving</td>
                            <td>Stacker</td>
                            <td>Kobus Blue</td>
                            <td><a href="">View Details</a></td>
                        </tr>
                        <tr>
                            <td>Receiving</td>
                            <td>Machine Operators</td>
                            <td>Thabo Botha</td>
                            <td><a href="">View Details</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Assets Modal -->
  <div class="modal fade" id="assets-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Assets</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <button type="button" id="" class="btn btn-success">Add Asset</button>
            </div>
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Department</th>
                            <th>Category</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>No</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Receiving</td>
                            <td>Moveable Asset</td>
                            <td>Forklift</td>
                            <td>Toyota</td>
                            <td>123</td>
                            <td><a href="">View Details</a></td>
                        </tr>
                        <tr>
                            <td>Receiving</td>
                            <td>Moveable Asset</td>
                            <td>Forklift</td>
                            <td>Toyota</td>
                            <td>124</td>
                            <td><a href="">View Details</a></td>
                        </tr>
                        <tr>
                            <td>Receiving</td>
                            <td>Moveable Asset</td>
                            <td>Forklift</td>
                            <td>Toyota</td>
                            <td>125</td>
                            <td><a href="">View Details</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Assets Modal -->
  <div class="modal fade" id="assets-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Assets</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <button type="button" id="" class="btn btn-success">Add Asset</button>
            </div>
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Department</th>
                            <th>Category</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>No</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Receiving</td>
                            <td>Moveable Asset</td>
                            <td>Forklift</td>
                            <td>Toyota</td>
                            <td>123</td>
                            <td><a href="">View Details</a></td>
                        </tr>
                        <tr>
                            <td>Receiving</td>
                            <td>Moveable Asset</td>
                            <td>Forklift</td>
                            <td>Toyota</td>
                            <td>124</td>
                            <td><a href="">View Details</a></td>
                        </tr>
                        <tr>
                            <td>Receiving</td>
                            <td>Moveable Asset</td>
                            <td>Forklift</td>
                            <td>Toyota</td>
                            <td>125</td>
                            <td><a href="">View Details</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Leadership Modal -->
  <div class="modal fade" id="leadership-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Leadership</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <a href="addLeadership-modal" data-toggle="modal" data-target="#addLeadership-modal"><button type="button" id="addLeadership" class="btn btn-success">Add Leadership</button></a>
            </div>
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Position</th>
                            <th>Department</th>
                            <th>Occupation</th>
                            <th>Employee Name Surname</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Supervisor</td>
                            <td>Receiving</td>
                            <td>Forklift Driver</td>
                            <td>John Spies</td>
                            <td><a href="">View Details</a></td>
                        </tr>
                        <tr>
                            <td>Supervisor</td>
                            <td>Receiving</td>
                            <td>Stacker</td>
                            <td>Kobus Blue</td>
                            <td><a href="">View Details</a></td>
                        </tr>
                        <tr>
                            <td>Department Manager</td>
                            <td>Receiving</td>
                            <td>Machine Operator</td>
                            <td>Thabo Botha</td>
                            <td><a href="">View Details</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div><!--End of col-lg-10 --> 
</div>
<script>
$(document).ready(function(){

  $('.next').click(function(){
  
    var nextId = $(this).parents('.tab-pane').next().attr("id");
    $('[href=#'+nextId+']').tab('show');
    return false;
    
  })
  
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    
    //update progress
    var step = $(e.target).data('step');
    var percent = (parseInt(step) / 4) * 100;
    
    $('.progress-bar').css({width: percent + '%'});
    $('.progress-bar').text("Step " + step + " of 4");
    
    //e.relatedTarget // previous tab
    
  })
  
  $('.first').click(function(){
  
    $('#myWizard a:first').tab('show')
  
  })

});
</script>
<!--End of container-fluid -->
<?php 
/*
 * 
 * 
 * 
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    