<?php 
$title = 'Process Control Sample Point';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of col-lg-2 side-bar --> 
<div class="col-lg-10">
    <!--sub menu-->
      <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=process&action=defaultAction">Process Dash</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_stock_supply">Stock / Supply</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_risk_controls">Risk / Controls</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_rps">RPS</a></li>
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_control_sample_point">Control / Sample Point</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_products_services">Products / Service</a></li>
        </ul>
        </div>
    </div>  
    <!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-heading">Control / Sample Point Selection</div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2" for="productsService">Products / Service</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="productsService" class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a product / service">
                        <option>Wood Shutters</option>
                        <option>Fire Extinguishers</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="department">Department</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="department" class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more department" data-header="Close">
                        <option>Wood Department</option>
                        <option>IT</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="process">Process</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="process" class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more processes" data-header="Close">
                        <option>Process 1</option>
                        <option>Process 2</option>
                        <option>Process 3</option>
                        <option>Process 4</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" id="viewStock" class="btn btn-success">View</button>
                </div>
            </div>
        </form>
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#addControlSampling-modal" data-toggle="modal" data-target="#addControlSampling-modal"><button type="button" id="addControlSampling" class="btn btn-success">Add Control/Sampling</button></a>
        <a href="#linkControlSampling-modal" data-toggle="modal" data-target="#linkControlSampling-modal"><button type="button" id="linkControlSampling" class="btn btn-success">Link Control/Sampling</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Control / Sample Point</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Group</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Resource Link</th>
                    <th>Document Link</th>
                    <th>View Process</th>
                    <th>Frequency Input</th>
                    <th>Min</th>
                    <th>Baseline</th>
                    <th>Max</th>
                    <th>Units</th>
                    <th>Per</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Food</td>
                    <td>Temperature</td>
                    <td>Freezer Temp</td>
                    <td><a href="">View</a></td>
                    <td><a href="">View</a></td>
                    <td><a href="">View</a></td>
                    <td>Hourly</td>
                    <td>-20</td>
                    <td>-10</td>
                    <td>0</td>
                    <td>C</td>
                    <td>Hour</td>
                    <td><a href="">View History</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!-- Add Control Sampling Modal -->
  <div class="modal fade" id="addControlSampling-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Measurement</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addControlSamplingForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a group">
                        <option>Raw Materials</option>
                        <option>Consumables</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                        <option>Type 1</option>
                        <option>Type 2</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="description">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="description" id="description" required placeholder=""> 
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Control Point</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="units">Units</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="per">Per</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Make a selection">
                        <option>Day</option>
                        <option>Week</option>
                        <option>Month</option>
                        <option>Year</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="measurementReading">Measurement Reading Fequency</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="measurementReading" id="measurementReading" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="documentLink">Document Link</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="documentLink" id="documentLink" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="minimum">Minimum</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="minimum" id="minimum" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="baseline">Baseline</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="baseline" id="baseline" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="maximum">Maximum</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="maximum" id="maximum" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="units2">Units</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="per2">Per</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Make a selection">
                        <option>Day</option>
                        <option>Week</option>
                        <option>Month</option>
                        <option>Year</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="processDescription">Process Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" name="processDescription" id="processDescription" required placeholder=""></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notify">Notify when minimum & maximum is reached</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" name="notify" id="notify" required placeholder=""></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addControlSamplingBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Link Control Sampling Modal -->
  <div class="modal fade" id="linkControlSampling-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Link Measurement</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="linkControlSamplingForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a group">
                        <option>Raw Materials</option>
                        <option>Consumables</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                        <option>Type 1</option>
                        <option>Type 2</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="description">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="description" id="description" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="linkControlSamplingBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div><!--End of col-lg-10 --> 
</div>
<!--End of container-fluid -->
<?php 
/*
 * 
 * 
 * 
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    