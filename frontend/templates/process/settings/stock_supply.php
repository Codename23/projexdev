<?php 
$title = 'Process Stock Supply';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of col-lg-2 side-bar --> 
<div class="col-lg-10">
    <!--sub menu-->
     <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=process&action=defaultAction">Process Dash</a></li>
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_stock_supply">Stock / Supply</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_risk_controls">Risk / Controls</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_rps">RPS</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_control_sample_point">Control / Sample Point</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_products_services">Products / Service</a></li>
        </ul>
        </div>
    </div>  
    <!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-heading">Stock Supply Selection</div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2" for="productsService">Products / Service</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="productsService" class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a product / service">
                        <option>Wood Shutters</option>
                        <option>Fire Extinguishers</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="department">Department</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="department" class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more department" data-header="Close">
                        <option>Wood Department</option>
                        <option>IT</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="process">Process</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="process" class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more processes" data-header="Close">
                        <option>Process 1</option>
                        <option>Process 2</option>
                        <option>Process 3</option>
                        <option>Process 4</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" id="viewStock" class="btn btn-success">View</button>
                </div>
            </div>
        </form>
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#addMaterial-modal" data-toggle="modal" data-target="#addMaterial-modal"><button type="button" id="addMaterial" class="btn btn-success">Add Material</button></a>
        <a href="#linkMaterial-modal" data-toggle="modal" data-target="#linkMaterial-modal"><button type="button" id="linkMaterial" class="btn btn-success">Link Material</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Stock / Supply</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>No</th>
                    <th>Usage</th>
                    <th>Unit P/M</th>
                    <th>Minimum Stock Level</th>
                    <th>Units</th>
                    <th>Current Supplier</th>
                    <th>Order to Delivery Days</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Consumable</td>
                    <td>HCS</td>
                    <td>Diesel</td>
                    <td>232</td>
                    <td>2000</td>
                    <td>L</td>
                    <td>1000</td>
                    <td>L</td>
                    <td>Engine</td>
                    <td>2</td>
                    <td><a href="#addProcessUsage-modal" data-toggle="modal" data-target="#addProcessUsage-modal">Add Process Usage</a></td>
                    <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                </tr>
                <tr>
                    <td>Consumable</td>
                    <td>Wrapping</td>
                    <td>Bubble Wrap</td>
                    <td>4324</td>
                    <td>5</td>
                    <td>Units</td>
                    <td>10</td>
                    <td>Units</td>
                    <td>Paper and Plastics</td>
                    <td>8</td>
                    <td><a href="#addProcessUsage-modal" data-toggle="modal" data-target="#addProcessUsage-modal">Add Process Usage</a></td>
                    <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!-- Add Material Modal -->
  <div class="modal fade" id="addMaterial-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Material</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addMaterialForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="category">Category</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a category">
                        <option>Raw Materials</option>
                        <option>Consumables</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                        <option>Type 1</option>
                        <option>Type 2</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="description">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="description" id="description" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="departmentsToBeUsed">Departments To Be Used</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                        <option>I.T</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Additional Information</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="manufacturer">Manufacturer</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="manufacturer" id="manufacturer" required value="Bosh"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="supplier">Supplier</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more suppliers">
                        <option>Supplier 1</option>
                        <option>Supplier 2</option>
                        <option>Supplier 3</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderToDelivery">Order to delivery days</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a number">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="cost">Cost</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="cost" id="cost" required> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="perUnit">Per Unit</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="usagePerDay">Usage</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="usagePerDay" id="usagePerDay" required> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="per">Per</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Make a selection">
                        <option>Day</option>
                        <option>Week</option>
                        <option>Month</option>
                        <option>Year</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="units">Units</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select>
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="minimumStockLevel">Minimum Stock Level</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="minimumStockLevel" id="minimumStockLevel" required> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="stockUnits">Units</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addMaterialBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Link Material Modal -->
  <div class="modal fade" id="linkMaterial-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Link Material</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="linkMaterialForm" action="material.php">
            <div class="form-group">
                <label class="control-label col-sm-4" for="category">Category</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a category">
                        <option>Raw Materials</option>
                        <option>Consumables</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                        <option>Type 1</option>
                        <option>Type 2</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="description">Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="description" id="description" required placeholder=""> 
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Additional Information</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="manufacturer">Manufacturer</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="manufacturer" id="manufacturer" required value="Bosh"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="supplier">Supplier</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more suppliers">
                        <option>Supplier 1</option>
                        <option>Supplier 2</option>
                        <option>Supplier 3</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderToDelivery">Order to delivery days</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a number">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="cost">Cost</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="cost" id="cost" required> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="perUnit">Per Unit</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="usagePerDay">Usage</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="usagePerDay" id="usagePerDay" required> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="per">Per</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Make a selection">
                        <option>Day</option>
                        <option>Week</option>
                        <option>Month</option>
                        <option>Year</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="units">Units</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select>
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="minimumStockLevel">Minimum Stock Level</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="minimumStockLevel" id="minimumStockLevel" required> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="stockUnits">Units</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="linkMaterialBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Add Process Usage Modal -->
  <div class="modal fade" id="addProcessUsage-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Process Usage</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addProcessUsageForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="supplier">Supplier</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more suppliers">
                        <option>Supplier 1</option>
                        <option>Supplier 2</option>
                        <option>Supplier 3</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderToDelivery">Order to delivery - Days</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a number">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="cost">Cost</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="cost" id="cost" required> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="perUnit">Per Unit</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="usage">Usage</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="usage" id="usage" required> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="unitsPerMonth">Units Per Month</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="minimumStockLevel">Minimum Stock Level</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="minimumStockLevel" id="minimumStockLevel" required> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="units">Units</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addTotalUsageBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div><!--End of col-lg-10 --> 
</div>
<!--End of container-fluid -->
<?php 
/*
 * 
 * 
 * 
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    