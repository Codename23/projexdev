<?php 
$title = 'Process RPS';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of col-lg-2 side-bar --> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=process&action=defaultAction">Process Dash</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_stock_supply">Stock / Supply</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_risk_controls">Risk / Controls</a></li>
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_rps">RPS</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_control_sample_point">Control / Sample Point</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_products_services">Products / Service</a></li>
        </ul>
        </div>
    </div>  
    <!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-heading">RPS Selection</div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2" for="productsService">Products / Service</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="productsService" class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a product / service">
                        <option>Wood Shutters</option>
                        <option>Fire Extinguishers</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="department">Department</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="department" class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more department" data-header="Close">
                        <option>Wood Department</option>
                        <option>IT</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="process">Process</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="process" class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more processes" data-header="Close">
                        <option>Process 1</option>
                        <option>Process 2</option>
                        <option>Process 3</option>
                        <option>Process 4</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" id="viewStock" class="btn btn-success">View</button>
                </div>
            </div>
        </form>
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#addCriticalAsset-modal" data-toggle="modal" data-target="#addCriticalAsset-modal"><button type="button" id="addCriticalAsset" class="btn btn-success">Add Critical Asset</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Process Stoppers Assessment</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>No</th>
                    <th>Date Added</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php //allProcessStoppers?>
                   <?php
               $counter = 0;                
               if($data['allProcessStoppers']): foreach($data['allProcessStoppers'] as $rps):
                   $counter++;
                   ?>
                            <tr>                                
                                <td><?php echo $rps['department_name']; ?></td>
                                <td><?php echo $rps['category']; ?></td>                                
                                <td><?php echo $rps['type']; ?></td>
                                <td><?php echo $rps['description']; ?></td>
                                <td><?php echo $rps['no']; ?></td>
                                <td><?php echo $rps['date_created']; ?></td>
                                <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                            </tr>
                        <?php endforeach;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
    
    <!-- Add Critical Asset Modal -->
  <div class="modal fade" id="addCriticalAsset-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Critical Asset</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addCriticalAssetForm" action="<?php echo BASE_URL;?>/index.php?action=post_process_stoppers&module=process">
            <div class="form-group">
                <label class="control-label col-sm-4" for="department">Department</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="process_department" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                        <?php
                        foreach($data['allDepartments'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                        }?>                        
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="searchAsset">Search Asset</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="searchAsset" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select an asset">
                            <?php
                        foreach($data['assets'] as $asset)
                        {
                            echo "<option value='{$asset["id"]}'>{$asset['description']}</option>";
                        }?>     
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addCriticalAssetBtn" name="addCriticalAssetBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->


</div><!--End of col-lg-10 --> 
</div>
<!--End of container-fluid -->
<?php 
/*
 * 
 * 
 * 
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    