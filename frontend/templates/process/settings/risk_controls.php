<?php 
$title = 'Process Risk Controls';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of col-lg-2 side-bar --> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=process&action=defaultAction">Process Dash</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_stock_supply">Stock / Supply</a></li>
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_risk_controls">Risk / Controls</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_rps">RPS</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_control_sample_point">Control / Sample Point</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_products_services">Products / Service</a></li>
        </ul>
        </div>
    </div>   
    <!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-heading">Risk / Controls Selection</div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2" for="productsService">Products / Service</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="productsService" class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a product / service">
                        <option>Wood Shutters</option>
                        <option>Fire Extinguishers</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="department">Department</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="department" class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more department" data-header="Close">
                        <option>Wood Department</option>
                        <option>IT</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="process">Process</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="process" class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more processes" data-header="Close">
                        <option>Process 1</option>
                        <option>Process 2</option>
                        <option>Process 3</option>
                        <option>Process 4</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" id="viewStock" class="btn btn-success">View</button>
                </div>
            </div>
        </form>
    </div>
    </div>

    <form class="form-horizontal" method="post" name="riskControlsForm" action="">
<div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Risk Assessment</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#requestAssessment-modal" data-toggle="modal" data-target="#requestAssessment-modal"><button type="button" id="requestAssessment" class="btn btn-success">Request SHEQF Assessment</button></a>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#addRiskAssessment-modal" data-toggle="modal" data-target="#addRiskAssessment-modal"><button type="button" id="addRiskAssessment" class="btn btn-success">Add Risk Assessment</button></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Risk No</th>
                                    <th>Group</th>
                                    <th>Hazard Types</th>
                                    <th>Hazard Description</th>
                                    <th>Risk Types</th>
                                    <th>Risk Desription</th>
                                    <th>Score</th>
                                    <th>Control</th>
                                    <th>Control Description</th>
                                    <th>Control in place</th>
                                    <th>New Score</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>Fire</td>
                                    <td></td>
                                    <td>Property Damage</td>
                                    <td></td>
                                    <td class="majordanger-btn">200</td>
                                    <td></td>
                                    <td></td>
                                    <td>Yes</td>
                                    <td class="green-btn">20</td>
                                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>Explode</td>
                                    <td></td>
                                    <td>Property Damage</td>
                                    <td></td>
                                    <td class="red-btn">150</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="green-btn">20</td>
                                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>Noise</td>
                                    <td></td>
                                    <td>Hearing Loss</td>
                                    <td></td>
                                    <td class="green-btn">50</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="green-btn">20</td>
                                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                </tr>
                            </tbody>
                        </table>    
                    </div><br/>
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#sheqfNotes-modal" data-toggle="modal" data-target="#sheqfNotes-modal"><button type="button" id="sheqfNotes" class="btn btn-success">SHEQF Notes</button></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Group</th>
                                    <th>Topic</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Risk Control Check Panel</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Risk No</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                    <th>Active</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Yes</td>
                                </tr>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    <!--End of panel-default-->

<!-- Request Assessment Modal -->
  <div class="modal fade" id="requestAssessment-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Request SHEQF Assessment</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="requestAssessmentForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="sheqfPriority">Priority</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more priorities" data-header='Close'>
                        <option>Priority 1</option>
                        <option>Priority 2</option>
                        <option>Priority 3</option>
                        <option>Priority 4</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="sheqfDueDate">Due Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="sheqfDueDate" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="sheqfResponsiblePerson">Responsible Person</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                        <option>Nick Botha</option>
                        <option>Sunny Mathole</option>
                        <option>Brian Douglas</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="sheqfNotes">Notes</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" id="sheqfNotes" required placeholder=""></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="sheqfBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
    
<!-- Add Risk Assessment Modal -->
  <div class="modal fade" id="addRiskAssessment-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Risk Assessment</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addRiskAssessmentForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="group" class="form-control">
                    <option value selected disabled>Please select a group</option>
                    <option value="1">OHS (Health & Safety)</option>
                    <option value="2">E (Environment)</option>
                    <option value="3">Q (Quality)</option>
                    <option value="4">F (Food)</option>
                    </select>
                </div>
            </div>
            
            <div class="modal-custom-h5"><span><h5>Hazard</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="hazardType">Hazard Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a hazard type">
                        <option>Electricity</option>
                        <option>Fire</option>
                        <option>Height</option>
                        <option>Heavy Resource</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="hazardDescription">Hazard Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="hazardDescription" required placeholder="">
                </div>
            </div>
            
            <div class="modal-custom-h5"><span><h5>Risk</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="riskType">Risk Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more risk types" data-header="Close">
                    <option>Type 1</option>
                    <option>Type 2</option>
                    <option>Type 3</option>
                </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="riskDescription">Risk Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="riskDescription" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <span id="pscore"></span>
                <a href="#risk-assessment-score-modal" data-toggle="modal" data-target="#risk-assessment-score-modal"><button type="button" id="risk-assessment-score" class="btn btn-success">Add Score</button></a>
                </div>
            </div>
            
            <div class="modal-custom-h5"><span><h5>Control</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="controlType">Control Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more control types" data-header="Close">
                    <option>Type 1 - 10%</option>
                    <option>Type 2 - 20%</option>
                    <option>Type 3 - 50%</option>
                </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="controlDescription">Control Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="controlDescription" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="controlInEffect">Control Currently in Effect</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <label class="radio-inline"><input type="radio" name="controlInEffect" id="controlInEffect1" value="0" required>Yes</label>
                    <label class="radio-inline"><input type="radio" name="controlInEffect" id="controlInEffect2" value="1" >No</label>
                </div>
            </div>
            <div class="control hidden-div">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="controlSuggestedDueDate">Suggested Due Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="controlSuggestedDueDate" name="controlSuggestedDueDate" required >
                    </div>
                </div>
               <div class="form-group">
                    <label class="control-label col-sm-4" for="controlResponsiblePerson">Responsible Person</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                        <option>Nick Botha</option>
                        <option>Brian Douglas</option>
                        <option>Sunny Mathole</option>
                    </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="controlNotes">Notes</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows='5' class="form-control" id="controlNotes" name="controlNotes" required ></textarea>
                    </div>
                </div>
            </div>
            <div class="controlYes hidden-div2">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="controlYesReduction">Reduced By</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" id="controlYesReduction" name="controlYesReduction" value='50%' readonly="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="controlYesScore">New Score</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" id="controlYesScore" name="controlYesScore" value='200' readonly="">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addRiskAssessmentBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Modal -->
  <div class="modal fade" id="risk-assessment-score-modal" role="dialog">
    <div class="modal-dialog risk-assessment-score-dialog-modal">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Risk Assessment Score</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" name="riskAssessmentScoreForm" >
              <div class="table-responsive">
                  <table width="100%" class="table table-bordered">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="text-center" colspan="6">Severity of the potential injury/damage</th>
                </tr>
                </thead>
                <tbody>
                <tr class="text-center">
                    <td></td>
                    <td width="16%"><p>Insignificant damage to Property, Equipment or Minor Injury</p></td>
                    <td width="16%"><p>Non-Reportable Injury, minor loss of Process or slight damage to Property</p></td>
                    <td width="16%"><p>Reportable Injury moderate loss of Process or limited damage to Property</p></td>
                    <td width="16%"><p>Major Injury, Single Fatality critical loss of Process/damage to Property</p></td>
                    <td width="16%"><p>Multiple Fatalities Castastrophic Loss of Business</p></td>
                </tr>
                <tr class="text-center">
                    <td><b>Likelihood of the hazard happening</b></td>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                </tr>

                <tr class="text-center number">
                    <td class="sideTitle">Almost Certain<br/>50</td>
                    <td class="green-btn">50</td>
                    <td class="yellow-btn">100</td>
                    <td class="red-btn">150</td>
                    <td class="majordanger-btn">200</td>
                    <td class="majordanger-btn">250</td>
                </tr>
                <tr class="text-center number"> 
                    <td class="sideTitle">Will probably occur<br/>40</td>
                    <td class="green-btn">40</td>
                    <td class="yellow-btn">80</td>
                    <td class="red-btn">120</td>
                    <td class="majordanger-btn">160</td>
                    <td class="majordanger-btn">250</td>
                </tr>
                <tr class="text-center number">
                    <td class="sideTitle">Possible occur<br/>30</td>
                    <td class="green-btn">30</td>
                    <td class="yellow-btn">60</td>
                    <td class="yellow-btn">90</td>
                    <td class="red-btn">120</td>
                    <td class="red-btn">150</td>
                </tr>
                <tr class="text-center number">
                    <td class="sideTitle">Remote possibility<br/>20</td>
                    <td class="green-btn">20</td>
                    <td class="green-btn">40</td>
                    <td class="yellow-btn">60</td>
                    <td class="yellow-btn">80</td>
                    <td class="yellow-btn">100</td>
                </tr>
                <tr id="extreme" class="text-center number">
                    <td class="sideTitle">Extremely Unlikely<br/>100</td>
                    <td class="green-btn">10</td>
                    <td class="green-btn">20</td>
                    <td class="green-btn">30</td>
                    <td class="green-btn">40</td>
                    <td class="green-btn">50</td>
                </tr>
                </tbody>
                </table>
                </div>
                <div class="form-group">
                    <div class="col-sm-8">
                    <button type="button" id="riskAssessmentBtn" class="btn btn-success">Add Score</button>
                    </div>
                </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->

<!-- SHEQF Notes Modal -->
  <div class="modal fade" id="sheqfNotes-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">SHEQF Notes</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="sheqfNotesForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="group" class="form-control">
                    <option value selected disabled>Please select a group</option>
                    <option value="1">OHS (Health & Safety)</option>
                    <option value="2">E (Environment)</option>
                    <option value="3">Q (Quality)</option>
                    <option value="4">F (Food)</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="topic">Topic</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="topic" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notes">Notes</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" id="notes" required placeholder=""></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="sheqfNotesBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
   
</div><!--End of col-lg-10 --> 
</div>
<!--End of container-fluid -->
<script>    
    $(function() {
    $("#controlSuggestedDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    $("#sheqfDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
        
    $('input[name="controlInEffect"]').click(function(){
            if($(this).attr("value")==="1"){
                $(".hidden-div").not(".control").hide();
                $(".control").show();
            }else{
                $(".control").hide();
            }
    });
    $('input[name="controlInEffect"]').click(function(){
            if($(this).attr("value")==="0"){
                $(".hidden-div2").not(".controlYes").hide();
                $(".controlYes").show();
            }else{
                $(".controlYes").hide();
            }
    });
    });
    $("#riskAssessmentBtn").click(function(){
        $("#risk-assessment-score-modal").modal('toggle');
    });

    $('.green-btn, .yellow-btn, .red-btn, .majordanger-btn').click( function() {
      $(this).parents('table').find('td').each( function( index, element ) {
          $(element).removeClass('on');
      } );
      $(this).addClass('on');
    } );
    
    $( ".number td:not(.sideTitle)" ).click(function() {
    //alert($(this).text());
    $("#pscore").text($(this).text());
    });
</script>
<?php 
/*
 * 
 * 
 * 
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    