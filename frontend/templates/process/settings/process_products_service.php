<?php 
$title = 'Process Products / Services';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of col-lg-2 side-bar --> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=process&action=defaultAction">Process Dash</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_stock_supply">Stock / Supply</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_risk_controls">Risk / Controls</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_rps">RPS</a></li>
            <li><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_control_sample_point">Control / Sample Point</a></li>
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=process&action=view_products_services">Products / Service</a></li>
        </ul>
        </div>
    </div>  
    <!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#addProductService-modal" data-toggle="modal" data-target="#addProductService-modal"><button type="button" id="addProductService" class="btn btn-success">Add Product / Service</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Product / Service</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Code</th>
                    <th>Product / Service</th>
                    <th>Description of final product / service</th>
                    <th>Date Added</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                  <?php
               $counter = 0;                
               if($data['allProductsServices']): foreach($data['allProductsServices'] as $pros):
                   $counter++;
                   ?>
                            <tr>
                                <td><?php echo $counter;?></td>
                                <td><?php echo $pros['prod_service']; ?></td>
                                <td><?php echo $pros['description']; ?></td>                                
                                <td><?php echo $pros['date_created']; ?></td>
                                <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                 <td></td>
                            </tr>
                        <?php endforeach;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!-- Add Product Service Step Modal -->
  <div class="modal fade" id="addProductService-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Product / Service</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addProductServiceForm" action="<?php echo BASE_URL;?>/index.php?action=post_product_service&module=process">
            <div class="form-group">
                <label class="control-label col-sm-4" for="product">Product</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="product" id="product" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="description">Description of final product/service</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="description" id="description" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addProductServiceBtn" name="addProductServiceBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div><!--End of col-lg-10 --> 
</div>
<!--End of container-fluid -->
<?php 
/*
 * 
 * 
 * 
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    