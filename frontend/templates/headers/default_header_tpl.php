<?php
/**
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

include_once('classes/page_class.php');

$objPage->startHeader();
$objPage->setTitle($title);

//include stylesheet
$externalstylesheet = array('http://fonts.googleapis.com/css?family=Oswald:400,700,300',
 'http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700',
    'http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
                            HOSTNAME . '/styles/jquery-ui-1.10.4.custom.min.css',
                            HOSTNAME . '/styles/font-awesome.min.css',
                            HOSTNAME . '/styles/bootstrap.min.css',
                            HOSTNAME . '/styles/all.css',
                            HOSTNAME . '/styles/main.css',
                            HOSTNAME . '/styles/style-responsive.css',
                            HOSTNAME . '/styles/fullcalendar.min.css',
                            HOSTNAME . '/styles/fullcalendar.print.min.css',
                            'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css',
                            HOSTNAME . '/styles/bootstrap-select.css');

$stylesheet = array('responsive.css','dataTables.bootstrap.css', 'dataTables.responsive.css');
$objPage->includeExternalStyle($externalstylesheet);
$objPage->includeStyle($stylesheet);

//$javascript = array('custom.js', 'dataTables.bootstrap.min.js', 'dataTables.responsive.js', 'jquery.dataTables.min.js');
$javascript = array('custom.js','jquery.ckie.js','sheq.documents.js');
$externaljavascript = array(HOSTNAME .'/vendor/components/jquery/jquery.min.js',
                            HOSTNAME .'/vendor/components/jqueryui/ui/jquery-ui.js');

                            /*HOSTNAME .'/vendor/components/bootstrap/js/bootstrap.min.js',
                            HOSTNAME .'/vendor/select2/select2/dist/js/select2.min.js',
                            HOSTNAME .'/vendor/trentrichardson/jquery-timepicker-addon/dist/jquery-ui-timepicker-addon.js',
                            HOSTNAME .'/vendor/tinymce/tinymce/tinymce.min.js',
                            HOSTNAME . '/vendor/bootstrap-select/bootstrap-select/dist/js/bootstrap-select.min.js',
                            HOSTNAME . '/frontend/js/jquery.babypaunch.spinner.min.js',
                            HOSTNAME . '/vendor/grimmlink/toastr/build/toastr.min.js');*/
$objPage->includeExternalScript($externaljavascript);
$objPage->includeScript($javascript);


?>   
<script src="http://41.185.28.112/sheqmockup/js/sweetalert.js"></script>
<link rel="stylesheet" href="http://41.185.28.112/sheqmockup/css/sweetalert.css"><style>
  body {
    min-width: 520px;
  }
  .column {
    /*width: 170px;*/
    float: left;
    /*padding-bottom: 100px;*/
  }
  .portlet {
    margin: 0 1em 1em 0;
    padding: 0.3em;
  }
  .portlet-header {
    padding: 0.2em 0.3em;
    margin-bottom: 0.5em;
    position: relative;
  }
  .portlet-toggle {
    position: absolute;
    top: 50%;
    right: 0;
    margin-top: -8px;
  }
  .portlet-content {
    padding: 0.4em;
  }
  .portlet-placeholder {
    border: 1px dotted black;
    margin: 0 1em 1em 0;
    height: 50px;
  }
  </style>    
    <script>
        var AJAX_ROOT = "<?php echo BASE_URL; ?>/index.php?";
   function getMoreInfoDropDownList(element,next_element,url)
   {
     var selected_id = $(element).val();
     var obj = $(next_element);
     var ur = url;;
     $.ajax({
        type:"GET",
        url:ur,
        dataType:"json",
        data:{group_id:selected_id},
        success:function(result)
        {
             var rs = JSON.stringify(result);
             var action = JSON.parse(rs);
             $(obj).find('option').remove();   
             var row = "";
             $(action).each(function(i,val)
              {        
                  row = "<option value='"+val.option_key+"'>"+val.value+"</option>";
                  $(obj,' .selectpicker').append(row);                                 
              });                
              
              $(obj,' .selectpicker').selectpicker('refresh');
        }
     });
   }
    </script>
   
<?php
$objPage->endHeader();
