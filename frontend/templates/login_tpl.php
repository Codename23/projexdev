<?php 
$title = 'Login';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<style>
body {
    background: url('http://www.tinx-it.com/wp-content/uploads/2016/03/bigstock-business-technology-developm-82683851.jpg')center center no-repeat;
    background-size: cover;
}
.panel{
    border:1px solid #ccc;
    background:#fff;
    opacity: 0.9;
}
</style>
<div class="container-fluid">
    <div class="row">
    <div class="col-md-12 text-center">
	<br/>
        <img src="http://41.185.28.112/frontendmockup/images/logo2.png" alt="logo" /> <span style="    text-overflow: ellipsis;
    overflow: hidden;
    font-weight: 400;
    font-size: 22px;
    width: 100%;
    color: #fff !important;
    margin-left: 0 !important;
    display: block;
    height: 61px;
    margin: 0;
	text-align:center;
    padding-left: 10px;">ProjeX</span>
    </div>
    </div>
</div>
<div class="container">
    <div class="row">
    <div class="col-md-6 col-md-offset-3">
	<div class="login-panel panel panel-default">
	<div class="panel-heading">Please Sign In</div>
        <div class="panel-body">
        <form class="form-horizontal" name="loginForm" id="loginform" action="index.php" method="post">
            <div class="form-group">
                <label class="control-label col-sm-4" for="username">Username/Email</label>
                <div class="col-sm-8">
                <input type="text" class="form-control" name="username" id="username" required placeholder="Enter your username">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="password">Password</label>
                <div class="col-sm-8">
                <input type="password" class="form-control" name="password" id="password" required placeholder="Enter your password">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4"><input type="checkbox" name="rememberMe"></label>
                <div class="col-sm-8">
                <label class="control-label" for="password">Remember me</label>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4"></label>
                <div class="col-sm-8">
                <label class="control-label" for=""><a href="company_registration.php">Create Account</a></label>
                | <label class="control-label" for=""><a href="forgot_password.php">Forgot Password</a></label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <input name="action" value="processlogin" type="hidden">
                <input name="module" value="user" type="hidden">
                <button type="submit" name="login" class="btn btn-success">Login</button>
                </div>
            </div>
        </form>
        </div>
	</div>
        </div>
    </div><!--End of row-->
</div><!--End of container-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php');?>