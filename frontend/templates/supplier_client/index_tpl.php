<?php
$title = 'Supplier/Clients Dashboard';

include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */

    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <!--End of navigation-->

    <!--<h1 class="heading-title">Workspace </h1>-->

    <?php
    /*
     * Include side menu from the include file
     */
    include_once('frontend/templates/menus/side-menu.php');
    ?>

    <div class="col-lg-10">

        <!--sub menu-->
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-justified topbar-menu">
                    <li><a href="<?php echo BASE_URL . "/index.php?module=provider&action=viewAllProviders"; ?>">Service Providers</a></li>
                    <li><a href="<?php echo BASE_URL . "/index.php?module=client&action=viewAllClients"; ?>">Customers</a></li>
                    <li><a href="<?php echo BASE_URL . "/index.php?module=supplier&action=viewAllSuppliers"; ?>">Suppliers</a></li>
                </ul>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>
            <!-- /.panel-heading -->
            <div class="panel-body text-center">
                <div class="row">
                    <div class="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                        <div class="info-box blue-bg">
                            <div class="title">Total Service Providers Onsite Today</div>
                            <div class="count">9</div>
                        </div><!--/.info-box-->
                    </div>
                    <div class="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                        <div class="info-box brown-bg">
                            <div class="title">Total Customer Complaints</div>
                            <div class="count">
                                <a href="#" data-toggle="modal" data-target="#chart-modal" class="chart-modal"><i class="glyphicon glyphicon-signal modal-icon"></i></a>

                                <div class="modal fade" id="chart-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <!--<div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                              <h4 class="modal-title" id="myModalLabel">Assortment Analysis</h4>
                                            </div>-->
                                            <div class="modal-body">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div id="container"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--/.info-box-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                        <div class="info-box dark-bg">
                            <div class="title">Supplier Error</div>
                            <div class="count">
                                <a href="#" data-toggle="modal" data-target="#chart-modal2" class="chart-modal2"><i class="glyphicon glyphicon-stats modal-icon"></i></a>

                                <div class="modal fade" id="chart-modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">

                                            <div class="modal-body">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div id="container2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--/.info-box-->
                    </div>
                    <div class="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                        <div class="info-box green-bg">
                            <div class="title">Upcoming Contractor Work</div>
                            <div class="count">
                                <a href="#" data-toggle="modal" data-target="#upcoming-table-modal" class="upcoming-table-modal"><i class="glyphicon glyphicon-th-list modal-icon"></i></a>


                            </div>
                        </div><!--/.info-box-->
                    </div>
                </div>
                <div class="modal fade" id="upcoming-table-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Upcoming Contractor Work</h4>
                            </div>
                            <div class="modal-body">
                                <div class="table-responsive">
                                    <table width="100%" class="table table-striped table-hover" id="supplierDashboardUpcoming">
                                        <thead class="text-center">
                                        <tr>
                                            <th>Date</th>
                                            <th>Inspection Name</th>
                                            <th>Department</th>
                                            <th>Issue</th>
                                            <th>Description</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>15 June 2016</td>
                                            <td>Inspection</td>
                                            <td>Wood</td>
                                            <td></td>
                                            <td>Fire Equipment inspection register</td>
                                        </tr>
                                        <tr>
                                            <td>15 June 2016</td>
                                            <td>Inspection</td>
                                            <td>Wood</td>
                                            <td></td>
                                            <td>Fire Equipment inspection register</td>
                                        </tr>
                                        <tr>
                                            <td>15 June 2016</td>
                                            <td>Inspection</td>
                                            <td>Wood</td>
                                            <td></td>
                                            <td>Fire Equipment inspection register</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- /.panel-body -->
        </div>
    </div><!--End of row-->



</div><!--End of container-fluid-->

<?php
/*
 *
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script type="text/javascript">
    $(function () {
        Highcharts.chart('container', {
            title: {
                text: 'Monthly Average Customer Complaints',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Numbers of complaints'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: '°C'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Complaint Type 1',
                data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
            }, {
                name: 'Complaint Type 2',
                data: [1, 1.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
            }, {
                name: 'Complaint Type 3',
                data: [1.5, 1.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
            }, {
                name: 'Complaint Type 4',
                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
            }]
        });
        var chart = $('#container').highcharts();
        $('#chart-modal').on('show.bs.modal', function() {
            $('#container').css('visibility', 'hidden');
        });
        $('#chart-modal').on('shown.bs.modal', function() {
            $('#container').css('visibility', 'initial');
            chart.reflow();
        });
    });

    var chart;

    var options = {
        chart: {
            reflow: false,
            renderTo: 'container2',
            type: 'pie'
        },
        title: {
            text: 'Supplier Error',
            x: -20 //center
        },
        series: [{
            data: [33, 33, 33]
        }]
    };

    function createChart() {
        var chart = new Highcharts.Chart(options);
    }

    jQuery(document).ready(function ($) {
        createChart();
        var windowWidth = $(window).width();

        $(window).resize(function () {
            if ($(window).width() !== windowWidth) {
                windowWidth = $(window).width();
                createChart();
            }
        });

        $('#button').on('click', function () {
            createChart();
        });
        var chart = $('#container2').highcharts();
        $('#chart-modal2').on('show.bs.modal', function() {
            $('#container2').css('visibility', 'hidden');
        });
        $('#chart-modal2').on('shown.bs.modal', function() {
            $('#container2').css('visibility', 'initial');
            chart.reflow();
        });
    });
</script>
