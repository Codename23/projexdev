<?php 
/*
 * Header file
 */
$title = 'Statutory Plan';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
global $objAssets;
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=assets&action=statutory_maintenance_plan">Statutory Maintenance Plan</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=assets&action=statutory_upcoming">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=assets&action=job_listing">Job</a></li>
        <!--<li><a href="statutory_approval.php">Approval</a></li>
        <li><a href="statutory_scheduled.php">Scheduled</a></li>
        <li><a href="statutory_active.php">Active</a></li>-->
        <li><a href="statutory_history.php">History</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

    <div class="panel panel-default">
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table no-border-table">
            <thead>
                <tr>
                    <th width="6%">Department</th>
                    <th width="10%">Category</th>
                    <th width="6%">Type</th>
                    <th width="8%">Description</th>
                    <th width="25%">Number</th>
                </tr>
            </thead>
            <tbody>
                  <?php
                            if(!empty($data['assets'])){
                                foreach($data['assets'] as $assetDetail){                                
                                echo "<tr>                                                                                 
                                        <td>{$assetDetail['department_name']}</td>
                                        <td>{$assetDetail['categoryType']}</td>
                                        <td>{$assetDetail['assetType']}</td>
                                        <td>{$assetDetail['description']}</td>
                                        <td>{$assetDetail['company_asset_number']}</td>";                               
                                }
                            }
                            ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#addStatutoryMaintenance-modal" data-toggle="modal" data-target="#addStatutoryMaintenance-modal"><button type="button" id="addStatutoryMaintenance" class="btn btn-success">Add Statutory Maintenance</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Maintenance Plan</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Internal / External</th>
                    <th>Statutory Type</th>
                    <th>Description</th>
                    <th>Notes</th>
                    <th>Frequency</th>
                    <th>Next Date</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                    <?php
                            if(!empty($data['allStatutoryPlans'])){
                                foreach($data['allStatutoryPlans'] as $assetDetail){                                
                                echo "<tr>                                                                                 
                                        <td>{$assetDetail['int_ex']}</td>
                                        <td>{$assetDetail['type_id']}</td>
                                        <td>{$assetDetail['description']}</td>
                                        <td>{$assetDetail['notes']}</td>
                                        <td>{$assetDetail['frequency']}</td>                               
                                        <td>{$assetDetail['due_date']}</td>"
                                     . "<td></td></tr>";                               
                                }
                            }
                            ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!--Add Statutory Maintenance Modal -->
  <div class="modal fade" id="addStatutoryMaintenance-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Statutory Maintenance</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addStatutoryMaintenanceForm" action="<?php echo BASE_URL;?>/index.php?edit=<?php echo (int)$_GET["edit"];?>&action=postStatutoryPlan&module=assets">
                <input type="hidden" name="txt_id" value="<?php echo (int)$_GET["edit"];?>"/>                
                <div class="form-group">
                    <label class="control-label col-sm-4" for="internalExternal">Internal / External</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="form-control" name="internalExternal" id="internalExternal" required>
                        <option value="Internal">Internal</option>
                        <option value="External">External</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="typeService">Type Service / Maintenance</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="typeService" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                        <option value="1">Type 1</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="description">Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="description" id="description" required placeholder=""></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Notes</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="notes" id="notes" required placeholder=""></textarea>
                    </div>
                </div>
                <div class="form-group" id="frequency">
                    <label class="control-label col-sm-4" for="frequency">Frequency</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="form-control" name="frequency" id="frequency" required>
                        <option value selected disabled>Please select a frequency</option>
                        <option value="Daily">Daily</option>
                        <option value="Weekly">Weekly</option>
                        <option value="Monthly">Monthly</option>
                        <option value="Every 6 Months">Every 6 Months</option>
                        <option value="Yearly">Yearly</option>
                        <option value="Every 2 Years">Every 2 Years</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="dueDate">Due Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="dueDate" id="dueDate" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" id="addBtn" name="addBtn" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<script>
$(function() {
    $("#dueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
});
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');  
?>  
    