<?php 
/*
 * Header file
 */
include_once('includes/header.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('includes/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('includes/side-menu.php'); 
?> 
<!--End of navigation--> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="resource.php">All Assets</a></li>
        <li><a href="resource_unlisted.php">Unlisted Resources</a></li>
        <li><a href="resource_statutory_maintenance.php">Statutory Maintenance</a></li>
        <li><a href="resource_calibration.php">Calibration</a></li>
        <li><a href="resource_nonconformance_history.php">Non-Conformance History</a></li>
        <li class="active"><a href="resource_settings.php">Settings</a></li>
        <li><a href="resource_archive.php">Archive</a></li>
    </ul>
    </div>
    </div>
    
    <div class="row">    
    <div class="col-lg-8">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="resource_settings.php">Resource Types</a></li>
        <li><a href="resource_settings_statutory_maintenance.php">Statutory Maintenance Types</a></li>
        <li class="active"><a href="resource_settings_calibration.php">Calibration Types</a></li>
    </ul>
    </div>
    </div>
    <!--End of sub menu-->

    <div class="panel panel-default">
        <div class="panel-body">
        <button type="button" onclick="window.location.href=''" id="addNew" class="btn btn-success">Add New</button>
        </div>
    </div> 
    <!--End of panel panel-default-->

    <div class="panel panel-default">
    <div class="panel-heading">Calibration Types</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="resourceTbl">
            <thead>
                <tr>
                    <th>List of Calibration Types</th>
                    <th>Measured in</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
        </div>
</div><!--End of container-fluid-->
<script src="js/bootstrap-select.js"></script>
<script>

</script>
<?php 
/*
 * 
 */
include_once('includes/footer.php');
?>  
    