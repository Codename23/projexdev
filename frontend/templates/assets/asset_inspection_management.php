<?php
$title = 'Asset Management';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');

?>

<div class="container-fluid">
        <?php include_once('frontend/templates/menus/side-menu.php'); ?>
        <!--sub menu-->
        <div class="row">    
            <div class="col-lg-12">
            <ul class="nav nav-pills nav-justified topbar-menu">
                <li class="active"><a href="index.php">All Assets</a></li>
                <li><a href="index.php">Info</a></li>
                <li><a href="index.php">SHEQ</a></li>
                <li><a href="index.php">Statutory History</a></li>
                <li><a href="index.php">Maintenance Record</a></li>
            </ul>
            </div>
        </div>
        <!--End of sub menu-->
        <!--Start of main col-->
        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
            <div class="panel-group">  
                <div class="panel panel-default">
                <div class="panel-heading"><h4>Statutory inspection management</h4></div>
                <div class="panel-body"> 
                    <div class="table-responsive">          
                    <table class="table">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Department</th>
                          <th>Category</th>
                          <th>Resource Type</th>
                          <th>Description Name</th>
                          <th>Company Resource Number</th>
                          <th>Manufacturer/Brand</th>
                          <th>Next Date</th>
                          <th>Type</th>
                          <th>Schedule</th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php
                          $count = 1;
                            if(!empty($data['assets'])){
                                foreach($data['assets'] as $assetDetail){
                                echo '<tr>
                                        <td>' . $count . '</td>
                                        <td>' . $assetDetail['department_name'] . '</td>
                                        <td>' . $assetDetail['categoryType'] . '</td>
                                        <td>' . $assetDetail['assetType'] . '</td>
                                        <td>' . $assetDetail['name'] . '</td>
                                        <td>' . $assetDetail['company_asset_number'] . '</td>
                                        <td>' . $assetDetail['manufacture_name'] . '</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>'; 
                                if($assetDetail['is_active']){
                                    echo '<td><a data-toggle="tooltip" title="Deactivate asset" href="index.php?module=assets&action=deactivateAsset&id=' . $assetDetail['id'] . '"><span style="color:red" class="glyphicon glyphicon-remove"></span></td>';    
                                }else{
                                    echo '<td><a data-toggle="tooltip" title="Activate asset" href="index.php?module=assets&action=activateAsset&id=' . $assetDetail['id'] . '"><span style="color:green" class="glyphicon glyphicon-ok"></span></td>'; 
                                }           
                                echo '<td><a data-toggle="tooltip" title="Edit asset" href="index.php?module=assets&action=editAsset&id=' . $assetDetail['id'] . '&departmentId=' . $assetDetail['departmentId'] . '"><span style="color:black" class="glyphicon glyphicon-pencil"></span></td>
                                      <td><a data-toggle="tooltip" title="Delete asset" href="index.php?action=deleteAsset&module=assets&id=' . $assetDetail['id'] . '"><span style="color:black" class="glyphicon glyphicon-trash"></span></a></td></tr>';
                                    $count ++;
                                }    
                            }else{
                                 echo "<tr><td>There is no assets yet</td></tr>";
                            }
                            
                            ?>
                      </tbody>
                    </table>
                    </div>                    
                </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form method="post" name="addAssets" action="index.php">
                            <input type="hidden" name="action" value="addAsset">
                            <input type="hidden" name="module" value="assets">
                            <button type="submit" name="" class="btn btn-success">Add New Resource</button>
                        </form>
                    </div>
                </div>
                <!--End of panel-default-->
            </div>
        </div>
        <!--End of main col-->
    </div>

<script>
    $(document).on("click", ".trash", function(){
        var assetId = $(this).data('id');
        $('.modal-body #assetId').val(assetId);  
    });
    
    
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
    
    $(document).ready(function(){
        function search(){
            var searchterm = $("#searchAsset").val();
            if(searchterm !== ""){
                $("#result").html("<img alt='ajax search' src='ajax-loader.gif' />");
                $.ajax({
                    type:"post",
                    url:"frontend/templates/assets/search_asset.php",
                    data:"searchterm="+searchterm,
                    success:function(data){
                        $("#result").html(data);
                        $("#searchAsset").val("");
                    }
                });
            }
        }
        
        $("#buttonSearch").click(function(){
            search();
        });
        
        $("#searchAsset").keyup(function(e){
            if(e.keyCode == 13){
               search();  
            }
        });
        
    });
    
</script>
<?php include_once('frontend/templates/footers/default_footer_tpl.php');?>