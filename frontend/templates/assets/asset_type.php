<?php
/**
 * Ajax script to search resource type
 * 
 * @package sheqonline
 * @author Sunny Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
//Include the common file
require_once '../../../config.php';

processAjax();

function processAjax() 
{
    global $db;
    $categoryid = $_POST["categoryId"];   

    $sql = "SELECT * FROM tbl_asset_type WHERE category_id = " . $db->sqs($categoryid);
    $response = $db->getall($sql);
    
    if(!empty($response)){
    ?>
        <select id="assetType" name="assetType" required>
        <?php
        foreach($response as $resourceTypeDetailsArr){
           echo '<option value="' . $resourceTypeDetailsArr['id'] . '">' . $resourceTypeDetailsArr['name'] . '</option>';        
        }
        ?>
        </select>     
    <?php
    }else{
        echo "<p>No matched found for <b>$serchterm</b></p>";
    }
     
}
?> 