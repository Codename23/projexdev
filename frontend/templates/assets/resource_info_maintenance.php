<?php 
/*
 * Header file
 */
$title = "Maintenance";
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php');
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
$id =  (int)$_REQUEST['id'];
?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=assetmanagement&module=assets">All Assets</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=view&module=assets&id=<?php echo $id;?>">Info</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_documents&module=assets&id=<?php echo $id;?>">Documents</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_competence&module=assets&id=<?php echo $id;?>">Competence</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_substance&module=assets&id=<?php echo $id;?>">Substance</a></li>
        <!--<li><a href="resource_info_sheq.php">SHEQ</a></li>-->
        <!--<li><a href="resource_info_statutory.php">Statutory</a></li>
        <li><a href="resource_info_calibration.php">Calibration</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_inspected&module=assets&id=<?php echo $id;?>">Inspected</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_sop&module=assets&id=<?php echo $id;?>">SOP</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_maintenance&module=assets&id=<?php echo $id;?>">Maintenance</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_noc&module=assets&id=<?php echo $id;?>">Non-Conformance</a></li>
    </ul>
    </div>
</div>

<div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="resource_info_maintenance.php">SMP</a></li>
        <li><a href="resource_info_maintenance_active.php">Active</a></li>
        <li><a href="resource_info_maintenance_preventative.php">Preventative</a></li>
        <li><a href="resource_info_maintenance_statutory.php">Statutory</a></li>
        <li><a href="resource_info_maintenance_calibration.php">Calibration</a></li>
        <li><a href="resource_info_maintenance_components.php">Components</a></li>
        <li><a href="resource_info_maintenance_parts.php">Parts</a></li>
    </ul>
    </div>
</div>
<!--End of sub menu-->

<div class="panel panel-default">
<div class="panel-body">
    <a href="#-modal" data-toggle="modal" data-target="#-modal"><button type="button" id="" class="btn btn-success">Safe Maintenance Procedure Types</button></a>
</div>
</div>

<div class="panel panel-default">
<div class="panel-heading">SMP</div>
<div class="panel-body">
<div class="table-responsive">
    <table width="100%" class="table table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Type of Maintenance</th>
                <th>Description</th>
                <th>Link To Component</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Preventative</td>
                <td>Engine Service</td>
                <td>Engine</td>
                <td><a href="resource_info_maintenance_smp_description.php">Description</a></td>
                <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
            </tr>
            <tr>
                <td>2</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
            </tr>
        </tbody>
    </table>
</div>
</div>
</div>
<!--End of the panel panel-default-->

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');  
?>  
    