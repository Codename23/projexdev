<?php
$title = 'View Asset';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>
<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); 
$id =  (int)$_REQUEST['id'];
?>  
<div class="col-lg-10">
<!--sub menu-->
<div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=assetmanagement&module=assets">All Assets</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=view&module=assets&source_type=Resource&id=<?php echo $id;?>">Info</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_documents&module=assets&source_type=Resource&id=<?php echo $id;?>">Documents</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_competence&module=assets&source_type=Resource&id=<?php echo $id;?>">Competence</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_substance&module=assets&source_type=Resource&id=<?php echo $id;?>">Substance</a></li>
        <!--<li><a href="resource_info_sheq.php">SHEQ</a></li>-->
        <!--<li><a href="resource_info_statutory.php">Statutory</a></li>
        <li><a href="resource_info_calibration.php">Calibration</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_inspected&module=assets&source_type=Resource&id=<?php echo $id;?>">Inspected</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_sop&module=assets&source_type=Resource&id=<?php echo $id;?>">SOP</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_maintenance&module=assets&source_type=Resource&id=<?php echo $id;?>">Maintenance</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_noc&module=assets&source_type=Resource&id=<?php echo $id;?>">Non-Conformance</a></li>
    </ul>
    </div>
</div>
<!--End of sub menu-->

<div class="panel panel-default">
    <!--<div class="panel-heading">Personal Details</div>-->
    <div class="panel-body">
        
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th colspan="2">Resource Details</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td width="30%">Department</td>
                    <td><?php echo $data['viewassets']['department_name']; ?></td>
                </tr>
                <tr>
                    <td>Resource Category</td>
                    <td><?php echo $data['viewassets']['categoryType']; ?></td>
                </tr>
                <tr>
                    <td>Resource Type</td>
                    <td><?php echo $data['viewassets']['assetType']; ?></td>
                </tr>
                <tr>
                    <td>Description Name</td>
                    <td><?php echo $data['viewassets']['description']; ?></td>
                </tr>
                <tr>
                    <td>Inventory Number</td>
                    <td><?php echo $data['viewassets']['company_asset_number']; ?></td>
                </tr>
                <?php 
                    $count = 0;
                    $answersDetsArr = array();
                    $answersDetsArr = explode(',' , $data['viewassets']['question_answers']);

                    foreach($data['assetQuestions'] as $QuestionDetailsArr){
                        echo '<tr>
                                <td>' . $QuestionDetailsArr['description'] .'</td>
                                <td>';
                                if(!empty($data['viewassets']['question_answers'])){
                                    if($answersDetsArr[$count] == 1){
                                        echo '<span style="color:green" class="glyphicon glyphicon-ok"></span> Yes';
                                    } else{
                                        echo '<span style="color:red" class="glyphicon glyphicon-remove"></span> No';
                                    } 
                                }
                        echo '</td></tr>'; 
                        $count ++;
                    }    
                ?>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                    <td colspan="2"><b>Additional Information</b></td>
                </tr>              
                <tr>
                  <td>Manufacture/Brand</td>
                  <td><?php echo $data['viewassets']['manufacture_name']; ?></td>
                </tr>
                <tr>
                  <td>Serial Number</td>
                  <td><?php echo $data['viewassets']['serial_number']; ?></td>
                </tr>
                <tr>
                  <td>Date Purchased</td>
                  <td><?php echo date("d M Y ", strtotime($data['viewassets']['date_purchased'])); ?></td>
                </tr>
                <tr>
                  <td>Cost</td>
                  <td><?php echo 'R' . $data['viewassets']['cost']; ?></td>
                </tr>
                <tr>
                  <td>Notes</td>
                  <td><?php echo $data['viewassets']['notes']; ?></pre></td>
                </tr>
                <tr>
                  <td>Photo</td>
                  <td><?php ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    
    </div>
    </div>
<!--End of the panel panel-default-->

</div>
</div><!--End of container-fluid-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php');  
    