<?php 
/*
 * Header file
 */
$title = 'Statutory';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
//set the inspection_id

?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li ><a href="<?php echo BASE_URL;?>/index.php?module=assets&action=statutory_maintenance_plan">Statutory Maintenance Plan</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=assets&action=statutory_upcoming">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=assets&action=job_listing">Job</a></li>
        <!--<li><a href="statutory_approval.php">Approval</a></li>
        <li><a href="statutory_scheduled.php">Scheduled</a></li>
        <li><a href="statutory_active.php">Active</a></li>-->
        <li><a href="statutory_history.php">History</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

    <div class="panel panel-default">
    <div class="panel-heading">Search Statutory Plan</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Asset Type</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                <option>Forklift</option>
                <option>Wood</option>
                <option>Shutter</option>
                <option>Petrol</option>
                <option>Fire Extinguishers</option>
            </select> 
        </div>
        </div>
        
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Service Month</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more months" data-header="Close">
                <option>January</option>
                <option>February</option>
                <option>March</option>
                <option>April</option>
                <option>May</option>
                <option>June</option>
                <option>July</option>
                <option>August</option>
                <option>September</option>
                <option>October</option>
                <option>November</option>
                <option>December</option>
            </select> 
        </div>
        </div>
            
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Status</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more status types" data-header="Close">
                <option value="1">Due in a month</option>
                <option value="2">Due in 2 months</option>
                <option value="3">Overdue</option>
                <option value="4">Good</option>
            </select> 
        </div>
        </div>
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#createJob-modal" data-toggle="modal" data-target="#createJob-modal"><button type="button" id="createJob" class="btn btn-success">Create Job</button></a>
    </div>
    </div>

<form class="form-horizontal" method="post" name="createJobInternalForm" action="<?php echo BASE_URL;?>/index.php?module=assets&action=create_statutory_job">
    <div class="panel panel-default">
    <div class="panel-heading">Upcoming</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="upcomingTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Number</th>
                    <th>Internal / External</th>
                    <th>Service Type</th>
                    <th>Description</th>
                    <th>Notes</th>
                    <th>Next Service Due</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                            if(!empty($data['allUpcomingStatutory'])){
                                foreach($data['allUpcomingStatutory'] as $assetDetail){                                
                                echo '<tr>      
                                        <td><input type="checkbox" name="upcoming[]" value="'.$assetDetail["plan_id"].'"/></td>
                                        <td>'.$assetDetail["department_name"].'</td>
                                        <td>'.$assetDetail["categoryType"].'</td>
                                        <td>'.$assetDetail["assetType"].'</td>
                                        <td>'.$assetDetail["asset_description"].'</td>
                                        <td>'.$assetDetail["company_asset_number"].'</td>
                                        <td>'.$assetDetail["int_ex"].'</td>                                        
                                        <td>'.$assetDetail["type_id"].'</td>
                                        <td>'.$assetDetail["maintenance_description"].'</td>
                                        <td>'.$assetDetail["maintenance_notes"].'</td>                             
                                        <td>'.$assetDetail["due_date"].'</td>
                                        <td class="yellow-table-bg">Due In A Month</td><td><a href="#">View Resource</a></td></tr>';                               
                                }
                            }
                            ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!-- Create Job Modal -->
  <div class="modal fade" id="createJob-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Job</h4>
        </div>
        <div class="modal-body">            
            <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Job Details</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="description">Description</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="description" id="description" required placeholder=""> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="scheduledStartDateTime">Scheduled Start Date & Time</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="scheduledStartDateTime" id="scheduledStartDateTime" required placeholder=""> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="endDateTime">End Date & Time</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="endDateTime" id="endDateTime" required placeholder=""> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="person_responsible_selected" id="person_responsible_selected"/>
                                        <label class="control-label col-sm-4" for="responsiblePerson">Responsible Person</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" id="responsible_person" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people" data-header="Close">
                                          
                                                <?php
                            foreach($data['allEmployees'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>  
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="departments_selected" id="departments_selected"/>
                                        <label class="control-label col-sm-4" for="departmentInvolved">Department Involved</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" id="search_departments" multiple="multiple" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                                                                        <?php
                                                   foreach($data['allDepartments'] as $group)
                                                   {
                                                       echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                                                   }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Job Description</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="maintenanceType">Maintenance Type</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" name="maintenanceType" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                                            <option value="1">Service</option>
                                            <option value="2">Maintenance</option>
                                            <option value="3">Replacement</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="fullJobDescription">Job Details</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <textarea rows="5" class="form-control" name="fullJobDescription" id="fullJobDescription" required placeholder=""></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Risk Factor</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="riskLevel">Risk Level</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" name="risk_level" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a level">
                                            <option value="Low">Low</option>
                                            <option value="Medium">Medium</option>
                                            <option value="High">High</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="hazardTypes">Hazard Types</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" name="hazardTypes" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more types" data-header="Close">
                                            <option>Working at heights</option>
                                            <option>Electricity</option>
                                            <option>Machines</option>
                                            <option>Hot work</option>
                                            <option>Noise</option>
                                            <option>Fumes</option>
                                            <option>Vessels under pressure</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Supplier</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="companyName">Company Name</label>
                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" name="companyName" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a company">
                                                 <?php
                            foreach($data['allSupplier'] as $group)
                            {
                                echo "<option value='{$group["id"]}'>{$group['supplier_name']}</option>";
                            }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Resource / Tools to be used in the maintenance</a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                         <input type="hidden" class="form-control" name="resources_used" id="resources_used" placeholder=""> 
                                        <label class="control-label col-sm-4" for="nonListedDescription">Non Listed Description</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="nonListedDescription" id="nonListedDescription" required placeholder=""> 
                                        </div>
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8">
                                    <a href="#importFromAssets-modal" data-toggle="modal" data-target="#importFromAssets-modal"><button type="button" id="importFromAssets" class="btn btn-success">Add</button></a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table width="100%" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Category</th>
                                                <th>Type</th>
                                                <th>Description</th>
                                                <th>No</th>
                                                <th>Latest Non-Conformance</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Moveable Asset</td>
                                                <td>Ladder</td>
                                                <td>Aluminium Ladder</td>
                                                <td>12</td>
                                                <td><a href="">View</a></td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Parts</a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="parts_used" id="parts_used" placeholder="">
                                        <label class="control-label col-sm-4" for="partsNotes">Parts Notes</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="partsNotes" id="partsNotes" placeholder=""> 
                                        </div>
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8">
                                    <a href=""><button type="button" id="" class="btn btn-success">Add Part</button></a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table width="100%" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Type</th>
                                                <th>Description</th>
                                                <th>Measurements</th>
                                                <th>Quantity</th>
                                                <th>In Stock / to be Purchased</th>
                                                <th>Suggested Supplier</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Glass</td>
                                                <td>Safety Window</td>
                                                <td>2 metres x 1 metre</td>
                                                <td>1 unit</td>
                                                <td>TBP</td>
                                                <td>PG Glass</td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Approval</a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <input type="hidden" class="form-control" name="approval_required" id="approval_required" placeholder="">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="sheqfRequest">SHEQF</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <a href=""><button type="button" id="" class="btn btn-success">Request</button></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="financialRequest">Financial</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <a href=""><button type="button" id="" class="btn btn-success">Request</button></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="managerialRequest">Managerial</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <a href=""><button type="button" id="" class="btn btn-success">Request</button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8">
                    <button type="submit" id="createJobBtn" name="createJobBtn" class="btn btn-success">Create Job</button>
                    </div>
                </div>                
                <!--End of panel-default-->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
</form>
<!--End Of Modal -->
    <!--End of the panel panel-default-->

<!-- Create Job Modal -->
<form class="form-horizontal" method="post" name="createJobInternalForm" action="<?php echo BASE_URL;?>/index.php?module=assets&action=create_statutory_job">
  <div class="modal fade" id="createJob-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Job</h4>
        </div>
        <div class="modal-body">            
            <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Job Details</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="description">Description</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="description" id="description" required placeholder=""> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="scheduledStartDateTime">Scheduled Start Date & Time</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="scheduledStartDateTime" id="scheduledStartDateTime" required placeholder=""> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="endDateTime">End Date & Time</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="endDateTime" id="endDateTime" required placeholder=""> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="person_responsible_selected" id="person_responsible_selected"/>
                                        <label class="control-label col-sm-4" for="responsiblePerson">Responsible Person</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" id="responsible_person" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people" data-header="Close">
                                          
                                                <?php
                            foreach($data['allEmployees'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>  
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="departments_selected" id="departments_selected"/>
                                        <label class="control-label col-sm-4" for="departmentInvolved">Department Involved</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" id="search_departments" multiple="multiple" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                                                                        <?php
                                                   foreach($data['allDepartments'] as $group)
                                                   {
                                                       echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                                                   }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Job Description</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="maintenanceType">Maintenance Type</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" name="maintenanceType" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                                            <option value="1">Service</option>
                                            <option value="2">Maintenance</option>
                                            <option value="3">Replacement</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="fullJobDescription">Job Details</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <textarea rows="5" class="form-control" name="fullJobDescription" id="fullJobDescription" required placeholder=""></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Risk Factor</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="riskLevel">Risk Level</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" name="risk_level" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a level">
                                            <option value="Low">Low</option>
                                            <option value="Medium">Medium</option>
                                            <option value="High">High</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="hazardTypes">Hazard Types</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" name="hazardTypes" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more types" data-header="Close">
                                            <option>Working at heights</option>
                                            <option>Electricity</option>
                                            <option>Machines</option>
                                            <option>Hot work</option>
                                            <option>Noise</option>
                                            <option>Fumes</option>
                                            <option>Vessels under pressure</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Supplier</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="companyName">Company Name</label>
                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                            <select class="selectpicker form-multiselect" name="companyName" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a company">
                                                 <?php
                            foreach($data['allSupplier'] as $group)
                            {
                                echo "<option value='{$group["id"]}'>{$group['supplier_name']}</option>";
                            }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Resource / Tools to be used in the maintenance</a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                         <input type="hidden" class="form-control" name="resources_used" id="resources_used" placeholder=""> 
                                        <label class="control-label col-sm-4" for="nonListedDescription">Non Listed Description</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="nonListedDescription" id="nonListedDescription" required placeholder=""> 
                                        </div>
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8">
                                    <a href="#importFromAssets-modal" data-toggle="modal" data-target="#importFromAssets-modal"><button type="button" id="importFromAssets" class="btn btn-success">Add</button></a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table width="100%" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Category</th>
                                                <th>Type</th>
                                                <th>Description</th>
                                                <th>No</th>
                                                <th>Latest Non-Conformance</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Moveable Asset</td>
                                                <td>Ladder</td>
                                                <td>Aluminium Ladder</td>
                                                <td>12</td>
                                                <td><a href="">View</a></td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Parts</a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="parts_used" id="parts_used" placeholder="">
                                        <label class="control-label col-sm-4" for="partsNotes">Parts Notes</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="partsNotes" id="partsNotes" placeholder=""> 
                                        </div>
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8">
                                    <a href=""><button type="button" id="" class="btn btn-success">Add Part</button></a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table width="100%" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Type</th>
                                                <th>Description</th>
                                                <th>Measurements</th>
                                                <th>Quantity</th>
                                                <th>In Stock / to be Purchased</th>
                                                <th>Suggested Supplier</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Glass</td>
                                                <td>Safety Window</td>
                                                <td>2 metres x 1 metre</td>
                                                <td>1 unit</td>
                                                <td>TBP</td>
                                                <td>PG Glass</td>
                                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Approval</a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <input type="hidden" class="form-control" name="approval_required" id="approval_required" placeholder="">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="sheqfRequest">SHEQF</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <a href=""><button type="button" id="" class="btn btn-success">Request</button></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="financialRequest">Financial</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <a href=""><button type="button" id="" class="btn btn-success">Request</button></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="managerialRequest">Managerial</label>
                                        <div class="col-lg-6 col-md-4 col-sm-8">
                                            <a href=""><button type="button" id="" class="btn btn-success">Request</button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8">
                    <button type="submit" id="createJobBtn" name="createJobBtn" class="btn btn-success">Create Job</button>
                    </div>
                </div>                
                <!--End of panel-default-->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
</form>
<!--End Of Modal -->
    
<!-- Import From Assets Modal -->
  <div class="modal fade" id="importFromAssets-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Import From Assets</h4>
        </div>
        <div class="modal-body table-responsive">
            <form class="form-horizontal" method="post" name="searchAssets" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="searchDepartment">Department</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                        <option>Warehouse</option>
                        <option>Receiving</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="searchType">Type</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more types" data-header="Close">
                        <option>Compressor</option>
                        <option>Forklift</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="button" id="searchAssetsBtn" class="btn btn-success">View</button>
                    </div>
                </div>
            </form>
            <table width="100%" class="table table-hover" id="importFromAssetsTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Number</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
            </tbody>
        </table>
        <div class="form-group">
            <div class="col-sm-8">
            <button type="button" id="importFromAssetsBtn" class="btn btn-success">Add</button>
            </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<script>
$(function(){
    $("#scheduledStartDateTime").datetimepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt', 
        maxDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datetimepicker();
    
    $("#endDateTime").datetimepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt',         
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datetimepicker();
});

$('#importFromAssetsTbl').tablecheckbox();
var chkbox = document.getElementsByClassName("chkbox");

$("#importFromAssetsBtn").click(function(){
    $("#importFromAssets-modal").modal('toggle');
});
  
    
    $('.selectpicker').on('change', function(){
              
        var selected = $(this," option:selected").val();
        focus_dropdown = $(this).parent().children('select').attr('id');
        switch(focus_dropdown)
        {
            case 'responsible_person':
                $("#person_responsible_selected").val(selected);
                console.log(selected);
                break;
           case 'search_departments':
               $("#departments_selected").val(selected);
               console.log(selected);
                break;     
        }       
    });
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php'); 
?>  
    