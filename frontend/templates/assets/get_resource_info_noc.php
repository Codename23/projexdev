<?php 
/*
 * Header file
 */
$title = "Non-Conformance";
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php');
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
$id =  (int)$_REQUEST['id'];
?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=assetmanagement&module=assets">All Assets</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=view&module=assets&id=<?php echo $id;?>">Info</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_documents&module=assets&id=<?php echo $id;?>">Documents</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_competence&module=assets&id=<?php echo $id;?>">Competence</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_substance&module=assets&id=<?php echo $id;?>">Substance</a></li>
        <!--<li><a href="resource_info_sheq.php">SHEQ</a></li>-->
        <!--<li><a href="resource_info_statutory.php">Statutory</a></li>
        <li><a href="resource_info_calibration.php">Calibration</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_inspected&module=assets&id=<?php echo $id;?>">Inspected</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_sop&module=assets&id=<?php echo $id;?>">SOP</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_maintenance&module=assets&id=<?php echo $id;?>">Maintenance</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_noc&module=assets&id=<?php echo $id;?>">Non-Conformance</a></li>
    </ul>
    </div>
</div>
<!--End of sub menu-->

<div class="panel panel-default">
    <div class="panel-heading">Incident Management</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>No.</th>
                    <th>Date</th>
                    <th>Reported Person</th>
                    <th>Non-Conformance Type</th>
                    <th>Description of Event</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
</div>
<!--End of the panel panel-default-->

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');  
?>  
    