<?php
$title = 'Add Asset';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?>  
<div class="col-lg-10">
<!--sub menu-->
<div class="row">    
    <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li class="active"><a href="index.php?module=assets&action=defaultAction">All Assets</a></li>
            <li><a href="index.php?module=assets&action=defaultAction">Unlisted Resources</a></li>
            <li><a href="index.php?module=assets&action=defaultAction">Statutory Maintenance</a></li>
            <li><a href="index.php?module=assets&action=defaultAction">Calibration</a></li>
            <li><a href="index.php?module=assets&action=defaultAction">Non-Conformance History</a></li>
            <li><a href="index.php?module=assets&action=defaultAction">Settings</a></li>
            <li><a href="index.php?module=assets&action=defaultAction">Archive</a></li>
        </ul>
    </div>
</div>
<!--End of sub menu-->
<div class="panel-group">
    <form class="form-horizontal" enctype="multipart/form-data" name="addAsset" method="post" action="<?php echo BASE_URL; ?>/index.php?module=assets&action=saveAsset">
    <div class="panel panel-default">
    <div class="panel-heading">New Resource</div>
    <div class="panel-body"> 
        <div class="form-group">
            <label class="control-label col-sm-4" for="departmentid">Department</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect form-control" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department" id="departmentid" name="departmentid">
                    <?php
                        foreach($data['department'] as $departmentNameArr){
                           echo '<option value="' . $departmentNameArr['id']. '">' . $departmentNameArr['department_name'] . ' - ' . $departmentNameArr['department_building'] . '</option>';
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="categoryType">Resource Category</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect form-control" id="categoryType" name="categoryType" title="Select a category" required>
                    <?php
                    foreach($data['assetCategory'] as $categoryTypeArr){
                       echo '<option value="' . $categoryTypeArr['id']. '">' .$categoryTypeArr['name']. '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="assetType">Type</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" id="assetType" name="assetType" title="Select a type" required></select> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="description">Description Name</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="description" name="description" required>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="assetNumber">Asset No.</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="assetNumber" name="assetNumber" placeholder="Please enter asset number" required>
            </div>
        </div>
        <?php $count = 1; foreach($data['assetQuestions'] as $QuestionDetailsArr){?>
        <div class="form-group">
            <label class="control-label col-sm-4" for="question"><?php echo $QuestionDetailsArr['description'] ?></label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <label class="radio-inline"><input type="radio" checked  name="<?php echo $QuestionDetailsArr['name'] ?>" value="1">Yes</label>
            <label class="radio-inline"><input type="radio"  name="<?php echo $QuestionDetailsArr['name'] ?>" value="0">No</label>
            </div>
        </div>
        <?php $count ++; } ?>  
    </div>
    </div>
    <div class="panel panel-default">
    <div class="panel-heading">Additional Information</div>
        <div class="panel-body">
            <div class="form-group">
                <label class="control-label col-sm-4" for="manufacturer">Manufacturer/Brand</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="manufacturer" id="manufacturer" placeholder="Please provide a manufacturer/brand">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="inventoryNumber">Serial Number</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="inventoryNumber" id="inventoryNumber" placeholder="Please provide a inventory number">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="datePurchased">Date Purchased</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="datePurchased" name="datePurchased">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="cost">Cost</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="cost" name="cost" placeholder="Please provide a cost">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notes">Notes</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" id="notes" name="notes"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="uploadPhotos">Upload Photos</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="file" id="uploadPhotos" name="uploadPhotos">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" name="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <!--End of panel panel-default-->
</form> 
</div>

</div>
</div><!--End of container-fluid-->
<script>
$(document).ready(function() { 
    $(".hideInspection").hide();
    showCategoryType();
    
    $('.optStatutoryInspection').change(function(){
        var option = $(this).val();
        if(option === 'Yes'){
           $(".hideInspection").show();
       }else{
           $(".hideInspection").hide();
       }
    });

    $("#datePurchased").datepicker({dateFormat:"yy/mm/dd", maxDate: new Date(),changeYear:true, changeMonth:true,yearRange:"1900:3000"}).datepicker();
    $("#dateExpired").datepicker({dateFormat:"yy/mm/dd", minDate: new Date(),changeYear:true, changeMonth:true,yearRange:"1900:3000"}).datepicker();

    tinymce.init({
        selector: '.tinyMCEselector',
        menubar: false
    });

});

$('body').on('change', '#categoryType', function(){
    var group_id = $(this).val();
    showCategoryType(group_id, 'assetType');
    $('#assetType').selectpicker('refresh');
});
/**
 * Method to show category types
 * @param {String} selectorid
 * @param {String} groupid
 * @return {Array}
 */
 function showCategoryType(groupid, selectorid){
     var opt = '';
     var selector = "#"+selectorid;
     $.ajax({
         url : "index.php?module=assets&action=showAssetTypes",
         type: "POST",
         data :'categoryId='+ groupid,
         dataType:'json',
         success: function (data){
                 $(selector).find('option').remove();
                 if(data.length > 0){
                     $.each(data, function(key, val) {
                         opt = $('<option/>');
                         opt.val(val.id);
                         opt.text(val.name);
                         $(selector).append(opt);
                     }); 
                     $(selector).selectpicker('refresh');
                 }else{
                    $(selector).find('option').remove();
                 }
         },
         error: function(error){
             console.log('Request failed: ' + error);
         }
     });
 }
</script> 
<?php include_once('frontend/templates/footers/default_footer_tpl.php');
    