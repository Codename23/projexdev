<?php
////////////////////// ETHIED //////////////////////////
include '../../../modules/db.php';
include '../../../modules/Employees.php';
include '../../../modules/legs.php';
include '../../../modules/owner.php';
include '../../../modules/regs.php';
include '../../../modules/stations.php';
//////////////////////// ETHIED ////////////////////////
include '../../../partials/edit_leg_view.php';
include '../../../partials/assign_leg.php';
//////////////////////// ETHIED ////////////////////////
$employees = new Employees();
$legs = new Legs();
$owner = new Owner();
$list_owners = $owner->getAll();
$reg = new Register();
$list_reg = $reg->getAll();
$station = new Stations();
$list_legs = $legs->getAll();
?>
<!DOCTYPE html>
<html>
            <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">      
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo BASE_PATH;?>css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<?php echo BASE_PATH;?>css/sb-admin.css" rel="stylesheet">
        <!-- Morris Charts CSS -->
        <link href="<?php echo BASE_PATH;?>css/plugins/morris.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="<?php echo BASE_PATH;?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <title>Administrator Home</title>
        <style>
            
.nav-tabs li{    
    width: 50%;
    font-weight: bold;
    text-transform: uppercase;
    font-size:20px;
    
}
        </style>
    <body style="background:#fff;font-size: 12px;">
         <div id="wrapper">

        <!-- Navigation -->
       <?php include '../../../partials/nav-bar.php';?>
        <!-- /#page-wrapper -->

                   <div id="page-wrapper" >

            <div class="container-fluid" >

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                         <div class="card" style="text-align: center;">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a aria-controls="searchLegModal" role="tab" data-toggle="modal" data-target="#searchLegModal" href="#searchLegModal">QUERY <span style="font-weight: 100;color:#232323;">LEG</span></a></li>
                                <li class="active" role="presentation"><a aria-controls="create_leg" href="#flight_information" role="tab" data-toggle="modal" data-target="#createLegModal">CREATE <span style="font-weight: 100;color:#232323;">LEG</span></a></li>                                
                            </ul>
                        </div>                        
                    </div>
                </div>
                <!-- /.row -->
					 <style>
                                    tr {
/*width: 100%;
display: inline-table;
table-layout: fixed;*/
}

table{            
 display: -moz-groupbox;  
 font-size:12px;
}
tbody{
  /*overflow-y: scroll;      
  height: 300px;           
  width: 100%;*/
 
}
table tbody tr:hover
{
    background:#fcb106 !important;
    color:#fff;
}
                                </style>
				<!-- view employee section -->
                                   <?php
          if(isset($_POST["btnCreateLeg"]))
          {
            if($legs->hasCreatedRecord($_POST["flight"],$_POST["owner"],$_POST["date"], $_POST["dep"], $_POST["arv"], $_POST["reg"], $_POST["std"], $_POST["etd"], $_POST["pos"], $_POST["mcr"]))
            {
                ?>
               <script>window.location = 'kermit.globalloadcontrol.net/views/main/legquery/index.php';</script>
                <?php
            }
          }
          if(isset($_POST["BtnSearchLeg"]))
          {
              $list_legs = $legs->getSearchedLeg($_POST["searched_leg"]);
          }
          ?>
				<div class="row">
                    <div class="col-lg-12">
                            <div class="table-responsive">
                           <table class="table table-hover table-striped">
                                <thead>
                                    <tr>                                        
					<th>Edit</th>
                                        <th>OWNER</th>
                                        <th>FLIGHT</th>                                        
                                        <th>DATE</th>
                                        <th>DEP</th>
                                        <th>ARV</th>
                                        <th>REG</th>
                                        <th>STD</th>
                                        <th>ETD</th>
                                        <th>POS</th>
                                        <th>MCR</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php 
                                   foreach($list_legs as $admin):
                   ?>
                            <tr>
                                <td><a href="edit.php?id=<?php echo $admin['id']; ?>"><span class="glyphicon glyphicon-edit"></span></td>
                                <?php
					if($_SESSION["sess_admin_department"] == "Agent")
					{
                                             echo "<td></td>";
					     				                                    
					}
                                        else
                                        {
                                            echo "<td>{$admin['owner']}</td>";	
                                        }
                                ?>
                                <td><b><a href="../content/index.php?fl=<?php echo $admin['id'];?>"><span class="glyphicon glyphicon-upload"></span> <?php echo $admin['flight'];?></a></b></td>                                
                                <td><?php echo $admin['flight_date']; ?></td>
                                <td><?php echo $admin['dep']; ?></td>                                
                                <td><?php echo $admin['arv']; ?></td>
                                <td><?php echo $admin['reg']; ?></td>
                                <td><?php echo $admin['std']; ?></td>
                                <td><?php echo $admin['etd']; ?></td>
                                <td><?php echo $admin['pos']; ?></td>
                                <td><?php echo $admin['mcr']; ?></td>                       
                                 <td></td>
                            </tr>
                        <?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>				
				<!-- /.Employees -->
				
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
            </div>
    <!-- /#wrapper -->
<style>
    .form-control{
        height:28px;
    }   
</style>
<!-- Modal -->
  <div class="modal fade" id="createLegModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <form method="POST">
      <div class="modal-content">
        <div class="modal-header default-bg-color default-txt-color">
          <button type="button" class="close default-txt-color" style="color:#fff;" data-dismiss="modal">&times;</button>
          <p class="modal-title">CREATE LEG</p>   

        </div>
        <div class="modal-body">
            
                <div class="row">
                   <div class="col-sm-6">
                       <div class="form-group">
                           <label>Owner *</label>
                           <select required="true" name="owner" class="form-control">
                                         <?php
                               foreach($list_owners as $o)
                               {
                                   echo "<option value='{$o["name"]}'>{$o["name"]}</option>";
                               }
                               ?>
                           </select>
                       </div>
                    </div>
                    <div class="col-sm-6">
                       <div class="form-group">
                           <label>FLIGHT DATE</label>
                           <input required="true"  name="date" type="date" class="form-control"/>
                       </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                       <div class="form-group">
                           <label>DEP</label>
                           <input type="text" name="dep" class="form-control"/>
                       </div>
                    </div>                
                   <div class="col-sm-6">
                       <div class="form-group">
                           <label>ARV</label>
                          <input required="true" name="arv" type="text" class="form-control"/>
                       </div>
                    </div>
                </div>  
                  <div class="row">
                    <div class="col-sm-6">
                       <div class="form-group">
                           <label>REGISTRATION</label>
                           <select required="true" name="reg" class="form-control">
                                                                          <?php
                               foreach($list_reg as $o)
                               {
                                   echo "<option>{$o["name"]}</option>";
                               }
                               ?>
                           </select>
                       </div>
                    </div>                                    
                    <div class="col-sm-6">
                       <div class="form-group">
                           <label>STD</label>
                           <input required="true" name="std" type="time" class="form-control"/>
                       </div>
                    </div>
                                </div>
                <div class="row">
                    <div class="col-sm-6">
                       <div class="form-group">
                           <label>ETD</label>
                          <input required="true" name="etd" type="time" class="form-control"/>
                       </div>
                    </div>
<div class="col-sm-6">
                       <div class="form-group">
                           <label>POS</label>
                           <input required="true" name="pos" type="text" class="form-control"/>
                       </div>
                    </div>
                </div>
                <div class="row">
<div class="col-sm-6">
                       <div class="form-group">
                           <label>MCR</label>
                           <select required="true" name="mcr" class="form-control">
                               <option>YES</option>
                               <option>NO</option>                               
                           </select>
                       </div>
                    </div>  
                    <div class="col-sm-6">
                       <div class="form-group">
                           <label>FLIGHT DESIGNATION</label>
                           <input required="true" name="flight" type="text" class="form-control"/>
                       </div>
                    </div>
                </div>
            
        </div>
        <div class="modal-footer footer-bg">
            <button type="submit" name ="btnCreateLeg" class="btn btn-warning">Create Leg</button>
        </div>
      </div>
      </form>
      
    </div>
  </div>
  
    <div class="modal fade" id="searchLegModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <form method="POST">
      <div class="modal-content">
        <div class="modal-header default-bg-color default-txt-color">
          <button type="button" class="close default-txt-color" style="color:#fff;" data-dismiss="modal">&times;</button>
          <p class="modal-title">SEARCH LEG</p>   

        </div>
        <div class="modal-body">
            
                <div class="row">                   
                    <div class="col-sm-12">
                       <div class="form-group">
                           <label>FLIGHT</label>
                           <input required="true" placeholder="e.g LH 203" name="searched_leg" type="text" class="form-control"/>
                       </div>
                    </div>
                </div>
            
        </div>
        <div class="modal-footer footer-bg">
            <button type="submit" name ="BtnSearchLeg" class="btn btn-warning">Find Legs</button>
        </div>
      </div>
      </form>
      
    </div>
  </div>
    <!-- jQuery -->
    <script src="<?php echo BASE_PATH;?>js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo BASE_PATH;?>js/bootstrap.min.js"></script>      
    </body>
</html>
