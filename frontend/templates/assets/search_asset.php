<?php
/**
 * Ajax script to search resource type
 * 
 * @package sheqonline
 * @author Sunny Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
//Include the common file
require_once '../../../config.php';

processAjax();

function processAjax() 
{
    global $db;
    $serchterm = $_POST["searchterm"];   

    $sql = "SELECT * FROM tbl_assets WHERE name LIKE '%" . $serchterm ."%' OR company_asset_number LIKE '%" . $serchterm ."%' LIMIT 20";
    $response = $db->getall($sql);
    
    if(!empty($response)){
    ?>        
            <table class="table">
            <thead>
                <tr>
                  <th>Name</th>
                  <th>Manufacturer</th>
                  <th>Company Asset Number</th>
                  <th>Serial Number</th>
                  <th>Date created</th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach($response as $assetDetail){
               echo '<tr>
                        <td><a href="index.php?module=assets&action=view&id=' . $assetDetail['id']  .'">' . $assetDetail['name'] . '</a></td>
                        <td>' . $assetDetail['manufacture_name'] . '</td>
                        <td>' . $assetDetail['company_asset_number'] . '</td>
                        <td>' . $assetDetail['serial_number'] . '</td>
                        <td>' . date("D, d M y H:i:s O", strtotime($assetDetail['date_created'])) . '</td>';
                        if($assetDetail['is_active']){
                            echo '<td><a data-toggle="tooltip" title="Deactivate asset" href="index.php?module=assets&action=deactivateAsset&id=' . $assetDetail['id'] . '"><span style="color:red" class="glyphicon glyphicon-remove"></span></td>';    
                        }else{
                            echo '<td><a data-toggle="tooltip" title="Activate asset" href="index.php?module=assets&action=activateAsset&id=' . $assetDetail['id'] . '"><span style="color:green" class="glyphicon glyphicon-ok"></span></td>'; 
                        }           
                        echo '<td><a data-toggle="tooltip" title="Edit asset" href="index.php?module=assets&action=editAsset&id=' . $assetDetail['id'] . '&departmentId=' . $assetDetail['department_id'] . '"><span style="color:black" class="glyphicon glyphicon-pencil"></span></td>
                              <td><a data-toggle="tooltip" title="Delete asset" href="index.php?action=deleteAsset&module=assets&id=' . $assetDetail['id'] . '"><span style="color:black" class="glyphicon glyphicon-trash"></span></a></td></tr>';
                        echo '</tr>';   
            }
            ?>
            </tbody>
            </table>     
        <?php
    }else{
        echo "<p>No matched found for <b>$serchterm</b></p>";
    }
     
}
?> 