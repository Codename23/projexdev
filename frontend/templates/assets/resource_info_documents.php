<?php 
/*
 * Header file
 */
$title = 'View Asset';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php');
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
$id =  (int)$_REQUEST['id'];
?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=assetmanagement&module=assets">All Assets</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=view&module=assets&id=<?php echo $id;?>">Info</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_documents&module=assets&id=<?php echo $id;?>">Documents</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_competence&module=assets&id=<?php echo $id;?>">Competence</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_substance&module=assets&id=<?php echo $id;?>">Substance</a></li>
        <!--<li><a href="resource_info_sheq.php">SHEQ</a></li>-->
        <!--<li><a href="resource_info_statutory.php">Statutory</a></li>
        <li><a href="resource_info_calibration.php">Calibration</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_inspected&module=assets&id=<?php echo $id;?>">Inspected</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_sop&module=assets&id=<?php echo $id;?>">SOP</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_maintenance&module=assets&id=<?php echo $id;?>">Maintenance</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_noc&module=assets&id=<?php echo $id;?>">Non-Conformance</a></li>
    </ul>
    </div>
</div>
<!--End of sub menu-->

<div class="panel panel-default">
    <div class="panel-body">
    <a href="#addDoc-modal" data-toggle="modal" data-target="#addDoc-modal"><button type="button" id="addDocument" class="btn btn-success">Add Document</button></a>
    </div>
</div> 

<div class="panel panel-default">
    <div class="panel-heading">Documents</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Description</th>
                    <th>Date Loaded</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
    
    <!--Add Doc Modal -->
  <div class="modal fade" id="addDoc-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Document</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addDocForm" action="resource_info_documents.php">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="docDescription">Doc Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="docDescription" id="docDescription" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="docUpload">Upload</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="file" name="docUpload" id="docUpload"> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" id="addDocBtn" class="btn btn-success">Add</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');  
?>  
    