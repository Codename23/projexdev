<?php 
/*
 * Header file
 */
$title = 'Statutory Job';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
global $objAssets;
?>
<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
           <li><a href="<?php echo BASE_URL;?>/index.php?module=assets&action=statutory_maintenance_plan">Statutory Maintenance Plan</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=assets&action=statutory_upcoming">Upcoming</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=assets&action=job_listing">Job</a></li>
        <!--<li><a href="statutory_approval.php">Approval</a></li>
        <li><a href="statutory_scheduled.php">Scheduled</a></li>
        <li><a href="statutory_active.php">Active</a></li>-->
        <li><a href="statutory_history.php">History</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

<div class="panel panel-default">
    <div class="panel-heading">Approval Process</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Job No</th>
                    <th>Starting Date</th>
                    <th>Internal/External</th>
                    <th>Description</th>
                    <th>Departments</th>
                    <th>Risk Level</th>
                    <th>Hazard Types</th>
                    <th>Approval Status</th>
                    <th>PO No</th>
                    <th>Responsible Person</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                  <?php
                          $count = 1;
                            if(!empty($data['allStatutoryJobs'])){
                                foreach($data['allStatutoryJobs'] as $assetDetail)
                              {
                                //$url = BASE_URL."/index.php?edit={$assetDetail["record_id"]}&action=statutory_plan&module=assets";
                                echo "<tr>
                                        <td>{$assetDetail['id']}</td>
                                        <td>{$assetDetail['start_date']}</td>
                                        <td>{$assetDetail['int_ex']}</td>
                                        <td>{$assetDetail['description']}</td>
                                        <td>{$assetDetail['department_involved']}</td>
                                        <td>{$assetDetail['risk_level']}</td>
                                        <td>{$assetDetail['hazard_types']}</td>
                                        <td>{$assetDetail['approval_required']}</td>
                                        <td>1</td>
                                       <td>{$assetDetail['responsible_persons']}</td>                                             
                                       <td><a href='#schedule-modal' data-toggle='modal' data-target='#schedule-modal'>Schedule</a></td>
                                        <td><a href='#-modal' data-toggle='modal' data-target='#-modal'>Reschedule</a></td>
                                        <td><a href='#signoff-modal' data-toggle='modal' data-target='#signoff-modal'>Sign Off</a></td>
                                        <td><a href=''>View Job</a></td>";                             
                                }
                            }
                            ?>
                    <!--
                    <td class="yellow-table-bg">Medium</td>
                    <td><a href="#viewHazardTypes-modal" data-toggle="modal" data-target="#viewHazardTypes-modal">View (7)</a></td>
-->           
            </tbody>
        </table>    
    </div>
    </div>
    </div>

<!-- View Departments Modal -->
  <div class="modal fade" id="viewDepartments-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">View Departments</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Department</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Warehouse</td>
                        <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- View Hazard Types Modal -->
  <div class="modal fade" id="viewHazardTypes-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">View Hazard Types</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Hazard Types</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Working at heights</td>
                        <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Electricity</td>
                        <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Machine</td>
                        <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Hot Work</td>
                        <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Noise</td>
                        <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Fumes</td>
                        <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Vessels under pressure</td>
                        <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Schedule Modal -->
  <div class="modal fade" id="schedule-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Job Details</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="scheduleForm" action="<?php echo BASE_URL;?>/index.php?action=schedule_or_reschedule_job&module=assets">
            <div class="form-group">
                <label class="control-label col-sm-4" for="startingDateTime">Starting Date & Time</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="startingDateTime" id="startingDateTime" required placeholder=""> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="endDate">End Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="endDate" id="endDate" required placeholder=""> 
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Notifications - Once Scheduled</h5></span></div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="department">Department</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="department" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department">
                    <option value="1">IT</option>
                    <option value="2">Finance</option>
                    <option value="3">HR</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="manager">Manager</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="manager" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a manager">
                    <option value="1">Nick Botha</option>
                    <option value="2">Sunny Mathole</option>
                    <option value="3">Brian Douglas</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="sheqfManager">SHEQF Manager</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="sheqfManager" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a sheqf manager">
                    <option value="1">Nick Botha</option>
                    <option value="2">Sunny Mathole</option>
                    <option value="3">Brian Douglas</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="otherPeople">Other People</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="otherPeople" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more people" data-header="Close">
                    <option value="1">Nick Botha</option>
                    <option value="2">Sunny Mathole</option>
                    <option value="3">Brian Douglas</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="scheduleBtn" name="scheduleBtn" class="btn btn-success">Schedule</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Sign Off Modal -->
  <div class="modal fade" id="signoff-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sign Off</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
            <table width="100%" class="table table-hover">
                <thead>
                    <tr>
                        <th colspan="2">Job Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="30%">Maintenance Type</td>
                        <td>Service</td>
                    </tr>
                    <tr>
                        <td>Full Job Description</td>
                        <td>This is a description</td>
                    </tr>
                </tbody>
            </table>
            </div>
            
            <div class="modal-custom-h5"><span><h5>Supplier</h5></span></div>
            <form class="form-horizontal" method="post" name="serviceSignOffForm" action="">
            <div class="form-group">
                <div class="col-sm-8">
                <a href="#addSupplier-modal" data-toggle="modal" data-target="#addSupplier-modal"><button type="button" id="addSupplier" class="btn btn-success">Add Supplier</button></a>
                </div>
            </div>
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Supplier</th>
                            <th>Service</th>
                            <th>Rating</th>
                            <th>Comments</th>
                            <th>Cost</th>
                            <th>Evidence</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>James Painters</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>R 1200</td>
                            <td></td>
                            <td><a href="#complete-modal" data-toggle="modal" data-target="#complete-modal">Complete</a></td>
                            <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        </tr>
                    </tbody>
                </table>    
            </div>
            <br/>
                
            <div class="modal-custom-h5"><span><h5>Asset That Was Maintained</h5></span></div>
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Department</th>
                            <th>Category</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>No</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Receiving</td>
                            <td>Moveable Asset</td>
                            <td>Forklift</td>
                            <td>10 Ton Diesel</td>
                            <td>12</td>
                        </tr>
                    </tbody>
                </table>    
            </div>
            <div class="form-group">
                <div class="col-sm-8">
                <button type="submit" id="serviceSignOffBtn" class="btn btn-success">Sign Off</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Add Supplier Modal -->
  <div class="modal fade" id="addSupplier-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Complete Supplier Details</h4>
        </div>
        <div class="modal-body table-responsive">
            <form class="form-horizontal" method="post" name="addSupplierForm" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="signoffSupplierName">Supplier Name</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a supplier">
                        <option value="1">Nick Botha</option>
                        <option value="2">Sunny Mathole</option>
                        <option value="3">Brian Douglas</option>
                        </select>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-sm-4" for="signoffRate">Rate Service Provider</label>
                    <div class="col-lg-6 col-md-4 col-sm-8 star-rating">
                        <input type="hidden" class="rating"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="noteOfServiceProvider">Note of Service Provider</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea class="form-control" rows="5" name="ServiceTypeDescription" id="ServiceTypeDescription" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="supportingDocs">Upload Supporting Documents</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="file" class="supportingDocs"/>
                    </div>
                </div>   
                <div class="form-group">
                    <label class="control-label col-sm-4" for="costOfService">Cost of Service</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="costOfService" id="costOfService" required value="R">
                    </div>
                </div>
            </form>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
            <button type="button" id="addSupplierBtn" class="btn btn-success">Submit</button>
            </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<script src="js/bootstrap-rating.js"></script>
<script>
$(function() {
    $("#startingDateTime").datetimepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt', 
        maxDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"
    }).datetimepicker();
    
    $("#endDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    
    $('.rating').each(function () {
          $('<span class="label label-success"></span>')
            .text($(this).val() || ' ')
            .insertAfter(this);
        });
        $('.rating').on('change', function () {
          $(this).next('.label').text($(this).val());
        });
    
    $("#addSupplierBtn").click(function(){
    $("#addSupplier-modal").modal('toggle');
    });
});
</script>

<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');  
?>  
    