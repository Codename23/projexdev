<?php
$title = 'Asset Management';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
    <div class="row">
        <!--Start of main col-->
        <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
            <div class="panel-group">
                <div class="panel panel-default">
                <div class="panel-heading"><h4>Search Assets</h4></div>
                <div class="panel-body"> 
                   <form method="post" name="searchAsset" action="index.php">
                        
                        <div class="form-group">
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="searchAsset" name="searchAsset" placeholder="Search resource by resource number or name" required>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-8">
                                <input type="hidden" name="action" value="searchAsset">
                                <input type="hidden" name="module" value="assets">
                                <button type="submit" name="" class="btn btn-success">Search Asset</button>    
                            </div>
                        </div>
                    </form>
                </div>  
                </div>
                
                <div class="panel panel-default">
                <div class="panel-heading"><h4>Asset management</h4></div>
                <div class="panel-body"> 
                    <div class="table-responsive">          
                    <table class="table">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Manufacturer</th>
                          <th>Company Asset Number</th>
                          <th>Serial Number</th>
                          <th>Date created</th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php
                          $count = 1;
                            if(!empty($data['assets'])){
                                foreach($data['assets'] as $assetDetail){
                                echo '<tr>
                                        <td>' . $count . '</td>
                                        <td>' . $assetDetail['name'] . '</td>
                                        <td>' . $assetDetail['manufacture_name'] . '</td>
                                        <td>' . $assetDetail['company_asset_number'] . '</td>
                                        <td>' . $assetDetail['serial_number'] . '</td>
                                        <td>' . date("D, d M y H:i:s O", strtotime($assetDetail['date_created'])) . '</td>';
                                if($assetDetail['is_active']){
                                    echo '<td><a data-toggle="tooltip" title="Deactivate asset" href="index.php?module=assets&action=deactivateAsset&id=' . $assetDetail['id'] . '"><span style="color:red" class="glyphicon glyphicon-remove"></span></td>';    
                                }else{
                                    echo '<td><a data-toggle="tooltip" title="Activate asset" href="index.php?module=assets&action=activateAsset&id=' . $assetDetail['id'] . '"><span style="color:green" class="glyphicon glyphicon-ok"></span></td>'; 
                                }           
                                echo '<td><a data-toggle="tooltip" title="Edit asset" href="index.php?module=assets&action=editAsset&id=' . $assetDetail['id'] . '"><span style="color:black" class="glyphicon glyphicon-pencil"></span></td>
                                      <td><a data-toggle="tooltip" title="Delete asset" onclick="return confirm("Are you sure ");" href="index.php?action=deleteAsset&module=assets&id=' . $assetDetail['id'] . '"><span style="color:black" class="glyphicon glyphicon-trash"></span></a></td></tr>';
                                    $count ++;
                                }    
                            }else{
                                 echo "<tr><td>There is no assets yet</td></tr>";
                            }
                            
                            ?>
                      </tbody>
                    </table>
                    </div>                    
                </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form method="post" name="addAssets" action="index.php">
                            <input type="hidden" name="action" value="addAsset">
                            <input type="hidden" name="module" value="assets">
                            <button type="submit" name="" class="btn btn-success">Add New Asset</button>
                        </form>
                    </div>
                </div>
                <!--End of panel-default-->
            </div>
        </div>
        <!--End of main col-->
    </div>
</div>
<script>
    $(document).on("click", ".trash", function(){
        var assetId = $(this).data('id');
        $('.modal-body #assetId').val(assetId);  
    });
    
    
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<?php include_once('frontend/templates/footers/default_footer_tpl.php');?>