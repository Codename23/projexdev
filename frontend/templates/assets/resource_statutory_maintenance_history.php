<?php 
/*
 * Header file
 */
include_once('includes/header.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('includes/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('includes/side-menu.php'); 
?> 
<!--End of navigation--> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="resource.php">All Assets</a></li>
        <li><a href="resource_unlisted.php">Unlisted Resources</a></li>
        <li class="active"><a href="resource_statutory_maintenance.php">Statutory Maintenance</a></li>
        <li><a href="resource_calibration.php">Calibration</a></li>
        <li><a href="resource_nonconformance_history.php">Non-Conformance History</a></li>
        <li><a href="resource_settings.php">Settings</a></li>
        <li><a href="resource_archive.php">Archive</a></li>
    </ul>
    </div>
    </div>
    
    <div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="resource_statutory_maintenance.php">Dashboard</a></li>
        <li><a href="resource_statutory_maintenance_scheduled.php">Scheduled</a></li>
        <li><a href="resource_statutory_maintenance_suppliers.php">Suppliers</a></li>
        <li class="active"><a href="resource_statutory_maintenance_history.php">History</a></li>
        <li><a href="resource_statutory_maintenance_settings.php">Settings</a></li>
    </ul>
    </div>
    </div>
    <!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-heading">Search Statutory Maintenance History</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>Wood</option>
                <option>I.T</option>
                <option>Finance</option>
                <option>HR</option>
            </select>
        </div>
        </div> 
            
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Category</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more categories" data-header="Close">
                <option>Fixed Assets</option>
                <option>Moveable Assets</option>
                <option>Raw Materials</option>
                <option>Final Products</option>
                <option>Consumables</option>
                <option>Emergency Equipment</option>
                <option>Motorized Equipment</option>
            </select> 
        </div>
        </div>
        
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Type</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                <option>Building</option>
                <option>Fixed Electrical Equipment</option>
                <option>Key is to be able to add on own behalf</option>
                <option>Forklift</option>
                <option>Wood</option>
                <option>Shutter</option>
                <option>Petrol</option>
                <option>Fire Extinguishers</option>
                <option>Forklift</option>
            </select> 
        </div>
        </div>
            
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Resource Number</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more resource numbers"data-header="Close">
                <option>123456</option>
                <option>123456789</option>
                <option>1245789</option>
            </select>
        </div>
        </div>   
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>
   
    <div class="panel panel-default">
    <div class="panel-heading">Statutory Maintenance History</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="statutoryMaintenanceHistoryTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>No.</th>
                    <th>Service History</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Wood</td>
                    <td></td>
                    <td>Extinguisher</td>
                    <td>9KG DCP</td>
                    <td>023</td>
                    <td><a href="#serviceHistory-modal" data-toggle="modal" data-target="#serviceHistory-modal">View</a></td>
                    <td><a href="">View Resource</a></td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Wood</td>
                    <td></td>
                    <td>Extinguisher</td>
                    <td>9KG DCP</td>
                    <td>023</td>
                    <td><a href="#serviceHistory-modal" data-toggle="modal" data-target="#serviceHistory-modal">View</a></td>
                    <td><a href="">View Resource</a></td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Wood</td>
                    <td></td>
                    <td>Extinguisher</td>
                    <td>9KG DCP</td>
                    <td>023</td>
                    <td><a href="#serviceHistory-modal" data-toggle="modal" data-target="#serviceHistory-modal">View</a></td>
                    <td><a href="">View Resource</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
<!--Service History Modal -->
  <div class="modal fade" id="serviceHistory-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Service History</h4>
        </div>
        <div class="modal-body table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
            <tr>
                <th>Service Date</th>
                <th>Internal/External</th>
                <th>Supplier</th>
                <th>Supplier Rating</th>
                <th>Duration</th>
                <th>Cost</th>
                <th>View Documents</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><a href="">View</a></td>
            </tr>
            </tbody>
        </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<script src="js/jquery.tablecheckbox.js"></script>
<script src="js/bootstrap-select.js"></script>
<script>
    $('#statutoryMaintenanceHistoryTbl').tablecheckbox();
</script>
<?php 
/*
 * 
 */
include_once('includes/footer.php');
?>  
    