<?php
$title = 'Settings';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?>  
<!--End of navigation--> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">    
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li><a href="index.php?module=assets&action=defaultAction">All Assets</a></li>
            <li><a href="index.php?module=assets&action=unlistedAsset">Unlisted Resources</a></li>
            <li><a href="index.php?module=assets&action=statutoryMaintenance">Statutory Maintenance</a></li>
            <li><a href="index.php?module=assets&action=calibration">Calibration</a></li>
            <li><a href="index.php?module=assets&action=nonComformanceHistory">Non-Conformance History</a></li>
            <li class="active"><a>Settings</a></li>
            <li><a href="index.php?module=assets&action=archive">Archive</a></li>
        </ul>
        </div>
    </div>
    <!--End of sub menu-->
    
    <div class="row">    
    <div class="col-lg-8">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="resource_settings.php">Resource Types</a></li>
        <li><a href="resource_settings_statutory_maintenance.php">Statutory Maintenance Types</a></li>
        <li><a href="resource_settings_calibration.php">Calibration Types</a></li>
    </ul>
    </div>
    </div>
    <!--End of sub menu-->

    <div class="panel panel-default">
        <div class="panel-body">
        <button type="button" onclick="window.location.href=''" id="addNew" class="btn btn-success">Add New</button>
        </div>
    </div> 
    <!--End of panel panel-default-->

    <div class="panel panel-default">
    <div class="panel-heading">Resource Types</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="resourceTbl">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Type</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
        </div>
</div><!--End of container-fluid-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php');  
    