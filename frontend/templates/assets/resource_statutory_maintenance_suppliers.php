<?php 
/*
 * Header file
 */
include_once('includes/header.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('includes/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('includes/side-menu.php'); 
?> 
<!--End of navigation--> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="resource.php">All Assets</a></li>
        <li><a href="resource_unlisted.php">Unlisted Resources</a></li>
        <li class="active"><a href="resource_statutory_maintenance.php">Statutory Maintenance</a></li>
        <li><a href="resource_calibration.php">Calibration</a></li>
        <li><a href="resource_nonconformance_history.php">Non-Conformance History</a></li>
        <li><a href="resource_settings.php">Settings</a></li>
        <li><a href="resource_archive.php">Archive</a></li>
    </ul>
    </div>
    </div>
    
    <div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="resource_statutory_maintenance.php">Dashboard</a></li>
        <li><a href="resource_statutory_maintenance_scheduled.php">Scheduled</a></li>
        <li class="active"><a href="resource_statutory_maintenance_suppliers.php">Suppliers</a></li>
        <li><a href="resource_statutory_maintenance_history.php">History</a></li>
        <li><a href="resource_statutory_maintenance_settings.php">Settings</a></li>
    </ul>
    </div>
    </div>
    <!--End of sub menu-->

    <div class="panel panel-default">
        <div class="panel-body">
        <button type="button" onclick="window.location.href='supplier_add.php'" id="addSupplier" class="btn btn-success">Add Suppliers</button>
        </div>
    </div> 
    <!--End of panel panel-default-->
    
    <div class="panel panel-default">
    <div class="panel-heading">Search Suppliers</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Company Name</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>Company 1</option>
                <option>Company 2</option>
                <option>Company 3</option>
                <option>Company 4</option>
            </select>
        </div>
        </div> 
            
        <div class="col-md-3 col-sm-6">
        <div class="form-group">
            <p><b>Operating Area</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more descriptions" data-header="Close">
                <option>Area 1</option>
                <option>Area 2</option>
                <option>Area 3</option>
            </select> 
        </div>
        </div>
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Suppliers</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Company Name</th>
                    <th>Operating Area</th>
                    <th>Type</th>
                    <th>Approval</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
        </div>
</div><!--End of container-fluid-->
<script src="js/bootstrap-select.js"></script>
<?php 
/*
 * 
 */
include_once('includes/footer.php');
?>  
    