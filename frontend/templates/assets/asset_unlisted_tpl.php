<?php
$title = 'Unlisted Resource';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?>  
<!--End of navigation--> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">    
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li><a href="index.php?module=assets&action=defaultAction">All Assets</a></li>
            <li class="active"><a>Unlisted Resources</a></li>
            <li><a href="index.php?module=assets&action=statutoryMaintenance">Statutory Maintenance</a></li>
            <li><a href="index.php?module=assets&action=calibration">Calibration</a></li>
            <li><a href="index.php?module=assets&action=nonComformanceHistory">Non-Conformance History</a></li>
            <li><a href="index.php?module=assets&action=settings">Settings</a></li>
            <li><a href="index.php?module=assets&action=archive">Archive</a></li>
        </ul>
        </div>
    </div>
    <!--End of sub menu-->
    <div class="panel panel-default">
        <div class="panel-body">
        <button type="button" onclick="window.location.href='resource_add.php'" id="resourceToDatabase" class="btn btn-success">Add Resource to Database</button>
        </div>
    </div> 
    <!--End of panel panel-default-->
    
    <div class="panel panel-default">
    <div class="panel-heading">Search Resources</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>Wood</option>
                <option>I.T</option>
                <option>Finance</option>
                <option>HR</option>
            </select>
        </div>
        </div> 
            
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Type</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                <option>Type 1</option>
                <option>Type 2</option>
                <option>Type 3</option>
            </select> 
        </div>
        </div>
        
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Date Added</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more dates" data-header="Close">
                <option>2017/03/20</option>
                <option>2018/03/20</option>
                <option>2016/03/20</option>
            </select> 
        </div>
        </div>
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Unlisted Resources</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="resourceTbl">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Date Added</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Wood</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Wood</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Wood</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
        </div>
</div><!--End of container-fluid-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php');
    