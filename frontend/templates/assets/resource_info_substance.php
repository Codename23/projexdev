<?php 
/*
 * Header file
 */
$title = 'Health Hazardous Material';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php');
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
$id =  (int)$_REQUEST['id'];
?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=assetmanagement&module=assets">All Assets</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=view&module=assets&id=<?php echo $id;?>">Info</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_documents&module=assets&id=<?php echo $id;?>">Documents</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_competence&module=assets&id=<?php echo $id;?>">Competence</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_substance&module=assets&id=<?php echo $id;?>">Substance</a></li>
        <!--<li><a href="resource_info_sheq.php">SHEQ</a></li>-->
        <!--<li><a href="resource_info_statutory.php">Statutory</a></li>
        <li><a href="resource_info_calibration.php">Calibration</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_inspected&module=assets&id=<?php echo $id;?>">Inspected</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_sop&module=assets&id=<?php echo $id;?>">SOP</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_maintenance&module=assets&id=<?php echo $id;?>">Maintenance</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_noc&module=assets&id=<?php echo $id;?>">Non-Conformance</a></li>
    </ul>
    </div>
</div>
<!--End of sub menu-->

<div class="panel panel-default">
    <div class="panel-body">
        <a href='resource_info_substance_add_substance.php'><button type="button" id="addSubstance" class="btn btn-success">Add Substance</button></a>
        <a href="#linkSubstance-modal" data-toggle="modal" data-target="#linkSubstance-modal"><button type="button" id="linkSubstance" class="btn btn-success">Link Substance</button></a>
    </div>
</div> 

<div class="panel panel-default">
    <div class="panel-heading">HCS</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Substance Name</th>
                    <th>Inventory No</th>
                    <th>Manufacturer</th>
                    <th>Download MSDF</th>
                    <th>Chemical Table</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href=""><span class="glyphicon glyphicon-pencil"></span></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href=""><span class="glyphicon glyphicon-pencil"></span></a></td>
                </tr>
            </tbody>
        </table>    
    </div>
    </div>
    </div>

<!--Link Substance Modal -->
  <div class="modal fade" id="linkSubstance-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Link Substance</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="linkSubstanceForm" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="category">Category</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a category">
                        <option>Category 1</option>
                        <option>Category 2</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="type">Type</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                        <option>Type 1</option>
                        <option>Type 2</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" id="linkSubstanceBtn" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');  
?>  
    