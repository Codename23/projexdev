<?php 
/*
 * Header file
 */
include_once('includes/header.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('includes/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('includes/side-menu.php'); 
?> 
<!--End of navigation--> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="resource.php">All Assets</a></li>
        <li><a href="resource_unlisted.php">Unlisted Resources</a></li>
        <li><a href="resource_statutory_maintenance.php">Statutory Maintenance</a></li>
        <li><a href="resource_calibration.php">Calibration</a></li>
        <li class="active"><a href="resource_nonconformance_history.php">Non-Conformance History</a></li>
        <li><a href="resource_settings.php">Settings</a></li>
        <li><a href="resource_archive.php">Archive</a></li>
    </ul>
    </div>
    </div>
    
    <div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="resource_nonconformance_history.php">Incident Management</a></li>
        <li class="active"><a href="resource_nonconformance_history_inspection.php">Inspection</a></li>
        <li><a href="resource_nonconformance_history_corrective.php">Corrective Action</a></li>
        <li><a href="resource_nonconformance_history_improvement.php">Improvement</a></li>
    </ul>
    </div>
    </div>
    <!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-heading">Search Inspection</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>Wood</option>
                <option>I.T</option>
                <option>Finance</option>
                <option>HR</option>
            </select>
        </div>
        </div> 
            
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Category</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more categories" data-header="Close">
                <option>Fixed Assets</option>
                <option>Moveable Assets</option>
                <option>Raw Materials</option>
                <option>Final Products</option>
                <option>Consumables</option>
                <option>Emergency Equipment</option>
                <option>Motorized Equipment</option>
            </select> 
        </div>
        </div>
        
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Type</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                <option>Building</option>
                <option>Fixed Electrical Equipment</option>
                <option>Key is to be able to add on own behalf</option>
                <option>Forklift</option>
                <option>Wood</option>
                <option>Shutter</option>
                <option>Petrol</option>
                <option>Fire Extinguishers</option>
                <option>Forklift</option>
            </select> 
        </div>
        </div>
            
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Number</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more numbers"data-header="Close">
                <option>123456</option>
                <option>123456789</option>
                <option>1245789</option>
            </select>
        </div>
        </div>    
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Inspection</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>No.</th>
                    <th>Date</th>
                    <th>Inspection Type</th>
                    <th>Inspector</th>
                    <th>Non-Conformance</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
        </div>
</div><!--End of container-fluid-->
<script src="js/bootstrap-select.js"></script>
<script>

</script>
<?php 
/*
 * 
 */
include_once('includes/footer.php');
?>  
    