<?php 
/*
 * Header file
 */
include_once('includes/header.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('includes/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('includes/side-menu.php'); 
?> 
<!--End of navigation--> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="resource.php">All Assets</a></li>
        <li><a href="resource_unlisted.php">Unlisted Resources</a></li>
        <li><a href="resource_statutory_maintenance.php">Statutory Maintenance</a></li>
        <li class="active"><a href="resource_calibration.php">Calibration</a></li>
        <li><a href="resource_nonconformance_history.php">Non-Conformance History</a></li>
        <li><a href="resource_settings.php">Settings</a></li>
        <li><a href="resource_archive.php">Archive</a></li>
    </ul>
    </div>
    </div>
    
    <div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="resource_calibration.php">Dashboard</a></li>
        <li><a href="resource_calibration_status.php">Status</a></li>
        <li class="active"><a href="resource_calibration_scheduled.php">Scheduled</a></li>
        <li><a href="resource_calibration_suppliers.php">Suppliers</a></li>
        <li><a href="resource_calibration_history.php">History</a></li>
        <li><a href="resource_calibration_settings.php">Settings</a></li>
    </ul>
    </div>
    </div>
    <!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-heading">Search Scheduled Calibration</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>Wood</option>
                <option>I.T</option>
                <option>Finance</option>
                <option>HR</option>
            </select>
        </div>
        </div> 
            
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Category</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more categories" data-header="Close">
                <option>Fixed Assets</option>
                <option>Moveable Assets</option>
                <option>Raw Materials</option>
                <option>Final Products</option>
                <option>Consumables</option>
                <option>Emergency Equipment</option>
                <option>Motorized Equipment</option>
            </select> 
        </div>
        </div>
        
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Type</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                <option>Building</option>
                <option>Fixed Electrical Equipment</option>
                <option>Key is to be able to add on own behalf</option>
                <option>Forklift</option>
                <option>Wood</option>
                <option>Shutter</option>
                <option>Petrol</option>
                <option>Fire Extinguishers</option>
                <option>Forklift</option>
            </select> 
        </div>
        </div>
            
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Resource Number</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more resource numbers"data-header="Close">
                <option>123456</option>
                <option>123456789</option>
                <option>1245789</option>
            </select>
        </div>
        </div>   
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#rescheduleCalibration-modal" data-toggle="modal" data-target="#rescheduleCalibration-modal"><button type="button" id="rescheduleCalibration" class="btn btn-default">Reschedule Calibration</button></a>
        <a href="#signOffCalibration-modal" data-toggle="modal" data-target="#signOffCalibration-modal"><button type="button" id="signOffCalibration" class="btn btn-default">Sign Off Calibration</button></a>
    </div>
    </div>
   
    <div class="panel panel-default">
    <div class="panel-heading">Scheduled Calibration</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="calibrationScheduledTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Number</th>
                    <th>Internal / External</th>
                    <th>Calibration Type</th>
                    <th>Frequency</th>
                    <th>Next Calibration Due</th>
                    <th>Scheduled Date</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Wood</td>
                    <td></td>
                    <td>Extinguisher</td>
                    <td>9KG DCP</td>
                    <td>023</td>
                    <td></td>
                    <td>Normal</td>
                    <td>Yearly</td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Wood</td>
                    <td></td>
                    <td>Extinguisher</td>
                    <td>9KG DCP</td>
                    <td>023</td>
                    <td></td>
                    <td>Normal</td>
                    <td>Yearly</td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Wood</td>
                    <td></td>
                    <td>Extinguisher</td>
                    <td>9KG DCP</td>
                    <td>023</td>
                    <td></td>
                    <td>Normal</td>
                    <td>Yearly</td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->

<!-- Reschedule Calibration Modal -->
  <div class="modal fade" id="rescheduleCalibration-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reschedule Calibration</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="rescheduleCalibrationForm" action="resource_calibration_scheduled.php">
            <div class="form-group">
                <label class="control-label col-sm-4" for="maintenanceType">Maintenance Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="maintenanceType" class="form-control">
                    <option value selected disabled>Please select a maintenance type</option>
                    <option value="1">Normal</option>
                    <option value="2">Pressure Test</option>
                    </select>
                </div>
            </div>   
            <div class="form-group">
                <label class="control-label col-sm-4" for="startDateTime">Start Date & Time</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="startDateTime" id="startDateTime" required placeholder="">
                </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-sm-4" for="endDateTime">End Date & Time</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="endDateTime" id="endDateTime" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="responsiblePerson">Responsible Person</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more responsible people" data-header="Close">
                        <option>Warren Windovogel</option>
                        <option>Sunnyboy Mathole</option>
                        <option>Nick Botha</option>
                        <option>Nasir Jones</option>
                    </select> 
                </div>
            </div>    
            <div class="form-group">
                <label class="control-label col-sm-4" for="notifyPeople">Notify People</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more people" data-header="Close">
                        <option>Warren Windovogel</option>
                        <option>Sunnyboy Mathole</option>
                        <option>Nick Botha</option>
                        <option>Nasir Jones</option>
                    </select> 
                </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-sm-4" for="notes">Notes</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="notes" id="notes" required></textarea>
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Additional Information</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="serviceProvider">Service Provider</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="serviceProvider" id="serviceProvider" required placeholder="">
                </div>
            </div>   
            <div class="form-group">
                <label class="control-label col-sm-4" for="departmentInvolved">Department Involved</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments" data-header="Close">
                        <option>I.T</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div> 
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="rescheduleCalibrationBtn" class="btn btn-success">Reschedule Calibration</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Sign Off Calibration Modal -->
  <div class="modal fade" id="signOffCalibration-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sign Off Calibration</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
            <table width="100%" class="table table-hover">
                <thead>
                    <tr>
                        <th colspan="2">Calibration Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="30%">Maintenance / Service Type</td>
                        <td>Normal</td>
                    </tr>
                    <tr>
                        <td>Date of Service</td>
                        <td>2018/05/12</td>
                    </tr>
                    <tr>
                        <td>Responsible Person</td>
                        <td>Nick Botha</td>
                    </tr>
                    <tr>
                        <td>Notes</td>
                        <td>This is the note details</td>
                    </tr>
                    <tr>
                        <td>Service Provider</td>
                        <td>Service Provider</td>
                    </tr>
                    <tr>
                        <td>Supplier</td>
                        <td>Supplier</td>
                    </tr>
                    <tr>
                        <td>Department Involved</td>
                        <td>Wood</td>
                    </tr>
                </tbody>
            </table>
            </div>
            
            <div class="modal-custom-h5"><span><h5>Sign Off</h5></span></div>
            <form class="form-horizontal" method="post" name="calibrationSignOffForm" action="resource_calibration_scheduled.php">
            <div class="form-group">
                <label class="control-label col-sm-4" for="signoffResponsiblePerson">Responsible Person</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more responsible people" data-header="Close">
                        <option>Warren Windovogel</option>
                        <option>Sunnyboy Mathole</option>
                        <option>Nick Botha</option>
                        <option>Nasir Jones</option>
                    </select> 
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="signoffRate">Rate Service Provider</label>
                <div class="col-lg-6 col-md-4 col-sm-8 star-rating">
                    <input type="hidden" class="rating"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="noteOfServiceProvider">Note of Service Provider</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="ServiceTypeDescription" id="ServiceTypeDescription" required></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="supportingDocs">Upload Supporting Documents</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="file" class="supportingDocs"/>
                </div>
            </div>   
            <div class="form-group">
                <label class="control-label col-sm-4" for="costOfService">Cost of Service</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="costOfService" id="costOfService" required value="R">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="nextServiceDate">Next Service Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="nextServiceDate" id="nextServiceDate" required placeholder="">
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="signOffMeasurement">Measurement</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="signOffMeasurement" id="signOffMeasurement" required placeholder="">
                </div>
            </div> 
            <div class="form-group">
                <span class="control-label col-sm-4"></span>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <button type="button" id="lodgeCorrectiveAction" class="btn btn-success">Lodge Corrective Action</button>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="password">Password</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="password" class="form-control" name="password" id="password" required placeholder="">
                </div>
            </div> 
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="signOffCalibrationBtn" class="btn btn-success">Sign Off</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<script src="js/bootstrap-rating.js"></script>
<script src="js/jquery.tablecheckbox.js"></script>
<script src="js/bootstrap-select.js"></script>
<script>
$(function(){    
    $("#startDateTime").datetimepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt', 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datetimepicker();
    $("#endDateTime").datetimepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt', 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datetimepicker();
    
    $("#nextServiceDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    
    $('.rating').each(function () {
          $('<span class="label label-success"></span>')
            .text($(this).val() || ' ')
            .insertAfter(this);
        });
        $('.rating').on('change', function () {
          $(this).next('.label').text($(this).val());
        });
});
$('#calibrationScheduledTbl').tablecheckbox();
var chkbox = document.getElementsByClassName("chkbox");

    function onChangeListener() {
        $("#rescheduleCalibration ,#signOffCalibration").removeClass("btn-success").addClass("btn-default");
        for (var i = 0; i < chkbox.length; i++) {
            if (chkbox[i].checked) {
                $("#rescheduleCalibration ,#signOffCalibration").removeClass("btn-default").addClass("btn-success");
            }
        }
    }

    for (var i = 0; i < chkbox.length; i++) {
        var checkbox = chkbox[i];
        checkbox.addEventListener("change", onChangeListener);
    }
</script>
<?php 
/*
 * 
 */
include_once('includes/footer.php');
?>  
    