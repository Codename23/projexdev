<?php
$title = 'Calibration';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?>  
<!--End of navigation--> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">    
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li><a href="index.php?module=assets&action=defaultAction">All Assets</a></li>
            <li><a href="index.php?module=assets&action=unlistedAsset">Unlisted Resources</a></li>
            <li><a href="index.php?module=assets&action=statutoryMaintenance">Statutory Maintenance</a></li>
            <li class="active"><a>Calibration</a></li>
            <li><a href="index.php?module=assets&action=nonComformanceHistory">Non-Conformance History</a></li>
            <li><a href="index.php?module=assets&action=settings">Settings</a></li>
            <li><a href="index.php?module=assets&action=archive">Archive</a></li>
        </ul>
        </div>
    </div>
    <!--End of sub menu-->
    
    <div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="resource_calibration.php">Dashboard</a></li>
        <li><a href="resource_calibration_status.php">Status</a></li>
        <li><a href="resource_calibration_scheduled.php">Scheduled</a></li>
        <li><a href="resource_calibration_suppliers.php">Suppliers</a></li>
        <li><a href="resource_calibration_history.php">History</a></li>
        <li><a href="resource_calibration_settings.php">Settings</a></li>
    </ul>
    </div>
    </div>
    <!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-heading">Search Calibration</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>Wood</option>
                <option>I.T</option>
                <option>Finance</option>
                <option>HR</option>
            </select>
        </div>
        </div> 
            
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Category</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more categories" data-header="Close">
                <option>Fixed Assets</option>
                <option>Moveable Assets</option>
                <option>Raw Materials</option>
                <option>Final Products</option>
                <option>Consumables</option>
                <option>Emergency Equipment</option>
                <option>Motorized Equipment</option>
            </select> 
        </div>
        </div>
        
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Type</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                <option>Building</option>
                <option>Fixed Electrical Equipment</option>
                <option>Key is to be able to add on own behalf</option>
                <option>Forklift</option>
                <option>Wood</option>
                <option>Shutter</option>
                <option>Petrol</option>
                <option>Fire Extinguishers</option>
                <option>Forklift</option>
            </select> 
        </div>
        </div>
            
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Frequency</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more frequencies"data-header="Close">
                <option value="1">Daily</option>
                <option value="2">Weekly</option>
                <option value="3">Monthly</option>
                <option value="4">Every 6 Months</option>
                <option value="5">Yearly</option>
                <option value="6">Every 2 Years</option>
            </select>
        </div>
        </div>   
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
        <button type="button" onclick="window.location.href=''" id="addListedResources" class="btn btn-success">Add Listed Resources</button>
        </div>
    </div> 
    <!--End of panel panel-default-->
    
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#setCalibration-modal" data-toggle="modal" data-target="#setCalibration-modal"><button type="button" id="setCalibration" class="btn btn-default">Set Calibration</button></a>
        <a href="#scheduleCalibration-modal" data-toggle="modal" data-target="#scheduleCalibration-modal"><button type="button" id="scheduleCalibration" class="btn btn-default">Schedule Calibration</button></a>
    </div>
    </div>
   
    <div class="panel panel-default">
    <div class="panel-heading">Calibration</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="calibrationTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Number</th>
                    <th>Internal / External</th>
                    <th>Calibration Type</th>
                    <th>Frequency</th>
                    <th>Next Calibration Due</th>
                    <th>Scheduled Date</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(!empty($data['assets'])){
                      foreach($data['assets'] as $assetDetail){
                        
                        if(substr($assetDetail['question_answers'],2,3) == 1){
                            echo '<tr>
                                  <td><input type="checkbox" class="chkbox" name="assetname[]" value="' . $assetDetail['id'] .'"></td>
                                  <td>' . $assetDetail['department_name'] . '</td>
                                  <td>' . $assetDetail['categoryType'] . '</td>
                                  <td>' . $assetDetail['assetType'] . '</td>
                                  <td><a href="index.php?module=assets&action=view&id=' . $assetDetail['id']  .'">' . $assetDetail['description'] . '</a></td>
                                  <td>' . $assetDetail['company_asset_number'] . '</td> 
                                  <td></td>
                                  <td></td>  
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>  
                                  <td><a href="index.php?module=assets&action=view&id=' . $assetDetail['id']  .'">view</a></td>
                                  <td><a data-toggle="tooltip" title="Edit asset" href="index.php?module=assets&action=editAsset&id=' . $assetDetail['id'] . '"><span class="glyphicon glyphicon-pencil"></span></td>
                                  <td><a class="trash" data-toggle="modal" data-target="#myModal" data-id="' . $assetDetail['id'] . '"><span class="glyphicon glyphicon-trash"></span></a></td>
                                </tr>';
                          }
                      }
                }
                ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
    
<!-- Set Calibration Modal -->
  <div class="modal fade" id="setCalibration-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Calibration Due Date</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="setCalibrationForm" action="resource_calibration.php">
            <div class="form-group">
                <label class="control-label col-sm-4" for="internalExternal">Internal / External</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="internalExternal" class="form-control">
                    <option value="1">Internal</option>
                    <option value="2">External</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="typeService">Type Service / Maintenance</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="typeService" class="form-control">
                    <option value selected disabled>Please select a service type</option>
                    <option value="1">Normal</option>
                    <option value="2">Pressure Test</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="dueDate">Due Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="dueDate" id="dueDate" required placeholder="">
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="frequency">Frequency</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="frequency" class="form-control">
                    <option value selected disabled>Please select a frequency</option>
                    <option value="1">Daily</option>
                    <option value="2">Weekly</option>
                    <option value="3">Monthly</option>
                    <option value="4">Every 6 Months</option>
                    <option value="5">Yearly</option>
                    <option value="6">Every 2 Years</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="baselineUnit">Baseline Unit</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="baselineUnit" id="baselineUnit" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="measuredIn">Measured In</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="measuredIn" id="measuredIn" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="button" id="addAnotherBtn" class="btn btn-success">Add Another Calibration Type</button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="setCalibrationBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Schedule Calibration Modal -->
  <div class="modal fade" id="scheduleCalibration-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Schedule Calibration</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="scheduleCalibrationForm" action="resource_calibration.php">
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="type" class="form-control">
                    <option value selected disabled>Please select a type</option>
                    <option value="1">Normal</option>
                    <option value="2">Pressure Test</option>
                    </select>
                </div>
            </div>   
            <div class="form-group">
                <label class="control-label col-sm-4" for="startDateTime">Start Date & Time</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="startDateTime" id="startDateTime" required placeholder="">
                </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-sm-4" for="endDateTime">End Date & Time</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="endDateTime" id="endDateTime" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="responsiblePerson">Responsible Person</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more responsible people" data-header="Close">
                        <option>Warren Windovogel</option>
                        <option>Sunnyboy Mathole</option>
                        <option>Nick Botha</option>
                        <option>Nasir Jones</option>
                    </select> 
                </div>
            </div>    
            <div class="form-group">
                <label class="control-label col-sm-4" for="notifyPeople">Notify People</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more people" data-header="Close">
                        <option>Warren Windovogel</option>
                        <option>Sunnyboy Mathole</option>
                        <option>Nick Botha</option>
                        <option>Nasir Jones</option>
                    </select> 
                </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-sm-4" for="notes">Notes</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="notes" id="notes" required></textarea>
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Additional Information</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="serviceProvider">Service Provider</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="serviceProvider" id="serviceProvider" required placeholder="">
                </div>
            </div>   
            <div class="form-group">
                <label class="control-label col-sm-4" for="departmentInvolved">Department Involved</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments" data-header="Close">
                        <option>I.T</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div> 
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="scheduleCalibrationBtn" class="btn btn-success">Schedule Calibration</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<script>
$(function(){
    $("#dueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    
    $("#startDateTime").datetimepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt', 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datetimepicker();
    $("#endDateTime").datetimepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt', 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datetimepicker();
});
$('#calibrationTbl').tablecheckbox();
var chkbox = document.getElementsByClassName("chkbox");

    function onChangeListener() {
        $("#setCalibration ,#scheduleCalibration").removeClass("btn-success").addClass("btn-default");
        for (var i = 0; i < chkbox.length; i++) {
            if (chkbox[i].checked) {
                $("#setCalibration ,#scheduleCalibration").removeClass("btn-default").addClass("btn-success");
            }
        }
    }

    for (var i = 0; i < chkbox.length; i++) {
        var checkbox = chkbox[i];
        checkbox.addEventListener("change", onChangeListener);
    }
</script>
<?php include_once('frontend/templates/footers/default_footer_tpl.php');  
    