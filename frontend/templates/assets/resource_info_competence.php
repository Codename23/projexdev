<?php 
/*
 * Header file
 */
$title = 'Resource Competence';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php');
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
$id =  (int)$_REQUEST['id'];
?> 
<!--End of navigation get_resource_info_competence --> 
<div class="col-lg-10">
<!--sub menu-->
<div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=assetmanagement&module=assets">All Assets</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?action=view&module=assets&id=<?php echo $id;?>">Info</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_documents&module=assets&id=<?php echo $id;?>">Documents</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_competence&module=assets&id=<?php echo $id;?>">Competence</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_substance&module=assets&id=<?php echo $id;?>">Substance</a></li>
        <!--<li><a href="resource_info_sheq.php">SHEQ</a></li>-->
        <!--<li><a href="resource_info_statutory.php">Statutory</a></li>
        <li><a href="resource_info_calibration.php">Calibration</a></li>-->
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_inspected&module=assets&id=<?php echo $id;?>">Inspected</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_sop&module=assets&id=<?php echo $id;?>">SOP</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_maintenance&module=assets&id=<?php echo $id;?>">Maintenance</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=get_resource_info_noc&module=assets&id=<?php echo $id;?>">Non-Conformance</a></li>
    </ul>
    </div>
</div>
<!--End of sub menu-->

<div class="panel panel-default">
<div class="panel-body">
    <a href="#linkCourse-modal" data-toggle="modal" data-target="#linkCourse-modal"><button type="button" id="linkCourse" class="btn btn-success">Link Course</button></a>
</div>
</div>

<div class="panel panel-default">
<div class="panel-heading">Competence</div>
<div class="panel-body">
<div class="table-responsive">
<table width="100%" class="table table-hover">
    <thead>
    <tr>
        <th>Category</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Internal Training</td>
        <td>OHS Induction</td>
    </tr>
    <tr>
        <td>External Training</td>
        <td>First Aid Level 1</td>
    </tr>
    <tr>
        <td>License</td>
        <td>Forklift</td>
    </tr>
    </tbody>
</table>
</div>
</div>
</div>
<!--End of panel panel-default-->

<!-- Link Course Modal -->
  <div class="modal fade" id="linkCourse-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Link Course</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="linkCourseForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="internalTraining">Internal Training</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more internal training" data-header="Close">
                        <option>Internal Training 1</option>
                        <option>Internal Training 2</option>
                        <option>Internal Training 3</option>
                        <option>Internal Training 4</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="externalTraining">External Training</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more external training" data-header="Close">
                        <option>External Training 1</option>
                        <option>External Training 2</option>
                        <option>External Training 3</option>
                        <option>External Training 4</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="license">License</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more licenses" data-header="Close">
                        <option>License 1</option>
                        <option>License 2</option>
                        <option>License 3</option>
                        <option>License 4</option>
                    </select> 
                </div>
            </div>
                <div class="form-group">
                <label class="control-label col-sm-4" for="professionalBodies">Professional Bodies</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more professional bodies" data-header="Close">
                        <option>Professional Bodies 1</option>
                        <option>Professional Bodies 2</option>
                        <option>Professional Bodies 3</option>
                        <option>Professional Bodies 4</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="linkCourseBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');  
?>  
    