<?php
$title = 'View All Asset';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?>  
<!--End of navigation--> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">    
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li class="active"><a href="<?php echo BASE_URL;?>index.php?action=assetmanagement&module=assets">All Assets</a></li>
            <li><a href="index.php?module=assets&action=unlistedAsset">Unlisted Resources</a></li>
            <li><a href="index.php?module=assets&action=statutoryMaintenance">Statutory Maintenance</a></li>
            <li><a href="index.php?module=assets&action=calibration">Calibration</a></li>
            <li><a href="index.php?module=assets&action=nonComformanceHistory">Non-Conformance History</a></li>
            <li><a href="index.php?module=assets&action=settings">Settings</a></li>
            <li><a href="index.php?module=assets&action=archive">Archive</a></li>
        </ul>
        </div>
    </div>
    <!--End of sub menu-->
    <div class="panel panel-default">
        <div class="panel-body">
            <a href="index.php?module=assets&action=addAsset"><button type="button" id="addResource" class="btn btn-success">Add Resource</button></a>
            <a href="#addNonConformance-modal" data-toggle="modal" data-target="#addNonConformance-modal"><button type="button" id="addNonConformance" class="btn btn-success">Load Resource Non-Conformance</button></a>
        </div>
    </div> 
    <!--End of panel panel-default-->
    
    <div class="panel panel-default">
    <div class="panel-heading">Search Resources</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>Wood</option>
                <option>I.T</option>
                <option>Finance</option>
                <option>HR</option>
            </select>
        </div>
        </div> 
            
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Category</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more categories" data-header="Close">
                <option>Fixed Assets</option>
                <option>Moveable Assets</option>
                <option>Raw Materials</option>
                <option>Final Products</option>
                <option>Consumables</option>
                <option>Emergency Equipment</option>
                <option>Motorized Equipment</option>
            </select> 
        </div>
        </div>
        
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Type</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                <option>Building</option>
                <option>Fixed Electrical Equipment</option>
                <option>Key is to be able to add on own behalf</option>
                <option>Forklift</option>
                <option>Wood</option>
                <option>Shutter</option>
                <option>Petrol</option>
                <option>Fire Extinguishers</option>
                <option>Forklift</option>
            </select> 
        </div>
        </div>
            
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Resource Number</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more resource numbers"data-header="Close">
                <option>123456</option>
                <option>123456789</option>
                <option>1245789</option>
            </select>
        </div>
        </div>     
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#statutoryMaintenance-modal" data-toggle="modal" data-target="#statutoryMaintenance-modal"><button type="button" id="statutoryMaintenance" class="btn btn-default">Add to Statutory Maintenance</button></a>
        <a href="#addtoCalibration-modal" data-toggle="modal" data-target="#addtoCalibration-modal"><button type="button" id="addtoCalibration" class="btn btn-default">Add to Calibration</button></a>
    </div>
    </div>
   
    <div class="panel panel-default">
    <div class="panel-heading">All Resources</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="resourceTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description Name</th>
                    <th>Number</th>
                    <th>SOP</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php
                $count = 1; 
               // echo count($data['assets']);
               if($data['assets']): foreach($data['assets'] as $assetDetail):                     
                      echo '<tr>
                              <td><input type="checkbox" class="chkbox" name="assetname[]" value="' . $assetDetail['id'] .'"></td>
                              <td>' . $assetDetail['department_name'] . '</td>
                              <td>' . $assetDetail['categoryType'] . '</td>
                              <td>' . $assetDetail['assetType'] . '</td>
                              <td><a href="index.php?module=assets&action=view&id=' . $assetDetail['id']  .'">' . $assetDetail['description'] . '</a></td>
                              <td>' . $assetDetail['company_asset_number'] . '</td>           
                              <td>View</td>
                              <td><a href="index.php?module=assets&action=view&source_type=Resource&id=' . $assetDetail['id']  .'">View Resource</a></td>                              
                            </tr>';
               //<td><a data-toggle="tooltip" title="Edit asset" href="index.php?module=assets&action=editAsset&id=' . $assetDetail['id'] . '"><span class="glyphicon glyphicon-pencil"></span></td>
                              //<td><a class="trash" data-toggle="modal" data-target="#myModal" data-id="' . $assetDetail['id'] . '"><span class="glyphicon glyphicon-trash"></span></a></td>
                          $count ++;                         
                   endforeach;endif; 
            ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
<!-- Add Non-Conformance Modal -->
  <div class="modal fade" id="addNonConformance-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Non-Conformance</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addNonConformanceForm" action="resource.php">
            <div class="form-group">
                <label class="control-label col-sm-4" for="date">Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="date" id="date"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="dueDate">Due Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="dueDate" id="dueDate"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="group" class="form-control">
                    <option value selected disabled>Please select a group</option>
                    <option value="1">OHS (Health & Safety)</option>
                    <option value="2">E (Environment)</option>
                    <option value="3">Q (Quality)</option>
                    <option value="4">F (Food)</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="group" class="form-control">
                    <option value selected disabled>Please select a type</option>
                    <option value="1">Not Specified</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="department">Department</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments">
                        <option>I.T</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="nonConformanceDetails">Non-Conformance Details</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="nonConformanceDetails" id="nonConformanceDetails"></textarea>
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="recommendedImprovement">Recommended Improvement</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="recommendedImprovement" id="recommendedImprovement"></textarea>
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="resourceInvolved">Resource Involved</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a resource">
                        <option>Resource 1</option>
                        <option>Resource 2</option>
                        <option>Resource 3</option>
                    </select> 
                </div>
            </div>
            <span class="control-label col-sm-4"></span>
            <div class="col-lg-6 col-md-4 col-sm-8">
                <p style="color:red;"><b>Resource not listed</b></p>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notListedResourceType">Not Listed</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedResourceType" id="notListedResourceType" placeholder="Type"> 
                </div>
            </div>
            <div class="form-group">
                <span class="col-sm-4" for="notListedResourceDescription"></span>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedResourceDescription" id="notListedResourceDescription" placeholder="Description"> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="personInvolved">Person Involved</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                        <option>Nick Botha</option>
                        <option>Sunny Mathole</option>
                        <option>Brian Douglas</option>
                    </select> 
                </div>
            </div>
            <span class="control-label col-sm-4"></span>
            <div class="col-lg-6 col-md-4 col-sm-8">
                <p style="color:red;"><b>Person not listed</b></p>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notListedPersonType">Not Listed</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedPersonType" id="notListedPersonType" placeholder="Type"> 
                </div>
            </div>
            <div class="form-group">
                <span class="col-sm-4" for="notListedPersonDescription"></span>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedPersonDescription" id="notListedPersonDescription" placeholder="Description"> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="supplier">Supplier</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a supplier">
                        <option>Supplier 1</option>
                        <option>Supplier 2</option>
                        <option>Supplier 3</option>
                    </select> 
                </div>
            </div>
            <span class="control-label col-sm-4"></span>
            <div class="col-lg-6 col-md-4 col-sm-8">
                <p style="color:red;"><b>Supplier not listed</b></p>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notListedSupplierCompany">Not Listed</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedSupplierCompany" id="notListedSupplierCompany" placeholder="Company Name"> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="uploadPhoto">Take/Upload Photo</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="file" name="uploadPhoto" id="uploadPhoto"> 
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Send To Investigate</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigator">Investigator</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a investigator">
                        <option>Nick Botha</option>
                        <option>Sunny Mathole</option>
                        <option>Brian Douglas</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigatorDueDate">Due Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="investigatorDueDate" id="investigatorDueDate"> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addNonConformanceBtn" class="btn btn-success">Add Non-Conformance</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<script src="js/jquery.tablecheckbox.js"></script>
<script src="js/bootstrap-select.js"></script>
<script>
$(function() {
$("#date").datepicker({
    dateFormat:"yy/mm/dd",
    maxDate: new Date(), 
    minDate: new Date()});
$("#date").datepicker("setDate", new Date());
$("#dueDate").datepicker({
    dateFormat:"yy/mm/dd", 
    minDate: new Date(),
    changeYear:true, 
    changeMonth:true,
    yearRange:"1900:3000"}).datepicker();
$("#investigatorDueDate").datepicker({
    dateFormat:"yy/mm/dd", 
    minDate: new Date(),
    changeYear:true, 
    changeMonth:true,
    yearRange:"1900:3000"}).datepicker();
});

$('#resourceTbl').tablecheckbox();
var chkbox = document.getElementsByClassName("chkbox");

    function onChangeListener() {
        $("#statutoryMaintenance ,#addtoCalibration").removeClass("btn-success").addClass("btn-default");
        for (var i = 0; i < chkbox.length; i++) {
            if (chkbox[i].checked) {
                $("#statutoryMaintenance ,#addtoCalibration").removeClass("btn-default").addClass("btn-success");
            }
        }
    }

    for (var i = 0; i < chkbox.length; i++) {
        var checkbox = chkbox[i];
        checkbox.addEventListener("change", onChangeListener);
    }
</script>
<?php include_once('frontend/templates/footers/default_footer_tpl.php');  
    