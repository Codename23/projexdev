<?php
$title = 'Edit Asset';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?>  
<div class="col-lg-10">
<!--sub menu-->
<div class="row">    
    <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li class="active"><a href="index.php?module=assets&action=defaultAction">All Assets</a></li>
            <li><a href="index.php?module=assets&action=defaultAction">Unlisted Resources</a></li>
            <li><a href="index.php?module=assets&action=defaultAction">Statutory Maintenance</a></li>
            <li><a href="index.php?module=assets&action=defaultAction">Calibration</a></li>
            <li><a href="index.php?module=assets&action=defaultAction">Non-Conformance History</a></li>
            <li><a href="index.php?module=assets&action=defaultAction">Settings</a></li>
            <li><a href="index.php?module=assets&action=defaultAction">Archive</a></li>
        </ul>
    </div>
</div>
<!--End of sub menu-->
<div class="panel-group">
    <form class="form-horizontal" name="addAsset" method="post" action="<?php echo BASE_URL; ?>/index.php?module=assets&action=saveAsset">
    <div class="panel panel-default">
    <div class="panel-heading">New Resource</div>
    <div class="panel-body"> 
        <div class="form-group">
            <label class="control-label col-sm-4" for="departmentid">Department</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect form-control" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a department" id="departmentid" name="departmentid">
                    <?php
                        foreach($data['department'] as $departmentDetailsArr){
                            if($data['editassets']['department_id'] == $departmentDetailsArr['id']){
                                echo '<option selected="selected" value="' . $departmentDetailsArr['id']. '">' . $departmentDetailsArr['department_name'] . ' - ' . $departmentDetailsArr['department_building'] . '</option>';
                            }else{
                                echo '<option value="' . $departmentDetailsArr['id']. '">' . $departmentDetailsArr['department_name'] . ' - ' . $departmentDetailsArr['department_building'] . '</option>';
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="categoryType">Resource Category</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect form-control" id="categoryType" name="categoryType" title="Select a category" required>
                  <?php
                    foreach($data['editCategoryType'] as $categoryTypeArr){
                        if($data['editassets']['categorytype_id'] == $categoryTypeArr['id']){
                            echo '<option selected="selected" value="' .$categoryTypeArr['id']. '">' .$categoryTypeArr['name']. '</option>';
                        }else{
                            echo '<option value="' .$categoryTypeArr['id']. '">' .$categoryTypeArr['name']. '</option>';  
                        }
                    }
                  ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="assetType">Type</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" id="assetType" name="assetType" title="Select a type" required></select> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="description">Description Name</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="description" name="description" value="<?php echo $data['editassets']['description']; ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="assetNumber">Asset No.</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="assetNumber" name="assetNumber" value="<?php echo $data['editassets']['company_asset_number']; ?>" required>
            </div>
        </div>
        <?php 
        $count = 0;
        if(!empty($data['assetQuestions'])){

            $answersListArr = array();
            $strAnswersList = explode(',' , $data['editassets']['question_answers']);

            foreach($strAnswersList as $answersList){
                $answersListArr[] = $answersList;
            }
            foreach($data['assetQuestions'] as $QuestionDetailsArr){
            ?>
            <div class="form-group">
                <label class="control-label col-sm-4" for="question"><?php echo $QuestionDetailsArr['description'] ?></label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <label class="radio-inline"><input type="radio" <?php if(!empty($data['editassets']['question_answers'])){if($answersListArr[$count] == 1){ echo ' checked="checked" '; }}?> name="<?php echo $QuestionDetailsArr['name'] ?>" value="1">Yes</label>
                <label class="radio-inline"><input type="radio" <?php if(!empty($data['editassets']['question_answers'])){if($answersListArr[$count] == 0){ echo ' checked="checked" '; }}?> name="<?php echo $QuestionDetailsArr['name'] ?>" value="0">No</label>
                </div>
            </div>
            <?php 
            $count ++; 
            }
        }
        ?>   
    </div>
    </div>
    <div class="panel panel-default">
    <div class="panel-heading">Additional Information</div>
        <div class="panel-body">
            <div class="form-group">
                <label class="control-label col-sm-4" for="manufacturer">Manufacturer/Brand</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="manufacturer" id="manufacturer" value="<?php echo $data['editassets']['manufacture_name']; ?>" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="inventoryNumber">Serial Number</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="inventoryNumber" id="inventoryNumber" value="<?php echo $data['editassets']['serial_number']; ?>" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="datePurchased">Date Purchased</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="datePurchased" name="datePurchased" value="<?php echo $data['editassets']['date_purchased']; ?>" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="cost">Cost</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="cost" name="cost" value="<?php echo $data['editassets']['cost']; ?>" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notes">Notes</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" id="notes" name="notes"><?php echo $data['editassets']['cost']; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="uploadPhotos">Upload Photos</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="file" id="uploadPhotos" name="uploadPhotos">
                </div>
            </div>
        </div>
    </div>
    <!--End of panel panel-default-->
</form>   
</div>
<div class="panel panel-default">
    <div class="panel-heading">Documents</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="resourceTbl">
            <thead>
                <tr>
                    <th>Document Description</th>
                    <th>Date Loaded</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(!empty($data['assetDocuments'])){
                    foreach($data['assetDocuments'] as $assetDocumentsDetails){
                        echo
                        '<tr>
                            <td>'. $assetDocumentsDetails['filename'] .'</td>
                            <td>'. date("d M Y ", strtotime($assetDocumentsDetails['date_created'])) .'</td>
                            <td><a href="'. HOSTNAME . '/documents/documents/'. $assetDocumentsDetails['filename'] .'">View</a></td>
                            <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                        </tr>';
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="form-group">
        <div class="col-sm-8">
        <a href="#addDoc-modal"   data-toggle="modal" data-target="#addDoc-modal"><button type="button" class="btn btn-success">Add Document</button></a>
        </div>
    </div>
    </div>
</div>
<!--Add Doc Modal -->
  <div class="modal fade" id="addDoc-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Document</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" enctype="multipart/form-data" name="addDocForm" action="index.php?module=assets&action=saveAssetDocument">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="docDescription">Doc Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="docDescription" id="docDescription" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="docUpload">Upload</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="hidden" name="assetid" value="<?php echo $_GET['id']; ?>"> 
                    <input type="file" name="docUpload" id="docUpload"> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" id="addDocBtn" class="btn btn-success">Add</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<script>
$(document).ready(function() { 
    $(".hideInspection").hide();
    showCategoryType();
    
    $('.optStatutoryInspection').change(function(){
        var option = $(this).val();
        if(option === 'Yes'){
           $(".hideInspection").show();
       }else{
           $(".hideInspection").hide();
       }
    });

    $("#datePurchased").datepicker({dateFormat:"yy/mm/dd", maxDate: new Date(),changeYear:true, changeMonth:true,yearRange:"1900:3000"}).datepicker();
    $("#dateExpired").datepicker({dateFormat:"yy/mm/dd", minDate: new Date(),changeYear:true, changeMonth:true,yearRange:"1900:3000"}).datepicker();

    tinymce.init({
        selector: '.tinyMCEselector',
        menubar: false
    });

});

$('body').on('change', '#categoryType', function(){
    var group_id = $(this).val();
    showCategoryType(group_id, 'assetType');
    $('#assetType').selectpicker('refresh');
});
/**
 * Method to show category types
 * @param {String} selectorid
 * @param {String} groupid
 * @return {Array}
 */
 function showCategoryType(groupid, selectorid){
     var opt = '';
     var selector = "#"+selectorid;
     $.ajax({
         url : "index.php?module=assets&action=showAssetTypes",
         type: "POST",
         data :'categoryId='+ groupid,
         dataType:'json',
         success: function (data){
                 $(selector).find('option').remove();
                 if(data.length > 0){
                     $.each(data, function(key, val) {
                         opt = $('<option/>');
                         opt.val(val.id);
                         opt.text(val.name);
                         $(selector).append(opt);
                     }); 
                     $(selector).selectpicker('refresh');
                 }else{
                    $(selector).find('option').remove();
                 }
         },
         error: function(error){
             console.log('Request failed: ' + error);
         }
     });

</script> 
<?php include_once('frontend/templates/footers/default_footer_tpl.php');
    