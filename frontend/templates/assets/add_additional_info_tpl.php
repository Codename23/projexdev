<?php
$title = 'Add Additional Assets';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?> 
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2"></div>
        <!--Start of main col-->
        <div class="col-lg-8">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Add Additional information</h4></div>
                <div class="panel-body"> 
                    <form class="form-horizontal" method="post" action="<?php echo BASE_URL; ?>/index.php">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="manufacturerManual">Manufacturers Manual</label>
                        <div class="col-lg-4 col-md-4 col-sm-8">
                            <input type="file" id="manufacturerManual" name="manufacturerManual">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="msdn">MSDN</label>
                        <div class="col-lg-4 col-md-4 col-sm-8">
                            <input type="file" id="msdn" name="msdn">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="uploadguarantee">Upload Guarantee</label>
                        <div class="col-lg-4 col-md-4 col-sm-8">
                            <input type="file" id="uploadguarantee" name="uploadguarantee">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="uploadimage">Upload image</label>
                        <div class="col-lg-4 col-md-4 col-sm-8">
                            <input type="file" id="uploadimage" name="uploadimage">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="cost">Cost of a Resource</label>
                        <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="number" class="form-control" id="cost" name="cost" placeholder="Enter cost of a resource" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                        <input type="hidden" name="action" value=" ">
                        <input type="hidden" name="module" value="assets">
                        <button type="submit" name="submit" class="btn btn-success">Submit</button>
                        <button type="submit" name="submit" value="cancel" class="btn btn-default" formnovalidate="formnovalidate">Back</button>
                        </div>
                    </div>
                    </form>
                </div>
                </div>
                <!--End of panel-default-->
            </div>
        </div>
        <!--End of main col-->
        <div class="col-lg-2"></div>
    </div>
</div>
<script>
$(document).ready(function(){
        

});
</script> 
<?php include_once('frontend/templates/footers/default_footer_tpl.php');?>
          