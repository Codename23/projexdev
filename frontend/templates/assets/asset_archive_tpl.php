<?php
$title = 'Archive';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?>  
<!--End of navigation--> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">    
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li><a href="index.php?module=assets&action=defaultAction">All Assets</a></li>
            <li><a href="index.php?module=assets&action=unlistedAsset">Unlisted Resources</a></li>
            <li><a href="index.php?module=assets&action=statutoryMaintenance">Statutory Maintenance</a></li>
            <li><a href="index.php?module=assets&action=calibration">Calibration</a></li>
            <li><a href="index.php?module=assets&action=nonComformanceHistory">Non-Conformance History</a></li>
            <li><a href="index.php?module=assets&action=settings">Settings</a></li>
            <li class="active"><a>Archive</a></li>
        </ul>
        </div>
    </div>
    <!--End of sub menu-->

    <div class="panel panel-default">
    <div class="panel-heading">Archive</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="resourceTbl">
            <thead>
                <tr>
                    <th>Module</th>
                    <th>Category</th>
                    <th>Description</th>
                    <th>Reason</th>
                    <th>Modified By</th>
                    <th>Date Modified</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">Restore</a></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">Restore</a></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">Restore</a></td>
                    <td><a href="">View</a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
        </div>
</div><!--End of container-fluid-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php');    
    