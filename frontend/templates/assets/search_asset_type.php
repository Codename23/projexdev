<?php
/**
 * Ajax script to search resource type
 * 
 * @package sheqonline
 * @author Sunny Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
//Include the common file
require_once '../../../config.php';

processAjax();

function processAjax() 
{
    global $db;
    $serchterm = strip_tags(trim($_GET["assettype"]));   

    $sql = "SELECT id, name FROM tbl_asset_type WHERE name LIKE '%" . $serchterm ."%' LIMIT 20";
    $response = $db->getall($sql);
    
    if(!empty($response)){
        foreach($response as $assetDetailsArr){
            $data[] = array("id"=>$assetDetailsArr["id"], "text"=>$assetDetailsArr["name"]);
        }
    }else{
        $data[] = array("id"=>"0", "text"=>"Asset Type Not Found");
    }
    
    //return the result in jason
    echo json_encode($data);  
}
?> 