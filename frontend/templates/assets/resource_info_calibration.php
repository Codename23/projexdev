<?php 
/*
 * Header file
 */
include_once('includes/header.php');
?>
<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('includes/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('includes/side-menu.php'); 
?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<div class="row">    
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="resource.php">All Assets</a></li>
        <li><a href="resource_info.php">Info</a></li>
        <li><a href="resource_info_documents.php">Documents</a></li>
        <li><a href="resource_info_substance.php">Substance</a></li>
        <li><a href="resource_info_sheq.php">SHEQ</a></li>
        <li><a href="resource_info_inspected.php">Inspected</a></li>
        <li><a href="resource_info_statutory.php">Statutory</a></li>
        <li class="active"><a href="resource_info_calibration.php">Calibration</a></li>
    </ul>
    </div>
</div>
<!--End of sub menu-->

<div class="panel panel-default">
    <div class="panel-heading">Calibration Type</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Calibration</th>
                    <th>Frequency</th>
                    <th>Base Measurement</th>
                    <th>Measured In</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->

<div class="panel panel-default">
    <div class="panel-heading">Calibration</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Calibration Type</th>
                    <th>Service Provider</th>
                    <th>Measurements</th>
                    <th>Measured In</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View Report</a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View Report</a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View Report</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('includes/footer.php');
?>  
    