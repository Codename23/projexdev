<?php 
/*
 * Header file
 */
$title = 'Statutory Maintenance Plan';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
//set the inspection_id

?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=assets&action=statutory_maintenance_plan">Statutory Maintenance Plan</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=assets&action=statutory_upcoming">Upcoming</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?module=assets&action=job_listing">Job</a></li>
        <!--<li><a href="statutory_approval.php">Approval</a></li>
        <li><a href="statutory_scheduled.php">Scheduled</a></li>
        <li><a href="statutory_active.php">Active</a></li>-->
        <li><a href="statutory_history.php">History</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

    <div class="panel panel-default">
    <div class="panel-heading">Search Statutory Maintenance Plan</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-3 col-sm-3">
        <div class="form-group">
            <p><b>Asset Type</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                <option>Forklift</option>
                <option>Wood</option>
                <option>Shutter</option>
                <option>Petrol</option>
                <option>Fire Extinguishers</option>
            </select> 
        </div>
        </div>
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-body">
        <a href="<?php echo BASE_URL;?>/index.php?action=viewAssetStatutory&module=assets" ><button type="button" id="importFromAssets" class="btn btn-success">Import From Assets</button></a>
        <a href="#addStatutoryMaintenance-modal" data-toggle="modal" data-target="#addStatutoryMaintenance-modal"><button type="button" id="addStatutoryMaintenance" class="btn btn-success">Add Statory Maintenance Plan</button></a>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Statutory Maintenance Plan</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover" id="statutoryPlanTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Number</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
               <?php
                          $count = 1;
                            if(!empty($data['assets'])){
                                foreach($data['assets'] as $assetDetail){
                                $url = BASE_URL."/index.php?edit={$assetDetail["record_id"]}&action=statutory_plan&module=assets";
                                echo "<tr>
                                        <td>
                                         <input type='checkbox' class='checkbox' name='cbxAsset[]'/></td>
                                        <td>{$assetDetail['department_name']}</td>
                                        <td>{$assetDetail['categoryType']}</td>
                                        <td>{$assetDetail['assetType']}</td>
                                        <td>{$assetDetail['description']}</td>
                                        <td>{$assetDetail['company_asset_number']}</td>
                                        <td><a href='{$url}'>Statutory Plan</a></td>
                                       <td><a href='#'>View Resource</a></td>
                                        <td></td>
                                        <td></td>";                               
                                }
                            }
                            ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!-- Import From Assets Modal -->
  <div class="modal fade" id="importFromAssets-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Import From Assets</h4>
        </div>
        <div class="modal-body table-responsive">
            <form class="form-horizontal" method="post" name="searchAssets" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="searchDepartment">Department</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" title="Select a department">
                        <?php
                        foreach($data['allDepartments'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                        }?>                                                
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="searchType">Type</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect search_type" data-live-search="true" data-live-search-placeholder="Search" title="Select a type">

                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="button" id="searchAssetsBtn" class="btn btn-success">View</button>
                    </div>
                </div>
            </form>
            <table width="100%" class="table table-hover" id="importFromAssetsTbl">
            <thead>
                <tr>
                    <th><input type="checkbox" class="chkbox"></th>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Number</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="checkbox" class="chkbox"></td>
                    <td>Warehouse</td>
                    <td>Fix Electrical Equipment</td>
                    <td>Compressor</td>
                    <td>Big Compressor</td>
                    <td>569</td>
                </tr>
            </tbody>
        </table>
        <div class="form-group">
            <div class="col-sm-8">
            <button type="button" id="importFromAssetsBtn" class="btn btn-success">Add</button>
            </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!--Add Statutory Maintenance Modal -->
  <div class="modal fade" id="addStatutoryMaintenance-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Statutory Maintenance</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addStatutoryMaintenanceForm" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="internalExternal">Internal / External</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="form-control" name="internalExternal" id="internalExternal" required>
                        <option value="1">Internal</option>
                        <option value="2">External</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="typeService">Type Service / Maintenance</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                        <option value="1">Type 1</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="description">Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="description" id="description" required placeholder=""></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Notes</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="notes" id="notes" required placeholder=""></textarea>
                    </div>
                </div>
                <div class="form-group" id="frequency">
                    <label class="control-label col-sm-4" for="frequency">Frequency</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="form-control" name="frequency" id="frequency" required>
                        <option value selected disabled>Please select a frequency</option>
                       <option value="Daily">Daily</option>
                        <option value="Weekly">Weekly</option>
                        <option value="Monthly">Monthly</option>
                        <option value="Every 6 Months">Every 6 Months</option>
                        <option value="Yearly">Yearly</option>
                        <option value="Every 2 Years">Every 2 Years</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="dueDate">Due Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="dueDate" id="dueDate" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" name="addBtn" id="addBtn" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<!--<script src="js/jquery.tablecheckbox.js"></script>-->
<script>
 $(document).ready(function()
 {{ 
         
    if(".search_type").on("click",function()
    {
       var id = alert(this.value);
       alert(id);
    });
    $('.selectpicker').on('change', function(){
              
        var selected = $(this," option:selected").val();
        focus_dropdown = $(this).parent().children('select').attr('id');
        switch(focus_dropdown)
        {
            case 'search_groups':
                $("#groups_selected").val(selected);
                break;
           case 'search_departments':
               $("#departments_selected").val(selected);
                break;                
            case 'search_status':
                $("#status_selected").val(selected);
                break;
        }
       // console.log(focus_dropdown);
    });
    $('#statutoryPlanTbl').tablecheckbox();
    var chkbox = document.getElementsByClassName("chkbox");

    $('#importFromAssetsTbl').tablecheckbox();
    var chkbox = document.getElementsByClassName("chkbox");
 });
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');  
?>  
    