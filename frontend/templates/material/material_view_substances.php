<?php 
/*
 * Header file
 */
$title = "Material View Substance";
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<?php
  global $objMaterial;
  
?>
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllMaterials&module=material">All Materials</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&action=view_material_info&module=material">Info</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&action=material_view_supply_usage&module=material">Supply / Usage</a></li>
                <li ><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=material_view_docs&module=material">Documents</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=material_view_substances&module=material">Substance</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=material_view_sop&module=material">SOP</a></li>
    </ul>
    </div>
</div>
<!--End of sub menu-->
    
<div class="panel panel-default">
    <div class="panel-body">
        <a href='resource_info_substance_add_substance.php'><button type="button" id="addSubstance" class="btn btn-success">Add Substance</button></a>
        <a href="#linkSubstance-modal" data-toggle="modal" data-target="#linkSubstance-modal"><button type="button" id="linkSubstance" class="btn btn-success">Link Substance</button></a>
    </div>
</div> 

<div class="panel panel-default">
<div class="panel-heading">Substances</div>
<div class="panel-body">
<div class="table-responsive">
    <table width="100%" class="table table-hover">
        <thead>
            <tr>
                <th>Category</th>
                <th>Type</th>
                <th>Description</th>
                <th>Inventory No</th>
                <th>Departments</th>
                <th>Linked to Asset</th>
                <th>Substance Properties</th>
                <th>Properties Emergency</th>
                <th>MSDS</th>
                <th>SOP</th>
                <th>Details</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Consumable</td>
                <td>Substance</td>
                <td>Diesel</td>
                <td>12413</td>
                <td><a href="">View</a></td>
                <td><a href="">View</a></td>
                <td class="green-table-bg">C</td>
                <td class="green-table-bg">C</td>
                <td><a href="">View</a></td>
                <td><a href="">SOP</a></td>
                <td><a href="">Add Details</a></td>
                <td><a href="">View Substance</a></td>
                <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
            </tr>
            <tr>
                <td>Consumable</td>
                <td>Substance</td>
                <td>Petrol</td>
                <td>1231</td>
                <td><a href="">View</a></td>
                <td><a href="">View</a></td>
                <td class="red-table-bg">NC</td>
                <td class="red-table-bg">NC</td>
                <td><a href="">View</a></td>
                <td><a href="">SOP</a></td>
                <td><a href="">Add Details</a></td>
                <td><a href="">View Substance</a></td>
                <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
            </tr>
        </tbody>
    </table>
</div>
</div>
</div>
<!--End of the panel panel-default-->

<!--Link Substance Modal -->
  <div class="modal fade" id="linkSubstance-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Link Substance</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="linkSubstanceForm" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="category">Category</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a category">
                        <option>Category 1</option>
                        <option>Category 2</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="type">Type</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                        <option>Type 1</option>
                        <option>Type 2</option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" id="linkSubstanceBtn" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    