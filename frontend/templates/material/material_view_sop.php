<?php 
/*
 * Header file
 */
$title = "Material SOP";
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<?php
  global $objMaterial;
  $id =  (int)$_REQUEST['material_id'];
?>
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllMaterials&module=material">All Materials</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=view_material_info&module=material">Info</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=material_view_supply_usage&module=material">Supply / Usage</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=material_view_docs&module=material">Documents</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=material_view_substances&module=material">Substance</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=material_view_sop&module=material">SOP</a></li>
    </ul>
    </div>
</div>   
<!--End of sub menu-->
    
    
    <form class="form-horizontal" method="post" name="sopForm" action="">
<div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">SOP</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#addSop-modal" data-toggle="modal" data-target="#addSop-modal"><button type="button" id="addSop" class="btn btn-success">Add Standard Operating Procedure</button></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th width='80%'></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $counter = 0;
                                if($data['sops']): foreach($data['sops'] as $sops):   
                                      $counter++;
                                ?>
                                <tr>
                                    <td><?php echo $counter;?></td>
                                    <td><?php echo $sops["description"];?></td>
                                    <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                                </tr>
                                <?php endforeach;endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">SOP Documents</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#addSopDoc-modal" data-toggle="modal" data-target="#addSopDoc-modal"><button type="button" id="addSopDoc" class="btn btn-success">Add SOP Document</button></a>
                        </div>
                    </div>
  
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Document Description</th>
                                    <th>Document</th>
                                    <th>Date Uploaded</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                                $counter = 0;
                                if($data['sopsDoc']): foreach($data['sopsDoc'] as $sops):                                
                                      $counter++;
                                        ?>
                                        <tr>
                                            <td><?php echo $counter;?></td>
                                            <td><?php echo $sops["description"];?></td>
                                            <td><a href="<?php echo BASE_URL.'/frontend/documents/'.$sops["document_name"];?>" target="_blank">Download</a></td>
                                            <td><?php echo $sops["date_created"];?></td>
                                        </tr>
                                   <?php endforeach;endif; ?>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Risk Assessment</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#requestAssessment-modal" data-toggle="modal" data-target="#requestAssessment-modal"><button type="button" id="requestAssessment" class="btn btn-success">Request SHEQF Assessment</button></a>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#addRiskAssessment-modal" data-toggle="modal" data-target="#addRiskAssessment-modal"><button type="button" id="addRiskAssessment" class="btn btn-success">Add Risk Assessment</button></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Risk No</th>
                                    <th>Group</th>
                                    <th>Hazard Types</th>
                                    <th>Hazard Description</th>
                                    <th>Risk Types</th>
                                    <th>Risk Desription</th>
                                    <th>Score</th>
                                    <th>Control</th>
                                    <th>Control Description</th>
                                    <th>Control in place</th>
                                    <th>New Score</th>
                                    <th>Date Added</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                                $counter = 0;
                                function getClass($row,$column)
                                {
                                    $class = "";
      
                                      //score
                                      if((int)$row[$column] < 51)
                                      {
                                          $class = "green-btn";
                                      }
                                      if((int)$row[$column] > 50 && (int)$row[$column] < 101)
                                      {
                                          $class = "yellow-btn";
                                      }
                                      if((int)$row[$column] > 101 && (int)$row[$column] < 151)
                                      {
                                          $class = "red-btn";
                                      }
                                      if((int)$row[$column] > 151)
                                      {
                                          $class = "majordanger-btn";
                                      }
                                      return $class;
                                }
                                if($data['risk_assessment']): foreach($data['risk_assessment'] as $risk):                                
                                      $counter++;
                                      $class = "";
                                      $control_in_place = "No";
                                      if($risk["is_control_in_effect"] == 1)
                                      {
                                          $control_in_place = "Yes";
                                      }
                                      $p_class = getClass($risk,"pscore");
                                      $new_score = getClass($risk,"new_score");
                                      //new score
                                      
                                        ?>
                                        <tr>
                                            <td><?php echo $counter;?></td>
                                            <td><?php echo $risk["group_id"];?></td>
                                           <td><?php echo $risk["hazard_type"];?></td>
                                           <td><?php echo $risk["hazard_description"];?></td>
                                           <td><?php echo $risk["risk_type"];?></td>
                                           <td><?php echo $risk["risk_description"];?></td>
                                           <td class="<?php echo $p_class;?>"><?php echo $risk["pscore"];?></td>
                                           <td><?php echo $risk["control_type"];?></td>
                                           <td><?php echo $risk["control_description"];?></td>
                                           <td><?php echo $control_in_place;?></td>
                                           <td class="<?php echo $new_score;?>"><?php echo $risk["new_score"];?></td>
                                           <td><?php echo $risk["date_created"];?></td>                                            
                                           <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                           <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                        </tr>
                                   <?php endforeach;endif; ?>
                            </tbody>
                        </table>    
                    </div><br/>
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#sheqfNotes-modal" data-toggle="modal" data-target="#sheqfNotes-modal"><button type="button" id="sheqfNotes" class="btn btn-success">SHEQF Notes</button></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Group</th>
                                    <th>Topic</th>
                                    <th>Description</th>
                                    <th>Date Added</th>
                                </tr>
                            </thead>
                            <tbody><?php
                                        if($data['sheqf_notes']): foreach($data['sheqf_notes'] as $notes):                                
                                        ?>
                                        <tr>                                           
                                           <td><?php echo $notes["group_id"];?></td>
                                           <td><?php echo $notes["topic"];?></td>
                                           <td><?php echo $notes["notes"];?></td>
                                           <td><?php echo $notes["date_created"];?></td>                                            
                                        </tr>
                                   <?php endforeach;endif; ?>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">PPE Requirements</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#minimumPpe-modal" data-toggle="modal" data-target="#minimumPpe-modal"><button type="button" id="minimumPpe" class="btn btn-success">Minimum PPE</button></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Foot Protection</td>
                                    <td>Safety Shoes</td>
                                </tr>
                                <tr>
                                    <td>Head PRotection</td>
                                    <td>Hard Hat</td>
                                </tr>
                                <tr>
                                    <td>Visual</td>
                                    <td>High Vest</td>
                                </tr>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Permit Link</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#permitReqiurements-modal" data-toggle="modal" data-target="#permitReqiurements-modal"><button type="button" id="permitReqiurements" class="btn btn-success">Permit Requirements</button></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Link Document</th>
                                </tr>
                            </thead>
                            <tbody>                                
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    <!--End of panel-default-->
    
<!-- Add SOP Modal -->
  <div class="modal fade" id="addSop-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add SOP</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addSopForm" action="<?php echo BASE_URL;?>/index.php?&material_id=<?php echo $_GET["material_id"];?>&action=post_sop_material&module=material&source_type=Material">
                <input type="hidden" name="source_type" value="Material"/>
                <input type="hidden" name="asset_id" value="<?php echo $id?>"/>
            <div class="form-group">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="sopDescription">Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="sopDescription" id="sopDescription" required placeholder=""> 
                    </div>
                </div>
            </div>
   
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addSopDocBtn" name="addSopDocBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
    
<!-- Add SOP Doc Modal -->
  <div class="modal fade" id="addSopDoc-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add SOP Document</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" enctype="multipart/form-data" name="addSopDocForm" action="<?php echo BASE_URL.'/index.php?action=upload_sop_document_material&module=material&source_type=Material&material_id='.$id;?>">
                <input type="hidden" name="source_type" value="Material"/>
                <input type="hidden" name="asset_id" value="<?php echo $id?>"/>
            <div class="form-group">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="docDescription">Document Description</label>
                        <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="docDescription" id="docDescription" required placeholder=""> 
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="docUpload">Document Upload</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="file" name="docUpload" id="docUpload" required> 
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addSopDocBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
    
<!-- Request Assessment Modal -->
  <div class="modal fade" id="requestAssessment-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Request SHEQF Assessment</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="requestAssessmentForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="sheqfPriority">Priority</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more priorities" data-header='Close'>
                        <option>Priority 1</option>
                        <option>Priority 2</option>
                        <option>Priority 3</option>
                        <option>Priority 4</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="sheqfDueDate">Due Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="sheqfDueDate" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="sheqfResponsiblePerson">Responsible Person</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                        <option>Nick Botha</option>
                        <option>Sunny Mathole</option>
                        <option>Brian Douglas</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="sheqfNotes">Notes</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" id="sheqfNotes" required placeholder=""></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="sheqfBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
    
<!-- Add Risk Assessment Modal -->
  <div class="modal fade" id="addRiskAssessment-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Risk Assessment</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addRiskAssessmentForm" action="<?php echo BASE_URL.'/index.php?action=post_risk_assessment_material&module=material&source_type=Material&material_id='.$id;?>">
                <input type="hidden" name="source_type" value="Material"/>
                <input type="hidden" name="asset_id" value="<?php if(isset($_POST['asset_id']))
                {
                    echo $_POST["asset_id"];
                }
                else
                {
                    echo $_GET["material_id"];
                }?>"/>
                
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="risk_group" class="form-control">
                        <option value selected disabled>Please select a group</option>
                             <?php
                        foreach($data['allSheqTeams'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['sheqteam_name']}</option>";
                        }?>
                    </select>
                </div>
            </div>
            
            <div class="modal-custom-h5"><span><h5>Hazard</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="hazardType">Hazard Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="harzard_type" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a hazard type">
                       <?php
                            foreach($data['hazard'] as $hazard)
                            {
                                echo "<option value='{$hazard["id"]}'>{$hazard['hazard']}</option>";
                            }
                        ?>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="hazardDescription">Hazard Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="hazard_description" id="hazardDescription" required placeholder="">
                </div>
            </div>
            
            <div class="modal-custom-h5"><span><h5>Risk</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="riskType">Risk Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect" name="risk_type" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more risk types">
                   <?php
                            foreach($data['risk'] as $risk)
                            {
                                echo "<option value='{$risk["id"]}'>{$risk['risk']}</option>";
                            }
                        ?>
                </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="riskDescription">Risk Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="risk_description" id="riskDescription" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <!--<span id="pscore"></span>-->
                <input type="text" name="pscore" id="pscore"/>
                <a href="#risk-assessment-score-modal" data-toggle="modal" data-target="#risk-assessment-score-modal"><button type="button" id="risk-assessment-score" class="btn btn-success">Add Score</button></a>
                </div>
            </div>
            
            <div class="modal-custom-h5"><span><h5>Control</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="controlType">Control Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <select class="selectpicker form-multiselect get_control_reduction" name="control_type" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more control types">
                     <?php
                            foreach($data['control'] as $control)
                            {
                                echo "<option value='{$control["id"]}'>{$control['control_type']}</option>";
                            }
                        ?>
                </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="controlDescription">Control Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="control_description" id="controlDescription" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="controlInEffect">Control Currently in Effect</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <label class="radio-inline"><input type="radio" name="controlInEffect" id="controlInEffect1" value="1" required>Yes</label>
                    <label class="radio-inline"><input type="radio" name="controlInEffect" id="controlInEffect2" value="0" >No</label>
                </div>
            </div>
            <div class="control hidden-div">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="controlSuggestedDueDate">Suggested Due Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="controlSuggestedDueDate" name="controlSuggestedDueDate" >
                    </div>
                </div>
               <div class="form-group">
                    <label class="control-label col-sm-4" for="controlResponsiblePerson">Responsible Person</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                        <option>Nick Botha</option>
                        <option>Brian Douglas</option>
                        <option>Sunny Mathole</option>
                    </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="controlNotes">Notes</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows='5' class="form-control" id="controlNotes" name="controlNotes" ></textarea>
                    </div>
                </div>
            </div>
            <div class="controlYes hidden-div2">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="controlYesReduction">Reduced By</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" id="controlYesReduction" name="controlYesReduction" value='50%' readonly="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="controlYesScore">New Score</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" id="controlYesScore" name="controlYesScore" value='200' readonly="">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <input type="submit" id="addRiskAssessmentBtn" name="addRiskAssessmentBtn" class="btn btn-success" value="Submit">
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Modal -->
  <div class="modal fade" id="risk-assessment-score-modal" role="dialog">
    <div class="modal-dialog risk-assessment-score-dialog-modal">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Risk Assessment Score</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" name="riskAssessmentScoreForm" >
              <div class="table-responsive">
                  <table width="100%" class="table table-bordered">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="text-center" colspan="6">Severity of the potential injury/damage</th>
                </tr>
                </thead>
                <tbody>
                <tr class="text-center">
                    <td></td>
                    <td width="16%"><p>Insignificant damage to Property, Equipment or Minor Injury</p></td>
                    <td width="16%"><p>Non-Reportable Injury, minor loss of Process or slight damage to Property</p></td>
                    <td width="16%"><p>Reportable Injury moderate loss of Process or limited damage to Property</p></td>
                    <td width="16%"><p>Major Injury, Single Fatality critical loss of Process/damage to Property</p></td>
                    <td width="16%"><p>Multiple Fatalities Castastrophic Loss of Business</p></td>
                </tr>
                <tr class="text-center">
                    <td><b>Likelihood of the hazard happening</b></td>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                </tr>

                <tr class="text-center number">
                    <td class="sideTitle">Almost Certain<br/>50</td>
                    <td class="green-btn">50</td>
                    <td class="yellow-btn">100</td>
                    <td class="red-btn">150</td>
                    <td class="majordanger-btn">200</td>
                    <td class="majordanger-btn">250</td>
                </tr>
                <tr class="text-center number"> 
                    <td class="sideTitle">Will probably occur<br/>40</td>
                    <td class="green-btn">40</td>
                    <td class="yellow-btn">80</td>
                    <td class="red-btn">120</td>
                    <td class="majordanger-btn">160</td>
                    <td class="majordanger-btn">250</td>
                </tr>
                <tr class="text-center number">
                    <td class="sideTitle">Possible occur<br/>30</td>
                    <td class="green-btn">30</td>
                    <td class="yellow-btn">60</td>
                    <td class="yellow-btn">90</td>
                    <td class="red-btn">120</td>
                    <td class="red-btn">150</td>
                </tr>
                <tr class="text-center number">
                    <td class="sideTitle">Remote possibility<br/>20</td>
                    <td class="green-btn">20</td>
                    <td class="green-btn">40</td>
                    <td class="yellow-btn">60</td>
                    <td class="yellow-btn">80</td>
                    <td class="yellow-btn">100</td>
                </tr>
                <tr id="extreme" class="text-center number">
                    <td class="sideTitle">Extremely Unlikely<br/>100</td>
                    <td class="green-btn">10</td>
                    <td class="green-btn">20</td>
                    <td class="green-btn">30</td>
                    <td class="green-btn">40</td>
                    <td class="green-btn">50</td>
                </tr>
                </tbody>
                </table>
                </div>
                <div class="form-group">
                    <div class="col-sm-8">
                    <button type="button" id="riskAssessmentBtn" class="btn btn-success">Add Score</button>
                    </div>
                </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->

<!-- SHEQF Notes Modal -->
  <div class="modal fade" id="sheqfNotes-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">SHEQF Notes</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="sheqfNotesForm" action="<?php echo BASE_URL.'/index.php?action=post_sheqf_notes_material&module=material&source_type=Material&material_id='.$id;?>">
                <input type="hidden" name="source_type" value="Material"/>
                <input type="hidden" name="asset_id" value="<?php if(isset($_POST['asset_id']))
                {
                    echo $_POST["asset_id"];
                }
                else
                {
                    echo $_GET["material_id"];
                }?>"/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="group" class="form-control" required>
                    <option value selected disabled>Please select a group</option>
                        <?php
                        foreach($data['allSheqTeams'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['sheqteam_name']}</option>";
                        }?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="topic">Topic</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="topic" name="topic" required placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notes">Notes</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea rows="5" class="form-control" id="notes" name="notes" required placeholder=""></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="sheqfNotesBtn" name="sheqfNotesBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Minimum PPE Modal -->
  <div class="modal fade" id="minimumPpe-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Minimum PPE</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="minimumPpeForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="category">Category</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a category">
                        <option>Category 1</option>
                        <option>Category 2</option>
                        <option>Category 3</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more ppe types" data-header="Close">
                        <option>Type 1</option>
                        <option>Type 2</option>
                        <option>Type 3</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="minimumPpeBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Permit Requirements Modal -->
  <div class="modal fade" id="permitReqiurements-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Permit Requirements</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="permitReqiurementsForm" action="">
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="group" class="form-control">
                    <option value selected disabled>Please select a group</option>
                    <option value="1">OHS (Health & Safety)</option>
                    <option value="2">E (Environment)</option>
                    <option value="3">Q (Quality)</option>
                    <option value="4">F (Food)</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="documentDescription">Document Description</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="documentDescription" required placeholder="">
                </div>
            </div>   
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a document">
                        <option>Document 1</option>
                        <option>Document 2</option>
                        <option>Document 3</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="permitReqiurementsBtn" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<script>    
 $(document).ready(function()
 {
    $("#controlSuggestedDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    $("#sheqfDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
        
    $('input[name="controlInEffect"]').click(function(){
            if($(this).attr("value")==="0"){
                $(".hidden-div").not(".control").hide();
                $(".control").show();
            }else{
                $(".control").hide();
            }
    });
    $('input[name="controlInEffect"]').click(function(){
            if($(this).attr("value")==="1"){
                $(".hidden-div2").not(".controlYes").hide();
                $(".controlYes").show();
            }else{
                $(".controlYes").hide();
            }
    });

    $("#riskAssessmentBtn").click(function(){
        $("#risk-assessment-score-modal").modal('toggle');
    });

    $('.green-btn, .yellow-btn, .red-btn, .majordanger-btn').click( function() {
      $(this).parents('table').find('td').each( function( index, element ) {
          $(element).removeClass('on');
      } );
      $(this).addClass('on');
    } );
    
    $( ".number td:not(.sideTitle)" ).click(function() {
    //alert($(this).text());
    $("#pscore").val($(this).text());
    });
    
    $("#riskAssessmentBtn").on("click",function()
    {
       var reduced_by = parseInt($("#controlYesReduction").val()); 
     //  alert(reduced_by);
       calculate(reduced_by);
      /* if(reduced_by != null or reduced_by != 0)
       {
          
       }*/
    });
    function calculate(result)
    {
          var reduction = parseInt(result);
          var score = parseInt($("#pscore").val());
          var new_score = (reduction * score / 100);
          $("#controlYesReduction").val(reduction + "%");
          $("#controlYesScore").val((score - new_score));
    }
        $(".get_control_reduction").on("change",function()
        {
            var id = this.value;
            var url = "<?php echo BASE_URL;?>/index.php?action=ajax_get_control_reduction&module=assets";
            $.ajax({
               type:'GET',
               url:url,
               data:{control_id:id},
               success:function(result)
               {
                    calculate(result);
               }
            });
            return false;
        });
    });
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    