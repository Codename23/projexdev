<?php
$title = 'Material Supplier';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
    <!--sub menu-->
    <div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="index.php?module=material&action=view">All Materials</a></li>
        <li><a href="index.php?module=material&action=nonComformance">Non-Conformance</a></li>
        <li class="active"><a>Suppliers</a></li>
        <li><a>Settings</a></li>
        <li><a>Archive</a></li>
    </ul>
    </div>
    </div> 
    <!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-body">
        <button type="submit" onclick="window.location.href='index.php?module=supplier&action=addSupplier'" id="addSupplier" class="btn btn-success">Add Supplier</button>
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading">Suppliers</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Company Name</th>
                    <th>Operational Area</th>
                    <th>Approval Status</th>
                    <th>Supply Of</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(!empty($data['allSuppliers'])){
                    foreach($data['allSuppliers'] as $supplierDetsArr){
                    echo ' <tr>
                                <td>' . $supplierDetsArr['id']. '</td>
                                <td>' . $supplierDetsArr['supplier_name']. '</td>
                                <td>Electrical Motors</td>
                                <td>Electro PTY</td>
                                <td>' . $supplierDetsArr['supplier_town']. '</td>
                                <td>Not Approved</td>
                                <td><a href="">View</a></td>
                                <td><a href="">View Details</a></td>
                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                            </tr>';
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
  
</div>
</div><!--End of container-fluid-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php');  
    