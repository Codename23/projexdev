<?php
$title = 'Non Comformance Material';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="index.php?module=material&action=view">All Materials</a></li>
        <li class="active"><a>Non-Conformance</a></li>
        <li><a href="index.php?module=material&action=supplier">Suppliers</a></li>
        <li><a>Settings</a></li>
        <li><a>Archive</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-heading">Non Conformances</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Department</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>Reported Person</th>
                    <th>Non-Conformance Type</th>
                    <th>Description of Event</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="">View</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->

</div>
</div><!--End of container-fluid-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php');
    