<?php 
/*
 * Header file
 */
$title = 'Material';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getAllMaterials&module=material">All Materials</a></li>
        <li><a href="material_supply_usage.php">Supply/Usage</a></li>
        <li><a href="material_nonconformance.php">Non-Conformances</a></li>
        <li><a href="material_substance.php">Substance</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllSettings&module=material">Settings</a></li>
        <li><a href="">Archive</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

<div class="panel panel-default">
<div class="panel-body">
    <a href="#addMaterial-modal" data-toggle="modal" data-target="#addMaterial-modal"><button type="button" id="addMaterial" class="btn btn-success">Add Material</button></a>
    <a href="#-modal" data-toggle="modal" data-target="#-modal"><button type="button" id="" class="btn btn-success">Load Non-Conformance</button></a>
</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Materials</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Departments</th>
                    <th>Inventory No</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
  <?php
               $counter = 0;
               global $objDepartment;              
               if($data['AllMaterials']): foreach($data['AllMaterials'] as $material):
                   $counter++;
                   ?>
                            <tr>
                                <td><?php echo $material['category']; ?></td>
                                <td><?php echo $material['type']; ?></td>                                
                                <td><?php echo $material['description']; ?></td>
                                <td><?php echo $objDepartment->getAllDepartmentsByGroupId($material["department_use_id"]);?></td>  
                                <td><?php echo $material['inventory_no']; ?></td>                                                                
                                 <td><a href="<?php echo BASE_URL."/index.php?material_id={$material["id"]}&source_type=Material&action=material_view_sop&module=material";?>">SOP</a></td>
                                 <td><a href="<?php echo BASE_URL."/index.php?material_id={$material["id"]}&source_type=Material&action=material_view_supply_usage&module=material";?>">Usage</a></td>
                                 <td><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $material['id'];?>&action=view_material_info&module=material">View Details</a></td>
                                 <td></td>
                            </tr>
                        <?php endforeach;endif; ?>
            </tbody>
        </table>    
    </div>
    </div>
    </div>

<!-- Add Material Modal -->
  <div class="modal fade" id="addMaterial-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Material</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addMaterialForm" action="<?php echo BASE_URL;?>/index.php?action=post_material&module=material">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Add Material Details</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="category">Category</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <select class="selectpicker form-multiselect" name="categories" required data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a category">
                                    <?php
                                      foreach($data["allMaterialCategories"] as $categories)
                                     {
                                          echo  "<option value='{$categories["id"]}'>{$categories['category']}</option>";
                                     }?>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="type">Type</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <select class="selectpicker form-multiselect" name="types" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a type">
                                  <?php
                                      foreach($data["allMaterialTypes"] as $types)
                                     {
                                          echo  "<option value='{$types["id"]}'>{$types['type']}</option>";
                                     }?>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="description">Description</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                            <input type="text" class="form-control"  name="description" id="description" required placeholder=""> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="inventoryNo">Inventory No</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                            <input type="text" class="form-control" name="inventoryNo" id="inventoryNo" required placeholder=""> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="departmentsToBeUsed">Departments To Be Used</label>
                            <input type="hidden" name="departments_selected" id="departments_selected"/>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <select class="selectpicker form-multiselect" id="departmentsToBeUsed" name="departmentsToBeUsed" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments" data-header="Close">
                                <?php
                                   foreach($data['allDepartments'] as $group)
                                   {
                                       echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                                   }
                                 ?>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="manufacturer">Manufacturer</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="manufacturer" id="manufacturer" value=""> 
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Total Usage Per Month</a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="totalMeasurementPerMonth">Total Measurement</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="totalMeasurementPerMonth" id="totalMeasurementPerMonth"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="unitsPerMonth">Units Per Month</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                                    <option>Litre</option>
                                    <option>Kilogram</option>
                                    <option>Gram</option>
                                </select> 
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Minimum Stock Level</a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="totalMeasurementStockLevel">Total Measurement</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="totalMeasurementStockLevel" id="totalMeasurementStockLevel"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="units">Units</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                                    <option>Litre</option>
                                    <option>Kilogram</option>
                                    <option>Gram</option>
                                </select> 
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Supplier</a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="supplier">Supplier</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more suppliers">
                                    <option>Supplier 1</option>
                                    <option>Supplier 2</option>
                                    <option>Supplier 3</option>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="orderToDelivery">From order to delivery - Days</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a number">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="cost">Cost Per Units Including VAT</label>
                            <div class="col-lg-6 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="cost" id="cost"> 
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8">
                <button type="submit" id="addMaterialBtn" name="addMaterialBtn" class="btn btn-success">Add</button>
                </div>
            </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
</div>
</div><!--End of container-fluid-->
<script>
    $(document).ready(function()
    {
          $('.selectpicker').on('change', function(){              
        var selected = $(this," option:selected").val();
        focus_dropdown = $(this).parent().children('select').attr('id');
        switch(focus_dropdown)
        {
            case 'departmentsToBeUsed':
                $("#departments_selected").val(selected);
                break;
            default:break;
        }
        }); 
    });
</script>
<?php include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    