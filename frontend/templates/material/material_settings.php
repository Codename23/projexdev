<?php 
/*
 * Header file
 */
$title = "Material Settings";
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->

<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getAllMaterials&module=material">All Materials</a></li>
        <li><a href="material_supply_usage.php">Supply/Usage</a></li>
        <li><a href="material_nonconformance.php">Non-Conformances</a></li>
        <li><a href="material_substance.php">Substance</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllSettings&module=material">Settings</a></li>
        <li><a href="">Archive</a></li>
    </ul>
    </div>
</div>  
    
    <div class="row">
    <div class="col-lg-4">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=getAllSettings&module=material">Category</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllSettingsTypes&module=material">Types</a></li>
    </ul>
    </div>
    </div> 
    <!--End of sub menu-->

    <div class="panel panel-default">
        <div class="panel-body">
         <a href="#addCategory-modal" data-toggle="modal" data-target="#addCategory-modal"><button type="button" id="addCategory" class="btn btn-success">Add Category</button></a>
        </div>
    </div> 
    <!--End of panel panel-default-->

    <div class="panel panel-default">
    <div class="panel-heading">Categories</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover" id="resourceTbl">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Date Added</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                 <?php if($data['allMaterialCategories']): foreach($data['allMaterialCategories'] as $categories): ?>
                            <tr>
                                <td><?php echo $categories['category']; ?></td>
                                <td><?php echo $categories['date_created']; ?></td>                                
                                 <td><a href="#"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                            </tr>
                        <?php endforeach;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
    
<!--Add New Modal -->
  <div class="modal fade" id="addCategory-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Category</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addCategoryForm" action="<?php echo BASE_URL;?>/index.php?action=post_material_category&module=material">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="category">Category</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="category" id="category" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" name="addCategoryBtn" id="addCategoryBtn" class="btn btn-success">Add</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->
    
</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    