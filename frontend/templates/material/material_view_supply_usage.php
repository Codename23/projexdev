<?php 
/*
 * Header file
 */
$title = "Material View Supply Usage";
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<?php
  global $objMaterial;
  
?>
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllMaterials&module=material">All Materials</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&action=view_material_info&module=material">Info</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&action=material_view_supply_usage&module=material">Supply / Usage</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=material_view_docs&module=material">Documents</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=material_view_substances&module=material">Substance</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=material_view_sop&module=material">SOP</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-body">
        <a href="#addTotalUsage-modal" data-toggle="modal" data-target="#addTotalUsage-modal"><button type="button" id="addTotalUsage" class="btn btn-success">Add Total Usage</button></a>
    </div>
    </div>
    
    <div class="panel panel-default">
    <div class="panel-heading">Supply / Usage</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Total Usage Per Month</th>
                    <th>Unit</th>
                    <th>Minimum Stock Level</th>
                    <th>Units</th>
                    <th>Current Supplier</th>
                    <th>Supplier Details</th>
                    <th>Days to Deliver</th>
                    <th>Cost Per Unit</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $counter = 0;       
                    global $objSupplier;
                    if($data['AllMaterialUsage']): foreach($data['AllMaterialUsage'] as $supply):
                        $counter++;
                        ?>
                     <tr>
                         <td><?php echo $supply['usage_total_measurement']; ?></td>
                         <td><?php echo $supply['usage_units_pm']; ?></td>                                
                         <td><?php echo $supply['minimum_total_measurement']; ?></td>
                         <td><?php echo $supply['minimum_units']; ?></td>
                         <td><?php echo $objSupplier->getAllSupplierByGroupId($supply['suppliers']); ?></td>
                         <td><a href="#<?php echo $supply['suppliers']; ?>">View details</a></td>
                         <td><?php echo $supply['supplier_delivery_days']; ?></td>
                         <td><?php echo $supply['supplier_cost_per_unit_inc_vat']; ?></td>                                                                                          
                         <td></td>
                     </tr>
                 <?php endforeach;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
    
    <form class="form-horizontal" method="post" name="processUsageForm" action="">
<div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Process Usage</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-8">
                        <a href="#processManagement-modal" data-toggle="modal" data-target="#processManagement-modal"><button type="button" id="processManagement" class="btn btn-success">Go To Process Management</button></a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Department</th>
                                    <th>Product</th>
                                    <th>Process</th>
                                    <th>Step</th>
                                    <th>Usage</th>
                                    <th>Unit</th>
                                    <th>Per</th>
                                    <th>Minimum Stock Level</th>
                                    <th>Units</th>
                                    <th>Current Supplier</th>
                                    <th>Order to Delivery Days</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>                                 
                                <tr>
                                    <td>Receiving</td>
                                    <td>Wood Shutter</td>
                                    <td>Receiving</td>
                                    <td>1</td>
                                    <td>2000</td>
                                    <td>L</td>
                                    <td>Month</td>
                                    <td>1000</td>
                                    <td>L</td>
                                    <td>Engen</td>
                                    <td>2</td>
                                    <td class="yellow-table-bg"><a href="">View Process</a></td>
                                    <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                                </tr>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    <!--End of panel-default-->
    
<!-- Add Total Usage Modal -->
  <div class="modal fade" id="addTotalUsage-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Usage</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addTotalUsageForm" action="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET["material_id"];?>&action=post_material_view_usage&module=material">
                <input type="hidden" name="material_id" id="material_id" value="<?php echo $_GET["material_id"];?>"/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="totalMeasurementPerMonth">Total Measurement</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="totalMeasurementPerMonth" id="totalMeasurementPerMonth" required> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="unitsPerMonth">Units Per Month</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="unitsPerMonth" id="unitsPerMonth" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select> 
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Minimum Stock Level</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="totalMeasurementStockLevel">Total Measurement</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="totalMeasurementStockLevel" id="totalMeasurementStockLevel" required> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="units">Units</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="minimum_units" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a unit">
                        <option>Litre</option>
                        <option>Kilogram</option>
                        <option>Gram</option>
                    </select> 
                </div>
            </div><br/>

            <div class="modal-custom-h5"><span><h5>Supplier</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="supplier">Supplier</label>
                <input type="hidden" name="suppliers_selected" id="suppliers_selected"/>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="suppliers" id="suppliers"  multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more suppliers">
                       <?php
                            foreach($data['allSupplier'] as $group)
                            {
                                echo "<option value='{$group["id"]}'>{$group['supplier_name']}</option>";
                            }?>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="orderToDelivery">From order to delivery - Days</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="delivery_days" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a number">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="cost">Cost Per Units Including VAT</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="cost" id="cost" required> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addTotalUsageBtn" name="addTotalUsageBtn" class="btn btn-success">Add</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
  
</div>
</div><!--End of container-fluid-->
<script>
    $(document).ready(function()
    {
          $('.selectpicker').on('change', function(){              
        var selected = $(this," option:selected").val();
        focus_dropdown = $(this).parent().children('select').attr('id');
        switch(focus_dropdown)
        {
            case 'suppliers':
                $("#suppliers_selected").val(selected);
                break;
            default:break;
        }
        }); 
    });
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    