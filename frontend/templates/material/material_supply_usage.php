<?php 
/*
 * Header file
 */
$title = "Material Supply Usage";
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<?php
  global $objMaterial;
  
?>
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllMaterials&module=material">All Materials</a></li>
        <li ><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&action=view_material_info&module=material">Info</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&action=material_view_supply_usage&module=material">Supply / Usage</a></li>
        <li><a href="material_view_documents.php">Documents</a></li>
        <li><a href="material_view_substance.php">Substance</a></li>
        <li><a href="material_view_sop.php">SOP</a></li>
    </ul>
    </div>
</div>  
    <!--End of sub menu-->
    
    <div class="panel panel-default">
    <div class="panel-heading">Search Supply/Usage</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>I.T</option>
                <option>Human Resources</option>
                <option>Marketing</option>
                <option>Support and Services</option>
            </select>
        </div>
        </div>

        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Category</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more groups"data-header="Close">
                <option value="1">Raw Materials</option>
                <option value="2">Consumables</option>
            </select>
        </div>
        </div>

        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Type</b></p>
            <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types" data-header="Close">
                <option value="1">Wood</option>
                <option value="2">Substance</option>
            </select> 
        </div>
        </div>
        </div>
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>
    
    <!--<div class="panel panel-default">
    <div class="panel-body">
        <button type="submit" onclick="window.location.href='supplier_add.php'" id="addSupplier" class="btn btn-success">Add Supplier</button>
    </div>
    </div>-->
    
    <div class="panel panel-default">
    <div class="panel-heading">Supply / Usage</div>
    <div class="panel-body">
    <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Departments</th>
                    <th>Inventory No</th>
                    <th>Total Usage</th>
                    <th>Units</th>
                    <th>Per</th>
                    <th>Supplier</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Consumable</td>
                    <td>Substance</td>
                    <td>Diesel</td>
                    <td><a href="">View</a></td>
                    <td>12</td>
                    <td>2000</td>
                    <td>L</td>
                    <td>Month</td>
                    <td>Engine</td>
                    <td><a href="">View Supplier</a></td>
                    <td><a href="">View Details</a></td>
                    <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                </tr>
                <tr>
                    <td>Consumable</td>
                    <td>Substance</td>
                    <td>Petrol</td>
                    <td><a href="">View</a></td>
                    <td>13</td>
                    <td>4000</td>
                    <td>L</td>
                    <td>Month</td>
                    <td>BP</td>
                    <td><a href="">View Supplier</a></td>
                    <td><a href="">View Details</a></td>
                    <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    </div>
    <!--End of the panel panel-default-->
  
</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */

?>  
    