<?php 
/*
 * Header file
 */
$title = 'Material View Info';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
<?php
  global $objMaterial;
  
?>
<div class="row">
    <div class="col-lg-12">
    <ul class="nav nav-pills nav-justified topbar-menu">
        <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllMaterials&module=material">All Materials</a></li>
        <li class="active"><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&action=view_material_info&module=material">Info</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&action=material_view_supply_usage&module=material">Supply / Usage</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=material_view_docs&module=material">Documents</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=material_view_substances&module=material">Substance</a></li>
        <li><a href="<?php echo BASE_URL;?>/index.php?material_id=<?php echo $_GET['material_id'];?>&source_type=Material&action=material_view_sop&module=material">SOP</a></li>
    </ul>
    </div>
</div>  
<!--End of sub menu-->

<div class="panel panel-default">
    <div class="panel-heading">Info</div>
    <div class="panel-body">
        
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th colspan="2">Material Details</th>
                </tr>
            </thead>
            <tbody>
                 <?php
               global $objDepartment;              
               if($data['AllMaterialInfo']): foreach($data['AllMaterialInfo'] as $material):
                   ?>                     
                <tr>
                    <td width="30%">Category</td>
                    <td><?php echo $material['category'];?></td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td><?php echo $material['type'];?></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td><?php echo $material['description'];?></td>
                </tr>
                <tr>
                    <td>Inventory No</td>
                    <td><?php echo $material['inventory_no'];?></td>
                </tr>
                <tr>
                    <td>Manufacturer</td>
                    <td><?php echo $material['manufacture_name'];?></td>
                </tr>
                <tr>
                    <td>Department to be used</td>
                    <td><?php echo $objDepartment->getAllDepartmentsByGroupId($material["department_use_id"]);?></td>
                </tr>
               <?php endforeach;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!--<form class="form-horizontal" method="post" name="infoDetailsForm" action="material_view_info.php">
<div class="panel panel-default">
    <div class="panel-heading">Info Details</div>
    <div class="panel-body"> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="category">Category</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" >
                        <option>Raw Materials</option>
                        <option>Consumables</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="type" id="type" required value="Wood"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="description">Description</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="description" id="description" value="Yellow Wood"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="departmentsToBeUsed">Depatments To Be Used</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
                        <option>I.T</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="storingDepartment">Storing Department</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
                        <option>I.T</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Usage</div>
    <div class="panel-body"> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="cost">Cost</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="cost" id="cost" value="R50"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="perUnit">Per Unit</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="perUnit" id="perUnit" value="R150"> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="usagePerDay">Usage Per Day</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="usagePerDay" id="usagePerDay" value="R250"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="units">Units</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="units" id="units" value="R350"> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="minimumStockLevel">Minimum Stock Level</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="minimumStockLevel" id="minimumStockLevel" value="R450"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="stockUnits">Units</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="stockUnits" id="stockUnits" value="R550"> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="viewInfoBtn" class="btn btn-success">Update</button>
                </div>
            </div>
    </div>
    </div>
</form>-->


</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    