<?php
$title = 'Employee';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */
    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <?php
    /*
     * Include side menu from the include file
     */
    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->
    <div class="col-lg-10">
        <!--sub menu-->
        <!--
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-justified topbar-menu">
                    <li class="active"><a href="employees.php">Employee Info</a></li>
                    <li><a href="employees_sheqteam.php">SHEQ Team</a></li>
                    <li><a href="employees_emergency.php">Emergency</a></li>
                    <li><a href="employees_occupation.php">Occupation</a></li>
                    <li><a href="employees_training_competence.php">Training & Competence</a></li>
                    <li><a href="employees_health.php">Health</a></li>
                    <li><a href="employees_ppe.php">PPE</a></li>
                    <li><a href="employees_performance.php">Performance</a></li>
                </ul>
            </div>
        </div>
        -->
        <div class="panel panel-default">
            <div class="panel-body">
                <form class="form-inline" method="post" action="add_employees.php">
                    <div class="form-group">
                        <a href="<?php echo BASE_URL . "/index.php?module=employee&action=addEmployee"?>" class="btn btn-success">Add Employee</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Search Employee</div>
            <div class="panel-body">
                <form class="form-inline" method="post" name="search" action="">
                    <div class="form-group">
                        <label for="branch">Branch </label>
                        <select class="form-control" name="branch">
                            <option>Cape Town Branch</option>
                            <option>Airport City Branch</option>
                            <option>Durbanville Branch</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="department">Department </label>
                        <select class="form-control" name="department">
                            <option>Cape Town Branch</option>
                            <option>Airport City Branch</option>
                            <option>Durbanville Branch</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Name </label>
                        <input type="text" class="form-control" id="name" placeholder="Search name">
                    </div>
                    <div class="form-group">
                        <label for="surname">Surname </label>
                        <input type="text" class="form-control" id="surname" placeholder="Search surname">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Search">
                    </div>
                </form>
            </div>
        </div
            <!--End of sub menu-->
        <div class="panel panel-default">
            <div class="panel-heading">Employees</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>SHEQ Team</th>
                            <th>Number</th>
                            <th>Branch</th>
                            <th>Department</th>
                            <th>Full name</th>
                            <th>Surname</th>
                            <th>Occupation</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($data['allEmployees']): foreach($data['allEmployees'] as $employeeInfo): ?>
                        <tr>
                            <td>1</td>
                            <td>Yes</td>
                            <td><?php echo $employeeInfo['field_data']; ?></td>
                            <td><?php echo $employeeInfo['branch_name']; ?></td>
                            <td><?php echo $employeeInfo['department_name']; ?></td>
                            <td><a href="<?php echo BASE_URL . "/index.php?module=employee&action=viewEmployeeInfo&id=".$employeeInfo['users_id']; ?>"><?php echo $employeeInfo['firstname']; ?></a></td>
                            <td><?php echo $employeeInfo['lastname']; ?></td>
                            <td><?php echo $employeeInfo['occupation']; ?></td>
                            <td><a href="<?php echo BASE_URL . "/index.php?module=employee&action=viewEmployeeInfo&id=".$employeeInfo['users_id']; ?>">Edit</a></td>
                            <td class="danger"><a href="<?php echo BASE_URL . "/index.php?module=employee&action=deleteEmployee&id=".$employeeInfo['users_id']; ?>">Delete</a></td>
                        </tr>
                        <?php endforeach;endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
<?php
/*
 *
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>