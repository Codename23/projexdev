<?php
$title = 'Company Information';
global $objLanguage;
include_once('frontend/templates/headers/default_header_tpl.php');
//$objLanguage->addLanguageFromFile();
?>
<div class="container-fluid">
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */

    include_once('frontend/templates/menus/main-menu.php');
    ?>
    <!--End of navigation-->

    <?php
    /*
     * Include side menu from the include file
     */

    include_once('frontend/templates/menus/side-menu.php');
    ?>

    <div class="col-lg-10">
        <?php
        /*
         * Include sub menu from the include file
         */

        ?>
        <div class="row company">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">

                        <h2><span class="glyphicon glyphicon-user" aria-hidden="true"></span><?php echo $data['employeeName'];  ?></h2>
                    </div>
                </div>

                <!--BEGIN TABS-->
                <div class="tabbable tabbable-custom tabbable-full-width">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#employee_details" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('EMPLOYEE_DETAILS') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#sheq_team" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('SHEQ_TEAM') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#emergency" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('EMERGENCY') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#occupation" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('OCCUPATION') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#training_competence" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('TRAINING_COMPETENCE') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#health" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('HEALTH') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#ppe" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('PPE') ;?>
                            </a>
                        </li>
                        <li>
                            <a href="#performance" data-toggle="tab">
                                <?php  echo $objLanguage->languageText('PERFORMANCE') ;?>
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="employee_details">
                            <div class="panel-group">
                                <form enctype="multipart/form-data" class="form-horizontal"  method="post" action="<?php echo BASE_URL; ?>/index.php?module=employee&action=editEmployee">

                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $objLanguage->languageText('PERSONAL_DETAILS'); ?></div>

                                        <div class="panel-body">

                                            <?php foreach ($data['editEmployeeInfo'] as $employeeInfo): ?>

                                                <?php if($employeeInfo['field_name'] == 'branch_name' ){?>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="branchName"><?php  echo $objLanguage->languageText(strtoupper($employeeInfo['field_name'])); ?></label>
                                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                                            <select class="form-control" name="<?php echo $employeeInfo['field_name']; ?>" id="branchName">
                                                                <?php if($data['companyBranches']) :foreach ($data['companyBranches'] as $branch): ?>
                                                                    <?php if($employeeInfo['field_data'] == $branch['id']) :?>
                                                                    <option value="<?php echo $branch['id']; ?>" selected><?php echo $branch['branch_name']; ?></option>
                                                                     <?php else: ?>
                                                                    <option value="<?php echo $branch['id']; ?>"><?php echo $branch['branch_name']; ?></option>
                                                                    <?php endif ?>
                                                                <?php endforeach; endif; ?>
                                                            </select>
                                                        </div>
                                                    </div>


                                                <?php }elseif($employeeInfo['field_name'] == 'department_name' ){?>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="departmentName"><?php  echo $objLanguage->languageText(strtoupper($employeeInfo['field_name'])); ?></label>
                                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                                            <select class="form-control" name="<?php echo $employeeInfo['field_name']; ?>" id="departmentName">
                                                                <option value="">None</option>

                                                                <?php if($data['branchDepartments']) :foreach ($data['branchDepartments'] as $departments): ?>
                                                                    <?php if($employeeInfo['field_data'] == $departments['id']) :?>
                                                                    <option value="<?php echo $departments['id']; ?>" selected><?php echo $departments['department_name']; ?></option>
                                                                    <?php else: ?>
                                                                    <option value="<?php echo $departments['id']; ?>"><?php echo $departments['department_name']; ?></option>
                                                                    <?php endif ?>
                                                                <?php endforeach; endif;?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                <?php }elseif($employeeInfo['field_name'] == 'occupation' ){?>
                                                    <div class="form-group occupation_options">
                                                        <label class="control-label col-sm-4" for="occupationName"><?php  echo $objLanguage->languageText(strtoupper($employeeInfo['field_name'])); ?></label>
                                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                                            <select class="form-control" name="<?php echo $employeeInfo['field_name']; ?>" id="occupationName">

                                                                <?php if($data['departmentOccupations']) :foreach ($data['departmentOccupations'] as $occupations): ?>

                                                                    <?php if($employeeInfo['field_data'] == $occupations['id']) :?>
                                                                        <option value="<?php echo $occupations['id']; ?>" selected><?php echo $occupations['name']; ?></option>
                                                                    <?php else: ?>
                                                                        <option value="<?php echo $occupations['id']; ?>"><?php echo $occupations['name']; ?></option>
                                                                    <?php endif ?>

                                                            <?php endforeach; endif;?>
                                                            </select>
                                                        </div>
                                                    </div>


                                                <?php }elseif($employeeInfo['field_name'] == 'employee_blood_type' ){?>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="bloodtype"><?php  echo $objLanguage->languageText(strtoupper($employeeInfo['field_name'])); ?></label>
                                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                                            <select class="form-control" name="<?php echo $employeeInfo['field_name']; ?>" id="<?php echo $employeeInfo['field_name']; ?>">
                                                                <option value="1">O</option>
                                                                <option value="2">A</option>
                                                                <option value="3">B</option>
                                                                <option value="4">AB</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                <?php }elseif($employeeInfo['field_name'] == 'employee_allergies' ){?>

                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="allergies"><?php echo $objLanguage->languageText(strtoupper($employeeInfo['field_name'])); ?></label>
                                                        <div class="col-lg-6 col-md-6 col-sm-8">
                                                            <textarea class="form-control" name="<?php echo $employeeInfo['field_name']; ?>" id="<?php echo $employeeInfo['field_name']; ?>"><?php echo $employeeInfo['field_data']; ?></textarea>
                                                        </div>
                                                    </div>

                                                <?php }elseif($employeeInfo['field_name'] == 'employee_race'){?>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="<?php echo $employeeInfo['field_name']; ?>"><?php  echo $objLanguage->languageText(strtoupper($employeeInfo['field_name'])); ?></label>
                                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                                            <select class="form-control" name="<?php echo $employeeInfo['field_name']; ?>" id="<?php echo $employeeInfo['field_name']; ?>">

                                                                <?php if($data['ethnic']): foreach ($data['ethnic'] as $ethnic):?>
                                                                    <?php if($employeeInfo['field_data'] == $ethnic['id']) :?>
                                                                        <option value="<?php echo $ethnic['id']; ?>" selected ><?php echo $ethnic['name']; ?></option>
                                                                    <?php else:?>
                                                                        <option value="<?php echo $ethnic['id']; ?>" ><?php echo $ethnic['name']; ?></option>
                                                                    <?php endif;?>
                                                                <?php endforeach;endif;?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                <?php }elseif($employeeInfo['field_name'] == 'employee_gender'){?>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="<?php echo $employeeInfo['field_name']; ?>"><?php  echo $objLanguage->languageText(strtoupper($employeeInfo['field_name'])); ?></label>
                                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                                            <select class="form-control" name="<?php echo $employeeInfo['field_name']; ?>" id="<?php echo $employeeInfo['field_name']; ?>">
                                                                <?php if($data['gender']): foreach ($data['gender'] as $gender):?>
                                                                    <?php if($employeeInfo['field_data'] == $gender['id']) :?>
                                                                    <option value="<?php echo $gender['id']; ?>" selected ><?php echo $gender['name']; ?></option>
                                                                     <?php else:?>
                                                                    <option value="<?php echo $gender['id']; ?>" ><?php echo $gender['name']; ?></option>
                                                                    <?php endif;?>
                                                                <?php endforeach;endif;?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                <?php }elseif($employeeInfo['field_name'] == 'employee_marital_status'){?>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="maritalStatus"><?php  echo $objLanguage->languageText(strtoupper($employeeInfo['field_name'])); ?></label>
                                                        <div class="col-lg-6 col-md-6 col-sm-8">
                                                            <label class="radio-inline"><input type="radio" name="<?php echo $employeeInfo['field_name']; ?>" <?php echo ($employeeInfo['field_data'] == 1) ? 'checked' : ''; ?> value="1" >Married</label>
                                                            <label class="radio-inline"><input type="radio" name="<?php echo $employeeInfo['field_name']; ?>" <?php echo ($employeeInfo['field_data'] == 0) ? 'checked' : ''; ?> value="0">Single</label>
                                                        </div>
                                                    </div>
                                                <?php }elseif($employeeInfo['field_name'] == 'employee_language'){?>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="homeLanguage"><?php  echo $objLanguage->languageText(strtoupper($employeeInfo['field_name'])); ?></label>
                                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                                            <select class="form-control bfh-languages" name="<?php echo $employeeInfo['field_name']; ?>" data-language="en"></select>
                                                        </div>
                                                    </div>

                                                <?php }elseif($employeeInfo['field_name'] == 'second_language'){?>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="secondLanguage"><?php  echo $objLanguage->languageText(strtoupper($employeeInfo['field_name'])); ?></label>
                                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                                            <select class="form-control bfh-languages" name="<?php echo $employeeInfo['field_name']; ?>" data-language="en"></select>
                                                        </div>
                                                    </div>

                                                <?php }else{?>

                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="<?php echo $employeeInfo['field_name']; ?>"><?php  echo $objLanguage->languageText(strtoupper($employeeInfo['field_name'])); ?></label>
                                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                                            <input type="text" class="form-control" name="<?php echo $employeeInfo['field_name']; ?>" id="<?php echo $employeeInfo['field_name']; ?>" required placeholder="Enter <?php echo $objLanguage->languageText(strtolower($employeeInfo['field_name'])); ?>" value="<?php echo $employeeInfo['field_data']; ?>">
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <input type="hidden" name="id" value="<?php echo $employeeInfo['user_id']; ?>">
                                            <?php endforeach; ?>
                                            
                                            
                                            
                                            

                                            <div class="form-group">
                                                <label class="control-label col-sm-4" for="profileimage">Upload photo</label>
                                                <div class="col-lg-4 col-md-4 col-sm-8">
                                                    <input type="file" id="profileimage" name="profileimage">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-offset-4 col-sm-8">
                                                    <input type="hidden" name="employeeId" value="<?php echo $data['editEmployeeInfo'][0]['user_id']; ?>" />
                                                    <button type="submit" class="btn btn-success">Save</button>
                                                    <button type="reset" class="btn btn-default">Reset</button>
                                                </div>
                                            </div>
                                        </div><!--End of panel-body-->

                                    </div><!--End of panel panel-default-->
                                    </div><!--End of panel panel-default-->
                                </form>
                            </div><!--End of panel-group -->

                            <!--End of the personal detail form-->
                            <div class="panel panel-default">
                                <div class="panel-heading">Appointments</div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Appointment</th>
                                                <th>Status</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Fire Equipment</td>
                                                <td>Active</td>
                                                <td><a href="#">Edit</a></td>
                                                <td><a href="#">Delete</a></td>
                                            </tr>
                                            <tr>
                                                <td>Fire Equipment</td>
                                                <td>Inactive</td>
                                                <td><a href="#">Edit</a></td>
                                                <td><a href="#">Delete</a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End of the appointment deatil-->
                            <div class="panel panel-default">
                                <div class="panel-heading">Calendar</div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Upcoming Inspection</th>
                                                <th>Date</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Fire Equipment</td>
                                                <td>30 Sept 2016</td>
                                                <td><a href="#">Edit</a></td>
                                                <td><a href="#">Delete</a></td>
                                            </tr>
                                            <tr>
                                                <td>Fire Equipment</td>
                                                <td>30 Sept 2016</td>
                                                <td><a href="#">Edit</a></td>
                                                <td><a href="#">Delete</a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End of the panel-group-->


                        <!--tab_1_2-->
                        <div class="tab-pane" id="sheq_team">
                            <div class="sheqTeamResults"></div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane" id="emergency">
                            <div class="emergencyResults"></div>
                        </div>
                        <!--end tab-pane-->
                    </div><!--End of row -->
                </div><!--End of container-fluid -->
            </div>

        </div>
    </div>
    <!--END TABS-->
</div>

<!-- END PAGE CONTENT-->


<?php

include_once('frontend/templates/footers/default_footer_tpl.php');
?>

<script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {


        var target = $(e.target).attr("href");

        if ((target == '#sheq_team')) {
            $.post('<?php echo BASE_URL . "" . $data['editEmployeeInfo'][0]['user_id']; ?>', {}, function (data) {
                $('.sheqTeamResults').html(data);
            });
        }

        if ((target == '#emergency')) {
            $.post('<?php echo BASE_URL . "" . $data['editEmployeeInfo'][0]['user_id']; ?>', {}, function (data) {
                $('.emergencyResults').html(data);
            });
        }
    });
</script>