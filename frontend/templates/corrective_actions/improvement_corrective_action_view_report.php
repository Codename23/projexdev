<?php 
$title = 'Corrective Action View Report';
include_once('frontend/templates/headers/default_header_tpl.php');
?>

<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of col-lg-2 side-bar -->
<div class="col-lg-10">
<div class="panel-group">
    
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Corrective Action</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">No</td>
                                    <td>021</td>
                                </tr>
                                <tr>
                                    <td>Department</td>
                                    <td>I.T</td>
                                </tr>
                                <tr>
                                    <td>Priority</td>
                                    <td>High</td>
                                </tr>
                                <tr>
                                    <td>System Element</td>
                                    <td>Training</td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Non-Conformance</td>
                                    <td>This is a non-conformance</td>
                                </tr>
                                <tr>
                                    <td>Description of Corrective Action</td>
                                    <td>This is a description</td>
                                </tr>
                                <tr>
                                    <td>Responsible Person</td>
                                    <td>Sunny Mathole</td>
                                </tr>
                                <tr>
                                    <td>Due Date</td>
                                    <td>2017/10/16</td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Recommended Improvement</td>
                                    <td>This is an improvement</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Corrective Action Additional Information</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Permit</td>
                                    <td>Doc</td>
                                </tr>
                                <tr>
                                    <td>Procedure Link</td>
                                    <td>Doc1</td>
                                </tr>
                                <tr>
                                    <td>Form Link</td>
                                    <td>Doc1</td>
                                </tr>
                                <tr>
                                    <td>Method Statement Link</td>
                                    <td>Doc1</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Involvement</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Resources involved</td>
                                    <td>Conveyor Belt</td>
                                </tr>
                                <tr>
                                    <td>Employee Involved</td>
                                    <td>Brian Douglas</td>
                                </tr>
                                <tr>
                                    <td>Supplier Involved</td>
                                    <td>Supplier 1</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Images</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Photo</td>
                                    <td><img width="250px" class="img-responsive" src="<?php echo HOSTNAME;?>/images/uncovered-wires.jpg" alt="non-conformance image"></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Approval - Managerial</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Due Date</td>
                                    <td>2017/10/16</td>
                                </tr>
                                <tr>
                                    <td>Motivation</td>
                                    <td>This is a motivation</td>
                                </tr>
                                <tr>
                                    <td>Est Cost</td>
                                    <td>R500</td>
                                </tr>
                                <tr>
                                    <td>Document</td>
                                    <td>Doc 1</td>
                                </tr>
                                <tr>
                                    <td>Approved By</td>
                                    <td>
                                        <p><b>Name :</b> Emmanuel van der Westhuizen</p>
                                        <p><b>Approval Condition :</b> This is an approval condition</p>
                                        <p><b>Documents :</b> Doc 1</p>
                                        <p><b>Date Approved :</b> 2017/12/10</p>
                                        <p><b>Procedure :</b> Procedure Link</p>
                                        <p><b>Form :</b> Form Link</p>
                                        <p><b>Method :</b> Method Link</p>
                                        <p><b>Document 1 :</b> Doc</p>
                                        <p><b>Document 2 :</b> Doc</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Approval - Safety</a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Due Date</td>
                                    <td>2017/10/16</td>
                                </tr>
                                <tr>
                                    <td>Motivation</td>
                                    <td>This is a motivation</td>
                                </tr>
                                <tr>
                                    <td>Est Cost</td>
                                    <td>R500</td>
                                </tr>
                                <tr>
                                    <td>Document</td>
                                    <td>Doc 1</td>
                                </tr>
                                <tr>
                                    <td>Approved By</td>
                                    <td>
                                        <p><b>Name :</b> Emmanuel van der Westhuizen</p>
                                        <p><b>Approval Condition :</b> This is an approval condition</p>
                                        <p><b>Documents :</b> Doc 1</p>
                                        <p><b>Date Approved :</b> 2017/12/10</p>
                                        <p><b>Procedure :</b> Procedure Link</p>
                                        <p><b>Form :</b> Form Link</p>
                                        <p><b>Method :</b> Method Link</p>
                                        <p><b>Document 1 :</b> Doc</p>
                                        <p><b>Document 2 :</b> Doc</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Sign Off - Responsible Person</a>
                </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Responsible Person</td>
                                    <td>Sunny Mathole</td>
                                </tr>
                                <tr>
                                    <td>Sign Off Date</td>
                                    <td>2018/09/10</td>
                                </tr>
                                <tr>
                                    <td>Action Taken</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Cost of Corrective Action</td>
                                    <td>R500</td>
                                </tr>
                                <tr>
                                    <td>Documents</td>
                                    <td>Doc 1</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">Master Sign Off</a>
                </h4>
            </div>
            <div id="collapseEight" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Master Sign Off</td>
                                    <td>Nick Botha</td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td>2018/09/20</td>
                                </tr>
                                <tr>
                                    <td>Action Taken</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of panel-default-->
</div><!--end of panel group--> 

</div>
</div><!--end of container-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>    