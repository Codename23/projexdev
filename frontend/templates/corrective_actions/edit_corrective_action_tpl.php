<?php 
$title = 'Add Corrective Action';
include_once('frontend/templates/headers/default_header_tpl.php');
?>

<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of col-lg-2 side-bar -->
<div class="col-lg-10">
      <?php
                    $hasNonConformance = false;
                    $details = "";
                    $recommended_improvement = "";
                    $person_involved = 0;
                    $department_id = 0;
                    $due_date = "";
                    $nc_resources_involved = 0;
                     if(!empty($data["details"]))
                     {
                            foreach($data["details"] as $info)
                            {
                               $hasNonConformance = true;
                               $details = $info["non_conformance_details"];
                               $person_involved = $info["nc_person_involved"];
                               $department_id = $info["department_field"];
                               $recommended_improvement = $info["recommended_improvement_field"];
                               $due_date = $info["due_date"];
                               $nc_resources_involved = $info["nc_resources_involved"];
                               break;
                            }      
                       }
                    ?>
<div class="panel-group">
    <form class="form-horizontal" method="post" enctype="multipart/form-data" name="addCorrctiveActionForm" action="<?php 
    $url = "/index.php?action=addCorrectiveActionPost&module=corrective_action";
    /*if($hasNonConformance)
    {
        $url .= "&ncid=".(int)$_GET["ncid"];
        if(isset($_GET['why_id']))
        {
            $url .="&why_id=".(int)$_GET["why_id"];
        }
    }*/
    echo BASE_URL.$url;?>">
<div class="panel panel-default">  
<div class="panel-heading">Add Corrective Action</div>
<?php echo $_SERVER["HTTP_REFERER"];?>
<input type="hidden" name="ncid" value="<?php
if(isset($_GET["ncid"]))
{
    echo $_GET["ncid"];
}
?>"/>
<input type="hidden" name="why_id" value="<?php
if(isset($_GET["why_id"]))
{
    echo $_GET["why_id"];
}
?>"/>
<div class="panel-body">
            <div class="form-group">
                <label class="control-label col-sm-4">Department</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="cor_department" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                            <?php
                            if($department_id != 0)
                            {                                
                                foreach($data['allDepartments'] as $group)
                                {
                                    if((int)$group["id"] == $department_id)
                                    {
                                        echo "<option selected value='{$group["id"]}'>{$group['department_name']}</option>";
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                foreach($data['allDepartments'] as $group)
                                {
                                    echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                                }
                            }?>
                            
                    </select>  
                </div>    
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="priority">Priority</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="form-control" id="priority" name="priority">
                    <option value="1">Low</option>
                    <option value="2">Medium</option>
                    <option value="3">High</option>
                    <option value="4">Immediate</option>
                </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="systemElement">System Element</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <select class="form-control" name="systemElement" id="systemElement">
                    <option value="1">Training</option>
                    <option value="2">Inspection</option>
                    <option value="3">Audit</option>
                    <option value="4">Health</option>
                    <option value="5">Contractor / Supplier / Client</option>
                    <option value="6">Committee Meeting</option>
                    <option value="7">Risk Management</option>
                    <option value="8">Incident Management</option>
                    <option value="9">Resource</option>
                    <option value="10">HR</option>
                </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="nonConformanceDescription">Non-Conformance Description</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
               
                <textarea class="form-control" <?php
                if($hasNonConformance)
                {
                    echo "Readonly=true";
                }
                ?> rows="5" id="nonConformanceDescription" name="nonConformanceDescription"><?php                
                echo $details;
                ?></textarea>
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="correctiveActionDescription">Description of correction action</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <textarea class="form-control" rows="5" id="correctiveActionDescription" name="correctiveActionDescription"></textarea>
                </div>
            </div>
            <div class="form-group">
                <span class="control-label col-sm-4"></span>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="cor_person_responsible" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Responsible Person">
                       <?php
                       if($person_involved != 0)
                       {
                           //get person involved
                            foreach($data['allPersons'] as $group)
                            {
                                if((int)$group["uid"] == $person_involved)
                                {
                                   echo "<option selected value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                                   break;
                                }
                            }
                       }
                       else
                       {
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }
                       }
                       ?>
                    </select>  
                </div>    
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="recommendedImprovements">Recommended Improvements</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <textarea class="form-control"
                <?php 
                if($hasNonConformance)
                {
                    echo "Readonly=true";
                }?> rows="5" id="recommendedImprovements" name="recommendedImprovements"><?php
                echo $recommended_improvement;
                ?></textarea>
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notify_corrective_person" for="dep_manager">Notify Department Manager</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="dep_manager" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more managers">
                        <?php
                        foreach($data['allPersons'] as $group)
                        {
                            echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                        }                       
                       ?>
                    </select> 
                </div>
            </div>
                <div class="form-group">
                <label class="control-label col-sm-4" for="notify_corrective_person">Notification Person</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="notify_corrective_person" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="List of appointment types">
                        <?php
                        foreach($data['allPersons'] as $group)
                        {
                            echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                        }                       
                       ?>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="masterSignoff">Master Sign Off</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="master_sign_off" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="List of appointment types">
                        <?php
                        foreach($data['allPersons'] as $group)
                        {
                            echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                        }                       
                       ?>
                    </select> 
                </div>
            </div>
             
            <div class="form-group">
                <label class="control-label col-sm-4" for="correctiveActionDueDate">Due Date & Time</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="correctiveActionDueDate" id="correctiveActionDueDate" required
                       value="<?php
                       if(isset($due_date))
                       {
                           echo $due_date;
                       }
                       ?>">
                </div>
            </div>
            <!--<div class="form-group">
                <label class="control-label col-sm-4">Responsible Person</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                        <select class="form-control" name="correctiveActionPerson" id="correctiveActionPerson">
                        <option value="" selected disabled>Please select a name</option>
                        <option value="1">Warren Windovogel</option>
                        <option value="2">Sunnyboy Mathole</option>
                        <option value="3">Nick Botha</option>
                        <option value="4">Nasir Jones</option>
                     </select>  
                </div>    
            </div>
               
            <div class="form-group">
                <span class="col-xs-4"></span>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <div class="after-add-more">
                    <select class="form-control" name="correctiveActionDepartment" id="correctiveActionDepartment" required>
                        <option value="" selected disabled>Please select a department</option>
                        <option value="1">1 IT</option>
                        <option value="2">2 Finance</option>
                        <option value="3">3 Human Resource</option>
                        <option value="4">4 Support and Services</option>
                        <option value="5">Other</option>
                    </select> 
                    </div>
                    <br/>
                    <div> 
                        <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Add Person</button>
                    </div>
                </div>    
            </div>
               <br/>
             
            <div class="copy hide">
            <div class="control-group" style="margin-top:30px">
              <div class="form-group">
                <div class="col-sm-12">
                    <select class="form-control" name="correctiveActionPerson" id="correctiveActionPerson">
                        <option value="" selected disabled>Please select a name</option>
                        <option value="1">Warren Windovogel</option>
                        <option value="2">Sunnyboy Mathole</option>
                        <option value="3">Nick Botha</option>
                        <option value="4">Nasir Jones</option>
                    </select>
                </div>
              </div>
                   
              <div class="form-group">
                <div class="col-sm-12">
                    <select class="form-control" name="correctiveActionDepartment" id="correctiveActionDepartment" required>
                        <option value="" selected disabled>Please select a department</option>
                        <option value="1">1 IT</option>
                        <option value="2">2 Finance</option>
                        <option value="3">3 Human Resource</option>
                        <option value="4">4 Support and Services</option>
                        <option value="5">Other</option>
                    </select> 
                </div>
              </div>
              
            <div> 
              <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
            </div>
          </div>    
        </div>-->

               <!--<div class="form-group">
                <label class="control-label col-sm-4">Notify Manager</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                        <select class="form-control" name="correctiveActionManager" id="correctiveActionManager">
                        <option value="" selected disabled>Please select a name</option>
                        <option value="1">Warren Windovogel</option>
                        <option value="2">Sunnyboy Mathole</option>
                        <option value="3">Nick Botha</option>
                        <option value="4">Nasir Jones</option>
                     </select>  
                </div>    
            </div>
               
            <div class="form-group">
                <span class="col-xs-4"></span>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <div class="after-add-more2">
                    <select class="form-control" name="correctiveActionManagerDepartment" id="correctiveActionManagerDepartment" required>
                        <option value="" selected disabled>Please select a department</option>
                        <option value="1">1 IT</option>
                        <option value="2">2 Finance</option>
                        <option value="3">3 Human Resource</option>
                        <option value="4">4 Support and Services</option>
                        <option value="5">Other</option>
                    </select> 
                    </div>
                    <br/>
                    <div> 
                        <button class="btn btn-success add-more2" type="button"><i class="glyphicon glyphicon-plus"></i> Add Person</button>
                    </div>
                </div>    
            </div>
               <br/>
             
            <div class="copy2 hide">
            <div class="control-group" style="margin-top:30px">
              <div class="form-group">
                <div class="col-sm-12">
                    <select class="form-control" name="correctiveActionManager" id="correctiveActionManager">
                        <option value="" selected disabled>Please select a name</option>
                        <option value="1">Warren Windovogel</option>
                        <option value="2">Sunnyboy Mathole</option>
                        <option value="3">Nick Botha</option>
                        <option value="4">Nasir Jones</option>
                    </select>
                </div>
              </div>
                   
              <div class="form-group">
                <div class="col-sm-12">
                    <select class="form-control" name="correctiveActionManagerDepartment" id="correctiveActionManagerDepartment" required>
                        <option value="" selected disabled>Please select a department</option>
                        <option value="1">1 IT</option>
                        <option value="2">2 Finance</option>
                        <option value="3">3 Human Resource</option>
                        <option value="4">4 Support and Services</option>
                        <option value="5">Other</option>
                    </select> 
                </div>
              </div>
              
            <div> 
              <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
            </div>
          </div>    
        </div>  -->

        </div>
    </div>
    
    <!--End of panel panel-default-->
    <div class="panel panel-default">
   <div class="panel-body">
       <button type="button" id="addAdditionalInfo" class="btn btn-success">Add Additional Information</button>
   </div>
   </div>
   <!--End of the panel panel-default-->             
   <div class="panel panel-default" id="additionalInfoForm">
   <div class="panel-heading">Addtional Information</div>
   <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-4" for="correctiveActionPhotos">Photos</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="file" id="correctiveActionPhotos" name="correctiveActionPhotos">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="correctiveActionPermit">Permit</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="file" id="correctiveActionPermit" name="correctiveActionPermit">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="correctiveActionProcedureLink">Procedure Link</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" name="correctiveActionProcedureLink">
                <option value="" selected disabled>Please select a procedure link</option>
                <option>Link 1</option>
                <option>Link 2</option>
                <option>Link 3</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="correctiveActionFormLink">Form Link</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" name="correctiveActionFormLink">
                <option value="" selected disabled>Please select a form link</option>
                <option>Link 1</option>
                <option>Link 2</option>
                <option>Link 3</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="correctiveActionMethodLink">Method Statement Link</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="form-control" name="correctiveActionMethodLink">
                <option value="" selected disabled>Please select a method link</option>
                <option>Link 1</option>
                <option>Link 2</option>
                <option>Link 3</option>
            </select>
            </div>
        </div>
   </div>
   </div>  
   <!--End of panel panel-default-->
   
   <div class="panel panel-default">
   <div class="panel-body">
       <button type="button" id="resources" class="btn btn-success">Resources</button>
   </div>
   </div>
   <!--End of the panel panel-default--> 
   
   <div class="panel panel-default" id="resourcesForm">
   <div class="panel-heading">Resources</div>
   <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-4" for="correctiveActionResource">Resource</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="selectpicker form-multiselect" name="cor_resource" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more resource"data-header="Close">
                 <?php
                            if($nc_resources_involved != 0)
                            {      
                               
                                
                                foreach($data['allResources'] as $group)
                                {           
                                    if((int)$group["assid"] == $nc_resources_involved)
                                    {
                                        echo "<option selected value='{$group["assid"]}'>{$group['ts_name']}</option>";
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                foreach($data['allResources'] as $group)
                                {
                                    echo "<option value='{$group["assid"]}'>{$group['ts_name']}</option>";
                                }
                                
                            }?>
            </select> 
             </div>
         </div>
   </div>
   </div> 
   
   <div class="panel panel-default">
   <div class="panel-heading">Approval</div>
   <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-4" for="needApproval">Need Approval</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
                <label class="radio-inline"><input type="radio" name="needApproval"  required id="needApprovalYes" value="1">Yes</label>
                <label class="radio-inline"><input type="radio" name="needApproval" required id="needApprovalNo" value="0" >No</label>
            </div>
        </div>
       <div class="approvalYes hidden-div">
            <div class="form-group">
                <label class="control-label col-sm-4" for="committee">Committee</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="form-control" name="committee" id="committee">
                        <option value selected disabled>Please select a committee</option>
                        <option>Health</option>
                        <option>Safety</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
            <label class="control-label col-sm-4" for="branchLevel">Branch Level</label>
            <div class="col-lg-4 col-md-4 col-sm-8">
            <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
               <?php
                        foreach($data['allPersons'] as $group)
                        {
                            echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                        }                       
                       ?>
            </select> 
            </div>
            </div>
            <div class="form-group">
             <label class="control-label col-sm-4" for="groupLevel">Group Level</label>
             <div class="col-lg-4 col-md-4 col-sm-8">
             <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                    <?php
                        foreach($data['allPersons'] as $group)
                        {
                            echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                        }                       
                       ?>
             </select> 
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="motivation">Motivation</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <textarea class="form-control" rows="5" id="motivation" name="motivation"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="estimateCost">Estimate Cost</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" id="estimateCost" name="estimateCost" value="R" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="longevityCorrectiveAction">Longevity of Corrective Action</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" id="longevityCorrectiveAction" name="longevityCorrectiveAction" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="uploadDocs">Upload Documents</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="file" id="uploadDocs" name="uploadDocs">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
            <button type="submit" name="btnAddCorrectiveAction" class="btn btn-success">Update Corrective Action</button>
            </div>
        </div>
   </div>
   </div>  
   <!--End of panel panel-default-->
    
</form>  
</div>
</div>
</div>
<script>
$(function() {
 $("#correctiveActionDueDate").datetimepicker({
        dateFormat:"yy-mm-dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt',
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datetimepicker();
    
$('input[name="needApproval"]').click(function(){
        if($(this).attr("value")==="1"){
            $(".hidden-div").not(".approvalYes").hide();
            $(".approvalYes").show();
        }else{
            $(".approvalYes").hide();
        }
});
});

$("#additionalInfoForm").hide();
$("#addAdditionalInfo").click(function(){
    $("#additionalInfoForm").toggle('slow');
});

$("#resourcesForm").hide();
$("#resources").click(function(){
    $("#resourcesForm").toggle('slow');
});
</script>

<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  