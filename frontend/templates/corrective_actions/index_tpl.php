<?php 
$title = 'Add Corrective Action';
include_once('frontend/templates/headers/default_header_tpl.php');
?>

<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of col-lg-2 side-bar -->
    <div class="col-lg-10">
        <!--sub menu-->
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-justified topbar-menu">
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Non-Conformance</a></li>
                    <li><a href="improvement_incident.php">Incidents</a></li>
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Investigations</a></li>
                    <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action">Corrective Actions</a></li>
                    <li><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Improvement</a></li>
                    <li><a href="improvement_objective.php">Objective</a></li>
                </ul>
            </div>
        </div>         
        <div class="row">
            <div class="col-lg-4">
            <ul class="nav nav-pills nav-justified topbar-menu">
                <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action">All</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=showCorrectiveActionHistory&module=corrective_action">History</a></li>
            </ul>
            </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-body">
        <form class="form-inline" method="post" action="<?php echo BASE_URL;?>/index.php?action=addCorrectiveAction&module=corrective_action">
            <div class="form-group">
                <button type="submit" id="addCorrctiveAction" class="btn btn-success">Add Corrective Action</button>
            </div>
        </form>
        </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-heading">Search Corrective Actions</div>
        <div class="panel-body">
        <form class="form-inline" method="post" name="search" action="">
            <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Type</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more types"data-header="Close">
                    <option>Admin</option>
                    <option>Employee</option>
                    <option>Suppliers</option>
                    <option>Resource/Consumables/Maintenance</option>
                    <option>Document</option>
                    <option>SHEQ Teams</option>
                    <option>Committees</option>
                    <option>Training/Communication</option>
                    <option>Environmental</option>
                    <option>Behaviour</option>
                    <option>Inspections</option>
                    <option>Risk Assessment</option>
                    <option>Incident</option>
                    <option>Audit</option>
                </select>
            </div>
            </div>

            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Select Department</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                    <option>I.T</option>
                    <option>Human Resources</option>
                    <option>Marketing</option>
                    <option>Support and Services</option>
                </select>
            </div>
            </div>

            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Select Priority</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more priorities"data-header="Close">
                    <option>Low</option>
                    <option>Medium</option>
                    <option>High</option>
                </select>
            </div>
            </div>

            <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <p><b>Responsible Person</b></p>
                <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more person"data-header="Close">
                    <option>Nick</option>
                    <option>Sunny</option>
                    <option>Douglas</option>
                    <option>Warren</option>
                </select>
            </div>
            </div>
            </div>
            <div style="margin-left: 30px;">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Search">
            </div>
            </div>
        </form>
        </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-heading">Corrective Actions</div>
        <div class="panel-body">
        <div class="table-responsive">   
        
        <table width="100%" class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Department</th>
              <th>Type</th>
              <th>Priority</th>
              <th>Description</th>
              <th>Responsible Person</th>
              <th>Target Date</th>
              <th>Status</th>
              <th>Approved</th>
              <th></th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          <?php 
          $priority = array("Low","Medium","High","Immediate");
          if($data['allCorrectiveActions']): foreach($data['allCorrectiveActions'] as $ca): ?>
            <tr>
              <td><?php echo $ca['id'];?></td>
              <td><?php echo $ca['dep'];?></td>
              <td></td>
              <td><?php echo $ca['priority'];?></td>
              <td><?php echo $ca['description'];?></td>
              <td><?php echo $ca['responsible'];?></td>
              <td><?php echo $ca['ca_target_date'];?></td>
              <td></td>
              <td><?php 
              switch($ca['approval_required'])
              {
                  case 1:
                      //check the approval table if the corrective actions has been approved
                      break;
                  case 0:
                      //echo "not Required";
                      break;
              }?>
              </td>
              <td><a href="<?php echo BASE_URL;?>/index.php?caid=<?php echo $ca['id'];?>&action=viewCorrectiveActionDetails&module=corrective_action">View</a></td>
              <td><a href="<?php echo BASE_URL;?>/index.php?caid=<?php echo $ca['id'];?>&action=correctiveActionSignOff&module=corrective_action">Sign Off</a></td>
              <td><a href="<?php echo BASE_URL;?>/index.php?caid=<?php echo $ca['id'];?>&action=editCorrectiveAction&module=corrective_action"><span class="glyphicon glyphicon-pencil"></span></a></td>
            </tr>
             <?php endforeach;endif; ?>
          </tbody>
        </table>

        </div>
        </div>
    </div>
    <!--End of the pane panel-default-->
    </div>  
</div><!--End of container-fluid-->
</div>
<script>
$(function() {
 /*
  * $("#correctiveActionDueDate").datetimepicker({ 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt',
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datetimepicker();
  * 
  */   
$("#correctiveActionDueDate").datetimepicker({ 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt',
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datetimepicker();

$('input[name="needApproval"]').click(function(){
        if($(this).attr("value")==="0"){
            $(".hidden-div").not(".approvalYes").hide();
            $(".approvalYes").show();
        }else{
            $(".approvalYes").hide();
        }
});
});

$("#additionalInfoForm").hide();
$("#addAdditionalInfo").click(function(){
    $("#additionalInfoForm").toggle('slow');
});

$("#resourcesForm").hide();
$("#resources").click(function(){
    $("#resourcesForm").toggle('slow');
});
</script>

<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  