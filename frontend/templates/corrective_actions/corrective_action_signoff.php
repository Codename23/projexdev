<?php 
$title = 'Corrective Action Sign Off';
include_once('frontend/templates/headers/default_header_tpl.php');
?>

<div class="container-fluid"> 
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
$id = $_GET['caid'];
?> 
<!--End of col-lg-2 side-bar -->
<div class="col-lg-10">
<div class="panel-group">
    
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Corrective Action</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">No</td>
                                    <td>021</td>
                                </tr>
                                <tr>
                                    <td>Department</td>
                                    <td>I.T</td>
                                </tr>
                                <tr>
                                    <td>Priority</td>
                                    <td>High</td>
                                </tr>
                                <tr>
                                    <td>System Element</td>
                                    <td>Training</td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Non-Conformance</td>
                                    <td>This is a non-conformance</td>
                                </tr>
                                <tr>
                                    <td>Description of Corrective Action</td>
                                    <td>This is a description</td>
                                </tr>
                                <tr>
                                    <td>Responsible Person</td>
                                    <td>Sunny Mathole</td>
                                </tr>
                                <tr>
                                    <td>Due Date</td>
                                    <td>2017/10/16</td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Recommended Improvement</td>
                                    <td>This is an improvement</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Corrective Action Additional Information</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Permit</td>
                                    <td>Doc</td>
                                </tr>
                                <tr>
                                    <td>Procedure Link</td>
                                    <td>Doc1</td>
                                </tr>
                                <tr>
                                    <td>Form Link</td>
                                    <td>Doc1</td>
                                </tr>
                                <tr>
                                    <td>Method Statement Link</td>
                                    <td>Doc1</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Involvement</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Resources involved</td>
                                    <td>Conveyor Belt</td>
                                </tr>
                                <tr>
                                    <td>Employee Involved</td>
                                    <td>Brian Douglas</td>
                                </tr>
                                <tr>
                                    <td>Supplier Involved</td>
                                    <td>Supplier 1</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Images</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Photo</td>
                                    <td><img width="250px" class="img-responsive" src="<?php echo HOSTNAME;?>/images/uncovered-wires.jpg" alt="non-conformance image"></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Approval - Managerial</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Due Date</td>
                                    <td>2017/10/16</td>
                                </tr>
                                <tr>
                                    <td>Motivation</td>
                                    <td>This is a motivation</td>
                                </tr>
                                <tr>
                                    <td>Est Cost</td>
                                    <td>R500</td>
                                </tr>
                                <tr>
                                    <td>Document</td>
                                    <td>Doc 1</td>
                                </tr>
                                <tr>
                                    <td>Approved By</td>
                                    <td>
                                        <p><b>Name :</b> Emmanuel van der Westhuizen</p>
                                        <p><b>Approval Condition :</b> This is an approval condition</p>
                                        <p><b>Documents :</b> Doc 1</p>
                                        <p><b>Date Approved :</b> 2017/12/10</p>
                                        <p><b>Procedure :</b> Procedure Link</p>
                                        <p><b>Form :</b> Form Link</p>
                                        <p><b>Method :</b> Method Link</p>
                                        <p><b>Document 1 :</b> Doc</p>
                                        <p><b>Document 2 :</b> Doc</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Approval - Safety</a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Due Date</td>
                                    <td>2017/10/16</td>
                                </tr>
                                <tr>
                                    <td>Motivation</td>
                                    <td>This is a motivation</td>
                                </tr>
                                <tr>
                                    <td>Est Cost</td>
                                    <td>R500</td>
                                </tr>
                                <tr>
                                    <td>Document</td>
                                    <td>Doc 1</td>
                                </tr>
                                <tr>
                                    <td>Approved By</td>
                                    <td>
                                        <p><b>Name :</b> Emmanuel van der Westhuizen</p>
                                        <p><b>Approval Condition :</b> This is an approval condition</p>
                                        <p><b>Documents :</b> Doc 1</p>
                                        <p><b>Date Approved :</b> 2017/12/10</p>
                                        <p><b>Procedure :</b> Procedure Link</p>
                                        <p><b>Form :</b> Form Link</p>
                                        <p><b>Method :</b> Method Link</p>
                                        <p><b>Document 1 :</b> Doc</p>
                                        <p><b>Document 2 :</b> Doc</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of panel-default-->
</div><!--end of panel group-->

<!--End of panel panel-default-->
    <div class="panel panel-default">
   <div class="panel-body">
   <form class="form-inline" method="post">
       <div class="form-group">
       <button type="button" id="signOff" class="btn btn-success">Sign Off - Responsible Person</button>
       </div>
   </form>
   </div>
   </div>

<div class="panel panel-default" id="signOffForm">
<div class="panel-heading">Person Who Did Corrective Action</div>
<div class="panel-body">
<form class="form-horizontal" name="signOffForm">
    <div class="form-group">
    <label class="control-label col-sm-4" for="signoffName">Signed Off By</label>
    <div class="col-lg-4 col-md-4 col-sm-8">
        <input type="text" class="form-control" name="signoffName" id="signoffName" value="Sunny (auto filled)"> 
    </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="signoffDate">Sign Off Date</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="text" class="form-control" name="signoffDate" id="signoffDate" required placeholder="">
        </div>
    </div>
    <!--<div class="form-group">
        <span class="col-xs-4"></span>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <select class="form-control" name="signoffDepartment" id="signoffDepartment" required>
            <option value="" selected disabled>Please select a department</option>
            <option value="1">1 IT</option>
            <option value="2">2 Finance</option>
            <option value="3">3 Human Resource</option>
            <option value="4">4 Support and Services</option>
            <option value="5">Other</option>
        </select> 
        </div>
    </div>-->
    <div class="form-group">
        <label class="control-label col-sm-4" for="signoffActionTaken">Action Taken</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <textarea class="form-control" rows="5" id="signoffActionTaken" name="signoffActionTaken"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="signoffLodgeAction">Lodge Corrective Action</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <label class="radio-inline"><input type="radio" name="signoffLodgeAction" id="signoffLodgeAction1" value="0">Yes</label>
        <label class="radio-inline"><input type="radio" name="signoffLodgeAction" id="signoffLodgeAction2" value="1">No</label>
        </div>
    </div>
    <div class="form-group">
    <label class="control-label col-sm-4" for="costOfCorrectiveAction">Cost of corrective action</label>
    <div class="col-lg-4 col-md-4 col-sm-8">
        <input type="text" class="form-control" name="costOfCorrectiveAction" id="signoffNcostOfCorrectiveActioname" value="R"> 
    </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="signoffUploadDoc">Upload Documents</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="file" name="signoffUploadDoc" id="signoffUploadDoc">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="signoffPwd">Password</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="password" class="form-control" name="signoffPwd" id="signoffPwd" required placeholder="Enter your password">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-8">
        <button type="submit" class="btn btn-success">Submit</button>
        </div>
    </div>
</form>
</div>
</div>  

<!--End of panel panel-default-->
   <div class="panel panel-default">
   <div class="panel-body">
   <form class="form-inline" method="post">
       <div class="form-group">
       <button type="button" id="masterSignOff" class="btn btn-success">Master Sign Off</button>
       </div>
   </form>
   </div>
   </div>
    
    <div class="panel panel-default" id="masterSignOffForm">
<div class="panel-heading">Master Sign Off</div>
<div class="panel-body">
<form class="form-horizontal" name="masterSignOffForm" method="post" action="<?php echo BASE_URL;?>/index.php?action=correctiveActionSignOffPost&module=corrective_action">
    <input type="hidden" name="caid" value="<?php echo $id;?>"/>

    <div class="form-group">
        <label class="control-label col-sm-4" for="masterSignoffActionTaken">Action Taken</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <textarea class="form-control" rows="5" id="masterSignoffActionTaken" name="masterSignoffActionTaken"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="masterSignoffPwd">Password</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="password" class="form-control" name="masterSignoffPwd" id="masterSignoffPwd" required placeholder="Enter your password">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-8">
        <button type="submit" name="btnMasterSignOff" class="btn btn-success">Submit</button>
        </div>
    </div>
</form>
</div>
</div> 

</div>

<!-- Lodge Corrective Action Modal -->
  <div class="modal fade" id="lodgeCorrectiveAction-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Lodge Corrective Action</h4>
        </div>
        <div class="modal-body">
            <?php include 'corrective_action_includes/signoff_lodge_corrective_modal_form.php'; ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div><!--end of container-->
<script>
$(function() {
$("#signoffDate").datepicker({
    dateFormat:"yy/mm/dd",
    maxDate: new Date(), 
    minDate: new Date()});
$("#signoffDate").datepicker("setDate", new Date());
$("#masterSignoffDate").datepicker({
    dateFormat:"yy/mm/dd",
    maxDate: new Date(), 
    minDate: new Date()});
$("#masterSignoffDate").datepicker("setDate", new Date());

$("#signOffForm").hide();
$("#signOff").click(function(){
    $("#signOffForm").toggle('slow');
});
$("#masterSignOffForm").hide();
$("#masterSignOff").click(function(){
    $("#masterSignOffForm").toggle('slow');
});

$('input[name="signoffLodgeAction"]').change(function() {
   if($(this).is(':checked') && $(this).val() === '0') {
        $('#lodgeCorrectiveAction-modal').modal('show');
   }
});
$('input[name="masterSignoffLodgeAction"]').change(function() {
   if($(this).is(':checked') && $(this).val() === '0') {
        $('#lodgeCorrectiveAction-modal').modal('show');
   }
});

$("#addMoreTaskTopic").hide();
$("#taskTopic").change(function(){
   var taskTopic = $("#taskTopic" ).val();
   if(taskTopic === 'add'){
       $("#addMoreTaskTopic").show();
   }else{
       $("#addMoreTaskTopic").hide();
   }
});

$(".add-more").click(function(){ 
    var html = $(".copy").html();
    $(".after-add-more").after(html);
});

$("body").on("click",".remove",function(){ 
    $(this).parents(".control-group").remove();
});
      
 $(".add-more2").click(function(){ 
    var html = $(".copy2").html();
    $(".after-add-more2").after(html);
});

$("correctiveActionManagerDepartment").on("click",".remove",function(){ 
    $(this).parents(".control-group").remove();
});  

$("#correctiveActionDueDate").datetimepicker({
        dateFormat:"yy/mm/dd", 
        controlType: 'select',
	oneLine: true, 
        timeFormat: 'hh:mm tt',
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datetimepicker();
});
</script>

<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  