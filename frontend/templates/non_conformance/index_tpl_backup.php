<?php 
$title = 'Improvement';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of navigation-->   
  <div class="col-lg-10">
        <!--sub menu-->
        <div class="row">
            <div class="col-lg-12">
            <ul class="nav nav-pills nav-justified topbar-menu">
               <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Non-Conformance</a></li>
                <li><a href="improvement_incident.php">Incidents</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Investigations</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action">Corrective Actions</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Improvement</a></li>
                <li><a href="improvement_objective.php">Objective</a></li>
            </ul>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6">
            <ul class="nav nav-pills nav-justified topbar-menu">
                <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Continues Improvement</a></li>
                <li><a href="improvement_nonconformance_inspections.php">Inspections</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllHistoryConformance&module=non_conformance">History</a></li>
            </ul>
            </div>
        </div>
                
        <div class="panel panel-default">
        <div class="panel-heading">Search Non-Conformance</div>
        <div class="panel-body">
        <form class="form-inline" method="post" name="search" action="<?php echo BASE_URL?>/index.php?action=searchNonConformance&module=non_conformance">
            <div class="col-md-12" style="margin-bottom: 10px;">              
            <div class="col-md-4 col-sm-4">
            <div class="form-group">
                <p><b>Group</b></p>
                <?php
               /* if(isset($data['results']))
                {
                    printf($data['results']);
                }*/
                ?>
                <input type="hidden" name="groups_selected" id="groups_selected"/>
                <select class="selectpicker" id="search_groups" multiple="multiple" data-live-search="true" name="search_groups" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more groups"data-header="Close">
                         <?php
                        foreach($data['allSheqTeams'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['sheqteam_name']}</option>";
                        }?>
                </select>
            </div>
            </div>

            <div class="col-md-4 col-sm-4">
            <div class="form-group">
                <p><b>Select Department</b></p>
                <input type="hidden" name="departments_selected" id="departments_selected"/>
                <select class="selectpicker" multiple data-live-search="true" name="search_departments"  id="search_departments" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                        <?php
                        foreach($data['allDepartments'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                        }?>
                </select>
            </div>
            </div>
                
            <div class="col-md-4 col-sm-4">
            <div class="form-group">
                <p><b>Status</b></p>
                <input type="hidden" name="status_selected" id="status_selected"/>
                <select class="selectpicker" data-live-search="true" name="search_status" id="search_status" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more status types" data-header="Close">
                    <option value="1">Due in a month</option>
                    <option value="2">Due in 2 months</option>
                    <option value="3">Overdue</option>
                    <option value="4">Good</option>
                </select> 
            </div>
            </div>
            </div>
            <div style="margin-left: 30px;">
            <div class="form-group">
                <input type="submit" class="btn btn-success" name="btnSearchConformance" value="Search">
            </div>
            </div>
        </form>
        </div>
        </div>
        
        <div class="panel panel-default">
        <div class="panel-body">
            <a href="#addNonConformance-modal"   data-toggle="modal" data-target="#addNonConformance-modal"><button type="button" id="addNonConformance" class="btn btn-success">Add Non-Conformance</button></a>
        </div>
        </div>
        <div class="panel panel-default">
        <div class="panel-heading">All Non-Conformances</div>
        <div class="panel-body">
        <div class="table-responsive">   
        
        <table width="100%" class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Date</th>
              <th>Group</th>
              <th>Department</th>
              <th>Due Date</th>
              <th>Responsible Person</th>
              <th>Non Conformance Details</th>              
              <th>Lodged By</th>
              <th>Status</th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
               <?php if($data['allNonConformance']): foreach($data['allNonConformance'] as $nc): ?>
                            <tr>
                                <td><?php echo $nc['nc_id']; ?></td>
                                <td><?php echo $nc['nc_date_created']; ?></td>                                
                                <td><?php echo $nc['nc_group_field']; ?></td>
                                <td><?php echo $nc['department_name']; ?></td>
                                <td><?php echo $nc['nc_due_date']; ?></td>
                                <td><?php echo $nc['responsible']; ?></td>
                                <td><?php echo $nc['non_conformance_details']; ?></td>                                
                                 <td><?php echo $nc['loggedBy']; ?></td>
                                <td></td>
                                <td><a href="<?php echo BASE_URL;?>/index.php?ncid=<?php echo $nc['nc_id'];?>&action=view_non_conformance_details&module=non_conformance">View Details</a></td>
                                <?php
                                if(!empty($nc['photo_name']))
                                {
                                ?>
                                <td><a href="#nonConformancePhoto-modal" data-toggle="modal" data-img="<?php echo BASE_URL.'/frontend/media/uploads/'.$nc['photo_name'];?>" class="photo_view" data-target="#nonConformancePhoto-modal">View Photo</a></td>
                                <?php
                                }
                                else
                                {
                                    ?>
                                    <td></td>
                                    <?php
                                }
                                ?>
                                <td><a href="#signOff-modal" data-toggle="modal" conid="<?php echo $nc['nc_id'];?>" class="non_conformance_signed_off" data-target="#signOff-modal">Sign Off</a></td> 
                                <td><a href="<?php echo BASE_URL;?>/index.php?ncid=<?php echo $nc['nc_id'];?>&course=noc&action=addCorrectiveAction&module=corrective_action">Corrective Action</a></td>
                                <?php
                                if((int)$nc["Investigate"] == 0)
                                {
                                ?>
                                <td><a href="#investigate-modal" conid="<?php echo $nc['nc_id'];?>" data-toggle="modal" data-target="#investigate-modal" class="investigate_non_conformance">Investigate</a></td>
                                <?php
                                }
                                if((int)$nc["Investigate"] != 0)
                                {
                                    ?>
                                    <td><a href="<?php echo BASE_URL;?>/index.php?ncid=<?php echo $nc['nc_id'];?>&action=viewSingleInvestigation&module=investigation">View Investigation</a></td>
                                    <?php
                                }
                                ?>
                            </tr>
                        <?php endforeach;endif; ?>
          </tbody>
        </table>

        </div>
        </div>
    </div>
    <!--End of the pane panel-default-->
    <!-- Add Non-Conformance Modal -->
  <div class="modal fade" id="addNonConformance-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Non-Conformance</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="addNonConformanceForm" name="addNonConformanceForm" action="<?php echo BASE_URL;?>/index.php?action=addNoConformance&module=non_conformance">
            <div class="form-group">
                <label class="control-label col-sm-4" for="date">Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" readonly="true" class="form-control" name="date" id="date"> 
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="group" class="form-control">
                    <option value="0">Please select a group</option>
                        <?php
                        foreach($data['allSheqTeams'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['sheqteam_name']}</option>";
                        }?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select name="type" id="type_field" class="form-control">
                        <option value="0">Please select a type</option>
                        <option value="1">Not Specified</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="department">Department</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" id="add_nc_department_dropdown" name="nc_department" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments">                        
                           <?php
                        foreach($data['allDepartments'] as $group)
                        {
                            echo "<option value='{$group["id"]}'>{$group['department_name']}</option>";
                        }?>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="dueDate">Due Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="dueDate" id="dueDate"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="responsible_person">Responsible Person</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="responsible_person" id="responsible_person" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                            <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>
                    </select> 
                </div>
            </div>    
            <div class="form-group">
                <label class="control-label col-sm-4" for="nonConformanceDetails">Non-Conformance Details</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="nonConformanceDetails" id="nonConformanceDetails"></textarea>
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="recommended_improvement_field">Recommended Improvement</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="recommended_improvement_field" id="recommended_improvement_field"></textarea>
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="resource_inolved">Resource Involved</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect addResource" name="resource_inolved" id="resource_inolved" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a resource">
                           <?php
                            foreach($data['allResources'] as $group)
                            {
                                echo "<option value='{$group["as_id"]}'>{$group['ts_name']}</option>";
                            }?>
                    </select> 
                </div>
            </div>
            <div class="resourceContainer">
                <span class="control-label col-sm-4"></span>            
            <div class="col-lg-6 col-md-4 col-sm-8">
                <p style="color:red;"><b>Resource not listed</b></p>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notListedResourceType">Not Listed</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedResourceType" id="notListedResourceType" placeholder="Type"> 
                </div>
            </div>
            <div class="form-group">
                <span class="col-sm-4" for="notListedResourceDescription"></span>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedResourceDescription" id="notListedResourceDescription" placeholder="Description"> 
                </div>
            </div></div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="person_involved">Employee involved</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="person_involved" id="person_involved" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select an employee">
                            <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>
                    </select> 
                </div>
            </div>
            <div class="personContainer"><span class="control-label col-sm-4"></span>
            <div class="col-lg-6 col-md-4 col-sm-8">
                <p style="color:red;"><b>Person not listed</b></p>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notListedPersonType">Not Listed</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedPersonType" id="notListedPersonType" placeholder="Type"> 
                </div>
            </div>
            <div class="form-group">
                <span class="col-sm-4" for="notListedPersonDescription"></span>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedPersonDescription" id="notListedPersonDescription" placeholder="Description"> 
                </div>
            </div></div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="supplier">Supplier</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" name="supplier_id" id="supplier_id" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a supplier">
                             <?php
                            foreach($data['allSupplier'] as $group)
                            {
                                echo "<option value='{$group["id"]}'>{$group['supplier_name']}</option>";
                            }?>
                    </select> 
                </div>
            </div>
            <div class="supplierContainer"><span class="control-label col-sm-4"></span>
            <div class="col-lg-6 col-md-4 col-sm-8">
                <p style="color:red;"><b>Supplier not listed</b></p>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notListedSupplierCompany">Not Listed</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedSupplierCompany" id="notListedSupplierCompany" placeholder="Company Name"> 
                </div>
            </div></div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="uploadPhoto">Take/Upload Photo</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="file" accept="image/*" name="uploadPhoto" id="uploadPhoto"> 
                <img id="non_add_image" style="width:200px;height:200px;display:none;"/>
                </div>
            </div><br/>
<div class="form-group">
                <label class="control-label col-sm-4" for="investigate">Do you want to send to investigate?</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <label class="radio-inline"><input type="radio" name="investigate" id="investigate1" value="0" required>Yes</label>
                    <label class="radio-inline"><input type="radio" name="investigate" id="investigate2" value="1" >No</label>
                </div>
            </div><br/>
            <div class="sendToInvestigate hidden-div">
            <div class="modal-custom-h5"><span><h5>Send To Investigate</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigator">Investigator</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" id="investigator" name="investigator" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a investigator">
                          <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigatorDueDate">Due Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="investigatorDueDate" id="investigatorDueDate"> 
                </div>
            </div>          
            </div>
              <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addNonConformanceBtn" name="addNonConformanceBtn" class="btn btn-success">Add Non-Conformance</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Sign Off List Modal -->
  <div class="modal fade" id="signOff-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sign Off</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="signOffForm" action="<?php echo BASE_URL;?>/index.php?action=signedOffNonConformance&module=non_conformance">  
                  <input type="hidden" name="non_signed_id" id="non_signed_id"/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="signoffDate">Date</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="signoffDate" id="signoffDate"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="signoffActionTaken">Action Taken</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="signoffActionTaken" id="signoffActionTaken"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="signoffPassword">Password</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <input type="password" class="form-control" name="signoffPassword" id="signoffPassword"> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="signOffBtn" class="btn btn-success">Sign Off</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
<!-- Investigate Modal -->
  <div class="modal fade" id="investigate-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Investigate</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="investigateForm" action="<?php echo BASE_URL;?>/index.php?action=addInvestigation&module=investigation">
                <input type="hidden" name="non_id" id="non_id"/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigationOptions">Choose Investigation</label>
                <div class="col-lg-6 col-md-4 col-sm-8">
                <select class="form-control" name="investigationOptions" id="investigationOptions" required>
                    <option value selected>Please select an investigation</option>
                    <option value="addInvestigation">Add to upcoming investigation</option>
                    <option value="doInvestigation">Do investigation</option>
                </select>
                </div>
            </div>
             <div class="form-group" id="addInvestigationForm">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="investigationDueDate">Due Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="investigationDueDate" id="investigationDueDate" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="investigationInvestigator">Investigator</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" name="investigationInvestigator" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select an investigator">
                           <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" id="addInvestigationBtn" name="addInvestigationBtn" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </div>
                
            <div class="form-group" id="doInvestigationForm">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="doInvestigationInvestigator">Investigator</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="selectpicker form-multiselect" name="doInvestigationInvestigator" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select an investigator">
                          <?php
                            foreach($data['allPersons'] as $group)
                            {
                                echo "<option value='{$group["uid"]}'>{$group['firstname']} {$group['lastname']}</option>";
                            }?>
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" id="doInvestigationBtn" name="doInvestigationBtn" class="btn btn-success">Start</button>
                    </div>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->
<!--Person Involved Modal -->
  <div class="modal fade" id="personInvolved-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Person Involved</h4>
        </div>
        <div class="modal-body table-responsive">
        <table width="100%" class="table table-hover person_table">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Department</th>
                <th>Occupation</th>
            </tr>
            </thead>
            <tbody>
            <tr>

            </tr>
            </tbody>
        </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->
<!--Resource Involved Modal -->
  <div class="modal fade" id="resourceInvolved-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Resource Involved</h4>
        </div>
        <div class="modal-body table-responsive">
        <table width="100%" class="table table-hover resource_table">
            <thead>
            <tr>
                <th>#</th>
                <th>Category</th>
                <th>Type</th>
                <th>Description Name</th>
                <th>Number</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                
            </tr>
            </tbody>
        </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->

 <!--End Of Modal -->
<!-- Non-Conformance Photo Modal -->
  <div class="modal fade" id="nonConformancePhoto-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Non-Conformance Photo</h4>
        </div>
       <div class="modal-body text-center">
            <img class="img-responsive" id="non_photo_image" alt="wires">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->

    </div>  
</div><!--End of container-fluid-->
<script>
    $(document).ready(function()
    {
    $("#date").datepicker({
        dateFormat:"yy/mm/dd",
        maxDate: new Date(), 
        minDate: new Date()});
    $("#date").datepicker("setDate", new Date());
    $("#signoffDate").datepicker({
        dateFormat:"yy/mm/dd",
        maxDate: new Date(), 
        minDate: new Date()});
    $("#signoffDate").datepicker("setDate", new Date());
    
    $("#dueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    $("#investigatorDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    $("#investigationDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();    
      $("#addInvestigationForm").hide();
    $("#investigationOptions").change(function(){
       var investigationOptions = $("#investigationOptions" ).val();
       if(investigationOptions === 'addInvestigation'){
           $("#addInvestigationForm").show();
       }else{
           $("#addInvestigationForm").hide();
       }
    });
    $("#doInvestigationForm").hide();
    $("#investigationOptions").change(function(){
       var investigationOptions = $("#investigationOptions" ).val();
       if(investigationOptions === 'doInvestigation'){
           $("#doInvestigationForm").show();
       }else{
           $("#doInvestigationForm").hide();
       }
    });
    $('input[name="investigate"]').click(function(){
        if($(this).attr("value")==="0"){
            $(".hidden-div").not(".sendToInvestigate").hide();
            $(".sendToInvestigate").show();
        }else{
            $(".sendToInvestigate").hide();
        }
    });
    $(".photo_view").on("click",function()
    {
        var img_dir = $(this).attr('data-img');
        $("#non_photo_image").attr('src',img_dir);
    });
    $(".view_persons_involved").click(function()
    {
        var id_ = $(this).attr('data-resource');
        $.ajax({
            url:'<?php echo BASE_URL . "/index.php?module=non_conformance&action=viewPersonInvolved";?>',
            type:'POST',
            data:{person_id:id_},
            dataType: 'json',
            success:function(result)
            {
                var rs = JSON.stringify(result);
                var newPerson = JSON.parse(rs);
                if(parseInt(newPerson.status) == 200)
                {
                    $(".person_table tbody tr").html("<td>"+newPerson.id+"</td><td>"+newPerson.name+"</td><td>"+newPerson.surname+"</td><td>"+newPerson.department+"</td><td>"+newPerson.occupation+"</td>");
                }
            }
        });        
    });
    $(".view_resource").click(function()
    {
        var id_ = $(this).attr('data-resource');
        $.ajax({
            url:'<?php echo BASE_URL . "/index.php?module=non_conformance&action=viewAssetsInvolved";?>',
            type:'POST',
            data:{ass_id:id_},
            dataType: 'json',
            success:function(result)
            {
                var rs = JSON.stringify(result);
                var newResource = JSON.parse(rs);
                if(parseInt(newResource.status) == 200)
                {
                    $(".resource_table tbody tr").html("<td>"+newResource.id+"</td><td>"+newResource.category+"</td><td>"+newResource.type+"</td><td>"+newResource.description+"</td><td>"+newResource.number+"</td>");
                }
            }
        });             
    });

    //toggleContainers('.resourceContainer','Hide');
   // toggleContainers('.personContainer','Hide');
   // toggleContainers('.supplierContainer','Hide');
    
    $("#uploadPhoto").on("change",function()
    {
        var file = this.files[0];        
        var image_type = /image.*/;
        var reader = new FileReader();
        var img = document.getElementById("non_add_image");        
        if(file.type.match(image_type))
        {
            $(reader).on("load",function(e)
            {          
               $("#non_add_image").css({display:'block'}); 
               img.src = reader.result;
            });
            reader.readAsDataURL(file);
        }
        else
        {
            alert('File type not supported!');
        }        
    });
    function toggleContainers(container,val)
    {
        switch(val)
        {
            case 'Hide':
                $(container).hide();
                break;
            case 'Show':
                $(container).show();
                break;
        }
    }
    $("#resource_inolved").on("change",function()
    {        
        id = $(this).val();
        if(id === null || id === "")
        {
          $('.resourceContainer').show();
        }
        else
        {
            $('.resourceContainer').hide();
        }
    });
    $("#supplier_id").on("change",function()
    {
         id = $(this).val();
        if(id === null || id === "")
        {
          $('.supplierContainer').show();
        }
        else
        {
            $('.supplierContainer').hide();
        }
    });
    $("#person_involved").on("change",function()
    {
        id = $(this).val();
        if(id === null || id === "")
        {
          $('.personContainer').show();
        }
        else
        {
            $('.personContainer').hide();
        }
         
    });
    
    
    //refine this section
   /* $(".bs-searchbox input[type=text]").on("keyup",function()
    {
        var text_box = $(this).parent().parent().parent().children('select').attr('id');
        var obj = $(this).parent().parent().parent().children('select');
        switch(text_box)
        {   
            case 'search_groups':
               ////
                break;
           case 'add_nc_department_dropdown':
                 //if returned result is empty display department fields and store data 
                 $(obj).append("<option>Department not listed</option>");
                break;                
            case 'search_status':
               ///
                break;
        }
        console.log(text_box);
    });*/


    $('.selectpicker').on('change', function(){
              
        var selected = $(this," option:selected").val();
        focus_dropdown = $(this).parent().children('select').attr('id');
        switch(focus_dropdown)
        {
            case 'search_groups':
                $("#groups_selected").val(selected);
                break;
           case 'search_departments':
               $("#departments_selected").val(selected);
                break;                
            case 'search_status':
                $("#status_selected").val(selected);
                break;
        }
       // console.log(focus_dropdown);
    });
    
    $(".investigate_non_conformance").click(function()
    {
        var id = $(this).attr('conid'); 
        $("#non_id").val(id);
    });    
    $(".non_conformance_signed_off").click(function()
    {        
        var id = $(this).attr('conid'); 
        $("#non_signed_id").val(id);
    });    
    //
    });
</script>
<?php 
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  