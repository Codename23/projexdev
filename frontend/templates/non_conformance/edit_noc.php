<?php 
$title = "Edit Non-conformance";
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of navigation--> 
<div class="col-lg-10">
    <div class="panel-group">
    <div class="panel panel-default">
    <div class="panel-heading">Non-Conformance</div>
    <div class="panel-body"> 
            <form class="form-horizontal" method="post" name="addNonConformanceForm" action="improvement.php">
            <div class="form-group">
                <label class="control-label col-sm-4" for="date">Date</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="date" id="date"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="group">Group</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="group" class="form-control">
                    <option value selected disabled>Please select a group</option>
                    <option value="1">OHS (Health & Safety)</option>
                    <option value="2">E (Environment)</option>
                    <option value="3">Q (Quality)</option>
                    <option value="4">F (Food)</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="type">Type</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select name="group" class="form-control">
                    <option value selected disabled>Please select a type</option>
                    <option value="1">Not Specified</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="department">Department</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more departments">
                        <option>I.T</option>
                        <option>Finance</option>
                        <option>HR</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="dueDate">Due Date</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="dueDate" id="dueDate"> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="responsiblePerson">Responsible Person</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a person">
                        <option>Nick Botha</option>
                        <option>Sunny Mathole</option>
                        <option>Brian Douglas</option>
                        <option>Emmanuel Van Der Westhuizen</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="nonConformanceDetails">Non-Conformance Details</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="nonConformanceDetails" id="nonConformanceDetails"></textarea>
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="recommendedImprovement">Recommended Improvement</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <textarea class="form-control" rows="5" name="recommendedImprovement" id="recommendedImprovement"></textarea>
                </div>
            </div> 
            <div class="form-group">
                <label class="control-label col-sm-4" for="resourceInvolved">Resource Involved</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a resource">
                        <option>Resource 1</option>
                        <option>Resource 2</option>
                        <option>Resource 3</option>
                    </select> 
                </div>
            </div>
            <span class="control-label col-sm-4"></span>
            <div class="col-lg-8">
                <p style="color:red;"><b>Resource not listed</b></p>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notListedResourceType">Not Listed</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedResourceType" id="notListedResourceType" placeholder="Type"> 
                </div>
            </div>
            <div class="form-group">
                <span class="col-sm-4" for="notListedResourceDescription"></span>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedResourceDescription" id="notListedResourceDescription" placeholder="Description"> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="personInvolved">Employee Involved</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select an employee">
                        <option>Nick Botha</option>
                        <option>Sunny Mathole</option>
                        <option>Brian Douglas</option>
                    </select> 
                </div>
            </div>
            <span class="control-label col-sm-4"></span>
            <div class="col-lg-8">
                <p style="color:red;"><b>Person not listed</b></p>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notListedPersonType">Not Listed</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedPersonType" id="notListedPersonType" placeholder="Type"> 
                </div>
            </div>
            <div class="form-group">
                <span class="col-sm-4" for="notListedPersonDescription"></span>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedPersonDescription" id="notListedPersonDescription" placeholder="Description"> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="supplier">Supplier</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a supplier">
                        <option>Supplier 1</option>
                        <option>Supplier 2</option>
                        <option>Supplier 3</option>
                    </select> 
                </div>
            </div>
            <span class="control-label col-sm-4"></span>
            <div class="col-lg-8">
                <p style="color:red;"><b>Supplier not listed</b></p>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="notListedSupplierCompany">Not Listed</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <input type="text" class="form-control" name="notListedSupplierCompany" id="notListedSupplierCompany" placeholder="Company Name"> 
                </div>
            </div><br/>
            <div class="form-group">
                <label class="control-label col-sm-4" for="uploadPhoto">Take/Upload Photo</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="file" name="uploadPhoto" id="uploadPhoto"> 
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigate">Do you want to send to investigate?</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <label class="radio-inline"><input type="radio" name="investigate" id="investigate1" value="0" required>Yes</label>
                    <label class="radio-inline"><input type="radio" name="investigate" id="investigate2" value="1" >No</label>
                </div>
            </div><br/>

            <div class="sendToInvestigate hidden-div">
            <div class="modal-custom-h5"><span><h5>Send To Investigate</h5></span></div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigator">Investigator</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                    <select class="selectpicker form-multiselect" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select a investigator">
                        <option>Nick Botha</option>
                        <option>Sunny Mathole</option>
                        <option>Brian Douglas</option>
                    </select> 
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="investigatorDueDate">Due Date</label>
                <div class="col-lg-4 col-md-4 col-sm-8">
                <input type="text" class="form-control" name="investigatorDueDate" id="investigatorDueDate"> 
                </div>
            </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" id="addNonConformanceBtn" class="btn btn-success">Add Non-Conformance</button>
                </div>
            </div>
        </form>
        </div>
        </div>
        <!--End of the panel panel-default-->
</div>
</div><!--End of container-fluid-->
<script>
    $(function() {
    $("#date").datepicker({
        dateFormat:"yy/mm/dd",
        maxDate: new Date(), 
        minDate: new Date()});
    $("#date").datepicker("setDate", new Date());
    $("#signoffDate").datepicker({
        dateFormat:"yy/mm/dd",
        maxDate: new Date(), 
        minDate: new Date()});
    $("#signoffDate").datepicker("setDate", new Date());
    
    $("#dueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    $("#investigatorDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    $("#investigationDueDate").datepicker({
        dateFormat:"yy/mm/dd", 
        minDate: new Date(),
        changeYear:true, 
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
    });
    
    $("#addInvestigationForm").hide();
    $("#investigationOptions").change(function(){
       var investigationOptions = $("#investigationOptions" ).val();
       if(investigationOptions === 'addInvestigation'){
           $("#addInvestigationForm").show();
       }else{
           $("#addInvestigationForm").hide();
       }
    });
    $("#doInvestigationForm").hide();
    $("#investigationOptions").change(function(){
       var investigationOptions = $("#investigationOptions" ).val();
       if(investigationOptions === 'doInvestigation'){
           $("#doInvestigationForm").show();
       }else{
           $("#doInvestigationForm").hide();
       }
    });
    $('input[name="investigate"]').click(function(){
        if($(this).attr("value")==="0"){
            $(".hidden-div").not(".sendToInvestigate").hide();
            $(".sendToInvestigate").show();
        }else{
            $(".sendToInvestigate").hide();
        }
    });
</script>
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    