<?php 
$title = 'Improvement';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 

?> 
<?php
function getControlsForNonConformance($editpage,$noc_id)
{
?>
<div class="btn-group">
    <button style="color: white; text-decoration: none; background-color: #17a589;" class="btn btn-default dropdown-toggle" href="#" data-toggle="dropdown">
    Action <span class="caret"></span>
    </button>
    <ul class="dropdown-menu stay-open pull-right" role="menu" style="min-width: 150px;">
        <li><a href="<?php echo $editpage;?>"><span class="glyphicon glyphicon-pencil addglyphicon"></span> Edit</a></li>
        <li class="sweet-5" ncid="<?php echo $noc_id;?>"><a><span class="glyphicon glyphicon-trash addglyphicon delete_row"></span> Delete</a></li>
        <li class="divider"></li>        
        <li><a href="#"><span class="glyphicon glyphicon-plus addglyphicon"></span> Add Incident</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus addglyphicon"></span> Add Corrective Action</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus addglyphicon"></span> Request Maintenance</a></li>
        <li><a href="#investigate-modal" data-toggle="modal" data-target="#investigate-modal"><span class="glyphicon glyphicon-plus addglyphicon"></span> Request Investigation</a></li>
        <li><a href="#addImprovement-modal" ncid="<?php echo $noc_id;?>" data-toggle="modal" data-target="#addImprovement-modal"><span class="glyphicon glyphicon-plus addglyphicon"></span> Request Improvement</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus addglyphicon"></span> Request Objective</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus addglyphicon"></span> Request Training</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-plus addglyphicon"></span> Request Communication</a></li>
    </ul>
</div>
<?php 
}
    //use modals here
    //include_once('frontend/templates/modals/request_investigation_modal.php'); 
    include_once('frontend/templates/modals/request_improvement_modal.php'); 
?>
<script>
    $(document).ready(function()
    {
    $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
         $('.table-responsive').css( "overflow", "auto" );
    });
var buttons = document.querySelectorAll('.sweet-5');
// var row = $(this).parent().parent().parent().parent().html();
$(".sweet-5").on("click",function()
{
    var row = $(this).parent().parent().parent().parent();
    var noc_id = $(this).attr('ncid');
    swal({
          title: "Are you sure you want to delete this record?",
         // text: "You may restore this record, by requesting a restore action by your administrator!",
          type: "input",
          showCancelButton: true,
          confirmButtonClass: 'btn-danger',
          confirmButtonText: 'Yes, delete it!',
          closeOnConfirm: false,
          //closeOnCancel: false
          inputPlaceholder: "Reason For Deleting"
        },
        function (inputValue) 
        {
        if (inputValue === false)return false;
        if (inputValue === "") {
          swal.showInputError("You need to provide a reason for deleting!");
          return false;
        }
        //swal("Deleted!", "Reason For Deletion: " + inputValue, "success");  
        var url = "<?php echo BASE_URL;?>/index.php?action=deleteNonConformance&module=non_conformance";
        $.ajax({
            type:'post',
            url:url,
            data:{ncid:noc_id,reason:inputValue},
            success:function(result)
            {                                
                if(result == "succeed")
                {
                    swal("Deleted!", "", "success");  
                    $(row).fadeOut('slow');
                }
                else
                {
                    swal("Deletion failed","Please make sure you have Sufficient permissions to delete this record", "error");
                }
            }
        });
        
      });        
});
/*for (var i = 0; i < buttons.length; i++) {
  buttons[i].onclick = function(){
	
};
}*/
    });
</script>
<!--End of navigation-->   
  <div class="col-lg-10">
        <!--sub menu-->
        <div class="row">
            <div class="col-lg-12">
            <ul class="nav nav-pills nav-justified topbar-menu">
               <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Non-Conformance</a></li>
                <li><a href="improvement_incident.php">Incidents</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Investigations</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action">Corrective Actions</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Improvement</a></li>
                <li><a href="improvement_objective.php">Objective</a></li>
            </ul>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6">
            <ul class="nav nav-pills nav-justified topbar-menu">
                <li ><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Continues Improvement</a></li>
                <li><a href="improvement_nonconformance_inspections.php">Inspections</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllHistoryConformance&module=non_conformance">History</a></li>
                <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=viewAllArchiveConformance&module=non_conformance">Archive</a></li>
            </ul>
            </div>
        </div>
                
        <div class="panel panel-default">
        <div class="panel-heading">Archived Non-Conformances</div>
        <div class="panel-body">
        <div class="table-responsive">   
        
        <table width="100%" class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Date</th>
              <th>Group</th>
              <th>Department</th>
              <th>Due Date</th>
              <th>Responsible Person</th>
              <th>Non Conformance Details</th>              
              <th>Lodged By</th>
              <th>Status</th>
              <th>Reason for archive</th>              
              <th></th>
            </tr>
          </thead>
          <tbody>
               <?php if($data['allNonConformance']): foreach($data['allNonConformance'] as $nc): ?>
                            <tr>
                                <td><?php echo $nc['nc_id']; ?></td>
                                <td><?php echo $nc['nc_date_created']; ?></td>                                
                                <td><?php echo $nc['nc_group_field']; ?></td>
                                <td><?php echo $nc['department_name']; ?></td>
                                <td><?php echo $nc['nc_due_date']; ?></td>
                                <td><?php echo $nc['responsible']; ?></td>
                                <td><?php echo $nc['non_conformance_details']; ?></td>                                
                                <td><?php echo $nc['loggedBy']; ?></td>                                 
                                <td></td>
                                <td><?php echo $nc['in_active_reason']; ?></td>  
                                <td><a href="<?php echo BASE_URL;?>/index.php?ncid=<?php echo $nc['nc_id'];?>&action=restore_non_conformance&module=non_conformance">Restore</a></td>
                            </tr>
                        <?php endforeach;endif; ?>
          </tbody>
        </table>

        </div>
        </div>
    </div>
    <!--End of the pane panel-default-->
    </div>  
</div><!--End of container-fluid-->
<?php 
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  