<?php 
$title = 'View Non Conformance Report';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 
<!--End of navigation-->   
  <div class="col-lg-10">
        <!--sub menu-->
          <div class="row">
            <div class="col-lg-12">
            <ul class="nav nav-pills nav-justified topbar-menu">
                <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=viewAllConformance&module=non_conformance">Non-Conformance</a></li>
                <li><a href="improvement_incident.php">Incidents</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getAllInvestigations&module=investigation">Investigations</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllCorrectiveActions&module=corrective_action">Corrective Actions</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=getActiveImprovements&module=improvement">Improvement</a></li>
                <li><a href="improvement_objective.php">Objective</a></li>
            </ul>
            </div>
        </div>
          <?php
   global $objNonConformance;
   if(isset($data["allNonConformance"]) && !empty($data["allNonConformance"]))
   {
        foreach($data["allNonConformance"] as $i)
        {
             $objNonConformance->get_id = $i["nc_id"];
             $objNonConformance->get_created_date = $i["nc_date_created"];
             $objNonConformance->get_group_name = $i["nc_group_field"];
             $objNonConformance->get_department_name = $i["department_name"];
             $objNonConformance->get_due_date = $i["nc_due_date"];
             $objNonConformance->get_responsible_person = $i["responsible"];
             $objNonConformance->get_person_involved = $i["person_involve"];
             $objNonConformance->get_resource_involved= $i["resource"];
             $objNonConformance->get_non_conformance_details = $i["non_conformance_details"];
             $objNonConformance->get_non_conformance_recomm = $i["recommended_improvement_field"];
             $objNonConformance->get_supplier_name = $i["oc_name"];
             $objNonConformance->get_loggedBy = $i["loggedBy"];
             $objNonConformance->get_status = $i["status"];
             $objNonConformance->get_photo = $i["photo_name"];
             $objNonConformance->get_signedOffBy = $i["signedOffBy"];
             $objNonConformance->get_signedOffDate = $i["signedOffDate"];
             $objNonConformance->get_signedOffAction = $i["signedOffAction"];
             break;
        }
   }
   if($objNonConformance->get_id == 0)
   {
       //error handeling information
       header( 'Location: '.BASE_URL.'/index.php?action=dashboard&module=user');
   }
   ?>
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Non-Conformance Details</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">No</td>
                                    <td><?php echo $objNonConformance->get_id;?></td>
                                </tr>
                                <tr>
                                    <td>Group</td>
                                    <td><?php echo $objNonConformance->get_group_name;?></td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Not Specified</td>
                                </tr>
                                <tr>
                                    <td>Department</td>
                                    <td><?php echo $objNonConformance->get_department_name;?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Due Date</td>
                                    <td><?php echo $objNonConformance->get_due_date;?></td>
                                </tr>
                                <tr>
                                    <td>Responsible Person</td>
                                    <td><?php echo $objNonConformance->get_responsible_person;?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Non-Conformance</td>
                                    <td><?php echo $objNonConformance->get_non_conformance_details;?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Recommended Improvement</td>
                                    <td><?php echo $objNonConformance->get_non_conformance_recomm;?></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Involvement</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource Involved</td>
                                    <td><?php echo $objNonConformance->get_resource_involved;?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Employee Involved</td>
                                    <td><?php echo $objNonConformance->get_person_involved;?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Supplier Involved</td>
                                    <td><?php echo $objNonConformance->get_supplier_name;?></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Images</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Photo</td>
                                    <td>
                                        <?php
                    if($objNonConformance->get_photo != "")
                    {
                        echo "<img width='250px' class='img-responsive' src='".BASE_URL."/frontend/media/uploads/".$objNonConformance->get_photo."' alt='non-conformance image'/>";
                    }
                    else
                    {
                        echo "None";
                    }
?></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Sign Off</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="30%">Sign Off By</td>
                                    <td><?php echo $objNonConformance->get_signedOffBy;?></td>
                                </tr>
                                <tr>
                                    <td>Sign Off Date</td>
                                    <td><?php echo $objNonConformance->get_signedOffDate;?></td>
                                </tr>
                                <tr>
                                    <td>Action Taken</td>
                                    <td><?php echo $objNonConformance->get_signedOffAction;?></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
        
    </div>  
</div><!--End of container-fluid-->
<!--End of container-fluid-->
<?php 
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  