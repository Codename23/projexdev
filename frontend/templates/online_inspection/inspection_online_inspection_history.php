<?php 
/*
 * Header file
 */
$title = 'Online Inspection History';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online">Upcoming</a></li>
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspect_now">Inspect Now</a></li> 
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_nonconformances">Non-Conformances</a></li> 
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_plan">Inspection Plan</a></li>
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_create_inspection">Create Template</a></li>
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_history">Inspection History</a></li>             
            <!--<li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_settings">Settings</a></li> -->
        </ul>
        </div>
    </div>


    <div class="panel panel-default">
    <div class="panel-heading">Search Inspection History</div>
    <div class="panel-body">
    <form class="form-inline" method="post" name="search" action="">
        <div class="col-md-12" style="margin-bottom: 10px;">            
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Group</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more groups"data-header="Close">
                <option value="1">OHS (Health & Safety)</option>
                <option value="2">E (Environment)</option>
                <option value="3">Q (Quality)</option>
                <option value="4">F (Food)</option>
            </select>
        </div>
        </div>
        
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Description</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more descriptions"data-header="Close">
                <option value="1">Emergency Equipment</option>
                <option value="2">House Keeping</option>
            </select>
        </div>
        </div> 
            
        <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <p><b>Department</b></p>
            <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more departments"data-header="Close">
                <option>I.T</option>
                <option>HR</option>
                <option>Finance</option>
            </select>
        </div>
        </div>
        </div>
        
        <div style="margin-left: 30px;">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Search">
        </div>
        </div>
    </form>
    </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">Upcoming Inspection</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>Inspection Date</th>
                    <th>Group</th>                    
                    <th>Description</th>
                    <th>Department</th>
                    <th>Inspector</th>
                    <th>Score</th>
                    <th>Non-Conformance</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                  <?php
            $score = "0%";
            
               if($data['allInspections']): foreach($data['allInspections'] as $inspection):
                   ?>
                            <tr>
                                <td><?php echo $inspection['date_created']; ?></td>
                                <td><?php echo $inspection['sheqteam_name']; ?></td>                                                              
                                 <td><?php echo $inspection['inspection_description']; ?></td>                                                                                            
                                 <td><?php echo $inspection['department_id']; ?></td>  
                                  <td><?php echo $inspection['department_id']; ?></td>  
                                 <td><?php echo $inspection['department_id']; ?></td>  
                                <td><a href="#viewNonconformance-modal" data-toggle="modal" data-target="#viewNonconformance-modal">View (3)</a></td>
                                <td></td>
                            </tr>
                        <?php endforeach;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!--View Non-Conformances Modal -->
  <div class="modal fade" id="viewNonconformance-modal" role="dialog">
    <div class="modal-dialog risk-assessment-dialog-modal">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Non-Conformances</h4>
        </div>
        <div class="modal-body table-responsive">
          <table width="100%" class="table table-hover">
            <thead>
            <tr>
                <th>Department</th>
                <th>Category</th>
                <th>Type</th>
                <th>Description</th>
                <th>No</th>
                <th>C</th>
                <th>NC</th>
                <th>Comment</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Storage</td>
                <td>Fixed Electrical</td>
                <td>Air Conditioner</td>
                <td>LG 90000</td>
                <td>2</td>
                <td></td>
                <td>X</td>
                <td>Exposed Electrical Wires</td>
                <td><a href="">View</a></td>
                <td><a href="#actionTaken-modal" data-toggle="modal" data-target="#actionTaken-modal">Action Taken</a></td>
                <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
            </tr>
            <tr>
                <td>Storage</td>
                <td>Fixed Electrical</td>
                <td>Air Conditioner</td>
                <td>LG 90000</td>
                <td>2</td>
                <td></td>
                <td>X</td>
                <td>Exposed Electrical Wires</td>
                <td><a href="">View</a></td>
                <td><a href="#actionTaken-modal" data-toggle="modal" data-target="#actionTaken-modal">Action Taken</a></td>
                <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
            </tr>
            <tr>
                <td>Storage</td>
                <td>Fixed Electrical</td>
                <td>Air Conditioner</td>
                <td>LG 90000</td>
                <td>2</td>
                <td></td>
                <td>X</td>
                <td>Exposed Electrical Wires</td>
                <td><a href="">View</a></td>
                <td><a href="#actionTaken-modal" data-toggle="modal" data-target="#actionTaken-modal">Action Taken</a></td>
                <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
            </tr>
            </tbody>
        </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->

<!--Action Taken Modal -->
  <div class="modal fade" id="actionTaken-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Action Taken</h4>
        </div>
        <div class="modal-body table-responsive">
          <table width="100%" class="table table-hover">
            <thead>
            <tr>
                <th>Date</th>
                <th>Action</th>
                <th>Ref No</th>
                <th>Action Taken</th>
                <th>Done By</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>16/06/2017</td>
                <td>Requested Maintenance</td>
                <td>23232</td>
                <td>Fixed Electrical Wires</td>
                <td>Peter Jones</td>
            </tr>
            </tbody>
        </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    