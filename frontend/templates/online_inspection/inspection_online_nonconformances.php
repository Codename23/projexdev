<?php 
/*
 * Header file
 */
$title = 'Online Inspection Non-Conformance';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?>

<div class="container-fluid">
<?php include_once('frontend/templates/menus/side-menu.php'); ?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online">Upcoming</a></li>
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspect_now">Inspect Now</a></li> 
             <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_nonconformances">Non-Conformances</a></li> 
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_plan">Inspection Plan</a></li>
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_create_inspection">Create Template</a></li>
            <li ><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_history">Inspection History</a></li> 
           
            <!--<li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_inspection_settings">Settings</a></li> -->
        </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_nonconformances">Open Non-Conformances</a></li> 
            <li><a href="<?php echo BASE_URL;?>/index.php?module=online_inspection&action=inspection_online_nonconformances_closed">Closed Non-Conformance</a></li> 
        </ul>
        </div>
    </div>


    <div class="panel panel-default">
    <div class="panel-heading">Open Non-Conformances</div>
    <div class="panel-body"> 
        <div class="table-responsive">
        <table width="100%" class="table table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Date</th>
                    <th>Group</th>
                    <th>Inspection Description</th>
                    <th>Department</th>
                    <th>Non-Conformance</th>
                    <th>Resource</th>
                    <th>Lodged By</th>
                    <th>Photo</th>
                    <th>Action Taken</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                    <?php
               $counter = 0;
               if($data['allNonConformance']): foreach($data['allNonConformance'] as $nc):
                   $counter++;
                  if($counter > 5)
                  {
                      break;
                  }
                   ?>
                            <tr>
                                <td><?php echo $nc['nc_id']; ?></td>
                                <td><?php echo $nc['nc_date_created']; ?></td>   
                                 <td><?php echo $nc['nc_group_field']; ?></td>  
                                 <td><?php echo $nc['non_conformance_details']; ?></td>  
                                <td><?php echo $nc['department_name']; ?></td> 
                                 <td><?php echo $nc['non_conformance_details']; ?></td>  
                                 <td><?php echo $nc['nc_resources_involved']; ?></td>
                                 <td><?php echo $nc['loggedBy']; ?></td>                                 
                            <?php
                                if(!empty($nc['photo_name']))
                                {
                                ?>
                                <td><a href="#nonConformancePhoto-modal" data-toggle="modal" data-img="<?php echo BASE_URL.'/frontend/media/uploads/'.$nc['photo_name'];?>" class="photo_view" data-target="#nonConformancePhoto-modal">View Photo</a></td>
                                <?php
                                }
                                else
                                {
                                    ?>
                                    <td></td>
                                    <?php
                                }
                                ?>
                                 <td><a href="#actionTaken-modal" class="view_actions" data-actions="<?php echo $nc['nc_id'];?>" data-toggle="modal" data-target="#actionTaken-modal">View</a></td>                                 
                            
                                <td><a href="#signOff-modal" data-toggle="modal" conid="<?php echo $nc['nc_id'];?>" class="non_conformance_signed_off" data-target="#signOff-modal">Sign Off</a></td> 
                                
                                <td>
                                    <?php
                                        $edit_page = BASE_URL."/index.php?ncid={$nc['nc_id']}&action=editNonConformance&module=non_conformance";
                                        //getControlsForNonConformance($edit_page, $nc['nc_id']);
                                    ?>
                                   </td>
                            </tr>
                        <?php endforeach;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>

<!--View Non-Conformances Modal -->
<!--  <div class="modal fade" id="viewNonconformance-modal" role="dialog">
    <div class="modal-dialog risk-assessment-dialog-modal">
    
       Modal content
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Non-Conformances</h4>
        </div>
        <div class="modal-body table-responsive">
          <table width="100%" class="table table-hover">
            <thead>
            <tr>
                <th>Department</th>
                <th>Category</th>
                <th>Type</th>
                <th>Description</th>
                <th>No</th>
                <th>C</th>
                <th>NC</th>
                <th>Comment</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Storage</td>
                <td>Fixed Electrical</td>
                <td>Air Conditioner</td>
                <td>LG 90000</td>
                <td>2</td>
                <td></td>
                <td>X</td>
                <td>Exposed Electrical Wires</td>
                <td><a href="">View</a></td>
                <td><a href="#actionTaken-modal" data-toggle="modal" data-target="#actionTaken-modal">Action Taken</a></td>
                <td><?php //include 'action_tbl_includes/action_tbl_dropdown.php'; ?></td>
            </tr>
            </tbody>
        </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>-->
<!--End Of Modal -->

<!-- Action Taken Modal -->
  <div class="modal fade" id="actionTaken-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">View Actions Taken</h4>
        </div>
        <div class="modal-body table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Action</th>
                            <th>Ref No</th>
                            <th>Action Taken</th>
                            <th>Done By</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>16/06/2017</td>
                            <td>Request Maintenance</td>
                            <td>23232</td>
                            <td>Fixed Electrical Wires</td>
                            <td>Peter Jones</td>
                            <td><a href="#signoff-modal" data-toggle="modal" data-target="#signoff-modal">Sign Off</a></td>
                        </tr>
                    </tbody>
                </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

<!-- Sign Off Modal -->
  <div class="modal fade" id="signoff-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sign Off</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="signoffForm" action="">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="password">Password</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                    <input type="password" class="form-control" name="password" id="password" required placeholder=""> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" id="signoffBtn" class="btn btn-success">Sign Off</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
  </div>
<!--End Of Modal -->

</div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    