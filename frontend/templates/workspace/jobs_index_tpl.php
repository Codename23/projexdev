<?php 
$title = 'Dashboard Corrective Actions';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/main-menu.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 
?>
<!--End of navigation-->   

<h1 class="heading-title">Workspace </h1>

<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?> 

<div class="col-lg-10">
    
    <!--sub menu-->
    <div class="row">
        <div class="col-lg-12">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li><a href="workspace.php">Workspace</a></li>
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="dashboard_due_scheduled.php">Due / Scheduled</a></li>
            <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=displayJobs&module=workspace">Jobs</a></li>
            <li><a href="dashboard_my_department_status.php">My Depatments Status</a></li>   
            <li><a href="dashboard_system_audit.php">System Audit</a></li>   
        </ul>
        </div>
    </div>
 
    <div class="row">
        <div class="col-lg-6">
        <ul class="nav nav-pills nav-justified topbar-menu">
            <li class="active"><a href="dashboard_jobs.php">Outbox <span class="label label-danger">10</span></a></li>
            <li><a href="dashboard_jobs_from.php">Inbox <span class="label label-danger">0</span></a></li>
            <li><a href="">Approvals</a></li>
        </ul>
        </div>
    </div>  
    <style>
        .nav-tabs { border-bottom: 2px solid #DDD; }
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
    .nav-tabs > li > a { border: none; color: #666; }
        .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #4285F4 !important; background: transparent; }
        .nav-tabs > li > a::after { content: ""; background: #4285F4; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
    .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
.tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
.tab-pane { padding: 0px 0; }
.tab-content{padding:0px}

.card {background: #FFF none repeat scroll 0% 0%; box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3); margin-bottom: 30px; }
    </style>
  <div class="panel panel-default">
      <div class="panel-heading" style="padding:0px;">
          <div class="card" style="padding:0px;margin-bottom: 0px;">
                                    <ul class="nav nav-tabs panel-primary" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Non-Conformance <span class="label label-danger">10</span></a></li>
                                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Incident <span class="label label-danger">10</span></a></li>
                                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Corrective Action <span class="label label-danger">10</span></a></li>
                                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Maintenance <span class="label label-danger">10</span></a></li>
                                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Investigation <span class="label label-danger">10</span></a></li>
                                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Maintenance <span class="label label-danger">10</span></a></li>
                                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Improvement <span class="label label-danger">10</span></a></li>
                                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Objective <span class="label label-danger">10</span></a></li>
                                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Training <span class="label label-danger">10</span></a></li>
                                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Communication <span class="label label-danger">10</span></a></li>
                                    </ul>
      </div>
        <div class="panel-body" style="padding:0px;">     
<div class="card" style="padding:0px;margin-bottom: 0px;">
                                    <!-- Nav tabs -->

                                    <!-- Tab panes -->
                                    <div class="tab-content" >
                                        <div role="tabpanel" class="tab-pane active" id="home">
                                            <div class="table-responsive">   
        
        <table width="100%" class="table table-hover">
          <thead>
            <tr>
              <th>Date</th>
              <th>Job</th>
              <th>Description</th>
              <th>Priority</th>
              <th>Responsible Person</th>
              <th>Due Date</th>
              <th>Notes</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>15/01/2017</td>
              <td>Maintenance</td>
              <td>Exposed electrical wires</td>
              <td class="majordanger-table-bg">High</td>
              <td>Nick Botha</td>
              <td>16/01/2017</td>
              <td><a href="">View</a></td>
              <td>In Progress</td>
              <td><a href="">Sign Off</a></td>
            </tr>
          </tbody>
        </table>

        </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="profile">
                                          <div class="table-responsive">   
        
        <table width="100%" class="table table-hover">
          <thead>
            <tr>
              <th>Date</th>
              <th>Job</th>
              <th>Description</th>
              <th>Priority</th>
              <th>Responsible Person</th>
              <th>Due Date</th>
              <th>Notes</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>15/01/2017</td>
              <td>Maintenance</td>
              <td>Exposed electrical wires</td>
              <td class="majordanger-table-bg">High</td>
              <td>Nick Botha</td>
              <td>16/01/2017</td>
              <td><a href="">View</a></td>
              <td>In Progress</td>
              <td><a href="">Sign Off</a></td>
            </tr>
          </tbody>
        </table>

        </div>                                                                                      
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="messages">
                                           <div class="table-responsive">   
        
        <table width="100%" class="table table-hover">
          <thead>
            <tr>
              <th>Date</th>
              <th>Job</th>
              <th>Description</th>
              <th>Priority</th>
              <th>Responsible Person</th>
              <th>Due Date</th>
              <th>Notes</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>15/01/2017</td>
              <td>Maintenance</td>
              <td>Exposed electrical wires</td>
              <td class="majordanger-table-bg">High</td>
              <td>Nick Botha</td>
              <td>16/01/2017</td>
              <td><a href="">View</a></td>
              <td>In Progress</td>
              <td><a href="">Sign Off</a></td>
            </tr>
          </tbody>
        </table>

        </div> 
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="settings">
                                           <div class="table-responsive">   
        
        <table width="100%" class="table table-hover">
          <thead>
            <tr>
              <th>Date</th>
              <th>Job</th>
              <th>Description</th>
              <th>Priority</th>
              <th>Responsible Person</th>
              <th>Due Date</th>
              <th>Notes</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>15/01/2017</td>
              <td>Maintenance</td>
              <td>Exposed electrical wires</td>
              <td class="majordanger-table-bg">High</td>
              <td>Nick Botha</td>
              <td>16/01/2017</td>
              <td><a href="">View</a></td>
              <td>In Progress</td>
              <td><a href="">Sign Off</a></td>
            </tr>
          </tbody>
        </table>

        </div>                                             
                                        </div>
                                    </div>
</div>
	</div>
</div>
    <!--End of the pane panel-default-->


    </div>
</div><!--End of container-fluid-->
<?php 
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  