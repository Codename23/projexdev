<?php

//Start the session
session_start();

//Define the root dir
define( 'ROOT', dirname(__file__) );
define( 'TEMPLATES', ROOT.'/frontend/templates/');\
define( 'BASE_URL', 'http://localhost/projex');
define( 'HOSTNAME','http://localhost/projex');
//define( 'BASE_URL', 'http://41.185.28.112/sheqonline-dev');
//define( 'HOSTNAME','http://41.185.28.112/sheqonline-dev');
define( 'SALT1', 'asdfjhaufhawefasdf');
define( 'DEFAULT_LANG', 'English');
define( 'LANGUAGES', ROOT.'/languages/');
define('FOLDER_DIRECTORY_ROOT',ROOT.'/documents/');
define('SITE_IMAGE_PATH',HOSTNAME.'/frontend/media/images/');
//define('DEFAULT_FOLDER_VIEW','listView');
define('DEFAULT_FOLDER_VIEW','gridView');
///global types 


//Define modules array
//changed document to documents
$modulesArray = array('jobs','online_inspection','criticalpoint','critical_control','process','material','documents','sheq_team','folders','risk_assessment','users','incidents', 'asset','company', 'department','occupation','regions','client','supplier', 'service', 'audit','provider', 'employee','branch',
                      'asset_involved', 'witness_involved', 'person_involved', 'thirdparty_asset', 'permissions','visitor', 'publicperson','non_conformance','investigation','improvement','corrective_actions','actions','investigators','signed_off','strategy','maintenances');
//Include classes
include_once(ROOT.'/classes/db_class.php');
include_once(ROOT.'/classes/language_class.php');
include_once(ROOT.'/classes/template_class.php');

foreach($modulesArray as $module){
    include_once(ROOT.'/classes/' . $module . '_class.php');
}

//Define global db obj
global $db;
$config = array();

/* DB Settings
$config['db']                                   = array();
$config['db']['username']                       = 'root';
$config['db']['password']                       = 'ureyus22';
$config['db']['host']                           = 'localhost';
$config['db']['database']                       = 'sheqonline';
$config['db']['port']                = 3306;*/

 //DB Settings
$config['db']                                   = array();
$config['db']['username']                       = 'root';
$config['db']['password']                       = '';
$config['db']['host']                           = 'localhost';
$config['db']['database']                       = 'projex';
$config['db']['port']                = 3306;

$db = new db_class( $config['db'] );
	
$db->connect();


global $objUsers;
if(!is_object($objUsers)){
    $objUsers = new users_class();
}

global $objLanguage;
if(!is_object($objLanguage)){
    $objLanguage = new language();
}

global $objTemplate;
if(!is_object($objTemplate)){
    $objTemplate = new template_class();
}

global $objAssets;
if(!is_object($objAssets)){
    $objAssets = new asset();
}

global $objCompanies;
if(!is_object($objCompanies)){
    $objCompanies = new company();
}

global $objOccupations;
if(!is_object($objOccupations)){
    $objOccupations = new occupations();
}

global $objBranch;
if(!is_object($objBranch)){
    $objBranch = new branches();
}

global $objDepartment;
if(!is_object($objDepartment)){
    $objDepartment = new departments();
}

global $objRegions;
if(!is_object($objRegions)){
    $objRegions = new regions();
}

global $objSupplier;
if(!is_object($objSupplier)){
    $objSupplier = new suppliers();
}

global $objService;
if(!is_object($objService)){
    $objService = new services();
}

global $objAudit;
if(!is_object($objAudit)){
    $objAudit = new audits();
}

global $objProvider;
$objProvider = new providers();

global $objClient;
$objClient = new clients();

global $objIncidents;
if(!is_object($objIncidents)){
    $objIncidents = new incident();
}

global $objAssetinvolved;
if(!is_object($objAssetinvolved)){
    $objAssetinvolved = new assetinvolved();
}

global $objWitnessinvolved;
if(!is_object($objWitnessinvolved)){
    $objWitnessinvolved = new witnessinvolved();
}

global $objPersoninvolved;
if(!is_object($objPersoninvolved)){
    $objPersoninvolved = new personinvolved();
}

global $objThirdpartyasset;
if(!is_object($objThirdpartyasset)){
    $objThirdpartyasset = new thirdpartyasset();
}

global $objEmployee;
if(!is_object($objEmployee)){
    $objEmployee = new employees();
}





//setup global modules
global $objNonConformance;
if(!is_object($objNonConformance)){
    $objNonConformance = new non_conformances();
}

global $objCorrectiveAction;
if(!is_object($objCorrectiveAction)){
    $objCorrectiveAction = new corrective_actions();
}
global $objInvestigations;
if(!is_object($objInvestigations)){
    $objInvestigations = new investigations();
}
global $objImprovements;
if(!is_object($objImprovements)){
    $objImprovements = new improvements();
}
global $objActions;
if(!is_object($objActions)){
    $objActions = new actions();
}
global $objInvestigators;
if(!is_object($objInvestigators)){
    $objInvestigators = new investigators();
}
global $objSignedOff;
if(!is_object($objSignedOff)){
    $objSignedOff = new signed_off();
}
global $objStrategy;
if(!is_object($objStrategy)){
    $objStrategy = new strategy();
}
global $objCriticalControls;
if(!is_object($objCriticalControls)){
    $objCriticalControls = new critical_controls();
}

//added improvements




global $objPublicperson;
if(!is_object($objPublicperson)){
    $objPublicperson = new publicperson();
}

global $objVisitor;
if(!is_object($objVisitor)){
    $objVisitor = new visitor();
}

global $objRiskAssessment;
if(!is_object($objRiskAssessment)){
    $objRiskAssessment = new risk_assessments();
}
global $objccp;
if(!is_object($objccp)){
    $objccp = new criticalpoints();
}

global $objFolders;
$objFolders = new folders_class();

global $objSheqTeam;
$objSheqTeam = new sheqTeam();

global $objDocument;
if(!is_object($objDocument)){
    $objDocument = new documents_class();
}
global $objMaintenance;
if(!is_object($objMaintenance)){
    $objMaintenance = new maintenances();
}
global $objMaterial;
if(!is_object($objMaterial)){
    $objMaterial = new material_class();
}
global $objProcess;
if(!is_object($objProcess)){
    $objProcess = new processors();
}
global $objOnlineInspections;
if(!is_object($objOnlineInspections)){
    $objOnlineInspections = new online_inspections();
}
global $objJobs;
if(!is_object($objJobs)){
    $objJobs = new jobs();
}
/**
/**
 * Autoloader function to automatically load module controllers
 * 
 * @param type $class
 */
function __autoload($class)
{
    include_once(ROOT.'/controllers/'.$class.'.php');
}

