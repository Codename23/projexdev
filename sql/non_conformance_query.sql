CREATE TABLE IF NOT EXISTS `sheqonline`.`tbl_non_conformance` 
(
id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
due_date  TIMESTAMP,
group_field BIGINT NOT NULL,
deparment_field BIGINT NOT NULL,
non_conformance_details text,
type_field varchar(100),
recommended_improvement_field text,
resource_invloved text,
add_resource_field varchar(250),
person_involved text,
add_person_field varchar(250),
occupation_id bigint,
status varchar(50),   
`foreignkey_id` BIGINT NOT NULL,  
`first_field` VARCHAR(255) NULL DEFAULT NULL,  
`second_field` TINYINT NULL DEFAULT 0,  
`created_by` BIGINT UNSIGNED NULL DEFAULT NULL,  
`date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
 `modified_by` BIGINT UNSIGNED NULL DEFAULT NULL,  
 `date_modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,  PRIMARY KEY (`id`),  
 INDEX `fk_tbl_non_conformance_idx` (`foreignkey_id` ASC), 
 CONSTRAINT `fk_tbl_non_conformance`    FOREIGN KEY (`foreignkey_id`)   
 REFERENCES `letlivedb`.`tbl_foreign_keys_table` (`id`)    
 ON UPDATE CASCADE) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- new table 
CREATE TABLE IF NOT EXISTS `sheqonline`.`tbl_non_conformance` 
(
id BIGINT UNSIGNED  NOT NULL PRIMARY KEY AUTO_INCREMENT,
due_date  TIMESTAMP,
group_field BIGINT NOT NULL,
deparment_field BIGINT NOT NULL,
non_conformance_details text,
type_field varchar(100),
recommended_improvement_field text,
resource_invloved text,
add_resource_field varchar(250),
person_involved text,
add_person_field varchar(250),
occupation_id bigint,
status varchar(50),   
`foreignkey_id` BIGINT NOT NULL,  
`first_field` VARCHAR(255) NULL DEFAULT NULL,  
`second_field` TINYINT NULL DEFAULT 0,  
`created_by` BIGINT UNSIGNED NULL DEFAULT NULL,  
`date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
 `modified_by` BIGINT UNSIGNED NULL DEFAULT NULL,  
 `date_modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)
 
 /*,  
 INDEX `fk_tbl_non_conformance_idx` (`foreignkey_id` ASC), 
 CONSTRAINT `fk_tbl_non_conformance`    FOREIGN KEY (`foreignkey_id`)   
 REFERENCES `letlivedb`.`tbl_foreign_keys_table` (`id`)    
 ON UPDATE CASCADE) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;*/

