-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2017 at 09:41 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sheqonline`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appointments`
--

DROP TABLE IF EXISTS `tbl_appointments`;
CREATE TABLE `tbl_appointments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `settings_id` bigint(20) UNSIGNED NOT NULL,
  `appointed_employee_id` bigint(20) UNSIGNED NOT NULL,
  `appointment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `appointment_expiry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_approved` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appointment_courses`
--

DROP TABLE IF EXISTS `tbl_appointment_courses`;
CREATE TABLE `tbl_appointment_courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `settings_id` bigint(20) UNSIGNED NOT NULL,
  `course_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appointmnet_settings`
--

DROP TABLE IF EXISTS `tbl_appointmnet_settings`;
CREATE TABLE `tbl_appointmnet_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `appointment_group_id` bigint(20) UNSIGNED NOT NULL,
  `doc_id` bigint(20) UNSIGNED DEFAULT NULL,
  `appointment_name` varchar(45) NOT NULL,
  `renewal_frequency` int(11) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_renewal_frequency`
--

DROP TABLE IF EXISTS `tbl_renewal_frequency`;
CREATE TABLE `tbl_renewal_frequency` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `frequency_name` varchar(45) DEFAULT NULL,
  `frequency_description` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_renewal_frequency`
--

INSERT INTO `tbl_renewal_frequency` (`id`, `frequency_name`, `frequency_description`) VALUES
(1, '7D', 'calendar week'),
(2, '1M', '1 calendar month'),
(3, '1Y', '1 year'),
(4, '2Y', '2 years'),
(5, '3M', 'quarter'),
(6, '6M', 'six months');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sheqteam_groups`
--

DROP TABLE IF EXISTS `tbl_sheqteam_groups`;
CREATE TABLE `tbl_sheqteam_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sheqteam_name` varchar(250) NOT NULL,
  `sheqteam_description` varchar(250) NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `modified_by` bigint(20) UNSIGNED NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_sheqteam_groups`
--

INSERT INTO `tbl_sheqteam_groups` (`id`, `sheqteam_name`, `sheqteam_description`, `created_by`, `modified_by`, `date_modified`, `date_created`) VALUES
(1, 'OHS (Health & Safety)', '', 0, 0, NULL, '2017-03-14 07:57:34'),
(2, 'E (Environment)', '', 0, 0, NULL, '2017-03-20 08:35:11'),
(3, 'Q (Quality)', '', 0, 0, NULL, '2017-03-20 08:35:11'),
(4, 'F (Food)', '', 0, 0, NULL, '2017-03-20 08:35:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_appointments`
--
ALTER TABLE `tbl_appointments`
  ADD PRIMARY KEY (`id`,`settings_id`,`appointed_employee_id`),
  ADD KEY `fk_tbl_appointments_tbl_appointmnet_settings1_idx` (`settings_id`),
  ADD KEY `fk_tbl_employee_sid_idx` (`appointed_employee_id`);

--
-- Indexes for table `tbl_appointment_courses`
--
ALTER TABLE `tbl_appointment_courses`
  ADD PRIMARY KEY (`id`,`settings_id`),
  ADD KEY `fk_tbl_appointment_courses_tbl_appointmnet_settings1_idx` (`settings_id`);

--
-- Indexes for table `tbl_appointmnet_settings`
--
ALTER TABLE `tbl_appointmnet_settings`
  ADD PRIMARY KEY (`id`,`appointment_group_id`),
  ADD KEY `fk_tbl_appointmnet_settings_tbl_appointment_types1_idx` (`appointment_group_id`),
  ADD KEY `fk_tbl_appointment_document_idx` (`doc_id`);

--
-- Indexes for table `tbl_renewal_frequency`
--
ALTER TABLE `tbl_renewal_frequency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sheqteam_groups`
--
ALTER TABLE `tbl_sheqteam_groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_appointments`
--
ALTER TABLE `tbl_appointments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tbl_appointment_courses`
--
ALTER TABLE `tbl_appointment_courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_appointmnet_settings`
--
ALTER TABLE `tbl_appointmnet_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_renewal_frequency`
--
ALTER TABLE `tbl_renewal_frequency`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_sheqteam_groups`
--
ALTER TABLE `tbl_sheqteam_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_appointments`
--
ALTER TABLE `tbl_appointments`
  ADD CONSTRAINT `fk_tbl_appointments_tbl_appointmnet_settings1` FOREIGN KEY (`settings_id`) REFERENCES `tbl_appointmnet_settings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_employee_id` FOREIGN KEY (`appointed_employee_id`) REFERENCES `tbl_department_occupations` (`users_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_appointment_courses`
--
ALTER TABLE `tbl_appointment_courses`
  ADD CONSTRAINT `fk_tbl_appointment_courses_tbl_appointmnet_settings1` FOREIGN KEY (`settings_id`) REFERENCES `tbl_appointmnet_settings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_appointmnet_settings`
--
ALTER TABLE `tbl_appointmnet_settings`
  ADD CONSTRAINT `fk_tbl_appointment_document` FOREIGN KEY (`doc_id`) REFERENCES `tbl_documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_appointmnet_settings_tbl_appointment_types1` FOREIGN KEY (`appointment_group_id`) REFERENCES `tbl_sheqteam_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
