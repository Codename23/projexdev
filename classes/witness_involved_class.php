<?php
/**
 * Class containing all methods providing asset functionality
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

class witnessinvolved
{

    /**
     * Method to initiate the class
     * 
     * @access public
     */
    public function __construct() 
    {
      
    }
    /** Method to add asset involved
     * 
     * @param array $incidentArr
     */
    public function addWitnessInvolved($witnessDetsArr){
        global $db;
        if (is_array($witnessDetsArr) && count($witnessDetsArr) > 0) {
            if(isset($_SESSION['incidentid']) && isset($witnessDetsArr['witness_id'])){
                $sql = "INSERT INTO tbl_incident_witness (incident_id, witness_id, witness_type)
                        VALUES (". $db->sqs($_SESSION['incidentid'])." , ".$db->sqs($witnessDetsArr['witness_id'])." , ".$db->sqs($witnessDetsArr['witnessType']).")";
                $db->query($sql);
                $response = $db->insertId();
                return $response;  
            }else{
                return "Required information is missing";
            }   
        }else{
            return FALSE;
        }     
    }
    /**
     * Method to edit person involved
     */
    public function editWitnessInvolved($witnessDetsArr){
        global $db;
        if(is_array($witnessDetsArr) && count($witnessDetsArr)>0){
            $sql = "UPDATE tbl_incident_witness SET witness_id =".$db->sqs($witnessDetsArr['witnessid'])." , witness_type =".$db->sqs($witnessDetsArr['witnesstype']).", modified_by = ".$db->sqs($_SESSION['user_id'])." WHERE incident_id = ".$db->sqs($witnessDetsArr['witnessid']);
            $result = $db->query($sql);
            return $result;                 
        }
    }
    /**
     *Method to get all person involved 
     */
    public function getWitnessInvolved($id){
        global $db;
        $sql = "SELECT * FROM tbl_incident_witness WHERE id =" . $db->sqs($id);
        $response = $db->getRow($sql);
        return $response;
    }
    /**
     *Method to get all person involved 
     */
    public function getAllWitnessInvolved(){
        global $db;
        $sql = "SELECT * FROM tbl_incident_witness WHERE incident_id =" . $db->sqs($_SESSION['incidentid']);
        $response = $db->getAll($sql);
        return $response;
    }
    /**
     * Method to check if the person involved exist
     */
    public function witnessExist($witnessid, $witnesstype){
        global $db;
        
        $sql = "SELECT id FROM tbl_incident_witness WHERE witness_id =" . $db->sqs($witnessid).", witness_type =" . $db->sqs($witnesstype).", incident_id = ".$db->sqs($_SESSION['incidentid']);
        $response = $db->getOne($sql);
        if(is_array($response) && count($response)>0){
            return TRUE;
        } else {
            return FALSE;
        }
    }
     /**
     * Method to deactivate a Witness Involved
     * 
     * @param int $id
     * @return bool
     */
    public function deactivateWitness($id)
    {
        global $db;
        
        if(!is_null($id)){
            $sql = "UPDATE tbl_incident_witness SET is_active = 0 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return TRUE;
            } else {
                return FALSE;
            }            
        }
    }        
    /**
     * Method to activate a Witness Involved
     * 
     * @param int $id
     * @return bool
     */
    public function activateWitness($id)
    {
        global $db;
        
        if(!is_null($id)){
            $sql = "UPDATE tbl_incident_witness SET is_active = 1 WHERE id = " . $db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return TRUE;
            } else {
                return FALSE;
            }            
        }
    }
    /**
     *Method to get all witnesses 
     */
    public function witnessInvolvedDetails($incidentid, $witnessType) {
        global $db;
        
        switch($witnessType){
            case 1:
            $sql = "SELECT tbl_user_data.*
                           FROM tbl_incident
                           INNER JOIN tbl_incident_witness ON tbl_incident_witness.incident_id = tbl_incident.id
                           INNER JOIN tbl_users ON tbl_incident_witness.witness_id = tbl_users.id
                           INNER JOIN tbl_user_data ON tbl_user_data.user_id = tbl_users.id
                           INNER JOIN tbl_user_type_fields ON tbl_user_data.field_id = tbl_user_type_fields.id 
                           WHERE tbl_incident_witness.witness_type =" .$db->sqs($witnessType). " AND tbl_incident.id =" . $db->sqs($incidentid);
            break;
            case 2 :
            $sql = "SELECT tbl_incident_witness.*,
                           tbl_client_company.client_id,
                           tbl_client_company.company_name,
                           tbl_client_company.contact_person,
                           tbl_client_company.email,
                           tbl_client_company.company_contact_number
                           FROM tbl_incident
                           INNER JOIN tbl_incident_witness ON tbl_incident_witness.incident_id = tbl_incident.id
                           INNER JOIN tbl_client_company ON tbl_incident_witness.witness_id = tbl_client_company.id
                           WHERE tbl_incident_witness.witness_type =" .$db->sqs($witnessType). " AND tbl_incident.id =" . $db->sqs($incidentid);   
            break;
            case 3 :
            $sql = "SELECT tbl_incident_witness.*,
                           tbl_service_providers.id,
                           tbl_service_providers.provider_name,
                           tbl_service_providers.contact_person,
                           tbl_service_providers.provider_email,
                           tbl_service_providers.cell_number
                           FROM tbl_incident
                           INNER JOIN tbl_incident_witness ON tbl_incident_witness.incident_id = tbl_incident.id
                           INNER JOIN tbl_service_providers ON tbl_incident_witness.witness_id = tbl_service_providers.id
                           WHERE tbl_incident_witness.witness_type =" .$db->sqs($witnessType). " AND tbl_incident.id =" . $db->sqs($incidentid);   
            break;
            case 4 :
            $sql = "SELECT tbl_incident_witness.*,
                           tbl_suppliers.id,
                           tbl_suppliers.supplier_name,
                           tbl_suppliers.contact_person,
                           tbl_suppliers.supplier_email,
                           tbl_suppliers.cell_number
                           FROM tbl_incident
                           INNER JOIN tbl_incident_witness ON tbl_incident_witness.incident_id = tbl_incident.id
                           INNER JOIN tbl_suppliers ON tbl_incident_witness.witness_id = tbl_suppliers.id
                           WHERE tbl_incident_witness.witness_type =" .$db->sqs($witnessType). " AND tbl_incident.id =" . $db->sqs($incidentid);   
            break;
            case 5 :
            $sql = "SELECT tbl_incident_witness.*,
                           tbl_visitors.id,
                           tbl_visitors.first_name,
                           tbl_visitors.lastname,
                           tbl_visitors.company_name,
                           tbl_visitors.email,
                           tbl_visitors.contact_number
                           FROM tbl_incident
                           INNER JOIN tbl_incident_witness ON tbl_incident_witness.incident_id = tbl_incident.id
                           INNER JOIN tbl_visitors ON tbl_incident_witness.witness_id = tbl_visitors.id
                           WHERE tbl_incident_witness.witness_type =" .$db->sqs($witnessType). " AND tbl_incident.id =" . $db->sqs($incidentid);   
            break;
            case 6 :
            $sql = "SELECT tbl_incident_witness.*,
                           tbl_publicperson.id,
                           tbl_publicperson.first_name,
                           tbl_publicperson.lastname,
                           tbl_publicperson.company_name,
                           tbl_publicperson.email,
                           tbl_publicperson.contact_number
                           FROM tbl_incident
                           INNER JOIN tbl_incident_witness ON tbl_incident_witness.incident_id = tbl_incident.id
                           INNER JOIN tbl_publicperson ON tbl_incident_witness.witness_id = tbl_publicperson.id
                           WHERE tbl_incident_witness.witness_type =" .$db->sqs($witnessType). " AND tbl_incident.id =" . $db->sqs($incidentid);   
            break;
        }
        $response = $db->getAll($sql);
        if($response){
            return $response;
        } else {
            return false;
        }             
    }
}
