<?php
/**
 * Class containing all methods providing asset functionality
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

class thirdpartyasset
{

    /**
     * Method to initiate the class
     * 
     * @access public
     */
    public function __construct() 
    {
      
    }
    
    /**
     * Method to add asset involved
     * @param array $incidentArr
     */
    public function addThirdPartyAsset($thirdPartyDetsArr){
        global $db;
        if (is_array($thirdPartyDetsArr) && count($thirdPartyDetsArr) > 0) {
            if(isset($thirdPartyDetsArr['description']) && isset($_SESSION['incidentid'])){
                $sql = "INSERT INTO tbl_assets_thirdparty (incident_id, assetType, description, company_asset_number, effect_on_asset, created_by )
                        VALUES (". $db->sqs($_SESSION['incidentid'])." , ".$db->sqs($thirdPartyDetsArr['assetType'])." , ".$db->sqs($thirdPartyDetsArr['description'])." , ".$db->sqs($thirdPartyDetsArr['companyassetnumber'])." , ".$db->sqs($thirdPartyDetsArr['effectonasset'])." , " . $db->sqs($_SESSION['user_id'])." )";
                return  $response = $db->query($sql); 
            }else{
                return "Required information is missing";
            }   
        }else{
            return FALSE;
        }     
    }
    /**
     * Method to edit asset involved
     */
    public function editThirdPartyAsset($thirdPartyDetsArr){
        global $db;
        if(is_array($thirdPartyDetsArr) && count($thirdPartyDetsArr)>0){
            if(isset($_SESSION['incidentid']) && isset($thirdPartyDetsArr['description'])){
                $sql = "UPDATE tbl_assets_thirdparty SET assetType =".$db->sqs($thirdPartyDetsArr['assetType'])." , description =".$db->sqs($thirdPartyDetsArr['description']).", company_asset_number =".$db->sqs($thirdPartyDetsArr['companyassetnumber']).", effect_on_asset = " . $db->sqs($thirdPartyDetsArr['effectonasset']) .", modified_by = ".$db->sqs($_SESSION['user_id'])." WHERE id = ".$db->sqs($thirdPartyDetsArr['assetid']);
                $result = $db->query($sql);
                return $result;                   
            } else {
                return 'Required information missing.';
            }
        }        
    }
    /*
     * Method to get asset involved
     */
    public function getThirdPartyAsset($id){
        global $db;
        if(isset($id)){
            $sql = "SELECT * FROM tbl_assets_thirdparty WHERE id =".$db->sqs($id);
            $response = $db->getRow($sql);
            return $response;  
        }
    }
    
    /**
     * Method to get asset involved
     */
    public function getAllThirdPartyAsset($incidentid){
        global $db;
        $sql = "SELECT * FROM tbl_assets_thirdparty WHERE incident_id =".$db->sqs($incidentid);
        $response = $db->getAll($sql);
        return $response;
    }
   
}
