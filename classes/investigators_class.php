<?php

class investigators 
{
    public function __construct() {        
    }
    public function getAllInvestigators($is_active = null)
    {
        global $db;
        $sql = "select CONCAT(firstname,' ',lastname) as fullname,
             DATE_FORMAT(tbl_investigators.date_modified,'%m/%d/%Y') as iv_date_modified,tbl_departments.department_name,tbl_investigators.date_created,tbl_investigators.investigation_dep,
        tbl_investigators.investigation_group,tbl_investigators.id as invest_id from tbl_investigators
        inner join tbl_users
        on tbl_investigators.emp_id = tbl_users.id
        inner join tbl_departments
        on tbl_investigators.department_id = tbl_departments.id "
        . " where tbl_investigators.branch_id = {$db->sqs($_SESSION['branch_id'])} and "
        . " tbl_investigators.company_id = {$db->sqs($_SESSION['company_id'])}";
        if($is_active != null)
        {
            $sql .= " and tbl_investigators.is_active = 1";
        }
        else
        {
            $sql .= " and tbl_investigators.is_active = 0";
        }
         $getResult = $db->getAll($sql);
         if($getResult == true)
         {
             return $getResult;           
         }         
         else
         {
            return false;
         }
    }
    public function getInvestigatorById($is_active = null,$id)
    {
        global $db;
        $sql = "select CONCAT(firstname,' ',lastname) as fullname,
             DATE_FORMAT(tbl_investigators.date_modified,'%m/%d/%Y') as iv_date_modified,tbl_departments.department_name,tbl_investigators.date_created,tbl_investigators.investigation_dep,
        tbl_investigators.investigation_group,tbl_investigators.id as invest_id from tbl_investigators
        inner join tbl_users
        on tbl_investigators.emp_id = tbl_users.id
        inner join tbl_departments
        on tbl_investigators.department_id = tbl_departments.id "
        . " where tbl_investigators.id = {$id} and tbl_investigators.branch_id = {$db->sqs($_SESSION['branch_id'])} and "
        . " tbl_investigators.company_id = {$db->sqs($_SESSION['company_id'])}";
        if($is_active != null)
        {
            $sql .= " and tbl_investigators.is_active = 1";
        }
        else
        {
            $sql .= " and tbl_investigators.is_active = 0";
        }
         $getResult = $db->getAll($sql);
         if($getResult == true)
         {
             return $getResult;           
         }         
         else
         {
            return false;
         }
    }
    public function updateInvestigator($data = array())
    {
        global $db;
        $updateStatus = false;
        $sql = "update tbl_investigators set department_id = '{$db->sqs($data['department_id'])}',"
        . " occupation_id = '{$db->sqs($data['occupation_id'])}',"
        . " emp_id = '{$db->sqs($data['emp_id'])}',"
        . " investigation_dep = '{$db->sqs($data['investigation_dep'])}',"    
        . " investigation_group = '{$db->sqs($data['investigation_group'])}'," 
        . " is_active = '{$db->sqs($data['is_active'])}',"
        . " where branch_id = {$db->sqs($_SESSION['branch_id'])}"
        . " and company_id =  {$db->sqs($_SESSION['company_id'])} and id = ".$db->sqs($data['id']);
        $updateResult = $db->query($sql);
        if($updateResult)
        {
            $updateStatus = true;
        }
        return $updateStatus;
    }
    public function addInvestigator($data = array())
    {
        global $db;
        $status = false;
            $sql = "insert into tbl_investigators(department_id,occupation_id,emp_id,branch_id,company_id,investigation_dep,investigation_group,is_active,created_by)"
                . "VALUES(
                ".$db->sqs($data['department_id']).","
                 .$db->sqs($data['occupation_id']).","  
                .$db->sqs($data['emp_id']).","                    
                .$db->sqs($_SESSION['branch_id']).","
                .$db->sqs($_SESSION['company_id']).","
                .$db->sqs($data['investigation_dep']).","
                .$db->sqs($data['investigation_group']).",1,"
                .$db->sqs($_SESSION['user_id']).")";
            $insertResult = $db->query($sql);
            if($insertResult)
            {
                $status = true;
            }
        return $status;
    }
        public static function hasDeletedInvestigator($id)
    {
        global $db;
        $sql = " update tbl_investigators set is_active = 0 "
                . " where branch_id = {$db->sqs($_SESSION["branch_id"])}"
                . " and id = {$db->sqs($id)}";
         $result = $db->query($sql);
         if($result)
         {
             echo "succeed";
         }
         else
         {
             echo "failed";
         }
    }
    public  static function hasRestoredInvestigator($id)
    {
           global $db;
        $sql = " update tbl_investigators set is_active = 1 "
                . " where branch_id = {$db->sqs($_SESSION["branch_id"])}"
                . " and id = {$db->sqs($id)}";
         $result = $db->query($sql);
         if($result)
         {
             return "succeed";
         }
         else
         {
             return "failed";
         } 
    }
}
