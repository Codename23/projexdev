<?php
/**
 * SheqTeam class containing all SheqTeam methods
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class sheqTeam
{


    /**
     * Initialise sheqTeam class
     * sheqTeam constructor.
     */
    function __construct()
    {

    }


    /**
     * Method to get all Sheq Team Members
     * @param null $branchId
     * @return array|bool
     */
    public function getAllSheqTeamMembers($branchId = null){

        global $db;

        $sheqTeamSql = "SELECT * 
                      FROM tbl_appointments 
                      INNER JOIN tbl_department_occupations ON tbl_department_occupations.user_id = tbl_appointments.appointed_employee_id
                      WHERE tbl_department_occupations.branch_id = ".$db->sqs($branchId);

        $sheqTeamResults = $db->getAll($sheqTeamSql);

        if($sheqTeamResults){

            return $sheqTeamResults;
        }else{

            return false;
        }

    }


    /**
     * Method to get all appointment groups
     * @return array
     */
    public function getAllSheqTeamGroups(){

        global $db;

        $selectAppointmentGroupSql = "SELECT  *
                            FROM tbl_sheqteam_groups";

        $getGroupsResult = $db->getAll($selectAppointmentGroupSql);

        if($getGroupsResult){
            return $getGroupsResult ;
        }else{
            return array();
        }

    }

    /**
     * Method to get renewal Frequency
     * @return array
     */
    public function getRenewalFrequency(){

        global $db;
        $frequencySql = "SELECT *
                            FROM tbl_renewal_frequency";

        $getFrequencyResult = $db->getAll($frequencySql);

        if($getFrequencyResult){
            return $getFrequencyResult ;
        }else{
            return array();
        }

    }


    /**
     * Method to add document settings
     * @param array $settingData
     * @return bool
     */
    public function  addSheqTeamSettings($settingData = array()){

        global $db;

        $addSettingsSql = "INSERT INTO tbl_appointmnet_settings(
                                                                appointment_group_id,
                                                                doc_id,
                                                                appointment_name,
                                                                renewal_frequency,
                                                                created_by,
                                                                modified_by) 
                                                                VALUES(
                                                                  ".$db->sqs($settingData['groupId']).",
                                                                  ".$db->sqs($settingData['docId']).",
                                                                  ".$db->sqs($settingData['appointmentName']).",
                                                                  ".$db->sqs($settingData['renewalFrequency']).",
                                                                  ".$db->sqs($settingData['createdBy']).",
                                                                  ".$db->sqs($settingData['createdBy'])."
                                                                )";


        $addSettingsResults = $db->query($addSettingsSql);

        if($addSettingsResults){
            return true;
        }else{
            return false;
        }

    }

    /**
     * Method to update settings
     * @param array $settingData
     * @return bool
     */
    public function  updateSheqTeamSettings($settingData = array()){


        global $db;

        $sql = "UPDATE tbl_appointmnet_settings SET  appointment_group_id = ".$db->sqs($settingData['groupId']).",
                                                                doc_id =".$db->sqs($settingData['docId']).",
                                                                appointment_name =".$db->sqs($settingData['appointmentName']).",
                                                                renewal_frequency =  ".$db->sqs($settingData['renewalFrequency']).",
                                                                modified_by =  ".$db->sqs($settingData['modifiedBy'])."
                                                WHERE id = " . $db->sqs($settingData['settingId']);

        $result = $db->query($sql);
        if($result){
            return true;
        }else{
            return false;
        }

    }
    /**
     * Method to add appointment
     * @param array $appointmentData
     * @return bool
     */
    public function addAppointment($appointmentData = array()){

        global $db;
        $success = false;


        foreach ( $appointmentData['appointmentEmployee'] as $employee_id ) {


            $addAppointmentSql = "INSERT INTO tbl_appointments(
                                                        settings_id,
                                                        appointed_employee_id,
                                                        appointment_date,
                                                        appointment_expiry_date,   
                                                        created_by,       
                                                        modified_by
                                                        ) 
                                                                VALUES(
                                                                  " . $db->sqs($appointmentData['appointmentSetting']) . ",
                                                                  " . $db->sqs($employee_id) . ",
                                                                  " . $db->sqs($appointmentData['appointmentDate']) . ",
                                                                  " . $db->sqs($appointmentData['appointmentExpiry']) . ",
                                                                  " . $db->sqs($appointmentData['createdBy']) . ",
                                                                  " . $db->sqs($appointmentData['createdBy']) . "
                                                                )";

            $success = $db->query($addAppointmentSql);
        }

        return $success;

    }

    /**
     * Method to edit select appointment
     * @param array $appointmentData
     * @return bool
     */
    public function updateAppointment($appointmentData = array()){


        global $db;


        $updateAppointmentSql = "UPDATE tbl_appointments SET
                                                        settings_id = " . $db->sqs($appointmentData['appointmentSetting']) . ",
                                                        appointed_employee_id = " . $db->sqs($appointmentData['appointmentEmployee']) . ",
                                                        appointment_date = " . $db->sqs($appointmentData['appointmentDate']) . ",
                                                        appointment_expiry_date =" . $db->sqs($appointmentData['appointmentExpiry']) .", 
                                                        modified_by =  " . $db->sqs($appointmentData['modifiedBy']) . "
                                                   WHERE id =". $db->sqs($appointmentData['appointmentId']);


        $Results = $db->query($updateAppointmentSql);

        if($Results){

            return true;
        }else{
            return false;
        }


    }


    /**
     * Method to get all settings
     * @return array
     */
    public function getAllSheqTeamSettings(){


        global $db;

        $selectAppointmentSettingsSql = "SELECT  tbl_appointmnet_settings.*,
                                                  tbl_documents.filename,
                                                  tbl_renewal_frequency.frequency_description,
                                                  tbl_sheqteam_groups.sheqteam_name
                                          FROM tbl_appointmnet_settings
                                          INNER JOIN tbl_documents  ON tbl_documents.id = tbl_appointmnet_settings.doc_id
                                          INNER JOIN tbl_renewal_frequency  ON tbl_renewal_frequency.id = tbl_appointmnet_settings.renewal_frequency
                                          INNER JOIN tbl_sheqteam_groups ON tbl_sheqteam_groups.id = tbl_appointmnet_settings.appointment_group_id ";

        $getSettingsResult = $db->getAll($selectAppointmentSettingsSql);

        if($getSettingsResult){
            return $getSettingsResult ;
        }else{
            return array();
        }

    }

    /**
     * Method to get all appointment settings by group
     * @param null $groupId
     * @return array
     */
    public function getAppointmentSettingByGroup($groupId = null){

        global $db;

        $selectAppointmentSettingsSql = "SELECT *
                                          FROM tbl_appointmnet_settings
                                          WHERE appointment_group_id  = ".$db->sqs($groupId);

        $getSettingsResult = $db->getAll($selectAppointmentSettingsSql);
        if($getSettingsResult){
            return $getSettingsResult ;
        }else{
            return array();
        }

    }

    /**
     * Method to get selected appointment id
     * @param null $settingId
     * @return array
     */
    public function getAppointmentSettingInfo($settingId = null){

        global $db;

        $selectAppointmentSettingsSql = "SELECT tbl_appointmnet_settings.*,
                                                tbl_renewal_frequency.frequency_name
                                          FROM tbl_appointmnet_settings
                                          INNER JOIN tbl_renewal_frequency  ON tbl_renewal_frequency.id = tbl_appointmnet_settings.renewal_frequency
                                          WHERE tbl_appointmnet_settings.id  = ".$db->sqs($settingId);

        $getSettingsResult = $db->getRow($selectAppointmentSettingsSql);
        if($getSettingsResult){
            return $getSettingsResult ;
        }else{
            return array();
        }

    }

    /**
     * Method to get appointment info
     * @param null $appointmentId
     * @return array
     */
    public function getAppointmentAssignmentInfo($appointmentId = null){

        global $db;

        $selectAppointmentSql = "SELECT tbl_appointments.*,
                                        tbl_appointmnet_settings.appointment_name,
                                        tbl_appointmnet_settings.doc_id,
                                        tbl_documents.filename,
                                        tbl_sheqteam_groups.sheqteam_name,
                                        tbl_renewal_frequency.frequency_name,
                                        tbl_renewal_frequency.frequency_description,
                                        tbl_sheqteam_groups.id as group_id
                                  FROM tbl_appointments
                                  INNER JOIN tbl_appointmnet_settings  ON tbl_appointmnet_settings.id = tbl_appointments.settings_id
                                  INNER JOIN tbl_renewal_frequency  ON tbl_renewal_frequency.id = tbl_appointmnet_settings.renewal_frequency
                                  INNER JOIN tbl_department_occupations ON tbl_department_occupations.users_id = tbl_appointments.appointed_employee_id
                                  INNER JOIN tbl_documents  ON tbl_documents.id = tbl_appointmnet_settings.doc_id
                                  INNER JOIN tbl_sheqteam_groups ON tbl_sheqteam_groups.id = tbl_appointmnet_settings.appointment_group_id 
                                  WHERE  tbl_appointments.id = " . $db->sqs($appointmentId) ;

        $getAppointmentResult = $db->getRow($selectAppointmentSql);
        if($getAppointmentResult){
            return $getAppointmentResult ;
        }else{
            return array();
        }

    }

    public function getAllAppointments($branchId = null,$searchOptions = ''){



        global $db;

        $selectAppointmentSql = "SELECT tbl_appointments.*,
                                      tbl_appointmnet_settings.appointment_name,
                                     tbl_documents.filename,
                                     tbl_sheqteam_groups.sheqteam_name,
                                     tbl_renewal_frequency.frequency_name
                                          FROM tbl_appointments
                                          INNER JOIN tbl_appointmnet_settings  ON tbl_appointmnet_settings.id = tbl_appointments.settings_id
                                          INNER JOIN tbl_renewal_frequency  ON tbl_renewal_frequency.id = tbl_appointmnet_settings.renewal_frequency
                                          INNER JOIN tbl_department_occupations ON tbl_department_occupations.users_id = tbl_appointments.appointed_employee_id
                                          INNER JOIN tbl_documents  ON tbl_documents.id = tbl_appointmnet_settings.doc_id
                                          INNER JOIN tbl_sheqteam_groups ON tbl_sheqteam_groups.id = tbl_appointmnet_settings.appointment_group_id 
                                          WHERE  tbl_department_occupations.branch_id = " . $db->sqs($branchId) ." {$searchOptions}" ;


        $getAppointmentResult = $db->getAll($selectAppointmentSql);
        if($getAppointmentResult){
            return $getAppointmentResult ;
        }else{
            return array();
        }



    }

    /**
     * Method to make appointment  as approved
     * @param array $approveData
     * @return bool
     */
    public function approveAppointment($approveData = array()){

        global $db;

        $sql = "UPDATE tbl_appointments SET is_approved =" . $db->sqs($approveData['approved']) . " , 
                                                modified_by = " . $db->sqs($approveData['modifiedBy']) . "
                                                WHERE id = " . $db->sqs($approveData['appointmentId']);
        $result = $db->query($sql);
        if($result){
            return true;
        }else{
            return false;
        }

    }


    public function employeeAppointmentInfo($appointmentId = null){




    }



}