<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of process_class
 *
 * @author Emmanuel van der Westhuizen <emmanuel.vd.west@gmail.com>
 */
class critical_controls {
    //put your code here
    public function __construct() {
        
    }
    //Start of Products and Services
    public static function getAllProductsAndServices()
    {
          global $db;
            $sql = "SELECT * FROM tbl_process_products_services where is_active = 1 and company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
    public static function addProductService($product,$description)
    {
         global $db;    
        $sql = "INSERT INTO tbl_process_products_services (prod_service,description,branch_id, company_id, created_by) "
                . " VALUES (". $db->sqs($product)." , 
                ". $db->sqs($description)." ,     
                ". $db->sqs($_SESSION['branch_id'])." , "
                . "". $db->sqs($_SESSION['company_id'])." , "
                . "" .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);                
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    /************
    End of Product and Services
    **********/
    
    /******
     * 
     * START OF PROCESS STOPPERS
     * **********/
    public static function getAllProcessStoppers()
    {
         global $db;
            $sql = "select tbl_departments.department_name, tbl_asset_category.`name` as category,
tbl_asset_type.`name` as type,tbl_assets.description,tbl_assets.id as no,tbl_process_stoppers.id,tbl_process_stoppers.date_created
from tbl_process_stoppers
inner join tbl_departments
on tbl_process_stoppers.department_id = tbl_departments.id
inner join tbl_assets
on tbl_process_stoppers.asset_id = tbl_assets.id
inner join tbl_asset_category
on tbl_assets.categorytype_id = tbl_asset_category.id
inner join tbl_asset_type
on tbl_assets.asset_type = tbl_asset_type.id where tbl_process_stoppers.is_active = 1 and tbl_process_stoppers.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
   public static function addProcessStopper($department,$asset)
    {
         global $db;    
        $sql = "INSERT INTO tbl_process_stoppers (department_id,asset_id ,branch_id, company_id, created_by) "
                . " VALUES (". $db->sqs($department)." , 
                ". $db->sqs($asset)." ,     
                ". $db->sqs($_SESSION['branch_id'])." , "
                . "". $db->sqs($_SESSION['company_id'])." , "
                . "" .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);                
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    /************
    End of Process Stoppers
    **********/
}
