<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of corrective_actions
 *
 * @author Innovatorshill
 */
class corrective_actions
{
    //put your code here     
    public $corrective_action_id = 0;
    public function __construct() {
         
    }     
    public static function getCorrectiveAction($id = null)
    {
        
    }
    public function addCorrectiveActionModel($corrective_details = array())
    {
        global $db;  
        
        $status = true;          
        $sql =   "insert into tbl_corrective_actions(department_id,branch_id,company_id,"
                . "type_field,priority,system_element,non_description,recom_description,"
                . "source_id,why_id,source_desc,description,person_responsibile,notify_duty_manager,notify_person,"
                . "master_sign_off,target_date,resource_id,approval_required,"
                . "status,is_active,created_by)"
                . "VALUES(".$db->sqs($corrective_details['cor_department']).","
                 .$db->sqs($_SESSION['branch_id']).","
                 .$db->sqs($_SESSION['company_id']).","
                .$db->sqs($corrective_details['type_field']).","
                .$db->sqs($corrective_details['priority']).","
                .$db->sqs($corrective_details['system_element']).","
                 .$db->sqs($corrective_details['nonConformanceDescription']).","
                 .$db->sqs($corrective_details['recommendedImprovements']).","
                .$db->sqs($corrective_details['source_id']).","
                .$db->sqs($corrective_details['why_id']).","
                .$db->sqs($corrective_details['source_desc']).","
                .$db->sqs($corrective_details['correctiveActionDescription']).","
                .$db->sqs($corrective_details['cor_person_responsible']).","
                .$db->sqs($corrective_details['notify_duty_manager']).","                
                .$db->sqs($corrective_details['notify_person']).","
                .$db->sqs($corrective_details['master_sign_off']).","
                .$db->sqs($corrective_details['correctiveActionDueDate']).","
                .$db->sqs($corrective_details['resource_id']).","
                .$db->sqs($corrective_details['needApproval']).","
                .$db->sqs($corrective_details['status']).","                   
                 ."1,".$db->sqs($_SESSION['user_id']).")";
         
        /*if($corrective_details['needApproval'] == 3)
        {
         *  
         * 
             $sql = "insert into tbl_corrective_actions(department_id,branch_id,company_id,"
                . "type_field,priority,system_element,non_description,recom_description,"
                   . "non_conformance_id,description,"
                . "person_responsibile,notify_duty_manager,notify_person,"
                . "master_sign_off,target_date,upload_photo,permit_photo,procedure_link,"
                . "form_link,method_statement_link,resource_id,approval_required,"
                . "committe,branch_level,group_level,motivation,estimate_cost,"
                . "longevity_correction_action,upload_documents,status,is_active,created_by)"
                . "VALUES(
                ".$db->sqs($corrective_details['department_id']).","
                 .$db->sqs($_SESSION['branch_id']).","
                 .$db->sqs($_SESSION['company_id']).","
                .$db->sqs($corrective_details['type_field']).","
                .$db->sqs($corrective_details['priority']).","
                .$db->sqs($corrective_details['system_element']).","
                .$db->sqs($corrective_details['non_conformance_id']).","
                .$db->sqs($corrective_details['description']).","
                .$db->sqs($corrective_details['person_responsibile']).","
                .$db->sqs($corrective_details['notify_duty_manager']).","                
                .$db->sqs($corrective_details['notify_person']).","
                .$db->sqs($corrective_details['master_sign_off']).","
                .$db->sqs($corrective_details['target_date']).","
                .$db->sqs($corrective_details['upload_photo']).","
                .$db->sqs($corrective_details['permit_photo']).","
                .$db->sqs($corrective_details['procedure_link']).","
                .$db->sqs($corrective_details['form_link']).","
                .$db->sqs($corrective_details['method_statement_link']).","
                .$db->sqs($corrective_details['resource_id']).","
                .$db->sqs($corrective_details['approval_required']).","
                .$db->sqs($corrective_details['committe']).","
                .$db->sqs($corrective_details['branch_level']).","
                .$db->sqs($corrective_details['group_level']).","
                .$db->sqs($corrective_details['motivation']).","
                .$db->sqs($corrective_details['estimate_cost']).","
                .$db->sqs($corrective_details['longevity_correction_action']).","
                .$db->sqs($corrective_details['upload_documents']).","
                .$db->sqs($corrective_details['status']).","                   
                 ."1,".$db->sqs($_SESSION['user_id']).")";
        }*/
          
         $addCorrectiveResults = $db->query($sql);
        if($addCorrectiveResults == true)
        {
            $this->corrective_action_id = $db->insertId();
            
            if($corrective_details["why_id"] > 0)
            {
                //update the why record
                self::updateWhyRecord($corrective_details["why_id"],$corrective_details["source_id"]);
                $status = true;
            }
            else
            {
                if($corrective_details['source_desc'] == "noc")
                {
                    self::updateNoc($corrective_details['source_id']);
                    $status = true;
                }
            }            
            
        }    
        return $status;
    }
    public static function updateWhyRecord($why_id,$source_id)
    {
       global $db;  
       $status = false;
       $sql = "update tbl_why set correctiveId = 1 where id = ".$db->sqs($why_id)." "
               . " and action_link_id = ".$db->sqs($source_id);
       $updateNoc = $db->query($sql);
       if($updateNoc == true)
       {
            $status = true;
       }    
       return $status;
    }
    public static function updateNoc($noc_id)
    {
       global $db;  
       $status = false;
       $sql = "update tbl_non_conformance set "
               . " correctiveActionDate = NOW(),"
               . " correctiveActionAdded = 1,"
               . " correctiveActionAddedBy = ".$db->sqs($_SESSION['user_id']).",attended = 1"
               . " where id = ".$db->sqs($noc_id);
       $updateNoc = $db->query($sql);
       if($updateNoc == true)
       {
            $status = true;
       }    
       return $status;
    }
    public static function viewCorrectiveActions($active = null)
    {
        global $db;
        $getAllCorrectiveActions = "";
        $query = "";
        $condition = 0;
        if($active)
        {
             $query = "";
             $condition = 1;
        }
        $sql = "select DATE_FORMAT(ca.target_date,'%m/%d/%Y') as ca_target_date,ca.*,(case ca.priority when 1 then 'Low' when 2 then 'Medium' when 3 then 'High' when 4 then 'Immediate' end) as priority,"
                . "(select department_name from tbl_departments where tbl_departments.id = ca.department_id) as dep,"                
         . " (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp "
         . " from tbl_users where tbl_users.id = ca.person_responsibile) as responsible from tbl_corrective_actions ca"
         . " where ca.is_active = 1 and ca.attended = $condition and ca.branch_id = ".$db->sqs($_SESSION['branch_id']);
        $getAllCorrectiveActionsResult = $db->getAll($sql);
        if($getAllCorrectiveActionsResult){
            return $getAllCorrectiveActionsResult;
        }else{
            return false;
        }
    }
    public static function searchCorrectiveAction()
    {
        
    }
    public static function editCorrectiveAction()
    {
        
    }
}
