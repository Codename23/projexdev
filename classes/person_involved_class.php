<?php
/**
 * Class containing all methods providing asset functionality
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

class personinvolved
{

    /**
     * Method to initiate the class
     * 
     * @access public
     */
    public function __construct() 
    {
      
    }
    /**
     * 
     * Methold to add person involved
     * @param array $personInvlovedArr
     */
    public function addPersonInvolved($personInvolvedArr){
        global $db;
        if(is_array($personInvolvedArr) && count($personInvolvedArr) > 0) {
             if(isset($_SESSION['incidentid']) && isset($personInvolvedArr['person_involved_id']) && isset($personInvolvedArr['person_involved_type'])){
                if($this->personInvolvedExist($personInvolvedArr['person_involved_id'], $personInvolvedArr['person_involved_type'])){
                    return 'The person is already added.';
                }else{
                    $sql = "INSERT INTO tbl_incident_person_involved (incident_id, person_involved_id, person_involved_type, injured_or_ill, part_affected, effect_person, disease_description, hours_expected)"
                            ." VALUES (". $db->sqs($_SESSION['incidentid'])." , ". $db->sqs($personInvolvedArr['person_involved_id'])." , ". $db->sqs($personInvolvedArr['person_involved_type'])." , ". $db->sqs($personInvolvedArr['injured_or_ill'])." , ". $db->sqs($personInvolvedArr['part_affected'])." , ". $db->sqs($personInvolvedArr['effect_person'])." , ". $db->sqs($personInvolvedArr['disease_description'])." , ". $db->sqs($personInvolvedArr['hours_expected'])." )";
                    $response = $db->query($sql); 
                return $response;
                } 
             }else{
                return "Required information is missing";
             }
        }else{
            return FALSE;
        }
    } 
    /**
     * Method to edit person involved
     */
    public function editPersonInvolved($personInvolvedArr){
        global $db;
        
        if(is_array($personInvolvedArr) && count($personInvolvedArr)>0){
            if(isset($personInvolvedArr['id']) && isset($_SESSION['incidentid'])){
                $sql = "UPDATE tbl_incident_person_involved SET person_involved_id =".$db->sqs($personInvolvedArr['personinvolvedid'])." , injured_or_ill =".$db->sqs($personInvolvedArr['injured']).", part_affected =".$db->sqs($personInvolvedArr['partsaffected']).", effect_person =".$db->sqs($personInvolvedArr['effectonperson'])." , disease_description = ".$db->sqs($personInvolvedArr['diseasedescription']).", hours_expected = ".$db->sqs($personInvolvedArr['hoursexpected'])." modified_by = ".$db->sqs($_SESSION['user_id']).", WHERE incident_id = ".$db->sqs($_SESSION['incidentid']);
                $result = $db->query($sql);
                return $result;                 
            } else {
                return 'Required information missing.';
            }
        }  
    }
    /**
     *Method to get all person involved 
     */
    public function getPersonInvolved($id){
        global $db;
        $sql = "SELECT * FROM tbl_incident_person_involved WHERE id =" . $db->sqs($id);
        $response = $db->getRow($sql);
        if($response){
           return $response; 
        }else{
           return false; 
        }
    }
    /**
     *Method to get all person involved 
     */
    public function getAllPersonInvolved($incidentid) {
        global $db;
        $sql = "SELECT * FROM tbl_incident_person_involved WHERE incident_id =" . $db->sqs($incidentid);
        $response = $db->getAll($sql);
        if($response){
           return $response; 
        }else{
           return false; 
        }
    }
    /**
     *Method to get all person involved 
     */
    public function PersonInvolvedDetails($incidentid, $personInvolvedType) {
        global $db;
        
        switch($personInvolvedType){
            case 1:
            $sql = "SELECT tbl_user_data.*
                           FROM tbl_incident
                           INNER JOIN tbl_incident_person_involved ON tbl_incident_person_involved.incident_id = tbl_incident.id
                           INNER JOIN tbl_users ON tbl_incident_person_involved.person_involved_id = tbl_users.id
                           INNER JOIN tbl_user_data ON tbl_user_data.user_id = tbl_users.id
                           INNER JOIN tbl_user_type_fields ON tbl_user_data.field_id = tbl_user_type_fields.id 
                           WHERE tbl_incident_person_involved.person_involved_type =" .$db->sqs($personInvolvedType). " AND tbl_incident.id =" . $db->sqs($incidentid);
            break;
            case 2 :
            $sql = "SELECT tbl_incident_person_involved.*,
                           tbl_client_company.client_id,
                           tbl_client_company.company_name,
                           tbl_client_company.contact_person,
                           tbl_client_company.email,
                           tbl_client_company.company_contact_number
                           FROM tbl_incident
                           INNER JOIN tbl_incident_person_involved ON tbl_incident_person_involved.incident_id = tbl_incident.id
                           INNER JOIN tbl_client_company ON tbl_incident_person_involved.person_involved_id = tbl_client_company.id
                           WHERE tbl_incident_person_involved.person_involved_type =" .$db->sqs($personInvolvedType). " AND tbl_incident.id =" . $db->sqs($incidentid);   
            break;
            case 3 :
            $sql = "SELECT tbl_incident_person_involved.*,
                           tbl_service_providers.id,
                           tbl_service_providers.provider_name,
                           tbl_service_providers.contact_person,
                           tbl_service_providers.provider_email,
                           tbl_service_providers.cell_number
                           FROM tbl_incident
                           INNER JOIN tbl_incident_person_involved ON tbl_incident_person_involved.incident_id = tbl_incident.id
                           INNER JOIN tbl_service_providers ON tbl_incident_person_involved.person_involved_id = tbl_service_providers.id
                           WHERE tbl_incident_person_involved.person_involved_type =" .$db->sqs($personInvolvedType). " AND tbl_incident.id =" . $db->sqs($incidentid);   
            break;
            case 4 :
            $sql = "SELECT tbl_incident_person_involved.*,
                           tbl_suppliers.id,
                           tbl_suppliers.supplier_name,
                           tbl_suppliers.contact_person,
                           tbl_suppliers.supplier_email,
                           tbl_suppliers.cell_number
                           FROM tbl_incident
                           INNER JOIN tbl_incident_person_involved ON tbl_incident_person_involved.incident_id = tbl_incident.id
                           INNER JOIN tbl_suppliers ON tbl_incident_person_involved.person_involved_id = tbl_suppliers.id
                           WHERE tbl_incident_person_involved.person_involved_type =" .$db->sqs($personInvolvedType). " AND tbl_incident.id =" . $db->sqs($incidentid);   
            break;
            case 5 :
            $sql = "SELECT tbl_incident_person_involved.*,
                           tbl_visitors.id,
                           tbl_visitors.first_name,
                           tbl_visitors.lastname,
                           tbl_visitors.company_name,
                           tbl_visitors.email,
                           tbl_visitors.contact_number
                           FROM tbl_incident
                           INNER JOIN tbl_incident_person_involved ON tbl_incident_person_involved.incident_id = tbl_incident.id
                           INNER JOIN tbl_visitors ON tbl_incident_person_involved.person_involved_id = tbl_visitors.id
                           WHERE tbl_incident_person_involved.person_involved_type =" .$db->sqs($personInvolvedType). " AND tbl_incident.id =" . $db->sqs($incidentid);   
            break;
            case 6 :
            $sql = "SELECT tbl_incident_person_involved.*,
                           tbl_publicperson.id,
                           tbl_publicperson.first_name,
                           tbl_publicperson.lastname,
                           tbl_publicperson.company_name,
                           tbl_publicperson.email,
                           tbl_publicperson.contact_number
                           FROM tbl_incident
                           INNER JOIN tbl_incident_person_involved ON tbl_incident_person_involved.incident_id = tbl_incident.id
                           INNER JOIN tbl_publicperson ON tbl_incident_person_involved.person_involved_id = tbl_publicperson.id
                           WHERE tbl_incident_person_involved.person_involved_type =" .$db->sqs($personInvolvedType). " AND tbl_incident.id =" . $db->sqs($incidentid);   
            break;
        }
        $response = $db->getAll($sql);
        if($response){
            return $response;
        } else {
            return false;
        }             
    }
    
    /**
     * Method to check if the person involved exist
     */
    public function personInvolvedExist($personinvolvedid, $personinvolvedtype){
        global $db;
        
        $sql = "SELECT id FROM tbl_incident_person_involved WHERE person_involved_id =" . $db->sqs($personinvolvedid).", person_involved_type =" . $db->sqs($personinvolvedtype).", incident_id = ".$db->sqs($_SESSION['incidentid']);
        $response = $db->getOne($sql);
        if(is_array($response) && count($response)>0){
            return TRUE;
        } else {
            return FALSE;
        }
    }
     /**
     * Method to deactivate a Person Involved
     * 
     * @param int $id
     * @return bool
     */
    public function deactivatePersonInvolved($id)
    {
        global $db;
        
        if(!is_null($id)){
            $sql = "UPDATE tbl_incident_person_involved SET is_active = 0 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return TRUE;
            } else {
                return FALSE;
            }            
        }
    }        
    /**
     * Method to activate a Person Involved
     * 
     * @param int $id
     * @return bool
     */
    public function activatePersonInvolved($id)
    {
        global $db;
        
        if(!is_null($id)){
            $sql = "UPDATE tbl_incident_person_involved SET is_active = 1 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return TRUE;
            } else {
                return FALSE;
            }            
        }
    }
}
