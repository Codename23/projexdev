<?php

/**
 * Class containing all document related methods
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

//Include docx class
include_once ROOT.'/classes/DOCx.php';

class documents_class
{

    /**
     * 
     */
    var $objDocX;
    
    /**
     * Method to initiate the class
     * 
     * @access public
     */
    public function __construct()
    {
        $this->objDocX = new DOCx();
    }
    
    
/******* WARRENS code begins here *******/
    
    /**
     * Method to search viewable approved documents
     * 
     * @access public
     * @return mixed
     */
    public function searchApprovedDocuments($groupsArr = array(), $originatorArr = array())
    {
        global $db;
        
        $sql = "SELECT * FROM tbl_documents WHERE is_approved = 1";
        
        if(count($groupsArr)>0){
            $sql .= " AND group_id IN (". implode(',', $groupsArr).")";
        }
        
        if(count($originatorArr)>0){
            $sql .= " AND originator_id IN (". implode(',', $originatorArr).")";
        }

        $result = $db->getAll($sql);
        
        if($result){
            return $result;
        } else {
            return false;
        }        
    }

/**
     * Method to search viewable unapproved documents
     * 
     * @access public
     * @return mixed
     */
    public function searchUnapprovedDocuments($groupsArr = array(), $originatorArr = array())
    {
        global $db;
        
        $sql = "SELECT * FROM tbl_documents WHERE is_approved = 0";
        
        if(count($groupsArr)>0){
            $sql .= " AND group_id IN (". implode(',', $groupsArr).")";
        }
        
        if(count($originatorArr)>0){
            $sql .= " AND originator_id IN (". implode(',', $originatorArr).")";
        }

        $result = $db->getAll($sql);
        
        if($result){
            return $result;
        } else {
            return false;
        }        
    }
    
    /**
     * Method to add a new document
     * 
     * @access public
     * @return mixed 
     */
    public function addDocument($documentDetails)
    {
        global $db;
        global $objFolders;
        
        if(isset($documentDetails['type_id'])){
            //Get the folder location
            //$folderId = 2;
            //$folderInfo = $objFolders->getFolderInfo($folderId);

            $folderInfo = $objFolders->getFolderDirectoryPath("Documents",$documentDetails['group_id'],$documentDetails['type_id']);
            $folderId = $folderInfo['id'];

            $folderLocation = FOLDER_DIRECTORY_ROOT.$folderInfo['folder_path'];

            $filename = $documentDetails['document_description'].'_'.$documentDetails['document_revision_no'].'.docx';
            $target_file = $folderLocation.'/'.$filename;
            $uploadOk = 1;
            //Check if file already exists
            if (file_exists($target_file)) {
                $uploadOk = 0;
                return "File already exists.";
            }
            //Check file size
            if ($_FILES['uploadDoc']["size"] > 50000000) {
                $uploadOk = 0;
                return "File is too large.";
            }


            //Move file to folder
            if (move_uploaded_file($_FILES['uploadDoc']["tmp_name"], $target_file)) {
                
                //Add the document to the db
                $sql = "INSERT INTO tbl_documents (group_id, folder_id, doc_type_id, originator_id, branches, document_number, system_number, filename, description, current_revision_number, revision_frequency, next_revision_date, created_by) VALUES
                    (" . $db->sqs($documentDetails['group_id']) ." , ". $db->sqs($folderId) .",". $db->sqs($documentDetails['type_id']) .",". $db->sqs($documentDetails['document_originator_id']) .",". $db->sqs($documentDetails['branches']) .",". 
                        $db->sqs($documentDetails['document_no']) .",". $db->sqs($documentDetails['document_system_no']). ",". $db->sqs($filename) .",". 
                        $db->sqs($documentDetails['document_description']) .",". $db->sqs($documentDetails['document_revision_no']) .",". $db->sqs($documentDetails['document_revision_frequency']) .",". 
                        $db->sqs($documentDetails['documentNextRevisionDate']) .",". $db->sqs($_SESSION['user_id']).")";


                $result = $db->query($sql);
                
                
                if($result){
                    $response = $db->insertId();
                    //Add approved by users to approval table
                    if(!is_array($documentDetails['approvedBy'])){
                        $approvals = explode(',', $documentDetails['approvedBy']);
                        if(count($approvals)>0){
                            foreach($approvals as $approve){
                                $apprDets = array();
                                $apprDets['employee_id'] = $approve;
                                $apprDets['doc_id'] = $response;
                                $apprDets['is_approved'] = 1;
                                $apprDets['due_date'] = NULL;
                                $apprDets['approval_comments'] = NULL;
                                $apprDets['is_amendments'] = 0;
                                $apprDets['document_version'] = $documentDetails['document_revision_no'];
                                
                                $apprRes = $this->addDocumentApproval($apprDets);

                            }
                        }
                    }
                    return $response;
                } else {
                    move_uploaded_file($target_file, FOLDER_DIRECTORY_ROOT);
                    
                    return "There was an error uploading your document.";
                }
                
            } else {
                return "There was an error uploading your document.";
            }

            } else {
            return 'Required information missing.';
        }
        
    }

    /**
     * Method to add a new document type
     * 
     * @access public
     */
    public function addDocumentType($typeDetails) 
    {
        global $db;
        
        
    }

    /**
     * Method to add a new document link
     * 
     * @access public
     */
    public function addLink($linkDetails) 
    {
        global $db;
        
        
    }
    
    /**
     * Method to add a new document link
     * 
     * @access public
     */
    public function removeLink($linkDetails) 
    {
        global $db;
        
        
    }
    
/******* SUNNYS code begins here *******/
    /**
     * Method to add document approval
     * @return array $documentDetsArr
     * @access public
     * @return boolean
     */
    public function addDocumentApproval($documentDetsArr)
    {
        global $db;
        if(isset($documentDetsArr['doc_id']) && isset($documentDetsArr['employee_id'])){
            if($this->userExists($documentDetsArr['doc_id'], $documentDetsArr['document_version'], $documentDetsArr['employee_id'])){
                return 'The user is already added.';   
            }else{    
                $sql = "INSERT INTO tbl_document_approvals (doc_id, employee_id, due_date, document_version, is_approved, approval_comments, is_amendments, created_by) VALUES
                        (" . $db->sqs($documentDetsArr['doc_id']) ." , ". $db->sqs($documentDetsArr['employee_id']) .",". $db->sqs($documentDetsArr['due_date']) .",". $db->sqs($documentDetsArr['document_version']) .",". $db->sqs($documentDetsArr['is_approved']). ",". $db->sqs($documentDetsArr['approval_comments']) .",". $db->sqs($documentDetsArr['is_amendments']) .",". $db->sqs($_SESSION['user_id']).")";
                
                $response = $db->query($sql);

                if($response){
                    return "Your approval user is added successfully";
                }else{
                    return false;
                }
            }
        }else{
            return 'All required information';
        }
    }
    /**
     * Method to edit document approval
     * @return array $documentDetsArr
     * @access public
     * @return boolean
     */
    public function editDocumentApproval($documentDetsArr)
    {
        global $db;
        if(isset($documentDetsArr['doc_id']) && isset($documentDetsArr['employee_id'])){
            $sql = "UPDATE tbl_document_approvals SET doc_id = " . $db->sqs($documentDetsArr['doc_id']) .", 
                                                      employee_id = ". $db->sqs($documentDetsArr['employee_id']) .", 
                                                      due_date = ". $db->sqs($documentDetsArr['due_date']) .", 
                                                      document_version = ". $db->sqs($documentDetsArr['document_version']) .", 
                                                      is_approved = ". $db->sqs($documentDetsArr['is_approved']). ", 
                                                      approval_comments = ". $db->sqs($documentDetsArr['approval_comments']) .", 
                                                      is_amendments = ". $db->sqs($documentDetsArr['is_amendments']) .", 
                                                      modified_by = ". $db->sqs($_SESSION['user_id']) ."
                    WHERE id =".$db->sqs($documentDetsArr['id']);
            $response = $db->query($sql);
            if($response){
                return true;
            }else{
                return false;
            }
        }else{
            return "All required information";
        }
    }
    /**
     * Method to approve a document
     * @return array $documentDetsArr
     * @access public
     * @return boolean
     */
    public function approveDocument($documentDetsArr)
    {
        global $db;
        if(isset($documentDetsArr['id'])){
            if($documentDetsArr['approval'] == 0){
                $sql = "UPDATE tbl_document_approvals SET is_approved = '1', 
                                                      approval_comments = ". $db->sqs($documentDetsArr['approval_comments']) .", 
                                                      is_amendments = ". $db->sqs($documentDetsArr['is_amendments']) .", 
                                                      modified_by = ". $db->sqs($_SESSION['user_id']) ."
                    WHERE id =".$db->sqs($documentDetsArr['id']);
            }else{
                $sql = "UPDATE tbl_document_approvals SET is_approved = '0', 
                                                      approval_comments = ". $db->sqs($documentDetsArr['approval_comments']) .",
                                                      modified_by = ". $db->sqs($_SESSION['user_id']) ."
                                                      WHERE id =".$db->sqs($documentDetsArr['id']);
            }
            $response = $db->query($sql);
            if($response){
                foreach($documentDetsArr['email'] as $userEmail){
                    $this->sendEmailNotifications($userEmail,'Document email notification', $documentDetsArr['approval_comments']);
                }
                return "Your document is approved and relevent users are notified";
            }else{
                return false;
            }
        }else{
            return "All required information";
        }
    }
    /**
     * Method to send email to users
     * @global type $db
     * @param type $documentDetsArr
     * @return boolean|string
     */
    public function sendEmailNotifications($email, $subject, $message)
    {
        if(isset($email) && isset($subject) && isset($message)){
            $headers = 'From: webmaster@example.com' . "\r\n" .'Reply-To: webmaster@example.com' . "\r\n" .'X-Mailer: PHP/' . phpversion();
            mail($email, $subject, $message , $headers);
            
            return "Email sent successfully";
        }
    }
    /**
     * Method to delete document approval
     * 
     * @return array $documentDetsArr
     * @return int $id
     * @access public
     * @return boolean
     */
    public function deleteApproval($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "UPDATE tbl_document_approvals SET is_active='0' WHERE id = " . $db->sqs($id);
            $response = $db->query($sql);
            if($response){
                return "The user is successfully deleted";
            }else{
                return false;  
            }
        }
    }
    /**
     * Method to get all list of approved document
     * @return array $documentDetsArr
     * @access public
     * @return boolean
     */
    public function getAllDocumentAprroval($documentId)
    {
        global $db;
        $sql = "SELECT tbl_users.id,
                       tbl_users.firstname,
                       tbl_users.lastname,
                       tbl_document_approvals.due_date,
                       tbl_document_approvals.doc_id,
                       tbl_departments.department_name,
                       tbl_user_data.field_data,
                       tbl_occupations.name,
                       tbl_document_approvals.is_approved,
                       tbl_document_approvals.employee_id,
                       tbl_document_approvals.doc_id,
                       tbl_document_approvals.id as approval_id
                       FROM tbl_document_approvals 
                       INNER JOIN tbl_users ON tbl_document_approvals.employee_id = tbl_users.id
                       INNER JOIN tbl_department_occupations ON tbl_department_occupations.users_id = tbl_users.id
                       INNER JOIN tbl_departments ON tbl_departments.id  = tbl_department_occupations.department_id
                       INNER JOIN tbl_occupations ON tbl_department_occupations.occupation_id  = tbl_occupations.id
                       INNER JOIN tbl_user_data ON tbl_user_data.user_id = tbl_users.id
                       WHERE tbl_user_data.field_id = 10 AND tbl_document_approvals.doc_id =". $db->sqs($documentId);
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
    /**
     * Method to get all people who approve document
     * @param int $documentId
     * @access public
     * @return boolean
     */
    public function approvedBy($documentId)
    {
        global $db;
        $sql = "SELECT tbl_users.id,
                       tbl_users.firstname,
                       tbl_users.lastname
                       FROM tbl_document_approvals 
                       INNER JOIN tbl_users ON tbl_document_approvals.employee_id = tbl_users.id
                       INNER JOIN tbl_user_data ON tbl_user_data.user_id = tbl_users.id
                       WHERE tbl_document_approvals.is_approved = 1 AND tbl_document_approvals.doc_id =". $db->sqs($documentId) ." 
                       GROUP BY tbl_users.id";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
    /**
     * Method to get one document approval
     * @access public
     * @return boolean
     */
    public function getDocumentApproval($userid)
    {
        global $db;
        $sql = "SELECT tbl_document_approvals.*,
                       tbl_documents.*,
                       tbl_document_type.type_name,
                       tbl_document_type.type_description,
                       tbl_sheqteam_groups.sheqteam_name,
                       tbl_sheqteam_groups.sheqteam_description
                       FROM tbl_document_approvals 
                       INNER JOIN tbl_documents ON tbl_documents.id = tbl_document_approvals.doc_id
                       INNER JOIN tbl_document_type ON tbl_document_type.id = tbl_documents.doc_type_id
                       INNER JOIN tbl_sheqteam_groups ON tbl_sheqteam_groups.id = tbl_documents.group_id
                       WHERE tbl_document_approvals.is_approved = 0 AND tbl_document_approvals.employee_id =" .$db->sqs($userid);
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
    /**
     * Method to get all approved document
     * @param type $userid
     * @return boolean
     */
    public function getApprovedDocument($userid)
    {
        global $db;
        $sql = "SELECT tbl_document_approvals.*,
                       tbl_documents.*,
                       tbl_document_type.type_name,
                       tbl_document_type.type_description,
                       tbl_sheqteam_groups.sheqteam_name,
                       tbl_sheqteam_groups.sheqteam_description
                       FROM tbl_document_approvals 
                       INNER JOIN tbl_documents ON tbl_documents.id = tbl_document_approvals.doc_id
                       INNER JOIN tbl_document_type ON tbl_document_type.id = tbl_documents.doc_type_id
                       INNER JOIN tbl_sheqteam_groups ON tbl_sheqteam_groups.id = tbl_documents.group_id
                       WHERE tbl_document_approvals.is_approved = 1 AND tbl_document_approvals.employee_id =" .$db->sqs($userid);
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
    
    /**
     * Method to get all document types
     * @return boolean $response
     */
    public function getDocumentTypes()
    {
        global $db;
        $sql = "SELECT * FROM tbl_document_type";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }



    /**
     * Method to get document details
     * @param int $id Document id
     * @access public
     */
    public function getDocument($id)
    {
        global $db;
        $sql = "SELECT tbl_documents.id,
                       tbl_documents.description,
                       tbl_documents.folder_id,
                       tbl_documents.filename,
                       tbl_documents.document_number,
                       tbl_documents.system_number,
                       tbl_documents.current_revision_number,
                       tbl_documents.revision_frequency,
                       tbl_documents.next_revision_date,
                       tbl_document_type.type_name,
                       tbl_document_type.type_description,
                       tbl_sheqteam_groups.sheqteam_name,
                       tbl_sheqteam_groups.sheqteam_description,
                       tbl_users.firstname,
                       tbl_users.lastname
                       FROM tbl_documents 
                       INNER JOIN tbl_document_type ON tbl_document_type.id = tbl_documents.doc_type_id
                       INNER JOIN tbl_sheqteam_groups ON tbl_sheqteam_groups.id = tbl_documents.group_id
                       INNER JOIN tbl_users ON tbl_users.id = tbl_documents.originator_id
                       WHERE tbl_documents.id =" . $db->sqs($id);

        $response = $db->getRow($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
     /**
      * Function to check if a user exists on the same document and version
      * @param 
      * @return bool True if exist else false
      * @access public
      */
    public function userExists($documentId, $documentVersion, $employeeId)
    {
        global $db;

        $sql = "SELECT * FROM tbl_document_approvals WHERE doc_id = ".$db->sqs($documentId) ." AND document_version = ". $db->sqs($documentVersion) ." AND employee_id = ". $db->sqs($employeeId);
        $response = $db->getOne($sql);
        if(is_array($response) && count($response)>0){
            return true;
        } else {
            return false;
        }
    }
/******* BRIANS code begins here *******/
    
     /**
     * Method to archive the current version of a document
     * 
     * @access public
     */
    public function saveRevision($documentDetails) 
    {
        global $db;
        global $objFolders;
        global $objDocument;
        
        if(isset($documentDetails['doc_id'])){
            
            //Get the folder location
            $folderId = 2;
            $newDetails = $documentDetails;
            $documentDetails = $objDocument->getDocument($newDetails['doc_id']);
            $folderInfo = $objFolders->getFolderInfo($folderId);
            $folderLocation = FOLDER_DIRECTORY_ROOT.$folderInfo['folder_path']; 
            $rev = $documentDetails['document_revision_no']++;
            $filename = $documentDetails['document_description'].'_'.$rev.'.docx';
            $oldTarget = $folderLocation.'/'.$documentDetails['document_description'].'_'.$documentDetails['document_revision_no'].'.docx';
            $target_file = $folderLocation.'/'.$filename;
            $archiveLocation = $folderLocation.'/archive/'.$documentDetails['document_description'].'_'.$documentDetails['document_revision_no'].'.docx';
            $uploadOk = 1;
            //Check if file already exists
            if (file_exists($target_file)) {
                $uploadOk = 0;
                return "File already exists.";
            }
            //Check file size
            if ($_FILES['reuploadDoc']["size"] > 50000000) {
                $uploadOk = 0;
                return "File is too large.";
            }
            
            //Move file to folder
            if (move_uploaded_file($_FILES['reuploadDoc']["tmp_name"], $target_file)) {
                move_uploaded_file($oldTarget, $archiveLocation);
                //Update the document details
                $sql = "UPDATE tbl_documents SET originator_id = ". $db->sqs($newDetails['document_originator_id']) .",
                    revision_frequency = ". $db->sqs($newDetails['document_revision_frequency']) .",
                    next_revision_date = ". $db->sqs($newDetails['documentNextRevisionDate']) .",
                    modified_by = ". $db->sqs($_SESSION['user_id']) ."
                    WHERE id = ". $db->sqs($newDetails['docId']);

                $result = $db->query($sql);
                $response = $db->insertId();
                
                //Add old doc dets to archive
                $sql = "INSERT INTO tbl_document_version ( doc_id, version_number, document_link, created_by ) VALUES 
                    (". $db->sqs($newDetails['docId']) .", ". $db->sqs($documentDetails['document_revision_no']) .", ". $db->sqs($archiveLocation) .", ". $db->sqs($_SESSION['user_id']) .")";
                //var_dump($sql);
                //exit();
                $result = $db->query($sql);
                
                if($result){
                    
                    //Add approved by users to approval table
                    if(!is_array($newDetails['approvedBy'])){
                        $approvals = explode(',', $newDetails['approvedBy']);
                        if(count($approvals)>0){
                            foreach($approvals as $approve){
                                $apprDets = array();
                                $apprDets['employee_id'] = $approve;
                                $apprDets['doc_id'] = $response;
                                $apprDets['is_approved'] = 1;
                                $apprDets['due_date'] = NULL;
                                $apprDets['approval_comments'] = NULL;
                                $apprDets['is_amendments'] = 0;
                                $apprDets['document_version'] = $rev;
                                
                                $apprRes = $this->addDocumentApproval($apprDets);
                            }
                        }
                    }
                    return $response;
                } else {
                    move_uploaded_file($target_file, FOLDER_DIRECTORY_ROOT);
                    
                    return "There was an error uploading your document.";
                }
                
            } else {
                return "There was an error uploading your document.";
            }

            } else {
            return 'Required information missing.';
        }

        
    }
    
    /**
     * Method to get archived documents
     * 
     * @access public
     */
    public function getArchivedDocuments() 
    {
        global $db;
        
        $sql = "SELECT tbl_document_version.id, version_number, document_link, tbl_document_version.date_created, document_number, current_revision_number "
                . "FROM tbl_document_version JOIN tbl_documents ON tbl_document_version.doc_id = tbl_documents.id";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }        
    }


    /**
     * Method to get document type name
     * @return boolean $response
     */
    public function getDocumentTypeName($typeId = null)
    {
        global $db;
        $sql = "SELECT type_name   FROM tbl_document_type WHERE id=".$db->sqs($typeId);
        $response = $db->getOne($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
    
    /**
     * Method to retrieve available document codes
     * 
     * @access public
     * return mixed
     */
    public function getDocumentCodes()
    {
        global $db;
        
        $sql = "SELECT tbl_document_code_category.category_name, tbl_document_codes.code_name, tbl_document_codes.code_text, tbl_document_codes.code_table, tbl_document_codes.code_field FROM tbl_document_codes JOIN tbl_document_code_category "
                . "ON tbl_document_codes.category_id = tbl_document_code_category.id ORDER BY tbl_document_codes.category_id";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }        
    }

    /**
     * Method to save an appointment document
     */

    public function saveAppointmentDocument($appointmentData, $appointmentFileLocation)
    {
        global $objFolders;
        global $objEmployee;
        
        if(is_array($appointmentData) && isset($appointmentData['appointmentDocumentId']) && isset($appointmentData['employeeId']) && isset($appointmentData['appointmentDate']) && isset($appointmentData['appointmentDuration']) && isset($appointmentData['appointmentName'])){
            //Get document details
            $documentDetails = $this->getDocument($appointmentData['appointmentDocumentId']);
            //Get user details
            $employeeIDNumber = $objEmployee->getEmployeeIDNumber($appointmentData['employeeId']);
            $employeeName = $objEmployee->getEmployeeName($appointmentData['employeeId']);
            //Create marker and replacement text array
            $appointmentCodesArray = array('##AppointmentPerson##'=>$employeeName['field_data'], '##AppointmentIDNumber##'=>$employeeIDNumber['field_data'], '##LegalAppointment##'=>$appointmentData['appointmentName'], '##DateAppointed##'=>$appointmentData['appointmentDate'], '##AppointmentDuration##'=>$appointmentData['appointmentDuration']);            
            //Get document folder details in order to build document location
            $folderDetails = $objFolders->getFolderInfo($documentDetails['folder_id']);
            $folderLocation = FOLDER_DIRECTORY_ROOT.$folderDetails['folder_path']; 
            $filename = $documentDetails['document_description'].'_'.$documentDetails['document_revision_no'].'.docx';
            $fileLocation = $folderLocation.'/'.$filename;
            //Load the document 
            $this->objDocX->load($fileLocation);
            //Replace each marker with details
            $this->objDocX->setValues($appointmentCodesArray);
            //Folder to move document to
            //$newFileLocation = FOLDER_DIRECTORY_ROOT;
            if($this->objDocX->save($appointmentFileLocation)){
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }
}
