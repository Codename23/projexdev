<?php

class improvements
{
    public function __construct() 
            {        
    }  
    
    public static function viewImprovements()
    {
        global $db;
        $sql = "select tbl_improvement.id tbl_id, tbl_improvement.date_created,
        (select tbl_sheqteam_groups.sheqteam_name from tbl_sheqteam_groups 
        where tbl_sheqteam_groups.id = tbl_improvement.group_id) as gp,
        tbl_improvement.improvement_type,tbl_improvement.improvement_description,
        concat(tbl_users.firstname,' ',tbl_users.lastname) as emp
        from tbl_improvement
        left join tbl_users
        on tbl_improvement.responsible_person = tbl_users.id
        where tbl_improvement.is_active = 1 and requested = 0 and branch_id = ".$db->sqs($_SESSION['branch_id']);        
        return $db->getAll($sql);
    }    
    public static function addImprovement($improvement_data = array())
    {
         global $db;
            $addStatus = false;
            $addInvestigationSQL = "insert into tbl_improvement"
                . "(branch_id,company_id,group_id,action_link_id,action_type,why_link_id,improvement_type,improvement_description,responsible_person,sheqf_approval,managerial_approval,is_active,requested,created_by)"                
                . "VALUES(
                ".$db->sqs($_SESSION['branch_id']).","
                 .$db->sqs($_SESSION['company_id']).","
                .$db->sqs($improvement_data['group_id']).","
                .$db->sqs($improvement_data['action_link_id']).","
                .$db->sqs($improvement_data['action_type']).","    
                .$db->sqs($improvement_data['why_link_id']).","
                .$db->sqs($improvement_data['improvement_type']).","
                .$db->sqs($improvement_data['improvement_description']).","
                .$db->sqs($improvement_data['responsible_person']).","
                .$db->sqs($improvement_data['sheqf_approval']).","
                .$db->sqs($improvement_data['managerial_approval']).","                   
                 ."1,"
                .$db->sqs($improvement_data['requested']).","                   
                .$db->sqs($_SESSION['user_id']).")";        
            $addInvestigationResult = $db->query($addInvestigationSQL);
         if($addInvestigationResult == true)
         {
             $addStatus = true;            
         }         
         return $addStatus;
    }
    
}