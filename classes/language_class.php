<?php

/**
 * Class containing all methods relating to the multilingual interface and its language strings
 *
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class language
{

    /**
     * Method to initiate the class
     *
     * @access public
     */
    public function __construct()
    {

    }

    /**
     * Method to retrieve a language string using its code
     *
     * @global type $db
     * @param string $languageCode
     */
    public function languageText($languageCode, $langName = DEFAULT_LANG)
    {
        //Access global db object
        global $db;

        $langTextResult = "SELECT  tbl_language_data.language_text
                            FROM  tbl_language_data
                            INNER JOIN tbl_languages ON tbl_languages.id = tbl_language_data.language_id
                            WHERE tbl_language_data.code=" . $db->sqs($languageCode) . "
                            AND tbl_languages.name  =" . $db->sqs($langName) . "
                            AND tbl_languages.is_active = 1";

        $langResults = $db->getOne($langTextResult);

        if ($langResults) {
            return $langResults;
        } else {
            return false;
        }

    }

    /**
     * Method to add language from file to database.
     * @param null $langFile
     */
    public function addLanguageFromFile()
    {
        global $db;

        $date = new DateTime();
        $dateModified = $date->getTimestamp();

        //@TODO: assign session
        $createdBy = 0;//$_SESSION["user_id"];

        $dir = LANGUAGES;
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file == '.' || $file == '..')
                        continue;

                    if($this->checkExtension($dir . $file)){
                        $lang = ucfirst(basename($file, ".lang"));

                        $lang_id = $this->checkLanguageExist($lang);
                        if(!$lang_id){

                            $addLanguageSql = "INSERT INTO tbl_languages VALUES(name,is_active)VALUES(".$db->sqs($lang).",1)";
                            $addLanguageResults = $db->query($addLanguageSql);

                            if($addLanguageResults){

                                $lang_id = $db->insertId();
                                $myfile = fopen($dir . $file, "r") or die("Unable to open file!");

                                while (!feof($myfile)) {

                                    $data = explode("|",fgets($myfile));

                                    $addCodeSql = "INSERT INTO tbl_language_data(
                                                                              language_id,
                                                                              code,
                                                                              description,
                                                                              language_text,
                                                                              date_created,
                                                                              modified_by,
                                                                              date_modified
                                                                              )
                                                                 VALUES(
                                                                 ".$db->sqs($lang_id).",
                                                                 ".$db->sqs($data[0]).",
                                                                 ".$db->sqs($data[1]).",
                                                                 ".$db->sqs($data[2]).",
                                                                 ".$db->sqs($dateModified).",
                                                                 ".$db->sqs($createdBy).",
                                                                 ".$db->sqs($dateModified)." 
                                                                 )";
                                    $addCodeResults = $db->query($addCodeSql);
                                }

                                fclose($myfile);

                            }else{
                                return false;
                            }

                        }else{//language exists, add/update  missing language text

                            $myfile = fopen($dir . $file, "r") or die("Unable to open file!");

                            while (!feof($myfile)) {
                                $data = explode("|",fgets($myfile));


                                if($this->checkCodeExist($data[0],$lang_id)){

                                    $editLangCodeSql = "UPDATE tbl_language_data SET    
                                                                                code = ".$db->sqs($data[0]).",  
                                                                                description	= ".$db->sqs($data[1]).", 
                                                                                language_text = ".$db->sqs($data[2]).",   
                                                                                modified_by	= ".$db->sqs($createdBy).",
                                                                                date_modified = ".$db->sqs($dateModified)."
                                                
                                                            WHERE code=".$db->sqs($data[0])."
                                                            AND language_id=".$db->sqs($lang_id);


                                    $editLangCodeResults = $db->query($editLangCodeSql);



                                }else{
                                    $addCodeSql = "INSERT INTO tbl_language_data(
                                                                              language_id,
                                                                              code,
                                                                              description,
                                                                              language_text,
                                                                              date_created,
                                                                              modified_by,
                                                                              date_modified
                                                                              )
                                                                 VALUES(
                                                                 ".$db->sqs($lang_id).",
                                                                 ".$db->sqs($data[0]).",
                                                                 ".$db->sqs($data[1]).",
                                                                 ".$db->sqs($data[2]).",
                                                                 ".$db->sqs($dateModified).",
                                                                 ".$db->sqs($createdBy).",
                                                                 ".$db->sqs($dateModified)." 
                                                                 )";

                                    $addCodeResults = $db->query($addCodeSql);
                                }
                            }
                            fclose($myfile);
                        }

                    }

                }
                closedir($dh);
            }
        }

    }


    /**
     * Method to check if language exists ie english,french
     * @param $langName
     * @return bool|int|String
     */
    private function checkLanguageExist($langName){
        global $db;
        $langExistResult = "SELECT  id
                            FROM  tbl_languages
                            WHERE name =".$db->sqs($langName);
        $langResults = $db->getOne($langExistResult);

        if($langResults){
            return $langResults;
        }else{
            return false;
        }
    }

    /**
     * Method to check if code exists
     * @param $langCode
     * @param $lang_id
     * @return bool
     */
    private function checkCodeExist($langCode,$lang_id){
        global $db;
        $codeExistResult = "SELECT  code
                            FROM  tbl_language_data
                            WHERE code =".$db->sqs($langCode)."
                            AND language_id =".$db->sqs($lang_id);
        $codeResults = $db->getOne($codeExistResult);

        if($codeResults){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Method to check language file extension
     * @param $filePath
     * @return bool
     */
    private function checkExtension($filePath)
    {
        $info = pathinfo($filePath);
        if ($info["extension"] == "lang") {
            return true;
        }else{
            return false;
        }
    }

}
