<?php
/**
 * Service class containing all service methods
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class criticalpoints
{


    /**
     * Initialise service class
     * providers constructor.
     */
    public $hasCreatedRisk = false;
    
    function __construct()
    {

    }


    
    
    
    /////////////Emmanuel van der Westhuizen////////////////
    public function getAllControlSampling()
    {
        global $db;
        $sql = "select * from tbl_control_types where company_id = ".$db->sqs($_SESSION['company_id']);
        $result = $db->getAll($sql);
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    public function addType($group,$type)
    {
       global $db;
       $sql = "INSERT INTO tbl_control_types(group_id,type,company_id,branch_id,is_active,created_by)
                                VALUES(
                                                  " . $db->sqs($group) . ",
                                                  " . $db->sqs($type) . ",    
                                                  " . $db->sqs($_SESSION['company_id']) . ",
                                                  " . $db->sqs($_SESSION['branch_id']) . ",                                                  
                                                  " . $db->sqs(1) . ",
                                                  " . $db->sqs($_SESSION['user_id']) . ")";
        $result = $db->query($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    
    public function getAllControlPointTypes()
    {
        global $db;
        $sql = "select tbl_sheqteam_groups.sheqteam_name,type,
        tbl_control_types.date_created from tbl_control_types
        inner join tbl_sheqteam_groups
        on tbl_control_types.group_id = tbl_sheqteam_groups.id
        where tbl_control_types.is_active = 1 and tbl_control_types.company_id = ".$db->sqs($_SESSION['company_id']);
        $result = $db->getAll($sql);
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }     
}
