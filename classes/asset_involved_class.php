<?php
/**
 * Class containing all methods providing asset functionality
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

class assetinvolved
{

    /**
     * Method to initiate the class
     * 
     * @access public
     */
    public function __construct() 
    {
      
    }
    
    /**
     * Method to add asset involved
     * @param array $incidentArr
     */
    public function addAssetInvolved($assetInvolvedArr){
        global $db;
        if (is_array($assetInvolvedArr) && count($assetInvolvedArr) > 0) {
            if(isset($assetInvolvedArr['asset_id']) && isset($_SESSION['incidentid'])){
                $sql = "INSERT INTO tbl_incident_assets (asset_id, incident_id, involvement_type, effect_on_asset, incident_cause, downtime_expected, created_by )
                        VALUES (". $db->sqs($assetInvolvedArr['asset_id'])." , ".$db->sqs($_SESSION['incidentid'])." , ".$db->sqs($assetInvolvedArr['involvementType'])." , ".$db->sqs($assetInvolvedArr['effectonAsset'])." , ".$db->sqs($assetInvolvedArr['incidentCause'])." , ".$db->sqs($assetInvolvedArr['downtowntime'])." , " . $db->sqs($_SESSION['user_id'])." )";
                return  $response = $db->query($sql); 
            }else{
                return "Required information is missing";
            }   
        }else{
            return FALSE;
        }     
    }
    /**
     * Method to edit asset involved
     */
    public function editAssetInvolved($assetInvolvedArr){
        global $db;
        if(is_array($assetInvolvedArr) && count($assetInvolvedArr)>0){
            if(isset($_SESSION['incidentid']) && isset($assetInvolvedArr['assetid'])){
                $sql = "UPDATE tbl_incident_assets SET involvement_type =".$db->sqs($assetInvolvedArr['involvementType'])." , effect_on_asset =".$db->sqs($assetInvolvedArr['effectonAsset']).", incident_cause =".$db->sqs($assetInvolvedArr['incidentCause']).", downtime_expected = " . $db->sqs($assetInvolvedArr['downtowntime']) .", modified_by = ".$db->sqs($_SESSION['user_id'])." WHERE id = ".$db->sqs($assetInvolvedArr['assetinvolvedid']);
                $result = $db->query($sql);
                return $result;                   
            } else {
                return 'Required information missing.';
            }
        }        
    }
    /*
     * Method to get asset involved
     */
    public function getAssetInvolved($id){
        global $db;
        if(isset($id)){
            $sql = "SELECT tbl_assets.id,
                           tbl_assets.name,
                           tbl_assets.description,
                           tbl_assets.company_asset_number,
                           tbl_departments.department_name,
                           tbl_asset_category.name as category_name,
                           tbl_asset_type.name AS assetType,
                           tbl_incident_assets.involvement_type,
                           tbl_incident_assets.effect_on_asset,
                           tbl_incident_assets.incident_cause,
                           tbl_incident_assets.downtime_expected,
                           tbl_incident_assets.date_modified
                           FROM tbl_assets
                           INNER JOIN tbl_incident_assets ON tbl_incident_assets.asset_id = tbl_assets.id
                           INNER JOIN tbl_departments ON tbl_assets.department_id = tbl_departments.id
                           INNER JOIN tbl_asset_category ON tbl_asset_category.id = tbl_assets.categorytype_id
                           INNER JOIN tbl_asset_type ON tbl_asset_type.id = tbl_assets.resourcetype_id
                           INNER JOIN tbl_incident ON tbl_incident_assets.incident_id = tbl_incident.id
                           WHERE tbl_incident.id =".$db->sqs($id);
            $response = $db->getAll($sql);
            return $response;  
        }
    }
    
    /**
     * Method to get asset involved
     */
    public function getAllAssetInvolved($incidentNumber){
        global $db;
        $sql = "SELECT tbl_assets.name,
                       tbl_assets.description,
                       tbl_assets.company_asset_number
                       FROM tbl_incident
                       INNER JOIN tbl_incident_assets ON tbl_incident_assets.incident_id = tbl_incident.id
                       INNER JOIN tbl_assets ON tbl_incident_assets.asset_id = tbl_assets.id
                       WHERE tbl_incident.id =".$db->sqs($incidentNumber);
        $response = $db->getAll($sql);
        return $response;
    }
   
}
