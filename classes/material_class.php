<?php

/**
 * Class containing all methods providing material functionality
 * @package Sheqonline
 * @author Emmanuel van der Westhuizen <emmanuel@innovatorshill.co.za>
 * @copyright (c) 2017, Innovators Hill
 * @license 
 */
class material_class {

    public function __construct() 
    {
        
    }
    public function getAllMaterials()
    {
        global $db;
            $sql = "SELECT tbl_material.id,tbl_material_category.category,tbl_material_types.type,description,inventory_no,department_use_id,"
                    . "manufacture_name FROM tbl_material "
                    . "inner join tbl_material_category "
                    . "on tbl_material.category_id = tbl_material_category.id "
                    . "inner join tbl_material_types "
                    . "on tbl_material.type_id = tbl_material_types.id "
                    . "WHERE tbl_material_category.is_active = 1 and tbl_material_types.is_active = 1 and tbl_material.is_active = 1 and tbl_material.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
    public function getAllMaterialsInfoById($id)
    {
        global $db;
            $sql = "SELECT tbl_material.id,tbl_material_category.category,tbl_material_types.type,description,inventory_no,department_use_id,"
                    . "manufacture_name FROM tbl_material "
                    . "inner join tbl_material_category "
                    . "on tbl_material.category_id = tbl_material_category.id "
                    . "inner join tbl_material_types "
                    . "on tbl_material.type_id = tbl_material_types.id "
                    . "WHERE tbl_material_category.is_active = 1 and tbl_material_types.is_active = 1 and tbl_material.is_active = 1 and tbl_material.id = ".$db->sqs($id)." and tbl_material.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
    public function addMaterial($materialData = array())
    {
        global $db;   
        $response = array();
        
        if(is_array($materialData))
        {                 
                $sql = "INSERT INTO tbl_material (category_id, type_id, description, inventory_no, "
                        . "department_use_id, manufacture_name, branch_id, company_id, created_by) "
                        . " VALUES (". $db->sqs($materialData['category_id'])." , "
                        . $db->sqs($materialData['type_id'])." , "
                        . $db->sqs($materialData['description'])." , "
                        . "". $db->sqs($materialData['inventory_no'])." , "
                        . "". $db->sqs($materialData['department_use_id'])." , "
                        . "". $db->sqs($materialData['manufacture_name'])." , "
                        . "". $db->sqs($_SESSION['branch_id'])." , "
                        . "". $db->sqs($_SESSION['company_id'])." , "
                        . "" .$db->sqs($_SESSION['user_id']) ." )";
                $response = $db->query($sql);                
                if($response){
                    return $response;
                }else{
                    return false;
                }  
        } 
        else
        {
           return "Required information is missing";
        }
     }
     
    public function getAllMaterialsViewUsage($id)
    {
        global $db;
            $sql = "SELECT * from tbl_material_usage where tbl_material_usage.is_active = 1 and tbl_material_usage.material_id = ".$db->sqs($id);
           // echo $sql;
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
     
     public function addMaterialUsage($materialData = array())
    {
        global $db;   
        $response = array();        
        if(is_array($materialData))
        {                 
                $sql = "INSERT INTO tbl_material_usage (material_id, usage_total_measurement, usage_units_pm, minimum_total_measurement, "
                        . "minimum_units, suppliers, supplier_delivery_days, supplier_cost_per_unit_inc_vat,branch_id,company_id, created_by) "
                        . " VALUES (". $db->sqs($materialData['material_id'])." , "
                        . $db->sqs($materialData['usage_total_measurement'])." , "
                        . $db->sqs($materialData['usage_units_pm'])." , "
                        . "". $db->sqs($materialData['minimum_total_measurement'])." , "
                        . "". $db->sqs($materialData['minimum_units'])." , "
                        . "". $db->sqs($materialData['suppliers'])." , "
                        . "". $db->sqs($materialData['supplier_delivery_days'])." , "
                        . "". $db->sqs($materialData['supplier_cost_per_unit_inc_vat'])." , "
                        . "". $db->sqs($_SESSION['branch_id'])." , "
                        . "". $db->sqs($_SESSION['company_id'])." , "
                        . "" .$db->sqs($_SESSION['user_id']) ." )";
                $response = $db->query($sql);                
                if($response){
                    return $response;
                }else{
                    return false;
                }  
        } 
        else
        {
           return "Required information is missing";
        }
     }
    /**
     * Material Category Section
     */
     public static function getMaterialCategories()
     {
         global $db;    
        $sql = "SELECT  * FROM tbl_material_category WHERE is_active = 1 and company_id = ".$db->sqs($_SESSION['company_id']);
        $response = $db->getAll($sql);                
        if($response){
            return $response;
        }else{
            return false;
        }  
     }
     public static function addMaterialCategory($category)
     {
         global $db;    
        $sql = "INSERT INTO tbl_material_category (category,branch_id, company_id, created_by) "
                . " VALUES (". $db->sqs($category)." , 
                ". $db->sqs($_SESSION['branch_id'])." , "
                . "". $db->sqs($_SESSION['company_id'])." , "
                . "" .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);                
        if($response){
            return $response;
        }else{
            return false;
        }  
     }
     public static function editMaterialCategory($category,$id)
     {
         global $db;    
        $sql = "UPDATE tbl_material_category SET category = ". $db->sqs($category)." WHERE company_id = ".$db->sqs($_SESSION['company_id'])." and id = ". $db->sqs($id);
        $response = $db->query($sql);                
        if($response){
            return $response;
        }else{
            return false;
        }  
     }
     //**
     //Start of material Types
     public static function getMaterialTypes()
     {
         global $db;    
        $sql = "SELECT  * FROM tbl_material_types WHERE is_active = 1 and company_id = ".$db->sqs($_SESSION['company_id']);
        $response = $db->getAll($sql);                
        if($response){
            return $response;
        }else{
            return false;
        }  
     }
     public static function addMaterialTypes($type)
     {
         global $db;    
        $sql = "INSERT INTO tbl_material_types (type,branch_id, company_id, created_by) "
                . " VALUES (". $db->sqs($type)." , 
                ". $db->sqs($_SESSION['branch_id'])." , "
                . "". $db->sqs($_SESSION['company_id'])." , "
                . "" .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);                
        if($response){
            return $response;
        }else{
            return false;
        }  
     }
     public static function editMaterialTypes($type,$id)
     {
         global $db;    
        $sql = "UPDATE tbl_material_types SET type = ". $db->sqs($type)." WHERE company_id = ".$db->sqs($_SESSION['company_id'])." and id = ". $db->sqs($id);
        $response = $db->query($sql);                
        if($response){
            return $response;
        }else{
            return false;
        }  
     }
     
     /********
      * 
      * End of Material Types
      * 
      * ***********/
     
     
    public function addAssetMaterial()
    {
        global $db;
        $sql = "INSERT INTO tbl_asset_material(type, created_by) VALUES (2, ". $db->sqs($_SESSION['user_id']) . ")";
        $response = $db->query($sql);
        if($response){
            return $db->insertId();
        }else{
            return false;
        }     
    }
    /**
     * Method to add department material belong to
     * @access public
     * @global $db
     * @param int $materialid
     * @param string $departmentid
     * @return boolean
     */
    public function addMaterialBydepartment($materialid, $departmentid)
    {
        global $db;
        if(isset($materialid) && isset($departmentid)){
            $sql = "INSERT INTO tbl_department_material (department_id, material_id, branch_id, company_id ) VALUES (". $db->sqs($departmentid) . " , ". $db->sqs($materialid) . " , ". $db->sqs($_SESSION['branch_id']) . " , ". $db->sqs($_SESSION['company_id']) . ")";
            $response = $db->query($sql);
            if($response){
                return $response;
            }else{
                return false;
            }   
        }else{
            return "Required information is missing";
        }  
    }
    /**
     * Method to edit material
     * @access public
     * @global $db
     * @param array $materialDetailsArr
     * @return boolean
     */
    public function editMaterial($materialDetailsArr)
    {
        global $db;
        if(is_array($materialDetailsArr)){
            if(isset($materialDetailsArr['category']) 
                && isset($materialDetailsArr['type'])
                && isset($materialDetailsArr['description'])
                && isset($materialDetailsArr['department_use'])
                && isset($materialDetailsArr['department_store'])){
                
                $sql = "UPDATE tbl_material SET 
                        category_id = ". $db->sqs($materialDetailsArr['category_id'])." ,
                        type_id = ". $db->sqs($materialDetailsArr['type_id'])." ,
                        description = ". $db->sqs($materialDetailsArr['description'])." ,
                        department_use_id = ". $db->sqs($materialDetailsArr['department_use_id'])." ,
                        department_store_id = ". $db->sqs($materialDetailsArr['department_store_id'])." ,
                        manufacture_name = ". $db->sqs($materialDetailsArr['manufacture_name'])." ,
                        supplier_id = ". $db->sqs($materialDetailsArr['supplier_id'])." ,
                        cost = ". $db->sqs($materialDetailsArr['cost'])." ,
                        cost_unit = ". $db->sqs($materialDetailsArr['cost_unit'])." ,
                        average_usage =". $db->sqs($materialDetailsArr['average_usage'])." ,
                        average_usage_unit =". $db->sqs($materialDetailsArr['average_usage_unit'])." ,
                        stock_level = ". $db->sqs($materialDetailsArr['stock_level'])." ,
                        stock_level_unit =". $db->sqs($materialDetailsArr['stock_level_unit'])." ,
                        modified_by = ". $db->sqs($_SESSION['user_id'])."
                        WHERE id = ". $db->sqs($materialDetailsArr['id']);
                
                $response = $db->query($sql);
                if($response){
                    return $response;
                }else{
                    return false;
                }
            } else {
                return "Required information is missing";
            }
        }
    }
    /**
     * Method to activate material
     * @access public
     * @global $db
     * @param int $id
     * @return boolean
     */
    public function activateMaterial($id) 
    {
        global $db;
        if (!is_null($id)) {
            $sql = "UPDATE tbl_material SET is_active = 1 WHERE id = " . $db->sqs($id);
            $response = $db->query($sql);
            if($response) {
                return true;
            } else {
                return false;
            }
        }
    }
    /**
     * Method to deactivate or delete material
     * @access public
     * @global $db
     * @param int $id
     * @return boolean
     */
    public function deactivateMaterial($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "UPDATE tbl_material SET is_active = 0 WHERE id = " . $db->sqs($id);
            $response = $db->query($sql);
            if($response) {
                return true;
            } else {
                return false;
            }
        }
    }
    /**
     * Method to get one material
     * @access public
     * @global $db
     * @param int $id
     * @return boolean
     */
    public function getMaterial($id)
    {
        global $db;
        if (!is_null($id)){
            $sql = "SELECT * FROM tbl_material WHERE id = ".$db->sqs($id);
            $response = $db->getRow($sql);
            if($response){
                return $response;
            } else {
                return false;
            }
        }
    }
    /**
     * Method to get all materials
     * @access public
     * @global $db
     * @param boolean $activeOnly
     * @return boolean
     */
    public function getAllMaterial($activeOnly = true)
    {
        global $db;

        $sql = "SELECT * FROM tbl_material ";
        if($activeOnly) {
            $sql .= " WHERE is_active = 1";
        }else{
            $sql .= " WHERE is_active = 0";
        }
  
        $response = $db->getAll($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    }
    /**
     * Method to get all materials units
     * @access public
     * @global $db
     * @param boolean $activeOnly
     * @return boolean
     */
    public function getAllMaterialUnit($activeOnly = true)
    {
        global $db;

        $sql = "SELECT * FROM tbl_material_unit ";
        if($activeOnly) {
            $sql .= " WHERE is_active = 1";
        }else{
            $sql .= " WHERE is_active = 0";
        }
        $response = $db->getAll($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    }
    /**
     * Method to search material
     * @access public
     * @global $db
     * @param type $searchTerm
     * @return boolean
     */
    public function searchMaterial($searchTerm)
    {
        global $db;
        $sql = "SELECT * FROM tbl_material ";
        $response = $db->getall($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    }
}
