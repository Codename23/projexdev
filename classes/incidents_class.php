<?php
/**
 * Class containing all methods providing asset functionality
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

class incident
{

    /**
     * Method to initiate the class
     * 
     * @access public
     */
    public function __construct() 
    {
      
    }
    
    /**
     * Method to add incidents
     * 
     */
    public function addIncident($incidentDetailsArr){
        global $db;
        if (is_array($incidentDetailsArr) && count($incidentDetailsArr) > 0) {
            if(isset($incidentDetailsArr['incidentCategory']) &&
               isset($incidentDetailsArr['incidentType']) &&
               isset($incidentDetailsArr['incidentDepartment'])){
                
                if($_SESSION['incidentid'] == null){
                    $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWX';
                    $charslength = strlen($chars);
                    $randomString = '';

                    for($i = 0; $i < 10; $i++){
                        $randomString .= $chars[rand(0, $charslength - 1)];
                    }

                    $sql = "INSERT INTO tbl_incident (incident_category_id, incident_type_id, department_id, department_manager_id, incident_number, incident_date, shift, reported_person_id, created_by)
                    VALUES (" . $db->sqs($incidentDetailsArr['incidentCategory']). " , " . $db->sqs($incidentDetailsArr['incidentType']). " , " . $db->sqs($incidentDetailsArr['incidentDepartment']). " , " . $db->sqs($incidentDetailsArr['incidentDepartmentManager'])." , ".$db->sqs($randomString)." , ".$db->sqs($incidentDetailsArr['incidentDate']). " , " . $db->sqs($_POST['incidentShift'])." , " . $db->sqs($incidentDetailsArr['incidentReportedPerson']). " , ".$db->sqs($_SESSION['user_id'])." )"; 
                    $response = $db->query($sql); 
                    $_SESSION['incidentid'] = $db->insertId();
                    
                    $sql = "SELECT incident_number FROM tbl_incident WHERE id = ". $db->sqs($db->insertId());
                    $incidentnumber = $db->getRow($sql);
                    
                    if($response){
                        return $incidentnumber;
                    }else{
                        return false;
                    } 
                }else{
                   $this->editIncident($incidentDetailsArr); 
                }
                
            }else{
                return "Required information is missing";
            }   
        }
        
    }
    /**
     * Method to get types based on category id
     * 
     */
    public function getAllIncidentTypesByCategoryId($id){
        global $db;
        
        if(!empty($id)){
            $sql = "SELECT * FROM tbl_asset_type WHERE category_id = " . $db->sqs($id);
            $response = $db->getAll($sql);
            return $response;
        }else{
            return "Required information is missing";
        }
    }
    /**
     * Method to update incidents
     * @param array $incidentArr
     */
    public function editIncident($incidentDetailsArr){
        global $db;
        if (is_array($incidentDetailsArr) && count($incidentDetailsArr) > 0) {
            if(!empty($incidentDetailsArr)){
                    $sql = "UPDATE tbl_incident SET "
                        . "incident_category_id = " . $db->sqs($incidentDetailsArr["incidentCategory"]) .","
                        . "incident_type_id = " . $db->sqs($incidentDetailsArr["incidentType"]) .","
                        . "department_id =" . $db->sqs($incidentDetailsArr["incidentDepartment"]) .","
                        . "department_manager_id = " . $db->sqs($incidentDetailsArr["incidentDepartmentManager"]) .","
                        . "incident_date = " . $db->sqs($incidentDetailsArr["incidentDate"]) .","
                        . "shift =" . $db->sqs($incidentDetailsArr["incidentShift"]) .","
                        . "reported_person_id =" . $db->sqs($incidentDetailsArr["incidentReportedPerson"]) .","
                        . "incident_task =" . $db->sqs($incidentDetailsArr["incidentTask"]) .","
                        . "description = " . $db->sqs($incidentDetailsArr["incidentDescription"]) . ","
                        . "report_authorities =" . $db->sqs($incidentDetailsArr["incidentAuthorities"]) . ","
                        . "investigator_id =" . $db->sqs($incidentDetailsArr["incidentInvestigator"])  . ","
                        . "due_date =" . $db->sqs($incidentDetailsArr["incidentincidentDate"]) . "
                           WHERE id = " . $db->sqs($_SESSION["incidentid"]) . "";
                $response = $db->query($sql);
                if($response){
                    return true;
                }else{
                    return false;
                }
            }
        }
    }
    
    /**
     * Method to get one incident
     * @param int $id
     */
    public function getIncident($id){
        global $db;
        $sql = "SELECT tbl_incident.*,
                       tbl_incident_category.category_name,
                       tbl_incident_type.type_name,
                       tbl_departments.department_name
                       FROM tbl_incident
                       INNER JOIN tbl_departments ON tbl_departments.id = tbl_incident.department_id
                       INNER JOIN tbl_incident_category ON tbl_incident_category.id = tbl_incident.incident_category_id
                       INNER JOIN tbl_incident_type ON tbl_incident_type.id = tbl_incident.incident_type_id
                       WHERE tbl_incident.id = " . $db->sqs($id);
        $response = $db->getRow($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
    
    /**
     * Method to get all incidents
     * @param boolean $activeOnly
     */
    public function getAllIncidents(){
        global $db;
        $sql = "SELECT 
                tbl_incident.*,
                tbl_incident_category.category_name AS incidentCategory,
                tbl_incident_type.type_name AS incidentType,
                tbl_departments.department_name
                FROM tbl_incident 
                INNER JOIN tbl_departments ON tbl_departments.id = tbl_incident.department_id
                INNER JOIN tbl_incident_category ON tbl_incident_category.id = tbl_incident.incident_category_id
                INNER JOIN tbl_incident_type ON tbl_incident_type.id = tbl_incident.incident_type_id
                WHERE tbl_incident.is_active = 1 ORDER BY date_created";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    /**
     * Method to check if the incident is complete
     */
    public function checkIncidentStatus($id){
        global $db;
        
        $sql = "SELECT tbl_incident.id FROM tbl_incident
                       INNER JOIN tbl_incident_person_involved ON tbl_incident_person_involved.incident_id = tbl_incident.id
                       INNER JOIN tbl_incident_assets ON tbl_incident_assets.incident_id = tbl_incident.id
                       INNER JOIN tbl_incident_witness ON tbl_incident_witness.incident_id = tbl_incident.id
                       INNER JOIN tbl_assets_thirdparty ON tbl_assets_thirdparty.incident_id = tbl_incident.id
                       WHERE tbl_incident.id = " . $db->sqs($id);
        $response = $db->getRow($sql);
        if($response){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Method to retrieve incident category
     * 
     */
    public function getIncidentCategory(){
        global $db;
        
        $sql = "SELECT * FROM tbl_incident_category WHERE is_active = 1 ORDER BY date_created ASC";
        $response = $db->getAll($sql);
        return $response;
    }
     /**
     * Method to retrieve incident types
     */
    public function getIncidentTypesByCategoryId($id){
        global $db;

        $sql = "SELECT * FROM tbl_incident_type WHERE category_id = " . $db->sqs($id)." ORDER BY date_created ASC";
        $response = $db->getAll($sql);
        return $response;
    }
    /**
     * Method to retrieve incident types
     */
    public function getAllIncidentTypes(){
        global $db;

        $sql = "SELECT * FROM tbl_incident_type WHERE is_active = 1 ORDER BY date_created ASC";
        $response = $db->getAll($sql);
        return $response;
    }
    /**
     * Method to get incident number
     */
    public function getIncidentNumber(){
        global $db;
        if(isset($_SESSION['incidentid'])){
            $sql = "SELECT incident_number FROM tbl_incident WHERE id = " . $_SESSION['incidentid'];
            $response = $db->getOne($sql);
            return $response;  
        }    
    }
    /**
     * 
     */
    public function showReportedPerson(){
        global $db;
        if(isset($_SESSION['incidentid'])){
            $sql = "SELECT tbl_users.id,
                           tbl_users.firstname, 
                           tbl_users.lastname
                           FROM tbl_users 
                           INNER JOIN tbl_incident ON tbl_incident.reported_person_id = tbl_users.id 
                           WHERE tbl_incident.id = " . $_SESSION['incidentid'];
            $response = $db->getAll($sql);
            return $response;  
        }    
    }
    /**
     * Method to retrieve incident types
     */
    public function getAllAuthorities(){
        global $db;

        $sql = "SELECT * FROM tbl_incident_authorities";
        $response = $db->getAll($sql);
        return $response;
    }
}
