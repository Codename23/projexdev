<?php
/**
 * Service class containing all service methods
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class audits
{


    /**
     * Initialise service class
     * providers constructor.
     */
    function __construct()
    {

    }

    /**
     * Method to retrieve all audit types
     * @param null $branchId
     * @return array|bool
     */
    public function getAllAuditTypes($branchId = null){

        global $db;

        $allProvidersSql = "SELECT * FROM tbl_audit_types WHERE branch_id =".$db->sqs($branchId);

        $getAllProvidersResults = $db->getAll($allProvidersSql);


        if($getAllProvidersResults){

            return $getAllProvidersResults;
        }else{

            return false;
        }

    }




}