<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of non_conformance_class
 *
 * @author Emmanuel van der Westhuizen
 */
class non_conformances
{
    public $get_id = 0;
    public $get_created_date = "";
    public $get_group_name = "";
    public $get_department_name = "";
    public $get_due_date = "";
    public $get_responsible_person = "";
    public $get_person_involved = "";
    public $get_resource_involved = "";
    public $get_non_conformance_details = "";
    public $get_loggedBy = "";
    public $get_status = "";
    public $get_photo = "";
    public $get_investigator = "";
    public $get_investigate_due_date = "";
    public $get_location = "";
    public $get_responsible_person_id = 0;
    public $get_supplier_name = "";
    public $get_signedOffBy = 0;
    public $get_signedOffDate = "";
    public $get_signedOffAction = "";
    public $get_investigationType = "";
    //put your code here
    public function __construct() {
        
    }        
    
   public function viewAllConformance($signedOff = null)
   {
       global $db;       
       //refine query
       $getAllNonConformanceSql = "";
       
       if($signedOff == 1)
       {
            $getAllNonConformanceSql = "select nc.id as nc_id,
            DATE_FORMAT(nc.date_created,'%m/%d/%Y') as nc_date_created,
            DATE_FORMAT(nc.due_date,'%m/%d/%Y') as nc_due_date,
            DATE_FORMAT(nc.signedOffDate,'%m/%d/%Y') as signedOffDate,
            nc.group_field as nc_group_field,nc.Investigate,signedOffAction,
            nc.created_by as nc_created_by,nc.photo_name,nc.resource_involved as nc_resources_involved,person_involved as nc_person_involved,tbl_sheqteam_groups.sheqteam_name as nc_group_field,
            dep.department_name,sp.supplier_name as oc_name,
            nc.non_conformance_details,nc.status,CONCAT(tu.firstname,' ',tu.lastname) as loggedBy,
            (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
            tbl_users where tbl_users.id = nc_person_involved) as responsible,
            (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
            tbl_users where tbl_users.id = signedOffBy) as signedOffBy
            from tbl_non_conformance as nc
            left join tbl_sheqteam_groups
            on nc.group_field = tbl_sheqteam_groups.id
            left join tbl_departments 
            as dep on
            nc.department_field = dep.id      
            left join tbl_suppliers sp on 
            nc.supplier_id = sp.id
            inner join tbl_users tu
            on
            nc.created_by = tu.id
            where nc.branch_id = '{$_SESSION['branch_id']}'
            and nc.is_active = 1 and nc.attended = 1";
       }
       //marked as deleted
       if($signedOff == 2)
       {
            $getAllNonConformanceSql = "select nc.id as nc_id,
            DATE_FORMAT(nc.date_created,'%m/%d/%Y') as nc_date_created,
            DATE_FORMAT(nc.due_date,'%m/%d/%Y') as nc_due_date,
            DATE_FORMAT(nc.signedOffDate,'%m/%d/%Y') as signedOffDate,
            nc.group_field as nc_group_field,nc.Investigate,signedOffAction,in_active_reason,
            nc.created_by as nc_created_by,nc.photo_name,nc.resource_involved as nc_resources_involved,person_involved as nc_person_involved,tbl_sheqteam_groups.sheqteam_name as nc_group_field,
            dep.department_name,sp.supplier_name as oc_name,
            nc.non_conformance_details,nc.status,CONCAT(tu.firstname,' ',tu.lastname) as loggedBy,
            (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
            tbl_users where tbl_users.id = nc_person_involved) as responsible
            from tbl_non_conformance as nc
            left join tbl_sheqteam_groups
            on nc.group_field = tbl_sheqteam_groups.id
            left join tbl_departments 
            as dep on
            nc.department_field = dep.id      
            left join tbl_suppliers sp on 
            nc.supplier_id = sp.id
            inner join tbl_users tu
            on
            nc.created_by = tu.id
            where nc.branch_id = '{$_SESSION['branch_id']}'
            and nc.is_active = 0";
       }
       if($signedOff == null)
       {
            $getAllNonConformanceSql = "select nc.id as nc_id,
            DATE_FORMAT(nc.date_created,'%m/%d/%Y') as nc_date_created,
            DATE_FORMAT(nc.due_date,'%m/%d/%Y') as nc_due_date,
            DATE_FORMAT(nc.signedOffDate,'%m/%d/%Y') as signedOffDate,
            nc.group_field as nc_group_field,nc.Investigate,signedOffAction,
            nc.created_by as nc_created_by,nc.photo_name,nc.resource_involved as nc_resources_involved,person_involved as nc_person_involved,tbl_sheqteam_groups.sheqteam_name as nc_group_field,
            dep.department_name,sp.supplier_name as oc_name,
            nc.non_conformance_details,nc.status,CONCAT(tu.firstname,' ',tu.lastname) as loggedBy,
            (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
            tbl_users where tbl_users.id = nc_person_involved) as responsible,
            (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
            tbl_users where tbl_users.id = signedOffBy) as signedOffBy
            from tbl_non_conformance as nc
            left join tbl_sheqteam_groups
            on nc.group_field = tbl_sheqteam_groups.id
            left join tbl_departments 
            as dep on
            nc.department_field = dep.id      
            left join tbl_suppliers sp on 
            nc.supplier_id = sp.id
            inner join tbl_users tu
            on
            nc.created_by = tu.id
            where nc.branch_id = '{$_SESSION['branch_id']}'
            and nc.is_active = 1 and nc.attended = 0";
       }
      
        //will refine the above query...
        $allNonConformanceResult = $db->getAll($getAllNonConformanceSql);
        if($allNonConformanceResult){
            return $allNonConformanceResult;
        }else{
            return false;
        }
    }
    public static function searchNonConformance($tmp_group,$tmp_dep,$tmp_status,$isHistory = null)
    {
          global $db;       
       //refine query
       $getAllNonConformanceSql = "select nc.id as nc_id,
        DATE_FORMAT(nc.date_created,'%m/%d/%Y') as nc_date_created,
        DATE_FORMAT(nc.due_date,'%m/%d/%Y') as nc_due_date,
        nc.group_field as nc_group_field,nc.Investigate,
        DATE_FORMAT(nc.signedOffDate,'%m/%d/%Y') as signedOffDate,signedOffAction,
        nc.created_by as nc_created_by,nc.photo_name,nc.resource_involved as nc_resources_involved,person_involved as nc_person_involved,tbl_sheqteam_groups.sheqteam_name as nc_group_field,
        dep.department_name,sp.supplier_name as oc_name,
        nc.non_conformance_details,nc.status,CONCAT(tu.firstname,' ',tu.lastname) as loggedBy,
        (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
        tbl_users where tbl_users.id = nc_person_involved) as responsible,
        (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
        tbl_users where tbl_users.id = signedOffBy) as signedOffBy
        from tbl_non_conformance as nc
        left join tbl_sheqteam_groups
        on nc.group_field = tbl_sheqteam_groups.id
        left join tbl_departments 
        as dep on
        nc.department_field = dep.id      
        left join tbl_suppliers sp on 
        nc.supplier_id = sp.id
        inner join tbl_users tu
        on
        nc.created_by = tu.id
        where nc.branch_id = '{$_SESSION['branch_id']}'
        and nc.is_active = 1 ";
        if(!empty($isHistory))
        {
            $getAllNonConformanceSql .= " and nc.attended = 1 ";
        }
        if(empty($isHistory))
        {
            $getAllNonConformanceSql .= " and nc.attended = 0 ";
        }
        if(!empty($tmp_group))
        {
            $select_group = explode(',', $tmp_group);
            $counter = 0;
            foreach($select_group as $goup)
            {            
                $counter++;
                if($counter > 1)
                {
                    $getAllNonConformanceSql .= " or nc.group_field = ".$db->sqs($goup)." ";
                }
                else
                {
                    $getAllNonConformanceSql .= " and nc.group_field = ".$db->sqs($goup)." ";
                }
            }
        }
        if(!empty($tmp_dep))
        {
            $select_dep = explode(',', $tmp_dep);
            $counter = 0;
            foreach($select_dep as $dep)
            {            
                $counter++;
                if($counter > 1)
                {
                    $getAllNonConformanceSql .= " or nc.department_field = ".$db->sqs($dep)." ";
                }
                else
                {
                    $getAllNonConformanceSql .= " and nc.department_field = ".$db->sqs($dep)." ";
                }
            }
        }
        //will refine the above query...
        $allNonConformanceResult = $db->getAll($getAllNonConformanceSql);
        if($allNonConformanceResult)
        {
            return $allNonConformanceResult;
        }
        else
        {
            return false;
        }
    }
    public function addNoConformance($nonConformanceData = array())            
    {
         global $db;
         $addStatus = false;
         $new_date = new DateTime();
         $dateCreated = date_format($new_date, 'Y-m-d H:i:s');
         if(empty($nonConformanceData['uploadPhoto']))
         {
             $nonConformanceData['uploadPhoto'] = "";
         }
         $addncSql = "INSERT INTO tbl_non_conformance( 
                    branch_id,	        
                    company_id,	    
                    due_date,
                    group_field,	    
                    department_field,
                    non_conformance_details,recommended_improvement_field,
                    resource_involved,person_involved,responsible_person,supplier_id,photo_name,
                    Investigate,InvestigateDueDate,attended,is_active,
                    created_by)
            values(
           ".$db->sqs($_SESSION['branch_id']).","
            .$db->sqs($_SESSION['company_id']).","
            .$db->sqs($nonConformanceData['due_date']).","
            .$db->sqs($nonConformanceData['group_field']).","
            .$db->sqs($nonConformanceData['department_field']).","           
            .$db->sqs($nonConformanceData['non_conformance_details']).","                     
            .$db->sqs($nonConformanceData['recommended_improvement_field'])."," 
            .$db->sqs($nonConformanceData['resource_involved']).","                     
            .$db->sqs($nonConformanceData['person_involved']).","
            .$db->sqs($nonConformanceData['responsible_person']).","
            .$db->sqs($nonConformanceData['supplier_id']).","
            .$db->sqs($nonConformanceData['uploadPhoto']).","
            .$db->sqs($nonConformanceData['Investigate']).","
            .$db->sqs($nonConformanceData['InvestigateDueDate']).","
             ."0,1,".$db->sqs($_SESSION['user_id']).")";
        // echo $addncSql;
         $addNonConformanceResult = $db->query($addncSql);
         if($addNonConformanceResult == true)
         {
             $this->get_id = $db->insertId();
             $addStatus = true;            
         }         
         return $addStatus;
    }
    public static function viewDetailsById($id)
    {
         global $db;       
       //refine query
       $getAllNonConformanceSql = "select nc.id as nc_id,
        DATE_FORMAT(nc.date_created,'%m/%d/%Y') as nc_date_created,
        DATE_FORMAT(nc.due_date,'%m/%d/%Y') as nc_due_date,
         DATE_FORMAT(nc.signedOffDate,'%m/%d/%Y') as signedOffDate,
         nc.group_field as nc_group_field,nc.Investigate,nc.signedOffAction,
        recommended_improvement_field,
        nc.created_by as nc_created_by,nc.photo_name,nc.resource_involved as nc_resources_involved,person_involved as nc_person_involved,
        tbl_sheqteam_groups.sheqteam_name as nc_group_field,
        dep.department_name,sp.supplier_name as oc_name,
        nc.non_conformance_details,nc.status,CONCAT(tu.firstname,' ',tu.lastname) as loggedBy,
        (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from tbl_users where tbl_users.id = responsible_person) as responsible,
        (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from tbl_users where tbl_users.id = nc_person_involved) as person_involve,
        (select name from tbl_assets where tbl_assets.id = nc_resources_involved and nc.branch_id = '{$_SESSION['branch_id']}') as resource,
            (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
            tbl_users where tbl_users.id = nc.signedOffBy) as signedOffBy
        from tbl_non_conformance as nc
        left join tbl_sheqteam_groups
        on nc.group_field = tbl_sheqteam_groups.id
        left join tbl_departments 
        as dep on
        nc.department_field = dep.id      
        left join tbl_suppliers sp on 
        nc.supplier_id = sp.id
        inner join tbl_users as tu on        
        nc.created_by = tu.id
        left join tbl_investigations ti on 
        nc.id = ti.investigate_link_id
        where nc.branch_id = '{$_SESSION['branch_id']}'
        and nc.is_active = 1
        and nc.id = ".$db->sqs($id)."";
        
        $allNonConformanceResult = $db->getAll($getAllNonConformanceSql);
        if($allNonConformanceResult)
        {
            return $allNonConformanceResult;
        }
        else
        {
            return false;
        } 
    }
    public static function viewInvestigationDetailsById($id)
    {
         global $db;       
       //refine query
       $getAllNonConformanceSql = "select nc.id as nc_id,
        DATE_FORMAT(nc.date_created,'%m/%d/%Y') as nc_date_created,
        DATE_FORMAT(nc.due_date,'%m/%d/%Y') as nc_due_date,
        DATE_FORMAT(ti.investigator_due_date,'%m/%d/%Y') as nc_invest_due_date,
        ti.investigation_type,investigate_link_id,ti.id inv_id,
        nc.group_field as nc_group_field, nc.created_by as nc_created_by,
        nc.photo_name,nc.resource_involved as nc_resources_involved,responsible_person,person_involved as nc_person_involved,tbl_sheqteam_groups.sheqteam_name as nc_group_field,
        dep.department_name,
        nc.non_conformance_details,nc.status,CONCAT(tu.firstname,' ',tu.lastname) as loggedBy,
        (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from tbl_users where tbl_users.id = responsible_person) as responsible,
        (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from tbl_users where tbl_users.id = nc_person_involved) as person_involve,
        (select name from tbl_assets where tbl_assets.id = nc_resources_involved and nc.branch_id = 1) as resource,
        (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from tbl_users where tbl_users.id = ti.investigator) as investigator_person
        from tbl_non_conformance as nc
        left join tbl_sheqteam_groups
        on nc.group_field = tbl_sheqteam_groups.id
        left join tbl_departments 
        as dep on
        nc.department_field = dep.id      
        inner join tbl_users as tu on        
        nc.created_by = tu.id
        inner join tbl_investigations ti on 
        nc.id = ti.investigate_link_id 
        where nc.branch_id = '{$_SESSION['branch_id']}' and nc.is_active = 1 and nc.id = ".$db->sqs($id)."";
        
        $allNonConformanceResult = $db->getAll($getAllNonConformanceSql);
        if($allNonConformanceResult)
        {
            return $allNonConformanceResult;
        }
        else
        {
            return false;
        } 
    }    
    public static function getNonConformanceDetailsById($id)
    {
         global $db;       
       //refine query
       $getAllNonConformanceSql = "select nc.id as nc_id,DATE_FORMAT(nc.due_date,'%m/%d/%Y') as due_date,
        nc.resource_involved as nc_resources_involved,
        person_involved as nc_person_involved,nc.department_field,        
        nc.non_conformance_details,nc.recommended_improvement_field,nc.status from tbl_non_conformance as nc
        where nc.branch_id = '{$_SESSION['branch_id']}'
        and nc.is_active = 1 and nc.id = ".$db->sqs($id)."";
        $allNonConformanceResult = $db->getAll($getAllNonConformanceSql);
        if($allNonConformanceResult)
        {
            return $allNonConformanceResult;
        }
        else
        {
            return false;
        } 
    }
    
    public static function signedOffNonConformance($action_taken,$id)            
    {
        global $db;
        $sql = "update tbl_non_conformance set signedOff = 1, signedOffDate = NOW(),"
                . "signedOffBy = '{$_SESSION['user_id']}',"
                . "signedOffAction = ".$db->sqs($action_taken).",attended = 1 where id = ".$db->sqs($id);
         $result = $db->query($sql);
         if($result)
         {
             return $result;
         }
         else
         {
             return false;
         }
    }
    public static function hasDeletedNonConformance($id,$reason)
    {
        global $db;
        $sql = " update tbl_non_conformance set is_active = 0, "
                . "in_active_reason = {$db->sqs($reason)} "
                . " where branch_id = {$db->sqs($_SESSION["branch_id"])}"
                . " and id = {$db->sqs($id)}";
         $result = $db->query($sql);
         if($result)
         {
             echo "succeed";
         }
         else
         {
             echo "failed";
         }
    }
    public  static function hasRestoredNonConformance($id)
    {
           global $db;
        $sql = " update tbl_non_conformance set is_active = 1, "
                . "in_active_reason = null "
                . " where branch_id = {$db->sqs($_SESSION["branch_id"])}"
                . " and id = {$db->sqs($id)}";
         $result = $db->query($sql);
         if($result)
         {
             return "succeed";
         }
         else
         {
             return "failed";
         } 
    }
}
