<?php
/**
 * Provider class containing all client methods
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class clients
{


    /**
     * Initialise client class
     * clients constructor.
     */
    function __construct()
    {

    }


    /**
     * @return array|bool|string
     */
    public function getAllCompanyTypes(){

        global $db;

        $allCompanyTypesSql = "SELECT * FROM tbl_client_types";

        $typeResults = $db->getAll($allCompanyTypesSql);

        if($typeResults){

            return $typeResults;
        }else{

            return false;
        }

    }

    /**
     * @return array|bool
     */
    public function getClientContactGender(){

        global $db;

        $allGenderSql = "SELECT * FROM tbl_gender";

        $genderResults = $db->getAll($allGenderSql);

        if($genderResults){

            return $genderResults;
        }else{

            return false;
        }

    }


    /**
     * Method to get all company clients
     * @param null $branchId
     * @return array|bool|string
     */
    public function getAllCompanyClients($branchId = null){

        global $db;

        $allClientsSql = "SELECT    tbl_clients.id as companyId,
                                    tbl_clients.client_type,
                                    tbl_client_company.*,
                                    tbl_client_individual.*,
                                    tbl_client_types.type_name 
                            FROM tbl_clients
                              LEFT JOIN tbl_client_company ON tbl_client_company.client_id = tbl_clients.id
                              LEFT JOIN tbl_client_individual ON tbl_client_individual.client_id = tbl_clients.id
                              INNER JOIN tbl_client_types ON tbl_client_types.id = tbl_clients.client_type
                              
                           WHERE tbl_clients.is_deleted = 0 
                           AND tbl_clients.branch_id =".$db->sqs($branchId);


        $allClientsResults = $db->getAll($allClientsSql);

        if($allClientsResults){

            return $allClientsResults;
        }else{

            return false;
        }

    }

    /**
     * Method to get selected client results
     * @param null $clientId
     * @return array|bool
     */
    public function getClientInfo($clientId = null){

        global $db;

        $selectedClientSql = "SELECT    tbl_clients.id as companyId,
                                    tbl_clients.client_type,
                                    tbl_client_company.*,
                                    tbl_client_individual.*,
                                    tbl_client_types.type_name 
                            FROM tbl_clients
                              LEFT JOIN tbl_client_company ON tbl_client_company.client_id = tbl_clients.id
                              LEFT JOIN tbl_client_individual ON tbl_client_individual.client_id = tbl_clients.id
                              INNER JOIN tbl_client_types ON tbl_client_types.id = tbl_clients.client_type
                              WHERE tbl_clients.id =".$db->sqs($clientId);

        $clientInfoResults = $db->getRow($selectedClientSql);

        if($clientInfoResults){

            return $clientInfoResults;
        }else{
            return false;
        }

    }

    /**
     * Method to add a new client
     * @param array $clientData
     * @return bool
     */
    public function addClient($clientData = array()){

        global $db;

        $addClientSql = "INSERT INTO tbl_clients(
                                                client_type,
                                                branch_id
                                                ) VALUES(
                                                  ".$db->sqs($clientData['companyType']).",
                                                  ".$db->sqs($clientData['branchId'])."
                                                )";


        $addClientResult = $db->query($addClientSql);

        if($addClientResult){
            $success = false;
            $clientId = $db->insertId();
            if($clientData['companyIndividual']== 'company'){

                $success = $this->addCompany($clientData,$clientId);
            }else{
                $success =$this->addIndividual($clientData,$clientId);
            }
            return $success;
        }else{
            return false;
        }
    }

    /**
     * Method to add a new company
     * @param array $companyData
     * @param null $clientId
     * @return bool
     */
    private function addCompany($companyData = array(),$clientId = null){

        global $db;

        $date = new DateTime();
        $dateCreated = date_format($date, 'Y-m-d H:i:s');


        $addCompanySql = "INSERT INTO tbl_client_company(
                                                    client_id,    
                                                    company_name,	    
                                                    pty_number,    
                                                    vat_number,    
                                                    office_number,	    
                                                    contact_person,	    
                                                    company_contact_number,	    
                                                    email,  
                                                    website,   
                                                    company_address,	
                                                    company_town,
                                                    company_province,
                                                    created_by,   
                                                    date_created,	    
                                                    modified_by,    
                                                    date_modified
                                                )VALUES (	    
                                                    ".$db->sqs($clientId).",   
                                                    ".$db->sqs($companyData['companyName']).",
                                                    ".$db->sqs($companyData['ptyNumber']).", 
                                                    ".$db->sqs($companyData['vatNumber']).",  
                                                    ".$db->sqs($companyData['officeNumber']).", 
                                                    ".$db->sqs($companyData['contactPerson']).", 
                                                    ".$db->sqs($companyData['contactNumber']).", 
                                                    ".$db->sqs($companyData['email']).", 
                                                    ".$db->sqs($companyData['website']).", 
                                                    ".$db->sqs($companyData['headOfficeDetails']).", 
                                                    ".$db->sqs($companyData['headOfficeTown']).", 
                                                    ".$db->sqs($companyData['headOfficeProvince']).",
                                                    ".$db->sqs($companyData['createdBy']).",
                                                    ".$db->sqs($dateCreated).",
                                                    ".$db->sqs($companyData['createdBy']).",
                                                    ".$db->sqs($dateCreated)."
                                                    )";

        $addCompanyResult = $db->query($addCompanySql);

        if($addCompanyResult){

            return true;
        }else{
            return false;
        }
    }

    /**
     * Method to add individual client
     * @param array $individualData
     * @param null $clientId
     * @return bool
     */
    private function addIndividual($individualData = array(),$clientId = null){


        global $db;

        $date = new DateTime();
        $dateCreated = date_format($date, 'Y-m-d H:i:s');

        $addIndividualSql = "INSERT INTO tbl_client_individual(
                                                    client_id,	    
                                                    client_name,    
                                                    client_surname,    
                                                    client_gender,   
                                                    client_email,    
                                                    contact_number,	    
                                                    client_address,    
                                                    client_town,   
                                                    province,	    
                                                    created_by,	    
                                                    date_created,	    
                                                    modified_by,    
                                                    date_modified
                                                )VALUES (	    
                                                     ".$db->sqs($clientId).",    
                                                     ".$db->sqs($individualData['individualName']).", 
                                                     ".$db->sqs($individualData['individualSurname']).",    
                                                     ".$db->sqs($individualData['individualGender']) .",
                                                     ".$db->sqs($individualData['individualEmail']) .",
                                                     ".$db->sqs($individualData['individualContactNumber']) .",
                                                     ".$db->sqs($individualData['individualAddress']) .",
                                                     ".$db->sqs($individualData['town']) .",
                                                     ".$db->sqs($individualData['province']) .",
                                                     ".$db->sqs($individualData['createdBy']).",
                                                     ".$db->sqs($dateCreated).",
                                                     ".$db->sqs($individualData['createdBy']).",
                                                     ".$db->sqs($dateCreated)."
                                                    )
                                                    ";
        $addIndividualResult = $db->query($addIndividualSql);
        if($addIndividualResult){

            return true;
        }else{
            return false;
        }
    }

    /**
     * Method to edit client
     * @param array $clientData
     * @return bool
     */
    public function editClient($clientData = array()){

        $success = false;

        if($clientData['companyType'] == 1){

            $success = $this->editCompanyClient($clientData);
        }else{
            $success = $this->editIndividualClient($clientData);
        }

        return $success;
    }

    /**
     * Method to edit company information
     * @param array $companyData
     * @return bool
     */
    private function editCompanyClient($companyData = array()){

        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $editClientInfoSql = "UPDATE tbl_client_company SET    
                                                    company_name = ".$db->sqs($companyData['companyName'])." ,    
                                                    pty_number = ".$db->sqs($companyData['ptyNumber'])." ,  
                                                    vat_number = ".$db->sqs($companyData['vatNumber'])." ,  
                                                    office_number = ".$db->sqs($companyData['officeNumber'])." ,     
                                                    contact_person = ".$db->sqs($companyData['contactPerson'])." ,  
                                                    company_contact_number = ".$db->sqs($companyData['contactNumber'])." ,    
                                                    email = ".$db->sqs($companyData['email'])." ,  
                                                    website = ".$db->sqs($companyData['website'])." ,   
                                                    company_address = ".$db->sqs($companyData['headOfficeDetails'])." ,  
                                                    company_town = ".$db->sqs($companyData['headOfficeTown'])." ,  
                                                    company_province = ".$db->sqs($companyData['headOfficeProvince'])." ,  
                                                    modified_by = ".$db->sqs($companyData['modifiedBy'])." ,  
                                                    date_modified = ".$db->sqs($dateModified)." 
                                  WHERE client_id={$db->sqs($companyData['clientId'])}";

        $editClientInfoResults = $db->query($editClientInfoSql);

        if($editClientInfoResults){
            return true;
        }else{
            return false;
        }
    }


    /**
     * Method to edit individual client data
     * @param array $individualData
     * @return bool
     */
    private function editIndividualClient($individualData = array()){

        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $editClientInfoSql = "UPDATE tbl_client_individual SET 
                                            client_name	 = ".$db->sqs($individualData['individualName'])." ,     
                                            client_surname	= ".$db->sqs($individualData['individualSurname'])." ,        
                                            client_gender = ".$db->sqs($individualData['individualGender'])." ,        
                                            client_email = ".$db->sqs($individualData['individualEmail'])." ,        
                                            contact_number = ".$db->sqs($individualData['individualContactNumber'])." ,        
                                            client_address= ".$db->sqs($individualData['individualAddress'])." ,       
                                            client_town	 = ".$db->sqs($individualData['town'])." ,     
                                            province = ".$db->sqs($individualData['province'])." ,        
                                            modified_by	 = ".$db->sqs($individualData['modifiedBy'])." ,    
                                            date_modified = ".$db->sqs($dateModified)."   
                        WHERE client_id={$db->sqs($individualData['clientId'])}";

        $editClientInfoResults = $db->query($editClientInfoSql);

        if($editClientInfoResults){
            return true;
        }else{
            return false;
        }

    }


    /**
     * Method to delete selected client
     * @param null $clientId
     * @return bool
     */
    public function deleteClient($clientId = null){

        global $db;
        $deleteClientSql = "UPDATE tbl_clients SET 
                                    is_deleted	=  1
                        WHERE id={$db->sqs($clientId)}";

        $deleteClientResults = $db->query($deleteClientSql);

        if($deleteClientResults){
            return true;
        }else{
            return false;
        }

    }
    /**
     * Method to search client
     */
    public function searchClient($companyName = null, $clientFullname){
        global $db;
        
        if(isset($companyName)){
        $sql = "SELECT tbl_clients.id, 
                       tbl_client_company.company_name, 
                       tbl_client_company.contact_person
                       FROM tbl_clients 
                       INNER JOIN tbl_client_company ON tbl_client_company.client_id = tbl_clients.id
                       INNER JOIN tbl_branches ON tbl_clients.branch_id = tbl_branches.id
                       WHERE tbl_client_company.id = " . $db->sqs($companyName) ." AND tbl_client_company.contact_person LIKE '%" .$clientFullname."%'";
        }else{
            $sql = "SELECT tbl_clients.id, 
                           tbl_client_individual.client_name, 
                           tbl_client_individual.client_surname
                           FROM tbl_clients 
                           INNER JOIN tbl_client_individual ON tbl_client_individual.client_id = tbl_clients.id
                           WHERE tbl_client_individual.client_name LIKE '%" .$clientFullname."%'";
        }
        
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
    /**
     * Method 
     */
     public function getCompanyNameClients(){
        global $db;

        $sql = "SELECT id, company_name FROM tbl_client_company";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
}