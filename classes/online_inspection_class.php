<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of process_class
 *
 * @author Emmanuel van der Westhuizen <emmanuel.vd.west@gmail.com>
 */
class online_inspections {
    //put your code here
    public static $getGroup = "";
    public static $getInspectionDescription = "";
    public static $getDetails;
    public static $getFrequency;
    public static $getAssets;
    public static $getMaterials;
    
    public function __construct() {
        
    }
    //Start of Products and Services
    public function viewAllConformance($signedOff = null)
   {
       global $db;       
       //refine query
       $getAllNonConformanceSql = "";
       
       if($signedOff == 1)
       {
            $getAllNonConformanceSql = "select nc.id as nc_id,
            DATE_FORMAT(nc.date_created,'%m/%d/%Y') as nc_date_created,
            DATE_FORMAT(nc.due_date,'%m/%d/%Y') as nc_due_date,
            DATE_FORMAT(nc.signedOffDate,'%m/%d/%Y') as signedOffDate,
            nc.group_field as nc_group_field,nc.Investigate,signedOffAction,
            nc.created_by as nc_created_by,nc.photo_name,nc.resource_involved as nc_resources_involved,person_involved as nc_person_involved,tbl_sheqteam_groups.sheqteam_name as nc_group_field,
            dep.department_name,sp.supplier_name as oc_name,
            nc.non_conformance_details,nc.status,CONCAT(tu.firstname,' ',tu.lastname) as loggedBy,
            (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
            tbl_users where tbl_users.id = nc_person_involved) as responsible,
            (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
            tbl_users where tbl_users.id = signedOffBy) as signedOffBy
            from tbl_non_conformance as nc
            left join tbl_sheqteam_groups
            on nc.group_field = tbl_sheqteam_groups.id
            left join tbl_departments 
            as dep on
            nc.department_field = dep.id      
            left join tbl_suppliers sp on 
            nc.supplier_id = sp.id
            inner join tbl_users tu
            on
            nc.created_by = tu.id
            where nc.branch_id = '{$_SESSION['branch_id']}'
            and nc.is_active = 1 and nc.attended = 1";
       }
       //marked as deleted
       if($signedOff == 2)
       {
            $getAllNonConformanceSql = "select nc.id as nc_id,
            DATE_FORMAT(nc.date_created,'%m/%d/%Y') as nc_date_created,
            DATE_FORMAT(nc.due_date,'%m/%d/%Y') as nc_due_date,
            DATE_FORMAT(nc.signedOffDate,'%m/%d/%Y') as signedOffDate,
            nc.group_field as nc_group_field,nc.Investigate,signedOffAction,in_active_reason,
            nc.created_by as nc_created_by,nc.photo_name,nc.resource_involved as nc_resources_involved,person_involved as nc_person_involved,tbl_sheqteam_groups.sheqteam_name as nc_group_field,
            dep.department_name,sp.supplier_name as oc_name,
            nc.non_conformance_details,nc.status,CONCAT(tu.firstname,' ',tu.lastname) as loggedBy,
            (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
            tbl_users where tbl_users.id = nc_person_involved) as responsible
            from tbl_non_conformance as nc
            left join tbl_sheqteam_groups
            on nc.group_field = tbl_sheqteam_groups.id
            left join tbl_departments 
            as dep on
            nc.department_field = dep.id      
            left join tbl_suppliers sp on 
            nc.supplier_id = sp.id
            inner join tbl_users tu
            on
            nc.created_by = tu.id
            where nc.branch_id = '{$_SESSION['branch_id']}'
            and nc.is_active = 0";
       }
       if($signedOff == null)
       {
            $getAllNonConformanceSql = "select nc.id as nc_id,
            DATE_FORMAT(nc.date_created,'%m/%d/%Y') as nc_date_created,
            DATE_FORMAT(nc.signedOffDate,'%m/%d/%Y') as signedOffDate,
            nc.group_field as nc_group_field,nc.Investigate,signedOffAction,
            nc.created_by as nc_created_by,nc.photo_name,nc.resource_involved as nc_resources_involved,
            tbl_sheqteam_groups.sheqteam_name as nc_group_field,
            dep.department_name,
            nc.non_conformance_details,nc.status,CONCAT(tu.firstname,' ',tu.lastname) as loggedBy,           
            (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
            tbl_users where tbl_users.id = signedOffBy) as signedOffBy
            from tbl_non_conformance as nc
            left join tbl_sheqteam_groups
            on nc.group_field = tbl_sheqteam_groups.id
            left join tbl_departments 
            as dep on
            nc.department_field = dep.id      
            inner join tbl_users tu
            on
            nc.created_by = tu.id
            where nc.branch_id = '{$_SESSION['branch_id']}'
            and nc.is_active = 1 and nc.attended = 0";
       }
      
        //will refine the above query...
     //  echo $getAllNonConformanceSql;
        $allNonConformanceResult = $db->getAll($getAllNonConformanceSql);
        if($allNonConformanceResult){
            return $allNonConformanceResult;
        }else{
            return false;
        }
    }
    public static function getAllInspections()
    {
          global $db;
            $sql = "SELECT tbl_sheqteam_groups.sheqteam_name,tbl_online_inspections.*
from tbl_online_inspections
inner join
tbl_sheqteam_groups
on tbl_online_inspections.group_id = tbl_sheqteam_groups.id
where  tbl_online_inspections.is_active = 1
and inspection_status = 'Available' and tbl_online_inspections.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
    public static function getInspectionDetailsById($inspection_id)
    {
          global $db;
            $sql = "SELECT tbl_sheqteam_groups.sheqteam_name,tbl_online_inspections.*,tbl_online_inspection_plan.setupStatus
from tbl_online_inspection_plan
inner join
tbl_sheqteam_groups
on tbl_online_inspection_plan.group_id = tbl_sheqteam_groups.id
inner join tbl_online_inspections
on tbl_online_inspection_plan.inspection_description = tbl_online_inspections.id
where tbl_online_inspection_plan.is_active = 1
and tbl_online_inspection_plan.id = ".$db->sqs($inspection_id)." and tbl_online_inspection_plan.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else 
            {
                return false;
            }
    }
   public static function getAllInspectionsByGroup($group_id)
    {
          global $db;
            $sql = "SELECT tbl_sheqteam_groups.sheqteam_name,tbl_online_inspections.*
from tbl_online_inspections
inner join
tbl_sheqteam_groups
on tbl_online_inspections.group_id = tbl_sheqteam_groups.id
where  tbl_online_inspections.is_active = 1
and inspection_status = 'Available' and tbl_online_inspections.group_id = ".$db->sqs($group_id)." and tbl_online_inspections.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
    public static function getAllInspectionsList()
    {
          global $db;
            $sql = "SELECT tbl_sheqteam_groups.sheqteam_name,tbl_online_inspections.*
from tbl_online_inspections
inner join
tbl_sheqteam_groups
on tbl_online_inspections.group_id = tbl_sheqteam_groups.id
where  tbl_online_inspections.is_active = 1
and tbl_online_inspections.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
        public static function getAllHistoryInspections()
    {
          global $db;
            $sql = "SELECT tbl_sheqteam_groups.sheqteam_name,tbl_online_inspections.*,tbl_online_inspection_plan.inspectors_id,
                tbl_online_inspection_plan.department,tbl_online_inspection_plan.score
from tbl_online_inspection_plan
inner join
tbl_online_inspections
on tbl_online_inspection_plan.inspection_description = tbl_online_inspections.id
inner join
tbl_sheqteam_groups
on tbl_online_inspection_plan.group_id = tbl_sheqteam_groups.id
where tbl_online_inspection_plan.hasInspected = 1 and tbl_online_inspection_plan.is_active = 1
and tbl_online_inspection_plan.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
    //Start of Products and Services
    public static function getAllUpcomingInspections()
    {
          global $db;
            $sql = "SELECT tbl_sheqteam_groups.sheqteam_name,tbl_online_inspections.*
from tbl_online_inspections
inner join
tbl_sheqteam_groups
on tbl_online_inspections.group_id = tbl_sheqteam_groups.id
where tbl_online_inspections.is_active = 1
and tbl_online_inspections.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
    public static function getInspectionDetails($id)
    {
        global $db;
            $sql = "SELECT tbl_sheqteam_groups.sheqteam_name,tbl_online_inspections.*
from tbl_online_inspections
inner join
tbl_sheqteam_groups
on tbl_online_inspections.group_id = tbl_sheqteam_groups.id
where tbl_online_inspections.is_active = 1
and tbl_online_inspections.id = {$db->sqs($id)} and tbl_online_inspections.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                foreach($response as $item)
                {
                    static::$getGroup = $item["sheqteam_name"];
                    static::$getInspectionDescription = $item["inspection_description"];
                    static::$getDetails = $item["details"];
                    static::$getFrequency = $item["frequency"];
                }
                
            } else {
                return false;
            }
    }
    public static function addOnlineInspection($data)
    {
         global $db;    
        $sql = "INSERT INTO tbl_online_inspections (group_id,branch_id,company_id,"
                . "inspection_description,created_by)"
                . " VALUES (". $db->sqs($data["group_id"])." , "     
                . $db->sqs($_SESSION['branch_id'])." , "
                . "". $db->sqs($_SESSION['company_id'])." , "
                . "". $db->sqs($data['inspection_description'])." , "
                . "" .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);                
        if($response){
           return $db->insertId();
        }else{
            return false;
        }  
    }
    /************
    End of Product and Services
    **********/
    
    /******
     * 
     * START OF PROCESS STOPPERS
     * **********/
    public static function getAllOnlineInspectionPlansUpcoming()
    {
            global $db;
            $sql = "select tbl_sheqteam_groups.sheqteam_name,
            tbl_online_inspections.description_type,
            tbl_online_inspections.inspection_description,
            tbl_departments.department_name,tp.next_inspection_date,
            tbl_online_inspections.id as inspection_id,
            (select concat(tbl_users.firstname,' ',tbl_users.lastname) 
            from tbl_users where tbl_users.id = tp.inspectors_id) as emp
            from tbl_online_inspection_plan as tp
            inner join tbl_online_inspections
            on tp.inspection_description = tbl_online_inspections.id
            inner join tbl_sheqteam_groups
            on tbl_online_inspections.group_id = tbl_sheqteam_groups.id
            inner join tbl_departments
            on tbl_online_inspections.department_id = tbl_departments.id
            where tp.is_active = 1 and tbl_online_inspections.is_active = 1 
            and tbl_online_inspections.details <> null and tp.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
    public static function getAllOnlineInspectionPlansForView()
    {
         global $db;
            $sql = "select tbl_sheqteam_groups.sheqteam_name,tbl_online_inspections.id as online_id, 
tbl_online_inspections.description_type,next_inspection_date, tbl_online_inspections.inspection_description, 
tbl_departments.department_name,tp.hasScheduled,tp.id as tpid,tp.next_inspection_date, tbl_online_inspections.id as inspection_id, tp.hasInspected,
(select concat(tbl_users.firstname,' ',tbl_users.lastname) from tbl_users where tbl_users.id = tp.inspectors_id) as emp 
from tbl_online_inspection_plan as tp inner join tbl_online_inspections on tp.inspection_description = tbl_online_inspections.id 
inner join tbl_sheqteam_groups on tp.group_id = tbl_sheqteam_groups.id inner join tbl_departments on 
tp.department = tbl_departments.id
where tp.is_active = 1 and tbl_online_inspections.is_active = 1 
and tp.company_id = ".$db->sqs($_SESSION["company_id"])." order by tpid asc";
            //echo $sql;
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
    public static function getAllOnlineInspectionPlans()
    {
         global $db;
            $sql = "select tbl_sheqteam_groups.sheqteam_name,tbl_online_inspections.id as online_id, 
tbl_online_inspections.description_type,next_inspection_date, tbl_online_inspections.inspection_description, 
tbl_departments.department_name,tp.id as tpid,tp.next_inspection_date, tbl_online_inspections.id as inspection_id, 
(select concat(tbl_users.firstname,' ',tbl_users.lastname) from tbl_users where tbl_users.id = tp.inspectors_id) as emp 
from tbl_online_inspection_plan as tp inner join tbl_online_inspections on tp.inspection_description = tbl_online_inspections.id 
inner join tbl_sheqteam_groups on tp.group_id = tbl_sheqteam_groups.id inner join tbl_departments on 
tp.department = tbl_departments.id
where tp.hasScheduled = 1 and tp.is_active = 1 and tp.hasInspected = 0 and tbl_online_inspections.is_active = 1 
and tp.company_id = ".$db->sqs($_SESSION["company_id"])." order by tpid asc";
            //echo $sql;
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
   public static function addOnlineInspectionPlan($data)
    {
         global $db;    
       /* $sql = "INSERT INTO tbl_online_inspection_plan (group_id,"
                . "branch_id ,company_id,department, "
                . "inspection_description, "
                . "frequency,"
                . "next_inspection_date,inspectors_id,created_by) "
                . " VALUES (". $db->sqs($data["group_id"])." ,                 
                ". $db->sqs($_SESSION['branch_id'])." , "
                . "". $db->sqs($_SESSION['company_id'])." , "
                . $db->sqs($data["department"])." ,"    
                . $db->sqs($data["description"])." ,"     
                . $db->sqs($data["loadInspectionFrequency"])." ,"    
                . $db->sqs($data["nextInspectionDate"])." ,"   
                . $db->sqs($data["inspector"])." ,"   
                .$db->sqs($_SESSION['user_id']) ." )";*/
        $sql = "INSERT INTO tbl_online_inspection_plan (group_id,"
                . "branch_id ,company_id,department, "
                . "inspection_description, "
                . "frequency,"
                . "created_by)"
                . " VALUES (". $db->sqs($data["group_id"])." ,                 
                ". $db->sqs($_SESSION['branch_id'])." , "
                . "". $db->sqs($_SESSION['company_id'])." , "
                . $db->sqs($data["department"])." ,"    
                . $db->sqs($data["description"])." ,"     
                . $db->sqs($data["loadInspectionFrequency"])." ,"    
                .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);   
       // echo $sql;
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    public static function addPlanOnSchedule($data)
    {
         global $db;
        $sql = "UPDATE tbl_online_inspection_plan SET hasScheduled = 1,ScheduleDate = ".$db->sqs($data["ScheduleDate"]).","
                . "inspectors_id = ".$db->sqs($data["inspectors_id"]). "                                    
                WHERE id = ".$db->sqs($data["plan_id"]);
        $result = $db->query($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }       
    } 
    public static function addISO($data)
    {
             global $db;    
        $sql = "INSERT INTO tbl_online_inspection_leg_iso (inspection_id,"
                . "title ,description, "
                . "branch_id,company_id,created_by) "
                . " VALUES (". $db->sqs($data["inspection_id"])." ,                 
                ". $db->sqs($data['iso_title'])." , "
                . "". $db->sqs($data['iso_description'])." , "
                . $db->sqs($_SESSION["branch_id"])." ,"     
                . $db->sqs($_SESSION["company_id"])." ,"    
                .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);   
        //echo $sql;
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
   public static function getAllISOs($id)
    {
             global $db;    
      $sql = "SELECT * from tbl_online_inspection_leg_iso where inspection_id = ".$db->sqs($id)." and company_id = ".$db->sqs($_SESSION["company_id"]);
        $response = $db->getAll($sql);   
        //echo $sql;
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    public static function addCriticalCategory($data)
    {
        global $db;    
        $sql = "INSERT INTO tbl_online_inspection_category (inspection_id,"
                . "category, "
                . "branch_id,company_id,created_by) "
                . " VALUES (". $db->sqs($data["inspection_id"])." ,                 
                ". $db->sqs($data['category'])." , "
                . $db->sqs($_SESSION["branch_id"])." ,"     
                . $db->sqs($_SESSION["company_id"])." ,"    
                .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);   
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
   public static function loadAssessment($data)
   {
       global $db;
       //create assets record
       $sqlAsset = "INSERT INTO tbl_online_inspection_assets_assessment(inspection_id,asset_id,present,branch_id,company_id,created_by)"
               . "SELECT  ".$db->sqs($data["inspection_plan_id"]).",tbl_online_inspection_assets.asset_id,tbl_online_inspection_assets.present,"
               . "tbl_online_inspection_assets.branch_id,tbl_online_inspection_assets.company_id,tbl_online_inspection_assets.created_by"
               . " FROM tbl_online_inspection_assets"
               . " WHERE tbl_online_inspection_assets.inspection_id = ".$db->sqs($data["inspection_template_id"]);      
        $assetResult = $db->query($sqlAsset);       
        if($assetResult)
        {
               //creates material record             
                $sqlMaterial = "INSERT INTO tbl_online_inspection_materials_assessment(inspection_id,material_id,present,branch_id,company_id,created_by)"
               . "SELECT  ".$db->sqs($data["inspection_plan_id"]).",tbl_online_inspection_materials.material_id,tbl_online_inspection_materials.present,"
               . "tbl_online_inspection_materials.branch_id,tbl_online_inspection_materials.company_id,tbl_online_inspection_materials.created_by"
               . " FROM tbl_online_inspection_materials"
               . " WHERE tbl_online_inspection_materials.inspection_id = ".$db->sqs($data["inspection_template_id"]);      
                $materialResult = $db->query($sqlMaterial);
                if($materialResult)
                {
                    //creates the questions, list                    
                        $sql = "INSERT INTO tbl_online_inspection_questions_assessment(inspection_id,category_id,question,branch_id,company_id,created_by)"
                                . " SELECT  ".$db->sqs($data["inspection_plan_id"]).",tbl_online_inspection_category.category,tbl_online_inspection_questions.question,"
                                . " tbl_online_inspection_questions.branch_id,tbl_online_inspection_questions.company_id,tbl_online_inspection_questions.created_by"
                                . " FROM tbl_online_inspection_questions inner join"
                                . " tbl_online_inspection_category on"
                                . " tbl_online_inspection_questions.category_id = tbl_online_inspection_category.id"
                                . " WHERE tbl_online_inspection_questions.inspection_id = ".$db->sqs($data["inspection_template_id"]);      
                         $result = $db->query($sql);
                         if($result) 
                         {
                             //updates the online_inspection plan
                             $sql_online = "update tbl_online_inspection_plan set setupStatus = 'OK' where id = ".$db->sqs($data["inspection_plan_id"]);
                             $result_update = $db->query($sql_online);                             
                             if($result_update)
                             {
                                 return $result_update;
                             }
                         } 
                         else 
                         {
                             return false;
                         }       
                }else{return false;}
        }else{return false;}
    } 
   public static function getAllCriticalCategories($id)
   {
             global $db;    
      $sql = "SELECT * from tbl_online_inspection_category where inspection_id = ".$db->sqs($id)." and company_id = ".$db->sqs($_SESSION["company_id"]);
        $response = $db->getAll($sql);   
        //echo $sql;
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
   public static function getAllCriticalCategoriesAssessment($id)
   {
             global $db;    
      $sql = "SELECT * from tbl_online_inspection_category where inspection_id = ".$db->sqs($id)." and company_id = ".$db->sqs($_SESSION["company_id"]);
        $response = $db->getAll($sql);   
        //echo $sql;
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    public static function addCriticalQuestion($data)
    {
             global $db;    
        $sql = "INSERT INTO tbl_online_inspection_questions (inspection_id,"
                . "category_id,question,status,branch_id,company_id,created_by)"
                . " VALUES (". $db->sqs($data["inspection_id"])." ,                 
                ". $db->sqs($data['category'])." , "
                . $db->sqs($data['question'])." , "
                . $db->sqs($data['status'])." , "
                . $db->sqs($_SESSION["branch_id"])." ,"     
                . $db->sqs($_SESSION["company_id"])." ,"    
                .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);   
       // echo $sql;
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
   public static function getAllMaterials()
    {
        global $db;
            $sql = "SELECT * FROM tbl_online_inspection_materials WHERE inspection_id = ".$db->sqs($_SESSION["company_id"])."  and tbl_online_inspection_materials.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
   public static function getAllCriticalQuestion($id)
   {
             global $db;    
      $sql = "SELECT * from tbl_online_inspection_questions where inspection_id = ".$db->sqs($id)." and company_id = ".$db->sqs($_SESSION["company_id"]);
        $response = $db->getAll($sql);   
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
     public static function getAllCriticalQuestionAssessment($id)
   {
             global $db;    
      $sql = "SELECT * from tbl_online_inspection_questions_assessment where inspection_id = ".$db->sqs($id)." and company_id = ".$db->sqs($_SESSION["company_id"]);
        $response = $db->getAll($sql);   
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    public static function updateOnlineInspection($data)
    {
             global $db;    
        $sql = "UPDATE tbl_online_inspections SET inspection_status = 'Available',details = ". $db->sqs($data['details']). ",frequency = ". $db->sqs($data['frequency'])."
                where id = ". $db->sqs($data["inspection_id"]). " and company_id = ". $db->sqs($_SESSION['company_id']);      
        //echo $sql;
        $response = $db->query($sql);           
        if($response){
            return $response;
        }else{
            return false;
        }  

    }
    public static function addOnlineInspectionMaterial($data)
    {
        global $db;    
        $sql = "INSERT INTO tbl_online_inspection_materials (inspection_id,"
                . "material_id,present,branch_id,company_id,created_by)"
                . " VALUES (". $db->sqs($data["inspection_id"])." ,                 
                ". $db->sqs($data['material_id'])." , "
                . $db->sqs($data['present'])." , "
                . $db->sqs($_SESSION["branch_id"])." ,"     
                . $db->sqs($_SESSION["company_id"])." ,"    
                .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);   
       // echo $sql;
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    public static function addOnlineInspectionAsset($data)
    {
        global $db;    
        $sql = "INSERT INTO tbl_online_inspection_assets (inspection_id,"
                . "asset_id,present,branch_id,company_id,created_by)"
                . " VALUES (". $db->sqs($data["inspection_id"])." ,                 
                ". $db->sqs($data['asset_id'])." , "
                . $db->sqs($data['present'])." , "
                . $db->sqs($_SESSION["branch_id"])." ,"     
                . $db->sqs($_SESSION["company_id"])." ,"    
                .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);   
        //echo $sql;
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    public static function getAsset($id)
    {
          global $db;    
      $sql = "SELECT tbl_online_inspection_assets.id as assid,tbl_online_inspection_assets.present,tbl_departments.department_name,
                      tbl_asset_category.name as category_name,
                      tbl_asset_type.name as asset_name,
                      tbl_assets.id,tbl_assets.description as as_name,
                      tbl_assets.description,
                      question_answers,
                      manufacture_name,
                      company_asset_number,
                      serial_number,
                      date_purchased,
                      notes,
                      cost,
                      tbl_assets.is_active,
                      tbl_asset_category.name AS categoryType,
                      tbl_asset_type.name AS assetType
                      FROM tbl_online_inspection_assets 
                      INNER JOIN tbl_assets ON tbl_online_inspection_assets.asset_id = tbl_assets.id
                      INNER JOIN tbl_departments on tbl_assets.department_id = tbl_departments.id
                      INNER JOIN tbl_asset_category ON tbl_asset_category.id = tbl_assets.categorytype_id
                      INNER JOIN tbl_asset_type ON tbl_asset_type.id = tbl_assets.resourcetype_id
                      WHERE tbl_online_inspection_assets.inspection_id = ".$db->sqs($id)." and tbl_online_inspection_assets.company_id = ".$db->sqs($_SESSION["company_id"]);
        $response = $db->getAll($sql);   
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
     public static function getAssetAssessments($id)
    {
          global $db;    
      $sql = "SELECT tbl_online_inspection_assets_assessment.id as assid,tbl_online_inspection_assets_assessment.present,tbl_departments.department_name,
                      tbl_asset_category.name as category_name,
                      tbl_asset_type.name as asset_name,
                      tbl_assets.id,tbl_assets.description as as_name,
                      tbl_assets.description,
                      question_answers,
                      manufacture_name,
                      company_asset_number,
                      serial_number,
                      date_purchased,
                      notes,
                      cost,
                      tbl_assets.is_active,
                      tbl_asset_category.name AS categoryType,
                      tbl_asset_type.name AS assetType
                      FROM tbl_online_inspection_assets_assessment 
                      INNER JOIN tbl_assets ON tbl_online_inspection_assets_assessment.asset_id = tbl_assets.id
                      INNER JOIN tbl_departments on tbl_assets.department_id = tbl_departments.id
                      INNER JOIN tbl_asset_category ON tbl_asset_category.id = tbl_assets.categorytype_id
                      INNER JOIN tbl_asset_type ON tbl_asset_type.id = tbl_assets.resourcetype_id
                      WHERE tbl_online_inspection_assets_assessment.inspection_id = ".$db->sqs($id)." and tbl_online_inspection_assets_assessment.company_id = ".$db->sqs($_SESSION["company_id"]);
        $response = $db->getAll($sql);   
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    public static function getMaterials($id)
    {
          global $db;    
      $sql = "SELECT tbl_online_inspection_materials.id as assid,tbl_online_inspection_materials.present,tbl_online_inspection_materials.material_id as mid,tbl_material.id,tbl_material_category.category,tbl_material_types.type,description,inventory_no,department_use_id,"
                    . " manufacture_name FROM tbl_online_inspection_materials "
                    . "inner join tbl_material on  tbl_online_inspection_materials.material_id = tbl_material.id "
                    . "inner join tbl_material_category "
                    . "on tbl_material.category_id = tbl_material_category.id "
                    . "inner join tbl_material_types "
                    . "on tbl_material.type_id = tbl_material_types.id "
                    . "WHERE tbl_material_category.is_active = 1 and tbl_online_inspection_materials.inspection_id = ".$db->sqs($id)." "
              . " and tbl_material_types.is_active = 1 and tbl_material.is_active = 1 and tbl_online_inspection_materials.company_id = ".$db->sqs($_SESSION["company_id"]);      
        $response = $db->getAll($sql);   
        if($response){
            return $response;
        }else{
            return false;
        }  
    }public static function getMaterialsAssessment($id)
    {
          global $db;    
      $sql = "SELECT tbl_online_inspection_materials_assessment.id as assid,tbl_online_inspection_materials_assessment.present,tbl_online_inspection_materials_assessment.material_id as mid,tbl_material.id,tbl_material_category.category,tbl_material_types.type,description,inventory_no,department_use_id,"
                    . " manufacture_name FROM tbl_online_inspection_materials_assessment "
                    . "inner join tbl_material on  tbl_online_inspection_materials_assessment.material_id = tbl_material.id "
                    . "inner join tbl_material_category "
                    . "on tbl_material.category_id = tbl_material_category.id "
                    . "inner join tbl_material_types "
                    . "on tbl_material.type_id = tbl_material_types.id "
                    . "WHERE tbl_material_category.is_active = 1 and tbl_online_inspection_materials_assessment.inspection_id = ".$db->sqs($id)." "
              . " and tbl_material_types.is_active = 1 and tbl_material.is_active = 1 and tbl_online_inspection_materials_assessment.company_id = ".$db->sqs($_SESSION["company_id"]);      
        $response = $db->getAll($sql);   
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    //update assets
    public static function updateInspectionAssets($data)
    {
          global $db; 
          $sql = "UPDATE tbl_online_inspection_assets_assessment SET present =  ".$db->sqs($data["present"]).", modified_by = ".$db->sqs($_SESSION["user_id"])." "
         . " where tbl_online_inspection_assets_assessment.id = ".$db->sqs($data["asset_id"]).""        
          . " and tbl_online_inspection_assets_assessment.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->query($sql);   
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    //update materials
    public static function updateInspectionMaterials($data)
    {
          global $db; 
        $sql = "UPDATE tbl_online_inspection_materials_assessment SET present =  ".$db->sqs($data["present"]).", modified_by = ".$db->sqs($_SESSION["user_id"])." "
         . " where tbl_online_inspection_materials_assessment.id = ".$db->sqs($data["asset_id"]).""        
          . " and tbl_online_inspection_materials_assessment.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->query($sql);   
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    //update all the questions
    public static function updateQuestionsAssessment($data)
    {
          global $db; 
          $sql = "";
          if($data["complained"] == 1)
          {
                $sql = "UPDATE tbl_online_inspection_questions_assessment SET complaint =  ".$db->sqs($data["complained"]).","
                        . " status = 'Compliant',modified_by = ".$db->sqs($_SESSION["user_id"])." where tbl_online_inspection_questions_assessment.id = ".$db->sqs($data["id"]).""
                        . " and tbl_online_inspection_questions_assessment.inspection_id = ".$db->sqs($data["inspection_id"])." and"
                        . " tbl_online_inspection_questions_assessment.company_id = ".$db->sqs($_SESSION["company_id"]);
          }
          if($data["complained"] == 0)
          {
               $sql = "UPDATE tbl_online_inspection_questions_assessment SET complaint =  ".$db->sqs($data["complained"]).",resource_involved = ".$db->sqs($data["resource"]).""
                       .", comments = ".$db->sqs($data["comments"]).", photo = ".$db->sqs($data["photo"]).","
                        . " status = 'Not Compliant',modified_by = ".$db->sqs($_SESSION["user_id"])." where tbl_online_inspection_questions_assessment.id = ".$db->sqs($data["question_id"]).""
                        . " and tbl_online_inspection_questions_assessment.inspection_id = ".$db->sqs($data["inspection_id"])." and"
                        . " tbl_online_inspection_questions_assessment.company_id = ".$db->sqs($_SESSION["company_id"]);
          }
            $response = $db->query($sql);              
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    //update the plan record
    public static function updateInspectionToInspected($data)
    {
          global $db; 
          $sql = "UPDATE tbl_online_inspection_plan SET hasInspected =  1, modified_by = ".$db->sqs($_SESSION["user_id"])." "
          . " where id = ".$db->sqs($data["id"]).""
          . " and  company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->query($sql);   
          // echo $sql;
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    //add static function 
    public static function addInspectionNoConformance($nonConformanceData = array())            
    {
         global $db;
         $addStatus = false;
         $new_date = new DateTime();
         $dateCreated = date_format($new_date, 'Y-m-d H:i:s');
         if(empty($nonConformanceData['photo']))
         {
             $nonConformanceData['photo'] = "";
         }
         $addncSql = "INSERT INTO tbl_non_conformance( 
                    branch_id,	        
                    company_id,	    
                    group_field,	    
                    department_field,
                    non_conformance_details,
                    resource_involved,photo_name,inspection_id,question_id,
                    attended,is_active,
                    created_by)
            values(
           ".$db->sqs($_SESSION['branch_id']).","
            .$db->sqs($_SESSION['company_id']).","
            .$db->sqs($nonConformanceData['group_field']).","
            .$db->sqs($nonConformanceData['department_field']).","           
            .$db->sqs($nonConformanceData['comments']).","                      
            .$db->sqs($nonConformanceData['resource']).","                     
            .$db->sqs($nonConformanceData['photo']).","
            .$db->sqs($nonConformanceData['inspection_id']).","
            .$db->sqs($nonConformanceData['question_id']).","     
             ."0,1,".$db->sqs($_SESSION['user_id']).")";
         $addNonConformanceResult = $db->query($addncSql);
         if($addNonConformanceResult == true)
         {
             $addStatus = true;            
         }         
         return $addStatus;
    }
    /************
    End of Process Stoppers
    **********/
}
