<?php
/**
 * Class containing all methods providing asset functionality
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

class visitor
{
    /**
     * Method to initiate the class
     * 
     * @access public
     */
    public function __construct() 
    {
      
    }
    
    /**
     * Method to add asset involved
     * @param array $incidentArr
     */
    public function addVisitor($visitorDetsArr){
        global $db;
        if (is_array($visitorDetsArr) && count($visitorDetsArr) > 0) {
             if(isset($visitorDetsArr['firstname']) && isset($visitorDetsArr['contact_number'])){
                $sql = "INSERT INTO tbl_visitors (incident_id, first_name, lastname, id_number, contact_number, email, company_name, created_by)
                        VALUES (". $db->sqs($_SESSION['incidentid'])." , ". $db->sqs($visitorDetsArr['firstname'])." , ". $db->sqs($visitorDetsArr['lastname'])." , ". $db->sqs($visitorDetsArr['id_number'])." , ". $db->sqs($visitorDetsArr['contact_number'])." , ". $db->sqs($visitorDetsArr['email'])." , ". $db->sqs($visitorDetsArr['company'])." , ". $db->sqs($_SESSION['user_id'])." )";
                $db->query($sql);
                $response = $db->insertId();
                return $response;   
             }else{
                return "Required information is missing";
             }
        }else{
            return FALSE;
        }
    }
    /**
     * Method to edit asset involved
     */
    public function editVisitor($visitorDetsArr){
        global $db;
        if(is_array($visitorDetsArr) && count($visitorDetsArr)>0){
            if(isset($_SESSION['incidentid']) && isset($visitorDetsArr['description'])){
                $sql = "UPDATE tbl_visitors SET first_name =".$db->sqs($visitorDetsArr['firstname']).",lastname =".$db->sqs($visitorDetsArr['lastname'])." , id_number =".$db->sqs($visitorDetsArr['id_number']).", contact_number =" .$db->sqs($visitorDetsArr['contact_number']).", email = " . $db->sqs($visitorDetsArr['email']) .", company_name = " . $db->sqs($visitorDetsArr['company']) .", modified_by = ".$db->sqs($_SESSION['user_id'])." WHERE id = ".$db->sqs($visitorDetsArr['publicpersonid']);
                $result = $db->query($sql);
                return $result;                   
            } else {
                return 'Required information missing.';
            }
        }        
    }
    /*
     * Method to get asset involved
     */
    public function getVisitor($id){
        global $db;
        if(isset($id)){
            $sql = "SELECT * FROM tbl_visitors WHERE id =".$db->sqs($id);
            $response = $db->getRow($sql);
            return $response;  
        }
    }
    
    /**
     * Method to get asset involved
     */
    public function getAllVisitor(){
        global $db;
        $sql = "SELECT * FROM tbl_visitors WHERE incident_id =".$db->sqs($_SESSION['incidentid']);
        $response = $db->getAll($sql);
        return $response;
    }
   
}
