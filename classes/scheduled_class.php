<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of scheduled_class
 *
 * @author Innovatorshill
 */
class schedules{
    //put your code here
    public function __construct() {
                
    }
    //add to schedule list
   public static function getInspectionDetails($id)
    {
        global $db;
            $sql = "SELECT tbl_sheqteam_groups.sheqteam_name,tbl_online_inspections.*
from tbl_online_inspections
inner join
tbl_sheqteam_groups
on tbl_online_inspections.group_id = tbl_sheqteam_groups.id
where tbl_online_inspections.is_active = 1
and tbl_online_inspections.id = {$db->sqs($id)} and tbl_online_inspections.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                foreach($response as $item)
                {
                    static::$getGroup = $item["sheqteam_name"];
                    static::$getInspectionDescription = $item["inspection_description"];
                }
                
            } else {
                return false;
            }
    }
    public static function addToSchedule($data)
    {
        global $db;    
        $sql = "INSERT INTO tbl_schedule (source,source_id,start_date,end_date,department_id,manager_id,sheqf_manager_id,"
                . "other_people,rescheduled,branch_id,company_id,created_by)"
                . " VALUES (". $db->sqs($data["source"])." , "    
                . $db->sqs($data['source_id'])." , "    
                . "". $db->sqs($data['start_date'])." , "
                . "". $db->sqs($data['end_date'])." , "
                . "". $db->sqs($data['department_id'])." , "
                . "". $db->sqs($data['sheqf_manager_id'])." , "
                . "". $db->sqs($data['other_people'])." , "
                . "". $db->sqs($data['rescheduled'])." , "
                . "". $db->sqs($data['branch_id'])." , "
                . $db->sqs($_SESSION['company_id'])." ,"
                . "" .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);                
        if($response){
           return $db->insertId();           
        }else{
            return false;
        }  
    }
}
