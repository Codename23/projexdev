<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of signed_off_class
 *
 * @author Innovatorshill
 */
class signed_off {
    //put your code here
    public function __construct()
    {
        
    }
    public static function checkSource($id,$type)
    {
        global $db;
        $status = false;
        $sql = "select source_id from tbl_signed_off where signedOffType = '{$db->sqs($type)}' and source_id = {$db->sqs($id)}";
        $result = $db->getAll($sql);
        if($result == true)
        {
             $status = true;            
        }         
         return $status;
    }
    public static function hasSignedOffDataWithDate($signedOffData = array())
    {
        global $db;
        $update_signedOffData = false;
        $signedOffSQL = "update {$signedOffData["table"]} set"
                . " attended = 1,signedOff = 1, SignedOffDate = {$db->sqs($signedOffData["SignedOffDate"])}, signedOffBy = {$db->sqs($_SESSION['user_id'])},"
                . " signedOffAction = {$db->sqs($signedOffData["signedOffAction"])} "
                . " where id = {$db->sqs($signedOffData['source_id'])} and "
                . " branch_id = {$db->sqs($_SESSION['branch_id'])} and "
                . " company_id = {$db->sqs($_SESSION['company_id'])}";    
        
        $result = $db->query($signedOffSQL);
        if($result == true)
        {
             $result = true;            
        }         
        return $signedOffSQL;
        // echo $signedOffSQL;
    }
    public static function hasSignedOffData($signedOffData = array())
    {
        global $db;
        $update_signedOffData = false;
        $signedOffSQL = "update {$signedOffData["table"]} set"
                . " attended = 1,signedOff = 1, SignedOffDate = NOW(), signedOffBy = {$db->sqs($_SESSION['user_id'])},"
                . " signedOffAction = {$db->sqs($signedOffData["signedOffAction"])} "
                . " where id = {$db->sqs($signedOffData['source_id'])} and "
                . " branch_id = {$db->sqs($_SESSION['branch_id'])} and "
                . " company_id = {$db->sqs($_SESSION['company_id'])}";    
        
        $result = $db->query($signedOffSQL);
        if($result == true)
        {
             $result = true;            
        }         
         return $signedOffSQL;
    } 
    public static function hasSignedOff($signedOffData = array())
    {
        global $db;
        $signedOffSQL = "insert into tbl_signed_off"
                . "(branch_id,company_id,source_id,signedOffLevel,"
                . "signedOffType,is_active,created_by)"                
                . "VALUES(
                ".$db->sqs($_SESSION['branch_id']).","
                 .$db->sqs($_SESSION['company_id']).","
                .$db->sqs($signedOffData['source_id']).","
                .$db->sqs($signedOffData['signedOffLevel']).","
                .$db->sqs($signedOffData['signedOffType']).",1,"
                .$db->sqs($_SESSION['user_id']).")";    
        $result = false;
        
        //check if the source already exist
        if(self::checkSource($signedOffData['source_id'], $signedOffData['signedOffType']))
        {
            //else add the source
           // $result = $db->query($signedOffSQL);
        }
        else
        {
            $result = $db->query($signedOffSQL);
        }
        if($result == true)
        {
            if(self::hasSignedOffData($signedOffData))
            {
               $result = true;            
            }
        }         
         return $result;
    } 
}
