<?php
/**
 * Occupation class containing all company methods
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */


class occupations{


    /**
     * company constructor.
     *Initialize the class
     */
    function  __construct()
    {

    }


    /**
     * Return all occupations
     * @return array|bool
     */
    public function getAllOccupations(){
        global $db;



        $allOccupationsSql = "SELECT tbl_occupations.id,
                                      tbl_branches.branch_name,
                                      tbl_departments.department_name,
                                      tbl_occupations.name,
                                      tbl_occupations.description,
                                      tbl_branches.company_id
                            FROM tbl_occupations
                            INNER JOIN tbl_departments ON tbl_occupations.department_id = tbl_departments.id
                            INNER JOIN tbl_branches ON tbl_branches.id = tbl_departments.branch_id
                            ";


        $getOccupationResult = $db->getAll($allOccupationsSql);

        if($getOccupationResult){
            return $getOccupationResult;
        }else{
            return false;
        }

    }

    /**
     * Method to get all occupations by departments
     * @param null $departmentId
     * @return array|bool
     *
     */
    
    public function getDepartmentOccupations($departmentId = null){
        global $db;



        $allOccupationsSql = "SELECT tbl_occupations.id,
                                      tbl_branches.branch_name,
                                      tbl_departments.department_name,
                                      tbl_occupations.name,
                                      tbl_occupations.description,
                                      tbl_branches.company_id
                            FROM tbl_occupations
                            INNER JOIN tbl_departments ON tbl_occupations.department_id = tbl_departments.id
                            INNER JOIN tbl_branches ON tbl_branches.id = tbl_departments.branch_id
                            WHERE tbl_occupations.department_id = ".$db->sqs($departmentId);


        $getOccupationResult = $db->getAll($allOccupationsSql);

        if($getOccupationResult){
            return $getOccupationResult;
        }else{
            return false;
        }

    }

    /**
     * Method to get all occupations bt branch
     * @param null $branchId
     * @return array|bool
     */
    public function getBranchOccupations($branchId = null){
        global $db;



        $allOccupationsSql = "SELECT tbl_occupations.id,
                                      tbl_branches.branch_name,
                                      tbl_departments.department_name,
                                      tbl_occupations.name,
                                      tbl_occupations.description,
                                      tbl_branches.company_id
                            FROM tbl_occupations
                            INNER JOIN tbl_departments ON tbl_occupations.department_id = tbl_departments.id
                            INNER JOIN tbl_branches ON tbl_branches.id = tbl_departments.branch_id
                            WHERE tbl_departments.branch_id = ".$db->sqs($branchId);

        $getOccupationResult = $db->getAll($allOccupationsSql);

        if($getOccupationResult){
            return $getOccupationResult;
        }else{
            return false;
        }

    }

    /**
     * Method to get all company Occupations
     * @param null $companyId
     * @return array|bool
     */
    public function  getCompanyOccupationsInSource($source){

        global $db;

        $companyOccupationsSql = "SELECT tbl_occupations.id,
                                      tbl_departments.department_name,
                                      tbl_occupations.name,
                                      tbl_occupations.description
                            FROM tbl_occupations
                            INNER JOIN tbl_departments ON tbl_occupations.department_id = tbl_departments.id
                            WHERE tbl_occupations.id in ($source) and tbl_departments.company_id = ".$db->sqs($_SESSION["company_id"]);
        $getCompanyOccupationResult = $db->getAll($companyOccupationsSql);

        if($getCompanyOccupationResult){
            return $getCompanyOccupationResult;
        }else{
            return false;
        }

    }

    public function  getCompanyOccupations($companyId = null){

        global $db;

        $companyOccupationsSql = "SELECT tbl_occupations.id,
                                      tbl_branches.branch_name,
                                      tbl_departments.department_name,
                                      tbl_occupations.name,
                                      tbl_occupations.description,
                                      tbl_branches.company_id
                            FROM tbl_occupations
                            INNER JOIN tbl_departments ON tbl_occupations.department_id = tbl_departments.id
                            INNER JOIN tbl_branches ON tbl_branches.id = tbl_departments.branch_id
                            WHERE tbl_branches.company_id = ".$db->sqs($companyId);


        $getCompanyOccupationResult = $db->getAll($companyOccupationsSql);

        if($getCompanyOccupationResult){
            return $getCompanyOccupationResult;
        }else{
            return false;
        }

    }

    /**
     *  Method to return selected occupation details
     * @param null $occupationId
     * @return array|bool
     */
    public function getOccupationInfo($occupationId = null){

        global $db;

        $occupationId = intval($occupationId);

        $selectedOccupationSql = "SELECT tbl_occupations.id,
                                      tbl_branches.branch_name,
                                      tbl_departments.department_name,
                                      tbl_occupations.name,
                                      tbl_occupations.description,
                                      tbl_branches.company_id
                            FROM tbl_occupations
                            INNER JOIN tbl_departments ON tbl_occupations.department_id = tbl_departments.id
                            INNER JOIN tbl_branches ON tbl_branches.id = tbl_departments.branch_id
                            WHERE tbl_occupations.id = ".$db->sqs($occupationId);

        $getOccupationResult = $db->getAll($selectedOccupationSql);

        if($getOccupationResult){
            return $getOccupationResult;
        }else{
            return false;
        }

    }

    /**
     * Method to add new occupation
     * @param array $occupationData
     * @return bool
     */
    public function addOccupation($occupationData = array()){

        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $addOccupationSql = "INSERT INTO tbl_occupations ( 
										name,    
										department_id,										
										created_by,    
										date_created,	    
										modified_by,   
										date_modified )
                                  value (".$db->sqs($occupationData['occupationName']).",
                                          ".$db->sqs($occupationData['departmentId']).",
                                          ".$db->sqs($occupationData['createdBy']).",
                                          ".$db->sqs($dateModified).",
                                          ".$db->sqs($occupationData['createdBy']).",
                                          ".$db->sqs($dateModified).")";

        $addOccupationResults = $db->query($addOccupationSql);

        if($addOccupationResults){
            return true;
        }else{
            return false;
        }
    }


    /**
     * Method to edit selected occupation
     * @param array $occupationData
     * @return bool
     */
    public function editOccupation($occupationData = array()){

        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');


        $editOccupationInfoSql = "UPDATE tbl_occupations SET    
										name = ".$db->sqs($occupationData['occupationName']).",   
										modified_by	 = ".$db->sqs($occupationData['modifiedBy'])." ,  
										date_modified  = ".$db->sqs($dateModified)."
                                    WHERE id=".$db->sqs($occupationData['occupationId']);

        $editOccupationInfoResults = $db->query($editOccupationInfoSql);

        if($editOccupationInfoResults){
            return true;
        }else{
            return false;
        }
    }
}
