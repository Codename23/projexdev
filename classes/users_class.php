<?php

/**
 * Class containing all user related methods
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class users_class
{

    /**
     * Method to initiate the class
     * 
     * @access public
     */
    public function __construct()
    {
        
    }

    /**
     * Method to add a new user
     * 
     * @param array $userDetsArr
     * @return array
     */
    public function addUser($userDetsArr)
    {
        global $db;
        
        $date = new DateTime();
        $datecreated = date_format($date, 'Y-m-d H:i:s');

        if(is_array($userDetsArr) && count($userDetsArr)>0){
            if(isset($userDetsArr['username']) && isset($userDetsArr['password'])){
                if($this->userExists($userDetsArr['username'])){
                    return 'The username is already registered.';
                } else {
                    $sql = "INSERT INTO tbl_users (user_type_id,firstname, lastname, username, password, created_by, date_created) VALUES (". $db->sqs($userDetsArr['user_type_id']) . ","
                        .$db->sqs($userDetsArr['firstname']).", ".$db->sqs($userDetsArr['lastname'])." , ".$db->sqs($userDetsArr['username']).", ".$db->sqs(md5($userDetsArr['password'])).", ".$db->sqs($_SESSION['user_id']).", ".$db->sqs($datecreated).")";

                    $result = $db->query($sql);

                    if($result){
                        return $db->insertId();
                    }else{
                        return false;
                    }

                }                    
            } else {
                return 'Required information missing.';
            }
        }
    }

    /**
     * Method to edit a new user
     * 
     * @param array $userDetsArr
     * @return array
     */
    public function editUser($userDetsArr = array())
    {
        global $db;

        if (is_array($userDetsArr) && count($userDetsArr) > 0) {
            if (isset($userDetsArr['user_id'])) {

                $sql = "UPDATE tbl_users SET firstname =" . $db->sqs($userDetsArr['firstname']) . " , 
                                                lastname =" . $db->sqs($userDetsArr['lastname']) . ", 
                                                modified_by = " . $db->sqs($_SESSION['user_id']) . "
                                                WHERE id = " . $db->sqs($userDetsArr['user_id']);
                $result = $db->query($sql);
                if($result){
                    return true;
                }else{
                    return false;
                }
            }
        }
    }

    /**
     * Method to edit a user name
     *
     * @param array $userDetsArr
     * @return array
     */
    public function editUserName($userDetsArr = array())
    {
        global $db;

        if (is_array($userDetsArr) && count($userDetsArr) > 0) {
            if (isset($userDetsArr['username']) && isset($userDetsArr['password'])) {
                if($this->userExists($userDetsArr['username'])){
                    return 'The username is already registered.';
                } else {

                    $sql = "UPDATE tbl_users SET username =" . $db->sqs($userDetsArr['username']) . ", 
                                                modified_by = " . $db->sqs($_SESSION['user_id']) . "
                                                WHERE id = " . $db->sqs($userDetsArr['user_id']);
                    $result = $db->query($sql);

                    if($result){
                        return true;
                    }else{
                        return false;
                    }
                }
            }
        }
    }


    /**
     * Method to edit user's surname
     *
     * @param array $userData
     * @return array
     */
    public function editUserLastName($userData = array())
    {
        global $db;

        if(is_array($userData) && count($userData)>0){

            $sql = "UPDATE tbl_users SET  lastname =".$db->sqs($userData['lastname']).", modified_by = ".$db->sqs($_SESSION['user_id'])." WHERE id = ".$db->sqs($userData['userId']);
            $result = $db->query($sql);

            if($result){
                return true;
            }else{
                return false;
            }

        }

        return false;
    }

    /**
     * Method to edit user's first name
     *
     * @param array $userData
     * @return array
     */
    public function editUserFirstName($userData = array())
    {
        global $db;

        if(is_array($userData) && count($userData)>0){

            $sql = "UPDATE tbl_users SET  firstname =".$db->sqs($userData['firstname']).", modified_by = ".$db->sqs($_SESSION['user_id'])." WHERE id = ".$db->sqs($userData['userId']);
            $result = $db->query($sql);

            if($result){
                return true;
            }else{
                return false;
            }

        }

        return false;
    }


    
    /**
     * Method to delete a new user
     * 
     * @param int $userid
     * @return bool
     */
    public function deleteUser($userid)
    {
        global $db;
        
        if(!is_null($userid)){
            $sql = "DELETE FROM tbl_users WHERE id = ".$db->sqs($userid);
            $result = $db->query($sql);
            if($result){
                return TRUE;
            } else {
                return FALSE;
            }            
        }
    }
    
    /**
     * Method to deactivate a user
     * 
     * @param int $id
     * @return bool
     */
    public function deactivateUser($id)
    {
        global $db;
        
        if(!is_null($id)){
            $sql = "UPDATE tbl_users SET is_active = 0 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return TRUE;
            } else {
                return FALSE;
            }            
        }
    }        
    /**
     * Method to activate a user
     * 
     * @param int $id
     * @return bool
     */
    public function activateUser($id)
    {
        global $db;
        
        if(!is_null($id)){
            $sql = "UPDATE tbl_users SET is_active = 1 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return TRUE;
            } else {
                return FALSE;
            }            
        }
    }
    /**
     * Method to change user password
     * 
     * @param array $userDetsArr
     * @return bool
     */
    public function changePassword($userDetsArr){
        global $db;

    }
    /**
     * Method to reset user password 
     *
     * @param array $userDetsArr
     */
    public function resetUserPassword($userDetsArr)
    {
        global $db;
        
          
    }
    /**
     * Function to attempt to log a user into the system
     * @param type $username
     * @param type $password
     * return true on success else false
     */
    public function logIn($username, $password)
    {
        global $db;

        $sql = "SELECT * FROM tbl_users WHERE is_active = 1 AND username = ".$db->sqs($username)." AND password = ".$db->sqs(md5($password));
        $response = $db->getRow($sql);
        if(count($response)>0){
            //Get company details
            
            
            $_SESSION['user_id'] = $response['id'];
            $_SESSION['firstname'] = $response['firstname'];
            $_SESSION['lastname'] = $response['lastname'];
            $_SESSION['username'] = $response['username'];
            $_SESSION['usertype_id'] = $response['user_type_id'];
            $_SESSION['defaultBranch'] = null;
            $_SESSION['branch_id'] = null;
            $_SESSION['company_id'] = null;
            $_SESSION['loggedin'] = true;
            
            return true;
        } else {
            return 'Your username or password is incorrect.';
        }
    }


    /**
     * Get user locations
     * @return array|bool
     */
    public function getUserCompanies()
    {
        global $objCompanies;


        if ($_SESSION['usertype_id'] == 1) {

            return $objCompanies->getAllCompanies();
        } else {

            return $objCompanies->getUserCompanies($_SESSION['user_id']);
        }
    }
        /**
      * Function to get a users details
      * @param string $username
      * @return array $response 
      * @access public
      */
    public function getUserDetails($username)
    {
        global $db;
        
        $sql = "SELECT * FROM tbl_users WHERE username = ".$db->sqs($username);
        $response = $db->getRow($sql);
        return $response;
    }
    
    /**
      * Function to get users
      * @return array $users 
      * @access public
      */
    public function getUsers($activeOnly = FALSE)
    {
        global $db;
 
        if($activeOnly){
            $sql = "SELECT * FROM tbl_users WHERE is_active = 1 ORDER BY date_created DESC";
        } else {
            $sql = "SELECT * FROM tbl_users ORDER BY date_created DESC";
        }
        
        $users = $db->getAll($sql); 
        return $users;
    }
    
    /**
     * Function to check if a user exists by checking for username
     * @param string $username The email to check for
     * @return bool True if exist else false
     * @access public
     */
    public function userExists($username)
    {
        global $db;

        $sql = "SELECT id FROM tbl_users WHERE username = ".$db->sqs($username);
        $response = $db->getOne($sql);
        if(is_array($response) && count($response)>0){
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Function to log a user out and remove his session data
     * @access public
     * @return void
     */
    public function logOut()
    {
        session_unset();
        session_destroy();
    }   
    
}