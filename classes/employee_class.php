<?php
/**
 * Class containing all user related methods
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class employees{



    public function __construct()
    {

    }
    /**
     * Method to get all company users
     * @param null $companyId
     * @param null $branchId
     * @return array|bool
     */
        public function getEmployeeById($emp_id,$branchId)
        {
        global $db;
        $getAllEmployeeSql = "SELECT tbl_users.id as per_id,tbl_users.firstname,tbl_users.lastname,
                                tbl_departments.department_name,
                                tbl_occupations.name as occupation
                                FROM tbl_users
                                inner join tbl_department_occupations
                                on tbl_users.id = tbl_department_occupations.users_id
                                inner join tbl_departments
                                on tbl_department_occupations.department_id = tbl_departments.id
                                inner join tbl_occupations
                                on tbl_department_occupations.occupation_id = tbl_occupations.id
                                where tbl_users.id = ".$db->sqs($emp_id);
                                //WHERE tbl_departments.tbl_branches = ".$db->sqs($branchId).
                                //" and tbl_users.id = ".$db->sqs($emp_id);

       /* if($branchId){
            $getAllEmployeeSql .="  AND tbl_branches.id = ".$db->sqs($branchId) ;
        }*/

       /* $getAllEmployeeSql .=" 
                                AND tbl_users.is_active = 1
                                AND tbl_users.id = {$db->sqs($emp_id)}
                                AND tbl_user_data.field_id = 1 ";*/

        //  AND tbl_users.user_type_id = 5 .. taken out for now..
        $allEmployeeResults = $db->getAll($getAllEmployeeSql);

        if($allEmployeeResults){
            return $allEmployeeResults;
        }else{
            return false;
        }
    }
    
    //altered
    public function getAllEmployees($companyId = null,$branchId = null,$dep = null,$investigation = null){

        global $db;
        $getAllEmployeeSql = "SELECT *,tbl_occupations.name as occupation,tbl_users.id as uid  
                              FROM tbl_users
                                  LEFT JOIN tbl_department_occupations ON tbl_department_occupations.users_id  = tbl_users.id
                                  LEFT JOIN tbl_user_data ON tbl_user_data.user_id  = tbl_users.id
                                  LEFT JOIN tbl_user_type_fields  ON tbl_user_type_fields.id  = tbl_user_data.field_id
                                  LEFT JOIN tbl_branches  ON tbl_department_occupations.branch_id  = tbl_branches.id
                                  LEFT JOIN tbl_departments ON tbl_department_occupations.department_id  = tbl_departments.id
                                  LEFT JOIN tbl_occupations ON tbl_department_occupations.occupation_id  = tbl_occupations.id
                              WHERE tbl_branches.company_id = ".$db->sqs($companyId);
        if($dep != null)
        {
            $getAllEmployeeSql .=" AND tbl_departments.id = ".$db->sqs($dep);
        }
        if($branchId){
            $getAllEmployeeSql .="  AND tbl_branches.id = ".$db->sqs($branchId) ;
        }

        $getAllEmployeeSql .=" 
                                AND tbl_users.is_active = 1
                                AND tbl_user_data.field_id = 1 ";

        //  AND tbl_users.user_type_id = 5 .. taken out for now..
        $allEmployeeResults = $db->getAll($getAllEmployeeSql);

        if($allEmployeeResults){
            return $allEmployeeResults;
        }else{
            return false;
        }
    }

    /**
     * Method to get all company employees
     * @param null $companyId
     * @return int
     */
    public  function getNumberOfEmployees($companyId = null){

        global $db;
        $getAllEmployeeSql = "SELECT *,tbl_occupations.name as occupation 
                              FROM tbl_users
                                  LEFT JOIN tbl_department_occupations ON tbl_department_occupations.users_id  = tbl_users.id
                                  LEFT JOIN tbl_user_data ON tbl_user_data.user_id  = tbl_users.id
                                  LEFT JOIN tbl_user_type_fields  ON tbl_user_type_fields.id  = tbl_user_data.field_id
                                  LEFT JOIN tbl_branches  ON tbl_department_occupations.branch_id  = tbl_branches.id
                                  LEFT JOIN tbl_departments ON tbl_department_occupations.department_id  = tbl_departments.id
                                  LEFT JOIN tbl_occupations ON tbl_department_occupations.occupation_id  = tbl_occupations.id
                              WHERE tbl_branches.company_id = ".$db->sqs($companyId);

        $getAllEmployeeSql .=" AND tbl_users.user_type_id = 5
                              AND tbl_user_data.field_id = 1";


        $allEmployeeResults = $db->query($getAllEmployeeSql, true);

        return $db->getRowsReturned();

    }
    /**
     * Method to employee data
     * @param $employeeId
     * @return array|bool
     */
    public function getEmployeeInfo($employeeId = null){


        global $db;

        echo $getSelectedEmployeeSql = "SELECT * FROM tbl_user_data 
                                        INNER JOIN tbl_user_type_fields  ON tbl_user_data.field_id = tbl_user_type_fields.id
                                        WHERE tbl_user_data.user_id = ".$db->sqs($employeeId)." AND tbl_user_data.user_type_id = 5";

        $selectedUserResults = $db->getAll($getSelectedEmployeeSql);

        if($selectedUserResults){
            return $selectedUserResults;
        }else{
            return false;
        }
    }

    /**
     * Get employee name
     * @param $employeeId
     * @return array|bool
     */
    public function  getEmployeeName($employeeId = null){

        global $db;

        $getSelectedEmployeeSql = "SELECT tbl_user_data.field_data 
                                          FROM tbl_user_data 
                                   INNER JOIN tbl_user_type_fields
                                        ON tbl_user_data.field_id = tbl_user_type_fields.id
                                  WHERE tbl_user_data.user_id = ".$db->sqs($employeeId)."
                                  AND tbl_user_data.user_type_id = 5 
                                  AND tbl_user_type_fields.field_name = 'employee_fullname'";

        $selectedUserResults = $db->getOne($getSelectedEmployeeSql);

        if($selectedUserResults){
            return $selectedUserResults;
        }else{
            return false;
        }

    }

 /**
     * Get employee id number
     * @param $employeeId
     * @return array|bool
     */
    public function  getEmployeeIDNumber($employeeId = null){

        global $db;

        $getSelectedEmployeeSql = "SELECT tbl_user_data.field_data 
                                          FROM tbl_user_data 
                                   INNER JOIN tbl_user_type_fields
                                        ON tbl_user_data.field_id = tbl_user_type_fields.id
                                  WHERE tbl_user_data.user_id = ".$db->sqs($employeeId)."
                                  AND tbl_user_data.user_type_id = 5 
                                  AND tbl_user_type_fields.field_name = 'employee_id'";

        $selectedUserResults = $db->getOne($getSelectedEmployeeSql);

        if($selectedUserResults){
            return $selectedUserResults;
        }else{
            return false;
        }

    }
    
    /**
     * Method to delete selected employee
     * @param $employeeData
     * @return bool
     */
    public function  deleteEmployee($employeeData = array()){
        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $deleteEmployeeSql = "UPDATE tbl_users SET 
                                    is_active	=  0,
                                    modified_by =".$db->sqs($employeeData['modifiedBy']).",
                                    date_modified =".$db->sqs($dateModified)."
                        WHERE id=".$db->sqs($employeeData['employeeId']);

        $employeeDeleteResults = $db->query($deleteEmployeeSql);

        if($employeeDeleteResults){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Add new employee to the database
     * @param array $employeeData
     * @return bool
     */
    public function addEmployee($employeeData = array()){

        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $addStatus =  false;
        foreach ($employeeData  as $key  => $value) {

            $field_id  = $this->getFieldId($key,$employeeData['user_type'],$employeeData['employee_id']);
            if($field_id) {

                if($this->existsUserData($field_id,$employeeData['user_type'],$employeeData['employee_id'])){//update data
                    continue;
                }else{//insert new data..
                    $addUserDataSql = "INSERT INTO tbl_user_data ( 
                                                user_id,
                                                user_type_id,	    
                                                field_id,    
                                                field_data,   
                                                created_by,   
                                                date_created,    
                                                modified_by,   
                                                date_modified
                                                )
                                        values(
                                        ".$db->sqs($employeeData['employee_userid']).","
                        .$db->sqs($employeeData['user_type']).","
                        .$db->sqs($field_id).","
                        .$db->sqs($value).","
                        .$db->sqs($employeeData['created_by']).","
                        .$db->sqs($dateModified).","
                        .$db->sqs($employeeData['created_by']).","
                        .$db->sqs($dateModified).")";

                    $addStatus = $db->query($addUserDataSql);
                    if($addStatus){
                        $addStatus = true;
                    }else{
                        $addStatus = false;
                    }
                }
            }else{
                continue;
            }
        }

        return $addStatus;
    }

    /**
     * Method to add employee occupation
     * @param array $occupationData
     * @return bool
     */
    public function addEmployeeOccupation($occupationData =  array()){

        global $db;

        $addEmployeeOccupationSql = "INSERT INTO tbl_department_occupations ( 
                                                occupation_id,	    
                                                department_id,	    
                                                branch_id,	    
                                                company_id,	    
                                                users_id,	    
                                                user_type_id
                                                )
                                        values(
                                       ".$db->sqs($occupationData['occupationId']).","
            .$db->sqs($occupationData['departmentId']).","
            .$db->sqs($occupationData['branchId']).","
            .$db->sqs($occupationData['companyId']).","
            .$db->sqs($occupationData['userId']).","
            .$db->sqs($occupationData['userType']).")";

        $addEmployeeOccupationResults = $db->query($addEmployeeOccupationSql);

        if($addEmployeeOccupationResults){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Update Employee occupation data
     * @param array $occupationData
     * @return bool
     */
    public function  updateEmployeeOccupation($occupationData = array()){

        global $db;

        $updateOccupationDataSql = "UPDATE  tbl_department_occupations  SET 
                                                occupation_id = ".$db->sqs($occupationData['occupationId']).",   
                                                department_id= ".$db->sqs($occupationData['departmentId']).",   	    
                                                branch_id= ".$db->sqs($occupationData['branchId'])."   
                                           WHERE users_id = ".$db->sqs($occupationData['userId'])." 
                                           AND user_type_id = ".$db->sqs($occupationData['userType']);

        $updateOccupationResults = $db->query($updateOccupationDataSql);

        if($updateOccupationResults){
            return true;
        }else{
            return false;
        }

    }


    /**
     * Get selected user type fields
     * @param $tableField
     * @param $userType
     * @return bool|int|String
     */
    protected function getFieldId($tableField,$userType){

        global $db;

        $checkExistTypesSql = "SELECT id
                                  FROM tbl_user_type_fields
                                  WHERE field_name = ".$db->sqs($tableField)."
                                  AND user_type_id =".$db->sqs($userType);

        $idResults = $db->getOne($checkExistTypesSql);

        if($idResults){
            return 	$idResults;
        }else{
            return false;
        }
    }

    /**
     * Check if data exist/ update or insert
     * @param $fieldId
     * @param $userType
     * @param $userId
     * @return bool
     */
    public function  existsUserData($fieldId,$userType,$userId){

        global $db;
        $existUserDataSql = "SELECT *
                                  FROM  tbl_user_data
                                  WHERE  field_id = ".$db->sqs($fieldId)."
                                  AND user_type_id =".$db->sqs($userType)."
                                  AND user_id = ".$db->sqs($userId);

        $userDataResults = $db->getAll($existUserDataSql);

        if($userDataResults){
            return 	true;
        }else{
            return false;
        }
    }

    /**
     * Method to add employee data
     * @param array $employeeData
     * @return bool
     */
    public function  editEmployee($employeeData = array()){
        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $update_status = false;
        foreach ($employeeData  as $key  => $value) {

            $field_id  = $this->getFieldId($key,$employeeData['user_type']);
            if($field_id) {

                if($this->existsUserData($field_id,$employeeData['user_type'],$employeeData['employeeId'])){

                    $updateUserDataSql = "UPDATE  tbl_user_data  SET 
                                                field_data = ".$db->sqs($value).", 
                                                modified_by = ".$db->sqs($employeeData['modified_by']).",
                                                date_modified = ".$db->sqs($dateModified)."
                                           WHERE user_id = ".$db->sqs($employeeData['employeeId'])."
                                           AND user_type_id = ".$db->sqs($employeeData['user_type'])."   
                                           AND field_id = ".$db->sqs($field_id);

                    $update_status = $db->query($updateUserDataSql);

                }else{
                    continue;
                }
            }else{
                continue;
            }
        }

        return $update_status;
    }

    /**
     * Get user form fields(employee,system admin)
     * @param $userType
     * @return array|bool
     */
    public function getEmployeeFormDetails($userType = null){

        global $db;
        $getSelectedTypesSql = "SELECT * 
                                  FROM tbl_user_type_fields
                                  WHERE user_type_id = ".$db->sqs($userType)." ORDER BY field_order";

        $selectedTypeResults = $db->getAll($getSelectedTypesSql);

        if($selectedTypeResults){
            return $selectedTypeResults;
        }else{
            return false;
        }
    }

    /**
     * Get selected user type fields
     * @param $userType
     * @return array|bool
     */
    protected function getFields($userType = null){

        global $db;
        $getSelectedTypesSql = "SELECT * 
                                  FROM tbl_user_type_fields
                                  WHERE user_type_id = ".$db->sqs($userType);

        $selectedTypeResults = $db->getAll($getSelectedTypesSql);

        if($selectedTypeResults){
            return $selectedTypeResults;
        }else{
            return false;
        }
    }

    /**
     * Method to retrieve employee department
     * @param null $employeeId
     * @return bool|int|String
     */
    public function getEmployeeDepartment($employeeId = null){


        global $db;
        $getEmployeeDepartmentSql = "SELECT tbl_user_data.field_data
                                  FROM tbl_user_data
                                  INNER JOIN tbl_user_type_fields ON  tbl_user_type_fields.user_type_id = tbl_user_data.user_type_id
                                  WHERE  tbl_user_data.tbl_user_data.field_id = tbl_user_type_fields.id
                                  AND tbl_user_type_fields.id = 17
                                  AND tbl_user_data.user_id = ".$db->sqs($employeeId);

        $empDepartmentResults = $db->getOne($getEmployeeDepartmentSql);

        if($empDepartmentResults){
            return $empDepartmentResults;
        }else{
            return false;
        }

    }

    /**
     * Method to retrieve employee branch
     * @param null $employeeId
     * @return bool|int|String
     */
    public function getEmployeeBranch($employeeId =  null){


        global $db;
        $getEmployeeDepartmentSql = "SELECT tbl_user_data.field_data
                                  FROM tbl_user_data
                                  INNER JOIN tbl_user_type_fields ON  tbl_user_type_fields.user_type_id = tbl_user_data.user_type_id
                                  WHERE  tbl_user_data.tbl_user_data.field_id = tbl_user_type_fields.id
                                  AND tbl_user_type_fields.id = 16
                                  AND tbl_user_data.user_id = ".$db->sqs($employeeId);

        $empDepartmentResults = $db->getOne($getEmployeeDepartmentSql);

        if($empDepartmentResults){
            return $empDepartmentResults;
        }else{
            return false;
        }
    }

    /**
     * Method to retrieve occupation data
     * @param null $employeeId
     * @return bool|int|String
     */
    public function getEmployeeOccupation($employeeId = null){

        global $db;
        $getEmployeeDepartmentSql = "SELECT tbl_user_data.field_data
                                                FROM tbl_user_data
                                  INNER JOIN tbl_user_type_fields ON  tbl_user_type_fields.user_type_id = tbl_user_data.user_type_id
                                  WHERE  tbl_user_data.tbl_user_data.field_id = tbl_user_type_fields.id
                                  AND tbl_user_type_fields.id = 18
                                  AND tbl_user_data.user_id = ".$db->sqs($employeeId);

        $empDepartmentResults = $db->getOne($getEmployeeDepartmentSql);

        if($empDepartmentResults){
            return $empDepartmentResults;
        }else{
            return false;
        }
    }

    /**
     * @return array|bool
     */
    public function getEmployeeGender(){

        global $db;

        $allGenderSql = "SELECT * FROM tbl_gender";
        $genderResults = $db->getAll($allGenderSql);

        if($genderResults){

            return $genderResults;
        }else{

            return false;
        }

    }

    /**
     * Get user ethnic group
     * @return array|bool
     */
    public function getEmployeeRace(){

        global $db;

        $allEthnicSql = "SELECT * FROM tbl_ethnic";
        $ethnicSqlResults = $db->getAll($allEthnicSql);

        if($ethnicSqlResults){
            return $ethnicSqlResults;
        }else{
            return false;
        }

    }
    /**
     *
     */
    public function searchEmployee($searchterm, $department = null){
        global $db;

        $conditions = "";

        if(isset($searchterm) && isset($department)){
            $conditions = "AND (tbl_users.firstname LIKE '%" . $searchterm . "%' 
                           OR tbl_users.lastname LIKE '%" . $searchterm. "%' ) 
                           AND tbl_departments.id = " . $db->sqs($department);
        }

        if(!isset($searchterm) && isset($department)){
            $conditions = "AND tbl_departments.id = ".$db->sqs($department);

        }

        if(!isset($department) && isset($searchterm)){
            $conditions = "AND tbl_users.firstname LIKE '%" . $searchterm . "%' 
                           OR tbl_users.lastname LIKE '%" . $searchterm . "%'";
        }

        $sql = "SELECT  tbl_branches.branch_name,
                        tbl_departments.department_name,
                        tbl_users.firstname,
                        tbl_users.lastname,
                        tbl_users.id
                        FROM tbl_users
                        LEFT JOIN tbl_department_occupations ON tbl_department_occupations.users_id  = tbl_users.id
                        LEFT JOIN tbl_branches  ON tbl_department_occupations.branch_id  = tbl_branches.id
                        LEFT JOIN tbl_departments ON tbl_department_occupations.department_id  = tbl_departments.id
                        WHERE tbl_branches.company_id = " . $db->sqs($_SESSION['company_id']) . " " . $conditions;
        $response = $db->getall($sql);
        if($response){
            return $response;
        }else{
            return FALSE;
        }
    }
    /**
     * Method to get all employee by occupation
     * @param int $occupation
     * @access public
     * @return array
     */
    public function searchEmployeeByOccupation($occupationId = null){
        global $db;
        $sql = "SELECT tbl_users.id,
                       tbl_users.firstname,
                       tbl_users.lastname,
                       tbl_user_data.field_data
                       FROM tbl_users 
                       LEFT JOIN tbl_user_data ON tbl_user_data.user_id  = tbl_users.id
                       LEFT JOIN tbl_department_occupations ON tbl_department_occupations.users_id = tbl_users.id
                       LEFT JOIN tbl_occupations ON tbl_occupations.id  = tbl_department_occupations.occupation_id
                       WHERE tbl_user_data.field_id = 10 AND tbl_occupations.id = " . $db->sqs($occupationId);
        $response = $db->getall($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }

    /**
     * Method to get all employee by branch
     * @param int $branchId
     * @access public
     * @return array
     */
    public function getAllEmployeeByBranch($branchId = null){

        global $db;
        $sql = "SELECT  GROUP_CONCAT(tbl_user_data.field_data SEPARATOR  ' ') AS employee_name,tbl_user_data.user_id
                       FROM tbl_user_data
                       INNER JOIN tbl_department_occupations ON tbl_department_occupations.users_id = tbl_user_data.user_id
                       WHERE  (tbl_user_data.field_id = 4 OR  tbl_user_data.field_id = 3)
                       AND tbl_department_occupations.branch_id = " . $db->sqs($branchId). " GROUP BY tbl_user_data.user_id";

        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }

    /**
     * Method to get employee name
     * @param int $empId
     * @access public
     * @return array
     */
    public function getEmployeeNames($empId = null){

        global $db;
        $sql = "SELECT  GROUP_CONCAT(tbl_user_data.field_data SEPARATOR  ' ') AS employee_name
                       FROM tbl_user_data
                       WHERE  (tbl_user_data.field_id = 4 OR  tbl_user_data.field_id = 3)
                       AND tbl_user_data.user_id = " . $db->sqs($empId). " GROUP BY tbl_user_data.user_id";

        $response = $db->getOne($sql);

        if($response){
            return $response;
        }else{
            return false;
        }
    }
}