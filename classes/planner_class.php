<?php

/**
 * Class containing all planner related methods
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

class planner_class
{

    /**
     * Method to initiate the class
     * 
     * @access public
     */
    public function __construct()
    {

    }
    
    /**
     * Method to add a new event to the planner
     * 
     * @access public
     * @param array $eventDetails
     * @return mixed
     */
    public function addEvent($eventDetails)
    {
        global $db;
        
        
    }        
    
}