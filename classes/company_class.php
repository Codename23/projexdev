<?php
/**
 * Company class containing all company methods
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */


class company{


    /**
     * company constructor.
     *Initialize the class
     */
    function  __construct()
    {

    }


    /**
     * Method to return all companies
     * @return array|bool
     */
    public function getAllCompanies(){
        global $db;

        $allCompaniesSql = "SELECT id,company_name,
                                  vat_number,pty_number
                            FROM tbl_companies";

        $getAllCompaniesResult = $db->getAll($allCompaniesSql);

        if($getAllCompaniesResult){
            return $getAllCompaniesResult;
        }else{
            return false;
        }

    }


    /**
     * Method to get all companies assigned to user
     * @param null $userId
     * @return array
     */
    public function getUserCompanies($userId = null)
    {

        global $db;
        $userCompanySql = "SELECT tbl_department_occupations.company_id,
                                  tbl_companies.company_name
                              FROM tbl_department_occupations,tbl_companies
                              WHERE tbl_department_occupations.company_id  = tbl_companies.id
                              AND tbl_department_occupations.users_id =" . $db->sqs($userId) . "
                            GROUP BY tbl_department_occupations.company_id";


        $getUserCompaniesResult = $db->getAll($userCompanySql);

        if ($getUserCompaniesResult) {
            return $getUserCompaniesResult;
        } else {
            return array();
        }

    }

    /**
     * Method to return selected company details
     * @param null $companyId
     * @return array|bool
     */
    public function getCompanyInfo($companyId = null){
        global $db;

        $selectedCompanySql = "SELECT *
                            FROM tbl_companies
                            WHERE id =".$db->sqs($companyId);

        $getCompanyResult = $db->getRow($selectedCompanySql);

        if($getCompanyResult){
            return $getCompanyResult;
        }else{
            return false;
        }

    }

    /**
     * Get all company Branches
     * @param null $companyId
     * @return array
     */
    public function getCompanyBranches($companyId = null){

        global $objBranch;

        $getAllBranchesResult = $objBranch->getBranchByCompany($companyId);

        if($getAllBranchesResult){
            return $getAllBranchesResult;
        }else{
            return false;
        }

    }

    /**
     * Get all company Branches
     * @param null $companyId
     * @return array
     */
    public function getCompanyHODBranch($companyId = null){

        global $objBranch;

        $getAllBranchesResult = $objBranch->getHeadOfficeBranchByCompany($companyId);

        if($getAllBranchesResult){
            return $getAllBranchesResult;
        }else{
            return false;
        }

    }

    /**
     * Count company Branches
     * @param null $companyId
     * @return array
     */
    public function countCompanyBranches($companyId = null){

        global $objBranch;

        return $objBranch->getNumberOfBranches($companyId);

    }

    /**
     * Method to count company departments
     * @param null $companyId
     * @return array|bool
     */
    public function countCompanyDepartments($companyId = null){

        global $objDepartment;

        return $objDepartment->getNumberOfDepartments($companyId);
    }

    /**
     * Method to count company employees
     * @param null $companyId
     * @return array|bool
     */
    public function countCompanyEmployees($companyId = null){

        global $objEmployee;

        return $objEmployee->getNumberOfEmployees($companyId);


    }

    /**
     * Method to return selected branch info
     * @param null $branchId
     * @return array|bool
     */
    public function getBranchInfo($branchId = null){

        global $objBranch;
        $getBranchResult =  $objBranch->getBranchById($branchId);


        if($getBranchResult){
            return $getBranchResult;
        }else{
            return array();
        }

    }


    /**
     * Method to return all company departments
     * @param null $companyId
     * @return array
     */
    public function getCompanyDepartments($companyId = null){

        global $objDepartment;

        $getAllDepartmentsResult = $objDepartment->getDepartmentByCompany($companyId);

        if($getAllDepartmentsResult){
            return $getAllDepartmentsResult;
        }else{
            return false;
        }

    }


    /**
     * Method to return all branch departments
     * @param null $branchId
     * @return array|bool
     */
    public function getBranchDepartments($branchId = null){

        global $objDepartment;

        $getAllDepartmentsResult = $objDepartment->getDepartmentByBranch($branchId);

        if($getAllDepartmentsResult){
            return $getAllDepartmentsResult;
        }else{
            return array();
        }

    }


    /**
     * @param null $departmentId
     * @return array
     */
    public function getDepartmentInfo($departmentId = null){
        global $objDepartment;

        $getDepartmentResult = $objDepartment->getDepartmentById($departmentId);

        if($getDepartmentResult){
            return $getDepartmentResult ;
        }else{
            return array();
        }

    }

    /**
     * Method to add a new company
     * @param $companyData
     * @param int $createdBy
     * @return bool
     */
    public function addCompany($companyData,$createdBy = 0){
        global $objFolders;
        global $db;

        $parentFolderId = null;
        $success = false;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        if($createdBy == 0){
            $createdBy = $_SESSION["user_id"];
        }

        $addCompanySql = "INSERT INTO tbl_companies ( company_name,
                                                      vat_number,
                                                      company_logo,
                                                      sars_certificate,
                                                      due_dilligence,
                                                      publicliability_insurance,
                                                      pty_ck_documents,
                                                      ohs_legal_compliance_certificate,
                                                      pty_number,
                                                      office_address,
                                                      office_tel_number,
                                                      cell_number,
                                                      fax_number,
                                                      office_email,
                                                      financial_department_email,
                                                      marketing_department_email,
                                                      country_id,
                                                      region_id,
                                                      created_by,
                                                      date_created,
                                                      modified_by,
                                                      date_modified)
                          value (".$db->sqs($companyData['company_name']).",
                           ".$db->sqs($companyData['vat_number']).",
                           ".$db->sqs($companyData['logo']).",
                           null,
                           null,
                           null,
                           null,
                           null,
                           ".$db->sqs($companyData['pty_number']).",
                           ".$db->sqs($companyData['office_branch']).",
                           ".$db->sqs($companyData['office_number']).",
                           ".$db->sqs($companyData['cell_number']).",
                           ".$db->sqs($companyData['fax_number']).",
                           ".$db->sqs($companyData['email']).",
                           null,
                           null,
                           ".$db->sqs($companyData['country_id']).",
                           ".$db->sqs($companyData['region_id']).",
                           ".$db->sqs($createdBy).",
                           ".$db->sqs($dateModified).",
                           ".$db->sqs($createdBy).",
                           ".$db->sqs($dateModified).")";


        $addCompanyResults = $db->query($addCompanySql);

        if($addCompanyResults){

            $branchData = array();
            $branchData['companyId'] = $db->insertId();
            $branchData['branchName'] = $companyData['headOfficeName'];
            $branchData['officeAddress'] = $companyData['office_branch'];
            $branchData['officeNumber'] = $companyData['office_number'];
            $branchData['faxNumber'] = $companyData['fax_number'];
            $branchData['officeEmail'] = $companyData['email'];
            $branchData['isHeadOffice'] = 1;

            $branchData['countryId'] = $companyData['country_id'];
            $branchData['regionId'] = $companyData['region_id'];
            $branchData['createdBy'] = $createdBy;


            //add company folder
            $folderData = array(
                "folderName"=>$companyData['company_name'],
                "parentId" =>"NULL",
                "companyId" =>$branchData['companyId'],
                "userId"=>$createdBy
            );

            $parentFolderId = $objFolders->addNewFolder($folderData);

            $success = $objFolders->createFolderDirectory($folderData,$parentFolderId);

            if(!is_null($parentFolderId)){
                     $branchData['parentId'] = $parentFolderId;
             }

            if($this->addBranch($branchData)){
                $success = true;
            }



            return $success;
        }else{
            return $success;
        }

    }


    /**
     * Method to update company info
     * @param $companyData
     * @param int $modifiedBy
     * @return bool
     */
    public function editCompany($companyData,$modifiedBy = 0){

        global $db;

        $date = new DateTime();
        $dateModified =  date_format($date, 'Y-m-d H:i:s');

        if($modifiedBy == 0){
            $modifiedBy = $_SESSION["user_id"];
        }

        $editCompanyInfoSql = "UPDATE tbl_companies SET 
                                                      company_name = ".$db->sqs($companyData['company_name']).",
                                                      vat_number = ".$db->sqs($companyData['vat_number']).",
                                                      company_logo = ".$db->sqs($companyData['logo']).",
                                                      sars_certificate = '',
                                                      due_dilligence = '',
                                                      publicliability_insurance = '',
                                                      pty_ck_documents = '',
                                                      ohs_legal_compliance_certificate = '',
                                                      pty_number = ".$db->sqs($companyData['pty_number']).",
                                                      office_address = ".$db->sqs($companyData['office_branch']).",
                                                      office_tel_number =".$db->sqs($companyData['office_number']).",
                                                      cell_number = ".$db->sqs($companyData['cell_number']).",
                                                      fax_number =".$db->sqs($companyData['fax_number']).",
                                                      office_email = ".$db->sqs($companyData['email']).",
                                                      financial_department_email = '',
                                                      marketing_department_email = '',
                                                      country_id = ".$db->sqs($companyData['country_id']).",
                                                      region_id = ".$db->sqs($companyData['region_id']).",
                                                      modified_by=".$db->sqs($modifiedBy).",
                                                      date_modified=".$db->sqs($dateModified)."

                                                    WHERE id=".$db->sqs($companyData['company_id']);

        $editCompanyInfoResults = $db->query($editCompanyInfoSql);

        if($editCompanyInfoResults){
            return true;
        }else{
            return false;
        }



    }

    /**
     * Method to add a new branch
     * @param array $branchData
     * @return bool
     */
    public function addBranch($branchData = array()){

        global $objBranch;

        if(!empty($branchData)){
            $addBranchResults = $objBranch->addBranch($branchData);
        }

        if($addBranchResults){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Method to edit a  branch
     * @param array $branchData
     * @return bool
     */
    public function editBranch($branchData = array()){

        global $objBranch;

        $editBranchInfoResults = $objBranch->editBranch($branchData);
        if($editBranchInfoResults){
            return true;
        }else{
            return false;
        }
    }


    /**
     * Method to add a new department
     * @param $departmentData
     * @return bool
     */
    public function addDepartment($departmentData = array()){

        global $objDepartment;

        $addDepartmentResults = $objDepartment->addDepartment($departmentData);
        if($addDepartmentResults){
            return true;
        }else{
            return false;
        }
    }


    /**
     * Method to edit branch departments
     * @param array $departmentData
     * @return bool
     */
    public function editDepartment($departmentData = array() ){

        global $objDepartment;

        $editDepartmentInfoResults = $objDepartment->editDepartment($departmentData);

        if($editDepartmentInfoResults){
            return true;
        }else{
            return false;
        }
    }
}