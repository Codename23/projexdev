<?php

class investigations
{
    public $hasFive = false;
    public $hasAddedImprovementOrCorrectiveAction = false;
    public $investigation_id = 0;
    public $requested_id = 0;
    public $investigation_date = "";
    public $investigator = 0;
    /////////////////// Source Variables ///////////////////////////////////
    public $investigation_source_id = 0;
    public $investigation_source = "";
    public $source_date = "";
    public $source_group = "";
    public $source_description = "";
    public $source_type = "";
    public $source_reporting_person = "";
    public $source_details = "";
    public $source_dep = "";
    public $source_link = "";
    //////////////////// Involvement ////////////////////////////////////
    public $person_involved = "";
    public $suppler_involved = "";
    public $resources_involved = "";
    public $images = "";
    ///////////////////////////
    
    public function __construct() 
    {
        
    }   
    public function addInvestigation($investigate_data = array())
    {
        global $db;
        $addStatus = false;
        $addInvestigationSQL = "insert into tbl_investigations(investigate_link_id,branch_id,company_id,investigation_type,investigationOptions,"
                . "investigator,investigator_due_date,investigator_status,is_active,created_by)"
                . "VALUES(
                ".$db->sqs($investigate_data['investigate_link_id']).","
                 .$db->sqs($_SESSION['branch_id']).","
                 .$db->sqs($_SESSION['company_id']).","
                .$db->sqs($investigate_data['investigation_type']).","
                .$db->sqs($investigate_data['investigationOptions']).","
                .$db->sqs($investigate_data['investigationInvestigator']).","
                .$db->sqs($investigate_data['investigator_due_date']).","
                .$db->sqs($investigate_data['investigator_status']).","                   
                 ."1,"
                .$db->sqs($_SESSION['user_id']).")";        
        $addInvestigationResult = $db->query($addInvestigationSQL);        
         if($addInvestigationResult == true)
         {
             $this->requested_id = $db->insertId();
             $addStatus = true;            
         }         
         return $addStatus;
    }
    public function getInvestigationReport($id)    
    {
        global $db;
        $getAllSQL = "select ti.id as tid,nc.id as nc_id,
        DATE_FORMAT(nc.date_created,'%m/%d/%Y') as nc_date_created,
        DATE_FORMAT(nc.due_date,'%m/%d/%Y') as nc_due_date,
        DATE_FORMAT(ti.date_created,'%m/%d/%Y') as ti_lodge_date,
        tbl_sheqteam_groups.sheqteam_name as nc_group_field,
        dep.department_name,nc.resource_involved as nc_resources_involved,person_involved as nc_person_involved,
        nc.non_conformance_details,nc.status,nc.photo_name,CONCAT(tu.firstname,' ',tu.lastname) as loggedBy,
        DATE_FORMAT(ti.investigator_due_date,'%m/%d/%Y') as investigator_due_date,
        ti.investigationOptions,ti.investigator_status,ti.investigation_type,
        (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
        tbl_users where tbl_users.id = ti.investigator) as investigator,
        (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from tbl_users where tbl_users.id = nc_person_involved) as person_involve,
        (select name from tbl_assets where tbl_assets.id = nc_resources_involved and nc.branch_id = '{$_SESSION['branch_id']}') as resource,
             tbl_suppliers.supplier_name  
        from tbl_non_conformance as nc
        left join tbl_sheqteam_groups
        on nc.group_field = tbl_sheqteam_groups.id
        left join tbl_departments 
        as dep on
        nc.department_field = dep.id      
        inner join tbl_users tu
        on
        nc.created_by = tu.id
        inner join tbl_investigations as ti
        on nc.id = ti.investigate_link_id   
        left join tbl_suppliers
        on nc.supplier_id = tbl_suppliers.id
        where nc.branch_id = ".$db->sqs($_SESSION['branch_id'])
       ." and ti.investigation_type = 1
         and nc.is_active = 1 and ti.id = ".$db->sqs($id);     
        $getResult = $db->getAll($getAllSQL);
         if($getResult == true)
         {
             $this->investigation_id = $getResult[0]['tid'];  
             $this->investigation_date = $getResult[0]['ti_lodge_date'];  
             $this->investigator = $getResult[0]['investigator']; 
             //sources
             switch($getResult[0]['investigation_type'])
             {
                case 1:
                    $this->source_description = "Non-Conformance";
                break;
                case 2:
                    $this->source_description = "Incident";
                break;
             }
             $this->investigation_source_id = $getResult[0]["nc_id"];
             $this->source_date = $getResult[0]["nc_date_created"];
             $this->source_group = $getResult[0]["nc_group_field"];
             $this->source_dep = $getResult[0]["department_name"];            
             $this->source_reporting_person = $getResult[0]["loggedBy"];
             $this->source_details = $getResult[0]["non_conformance_details"];
             //involvement
             $this->person_involved = $getResult[0]["person_involve"]; ;
             $this->suppler_involved = $getResult[0]["supplier_name"]; ;
             $this->resources_involved = $getResult[0]["resource"]; ;
             //image
             $this->images = $getResult[0]["photo_name"];
         }
    }
    public function getInvestigationInfo($id)    
    {
        global $db;
        $getAllSQL = "select nc.id as nc_id,
        DATE_FORMAT(nc.date_created,'%m/%d/%Y') as nc_date_created,
        DATE_FORMAT(nc.due_date,'%m/%d/%Y') as nc_due_date,
        tbl_sheqteam_groups.sheqteam_name as nc_group_field,
        dep.department_name,
        nc.non_conformance_details,nc.status,ti.id as tid,
        DATE_FORMAT(ti.investigator_due_date,'%m/%d/%Y') as investigator_due_date,
        ti.investigationOptions,ti.investigator_status,
        (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
        tbl_users where tbl_users.id = ti.investigator) as investigator from tbl_non_conformance as nc
        left join tbl_sheqteam_groups
        on nc.group_field = tbl_sheqteam_groups.id
        left join tbl_departments 
        as dep on
        nc.department_field = dep.id      
        inner join tbl_users tu
        on
        nc.created_by = tu.id
        inner join tbl_investigations as ti
        on nc.id = ti.investigate_link_id        
        where nc.branch_id = ".$db->sqs($_SESSION['branch_id'])
       ." and ti.investigation_type = 1
         and nc.is_active = 1 and nc_id = ".$db->sqs($id);     
        $getResult = $db->getAll($getAllSQL);
         if($getResult == true)
         {
             return $getResult;           
         }         
         else
         {
            return false;
         }
    }
   public function getFiveWhy($action_link)    
    {
        global $db;
        $getAllSQL = "select * from tbl_why where tbl_why.branch_id = ".$db->sqs($_SESSION['branch_id'])." "
                . " and tbl_why.action_link_id = ".$db->sqs($action_link)." LIMIT 5";
        $getResult = $db->getAll($getAllSQL);
         if($getResult == true)
         {
             return $getResult;           
         }         
         else
         {
            return false;
         }
    }
    public function checkFiveWhyExist($action_id)
    {
        global $db;
        $sql = "select * from tbl_why where action_link_id = ".$db->sqs($action_id);
         $getResult = $db->getAll($sql);
         if($getResult == true)
         {
             $hasFive = true;
             return $getResult;           
         } 
         else{
             return false;
         }
       
    }
    public function addFiveWhy($source,$action_id,$WhyData = array())
    {
        global $db;
        $updateStatus = false;
            $sql = "insert into tbl_why(branch_id,company_id,source_type,action_link_id,why,type,description,recomm,priority,created_by)"
                . "VALUES(
                ".$db->sqs($_SESSION['branch_id']).","
                 .$db->sqs($_SESSION['company_id']).","  
                .$db->sqs($source).","
                .$db->sqs($action_id).","
                .$db->sqs($WhyData['why']).","                    
                .$db->sqs($WhyData['type']).","
                .$db->sqs($WhyData['description']).","
                .$db->sqs($WhyData['recomm']).","
                .$db->sqs($WhyData['priority']).","
                .$db->sqs($_SESSION['user_id']).")";
            $updateResult = $db->query($sql);
            if($updateResult)
            {
                $updateStatus = true;
            }
        return $updateStatus;
    }
    public function getAllInvestigations($is_history = null)    
    {
        global $db;
        $condition = 0;
        $query = "";
        
        if($is_history)
        {
             $query = "";
             $condition = 1;
        }
        $getAllSQL = "select nc.id as nc_id,
        DATE_FORMAT(nc.date_created,'%m/%d/%Y') as nc_date_created,
        DATE_FORMAT(nc.due_date,'%m/%d/%Y') as nc_due_date,
        tbl_sheqteam_groups.sheqteam_name as nc_group_field,
        dep.department_name,
        nc.non_conformance_details,nc.status,ti.id as tid,
        DATE_FORMAT(ti.investigator_due_date,'%m/%d/%Y') as investigator_due_date,
        $query
        ti.investigationOptions,ti.investigator_status,
        (select CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as emp from
        tbl_users where tbl_users.id = ti.investigator) as investigator from tbl_non_conformance as nc
        left join tbl_sheqteam_groups
        on nc.group_field = tbl_sheqteam_groups.id
        left join tbl_departments 
        as dep on
        nc.department_field = dep.id      
        inner join tbl_users tu
        on
        nc.created_by = tu.id
        inner join tbl_investigations as ti
        on nc.id = ti.investigate_link_id             
        where nc.branch_id = ".$db->sqs($_SESSION['branch_id'])
       ." and ti.investigation_type = 1
         and nc.is_active = 1 and ti.id and ti.attended = $condition";     
      
        $getResult = $db->getAll($getAllSQL);
         if($getResult == true)
         {
             return $getResult;           
         }         
         else
         {
            return false;
         }
    }
    public function updateInvestigationStatus($created_id,$non_id,$due_date)
    {
        global $db;
        $updateStatus = false;
        $sql = "update tbl_non_conformance set InvestigateDueDate = '{$due_date}',Investigate = ".$db->sqs($created_id)." where id = ".$db->sqs($non_id);
        $updateResult = $db->query($sql);
        if($updateResult)
        {
            $updateStatus = true;
        }
        return $updateStatus;
    }
 
}