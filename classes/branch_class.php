<?php
/**
 * Branch class containing all branch methods
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class branches
{


    /**
     * Initialise branch class
     * branch constructor.
     */
    function __construct()
    {

    }


    /**
     * Method to retrieve all branches
     * @return array|bool
     */
    public function getAllBranches(){

        global $db;

        $allBranchSql = "SELECT * FROM tbl_branches";

        $getAllBranchResults = $db->getAll($allBranchSql);


        if($getAllBranchResults){

            return $getAllBranchResults;
        }else{
            return false;
        }

    }

    /**
     * Method to get number of branches
     * @param null $companyId
     * @return int
     */
    public function getNumberOfBranches($companyId = null){

        global $db;

        $selectedBranchSql = "SELECT * FROM tbl_branches WHERE company_id =".$db->sqs($companyId);
        $db->query($selectedBranchSql,true);
        return $db->getRowsReturned();

    }

    /**
     * Method to retrieve selected branch info
     * @param null $branchId
     * @return array|bool
     */
    public  function getBranchById($branchId = null){

        global $db;

        $selectedBranchSql = "SELECT * FROM tbl_branches WHERE id =".$db->sqs($branchId);

        $getSelectedBranchResults = $db->getRow($selectedBranchSql);

        if($getSelectedBranchResults){

            return $getSelectedBranchResults;
        }else{
            return false;
        }
    }


    /**
     * Method to get branch information by branch name
     * @param null $branchName
     * @return array|bool
     */
    public function getBranchByName($branchName = null){

        global $db;

        $selectedBranchSql = "SELECT * FROM tbl_branches WHERE branch_name LIKE '%".$db->sqs($branchName)."%'";

        $getSelectedBranchResults = $db->getRow($selectedBranchSql);

        if($getSelectedBranchResults){

            return $getSelectedBranchResults;
        }else{
            return false;
        }

    }

    /**
     * Method to all retrieve branch information by company
     * @param null $companyId
     * @return array|bool
     */
    public function getBranchByCompany($companyId = null){

        global $db;

        $selectedBranchSql = "SELECT * FROM tbl_branches WHERE company_id =".$db->sqs($companyId);

        $getSelectedBranchResults = $db->getAll($selectedBranchSql);

        if($getSelectedBranchResults){

            return $getSelectedBranchResults;
        }else{
            return false;
        }

    }

    /**
     * Method to get company head Office
     * @param null $companyId
     * @return array|bool
     */
    public function getHeadOfficeBranchByCompany($companyId = null){

        global $db;

        $selectedBranchSql = "SELECT * FROM tbl_branches WHERE is_head_office = 1 AND company_id =".$db->sqs($companyId);

        $getSelectedBranchResults = $db->getRow($selectedBranchSql);

        if($getSelectedBranchResults){

            return $getSelectedBranchResults;
        }else{
            return false;
        }

    }

    /**
     * Method to retrieve branch by selected department
     * @param null $branchId
     * @return array|bool
     */
    public function getBranchByDepartment($branchId = null){

        global $db;

        $selectedBranchSql = "SELECT * FROM tbl_branches 
                                        INNER JOIN tbl_departments ON tbl_departments.branch_id = tbl_branches.id
                                        WHERE tbl_branches.id =".$db->sqs($branchId);

        $getSelectedBranchResults = $db->getRow($selectedBranchSql);

        if($getSelectedBranchResults){

            return $getSelectedBranchResults;
        }else{
            return false;
        }

    }

    /**
     * Method to add a new branch
     * @param array $branchData
     * @return bool
     */
    public function addBranch($branchData = array()){

        global $objBranch;
        global $db;


        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $addBranchSql = "INSERT INTO tbl_branches ( 
                                                company_id,	    
												branch_name	,    
												branch_address,	    
												branch_tel_number,    
												branch_fax_number,	    
												branch_email,	    
												is_head_office,
												country_id,	
												region_id,
												created_by,	    
												date_created,	    
												modified_by,	    
												date_modified)
                             value (
                                    ".$db->sqs($branchData['companyId']).",
                                    ".$db->sqs($branchData['branchName']).",
                                    ".$db->sqs($branchData['officeAddress']).",
                                    ".$db->sqs($branchData['officeNumber']).",
                                    ".$db->sqs($branchData['faxNumber']).",
                                    ".$db->sqs($branchData['officeEmail']).",
                                    ".$db->sqs($branchData['isHeadOffice']).",
                                    ".$db->sqs($branchData['countryId']).",
                                    ".$db->sqs($branchData['regionId']).",
                                    ".$db->sqs($branchData['createdBy']).",
                                    ".$db->sqs($dateModified).",
                                    ".$db->sqs($branchData['createdBy']).",
                                    ".$db->sqs($dateModified).
            ")";

        $addBranchResults = $db->query($addBranchSql);

        if($addBranchResults){
            $branchId =  $db->insertId();

            $folderData = array(
                "folderName"=>$branchData['branchName'],
                "parentId" => $branchData['parentId'],
                "companyId" =>$branchData['companyId'],
                "branchId" =>$branchId,
                "created_by"=>$branchData['createdBy'],
                "modified_by"=>$branchData['createdBy']
            );

            if($this->createBranchFolders($folderData)){

                return true;
            }else{

                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * Method to add branch folders
     * @param array $folderData
     * @return bool|int
     */
    public  function  createBranchFolders($folderData = array()){

        global $objFolders;
        $success = false;
        $parentFolderId = null;
        $companyFolderId = null;


        $companyFolderId = $objFolders->getCompanyFolderId($folderData['companyId']);

        if(!is_null($companyFolderId)) {
            $folderData['parentId'] = $companyFolderId;
            //second create branch folder
            $parentFolderId = $objFolders->addNewFolder($folderData);

            $success = $objFolders->createFolderDirectory($folderData,$parentFolderId,$folderData['parentId']);

            //create branch sub folders
            if (!is_null($parentFolderId)) {

                $mainFolderGroup = array("Documents",
                    "Employees",
                    "Suppliers",
                    "Resources",
                    "SHEQTeams",
                    "Inspections");

                $defaultGroupFolders = array("OHS (Health & Safety)", "E (Environment)", "Q (Quality)", "F (Food)");

                $defaultTypesFolders = array("Policies Statements", "Policy", "Procedures", "Instructions (SHEQ)", "Appointments","Forms-Checklists","Forms-Registers");

                $defaultResourceCategoriesFolders = array("Fixed Assets", "Movable Assets", "Raw Materials", "Final Products","Consumables","Emergency Equipment","Motorized Equipment");


                //add main folders
                foreach ($mainFolderGroup as $key => $mainFolderName) {

                    if($mainFolderName == "Documents"){
                        //create document folders
                        $folderGroupData = array(
                            "folderName" => $mainFolderName,
                            "parentId" => $parentFolderId,
                            "companyId" => $folderData['companyId'],
                            "branchId" => $folderData['branchId'],
                            "userId" => $folderData['createdBy'],
                            "modified_by" => $folderData['createdBy']
                        );

                        $parentMainFolderId = $objFolders->addNewFolder($folderGroupData);

                        $success = $objFolders->createFolderDirectory($folderGroupData,$parentMainFolderId,$parentFolderId);
                        //add a group
                        foreach ($defaultGroupFolders as $key => $groupFolderName) {

                            $folderGroupData = array(
                                "folderName" => $groupFolderName,
                                "parentId" => $parentMainFolderId,
                                "companyId" => $folderData['companyId'],
                                "branchId" => $folderData['branchId'],
                                "userId" => $folderData['createdBy'],
                                "modified_by" => $folderData['createdBy']
                            );

                            $parentGroupFolderId = $objFolders->addNewFolder($folderGroupData);

                            $success = $objFolders->createFolderDirectory($folderGroupData,$parentGroupFolderId,$parentMainFolderId);

                            if ($parentGroupFolderId){
                                foreach ($defaultTypesFolders as $key => $value) {

                                    $folderData = array(
                                        "folderName" => $value,
                                        "parentId" => $parentGroupFolderId,
                                        "companyId" => $folderGroupData['companyId'],
                                        "branchId" => $folderGroupData['branchId'],
                                        "userId" => $folderGroupData['createdBy'],
                                        "modified_by" => $folderGroupData['createdBy']
                                    );

                                    $folderId = $objFolders->addNewFolder($folderData);

                                    $success = $objFolders->createFolderDirectory($folderData,$folderId,$parentGroupFolderId);
                                }
                            }
                        }

                    }elseif($mainFolderName == "SHEQTeams"){

                        //create sheqteam folder folders
                        $folderGroupData = array(
                            "folderName" => $mainFolderName,
                            "parentId" => $parentFolderId,
                            "companyId" => $folderData['companyId'],
                            "branchId" => $folderData['branchId'],
                            "userId" => $folderData['createdBy'],
                            "modified_by" => $folderData['createdBy']
                        );

                        $parentMainFolderId = $objFolders->addNewFolder($folderGroupData);

                        $success = $objFolders->createFolderDirectory($folderGroupData,$parentMainFolderId,$parentFolderId);

                        //add a group
                        foreach ($defaultGroupFolders as $key => $groupFolderName) {

                            $folderGroupData = array(
                                "folderName" => $groupFolderName,
                                "parentId" => $parentMainFolderId,
                                "companyId" => $folderData['companyId'],
                                "branchId" => $folderData['branchId'],
                                "userId" => $folderData['createdBy'],
                                "modified_by" => $folderData['createdBy']
                            );

                            $parentGroupFolderId = $objFolders->addNewFolder($folderGroupData);

                            $success = $objFolders->createFolderDirectory($folderGroupData,$parentGroupFolderId,$parentMainFolderId);

                        }

                    }elseif($mainFolderName == "Resources"){
                        //create resources folder folders

                        $folderGroupData = array(
                            "folderName" => $mainFolderName,
                            "parentId" => $parentFolderId,
                            "companyId" => $folderData['companyId'],
                            "branchId" => $folderData['branchId'],
                            "userId" => $folderData['createdBy'],
                            "modified_by" => $folderData['createdBy']
                        );

                        $parentMainFolderId = $objFolders->addNewFolder($folderGroupData);

                        $success = $objFolders->createFolderDirectory($folderGroupData,$parentMainFolderId,$parentFolderId);
                        //add a category
                        foreach ($defaultResourceCategoriesFolders as $key => $categoryFolderName) {

                            $folderGroupData = array(
                                "folderName" => $categoryFolderName,
                                "parentId" => $parentMainFolderId,
                                "companyId" => $folderData['companyId'],
                                "branchId" => $folderData['branchId'],
                                "userId" => $folderData['createdBy'],
                                "modified_by" => $folderData['createdBy']
                            );

                            $parentCategoryFolderId = $objFolders->addNewFolder($folderGroupData);
                            $success = $objFolders->createFolderDirectory($folderGroupData,$parentCategoryFolderId,$parentMainFolderId);

                        }

                    }else{
                        //Create main folders
                        $folderGroupData = array(
                            "folderName" => $mainFolderName,
                            "parentId" => $parentFolderId,
                            "companyId" => $folderData['companyId'],
                            "branchId" => $folderData['branchId'],
                            "userId" => $folderData['createdBy'],
                            "modified_by" => $folderData['createdBy']
                        );

                        $parentMainFolderId = $objFolders->addNewFolder($folderGroupData);

                        $success = $objFolders->createFolderDirectory($folderGroupData,$parentMainFolderId,$parentFolderId);

                    }

                }


                return $success;
            }

            return $success;
        }
        return $success;
    }

    /**
     * Method to edit a  branch
     * @param array $branchData
     * @return bool
     */
    public function editBranch($branchData = array()){

        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $editBranchInfoSql = "UPDATE tbl_branches SET 
                                                      branch_name = ".$db->sqs($branchData['branchName']).",
                                                      branch_address = ".$db->sqs($branchData['officeAddress']).",
                                                      branch_tel_number = ".$db->sqs($branchData['officeNumber']).",
                                                      branch_fax_number = ".$db->sqs($branchData['faxNumber']).",
                                                      branch_email = ".$db->sqs($branchData['officeEmail']).",
                                                      modified_by= ".$db->sqs($branchData['modifiedBy']).",
                                                      region_id = ".$db->sqs($branchData['regionId']).",
                                                      country_id = ".$db->sqs($branchData['countryId']).",
                                                      date_modified= ".$db->sqs($dateModified)."

                                      WHERE id=".$db->sqs($branchData['branchId']);

        $editBranchInfoResults = $db->query($editBranchInfoSql);

        if($editBranchInfoResults){
            return true;
        }else{
            return false;
        }
    }



}