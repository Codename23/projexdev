<?php
/**
 * Department class containing all department methods
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class departments
{


    /**
     * Initialise service class
     * departments constructor.
     */
    function __construct()
    {

    }


    /**
     * Method to get all departments
     * @return array
     */
    public function getAllDepartments(){

        global $db;

        $selectedDepartmentSql = "SELECT  id,branch_id,company_id,
                                          department_name,
                                          department_building,
                                          department_floor,
                                  is_head_office,created_by,date_created,modified_by,date_modified
                            FROM tbl_departments";

        $getDepartmentResult = $db->getAll($selectedDepartmentSql);

        if($getDepartmentResult){
            return $getDepartmentResult ;
        }else{
            return array();
        }

    }

    /**
     * Method yo get department info
     * @param null $departmentId
     * @return array
     */
    public function getAllDepartmentsByListId($departments)
    {
        global $db;
        $selectedDepartmentSql = "SELECT department_name FROM tbl_departments WHERE id in (".$db->sqs($departments).")";
        $clean_string = str_replace("'", "", $selectedDepartmentSql);
        $getDepartmentResult = $db->getAll($clean_string);        
        return $getDepartmentResult;
    }
    public function getAllDepartmentsByGroupId($departments)
    {
        global $db;
        $selectedDepartmentSql = "SELECT department_name FROM tbl_departments WHERE id in (".$db->sqs($departments).")";
        $clean_string = str_replace("'", "", $selectedDepartmentSql);
        $getDepartmentResult = $db->getAll($clean_string);
        $str = "";
        $counter  = 0;
        foreach ($getDepartmentResult as $dep)
        {
            $counter++;
            
            if($counter ==  1)
            {
                $str = $dep["department_name"];
            }
            if($counter > 1)
            {
                $str .= ", ".$dep["department_name"];
            }
            
        }
        return $str;
    }
    public  function getDepartmentById($departmentId = null){

        global $db;

        $selectedDepartmentSql = "SELECT id,branch_id,company_id,department_name,department_building,department_floor,
                                  is_head_office,created_by,date_created,modified_by,date_modified
                            FROM tbl_departments
                            WHERE id=".$db->sqs($departmentId);

        $getDepartmentResult = $db->getAll($selectedDepartmentSql);

        if($getDepartmentResult){
            return $getDepartmentResult ;
        }else{
            return array();
        }

    }


    /**
     * Method to get department name
     * @param null $departmentId
     * @return array|bool|int|String
     */
    public function getDepartmentName($departmentId = null){

        global $db;

        $selectedDepartmentSql = "SELECT department_name
                            FROM tbl_departments
                            WHERE id=".$db->sqs($departmentId);

        $getDepartmentResult = $db->getOne($selectedDepartmentSql);

        if($getDepartmentResult){
            return $getDepartmentResult ;
        }else{
            return array();
        }
    }

    /**
     * Method to count company departments
     * @param null $companyId
     * @return array|bool
     */
    public function getNumberOfDepartments($companyId = null){


        global $db;

        $allDepartmentsSql = "SELECT tbl_departments.id,
                                      tbl_departments.department_name,
                                      tbl_departments.department_building,
                                      tbl_departments.department_floor,
                                      tbl_departments.is_head_office,
                                      tbl_departments.created_by,
                                      tbl_departments.date_created,
                                      tbl_departments.modified_by,
                                      tbl_departments.date_modified
                            FROM tbl_departments,tbl_branches
                            WHERE tbl_branches.id = tbl_departments.branch_id
                            AND  tbl_branches.company_id=".$db->sqs($companyId);


        $getAllDepartmentsResult = $db->query($allDepartmentsSql,true);

        return $db->getRowsReturned();
    }

    /**
     * Method to count company departments
     * @param null $branchId
     * @return array|bool
     */
    public function getNumberOfBranchDepartments($branchId = null){


        global $db;

        $allDepartmentsSql = "SELECT tbl_departments.id,
                                      tbl_departments.department_name,
                                      tbl_departments.department_building,
                                      tbl_departments.department_floor,
                                      tbl_departments.is_head_office,
                                      tbl_departments.created_by,
                                      tbl_departments.date_created,
                                      tbl_departments.modified_by,
                                      tbl_departments.date_modified
                            FROM tbl_departments,tbl_branches
                            WHERE tbl_branches.id = tbl_departments.branch_id
                            AND  tbl_departments.branch_id =".$db->sqs($branchId);


        $getAllDepartmentsResult = $db->query($allDepartmentsSql,true);

        return $db->getRowsReturned();
    }
    /**
     * Method to get department by company
     * @param null $companyId
     * @return array
     */
    public function getDepartmentByCompany($companyId = null){

        global $db;

        $allDepartmentsSql = "SELECT tbl_departments.id,
                                      tbl_departments.department_name,
                                      tbl_departments.department_building,
                                      tbl_departments.department_floor,
                                      tbl_departments.is_head_office,
                                      tbl_departments.created_by,
                                      tbl_departments.date_created,
                                      tbl_departments.modified_by,
                                      tbl_departments.date_modified,
                                      tbl_branches.branch_name
                            FROM tbl_departments,tbl_branches
                            WHERE tbl_branches.id = tbl_departments.branch_id
                            AND  tbl_branches.company_id=".$db->sqs($companyId);


        $getAllDepartmentsResult = $db->getAll($allDepartmentsSql);

        if($getAllDepartmentsResult){
            return $getAllDepartmentsResult;
        }else{
            return false;
        }

    }

    /**
     * Method to retrieve departments by branch id
     * @param null $branchId
     * @return array|bool
     */
    public function getDepartmentByBranch($branchId = null){

        global $db;

        $allDepartmentsSql = "SELECT id,department_name,department_building,department_floor,
                                  is_head_office,created_by,date_created,modified_by,date_modified
                            FROM tbl_departments
                            WHERE  branch_id =".$db->sqs($branchId);


        $getAllDepartmentsResult = $db->getAll($allDepartmentsSql);

        if($getAllDepartmentsResult){
            return $getAllDepartmentsResult;
        }else{
            return array();
        }

    }


    /**
     * Method to add a new department
     * @param $departmentData
     * @return bool
     */
    public function addDepartment($departmentData = array()){

        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        if($this->departmentExists($departmentData['departmentName'],$departmentData['branchId'],$departmentData['companyId'])){

            return false;
        }

        $addDepartmentSql = "INSERT INTO tbl_departments  ( 
                                            branch_id,	    
											company_id,	    
											department_name,    
											department_building,  
											department_floor,	    
											is_head_office,	    
											created_by,	    
											date_created,	    
											modified_by,	    
											date_modified
													  )
                          value (".$db->sqs($departmentData['branchId']).",
                           ".$db->sqs($departmentData['companyId']).",
                           ".$db->sqs($departmentData['departmentName']).",
                           ".$db->sqs($departmentData['buildingName']).",
                           ".$db->sqs($departmentData['floorName']).",
                           0,
                           ".$db->sqs($departmentData['createdBy']).",
                           ".$db->sqs($dateModified).",
                           ".$db->sqs($departmentData['createdBy']).",
                           ".$db->sqs($dateModified).")";

        $addDepartmentResults = $db->query($addDepartmentSql);

        if($addDepartmentResults){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Method to edit branch departments
     * @param array $departmentData
     * @return bool
     */
    public function editDepartment($departmentData = array() ){

        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $editDepartmentInfoSql = "UPDATE tbl_departments SET    
													department_name	= ".$db->sqs($departmentData['departmentName']).",  
													department_building	= ".$db->sqs($departmentData['buildingName']).",  
													department_floor = ".$db->sqs($departmentData['floorName']).",  
													is_head_office = 0,  
													modified_by	= ".$db->sqs($departmentData['modifiedBy']).",  
													date_modified = ".$db->sqs($dateModified)."
                                    WHERE id=".$db->sqs($departmentData['departmentId']) ."
                                    AND branch_id = ".$db->sqs($departmentData['branchId']) ;


        $editDepartmentInfoResults = $db->query($editDepartmentInfoSql);

        if($editDepartmentInfoResults){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Method to check if department exists
     * @param null $departmentName
     * @param null $branchId
     * @param null $companyId
     * @return bool
     */
    public function departmentExists($departmentName = null,$branchId = null,$companyId = null)
    {
        global $db;
        $sql = "SELECT id FROM tbl_departments WHERE 
                              department_name LIKE  '%".$db->sqs($departmentName)."%'
                               AND branch_id =".$db->sqs($branchId)."
                               AND company_id = ".$db->sqs($companyId);
        $response = $db->getOne($sql);
        if(is_array($response) && count($response)>0){
            return TRUE;
        } else {
            return FALSE;
        }
    }


}