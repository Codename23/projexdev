<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of jobs_class
 *
 * @author Innovatorshill
 */
class jobs {
    
    public function __construct() {
        
    }
    //create job
    public static function hasCreatedJob($jobData = array())
    {
         global $db;
        $response = array();
           $sql = "INSERT INTO tbl_jobs(source_id, source_type, job_description, start_date, end_date, responsible_persons,"
                   . " department_involved, maintenance_type,  job_details, risk_level, hazard_types, supplier_id,resources_used,parts_used,"
                   . " approval_required,created_by)"
                        . " VALUES (" . $db->sqs($jobData["source_id"]). ","
                        . $db->sqs($jobData['source_type']) . ","
                        . $db->sqs($jobData['job_description']). ", "
                        . $db->sqs($jobData['start_date']). ", "
                        . $db->sqs($jobData['end_date']). ", "
                        . $db->sqs($jobData['responsible_persons']) . ", "
                        . $db->sqs($jobData['department_involved']) . ", "
                        . $db->sqs($jobData['maintenance_type']) . ", "
                        . $db->sqs($jobData['job_details']) . ", "
                        . $db->sqs($jobData['risk_level']) . ", "
                        . $db->sqs($jobData['hazard_types']) . ", "
                        . $db->sqs($jobData['supplier_id']) . ", "
                        . $db->sqs($jobData['resources_used']) . ", "
                        . $db->sqs($jobData['parts_used']) . ", "
                        . $db->sqs($jobData['approval_required']) . ", " 
                        . $db->sqs($_SESSION['user_id']) . ")";
                $response['response'] = $db->query($sql);
                if($response['response'])
                {
                  return $response;
                }
                else
                {
                    return false;
                }  
    }
    
    //for statutory
    public static function getAllStatutoryJobs()
    {
       global $db;
        $response = array();
        $sql = "select tbl_jobs.id,tbl_jobs.start_date,tbl_statutory_maintenance_plan.int_ex,tbl_statutory_maintenance_plan.description,
tbl_jobs.department_involved,tbl_jobs.risk_level,tbl_jobs.hazard_types,tbl_jobs.approval_required,1,
tbl_jobs.responsible_persons
from tbl_jobs
inner join tbl_statutory_maintenance_plan
on tbl_jobs.source_id = tbl_statutory_maintenance_plan.id
where tbl_statutory_maintenance_plan.company_id = '{$db->sqs($_SESSION['company_id'])}'
and tbl_jobs.`signedOff` is null";
        $response = $db->getAll($sql);
        if($response)
        {
          return $response;
        }
        else
        {
            return false;
        }  
    } 
    //for requested maintenance
        //for statutory
    public static function getAllMaintenanceRequestedJobs()
    {
       global $db;
        $response = array();
        $sql = "select tbl_jobs.id,tbl_jobs.start_date,tbl_maintenance_request.description,tbl_maintenance_request.source as rs,tbl_maintenance_request.priority,
tbl_jobs.department_involved,tbl_jobs.risk_level,tbl_jobs.hazard_types,tbl_jobs.approval_required,1,
tbl_jobs.responsible_persons
from tbl_jobs
inner join tbl_maintenance_request
on tbl_jobs.source_id = tbl_maintenance_request.id
where tbl_maintenance_request.id = tbl_jobs.source_id and tbl_jobs.source_type = 'Maintenance' and
tbl_maintenance_request.company_id = '{$db->sqs($_SESSION['company_id'])}'
and tbl_jobs.`signedOff` is null";
        $response = $db->getAll($sql);
        if($response)
        {
          return $response;
        }
        else
        {
            return false;
        }  
    }
}
