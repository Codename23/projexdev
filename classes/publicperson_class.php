<?php
/**
 * Class containing all methods providing asset functionality
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

class publicperson
{
    /**
     * Method to initiate the class
     * 
     * @access public
     */
    public function __construct() 
    {
      
    }
    
    /**
     * Method to add asset involved
     * @param array $incidentArr
     */
    public function addPublicPerson($publicpersonDetsArr){
        global $db;
        if (is_array($publicpersonDetsArr) && count($publicpersonDetsArr) > 0) {
             if(isset($publicpersonDetsArr['firstname']) && isset($publicpersonDetsArr['contact_number'])){
                $sql = "INSERT INTO tbl_publicperson (incident_id, first_name, lastname, id_number, contact_number, email, company_name, created_by)
                        VALUES (". $db->sqs($_SESSION['incidentid'])." , ". $db->sqs($publicpersonDetsArr['firstname'])." , ". $db->sqs($publicpersonDetsArr['lastname'])." , ". $db->sqs($publicpersonDetsArr['id_number'])." , ". $db->sqs($publicpersonDetsArr['contact_number'])." , ". $db->sqs($publicpersonDetsArr['email'])." , ". $db->sqs($publicpersonDetsArr['company'])." , ". $db->sqs($_SESSION['user_id'])." )";
                $db->query($sql);
                $response = $db->insertId();
                return $response;   
             }else{
                return "Required information is missing";
             }
        }else{
            return FALSE;
        }
    }
    /**
     * Method to edit asset involved
     */
    public function editPublicPerson($publicpersonDetsArr){
        global $db;
        if(is_array($publicpersonDetsArr) && count($publicpersonDetsArr)>0){
            if(isset($_SESSION['incidentid']) && isset($publicpersonDetsArr['description'])){
                $sql = "UPDATE tbl_publicperson SET first_name =".$db->sqs($publicpersonDetsArr['firstname']).",lastname =".$db->sqs($publicpersonDetsArr['lastname'])." , id_number =".$db->sqs($publicpersonDetsArr['id_number']).", contact_number =" .$db->sqs($publicpersonDetsArr['contact_number']).", email = " . $db->sqs($publicpersonDetsArr['email']) .", company_name = " . $db->sqs($publicpersonDetsArr['company']) .", modified_by = ".$db->sqs($_SESSION['user_id'])." WHERE id = ".$db->sqs($publicpersonDetsArr['publicpersonid']);
                $result = $db->query($sql);
                return $result;                   
            } else {
                return 'Required information missing.';
            }
        }        
    }
    /*
     * Method to get asset involved
     */
    public function getPublicPerson($id){
        global $db;
        if(isset($id)){
            $sql = "SELECT * FROM tbl_publicperson WHERE id =".$db->sqs($id);
            $response = $db->getRow($sql);
            return $response;  
        }
    }
    
    /**
     * Method to get asset involved
     */
    public function getAllPublicPerson(){
        global $db;
        $sql = "SELECT * FROM tbl_publicperson WHERE incident_id =".$db->sqs($_SESSION['incidentid']);
        $response = $db->getAll($sql);
        return $response;
    }
   
}
