<?php
/** 
 * Class for page setup
 * 
 * @package sheqonline
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

//Include the common file
require_once 'config.php';

class page
{
    /**
     * Constructor 
     */
    public function __construct(){

    }
    
    /**
     * Start the head section of the page
     * @return void
     */
    public function startHeader(){
    echo'<!DOCTYPE html>
            <html lang="en">
            <head>';
        $this->includeMetaData();
    echo "\n<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>\n";
            $javascript = array('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js',
                                'https://oss.maxcdn.com/respond/1.4.2/respond.min.js');
            $this->includeExternalScript($javascript);
    echo "\n<![endif]-->";
    }
    
    /**
     * Include meta data
     * @return void
     */
    public function includeMetaData(){
        echo '<meta charset="utf-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <meta name="description" content="Sheqonline system">
              <meta name="author" content="Sheqonline">';
    }
    
    /**
     * Include a stylesheet from the css folder
     * @param array $stylesheet
     * @return void
     */
    public function includeStyle($stylesheets){    
        foreach($stylesheets as $filename){
            $path[] = "frontend/css/" . $filename; 
        } 
        $this->includeExternalStyle($path);
    }

    /**
     * Include an external stylesheet from another domain
     * @param array $externalstylesheet
     * @return void
     */
    public function includeExternalStyle($externalstylesheet){
        foreach($externalstylesheet as $location){
         echo "<link rel='stylesheet' type='text/css' href='" . $location . "' />\n";  
        }   
    }

    /**
     * Include a javascript file from the js folder
     * @param string $javascripts
     * @return filename
     */
    public function includeScript($javascripts){
        foreach($javascripts as $filename){
          $path[] = "frontend/js/" . $filename;  
        }
        $this->includeExternalScript($path);
    }

    /**
     * Inlcude an external javascript
     * @param array $javascript
     * @return void
     */

    public function includeExternalScript($javascript){
        foreach($javascript as $location){
          echo $string = "<script src='" . $location . "'></script>\n";  
        }
        
    }

    /**
     * Set title of the page
     * @param string $title
     * @return void
     */
    public function setTitle($title){
        echo "\n<title>Sheqonline | " . $title . "</title>\n";
    }

    /**
     * Set page heading name
     * @param string $heading
     * @return void
     */
    public function setHeading($heading){
        echo "\n<h1>" . $heading . "</h1>\n";
    }

    /**
     * Set page url 
     * @param string $url 
     * @param array $params
     * @return void
     */
    public function setUrl($url, $params){

    }

    /**
     * End the head section of the page
     * @return void
     */
    public function endHeader(){
        echo "</head>\n<body>";
    }

    /**
     * Get footer
     * @return void
     */
    public function getFooter(){
        echo "</body>\n</html>";
    }
}

$objPage = new page();
