<?php
/**
 * Controller class containing methods to process all client actions
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class client extends controller{


    /**
     * Method to view all clients
     */
    public static function viewAllClients(){

        $data = array();
        global $objClient;

        $branchId = $_SESSION['branchId'];
        $data['companyClients']= $objClient->getAllCompanyClients($branchId);
        parent::setVariables('title','client Info');
        parent::setView('templates/client','index_tpl',$data);
    }


    /**
     * Method to add a new client
     */
    public static function addClient(){

        global $objClient;

        if(!empty($_POST)){

            $clientData = array();

            $clientData['companyIndividual'] = $_POST['companyIndividual'];

            if($_POST['companyIndividual'] == 'company') {

                $clientData['companyType'] = 1;
                $clientData['companyName'] = $_POST['companyName'];
                $clientData['ptyNumber'] = $_POST['ptyNumber'];
                $clientData['vatNumber'] = $_POST['vatNumber'];
                $clientData['officeNumber'] = $_POST['officeNumber'];
                $clientData['contactPerson'] = $_POST['contactPerson'];
                $clientData['contactNumber'] = $_POST['contactNumber'];
                $clientData['email'] = $_POST['email'];
                $clientData['website'] = $_POST['website'];
                $clientData['headOfficeDetails'] = $_POST['headOfficeDetails'];
                $clientData['headOfficeTown'] = $_POST['headOfficeTown'];
                $clientData['headOfficeProvince'] = $_POST['headOfficeProvince'];

            }else{

                $clientData['companyType'] = 2;
                $clientData['individualName'] =  $_POST['individualName'];
                $clientData['individualSurname'] = $_POST['individualSurname'];
                $clientData['individualGender'] = $_POST['individualGender'];
                $clientData['individualEmail']= $_POST['individualEmail'];
                $clientData['individualContactNumber'] = $_POST['individualContactNumber'];
                $clientData['individualAddress'] = $_POST['individualAddress'];
                $clientData['town'] = $_POST['town'];
                $clientData['province'] = $_POST['province'];

                $clientData['reasonForListing'] = $_POST['reasonForListing'];
                $clientData['uploadDocuments'] = $_POST['uploadDocuments'];

            }

            $clientData['branchId'] =  $_SESSION['branchId'];
            $clientData['createdBy'] = $_SESSION['userid'];
            $clientData['modifiedBy']= $_SESSION['userid'];

            $addClient = $objClient->addClient($clientData);

            if($addClient){
                parent::nextPage('viewAllclients','client');
            }else{
                parent::nextPage('addClient','client');
            }

        }else{

            $data = array();

            $data['companyTypes'] = $objClient->getAllCompanyTypes();

            parent::setVariables('title','Add client');
            parent::setView('templates/client','add_client_tpl',$data);
        }

    }


    /**
     * Method to view client information
     */
    public static function viewClientInfo(){

        global $objClient;

        $data = array();
        $id = intval($_GET['id']);
        $data['clientInfo'] = $objClient->getClientInfo($id);

        parent::setVariables('title', 'Customer');
        if($data['clientInfo']['client_type'] == 1){
            parent::setView('templates/client', 'company_details_tpl', $data);
        }else{
            parent::setView('templates/client', 'individual_details_tpl', $data);
        }

    }

    /**
     * Method to view client information
     */
    public static function editClientInfo(){

        global $objClient;

        if(!empty($_POST)) {
            $clientData = array();
            if ($_POST['companyType'] == 1) {

                $clientData['companyType'] = $_POST['companyType'];
                $clientData['companyName'] = $_POST['companyName'];
                $clientData['ptyNumber'] = $_POST['ptyNumber'];
                $clientData['vatNumber'] = $_POST['vatNumber'];
                $clientData['officeNumber'] = $_POST['officeNumber'];
                $clientData['contactPerson'] = $_POST['contactPerson'];
                $clientData['contactNumber'] = $_POST['contactNumber'];
                $clientData['email'] = $_POST['email'];
                $clientData['website'] = $_POST['website'];
                $clientData['headOfficeDetails'] = $_POST['headOfficeDetails'];
                $clientData['headOfficeTown'] = $_POST['headOfficeTown'];
                $clientData['headOfficeProvince'] = $_POST['headOfficeProvince'];

            }else{

                $clientData['companyType'] = $_POST['companyType'];
                $clientData['individualName'] =  $_POST['individualName'];
                $clientData['individualSurname'] = $_POST['individualSurname'];
                $clientData['individualGender'] = $_POST['individualGender'];
                $clientData['individualEmail']= $_POST['individualEmail'];
                $clientData['individualContactNumber'] = $_POST['individualContactNumber'];
                $clientData['individualAddress'] = $_POST['individualAddress'];
                $clientData['town'] = $_POST['town'];
                $clientData['province'] = $_POST['province'];

                $clientData['reasonForListing'] = $_POST['reasonForListing'];
                $clientData['uploadDocuments'] = $_POST['uploadDocuments'];

            }
            $clientData['clientId']= $_POST['companyId'];
            $clientData['modifiedBy']= $_SESSION['userid'];

            $editClient = $objClient->editClient($clientData);

            if($editClient){
                parent::nextPage("viewClientInfo&id=".$_POST['companyId'],"client");

            }else{
                parent::nextPage("viewAllClients","client");
            }
        }
    }

    /**
     * Method to remove selected client
     */
    public static function deleteClient(){

        global $objClient;

        $id = intval($_GET['id']);
        $deleteClient = $objClient->deleteClient($id);

        if($deleteClient){
            parent::nextPage("viewAllClients","client");

        }else{
            parent::nextPage("viewAllClients","client");
        }

    }

    /**
     * Method to request for a new quotation
     */
    public static function requestQuote(){

        if(!empty($_POST)){

        }else{

            $data = array();
            parent::setVariables('title','Quotation');
            parent::setView('templates/client','request_quote_tpl',$data);

        }

    }

}