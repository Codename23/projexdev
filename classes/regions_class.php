<?php
/**
 * Regions class containing all region methods
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class regions
{


    /**
     * Initialise service class
     * providers constructor.
     */
    function __construct()
    {

    }


    /**
     * Method to get all companies
     * @return array|bool
     */
    public function getAllCountries(){

        global $db;

        $allCountriesSql = "SELECT * FROM tbl_countries";

        $getAllCountriesResults = $db->getAll($allCountriesSql);


        if($getAllCountriesResults){

            return $getAllCountriesResults;
        }else{

            return false;
        }

    }

    /**
     * Method to get all companies
     * @param null $countryId
     * @return array|bool
     */
    public function getCountryRegions($countryId = null){

        global $db;

        $allRegionsSql = "SELECT * FROM tbl_regions WHERE country =".$db->sqs($countryId);

        $getAllRegionsResults = $db->getAll($allRegionsSql);


        if($getAllRegionsResults){

            return $getAllRegionsResults;
        }else{

            return false;
        }

    }


    /**
     * Method to regions by company
     * @param null $companyId
     * @return array|bool
     */
    public function getCountryRegionsByCompany($companyId = null){

        global $db;

        $allRegionsSql = "SELECT tbl_regions.id,tbl_regions.name
                                            FROM  tbl_regions,tbl_countries, tbl_companies
                                            WHERE tbl_regions.country = tbl_countries.iso
                                            AND tbl_companies.country_id  = tbl_countries.iso
                                            AND tbl_companies.id  =".$db->sqs($companyId);


        $getAllRegionsResults = $db->getAll($allRegionsSql);


        if($getAllRegionsResults){

            return $getAllRegionsResults;
        }else{

            return false;
        }

    }




}