<?php
/**
 * Provider class containing all provider methods
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class providers
{


    /**
     * Initialise provider class
     * providers constructor.
     */
    function __construct()
    {

    }


    /**
     * Method to retrieve all providers based on user's branch
     * @param null $branchId
     * @return array|bool
     */
    public function getAllProviders($branchId = null){

        global $db;

        $allProvidersSql = "SELECT * FROM tbl_service_providers
                                      WHERE is_deleted = 0
                                      AND branch_id =".$db->sqs($branchId);

        $getAllProvidersResults = $db->getAll($allProvidersSql);


        if($getAllProvidersResults){

            return $getAllProvidersResults;
        }else{

            return false;
        }

    }

    /**
     * Method to get selected provider results
     * @param null $providerId
     * @return array|bool
     */
    public function getProviderInfo($providerId = null){

        global $db;

        $selectedProviderSql = "SELECT * FROM tbl_service_providers WHERE id =".$db->sqs($providerId);

        $providerInfoResults = $db->getRow($selectedProviderSql);

        if($providerInfoResults){

            return $providerInfoResults;
        }else{
            return false;
        }

    }

    /**
     * Method to add a new provider
     * @param array $providerData
     * @return bool
     */
    public function addProvider($providerData = array()){

        global $db;

        $date = new DateTime();
        $dateCreated =  date_format($date, 'Y-m-d H:i:s');

        $addProviderSql = "INSERT INTO tbl_service_providers(
                                                branch_id,
                                                provider_name,
                                                vat_number,
                                                pty_number, 
                                                office_number, 
                                                contact_person,
                                                cell_number,
                                                provider_email, 
                                                website,
                                                bee_certificate, 
                                                provider_address, 
                                                provider_town,
                                                province, 
                                                supporting_document,
                                                service_type,
                                                business_description, 
                                                service_frequency, 
                                                is_construction,
                                                is_high_risk, 
                                                compliance_description, 
                                                is_audited,
                                                audit_type, 
                                                audit_frequency, 
                                                next_audit,
                                                created_by,
                                                date_created,
                                                modified_by, 
                                                date_modified
                                                ) VALUES(
                                                 ".$db->sqs($providerData['branchId']).",
                                                  ".$db->sqs($providerData['companyName']).",
                                                  ".$db->sqs($providerData['vatNumber']).",
                                                  ".$db->sqs($providerData['ptyNumber']).",
                                                  ".$db->sqs($providerData['officeNumber']).",
                                                  ".$db->sqs($providerData['contactPerson']).",
                                                  ".$db->sqs($providerData['contactNumber']).",
                                                  ".$db->sqs($providerData['email']).",
                                                  ".$db->sqs($providerData['website']).",
                                                  ".$db->sqs($providerData['beeCertificate']).",
                                                  ".$db->sqs($providerData['headOfficeDetails']).",
                                                  ".$db->sqs($providerData['headOfficeTown']).",
                                                  ".$db->sqs($providerData['headOfficeProvince']).",
                                                  ".$db->sqs($providerData['uploadDocuments']).",
                                                  ".$db->sqs($providerData['serviceType']).",
                                                  ".$db->sqs($providerData['providerDetails']).",
                                                  ".$db->sqs($providerData['serviceFrequency']).",
                                                  ".$db->sqs($providerData['complianceConstruction']).",
                                                  ".$db->sqs($providerData['complianceHighRisk']).",
                                                  ".$db->sqs($providerData['complianceDescription']).",
                                                  ".$db->sqs($providerData['applicableServiceProvider']).",
                                                  ".$db->sqs($providerData['auditType']).",
                                                  ".$db->sqs($providerData['auditFrequency']).",
                                                  ".$db->sqs($providerData['nextAuditMonth']).",
                                                  ".$db->sqs($providerData['createdBy']).",
                                                  ".$db->sqs($dateCreated).",
                                                  ".$db->sqs($providerData['modifiedBy']).",
                                                  ".$db->sqs($dateCreated)."
                                                )";


        $addProviderResult = $db->query($addProviderSql);

        if($addProviderResult){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Method to update provider data
     * @param array $providerData
     * @return bool
     */
    public function editProvider($providerData = array()){

        global $db;
        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $editProviderSql = "UPDATE tbl_service_providers SET 
                                                provider_name =".$db->sqs($providerData['companyName']).",
                                                vat_number = ".$db->sqs($providerData['vatNumber']).",
                                                pty_number =".$db->sqs($providerData['ptyNumber']).",
                                                office_number = ".$db->sqs($providerData['officeNumber']).", 
                                                contact_person = ".$db->sqs($providerData['contactPerson']).",
                                                cell_number = ".$db->sqs($providerData['contactNumber']).",
                                                provider_email = ".$db->sqs($providerData['email']).",
                                                website = ".$db->sqs($providerData['website']).",
                                                provider_address = ".$db->sqs($providerData['headOfficeDetails']).",
                                                provider_town = ".$db->sqs($providerData['headOfficeTown']).",
                                                province = ".$db->sqs($providerData['headOfficeProvince']).",
                                                supporting_document = ".$db->sqs($providerData['uploadDocuments']).",
                                                service_type = ".$db->sqs($providerData['serviceType']).",
                                                business_description = ".$db->sqs($providerData['providerDetails']).", 
                                                service_frequency = ".$db->sqs($providerData['serviceFrequency']).",
                                                is_construction = ".$db->sqs($providerData['complianceConstruction']).",
                                                is_high_risk = ".$db->sqs($providerData['complianceHighRisk']).",
                                                compliance_description = ".$db->sqs($providerData['complianceDescription']).", 
                                                is_audited = ".$db->sqs($providerData['applicableServiceProvider']).",
                                                audit_type = ".$db->sqs($providerData['auditType']).",
                                                audit_frequency = ".$db->sqs($providerData['auditFrequency']).",
                                                next_audit = ".$db->sqs($providerData['nextAuditMonth']).",
                                                modified_by = ".$db->sqs($providerData['modifiedBy']).",
                                                date_modified = ".$db->sqs($dateModified)."
                             WHERE id={$db->sqs($providerData['providerId'])}";


        $editProviderResults = $db->query($editProviderSql);

        if($editProviderResults){
            return true;
        }else{
            return false;
        }

    }

    /**
     * Method to delete service provider
     * @param null $providerId
     * @return bool
     */
    public function deleteProvider($providerId = null){

        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $deleteProviderSql = "UPDATE tbl_service_providers SET 
                                    is_deleted	=  1,
                                    date_modified =".$db->sqs($dateModified)."
                        WHERE id={$db->sqs($providerId)}";

        $deleteProviderResults = $db->query($deleteProviderSql);

        if($deleteProviderResults){
            return true;
        }else{
            return false;
        }

    }
    /**
     * Method to search client
     */
    public function searchProvider($companyName, $fullname){
        global $db;

        $sql = "SELECT * FROM tbl_service_providers WHERE id = " . $db->sqs($companyName) ." AND contact_person LIKE '%" . $fullname."%'";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
    /**
     * Method 
     */
     public function getProviderCompanies(){
        global $db;
        $sql = "SELECT id, provider_name FROM tbl_service_providers";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }

}