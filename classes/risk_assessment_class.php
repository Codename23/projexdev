<?php
/**
 * Service class containing all service methods
 *
 * @package sheqonline
 * @author Emmanuel van der Westhuizen <emmanuel@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class risk_assessments
{


    /**
     * Initialise service class
     * providers constructor.
     */
    public $hasCreatedRisk = false;
    public $requested_id = 0;
    public $getStartRA_Id = 0;
    public $hasUpdatedRisk = false;
    function __construct()
    {

    }    
    /////////////////////////////
    public function addRARequest($data = array())    
    {
        if(is_array($data))
        {
             global $db;   
            $response = array();         
                    $sql = "INSERT INTO tbl_risk_assessment_request (source_id, source, due_date, priority, "
                            . "template_id,person_responsible, notes,branch_id,company_id,created_by) "
                            . " VALUES (". $db->sqs($data['source_id'])." , "
                            . $db->sqs($data['source'])." , "
                            . $db->sqs($data['due_date'])." , "
                            . $db->sqs($data['priority'])." , "
                            . $db->sqs($data['template_id'])." , "
                            . $db->sqs($data['person_responsible'])." , "
                            . $db->sqs($data['notes'])." , "
                            . "". $db->sqs($_SESSION['branch_id'])." , "
                            . "". $db->sqs($_SESSION['company_id'])." , "
                            . "" .$db->sqs($_SESSION['user_id']) ." )";                    
                    $response = $db->query($sql);                       
                    if($response){
                        $this->requested_id = $db->insertId();
                        return $response;
                    }else{
                        return false;
                    }  
            } 
            else
            {
               return "Required information is missing";
            }     
    }
   public static function addRequestOnSchedule($data)
    {
         global $db;
        $sql = "UPDATE tbl_risk_assessment_request SET requested_status = 'Scheduled',upcoming_date = ".$db->sqs($data["suggestedDate"]).","
                . "risk_assessor = ".$db->sqs($data["riskAssessor"]). "                                    
                WHERE tbl_risk_assessment_request.id = ".$db->sqs($data["requested_risk_id"]);
        $result = $db->query($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }       
    } 
    
    public static function getRequeqestedRAUpcoming()
    {
          global $db;
            $sql = "SELECT tbl_risk_assessment_request.*,concat(tbl_users.firstname,' ',tbl_users.lastname) as requested_by,tbl_risk_assessment_start.risk_template_id,
             tbl_risk_assessment_start.risk_type_id,tbl_risk_assessment_start.id as start_id,tbl_risk_assessment_start.is_draft                   
            from tbl_risk_assessment_request
            inner join tbl_users
            on tbl_risk_assessment_request.created_by = tbl_users.id
            left join tbl_risk_assessment_start
            on tbl_risk_assessment_request.id = tbl_risk_assessment_start.request_id
            where tbl_risk_assessment_request.is_active = 1
            and tbl_risk_assessment_request.requested_status = 'Scheduled' and tbl_risk_assessment_request.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            //echo $sql;
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
    public static function getAllRegistered()
    {
          global $db;
            $sql = "SELECT 
                DATE_FORMAT(tbl_risk_assessment_start.date_modified,'%m/%d/%Y') as date_submitted,
                tbl_risk_assessment_request.*,concat(tbl_users.firstname,' ',tbl_users.lastname) as assessor,tbl_risk_assessment_start.risk_template_id,
             tbl_risk_assessment_start.risk_type_id,tbl_risk_assessment_start.id as start_id,tbl_risk_assessment_start.is_draft,tbl_risk_assessment_start.description
             as risk_description,tbl_risk_assessment_start.departments_list,tbl_risk_assessment_start.occupation_list,tbl_risk_assessment_start.resource_list
            from tbl_risk_assessment_request
            inner join tbl_risk_assessment_start
            on tbl_risk_assessment_request.id = tbl_risk_assessment_start.request_id
            inner join tbl_users
            on tbl_risk_assessment_start.created_by = tbl_users.id
            where tbl_risk_assessment_request.is_active = 1
            and tbl_risk_assessment_request.requested_status = 'Assessed' and tbl_risk_assessment_request.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            //echo $sql;
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
    public static function getRequeqestedRA()
    {
          global $db;
            $sql = "SELECT tbl_risk_assessment_request.*,concat(tbl_users.firstname,' ',tbl_users.lastname) as requested_by
            from tbl_risk_assessment_request
            inner join tbl_users
            on tbl_risk_assessment_request.created_by = tbl_users.id
            where tbl_risk_assessment_request.is_active = 1
            and tbl_risk_assessment_request.requested_status IS NULL and tbl_risk_assessment_request.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
    public function getStartRow($id)
    {
         global $db;
        $sql = "select tbl_risk_assessment_start.*,tbl_risk_assessment_type.type_name,tbl_risk_assessment_templates.template "
                . " from tbl_risk_assessment_start"
                . " inner join tbl_risk_assessment_type"
                . " on tbl_risk_assessment_start.risk_type_id = tbl_risk_assessment_type.id"
                . " inner join tbl_risk_assessment_templates"
                . " on tbl_risk_assessment_start.risk_template_id = tbl_risk_assessment_templates.id "
                . " where tbl_risk_assessment_start.id = ".$db->sqs($id)." and tbl_risk_assessment_start.company_id = ".$db->sqs($_SESSION['company_id']);
        $result = $db->getAll($sql);
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    public function getAllHazard()
    {
        global $db;
        $sql = "select * from tbl_settings_hazards where company_id = ".$db->sqs($_SESSION['company_id']);
        $result = $db->getAll($sql);
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    public function addHazard($hazard,$group)
    {
       global $db;
       $sql = "INSERT INTO tbl_settings_hazards(group_id,company_id,branch_id,hazard,is_active,created_by)
                                VALUES(
                                                  " . $db->sqs($group) . ",
                                                  " . $db->sqs($_SESSION['company_id']) . ",
                                                  " . $db->sqs($_SESSION['branch_id']) . ",
                                                  " . $db->sqs($hazard) . ",
                                                  " . $db->sqs(1) . ",
                                                  " . $db->sqs($_SESSION['user_id']) . ")";
        $result = $db->query($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    public static function loadAssessment($data)
    {
       global $db;
       $sql = "INSERT INTO tbl_risk_assessment(start_id,category,question,group_id,company_id,branch_id,created_by)"
               . "SELECT  ".$db->sqs($data["loadrisk_start_id"]).",tbl_risk_assessment_category.category,tbl_risk_assessment_questions.question,"
               . "tbl_risk_assessment_templates.group_id,"
               .$db->sqs($_SESSION["company_id"]).",".$db->sqs($_SESSION["branch_id"]).",".$db->sqs($_SESSION["user_id"]).""
               . " FROM tbl_risk_assessment_questions inner join"
               . " tbl_risk_assessment_category on"
               . " tbl_risk_assessment_questions.category_id = tbl_risk_assessment_category.id"
               . " inner join tbl_risk_assessment_templates on"
               . " tbl_risk_assessment_questions.template_id = tbl_risk_assessment_templates.id"
               . " where tbl_risk_assessment_questions.company_id = ".$db->sqs($_SESSION["company_id"]);      
        $result = $db->query($sql);
        if ($result) 
        {
            $sql_ = "update tbl_risk_assessment_start set setupStatus = 'OK' where id = ".$db->sqs($data["loadrisk_start_id"]);
            $result = $db->query($sql_);
            if($result)
            {
                return $result;
            }
        } else {
            return false;
        } 
    }
    public function listRiskAssessments($start)
    {
        global $db;
        $sql = "select * from tbl_risk_assessment where start_id =  ".$db->sqs($start)." and company_id = ".$db->sqs($_SESSION['company_id']);
        $result = $db->getAll($sql);
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    public function addRiskTemplate($group,$risk_assessment,$template)
    {
       global $db;
       $sql = "INSERT INTO tbl_risk_assessment_templates(group_id,type_id,template,company_id,branch_id,is_draft,is_active,created_by)
                                VALUES(
                                                  " . $db->sqs($group) . ","
                                                    . $db->sqs($risk_assessment) . ",
                                                  " . $db->sqs($template) . ",    
                                                  " . $db->sqs($_SESSION['company_id']) . ",
                                                  " . $db->sqs($_SESSION['branch_id']) . ",1,                                                  
                                                  " . $db->sqs(1) . ",
                                                  " . $db->sqs($_SESSION['user_id']) . ")";
        $result = $db->query($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    public function addRiskType($type)
    {
       global $db;
       $sql = "INSERT INTO tbl_risk_assessment_type(company_id,branch_id,type_name,is_active,created_by)
                                VALUES(
                                                  " . $db->sqs($_SESSION['company_id']) . ",
                                                  " . $db->sqs($_SESSION['branch_id']) . ",
                                                  " . $db->sqs($type) . ",
                                                  " . $db->sqs(1) . ",
                                                  " . $db->sqs($_SESSION['user_id']) . ")";
        $result = $db->query($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    public function getAllRiskTypes()
    {
        global $db;
        $sql = "select * from tbl_risk_assessment_type where company_id = ".$db->sqs($_SESSION['company_id']);
        $result = $db->getAll($sql);
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    public function getAllRiskTemplates($id = null)
    {
        global $db;
        $sql = "select * from tbl_risk_assessment_templates where company_id = ".$db->sqs($_SESSION['company_id']);
        if($id != null)
        {
            $sql = "select * from tbl_risk_assessment_templates where type_id = ".$db->sqs($id)." and company_id = ".$db->sqs($_SESSION['company_id']);
        }        
        $result = $db->getAll($sql);
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    } 
    
     public static function addCriticalCategory($data)
    {
             global $db;    
        $sql = "INSERT INTO tbl_risk_assessment_category (template_id,"
                . "category, "
                . "branch_id,company_id,created_by) "
                . " VALUES (". $db->sqs($data["template_id"])." ,                 
                ". $db->sqs($data['category'])." , "
                . $db->sqs($_SESSION["branch_id"])." ,"     
                . $db->sqs($_SESSION["company_id"])." ,"    
                .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);         
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
   public static function getAllCriticalCategories($id)
   {
             global $db;    
      $sql = "SELECT * from tbl_risk_assessment_category where template_id = ".$db->sqs($id)." and company_id = ".$db->sqs($_SESSION["company_id"]);
        $response = $db->getAll($sql);   
        //echo $sql;
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    public static function addCriticalQuestion($data)
    {
             global $db;    
        $sql = "INSERT INTO tbl_risk_assessment_questions (template_id,"
                . "category_id,question,status,branch_id,company_id,created_by)"
                . " VALUES (". $db->sqs($data["template_id"])." ,                 
                ". $db->sqs($data['category'])." , "
                . $db->sqs($data['question'])." , "
                . $db->sqs($data['status'])." , "
                . $db->sqs($_SESSION["branch_id"])." ,"     
                . $db->sqs($_SESSION["company_id"])." ,"    
                .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);   
        echo $sql;
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
   public static function getAllCriticalQuestion($id)
   {
        global $db;    
        $sql = "SELECT * from tbl_risk_assessment_questions where template_id = ".$db->sqs($id)." and company_id = ".$db->sqs($_SESSION["company_id"]);
        $response = $db->getAll($sql);   
        if($response){
            return $response;
        }else{
            return false;
        }  
    }   
    //update status
    public static function updateNaAssessment($data)
    {
         global $db;
        $sql = "UPDATE tbl_risk_assessment SET status = 'N/A'                                    
                WHERE id = ".$db->sqs($data["assessment_id"]);
        $result = $db->query($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    public static function hasAssessed($data)
    {
         global $db;
        $sql = "UPDATE tbl_risk_assessment_start SET hasAssessed = 1,revisedBy = ".$db->sqs($data["revisedBy"])."                                   
                WHERE id = ".$db->sqs($data["assessment_id"]);
        $result = $db->query($sql);
        if ($result) {
            $sql_request = "UPDATE tbl_risk_assessment_request SET requested_status = 'Assessed'                                    
                WHERE id = ".$db->sqs($data["request_id"]);
             $result_sql = $db->query($sql_request);
             if($result_sql)
             {
                 return $result_sql;
             }
             else
             {
                 //reset column
                 $sql_r = "UPDATE tbl_risk_assessment_start SET hasAssessed = 0,revisedBy = NULL                                    
                  WHERE id = ".$db->sqs($data["assessment_id"]);
                 $res = $db->query($sql_r);
                 return false;
             }
             
        } else {
            return false;
        }
    }
    //mark as assesed hasAssessed
    public static function updateStatus($data)
    {
         global $db;
        $sql = "UPDATE tbl_risk_assessment_templates SET is_draft =  ".$db->sqs($data["status"])."                                    
                WHERE id = ".$db->sqs($data["template_id"]);
        $result = $db->query($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }
       
    }
    public function listRiskControls($start)
    {
        global $db;
        $sql = "select tbl_risk_assessment.risk_notes,tbl_risk_assessment.start_id as risk_no,tbl_risk_assessment.risk_photo,
        tbl_settings_hazards.hazard,tbl_risk_assessment.hazard_description,tbl_settings_risk.risk,
        tbl_risk_assessment.risk_description,pscore,tbl_settings_control.control_type,
        tbl_risk_assessment.control_description,tbl_risk_assessment.is_control_in_effect,
        tbl_risk_assessment.new_score,tbl_risk_assessment.id as risk_ass_id 
        from tbl_risk_assessment 
        right join tbl_settings_hazards
        on tbl_risk_assessment.hazard_type = tbl_settings_hazards.id
        right join tbl_settings_risk
        on tbl_risk_assessment.risk_type = tbl_settings_risk.id
        right join tbl_settings_control
        on tbl_risk_assessment.control_type = tbl_settings_control.id
        where tbl_risk_assessment.start_id = ".$db->sqs($start)." and tbl_risk_assessment.company_id = ".$db->sqs($_SESSION['company_id']);
        $result = $db->getAll($sql);
        
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    public function listRiskControlsNotActive($start)
    {
        global $db;
        $sql = "select tbl_risk_assessment.* from tbl_risk_assessment where  is_control_in_effect IS NULL and start_id =  ".$db->sqs($start)." and company_id = ".$db->sqs($_SESSION['company_id']);
        $result = $db->getAll($sql);
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    public function startRiskAssessment($data)
    {
             global $db;    
        $sql = "INSERT INTO tbl_risk_assessment_start (request_id,risk_type_id,risk_template_id,description,"
                . "starting_date,next_revision_date,frequency,departments_list,resource_list,occupation_list,branch_id,company_id,is_draft,created_by)"
                . " VALUES (". $db->sqs($data["request_id"])." ,                 
                ". $db->sqs($data['risk_type_id'])." , "
                . $db->sqs($data['risk_template_id'])." , "
                . $db->sqs($data['description'])." , "
                . $db->sqs($data['starting_date'])." , "
                . $db->sqs($data['next_revision_date'])." , "
                . $db->sqs($data['frequency'])." , "
                . $db->sqs($data['departments_list'])." , "
                . $db->sqs($data['resources_list'])." , "
                . $db->sqs($data['occupation_list'])." , "
                . $db->sqs($_SESSION["branch_id"])." ,"     
                . $db->sqs($_SESSION["company_id"])." ,1,"    
                .$db->sqs($_SESSION['user_id']) ." )";
        $response = $db->query($sql);    
        //echo $sql;
        if($response){
            $this->getStartRA_Id = $db->insertId();
            return $response;
        }else{
            return false;
        }  
    }
    public static function rescheduleRA($data)
    {
         global $db;
        $sql = "UPDATE tbl_risk_assessment_request SET upcoming_date =  ".$db->sqs($data["newDate"])."                                    
                WHERE company_id = ".$db->sqs($_SESSION["company_id"])." and id = ".$db->sqs($data["risk_requested_id"]);
        $result = $db->query($sql);
        //echo $sql;
        if ($result) {
            return $result;
        } else {
            return false;
        }
       
    }
    public function getAllRiskSettings()
    {
        global $db;
        $sql = "select * from tbl_settings_risk where company_id = ".$db->sqs($_SESSION['company_id']);
        $result = $db->getAll($sql);
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    public function addRiskSetting($risk,$group)
    {
       global $db;
       $sql = "INSERT INTO tbl_settings_risk(group_id,company_id,branch_id,risk,is_active,created_by)
                                VALUES(
                                                  " . $db->sqs($group) . ",
                                                  " . $db->sqs($_SESSION['company_id']) . ",
                                                  " . $db->sqs($_SESSION['branch_id']) . ",
                                                  " . $db->sqs($risk) . ",
                                                  " . $db->sqs(1) . ",
                                                  " . $db->sqs($_SESSION['user_id']) . ")";
        $result = $db->query($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    public function getAllControlSettings()
    {
        global $db;
        $sql = "select * from tbl_settings_control where company_id = ".$db->sqs($_SESSION['company_id']);
        $result = $db->getAll($sql);
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    public function addControlSetting($control_type,$group,$reduction)
    {
       global $db;
       $sql = "INSERT INTO tbl_settings_control(group_id,company_id,branch_id,control_type,reduction,is_active,created_by)
                                VALUES( " . $db->sqs($group) . ",
                                                  " . $db->sqs($_SESSION['company_id']) . ",
                                                  " . $db->sqs($_SESSION['branch_id']) . ",
                                                  " . $db->sqs($control_type) . ",
                                                  " . $db->sqs($reduction) . ",    
                                                  " . $db->sqs(1) . ",
                                                  " . $db->sqs($_SESSION['user_id']) . ")";
        $result = $db->query($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    public function getAllRisksAboveSetting($start_id)
    {
        global $db;
        $sql = "select count(*) as totalRisks from tbl_risk_assessment "
                . " where start_id = ".$db->sqs($start_id)." and new_score > 100 and company_id = ".$db->sqs($_SESSION['company_id']);
        $result = $db->getAll($sql);
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    public function getRisksControlled($start_id)
    {
        global $db;
        $sql = "select count(tb.start_id) as totalQuestions,(select count(tr.is_control_in_effect) from tbl_risk_assessment tr"
                . " where tr.is_control_in_effect = 1 and tr.start_id = tb.start_id) as totalControlled from tbl_risk_assessment as tb"
                . " where tb.start_id = ".$db->sqs($start_id)." and tb.company_id = ".$db->sqs($_SESSION['company_id']);
        $result = $db->getAll($sql);
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    public function getAllRiskAssessors()
    {
        global $db;
        $sql = "select concat(tbl_users.firstname,' ',tbl_users.lastname) as emp,
tbl_departments.department_name,tbl_risk_assessor.id,tbl_risk_assessor.date_created,tbl_risk_assessor.risk_dep,
tbl_risk_assessor.groups_
from tbl_risk_assessor
inner join tbl_users
on tbl_risk_assessor.employee_id  = tbl_users.id
inner join tbl_departments
on tbl_risk_assessor.dep_id = tbl_departments.id
WHERE tbl_risk_assessor.company_id = ".$db->sqs($_SESSION['company_id']);
        $result = $db->getAll($sql);
        if($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }
    public function addRiskAssessor($data = array())
    {
       global $db;
       $sql = "INSERT INTO tbl_risk_assessor(group_id,company_id,branch_id,dep_id,occupation,employee_id,risk_dep,groups_,is_active,created_by)
                                VALUES( " . $db->sqs($data["group_id"]) . ",
                                                  " . $db->sqs($_SESSION['company_id']) . ",
                                                  " . $db->sqs($_SESSION['branch_id']) . ",
                                                  " . $db->sqs($data["dep_id"]) . ",
                                                  " . $db->sqs($data["occupation"]) . ",    
                                                  " . $db->sqs($data["employee_id"]) . ",    
                                                  " . $db->sqs($data["risk_dep"]) . ",    
                                                  " . $db->sqs($data["group_id"]) . ",  
                                                  " . $db->sqs(1) . ",
                                                  " . $db->sqs($_SESSION['user_id']) . ")";
       //echo $sql;
        $result = $db->query($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    public function addRiskAssessmentPost($data = array())
    {
       global $db;
       $sql = "INSERT INTO tbl_risk_assessment(group_id,company_id,branch_id,source_id,source_type,hazard_type,hazard_description,risk_type,risk_description,pscore,control_type,control_description,is_control_in_effect,reduced_by,new_score,is_active,created_by)
                                VALUES( " . $db->sqs($data["group_id"]) . ",
                                                  " . $db->sqs($_SESSION['company_id']) . ",
                                                  " . $db->sqs($_SESSION['branch_id']) . ",
                                                  " . $db->sqs($data["source_id"]) . ",
                                                  " . $db->sqs($data["source_type"]) . ",
                                                  " . $db->sqs($data["hazard_type"]) . ",    
                                                  " . $db->sqs($data["hazard_description"]) . ",    
                                                  " . $db->sqs($data["risk_type"]) . ",    
                                                  " . $db->sqs($data["risk_description"]) . ",
                                                  " . $db->sqs($data["pscore"]) . ",
                                                  " . $db->sqs($data["control_type"]) . ", 
                                                  " . $db->sqs($data["control_description"]) . ", 
                                                  " . $db->sqs($data["is_control_in_effect"]) . ", 
                                                  " . $db->sqs($data["reduced_by"]) . ", 
                                                  " . $db->sqs($data["new_scrore"]) . ", 
                                                  " . $db->sqs(1) . ",
                                                  " . $db->sqs($_SESSION['user_id']) . ")";
       //echo $sql;
        $result = $db->query($sql);
        if ($result) {
            $this->hasCreatedRisk = true;
            return $result;
        } else {
            return false;
        }
    }
     public function updateRiskAssessment($data = array())
    {
       global $db;
       $sql = "update tbl_risk_assessment set 
           status = 'Checked',source_type='{$data["source_type"]}',
           hazard_type = '{$data["hazard_type"]}',hazard_description = '{$data["hazard_description"]}',
           risk_type= '{$data["risk_type"]}',risk_description = '{$data["risk_description"]}',pscore = '{$data["pscore"]}',control_type = '{$data["control_type"]}',
           control_description = '{$data["control_description"]}',is_control_in_effect = '{$data["is_control_in_effect"]}',
           reduced_by = '{$data["reduced_by"]}',new_score = '{$data["new_scrore"]}',risk_notes = '{$data["risk_photo"]}',risk_photo =  '{$data["risk_notes"]}'
           WHERE id = ".$data["source_id"]." and start_id = ".$data["start_id"]." and company_id = ".$db->sqs($_SESSION["company_id"]);
     //  echo $sql;
        $result = $db->query($sql);
        if ($result) {
            $this->hasUpdatedRisk = true;
            return $result;
        } else {
            return false;
        }
    }
    public function getControlReduction($control_id)
    {
         global $db;
        $sql = "SELECT reduction FROM tbl_settings_control WHERE is_active = 1 and id = " . $db->sqs($control_id)." and company_id = ". $db->sqs($_SESSION["company_id"]);
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        } 
    }
    /////// SHEQ NOTES //////
   public function getNewSHEQFNotes($id)
    {
         global $db;
        $sql = "SELECT * FROM tbl_sheqf_notes WHERE is_active = 1 and source_id = " . $db->sqs($id)." and company_id = ". $db->sqs($_SESSION["company_id"]);
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        } 
    } 
   public function getSHEQFNotes($asset_id,$source)
    {
         global $db;
        $sql = "SELECT * FROM tbl_sheqf_notes WHERE is_active = 1 and source_type = '{$source}' and source_id = " . $db->sqs($asset_id)." and company_id = ". $db->sqs($_SESSION["company_id"]);
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        } 
    }
    public function addSheQnotes($data = array())
    {
        global $db;
         $sql = "INSERT INTO tbl_sheqf_notes(group_id,company_id,branch_id,source_id,topic,notes,is_active,created_by)
                                VALUES( " . $db->sqs($data["group_id"]) . ",
                                                  " . $db->sqs($_SESSION['company_id']) . ",
                                                  " . $db->sqs($_SESSION['branch_id']) . ",
                                                  " . $db->sqs($data["source_id"]) . ",
                                                  " . $db->sqs($data["topic"]) . ",    
                                                  " . $db->sqs($data["notes"]) . ",    
                                                  " . $db->sqs(1) . ",
                                                  " . $db->sqs($_SESSION['user_id']) . ")";
       //echo $sql;
        $result = $db->query($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    public function getRiskAssessment($asset_id,$source)
    {
        global $db;
        $sql = "SELECT * FROM tbl_risk_assessment WHERE source_type = '{$source}' and is_active = 1 and source_id = " . $db->sqs($asset_id);
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    /////////////End of new code/////////////////////////////
    public function getRiskAssessmentNO()
    {

        $date = new DateTime();
        $dateCreated = date_format($date, 'H:i:s');
        $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWX';
        $charslength = strlen($chars);
        $randomString = '';
        for ($i = 0; $i < 10; $i++) {
            $randomString .= $chars[rand(0, $charslength - 1)];
        }

        return "RA-" . $randomString . $dateCreated;
    }

    /**
     * Method to get assessment number
     * @param null $riskId
     * @return bool|int|String
     */
    public function getRiskAssessmentUniqueNo($riskId = null)
    {


        global $db;

        $allRiskRecommendationTypesSql = "SELECT risk_assessment_no FROM tbl_risk_assessment WHERE id=" . $db->sqs($riskId);


        $getAllRiskRecTypesResults = $db->getOne($allRiskRecommendationTypesSql);

        if ($getAllRiskRecTypesResults) {

            return $getAllRiskRecTypesResults;
        } else {

            return false;
        }


    }

    /**
     * Method to get risk assessment info
     * @param null $riskId
     * @return array|bool
     */
    public function getRiskAssessmentHeaderInfo($riskId = null){

        global $db;

        $allRiskRecommendationTypesSql = "SELECT tbl_risk_assessment.*,tbl_users.firstname,tbl_users.lastname FROM tbl_risk_assessment
                                                  INNER JOIN tbl_users ON tbl_users.id = tbl_risk_assessment.risk_assessor
                                      
                                                  WHERE tbl_risk_assessment.id=" . $db->sqs($riskId);


        $getAllRiskRecTypesResults = $db->getRow($allRiskRecommendationTypesSql);

        if ($getAllRiskRecTypesResults) {

            return $getAllRiskRecTypesResults;
        } else {

            return false;
        }

    }

    /**
     * Method to get assistance info
     * @param null $riskId
     * @return array|bool
     */
    public function getRiskAssessmentAssistantInfo($riskId = null){

        global $db;

        $allRiskRecommendationTypesSql = "SELECT tbl_risk_assessor_assitance.*,tbl_users.firstname,
                                                  tbl_users.lastname 
                                                  FROM tbl_risk_assessor_assitance
                                                  INNER JOIN tbl_users ON tbl_users.id = tbl_risk_assessor_assitance.assitance_assessor
                                                  WHERE tbl_risk_assessor_assitance.riskId=" . $db->sqs($riskId);


        $getAllRiskRecTypesResults = $db->getRow($allRiskRecommendationTypesSql);

        if ($getAllRiskRecTypesResults) {

            return $getAllRiskRecTypesResults;
        } else {

            return false;
        }

    }

    /**
     * Method to get all risk assessments types
     */
    public function getRiskAssessmentsTypes()
    {

        global $db;

        $allRiskTypesSql = "SELECT * FROM tbl_risk_assessment_type";


        $getAllRiskTypesResults = $db->getAll($allRiskTypesSql);

        if ($getAllRiskTypesResults) {

            return $getAllRiskTypesResults;
        } else {

            return false;
        }

    }

    /**Method to get all risk assessments type by name
     * @param null $typeId
     * @return bool|int|String
     */
    public function getRiskAssessmentsTypeName($typeId = null)
    {

        global $db;

        $allRiskTypesSql = "SELECT type_name FROM tbl_risk_assessment_type WHERE id =".$db->sqs($typeId);


        $getAllRiskTypesResults = $db->getOne($allRiskTypesSql);

        if ($getAllRiskTypesResults) {

            return $getAllRiskTypesResults;
        } else {

            return false;
        }

    }


    /**
     * Method to get assessed items
     * @param null $riskId
     * @return array|bool
     */
    public function getRiskAssessmentItems($riskId = null)
    {

        global $db;

        $allRiskRecommendationTypesSql = "SELECT tbl_risk_assessment_items.*,tbl_assets.name as asset_name,tbl_assets.company_asset_number,
                                                  tbl_departments.department_name 
                                                  FROM tbl_risk_assessment_items 
                                            
                                            LEFT  JOIN tbl_assets ON tbl_assets.id = tbl_risk_assessment_items.item_id
                                            LEFT  JOIN tbl_departments ON tbl_departments.id = tbl_risk_assessment_items.department_id
                                            WHERE risk_assessment_id=".$db->sqs($riskId);


        $getAllRiskRecTypesResults = $db->getAll($allRiskRecommendationTypesSql);

        if ($getAllRiskRecTypesResults) {

            return $getAllRiskRecTypesResults;
        } else {

            return false;
        }

    }

    /**
     * Method to get all risk assessments types
     */
    public function getRiskAssessmentsRecommendationTypes()
    {

        global $db;

        $allRiskRecommendationTypesSql = "SELECT * FROM tbl_recommendation_types";


        $getAllRiskRecTypesResults = $db->getAll($allRiskRecommendationTypesSql);

        if ($getAllRiskRecTypesResults) {

            return $getAllRiskRecTypesResults;
        } else {

            return false;
        }

    }

    /**
     * Method to get all risk assessments groups header
     * @param null $groupId
     * @return array|bool
     */
    public function getRiskAssessmentsGroupsHeader($groupId = null)
    {

        global $db;

        $riskGroupsHeaderSql = "SELECT * FROM tbl_risk_assessment_groups 
                                      WHERE risk_type_id =" . $db->sqs($groupId) . " AND is_header = 1";

        $getRiskGroupsHeaderResults = $db->getRow($riskGroupsHeaderSql);

        if ($getRiskGroupsHeaderResults) {

            return $getRiskGroupsHeaderResults;
        } else {

            return false;
        }

    }

    /**
     * Method to get all risk assessments groups
     * @param null $groupId
     * @return array|bool
     */
    public function getRiskAssessmentsGroups($groupId = null)
    {

        global $db;

        $allRiskGroupsSql = "SELECT * FROM tbl_risk_assessment_groups 
                                      WHERE risk_type_id =" . $db->sqs($groupId) . " AND is_header = 0";

        $getAllRiskGroupsResults = $db->getAll($allRiskGroupsSql);

        if ($getAllRiskGroupsResults) {

            return $getAllRiskGroupsResults;
        } else {

            return false;
        }

    }

    /**
     * Method to get all risk areas of assessment
     * @return array|bool
     */
    public function getRiskAssessmentsAreas()
    {

        global $db;
        $data = array();
        $allRiskAreasSql = "SELECT * FROM tbl_risk_assessment_areas";

        $getAllRiskAreasResults = $db->getAll($allRiskAreasSql);

        if ($getAllRiskAreasResults) {

            return $getAllRiskAreasResults;

        } else {

            return false;
        }

    }


    /**
     * Method to get all risk areas of data areas
     * @return array|bool
     */
    public function getRiskAssessmentsDataAreas()
    {

        global $db;
        $data = array();
        $allRiskAreasSql = "SELECT * FROM tbl_risk_assessment_areas";

        $getAllRiskAreasResults = $db->getAll($allRiskAreasSql);

        if ($getAllRiskAreasResults) {

            foreach($getAllRiskAreasResults as $area){

                $data[$area['id']] = $area['area_name'];
            }

            return $data;
        } else {

            return false;
        }

    }

    /**
     * Method to get all risk areas of assessment
     * @return array|bool
     */
    public function getRiskAssessmentInitialData()
    {

        global $db;

        $allRiskAssessmentsSql = "SELECT tbl_risk_assessment_areas.id AS areas_id,
                                              tbl_risk_assessment_areas.area_name,
                                              tbl_risk_areas.*,
                                              tbl_risk_data.*,
                                              tbl_risk_assessment.*,
                                              tbl_recommendations.*,
                                              tbl_recommendation_types.*
                                    
                                            FROM `tbl_risk_assessment_areas`
                                               LEFT JOIN `tbl_risk_areas` ON `tbl_risk_areas`.`area_id`=`tbl_risk_assessment_areas`.`id`
                                               LEFT  JOIN `tbl_risk_data` ON `tbl_risk_areas`.`id`=`tbl_risk_data`.`risk_area_id`
                                               LEFT  JOIN `tbl_risk_assessment` ON `tbl_risk_assessment`.`id`=`tbl_risk_areas`.`risk_assessment_id`
                                               LEFT  JOIN `tbl_recommendations` ON `tbl_recommendations`.`risk_area_id`=`tbl_risk_data`.`risk_area_id`
                                               LEFT  JOIN `tbl_recommendation_types` ON `tbl_recommendations`.`type_id`=`tbl_recommendation_types`.`id`
                                            WHERE `tbl_risk_assessment`.`id` = 0";


        $getAllRiskAssessmentResults = $db->getAll($allRiskAssessmentsSql);


        if ($getAllRiskAssessmentResults) {

            return $getAllRiskAssessmentResults;
        } else {
            return false;
        }

    }

    /**
     * @param array $areasData
     * @param null $riskId
     * @param null $groupId
     * @return array|bool
     */
    public function getRiskAssessmentData($areasData = array(), $riskId = null, $groupId = null)
    {

        $riskAssessmentData = [];

        if (is_array($areasData) || is_object($areasData)) {
            foreach ($areasData AS $key=>$value) {

                $riskAssessmentData[$key]['groupId'] = $groupId;
                $riskAssessmentData[$key]['areas_id'] = $key;

                //Now get risk data
                $riskAssessmentData[$key]['areas_data'] = $this->getAreaData($value,$key, $riskId, $groupId);

            }

            if (count($riskAssessmentData)) {

                return $riskAssessmentData;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public function getAreaData($area_name = null,$areaId = null, $riskAssessmentId = null, $groupId = null)
    {

        global $db;

        $riskData = array();
        $allRiskAssessmentsSql = "SELECT `tbl_risk_data`.*,`tbl_risk_data`.`id` AS r_data_id,
                                        `tbl_risk_areas`.*
                                   FROM `tbl_risk_data`
                                   INNER JOIN `tbl_risk_areas` ON `tbl_risk_areas`.`area_id`=`tbl_risk_data`.`risk_area_id`
                                   INNER JOIN  `tbl_risk_assessment_groups` ON `tbl_risk_areas`.`risk_group_id`=`tbl_risk_assessment_groups`.`id`
                                   WHERE `tbl_risk_data`.`risk_assessment_id`=" . $db->sqs($riskAssessmentId) . " 
                                   AND `tbl_risk_data`.`risk_area_id`=" . $db->sqs($areaId) . "
                                   AND `tbl_risk_areas`.`area_id`=" . $db->sqs($areaId) . "
                                   AND `tbl_risk_areas`.`risk_assessment_id`=" . $db->sqs($riskAssessmentId) . "
                                   AND `tbl_risk_data`.`group_id`=" . $db->sqs($groupId) . "
                                   GROUP BY `tbl_risk_data`.`id`,`tbl_risk_data`.`group_id`,`tbl_risk_data`.`risk_assessment_id`";


        $riskDataResults = $db->getAll($allRiskAssessmentsSql);

        if($riskDataResults){

            foreach ($riskDataResults as $key=>$value){


                $riskData[$key]['area_name']= $area_name;
                $riskData[$key]['hazard']= $value['hazard'];
                $riskData[$key]['risk']= $value['risk'];
                $riskData[$key]['score']= $value['score'];
                $riskData[$key]['final_score']= $value['final_score'];
                $riskData[$key]['photo']= $value['photo'];
                //$riskData[$key]['risk_data_id']= $value['risk_data_id'];
                $riskData[$key]['r_data_id']= $value['r_data_id'];
                $riskData[$key]['area_id']= $value['area_id'];
                //$riskData[$key]['percentage']= $value['percentage'];
                $riskData[$key]['groupId']= $groupId;
                $riskData[$key]['activity_no']= $value['activity_no'];
                $riskData[$key]['noOfRecommendations'] = $this->getNumberOfRecommendations($value['r_data_id']);


            }
        }else{

            $riskData[0]['area_name']= $area_name;
            $riskData[0]['hazard']= '---';
            $riskData[0]['risk']='---';
            $riskData[0]['score']= '---';
            $riskData[0]['final_score']= '---';
            $riskData[0]['photo']= '---';
            //$riskData[0]['risk_data_id']= $areaId;
            $riskData[0]['r_data_id']= '';
            $riskData[0]['area_id']= $areaId;
            //$riskData[0]['percentage']= '';
            $riskData[0]['groupId']= $groupId;
            $riskData[0]['activity_no']= '';
            $riskData[0]['noOfRecommendations'] = '';

        }

        return $riskData;

    }


    /**
     * Method to get risk assessment data
     * @param null $riskId
     * @return array|bool
     */
    public function getRiskAssessmentInfo($riskId = null)
    {

        global $db;
        $riskAssessmentsInfoSql = "SELECT *
                                   FROM `tbl_risk_data`
                                   WHERE `tbl_risk_data`.`id`=" . $db->sqs($riskId);

        $response = $db->getRow($riskAssessmentsInfoSql);

        if ($response) {
            return $response;
        } else {
            return false;
        }

    }

    /**
     * Method to get all risk assessment
     * @return array|bool
     */
    public function getAllRiskAssessmentsRecommendations($dataId = null)
    {

        global $db;

        $allRiskRecommendationSql = "SELECT tbl_recommendations.id as rec_id,tbl_recommendations.*,tbl_recommendation_types.*
 
                                                FROM tbl_recommendations,
                                                tbl_recommendation_types
                                          WHERE tbl_recommendations.type_id = tbl_recommendation_types.id 
                                          AND tbl_recommendations.risk_data_id =" . $db->sqs($dataId);


        $getAllRecommendationsResults = $db->getAll($allRiskRecommendationSql);

        if ($getAllRecommendationsResults) {

            return $getAllRecommendationsResults;
        } else {
            return false;
        }

    }

    /**
     * Method to get recommendation info
     * @param null $recId
     * @return array|bool
     */
    public function getRecommendationInfo($recId = null){

        global $db;

        $recommendationInfoSql = "SELECT * FROM tbl_recommendations
                                          WHERE id =" . $db->sqs($recId);

        $getRecommendationsInfoResults = $db->getRow($recommendationInfoSql);


        if ($getRecommendationsInfoResults) {
            return $getRecommendationsInfoResults;
        } else {
            return false;
        }

    }

    /**
     * Method to get recommendations
     * @param null $dataId
     * @param null $recType
     * @return array|bool
     */
    public function getRecommendationByTypeDataId($dataId = null,$recType = null){

        global $db;

        $recommendationInfoSql = "SELECT * FROM tbl_recommendations
                                          WHERE risk_data_id =" . $db->sqs($dataId)." AND type_id=".$db->sqs($recType);

        $getRecommendationsInfoResults = $db->getAll($recommendationInfoSql);


        if ($getRecommendationsInfoResults) {
            return $getRecommendationsInfoResults;
        } else {
            return false;
        }

    }




    /**
     * Method to get number of recommendations
     * @param null $dataId
     * @return int
     */
    public function getNumberOfRecommendations($dataId = null){

        global $db;

        $selectedRecommendationSql = "SELECT * FROM tbl_recommendations WHERE risk_data_id =".$db->sqs($dataId)." AND  message <>''";
        $db->query($selectedRecommendationSql,true);
        return $db->getRowsReturned();

    }

    /**
     * Method to get all risk assessment
     * @return array|bool
     */
    public function getAllRiskAssessments()
    {

        global $db;

        $allRiskAssessmentsSql = "SELECT * FROM tbl_risk_assessment,
                                                tbl_risk_areas,
                                                tbl_risk_data
                                          WHERE tbl_risk_assessment.id = tbl_risk_areas.risk_assessment_id 
                                          AND tbl_risk_areas.id = tbl_risk_data.risk_area_id
                                          AND tbl_risk_assessment.is_deleted = 0";

        $getAllRiskAssessmentResults = $db->getAll($allRiskAssessmentsSql);


        if ($getAllRiskAssessmentResults) {

            return $getAllRiskAssessmentResults;
        } else {
            return false;
        }

    }

    /**
     * Method to get all completed risk assessments
     * @param null $branchId
     * @return array|bool
     */
    public function getAllCompleteRiskAssessments($branchId = null)
    {

        global $db;

        $allCompleteRiskAssessmentsSql = "SELECT * FROM tbl_risk_assessment,
                                                tbl_risk_areas,
                                                tbl_risk_data
                                          WHERE tbl_risk_assessment.id = tbl_risk_areas.risk_assessment_id 
                                          AND tbl_risk_areas.id = tbl_risk_data.risk_area_id
                                          AND tbl_risk_assessment.is_complete = 1
                                          AND tbl_departments.id = tbl_risk_assessment.department_id
                                          AND tbl_departments.branch_id = {$db->sqs($branchId)}";

        $getAllCompleteRiskAssessmentResults = $db->getAll($allCompleteRiskAssessmentsSql);


        if ($getAllCompleteRiskAssessmentResults) {

            return $getAllCompleteRiskAssessmentResults;
        } else {
            return false;
        }

    }

    /**
     * Method to get all incomplete risk assessments
     * @param null $companyId
     * @return array|bool
     */
    public function getAllInCompleteRiskAssessments($companyId = null)
    {

        global $db;

        $allInCompleteRiskAssessmentsSql = "SELECT tbl_risk_assessment.*,tbl_risk_assessment_type.type_name,
                                              tbl_users.firstname,tbl_users.lastname
                                              FROM tbl_risk_assessment
                                              INNER JOIN tbl_risk_assessment_type ON tbl_risk_assessment_type.id = tbl_risk_assessment.assessment_type_id
                                              INNER JOIN tbl_users ON tbl_users.id = tbl_risk_assessment.risk_assessor
                                              WHERE tbl_risk_assessment.is_complete = 0
                                              AND tbl_risk_assessment.company_id = {$db->sqs($companyId)}";


        $getAllIncompleteAssessmentResults = $db->getAll($allInCompleteRiskAssessmentsSql);


        if ($getAllIncompleteAssessmentResults) {

            return $getAllIncompleteAssessmentResults;
        } else {
            return false;
        }
    }

    /**
     *  Method to get all company risk assessments
     * @param null $companyId
     * @return array|bool
     */
    public function getAllCompanyRiskAssessments($companyId = null)
    {


        global $db;

        $allCompanyRiskAssessmentsSql = "SELECT * FROM tbl_risk_assessment,
                                                tbl_risk_areas,
                                                tbl_risk_data,
                                                tbl_departments
                                          WHERE tbl_risk_assessment.id = tbl_risk_areas.risk_assessment_id 
                                          AND tbl_risk_areas.id = tbl_risk_data.risk_area_id
                                          AND tbl_departments.id = tbl_risk_assessment.department_id
                                          AND tbl_departments.company_id = {$db->sqs($companyId)}";

        $getAllCompanyRiskAssessmentResults = $db->getAll($allCompanyRiskAssessmentsSql);


        if ($getAllCompanyRiskAssessmentResults) {

            return $getAllCompanyRiskAssessmentResults;
        } else {

            return false;
        }
    }

    /**
     * Method to get branch risk assessments
     * @param null $branchId
     * @return array|bool
     */
    public function getAllBranchRiskAssessments($branchId = null)
    {

        global $db;

        $allBranchRiskAssessmentsSql = "SELECT * FROM tbl_risk_assessment,
                                                tbl_risk_areas,
                                                tbl_risk_data,
                                                tbl_departments
                                          WHERE tbl_risk_assessment.id = tbl_risk_areas.risk_assessment_id 
                                          AND tbl_risk_areas.id = tbl_risk_data.risk_area_id
                                          AND tbl_departments.id = tbl_risk_assessment.department_id
                                          AND tbl_departments.branch_id = {$db->sqs($branchId)}";

        $getAllBranchRiskAssessmentResults = $db->getAll($allBranchRiskAssessmentsSql);


        if ($getAllBranchRiskAssessmentResults) {

            return $getAllBranchRiskAssessmentResults;
        } else {

            return false;
        }
    }

    /**
     * Method to get all risk assessments by departments
     * @param null $departmentId
     * @return array|bool
     */
    public function getAllDepartmentRiskAssessments($departmentId = null)
    {

        global $db;

        $allDepartmentRiskAssessmentsSql = "SELECT * FROM tbl_risk_assessment,
                                                tbl_risk_areas,
                                                tbl_risk_data,
                                                tbl_departments
                                          WHERE tbl_risk_assessment.id = tbl_risk_areas.risk_assessment_id 
                                          AND tbl_risk_areas.id = tbl_risk_data.risk_area_id
                                          AND tbl_departments.id = tbl_risk_assessment.department_id
                                          AND tbl_departments.id = {$db->sqs($departmentId)}";

        $getAllDepartmentRiskAssessmentResults = $db->getAll($allDepartmentRiskAssessmentsSql);


        if ($getAllDepartmentRiskAssessmentResults) {

            return $getAllDepartmentRiskAssessmentResults;
        } else {

            return false;
        }

    }

    /**
     * Method to add risk assessment
     * @param array $riskData
     * @return bool
     */
    public function addRiskAssessment($riskData = array())
    {
        global $db;

        $date = new DateTime();
        $dateCreated = date_format($date, 'Y-m-d H:i:s');

       $addRiskAssessmentSql = "INSERT INTO tbl_risk_assessment( 
                                                    risk_assessment_no,	    
                                                    risk_assessor,	    
                                                    user_type_id,	           
                                                    risk_assessment_date,
                                                    company_id,
                                                    assessment_type_id,
                                                    date_created,	    
                                                    created_by,    
                                                    date_modified,	    
                                                    modified_by	
                                                    )
                                               VALUES (
                                                  " . $db->sqs($riskData['riskAssessNumber']) . ",
                                                  " . $db->sqs($riskData['riskAssessor']) . ",
                                                  " . $db->sqs($riskData['userTypeId']) . ",
                                                  " . $db->sqs($riskData['riskAssessmentDate']) . ",
                                                  " . $db->sqs($riskData['companyId']) . ",
                                                  " . $db->sqs($riskData['assessmentType']) . ",
                                                  " . $db->sqs($dateCreated) . ",
                                                  " . $db->sqs($riskData['createdBy']) . ",
                                                  " . $db->sqs($dateCreated) . ",
                                                  " . $db->sqs($riskData['createdBy']) . "
                                                   )";

        $addRiskAssessmentResults = $db->query($addRiskAssessmentSql);

        if ($addRiskAssessmentResults) {
            return $db->insertId();
        } else {
            return false;
        }

    }

    /**
     * Method to add risk assessor assistance
     * @param null $assistantId
     * @param null $riskId
     * @return bool
     */
    public function addRiskAssessmentAssistance($assistantId= null, $riskId = null){

        global $db;
        $addRiskAssessorSql = "INSERT INTO tbl_risk_assessor_assitance( 
                                                    riskId,	    
                                                    assitance_assessor
                                                    )
                                               VALUES (
                                                  " .$db->sqs($riskId). ",
                                                  " .$db->sqs($assistantId) . "
                                                   )";

        $addRiskAssessmentResults = $db->query($addRiskAssessorSql);

        if ($addRiskAssessmentResults) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method to add item/resource being assessed
     * @param null $riskNumber
     * @param null $riskAssessmentType
     * @param array $riskAssessmentItems
     * @return bool
     */
    public function addRiskAssessmentItems($riskNumber = null, $riskAssessmentType = null, $riskAssessmentItems = array())
    {

        global $db;
        $success = false;

        $riskAssessmentItems = unserialize($riskAssessmentItems);
        foreach ($riskAssessmentItems as $item) {

            $item = explode('#', $item);

            $addRiskAssessmentItemSql = "INSERT INTO tbl_risk_assessment_items( 
                                                risk_assessment_id,    
                                                risk_assessment_group_id,    
                                                department_id,   
                                                item_id)
                                                VALUES (
                                                  " . $db->sqs($riskNumber) . ",
                                                  " . $db->sqs($riskAssessmentType) . ",
                                                  " . $db->sqs($item[0]) . ",
                                                  " . $db->sqs($item[1]) . "
                                                   )";

            $success = $db->query($addRiskAssessmentItemSql);
        }
        return $success;
    }

    /**
     * Method to add item/resource being assessed
     * @param null $riskNumber
     * @param null $riskAssessmentType
     * @param array $riskAssessmentItems
     * @return bool
     */
    public function addBaselineRiskAssessmentItems($riskNumber = null, $riskAssessmentType = null, $riskAssessmentItems = array())
    {

        global $db;
        $success = false;

        $riskAssessmentItems = unserialize($riskAssessmentItems);

        foreach ($riskAssessmentItems as $key=>$value) {


            $addRiskAssessmentItemSql = "INSERT INTO tbl_risk_assessment_items( 
                                                risk_assessment_id,        
                                                department_id)
                                                VALUES (
                                                  " . $db->sqs($riskNumber) . ",
                                                  " . $db->sqs($value) . "
                                                 
                                                   )";

            $success = $db->query($addRiskAssessmentItemSql);
        }

        return $success;

    }

    /**
     * Method to add risk assessment
     * @param null $areaId
     * @param null $groupId
     * @param null $assessmentId
     * @return bool
     */
    public function addRiskAssessmentArea($areaId = null, $groupId = null, $assessmentId = null)
    {

        global $db;

        $addRiskAssessmentSql = "INSERT INTO tbl_risk_areas(  
                                                    area_id,    
                                                    risk_group_id,	    
                                                    risk_assessment_id
                                                    )
                                               VALUES(
                                                  " . $db->sqs($areaId) . ",
                                                  " . $db->sqs($groupId) . ",
                                                  " . $db->sqs($assessmentId) . "
                                                 )";

        $addRiskAssessmentResults = $db->query($addRiskAssessmentSql);

        if ($addRiskAssessmentResults) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method to add risk assessment data
     * @param array $riskData
     * @return bool|int
     */
    public function addAssessmentRiskData($riskData = array())
    {
        global $db;

        $date = new DateTime();
        $dateCreated = date_format($date, 'Y-m-d H:i:s');

        $addRiskAssessmentSql = "INSERT INTO  tbl_risk_data(  
                                                        hazard,	    
                                                        risk,	    
                                                        score,	    
                                                        final_score,	    
                                                        photo,	    
                                                        date_created,	    
                                                        created_by,	    
                                                        date_modified,	    
                                                        modified_by,
                                                        group_id,
                                                        risk_area_id,	    
                                                        risk_assessment_id)
                                               VALUES (
                                                      " . $db->sqs($riskData['riskHazard']) . ",
                                                      " . $db->sqs($riskData['risk']) . ",
                                                      " . $db->sqs($riskData['score']) . ",
                                                      " . $db->sqs($riskData['finalScore']) . ",
                                                      " . $db->sqs($riskData['riskPhoto']) . ",
                                                      " . $db->sqs($dateCreated) . ",
                                                      " . $db->sqs($riskData['createdBy']) . ",
                                                      " . $db->sqs($dateCreated) . ",
                                                      " . $db->sqs($riskData['createdBy']) . ",
                                                      " . $db->sqs($riskData['groupId']) . ",
                                                      " . $db->sqs($riskData['riskArea']) . ",
                                                      " . $db->sqs($riskData['riskId']) . "
                                                   )";


        $addRiskAssessmentResults = $db->query($addRiskAssessmentSql);

        if ($addRiskAssessmentResults) {
            return $db->insertId();
        } else {
            return false;
        }

    }

    /**
     * Method to add recommendation
     * @param array $recommendationData
     * @param null $riskDataId
     * @return bool
     */
    public function addRiskRecommendation($recommendationData = array(), $riskDataId = null)
    {

        global $db;
        $success = false;

        $recommendationTypes = $this->getRiskAssessmentsRecommendationTypes();
        foreach ($recommendationTypes as $types) {
            $type_name = explode(' ',strtoupper($types['type_name']));
            if (!empty($recommendationData[strtolower($type_name[0])])) {
                foreach ($recommendationData[strtolower($type_name[0])] as $value) {
                    $addRiskRecommendationSql = "INSERT INTO tbl_recommendations( 
                                                message,    
                                                risk_data_id,    
                                                type_id)
                                                VALUES (
                                                  ".$db->sqs($value).",
                                                  ".$db->sqs($riskDataId).",
                                                   ".$db->sqs($types['id']) . "
                                                  )";

                    $success = $db->query($addRiskRecommendationSql);
                }
            }
        }
        return $success;

    }


    /**
     * Method to edit risk assessments
     * @param array $riskData
     * @return bool
     */
    public function editRiskAssessment($riskData = array())
    {

        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $editRiskAssessmentSql = "UPDATE tbl_risk_assessment  SET  risk_assessor = " . $db->sqs($riskData['riskAssessor']) . ",	    
                                                            user_type_id =  " . $db->sqs($riskData['userTypeId']) . ",        
                                                            assert_id =  " . $db->sqs($riskData['assetId']) . ",  
                                                            risk_type_id = " . $db->sqs($riskData['riskTypeId']) . ",
                                                            risk_assessment_date =  " . $db->sqs($riskData['riskAssessmentDate']) . ",   
                                                            date_modified=  " . $db->sqs($dateModified) . ",   	    
                                                            modified_by	=  " . $db->sqs($riskData['modifiedBy']) . "
                                                 WHERE id=" . $db->sqs($riskData['riskId']);

        $editRiskAssessmentResults = $db->query($editRiskAssessmentSql);

        if ($editRiskAssessmentResults) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method to delete delete risk assessments
     * @param null $riskId
     * @return bool
     */
    public function deleteRiskAssessment($riskId = null)
    {

        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $deleteRiskAssessmentSql = "UPDATE tbl_risk_assessment SET 
                                    is_deleted	=  1,
                                    date_modified =" . $db->sqs($dateModified) . "
                        WHERE id={$db->sqs($riskId)}";

        $deleteRiskAssessmentResults = $db->query($deleteRiskAssessmentSql);

        if ($deleteRiskAssessmentResults) {
            return true;
        } else {
            return false;
        }

    }


    /**
     * Method to update selected recommendation data
     * @param array $rec_data
     * @return bool
     */
    public function updateSingleRecommendation($rec_data = array()){

        global $db;

        $updateRecommendationSql = "UPDATE tbl_recommendations SET 
                                    message	 =" .$db->sqs($rec_data['recommendation_message']) . "
                        WHERE id={$db->sqs($rec_data['recommendationId'])}";

        $updateRecommendationResults = $db->query($updateRecommendationSql);

        if ($updateRecommendationResults) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Method to mark risk assessment as complete
     * @param null $riskId
     * @return bool
     */
    public function completeRiskAssessment($riskId = null)
    {

        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $deleteRiskAssessmentSql = "UPDATE tbl_risk_assessment SET 
                                    is_complete	=  1,
                                    date_modified =" . $db->sqs($dateModified) . "
                        WHERE id={$db->sqs($riskId)}";

        $deleteRiskAssessmentResults = $db->query($deleteRiskAssessmentSql);

        if ($deleteRiskAssessmentResults) {
            return true;
        } else {
            return false;
        }

    }


    /**
     * Update risk assessment data
     * @param array $riskData
     * @return bool
     */
    public function updateAssessmentRiskData($riskData = array())
    {
        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');


        $updateRiskAssessmentSql = "UPDATE tbl_risk_data SET  
                                                        hazard = " . $db->sqs($riskData['riskHazard']) . ",    
                                                        risk = " . $db->sqs($riskData['risk']) . ",   
                                                        score = " . $db->sqs($riskData['score']) . ",   
                                                        final_score =" . $db->sqs($riskData['finalScore']) . ",	    
                                                        photo = " . $db->sqs($riskData['riskPhoto']) . ",      
                                                        date_modified = " . $db->sqs($dateModified) . ",      
                                                        modified_by  = " . $db->sqs($riskData['createdBy']) . "
                                            WHERE id={$db->sqs($riskData['data_id'])}";


        $updateRiskAssessmentResults = $db->query($updateRiskAssessmentSql);

        if ($updateRiskAssessmentResults) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Method to add recommendation
     * @param array $recommendationData
     * @param null $riskDataId
     * @return bool
     */
    public function  updateMultipleRiskRecommendation($recommendationData = array(),$riskDataId = null){


        global $db;
        $success = false;

        $recommendationTypes = $this->getRiskAssessmentsRecommendationTypes();
        foreach ($recommendationTypes as $types) {
            $type_name = explode(' ',strtoupper($types['type_name']));
            if (!empty($recommendationData[strtolower($type_name[0])])) {

                foreach ($recommendationData[strtolower($type_name[0])] as $key=>$value) {

                    if(is_array($value)) {//Its an array, now we must update

                        $updRiskRecommendationSql = "UPDATE tbl_recommendations SET  
                                                        message = " . $db->sqs($value[0]) . "
                                            WHERE id={$db->sqs($key)} AND risk_data_id=".$db->sqs($riskDataId);

                        $success = $db->query($updRiskRecommendationSql);

                    }else{

                        $updateAddRiskRecommendationSql = "INSERT INTO tbl_recommendations( 
                                                message,    
                                                risk_data_id,    
                                                type_id)
                                                VALUES (
                                                  " . $db->sqs($value) . ",
                                                  " . $db->sqs($riskDataId) . ",
                                                   " . $db->sqs($types['id']) . "
                                                  )";

                        $success = $db->query($updateAddRiskRecommendationSql);

                    }
                }
            }
        }

        return $success;
    }



}
