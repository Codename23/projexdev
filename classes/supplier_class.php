<?php
/**
 * Supplier class containing all supplier methods
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */


class suppliers{


    /**
     * supplier constructor.
     *Initialize the class
     */
    function  __construct()
    {

    }


    /**
     * Method to get all suppliers
     * @param null $branchId
     * @return array|bool
     */
    public function getAllSuppliers($branchId =  null){

        global $db;

        $allSuppliersSql = "SELECT * FROM tbl_suppliers WHERE is_deleted = 0 AND branch_id =".$db->sqs($branchId);

        $getAllSupplierResult = $db->getAll($allSuppliersSql);


        if($getAllSupplierResult){

            return $getAllSupplierResult;
        }else{

            return false;
        }

    }
    public function getAllSupplierByGroupId($suppliers)
    {
        global $db;
        $sql = "SELECT supplier_name FROM tbl_suppliers WHERE id in (".$db->sqs($suppliers).")";
        $clean_string = str_replace("'", "", $sql);
        $result = $db->getAll($clean_string);
        $str = "";
        $counter  = 0;
        foreach ($result as $supplier)
        {
            $counter++;
            
            if($counter ==  1)
            {
                $str = $supplier["supplier_name"];
            }
            if($counter > 1)
            {
                $str .= ", ".$supplier["supplier_name"];
            }
            
        }
        return $str;
    }
    /**
     * Method to return selected supplier details
     * @return company object
     */
    public function getSupplierInfo($supplierId){
        global $db;

        $selectedSupplierSql = "SELECT *
                            FROM tbl_suppliers
                            WHERE id =".$db->sqs($supplierId);

        $getSupplierResult = $db->getRow($selectedSupplierSql);

        if($getSupplierResult){
            return $getSupplierResult;

        }else{
            return false;
        }

    }


    /**
     * Method to add new supplier
     * @param array $supplierData
     * @return bool
     */
	public function addSupplier($supplierData = array()){

        global $db;

        $date = new DateTime();
        $dateCreated = date_format($date, 'Y-m-d H:i:s');

        //business_description,
        //suppier_address
        $addSupplierSql = "INSERT INTO tbl_suppliers  ( 
                                                    branch_id,
                                                    supplier_name,	    
                                                    vat_number,    
                                                    pty_number,    
                                                    office_number,	    
                                                    contact_person,	    
                                                    cell_number,
                                                    supplier_email,    
                                                    website,
                                                    bee_certificate, 
                                                    supplier_address, 
                                                    supplier_town,
                                                    provience,
                                                    supporting_document,	    
                                                    service_type,    
                                                    supplier_details,	    
                                                    service_frequency,	    
                                                    is_construction,    
                                                    is_high_risk,    
                                                    compliance_description,	    
                                                    is_audited,   
                                                    audit_type,    
                                                    audit_frequency,	    
                                                    next_audit,	    
                                                    created_by,	    
                                                    date_created,	    
                                                    modified_by,    
                                                    date_modified
                                                    )
                                               VALUES (
                                                ".$db->sqs($supplierData['branchId']).",
                                                  ".$db->sqs($supplierData['companyName']).",
                                                  ".$db->sqs($supplierData['vatNumber']).",
                                                  ".$db->sqs($supplierData['ptyNumber']).",
                                                  ".$db->sqs($supplierData['officeNumber']).",
                                                  ".$db->sqs($supplierData['contactPerson']).",
                                                  ".$db->sqs($supplierData['contactNumber']).",
                                                  ".$db->sqs($supplierData['email']).",
                                                  ".$db->sqs($supplierData['website']).",
                                                  ".$db->sqs($supplierData['beeCertificate']).",
                                                  ".$db->sqs($supplierData['headOfficeDetails']).",
                                                  ".$db->sqs($supplierData['headOfficeTown']).",
                                                  ".$db->sqs($supplierData['headOfficeProvince']).",
                                                  ".$db->sqs($supplierData['uploadDocuments']).",
                                                  ".$db->sqs($supplierData['serviceType']).",
                                                  ".$db->sqs($supplierData['supplyDetails']).",
                                                  ".$db->sqs($supplierData['serviceFrequency']).",
                                                  ".$db->sqs($supplierData['complianceConstruction']).",
                                                  ".$db->sqs($supplierData['complianceHighRisk']).",
                                                  ".$db->sqs($supplierData['complianceDescription']).",
                                                  ".$db->sqs($supplierData['applicableSupplier']).",
                                                  ".$db->sqs($supplierData['auditType']).",
                                                  ".$db->sqs($supplierData['auditFrequency']).",
                                                  ".$db->sqs($supplierData['nextAuditMonth']).",
                                                  ".$db->sqs($supplierData['createdBy']).",
                                                  ".$db->sqs($dateCreated).",
                                                  ".$db->sqs($supplierData['modifiedBy']).",
                                                  ".$db->sqs($dateCreated)."
                                      
                                      )";

        $addSupplierResults = $db->query($addSupplierSql);

        if($addSupplierResults){
            return true;
        }else{
            return false;
        }
	}

    /**
     * Method to update supplier info
     * @param array $supplierData
     * @return bool
     */
	public function editSupplier($supplierData = array() ){
		
		global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $editSupplierInfoSql = "UPDATE tbl_suppliers SET    
                                                    supplier_name = ".$db->sqs($supplierData['companyName'])." ,  
                                                    vat_number= ".$db->sqs($supplierData['vatNumber'])." ,  
                                                    pty_number= ".$db->sqs($supplierData['ptyNumber'])." ,   
                                                    office_number= ".$db->sqs($supplierData['officeNumber'])." ,	    
                                                    contact_person= ".$db->sqs($supplierData['contactPerson'])." ,   
                                                    cell_number= ".$db->sqs($supplierData['contactNumber'])." ,
                                                    supplier_email= ".$db->sqs($supplierData['email'])." ,   
                                                    website= ".$db->sqs($supplierData['website'])." ,
                                                    supplier_address= ".$db->sqs($supplierData['headOfficeDetails'])." ,
                                                    supplier_town= ".$db->sqs($supplierData['headOfficeTown'])." ,
                                                    provience= ".$db->sqs($supplierData['headOfficeProvince'])." ,
                                                    supporting_document= ".$db->sqs($supplierData['uploadDocuments'])." ,    
                                                    service_type = ".$db->sqs($supplierData['serviceType'])." ,   
                                                    supplier_details = ".$db->sqs($supplierData['supplyDetails'])." ,   
                                                    service_frequency = ".$db->sqs($supplierData['serviceFrequency'])." ,	    
                                                    is_construction = ".$db->sqs($supplierData['complianceConstruction'])." ,   
                                                    is_high_risk = ".$db->sqs($supplierData['complianceHighRisk'])." ,    
                                                    compliance_description = ".$db->sqs($supplierData['complianceDescription'])." ,	    
                                                    is_audited = ".$db->sqs($supplierData['applicableSupplier'])." ,  
                                                    audit_type = ".$db->sqs($supplierData['auditType'])." ,   
                                                    audit_frequency = ".$db->sqs($supplierData['auditFrequency'])." ,	    
                                                    next_audit = ".$db->sqs($supplierData['nextAuditMonth'])." ,     
                                                    modified_by = ".$db->sqs($supplierData['modifiedBy'])." ,    
                                                    date_modified = ".$db->sqs($dateModified)." 
                                                    WHERE id={$db->sqs($supplierData['supplierId'])}";

        $editSupplierInfoResults = $db->query($editSupplierInfoSql);

        if($editSupplierInfoResults){
            return true;
        }else{
            return false;
        }
	}

    /**
     * Method to delete supplier
     * @param null $supplierId
     * @return bool
     */
	public function deleteSupplier($supplierId = null){

        global $db;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $deleteProviderSql = "UPDATE tbl_suppliers SET 
                                    is_deleted	=  1,
                                    date_modified =".$db->sqs($dateModified)."
                        WHERE id={$db->sqs($supplierId)}";

        $deleteProviderResults = $db->query($deleteProviderSql);

        if($deleteProviderResults){
            return true;
        }else{
            return false;
        }
    }
   /**
     * Method to search client
     */
    public function searchSupplier($companyName, $fullname){
        global $db;

        $sql = "SELECT * FROM tbl_suppliers WHERE id = " . $db->sqs($companyName) ." AND contact_person LIKE '%" . $fullname."%'";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
    /**
     * Method 
     */
     public function getSupplierCompanies(){
        global $db;
        $sql = "SELECT id, supplier_name FROM tbl_suppliers";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
}