<?php

/**
 * Class containing all methods providing asset functionality
 * 
 * @package sheqonline
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
class asset {

    /**
     * Method to initiate the class
     * @access public
     */   
    
    public function __construct() {
        
    }
    /**
     * Method to add asset
     * @access public
     * @global $db
     * @param array $assetDetailsArr
     * @return boolean
     */
    public function addAsset($assetDetailsArr) 
    {
        global $db;
        $response = array();
        
        if(is_array($assetDetailsArr)){
            if (isset($assetDetailsArr['description']) 
                && isset($assetDetailsArr['departmentid'])
                && isset($assetDetailsArr['assetcategoryid'])
                && isset($assetDetailsArr['assettypeid'])) {
                    $assetid = $this->addAssetMaterial();
                    $sql = "INSERT INTO tbl_assets (id, department_id, categorytype_id, resourcetype_id, description, manufacture_name, company_asset_number, serial_number,  date_purchased, cost, notes, question_answers, created_by)"
                        . " VALUES (" . $db->sqs($assetid) . ","
                        . $db->sqs($assetDetailsArr['departmentid']) . ","
                        . $db->sqs($assetDetailsArr['assetcategoryid']) . ", "
                        . $db->sqs($assetDetailsArr['assettypeid']) . ", "
                        . $db->sqs($assetDetailsArr['description']) . ", "
                        . $db->sqs($assetDetailsArr['manufacture']) . ", "
                        . $db->sqs($assetDetailsArr['companyassetnumber']) . ", "
                        . $db->sqs($assetDetailsArr['serialnumber']) . ", "
                        . $db->sqs($assetDetailsArr['datepurchased']) . ", "
                        . $db->sqs($assetDetailsArr['cost']) . ", " 
                        . $db->sqs($assetDetailsArr['notes']) . ", "
                        . $db->sqs($assetDetailsArr['answers']) . ", "
                        . $db->sqs($_SESSION['user_id']) . ")";
                $response['response'] = $db->query($sql);
                //$response['asset_id'] = $db->insertId();
                $this->addAssetBydepartment($assetid, $assetDetailsArr['departmentid']);
                $this->addAssetPhotos($response['asset_id']);
                if($response['response'])
                {
                    return $response;
                }else{
                    return false;
                }  
            } else {
                return "Required information is missing";
            }
        }
    }
    public function addAssetToMaintenance($id) 
    {
        global $db;
        $response = array();
         $sql = "INSERT INTO tbl_active_maintenance_asset(asset_id, branch_id, company_id,created_by)"
         . " VALUES (" . $db->sqs($id) . ","
         . $db->sqs($_SESSION['branch_id']) . ","
          . $db->sqs($_SESSION['company_id']) . ", "
          . $db->sqs($_SESSION['user_id']) . ")";
        $response['response'] = $db->query($sql);
        if($response['response'])
        {
            return $response;
        }
        else
        {
            return false;
        }  
    }
    public function addAssetToStatutory($id) 
    {
        global $db;
        $response = array();
         $sql = "INSERT INTO tbl_statutory_maintenance_asset(asset_id, branch_id, company_id,created_by)"
         . " VALUES (" . $db->sqs($id) . ","
         . $db->sqs($_SESSION['branch_id']) . ","
          . $db->sqs($_SESSION['company_id']) . ", "
          . $db->sqs($_SESSION['user_id']) . ")";
        $response['response'] = $db->query($sql);
        if($response['response'])
        {
            return $response;
        }
        else
        {
            return false;
        }  
    }
    public static function add_statutory_maintenance_plan($data = array())
    {
       global $db;
        $response = array();
         $sql = "INSERT INTO tbl_statutory_maintenance_plan(statutory_id,int_ex, type_id,description,notes,frequency,due_date,branch_id,company_id,created_by)"
         . " VALUES (" .$db->sqs($data["statutory_id"]). ","
         . $db->sqs($data["int_ex"]) . ","
         . $db->sqs($data["type_id"]) . ","
         . $db->sqs($data["description"]) . ","
         . $db->sqs($data["notes"]) . ","
          . $db->sqs($data["frequency"]) . ","
          . $db->sqs($data["due_date"]) . ","               
         . $db->sqs($_SESSION['branch_id']) . ","
          . $db->sqs($_SESSION['company_id']) . ", "
          . $db->sqs($_SESSION['user_id']) . ")";
        $response['response'] = $db->query($sql);
        if($response['response'])
        {
            return $response;
        }
        else
        {
            return false;
        }  
    }
    /**
     * Method to add material or asset
     * @access public
     * @global $db
     * @return int 
     */
    public function addAssetMaterial()
    {
        global $db;
        $sql = "INSERT INTO tbl_asset_material (type, created_by) VALUES (1, ". $db->sqs($_SESSION['company_id']) . ")";
        $response = $db->query($sql);
        if($response){
            return $db->insertId();
        }else{
            return false;
        }     
    }
     /**
     * Method to add asset documents
     * @access public
     * @param array $assetDetailsArr
     * @return boolean
     */
    public function addAssetDocument($assetid, $description) 
    {
        global $db;
        
        
        if(!file_exists(FOLDER_DIRECTORY_ROOT.'documents/')){
           mkdir(FOLDER_DIRECTORY_ROOT.'documents/',0777, true); 
        }
        
        $target_file = FOLDER_DIRECTORY_ROOT .'documents/'. basename($_FILES['docUpload']["name"]);
        $uploadOk = 1;

        // Check if file already exists
        if(file_exists($target_file)){
            return "File already exists.";
            $uploadOk = 0;
        } 
        
        // Check file size
        if($_FILES['docUpload']["size"] > 50000000){
            return "File is too large.";
            $uploadOk = 0;
        }

        if(move_uploaded_file($_FILES['docUpload']["tmp_name"], $target_file)){
            $sql = "INSERT INTO tbl_asset_document (asset_id, document_location, description, filename, created_by) VALUES (".$db->sqs($assetid)." ,  ".$db->sqs($target_file)." , ".$db->sqs($description).", ".$db->sqs(basename($_FILES['docUpload']["name"])).", ".$db->sqs($_SESSION['user_id']).")";
            $result = $db->query($sql);
        }else{
            return "There was an error uploading your file.";
        }
    }
     /**
     * Method to upload asset photos
     * @access public
     * @param array $assetid
     * @return boolean
     */
    public function addAssetPhotos($assetid)
    {
        global $db;
        
        if(!file_exists(FOLDER_DIRECTORY_ROOT.'images/')){
           mkdir(FOLDER_DIRECTORY_ROOT.'images/',0777, true); 
        }
        $target_file = FOLDER_DIRECTORY_ROOT .'images/'. basename($_FILES['uploadPhotos']["name"]);
        $uploadOk = 1;
        
        // Check if file already exists
        if(file_exists($target_file)){
            return "File already exists.";
            $uploadOk = 0;
        } 
        
        // Check file size
        if($_FILES['uploadPhotos']["size"] > 5000000){
            return "File is too large.";
            $uploadOk = 0;
        }
        
        if(move_uploaded_file($_FILES['uploadPhotos']["tmp_name"], $target_file)){
            $sql = "INSERT INTO tbl_asset_photo (asset_id, document_location, filename, created_by) VALUES (".$db->sqs($assetid)." , ".$db->sqs($target_file).", ".$db->sqs(basename($_FILES['uploadPhotos']["name"])).", ".$db->sqs($_SESSION['user_id']).")";
            $result = $db->query($sql);
            return true;
        } else {
            return true;
        }
    }
    /**
     * Method to add third party asset 
     * @access public
     * @global $db
     * @param array $assetDetailsArr
     * @return boolean
     */
    public function addThirdPartyAsset($assetDetailsArr)
    {
        global $db;
        if(is_array($assetDetailsArr)){
            if (isset($assetDetailsArr['assetType']) 
            && !empty($assetDetailsArr['description'])
            && !empty($assetDetailsArr['companyassetnumber'])) {
                $sql = "INSERT INTO tbl_assets_thirdparty (incident_id, assetType, description, company_asset_number, effect_on_asset, created_by) VALUES (".$db->sqs($_SESSION['incidentid'])." , " . $db->sqs($assetDetailsArr['assetType']) . ",". $db->sqs($assetDetailsArr['description']) . ",". $db->sqs($assetDetailsArr['companyassetnumber']).",". $db->sqs($assetDetailsArr['effectonasset']) . ",". $db->sqs($_SESSION['user_id']) . ")";
                $response = $db->query($sql);
                if($response){
                    return $response;
                }else{
                    return false;
                }
            } else {
                return "Required information is missing";
            }
        }
    }
    /**
     * Method to add department asset belong to
     * @access public
     * @global $db
     * @param int $assetid
     * @param int $departmentid
     * @return boolean
     */
    public function addAssetBydepartment($assetid, $departmentid)
    {
        global $db;
        if(isset($assetid) && isset($departmentid)){
            $sql = "INSERT INTO tbl_department_assets (department_id, assets_id, branch_id, company_id ) VALUES (". $db->sqs($departmentid) . " , ". $db->sqs($assetid) . " , ". $db->sqs($_SESSION['branch_id']) . " , ". $db->sqs($_SESSION['company_id']) . ")";
            $response = $db->query($sql);
            if($response){
                return $response;
            }else{
                return false;
            }   
        }else{
            return "Required information is missing";
        }  
    }
    /**
     * Method to edit asset
     * @access public
     * @global $db
     * @param array $assetDetailsArr
     * @return boolean
     */
    public function editAsset($assetDetailsArr) 
    {
        global $db;
        if (is_array($assetDetailsArr)){
            if (isset($assetDetailsArr['description']) 
                && isset($assetDetailsArr['departmentid'])
                && isset($assetDetailsArr['assetcategoryid'])
                && isset($assetDetailsArr['assettypeid'])) {
                $sql = "UPDATE tbl_assets SET 
                          department_id =" . $db->sqs($assetDetailsArr['departmentid'])
                        . ", categorytype_id =" . $db->sqs($assetDetailsArr['assetcategoryid'])
                        . ", resourcetype_id =" . $db->sqs($assetDetailsArr['assettypeid'])
                        . ", name =" . $db->sqs($assetDetailsArr['name'])
                        . ", description =" . $db->sqs($assetDetailsArr['description'])
                        . ", manufacture_name =" . $db->sqs($assetDetailsArr['manufacture'])
                        . ", company_asset_number =" . $db->sqs($assetDetailsArr['companyassetnumber'])
                        . ", serial_number =" . $db->sqs($assetDetailsArr['serialnumber'])
                        . ", date_purchased =" . $db->sqs($assetDetailsArr['datepurchased'])
                        . ", expiryDate =" . $db->sqs($assetDetailsArr['expiryDate'])
                        . ", cost =" . $db->sqs($assetDetailsArr['cost'])
                        . ", modified_by = " . $db->sqs($_SESSION['user_id'])
                        . " WHERE id = " . $db->sqs($assetDetailsArr['id']);
                $response = $db->query($sql);
                $response = $this->editAssetBydepartment($assetDetailsArr['id'], $assetDetailsArr['departmentid']);
                if($response){
                    return $response;
                }else{
                    return false;
                }
            } else {
                return "Required information is missing";
            }
        }
    }
    /**
     * Method to edit department asset
     * @access public
     * @global $db
     * @param int $assetid
     * @param int $departmentid     
     * @return boolean
     */
    public function editAssetBydepartment($assetid, $departmentid)
    {
        global $db;
        if(isset($assetid) && isset($departmentid)){
            $sql = "UPDATE tbl_department_assets SET department_id =" . $db->sqs($departmentid)." WHERE assets_id =" . $assetid;
            $response = $db->query($sql);
            if($response){
                return true;
            }else{
                return false;
            } 
        }else{
            return "Required information is missing";
        }  
    }
    /**
     * Method to delete asset
     * @access public
     * @param int $id
     * @return boolean
     */
    public function deleteAsset($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "DELETE FROM tbl_assets WHERE id = " . $db->sqs($id);
            $response = $db->query($sql);
            if($response){
                return true;
            }else{
                return false;
            }
        }else{
            return "Required information is missing";
        }
    }
    /**
     * Method to activate asset
     * @access public
     * @param int $id
     * @return boolean
     */
    public function activateAsset($id)
    {
        global $db;
        if (!is_null($id)) {
            $sql = "UPDATE tbl_assets SET is_active = 1 WHERE id = " . $db->sqs($id);
            $response = $db->query($sql);
            if($response) {
                return true;
            } else {
                return false;
            }
        }else{
            return "Required information is missing";
        }
    }
    /**
     * Method to deactivate or delete asset
     * @access public
     * @param int $id
     * @return boolean
     */
    public function deactivateAsset($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "UPDATE tbl_assets SET is_active = 0 WHERE id = " . $db->sqs($id);
            $response = $db->query($sql);
            if($response) {
                return true;
            } else {
                return false;
            }
        }else{
            return "Required information is missing";
        }
    }
    /**
     * Method to get one asset
     * @access public
     * @param int $id
     * @return boolean
     */
    public function getoneAsset($id)
    {
        global $db;
        $sql ="SELECT tbl_assets.*,
                      tbl_departments.id as departmentId,
                      tbl_departments.department_name,
                      tbl_asset_category.name as category_name,
                      tbl_asset_type.name as asset_name,
                      tbl_asset_category.name AS categoryType,
                      tbl_asset_type.name AS assetType
                      FROM tbl_department_assets 
                      INNER JOIN tbl_assets ON tbl_department_assets.assets_id = tbl_assets.id
                      INNER JOIN tbl_departments ON tbl_department_assets.department_id = tbl_departments.id
                      INNER JOIN tbl_branches ON tbl_department_assets.branch_id = tbl_branches.id
                      INNER JOIN tbl_companies ON tbl_department_assets.company_id = tbl_companies.id
                      INNER JOIN tbl_asset_category ON tbl_asset_category.id = tbl_assets.categorytype_id
                      INNER JOIN tbl_asset_type ON tbl_asset_type.id = tbl_assets.resourcetype_id
                      WHERE tbl_assets.id = " . $db->sqs($id) . " 
                      ORDER BY tbl_assets.id ";
        $response = $db->getRow($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    }
    /**
     * Method to get all asset and material
     * @access public
     * @param boolean $activeOnly
     * @return boolean
     */
    public function getMaterialAssets($activeOnly = false)
    {
        global $db;
        $sql ="SELECT tbl_asset_material.*,
                      tbl_assets.*,
                      tbl_assets.id as asset_id,
                      tbl_material.*,
                      tbl_material.id as material_id
                      FROM tbl_asset_material 
                      LEFT JOIN tbl_assets ON tbl_asset_material.id = tbl_assets.id
                      LEFT JOIN tbl_material ON tbl_asset_material.id = tbl_material.id";
        if($activeOnly){
            $sql .= " AND tbl_asset_material.is_active = 0";
        }else{
            $sql .= " AND tbl_asset_material.is_active = 1";
        }
        $sql .= " ORDER BY tbl_asset_material.id";
        $response = $db->getAll($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    }
    /**
     * Method to get all assets
     * @access public
     * @param boolean $activeOnly
     * @return boolean
     */
    public function getAllAsset($activeOnly = false)
    {
        global $db;
        $sql ="SELECT tbl_departments.id as departmentId,
                      tbl_departments.department_name,
                      tbl_asset_category.name as category_name,
                      tbl_asset_type.name as asset_name,
                      tbl_assets.id,tbl_assets.description as as_name,
                      tbl_assets.description,
                      question_answers,
                      manufacture_name,
                      company_asset_number,
                      serial_number,
                      date_purchased,
                      notes,
                      cost,
                      tbl_assets.is_active,
                      tbl_asset_category.name AS categoryType,
                      tbl_asset_type.name AS assetType
                      FROM tbl_department_assets 
                      INNER JOIN tbl_assets ON tbl_department_assets.assets_id = tbl_assets.id
                      INNER JOIN tbl_departments ON tbl_department_assets.department_id = tbl_departments.id
                      INNER JOIN tbl_branches ON tbl_department_assets.branch_id = tbl_branches.id
                      INNER JOIN tbl_companies ON tbl_department_assets.company_id = tbl_companies.id
                      INNER JOIN tbl_asset_category ON tbl_asset_category.id = tbl_assets.categorytype_id
                      INNER JOIN tbl_asset_type ON tbl_asset_type.id = tbl_assets.resourcetype_id
                      WHERE tbl_companies.id = " . $db->sqs($_SESSION['company_id']); 
        
        if ($activeOnly) {
            $sql .= " AND tbl_assets.is_active = 0";
        }else{
            $sql .= " AND tbl_assets.is_active = 1";
        }
        $sql .= " ORDER BY tbl_assets.id";
        $response = $db->getAll($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    }
    public function getAllAssetsInSource($activeOnly = false,$source)
    {
        global $db;
        $sql ="SELECT tbl_departments.id as departmentId,
                      tbl_departments.department_name,
                      tbl_asset_category.name as category_name,
                      tbl_asset_type.name as asset_name,
                      tbl_assets.id,tbl_assets.description as as_name,
                      tbl_assets.description,
                      question_answers,
                      manufacture_name,
                      company_asset_number,
                      serial_number,
                      date_purchased,
                      notes,
                      cost,
                      tbl_assets.is_active,
                      tbl_asset_category.name AS categoryType,
                      tbl_asset_type.name AS assetType
                      FROM tbl_department_assets 
                      INNER JOIN tbl_assets ON tbl_department_assets.assets_id = tbl_assets.id
                      INNER JOIN tbl_departments ON tbl_department_assets.department_id = tbl_departments.id
                      INNER JOIN tbl_branches ON tbl_department_assets.branch_id = tbl_branches.id
                      INNER JOIN tbl_companies ON tbl_department_assets.company_id = tbl_companies.id
                      INNER JOIN tbl_asset_category ON tbl_asset_category.id = tbl_assets.categorytype_id
                      INNER JOIN tbl_asset_type ON tbl_asset_type.id = tbl_assets.resourcetype_id
                      WHERE tbl_assets.id in ($source) and tbl_companies.id = " . $db->sqs($_SESSION['company_id']); 
        
        if ($activeOnly) {
            $sql .= " AND tbl_assets.is_active = 0";
        }else{
            $sql .= " AND tbl_assets.is_active = 1";
        }
        $sql .= " ORDER BY tbl_assets.id";
        $response = $db->getAll($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    }
    public function getAllAssetsNotImported($activeOnly = false)
    {
        global $db;
        $sql ="SELECT tbl_departments.id as departmentId,
                      tbl_departments.department_name,
                      tbl_asset_category.name as category_name,
                      tbl_asset_type.name as asset_name,
                      tbl_assets.id,
                      tbl_assets.description,
                      question_answers,
                      manufacture_name,
                      company_asset_number,
                      serial_number,
                      date_purchased,
                      notes,
                      cost,
                      tbl_assets.is_active,
                      tbl_asset_category.name AS categoryType,
                      tbl_asset_type.name AS assetType
                      FROM tbl_department_assets 
                      INNER JOIN tbl_assets ON tbl_department_assets.assets_id = tbl_assets.id
                      INNER JOIN tbl_departments ON tbl_department_assets.department_id = tbl_departments.id
                      INNER JOIN tbl_branches ON tbl_department_assets.branch_id = tbl_branches.id
                      INNER JOIN tbl_companies ON tbl_department_assets.company_id = tbl_companies.id
                      INNER JOIN tbl_asset_category ON tbl_asset_category.id = tbl_assets.categorytype_id
                      INNER JOIN tbl_asset_type ON tbl_asset_type.id = tbl_assets.resourcetype_id
                      WHERE tbl_department_assets.assets_id not in (
                      select asset_id from tbl_active_maintenance_asset
                      where company_id = tbl_companies.id
                      and is_active = 1
                      ) and tbl_companies.id = " . $db->sqs($_SESSION['company_id']); 
       // echo $sql;
        if ($activeOnly) {
            $sql .= " AND tbl_assets.is_active = 0";
        }else{
            $sql .= " AND tbl_assets.is_active = 1";
        }
        $sql .= " ORDER BY tbl_assets.id";
        $response = $db->getAll($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    }    
    public function getAllActiveAssetsNotImported($activeOnly = false)
    {
        global $db;
        $sql ="SELECT tbl_departments.id as departmentId,
                      tbl_departments.department_name,
                      tbl_asset_category.name as category_name,
                      tbl_asset_type.name as asset_name,
                      tbl_assets.id,
                      tbl_assets.description,
                      question_answers,
                      manufacture_name,
                      company_asset_number,
                      serial_number,
                      date_purchased,
                      notes,
                      cost,
                      tbl_assets.is_active,
                      tbl_asset_category.name AS categoryType,
                      tbl_asset_type.name AS assetType
                      FROM tbl_department_assets 
                      INNER JOIN tbl_assets ON tbl_department_assets.assets_id = tbl_assets.id
                      INNER JOIN tbl_departments ON tbl_department_assets.department_id = tbl_departments.id
                      INNER JOIN tbl_branches ON tbl_department_assets.branch_id = tbl_branches.id
                      INNER JOIN tbl_companies ON tbl_department_assets.company_id = tbl_companies.id
                      INNER JOIN tbl_asset_category ON tbl_asset_category.id = tbl_assets.categorytype_id
                      INNER JOIN tbl_asset_type ON tbl_asset_type.id = tbl_assets.resourcetype_id
                      WHERE tbl_department_assets.assets_id not in (
                      select asset_id from tbl_statutory_maintenance_asset
                      where company_id = tbl_companies.id
                      and is_active = 1
                      ) and tbl_companies.id = " . $db->sqs($_SESSION['company_id']); 
       // echo $sql;
        if ($activeOnly) {
            $sql .= " AND tbl_assets.is_active = 0";
        }else{
            $sql .= " AND tbl_assets.is_active = 1";
        }
        $sql .= " ORDER BY tbl_assets.id";
        $response = $db->getAll($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    }
   public function getAllAssetStatutory($activeOnly = false)
    {
        global $db;
        $sql ="SELECT tbl_departments.id as departmentId,
                      tbl_departments.department_name,
                      tbl_asset_category.name as category_name,
                      tbl_asset_type.name as asset_name,
                      tbl_assets.id,
                      tbl_statutory_maintenance_asset.id as record_id,
                      tbl_assets.description,
                      question_answers,
                      manufacture_name,
                      company_asset_number,
                      serial_number,
                      date_purchased,
                      notes,
                      cost,
                      tbl_assets.is_active,
                      tbl_asset_category.name AS categoryType,
                      tbl_asset_type.name AS assetType
                      FROM tbl_statutory_maintenance_asset
                      INNER JOIN tbl_department_assets 
                      on tbl_statutory_maintenance_asset.asset_id = tbl_department_assets.assets_id
                      INNER JOIN tbl_assets ON tbl_department_assets.assets_id = tbl_assets.id
                      INNER JOIN tbl_departments ON tbl_department_assets.department_id = tbl_departments.id
                      INNER JOIN tbl_branches ON tbl_department_assets.branch_id = tbl_branches.id
                      INNER JOIN tbl_companies ON tbl_department_assets.company_id = tbl_companies.id
                      INNER JOIN tbl_asset_category ON tbl_asset_category.id = tbl_assets.categorytype_id
                      INNER JOIN tbl_asset_type ON tbl_asset_type.id = tbl_assets.resourcetype_id
                      WHERE tbl_companies.id = " . $db->sqs($_SESSION['company_id']); 
        
        if ($activeOnly) {
            $sql .= " AND tbl_assets.is_active = 0";
        }else{
            $sql .= " AND tbl_assets.is_active = 1";
        }
        $sql .= " ORDER BY tbl_assets.id";
        $response = $db->getAll($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    }
    public function getAssetStatutoryById($id)
    {
        global $db;
        $sql ="SELECT tbl_departments.id as departmentId,
                      tbl_departments.department_name,
                      tbl_asset_category.name as category_name,
                      tbl_asset_type.name as asset_name,
                      tbl_assets.id as asset_id,
                      tbl_assets.description,
                      question_answers,
                      manufacture_name,
                      company_asset_number,
                      serial_number,
                      date_purchased,
                      notes,
                      cost,
                      tbl_assets.is_active,
                      tbl_asset_category.name AS categoryType,
                      tbl_asset_type.name AS assetType
                      FROM tbl_statutory_maintenance_asset
                      INNER JOIN tbl_department_assets 
                      on tbl_statutory_maintenance_asset.asset_id = tbl_department_assets.assets_id
                      INNER JOIN tbl_assets ON tbl_department_assets.assets_id = tbl_assets.id
                      INNER JOIN tbl_departments ON tbl_department_assets.department_id = tbl_departments.id
                      INNER JOIN tbl_branches ON tbl_department_assets.branch_id = tbl_branches.id
                      INNER JOIN tbl_companies ON tbl_department_assets.company_id = tbl_companies.id
                      INNER JOIN tbl_asset_category ON tbl_asset_category.id = tbl_assets.categorytype_id
                      INNER JOIN tbl_asset_type ON tbl_asset_type.id = tbl_assets.resourcetype_id
                      WHERE tbl_statutory_maintenance_asset.id =". $db->sqs($id).""
                . "    and tbl_companies.id = " . $db->sqs($_SESSION['company_id']); 
      //  echo $sql;     
        $response = $db->getAll($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    }
    public function getUpcomingStatutory()
    {
             global $db;
        $sql ="SELECT tbl_statutory_maintenance_plan.id as plan_id,tbl_departments.id as departmentId,
                      tbl_departments.department_name,
                      tbl_asset_category.name as categoryType,
                      tbl_assets.id,
                      tbl_statutory_maintenance_asset.id as record_id,
                      tbl_assets.description as asset_description ,
                      company_asset_number,
                      serial_number,
                      tbl_statutory_maintenance_plan.notes as maintenance_notes,
                      tbl_asset_type.name AS assetType,
                      tbl_statutory_maintenance_plan.int_ex,
                      tbl_statutory_maintenance_plan.type_id,
                      tbl_statutory_maintenance_plan.description as maintenance_description,
                      tbl_statutory_maintenance_plan.due_date,
                      tbl_statutory_maintenance_plan.frequency
                      FROM tbl_statutory_maintenance_plan
                      inner join tbl_statutory_maintenance_asset
                      on tbl_statutory_maintenance_plan.statutory_id = tbl_statutory_maintenance_asset.id
                      INNER JOIN tbl_department_assets 
                      on tbl_statutory_maintenance_asset.asset_id = tbl_department_assets.assets_id
                      INNER JOIN tbl_assets ON tbl_department_assets.assets_id = tbl_assets.id
                      INNER JOIN tbl_departments ON tbl_department_assets.department_id = tbl_departments.id
                      INNER JOIN tbl_branches ON tbl_department_assets.branch_id = tbl_branches.id
                      INNER JOIN tbl_companies ON tbl_department_assets.company_id = tbl_companies.id
                      INNER JOIN tbl_asset_category ON tbl_asset_category.id = tbl_assets.categorytype_id
                      INNER JOIN tbl_asset_type ON tbl_asset_type.id = tbl_assets.resourcetype_id
                      WHERE tbl_companies.id = " . $db->sqs($_SESSION['company_id']); 
        $response = $db->getAll($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    }
    /**
     * Method to search asset
     * @access public
     * @param String $searchTerm
     * @return boolean
     */
    public function searchAsset($searchTerm)
    {
        global $db;
        $sql = "SELECT tbl_assets.*,
                       tbl_departments.department_name,
                       tbl_asset_category.name AS categoryType,
                       tbl_asset_type.name AS assetType
                       FROM tbl_department_assets 
                       INNER JOIN tbl_assets ON tbl_department_assets.assets_id = tbl_assets.id
                       INNER JOIN tbl_departments ON tbl_department_assets.department_id = tbl_departments.id
                       INNER JOIN tbl_branches ON tbl_department_assets.branch_id = tbl_branches.id
                       INNER JOIN tbl_companies ON tbl_department_assets.company_id = tbl_companies.id
                       INNER JOIN tbl_asset_category ON tbl_asset_category.id = tbl_assets.categorytype_id
                       INNER JOIN tbl_asset_type ON tbl_asset_type.id = tbl_assets.resourcetype_id
                       WHERE tbl_branches.id = " . $db->sqs($_SESSION['branch_id']) . " tbl_assets.name LIKE '%" . $searchTerm ."%' OR tbl_assets.company_asset_number LIKE '%" . $searchTerm ."%' LIMIT 20";
        $response = $db->getall($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    } 
    /**
     * Method to get asset type
     * @access public
     * @param boolean $activeOnly
     * @return boolean
     */
    public function getAssetType($activeOnly = false)
    {
        global $db;
        $sql = "SELECT * FROM tbl_asset_type WHERE is_active = 1 ORDER BY date_created DESC";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
    public static function getStatutoryPlansForAsset($id)
    {
        global $db;
        $sql = "SELECT * FROM tbl_statutory_maintenance_plan WHERE statutory_id = ".$db->sqs($id)." and is_active = 1 ORDER BY date_created DESC";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
    /**
     * 
     */
    public function getAssetTypeByCategory($assetcategoryid = null)
    {
        global $db;
        $sql = "SELECT * FROM tbl_asset_type WHERE  is_active = 1 AND category_id = " . $db->sqs($assetcategoryid);
        $response = $db->getAll($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    }
    /**
     * Method to get asset category
     * @access public
     * @param boolean $activeOnly
     * @return boolean
     */
    public function getCategoryType($activeOnly = true) 
    {
        global $db;
        $sql = "SELECT * FROM tbl_asset_category ";
        if($activeOnly){
            $sql .= " WHERE is_active = 1";
        }else{
            $sql .= " WHERE is_active = 0";
        }
        $sql .= " ORDER BY date_created DESC";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        } else {
            return false;
        }
    }
    /**
     * Method to get asset questions
     * @access public
     * @param boolean $activeOnly
     * @return boolean
     */
    public function getAssetQuestions($activeOnly = true) 
    {
        global $db;
        $sql = "SELECT * FROM tbl_asset_questions WHERE";
        if($activeOnly){
            $sql .= " is_active = 1";
        } else {
            $sql .= " is_active = 0";
        }
        $sql .= " ORDER BY sort DESC";
        
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
    
    /**
     * Method to return Department assets
     * @access public
     * @global type $db
     * @param array $departmentids
     * @param int $assetcategoryid
     * @param int $assettypeid
     * @return boolean
     */
    public function getAllDepartmentCategoryTypes($departmentids = null, $assetcategoryid = null, $assettypeid = null){
        global $db;
        
        $sql = "SELECT tbl_assets.*,
                       tbl_departments.id AS departmentid,
                       tbl_departments.department_name,
                       tbl_departments.department_building,
                       tbl_asset_category.id AS assetcategoryid,
                       tbl_asset_category.name AS assetcategory_name,
                       tbl_asset_type.id AS assettypeid,
                       tbl_asset_type.name AS assettype_name
                       FROM tbl_assets
                       INNER JOIN tbl_departments ON tbl_assets.department_id = tbl_departments.id
                       INNER JOIN tbl_asset_category ON tbl_assets.categorytype_id = tbl_asset_category.id
                       INNER JOIN tbl_asset_type ON tbl_assets.resourcetype_id = tbl_asset_type.id
                       WHERE tbl_assets.is_active = 1 ";
        
        if(!is_null($assetcategoryid)){
            $departmentid = implode(',', $departmentids); 
            $sql .= " AND tbl_assets.department_id IN (" . $db->sqs($departmentid) . ")";
        }
        
        if(!is_null($assetcategoryid)){
            $sql .= " AND tbl_assets.categorytype_id = " . $db->sqs($assetcategoryid) . " ";
        }
        
        if(!is_null($assettypeid)){
            $sql .= " AND tbl_assets.resourcetype_id = " . $db->sqs($assettypeid) . " ";
        }
        
        $sql .= " GROUP BY tbl_departments.id, tbl_assets.id ORDER BY tbl_assets.date_created DESC";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }
    /**
     * Method to get all documents
     * @access public
     * @param 
     * @return boolean
     */
    public function getAssetDocuments($assetid)
    {
        global $db;
        if(isset($assetid)){
            $sql = "SELECT * FROM tbl_asset_document WHERE asset_id = " . $db->sqs($assetid); 
            $sql .= " ORDER BY id";
            $response = $db->getAll($sql);
            if($response) {
                return $response;
            } else {
                return false;
            }
        }else{
            return "Required information is missing";
        }
    }
    
    //////////// SOP //////////////////
    public static function addSOP($description,$resource,$type)
    {
        global $db;
        $status = false;
        if(isset($resource))
        {
            $sql = "INSERT INTO tbl_sop (description,source_id,source_type,active,created_by) VALUES(". $db->sqs($description) . ",". $db->sqs($resource) . ",". $db->sqs($type) . ",1, ". $db->sqs($_SESSION['user_id']) . ")";
            $response = $db->query($sql);
            if($response){
                $status = true;
                return $status;
            } 
        }else{
            return "Failed creating a standard operating procedure";
        }  
    }
    public static function getSop($resource_id,$source_type)
    {
        global $db;
        if(isset($resource_id)){
            $sql = "SELECT * FROM tbl_sop WHERE active = 1 and source_type = '{$source_type}' and source_id = " . $db->sqs($resource_id); 
            $response = $db->getAll($sql);
            if($response) {
                return $response;
            } else {
                return false;
            }
        }else{
            return "Required information is missing";
        }
    }
    //update
    public static function editSop($description,$id)
    {
         global $db;
        if(isset($id)){
            $sql = "UPDATE tbl_sop SET description = ".$db->sqs($description)." where id = " . $db->sqs($id); 
            $response = $db->query($sql);
            if($response) {
                return $response;
            } else {
                return false;
            }
        }else{
            return "Required information is missing";
        }
    }
    public static function removeSOP($id)
    {
         global $db;
        if(isset($id)){
            $sql = "UPDATE tbl_sop SET active = 0 where id = " . $db->sqs($id); 
            $response = $db->query($sql);
            if($response) {
                return $response;
            } else {
                return false;
            }
        }else{
            return "Required information is missing";
        }
    }
    
    
    //////SOP Documents Section ///////////////////
    //////////// SOP //////////////////
    public static function getAssetDoc($resource_id,$type,$source_type)
    {
        global $db;
        if(isset($resource_id)){
            $sql = "SELECT * FROM tbl_asset_document WHERE is_active = 1 and source_type = '{$source_type}' and document_type = '{$type}' and source_id = " . $db->sqs($resource_id);             
            $response = $db->getAll($sql);
            if($response) {
                return $response;
                //echo $sql;
            } else {
                return false;
            }
        }else{
            echo "Required information is missing";
        }
    }
    public static function addAssetDoc($description,$resource,$document_name,$type,$source_type)
    {
        global $db;
        $status = false;
        if(isset($resource))
        {
            $sql = "INSERT INTO tbl_asset_document (description,source_id,source_type,document_name,is_active,created_by,document_type) VALUES(". $db->sqs($description) . ",". $db->sqs($resource) . " ,'{$source_type}',". $db->sqs($document_name) . " ,1, ". $db->sqs($_SESSION['user_id']) . ",'{$type}')";
           // echo $sql;
            $response = $db->query($sql);
            if($response){
                $status = true;
                return $status;
            } 
        }else{
            return "Failed creating a standard operating procedure";
        }  
    }    
    public static function removeSOPDocument($id)
    {
         global $db;
        if(isset($id)){
            $sql = "UPDATE tbl_sop_documents SET is_active = 0 where id = " . $db->sqs($id); 
            $response = $db->query($sql);
            if($response) {
                return $response;
            } else {
                return false;
            }
        }else{
            return "Required information is missing";
        }
    }
    ///.End of SOP Documents Section///
    //STATUTORY SECTION
    public static function getStatutoryAssets()
    {
        global $db;
            $sql = "SELECT * FROM tbl_statutory_maintenance_asset WHERE is_active = 1 and company_id = ". $db->sqs($_SESSION["company_id"]);      
            $response = $db->getAll($sql);
            if($response) {
                return $response;
            } else {
                return false;
            }
    }
   public static function getActiveAssets()
    {
        global $db;
            $sql = "SELECT * FROM tbl_active_maintenance_asset WHERE is_active = 1 and company_id = ". $db->sqs($_SESSION["company_id"]);      
            $response = $db->getAll($sql);
            if($response) {
                return $response;
            } else {
                return false;
            }
    }
    public static function addAssetStatutory($description,$resource,$document_name,$type,$source_type)
    {
        global $db;
        $status = false;
        if(isset($resource))
        {
            $sql = "INSERT INTO tbl_asset_document (description,source_id,source_type,document_name,is_active,created_by,document_type) VALUES(". $db->sqs($description) . ",". $db->sqs($resource) . " ,'{$source_type}',". $db->sqs($document_name) . " ,1, ". $db->sqs($_SESSION['user_id']) . ",'{$type}')";
           // echo $sql;
            $response = $db->query($sql);
            if($response){
                $status = true;
                return $status;
            } 
        }else{
            return "Failed creating a standard operating procedure";
        }  
    }    
    
}
