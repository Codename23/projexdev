<?php
/**
 * Service class containing all service methods
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class services
{


    /**
     * Initialise service class
     * providers constructor.
     */
    function __construct()
    {

    }


    /**
     * Method to get all service types preferences
     * @param null $branchId
     * @return array|bool
     * @TODO: Get service by company/branch
     */
    public function getAllServiceTypes($branchId = null){

        global $db;

        $allProvidersSql = "SELECT * FROM tbl_service_types";

        $getAllProvidersResults = $db->getAll($allProvidersSql);


        if($getAllProvidersResults){

            return $getAllProvidersResults;
        }else{

            return false;
        }

    }




}