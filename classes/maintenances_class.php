<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of maintenances
 *
 * @author Innovatorshill
 */
class maintenances {
    //put your code here
    public $requested_id = 0;
    public function __construct() {
        
    }
    public function addMaintenanceRequest($data = array())    
    {
        if(is_array($data))
        {
             global $db;   
            $response = array();         
                    $sql = "INSERT INTO tbl_maintenance_request (source_id, source, due_date, priority, "
                            . "description, notes, department,branch_id,company_id,created_by) "
                            . " VALUES (". $db->sqs($data['source_id'])." , "
                            . $db->sqs($data['source'])." , "
                            . $db->sqs($data['due_date'])." , "
                            . $db->sqs($data['priority'])." , "
                            . $db->sqs($data['description'])." , "
                            . $db->sqs($data['notes'])." , "
                            . $db->sqs($data['deparment']).","
                            . "". $db->sqs($_SESSION['branch_id'])." , "
                            . "". $db->sqs($_SESSION['company_id'])." , "
                            . "" .$db->sqs($_SESSION['user_id']) ." )";                    
                    $response = $db->query($sql);                       
                    if($response){
                        $this->requested_id = $db->insertId();
                        return $response;
                    }else{
                        return false;
                    }  
            } 
            else
            {
               return "Required information is missing";
            }     
    }
    public static function getRequeqestedMaintenance()
    {
          global $db;
            $sql = "SELECT tbl_maintenance_request.*,concat(tbl_users.firstname,' ',tbl_users.lastname) as requested_by
from tbl_maintenance_request
inner join tbl_users
on tbl_maintenance_request.created_by = tbl_users.id
where tbl_maintenance_request.is_active = 1
and tbl_maintenance_request.company_id = ".$db->sqs($_SESSION["company_id"]);
            $response = $db->getAll($sql);
            if($response)
            {
                return $response;
            } else {
                return false;
            }
    }
    public function SignOffMaintenanceRequest($data = array())    
    {
        if(is_array($data))
        {
             global $db;   
            $response = array();         
                    $sql = "INSERT INTO tbl_maintenance_sign_off(source,source_id, maintenance_type, full_description, "
                            . "assets, part_type, part_description,part_down_time,part_replaced,part_cost,branch_id,company_id,created_by) "
                            . " VALUES (". $db->sqs($data['source'])." , "
                            . $db->sqs($data['source_id'])." , "
                            . $db->sqs($data['maintenance_type'])." , "
                            . $db->sqs($data['full_description'])." , "
                            . $db->sqs($data['description'])." , "
                            . $db->sqs($data['assets'])." , "
                            . $db->sqs($data['part_type'])." , "
                            . $db->sqs($data['part_description'])." , "
                            . $db->sqs($data['part_down_time'])." , "
                            . $db->sqs($data['part_replaced']).","
                            . $db->sqs($data['part_cost']).","
                            . "". $db->sqs($_SESSION['branch_id'])." , "
                            . "". $db->sqs($_SESSION['company_id'])." , "
                            . "" .$db->sqs($_SESSION['user_id']) ." )";                    
                    $response = $db->query($sql);                       
                    if($response){
                        return $response;
                    }else{
                        return false;
                    }  
            } 
            else
            {
               return "Required information is missing";
            }     
    }
    public function AddSupplierRating($data = array())    
    {
        if(is_array($data))
        {
             global $db;   
            $response = array();         
                    $sql = "INSERT INTO tbl_maintenance_supplier_details(source,source_id, supplier_id, rating_service, "
                            . "note_service, supporting_docs, cost_of_service,branch_id,company_id,created_by) "
                            . " VALUES (". $db->sqs($data['source'])." , "
                            . $db->sqs($data['source_id'])." , "
                            . $db->sqs($data['maintenance_type'])." , "
                            . $db->sqs($data['full_description'])." , "
                            . $db->sqs($data['description'])." , "
                            . $db->sqs($data['assets'])." , "
                            . $db->sqs($data['part_type'])." , "
                            . $db->sqs($data['part_description'])." , "
                            . $db->sqs($data['part_down_time'])." , "
                            . $db->sqs($data['part_replaced']).","
                            . $db->sqs($data['part_cost']).","
                            . "". $db->sqs($_SESSION['branch_id'])." , "
                            . "". $db->sqs($_SESSION['company_id'])." , "
                            . "" .$db->sqs($_SESSION['user_id']) ." )";                    
                    $response = $db->query($sql);                       
                    if($response){
                        return $response;
                    }else{
                        return false;
                    }  
            } 
            else
            {
               return "Required information is missing";
            }     
    }
}
