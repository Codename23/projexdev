<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of strategy_class
 *
 * @author Innovatorshill
 */
class strategy {
    //put your code here
    public function __construct() {
        
    }
    public static function viewStrategies($id)
    {
        global $db;
        $sql = " select tbl_strategy.id as stid,tbl_strategy.description as description,"
                . " tbl_strategy.duedate,tbl_strategy.responsible_person,tbl_strategy.signedOffBy "
                . " from tbl_strategy where tbl_strategy.is_active = 1 and tbl_strategy.source_id =  ".$db->sqs($id)." and tbl_strategy.branch_id = ".$db->sqs($_SESSION['branch_id']);        
        return $db->getAll($sql);
    }
    public static function addStrategy($data = array())
    {
         global $db;
            $status = false;
            $sql = "insert into tbl_strategy"
                . "(branch_id,company_id,source_id,source_type,duedate,responsible_person,description,is_active,created_by)"                
                . "VALUES(
                ".$db->sqs($_SESSION['branch_id']).","
                 .$db->sqs($_SESSION['company_id']).","
                .$db->sqs($data['source_id']).","
                .$db->sqs($data['source_type']).","
                .$db->sqs($data['duedate']).","
                .$db->sqs($data['responsible_person']).","
                .$db->sqs($data['description']).","
                 ."1,"
                .$db->sqs($_SESSION['user_id']).")";        
            $result = $db->query($sql);
         if($result == true)
         {
             $status = true;            
         }         
         return $result;
    }
    public static function editStrategy($data = array())
    {        
        global $db;
        $status = false;
        $sql = "update tbl_strategy set duedate = '{$db->sqs($data['duedate'])}',"
        . " responsible_person = '{$db->sqs($data['responsible_person'])}',"
        . " description = '{$db->sqs($data['description'])}'"
        . " where branch_id = {$db->sqs($_SESSION['branch_id'])}"
        . " and company_id =  {$db->sqs($_SESSION['company_id'])} and id = ".$db->sqs($data['id']);
        $result = $db->query($sql);
        if($result)
        {
            $status = true;
        }
        return $status;
    }    
}
