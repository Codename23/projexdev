<?php
/**
 * Controller class containing methods to process all folder actions
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class folders extends controller{


    /**
     * Method to add a new folder
     */
    public static function addNewFolder(){

        global $objFolders;
        $data = array();


        if(isset($_POST['folderName'])) {

            $companyName = isset($_SESSION['defaultCompany']) ? strtolower($_SESSION['defaultCompany']) : null;

            $folderData = array('folderName' => $_POST['folderName'],
                'parentId' =>intval($_POST['parentId']),
                "companyId"=>$_SESSION['company_id'],
                "userId"=>$_SESSION['user_id']);


            $folderId = $objFolders->addNewFolder($folderData);
            if(!is_null($folderId)) {

                $directoryData = array("folderId" =>$folderId,
                                        "companyName" =>$companyName,
                                        "companyId"=>$_SESSION['company_id'],
                                        "userId"=>$_SESSION['user_id']);

                $isFolderCreated = $objFolders->createFolderDirectory($directoryData);

                $data['type'] = 'success';
                $data['message'] = 'Folder Successfully added';
                controller::nextPage('viewAllDocumentsRecords','documents', $data);
            }else{

                $data['type'] = 'error';
                $data['message'] = 'Failed to add  new folder';
                controller::nextPage('viewAllDocumentsRecords','documents', $data);
            }
        }else{
            controller::nextPage('viewAllDocumentsRecords','documents', $data);

        }

    }


    /**
     * Method to update folders
     */
    public static function editSelectedFolder(){

        global $objFolders;
        $data = array();


        if(isset($_POST['folderName'])) {


            $folderData = array('folderName' => $_POST['folderName'],
                'parentId' =>intval($_POST['parentId']),
                "companyId"=>$_SESSION['company_id'],
                "folderId"=>$_POST['folderId'],
                "userId"=>$_SESSION['user_id']);

            $isUpdates = $objFolders->editFolder($folderData);

            if($isUpdates) {
                $data['type'] = 'success';
                $data['message'] = 'Folder Successfully Updated';
                controller::nextPage('viewAllDocumentsRecords','documents', $data);
            }else{

                $data['type'] = 'error';
                $data['message'] = 'Failed update selected folder data';
                controller::nextPage('viewAllDocumentsRecords','documents', $data);
            }
        }else{
            controller::nextPage('viewAllDocumentsRecords','documents', $data);

        }

    }




}

