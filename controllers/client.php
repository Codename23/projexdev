<?php
/**
 * Controller class containing methods to process all client actions
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class client extends controller{


    /**
     * Method to view all clients
     */
    public static function viewAllClients(){

        $data = array();
        global $objClient;
        global $objTemplate;

        $branchId = $_SESSION['branch_id'];

        $data['companyClients']= $objClient->getAllCompanyClients($branchId);
        $objTemplate->setVariables('title','client Info');
        $objTemplate->setView('templates/client','index_tpl',$data);
    }


    /**
     * Method to add a new client
     */
    public static function addClient(){

        global $objClient;
        global $objTemplate;
        
        if(!empty($_POST) && isset($_SESSION['branch_id'])){

            $clientData = array();

            $clientData['companyIndividual'] = $_POST['companyIndividual'];
            
            if($_POST['companyIndividual'] == 'company') {
                
                $clientData['companyType'] = 1;
                $clientData['companyName'] = $_POST['companyName'];
                $clientData['ptyNumber'] = $_POST['ptyNumber'];
                $clientData['vatNumber'] = $_POST['vatNumber'];
                $clientData['officeNumber'] = $_POST['officeNumber'];
                $clientData['contactPerson'] = $_POST['contactPerson'];
                $clientData['contactNumber'] = $_POST['contactNumber'];
                $clientData['email'] = $_POST['email'];
                $clientData['website'] = $_POST['website'];
                $clientData['headOfficeDetails'] = $_POST['headOfficeDetails'];
                $clientData['headOfficeTown'] = $_POST['headOfficeTown'];
                $clientData['headOfficeProvince'] = $_POST['headOfficeProvince'];

            }else{

                $clientData['companyType'] = 2;
                $clientData['individualName'] =  $_POST['individualName'];
                $clientData['individualSurname'] = $_POST['individualSurname'];
                $clientData['individualGender'] = $_POST['individualGender'];
                $clientData['individualEmail']= $_POST['individualEmail'];
                $clientData['individualContactNumber'] = $_POST['individualContactNumber'];
                $clientData['individualAddress'] = $_POST['individualAddress'];
                $clientData['town'] = $_POST['town'];
                $clientData['province'] = $_POST['province'];

                $clientData['reasonForListing'] = $_POST['reasonForListing'];
                $clientData['uploadDocuments'] = $_POST['uploadDocuments'];

            }

            $clientData['branchId'] =  $_SESSION['branch_id'];
            $clientData['createdBy'] = $_SESSION['user_id'];
            $clientData['modifiedBy']= $_SESSION['user_id'];

            $addClient = $objClient->addClient($clientData);

            if($addClient){

                $data['type'] = 'success';
                $data['message'] = 'New customer added successfully';
                if($_POST['formType'] != 'ajaxForm'){
                   controller::nextPage('viewAllClients','client', $data); 
                }
                
            }else{

                $data['type'] = 'success';
                $data['message'] = 'New customer added successfully';
                if($_POST['formType'] != 'ajaxForm'){
                    controller::nextPage('viewAllClients','client', $data);
                }

            }

        }else{

            $data = array();

            $data['companyTypes'] = $objClient->getAllCompanyTypes();
            $data['gender'] = $objClient->getClientContactGender();

            $objTemplate->setVariables('title','Add client');
            $objTemplate->setView('templates/client','add_client_tpl',$data);
        }

    }


    /**
     * Method to view client information
     */
    public static function viewClientInfo(){

        global $objClient;
        global $objTemplate;

        $data = array();
        $id = intval($_GET['id']);
        $data['clientInfo'] = $objClient->getClientInfo($id);
        $data['gender'] = $objClient->getClientContactGender();

        $objTemplate->setVariables('title', 'Customer');

        if($data['clientInfo']['client_type'] == 1){

            $objTemplate->setView('templates/client', 'company_details_tpl', $data);
        }else{
            $objTemplate->setView('templates/client', 'individual_details_tpl', $data);
        }

    }

    /**
     * Method to view client information
     */
    public static function editClientInfo(){

        global $objClient;

        if(!empty($_POST)) {
            $clientData = array();
            if ($_POST['companyType'] == 1) {

                $clientData['companyType'] = $_POST['companyType'];
                $clientData['companyName'] = $_POST['companyName'];
                $clientData['ptyNumber'] = $_POST['ptyNumber'];
                $clientData['vatNumber'] = $_POST['vatNumber'];
                $clientData['officeNumber'] = $_POST['officeNumber'];
                $clientData['contactPerson'] = $_POST['contactPerson'];
                $clientData['contactNumber'] = $_POST['contactNumber'];
                $clientData['email'] = $_POST['email'];
                $clientData['website'] = $_POST['website'];
                $clientData['headOfficeDetails'] = $_POST['headOfficeDetails'];
                $clientData['headOfficeTown'] = $_POST['headOfficeTown'];
                $clientData['headOfficeProvince'] = $_POST['headOfficeProvince'];

            }else{

                $clientData['companyType'] = $_POST['companyType'];
                $clientData['individualName'] =  $_POST['individualName'];
                $clientData['individualSurname'] = $_POST['individualSurname'];
                $clientData['individualGender'] = $_POST['individualGender'];
                $clientData['individualEmail']= $_POST['individualEmail'];
                $clientData['individualContactNumber'] = $_POST['individualContactNumber'];
                $clientData['individualAddress'] = $_POST['individualAddress'];
                $clientData['town'] = $_POST['town'];
                $clientData['province'] = $_POST['province'];

                $clientData['reasonForListing'] = $_POST['reasonForListing'];
                $clientData['uploadDocuments'] = $_POST['uploadDocuments'];

            }
            $clientData['clientId']= $_POST['companyId'];
            $clientData['modifiedBy']= $_SESSION['user_id'];

            $editClient = $objClient->editClient($clientData);

            if($editClient){

                $data['type'] = 'success';
                $data['message'] = 'Customer updated successfully';
                $data['id'] = $_POST['companyId'];
                controller::nextPage('viewClientInfo','client', $data);


            }else{
                $data['type'] = 'error';
                $data['message'] = 'Could not updated selected customer';
                $data['id'] = $_POST['companyId'];
                controller::nextPage('viewClientInfo','client', $data);
            }
        }
    }

    /**
     * Method to remove selected client
     */
    public static function deleteClient(){

        global $objClient;

        $id = intval($_GET['id']);
        $deleteClient = $objClient->deleteClient($id);

        if($deleteClient){

            $data['type'] = 'success';
            $data['message'] = 'Customer deleted successfully';
            controller::nextPage('viewAllClients','client', $data=[]);


        }else{
            $data['type'] = 'error';
            $data['message'] = 'Could not delete selected customer';
            controller::nextPage('viewAllClients','client', $data=[]);
        }

    }

    /**
     * Method to request for a new quotation
     */
    public static function requestQuote(){

        global $objTemplate;
        if(!empty($_POST)){

        }else{

            $data = array();
            $objTemplate->setVariables('title','Quotation');
            $objTemplate->setView('templates/client','request_quote_tpl',$data);

        }

    }
    /**
     * Method to search client details
     */
    public static function searchClient(){
        global $objClient;
        
        $searchClientDetsArr = $objClient->searchClient($_POST['customerCompanyName'], $_POST['customerNameSurname']);
        echo json_encode($searchClientDetsArr); 
        
    }
    

}