<?php
/**
 * Controller class containing methods to process all provider actions
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class provider extends controller{




    public static function viewAllProviders(){

        $data = array();
        global $objProvider;
        global $objTemplate;

        $branchId = $_SESSION['branch_id'];
        $data['serviceProviders']= $objProvider->getAllProviders($branchId);
        $objTemplate->setVariables('title','Supplier Info');
        $objTemplate->setView('templates/provider','index_tpl',$data);
    }


    /**
     * Method to add a new service provider
     */
    public static function addProvider(){

        global $objService;
        global $objAudit;
        global $objProvider;
        global $objTemplate;

        $data = array();
        if(!empty($_POST) && isset($_SESSION['branch_id'])){

            $providerData = array();
            $providerData['companyName'] = $_POST['companyName'];
            $providerData['contactPerson'] = $_POST['contactPerson'];
            $providerData['contactNumber'] = $_POST['contactNumber'];
            $providerData['email'] = $_POST['email'];
            $providerData['serviceType'] = $_POST['serviceType'];

            $providerData['branchId'] = $_SESSION['branch_id'];
            
            if($_POST['formType'] != 'ajaxForm' ){ 
                $providerData['vatNumber'] = $_POST['vatNumber'];
                $providerData['officeNumber'] = $_POST['officeNumber'];
                $providerData['ptyNumber'] = $_POST['ptyNumber'];
                $providerData['website'] = $_POST['website'];
                $providerData['beeCertificate'] = $_POST['beeCertificate'];
                $providerData['headOfficeDetails'] = $_POST['headOfficeDetails'];
                $providerData['headOfficeTown'] = $_POST['headOfficeTown'];
                $providerData['headOfficeProvince'] = $_POST['headOfficeProvince'];
                $providerData['providerDetails'] = $_POST['providerDetails'];
                $providerData['serviceFrequency'] = $_POST['serviceFrequency'];
                $providerData['uploadDocuments'] = $_POST['uploadDocuments'];
                $providerData['complianceConstruction'] = $_POST['complianceConstruction'];
                $providerData['complianceHighRisk'] = $_POST['complianceHighRisk'];
                $providerData['complianceDescription'] = $_POST['complianceDescription'];
                $providerData['applicableServiceProvider'] = $_POST['applicableServiceProvider'];
                $providerData['auditType'] = $_POST['auditType'];
                $providerData['auditFrequency'] = $_POST['auditFrequency'];
                $providerData['nextAuditMonth'] = $_POST['nextAuditMonth'];
                $providerData['createdBy'] = $_SESSION['user_id'];
                $providerData['modifiedBy']= $_SESSION['user_id'];
            }else{
                $providerData['vatNumber'] = null;
                $providerData['officeNumber'] = null;
                $providerData['ptyNumber'] = null;
                $providerData['website'] = null;
                $providerData['beeCertificate'] = null;
                $providerData['headOfficeDetails'] = null;
                $providerData['headOfficeTown'] = null;
                $providerData['headOfficeProvince'] = null;
                $providerData['providerDetails'] = null;
                $providerData['serviceFrequency'] = null;
                $providerData['uploadDocuments'] = null;
                $providerData['complianceConstruction'] = null;
                $providerData['complianceHighRisk'] = null;
                $providerData['complianceDescription'] = null;
                $providerData['applicableServiceProvider'] = null;
                $providerData['auditType'] = null;
                $providerData['auditFrequency'] = null;
                $providerData['nextAuditMonth'] = null;
              
            }
            $providerData['createdBy'] = $_SESSION['user_id'];
            $providerData['modifiedBy']= $_SESSION['user_id'];
            $addProvider = $objProvider->addProvider($providerData);

            if($addProvider){

                $data['type'] = 'success';
                $data['message'] = 'New service provider added successfully';
               
                if($_POST['formType'] != 'ajaxForm'){
                    controller::nextPage('viewAllProviders','provider', $data);
                }

            }else{

                $data['type'] = 'error';
                $data['message'] = 'Failed to add  service provider';
                
                if($_POST['formType'] != 'ajaxForm'){
                    controller::nextPage('viewAllProviders','provider', $data);
                }
            }

        }elseif(!isset($_GET['is_warning'])){
                $data['is_warning'] = false;
                if($_SESSION['branch_id'] == null){
                  $data['type'] = 'warning';
                  $data['message'] = 'Please select branch and enter required fields';
                  $data['is_warning'] = true;
                }
             controller::nextPage('addProvider','provider', $data);      
       
        }else{
            
            //Get service types
            $data['serviceTypes'] = $objService->getAllServiceTypes();//@TODO: get service types depending on company preferences
            $data['auditTypes'] = $objAudit->getAllAuditTypes();//@TODO: get audits types depending on company preferences
            $objTemplate->setVariables('title','Add provider');
            $objTemplate->setView('templates/provider','add_provider_tpl',$data);
        }
    }

    /**
     * Method to edit provider info
     */
    public static function editProviderInfo(){

        global $objProvider;

        if(!empty($_POST)){

            $providerData = array();

            $providerData['companyName'] = $_POST['companyName'];
            $providerData['ptyNumber'] = $_POST['ptyNumber'];
            $providerData['vatNumber'] = $_POST['vatNumber'];
            $providerData['officeNumber'] = $_POST['officeNumber'];
            $providerData['contactPerson'] = $_POST['contactPerson'];
            $providerData['contactNumber'] = $_POST['contactNumber'];
            $providerData['email'] = $_POST['email'];
            $providerData['website'] = $_POST['website'];
            $providerData['headOfficeDetails'] = $_POST['headOfficeDetails'];
            $providerData['headOfficeTown'] = $_POST['headOfficeTown'];
            $providerData['headOfficeProvince'] = $_POST['headOfficeProvince'];
            $providerData['serviceType'] = $_POST['serviceType'];
            $providerData['providerDetails'] = $_POST['providerDetails'];
            $providerData['serviceFrequency'] = $_POST['serviceFrequency'];
            $providerData['uploadDocuments'] = $_POST['uploadDocuments'];
            $providerData['complianceConstruction'] = $_POST['complianceConstruction'];
            $providerData['complianceHighRisk'] = $_POST['complianceHighRisk'];
            $providerData['complianceDescription'] = $_POST['complianceDescription'];
            $providerData['applicableServiceProvider'] = $_POST['applicableServiceProvider'];
            $providerData['auditType'] = $_POST['auditType'];
            $providerData['auditFrequency'] = $_POST['auditFrequency'];
            $providerData['nextAuditMonth'] = $_POST['nextAuditMonth'];
            $providerData['modifiedBy']= $_SESSION['user_id'];
            $providerData['providerId']= $_POST['providerId'];


            $editProvider =  $objProvider->editProvider($providerData);

            if($editProvider){

                $data['type'] = 'success';
                $data['message'] = 'Service provider updated successfully';
                $data['id'] = $_POST['providerId'];
                controller::nextPage('viewProviderInfo','provider', $data);

            }else{
                $data['type'] = 'error';
                $data['message'] = 'Failed to update service provider';
                $data['id'] = $_POST['providerId'];
                controller::nextPage('viewProviderInfo','provider', $data);


            }
        }
    }
    /**
     * Method to view service provider info
     */
    public static function viewProviderInfo(){

        global $objProvider;
        global $objService;
        global $objTemplate;

        $data = array();
        $id = intval($_GET['id']);
        $data['providerInfo'] = $objProvider->getProviderInfo($id);
        $data['serviceTypes'] = $objService->getAllServiceTypes();//@TODO: get service types depending on company preferences

        $objTemplate->setVariables('title', 'Service Provider');
        $objTemplate->setView('templates/provider', 'provider_details_tpl', $data);
    }

    /**
     * Method to delete a provider
     */
    public static function deleteProvider(){

        global $objProvider;

        $id = intval($_GET['id']);
        $deleteProvider = $objProvider->deleteProvider($id);

        if($deleteProvider){

            $data['type'] = 'success';
            $data['message'] = 'Service provider deleted successfully';
            controller::nextPage('viewAllProviders','provider', $data);


        }else{

            $data['type'] = 'error';
            $data['message'] = 'Failed to delete service provider ';
            controller::nextPage('viewAllProviders','provider', $data);

        }

    }

    /**
     * Method to request a new quote
     */
    public static function requestQuote(){
        global $objTemplate;

        if(!empty($_POST)){

        }else{

            $data = array();
            $objTemplate->setVariables('title','Quotation');
            $objTemplate->setView('templates/provider','request_quote_tpl',$data);

        }

    }
    /**
    * Method to search client details
    */
    public static function searchProvider(){
        global $objProvider;
        
        $ProviderDetsArr = $objProvider->searchProvider($_POST['serviceproviderCompanyName'], $_POST['serviceproviderNameSurname']);
        echo json_encode($ProviderDetsArr); 
        
    }

}