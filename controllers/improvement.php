<?php


class improvement extends controller
{    
    public static function getActiveImprovements()
    {
        global $objTemplate;
        global $objSheqTeam;
        global $objEmployee;
        global $objImprovements;
        $data = array();
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();    
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allImprovements'] = $objImprovements->viewImprovements();
        $objTemplate->setVariables('title','Improvement');
        $objTemplate->setView('templates/improvement','all_improvement',$data);
    }
    public static function getRequestedImprovements()
    {
        global $objTemplate;
        global $objSheqTeam;
        global $objEmployee;
        global $objImprovements;
        $data = array();
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();    
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allImprovements'] = $objImprovements->viewImprovements();
        $objTemplate->setVariables('title','Requested Improvement');
        $objTemplate->setView('templates/improvement','request_improvement',$data);
    }
    public static function getRecommendedImprovement()
    {
        global $objTemplate;
        global $objSheqTeam;
        $data = array();
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();   
        $objTemplate->setVariables('title','Improvement');
        $objTemplate->setView('templates/improvement/recommended/','recommended_improvement',$data);

    }
    public static function getHistoryImprovements()
    {
        global $objTemplate;
        global $objSheqTeam;
        $data = array();
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();   
        $objTemplate->setVariables('title','Improvement');
        $objTemplate->setView('templates/improvement','history',$data);

    }
    public static function displayStrategy()
    {
        global $objTemplate;
        global $objSheqTeam;
        global $objEmployee;
        global $objStrategy;
        $data = array();
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();   
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
       // $id = (int)$_GET["id"];
       // $data["allStrategies"] = $objStrategy->viewStrategies($id);       
        $id = 0;
        if(isset($_GET['id']))
        {
            $id = (int)$_GET['id'];
        }
        if(isset($_POST['txtHidden']))
        {
            $id = (int)$_POST['txtHidden'];
        }
        $data['allStrategy'] = $objStrategy->viewStrategies($id);
        $objTemplate->setVariables('title','Improvement');
        $objTemplate->setView('templates/improvement/strategy/','view_strategy',$data);
    }
    public static function signedOffImprovement()
    {
      global  $objSignedOff;
      if(isset($_POST['improvementSignOff']))
      {
        if(isset($_POST['txtHiddenSignedOff']))
        {          
           $data = array();
           $data["signedOffAction"] = $_POST['action_taken'];
           $data['signedOffLevel'] = "Master";
           $data["signedOffType"] = 5;
           $data['source_id'] = $_POST['txtHiddenSignedOff'];
           $data['SignedOffDate'] = $_POST["dateCompleted"];
           $data["table"] = 'tbl_strategy';
           $result = $objSignedOff->hasSignedOffDataWithDate($data);
        }
      }
      //get the history
     self::displayStrategy();
    }
    public static function masterSignOff()
    {
      global  $objSignedOff;
      if(isset($_POST['improvementSignOff']))
      {
        if(isset($_POST['txtHiddenSignedOff']))
        {          
           $data = array();
           $data["signedOffAction"] = $_POST['action_taken'];
           $data['signedOffLevel'] = "Master";
           $data["signedOffType"] = 5;
           $data['source_id'] = $_POST['txtHiddenSignedOff'];
           $data['SignedOffDate'] = $_POST["dateCompleted"];
           $data["table"] = 'tbl_strategy';
           $result = $objSignedOff->hasSignedOffDataWithDate($data);
        }
      }
      //get the history
     self::displayStrategy();
    }
    public static function submitStrategy()
    {
        global $objStrategy;
        if(isset($_POST['btnSubmitStrategy']))
        {
            if(isset($_POST['txtHidden']))
            {                
                $strategyData = array();
                $strategyData["source_id"] = $_POST['txtHidden'];
                $strategyData["responsible_person"] = $_POST['strategy_person'];
                $strategyData["duedate"] = $_POST['dueDate'];
                 $strategyData["source_type"] = 5;
                $strategyData["description"] = $_POST['strategyDescription'];                
                if($objStrategy->addStrategy($strategyData))
                {
                    
                }
            }
        }
        self::displayStrategy();
    } 
    public static function addImprovement()
    {
        if(isset($_POST['addImprovementBtn']))
        {
        echo "Hello World";
        /*global $objImprovements;
        global $objTemplate;
        global $objNonConformance;
        $improvementData = array();
        $id = 0;
        $why = 0;
        /*if(isset($_POST['noc_id']))
        {
            $id = (int)$_POST['noc_id'];
        }
        else 
        {
            if(isset($_GET["ncid"]))
               {
                   $id = (int)$_GET["ncid"];
               }        
               if(isset($_GET["why_id"]))
               {
                   $why = (int)$_GET["why_id"];
               }
        }
        
        $improvementData["action_link_id"] = $id; 
        $improvementData["why_link_id"] = $why;
        $improvementData["group_id"] = $_POST['improve_group']; 
        $improvementData["improvement_type"] = $_POST['improvementType'];
        $improvementData["improvement_description"] = $_POST['improvementDescription'];
        $improvementData["responsible_person"] = $_POST['responsible_person'];
        $improvementData["managerial_approval"] = $_POST['managerialApproval'];
        $improvementData["sheqf_approval"] = $_POST['sheqfApproval'];
        $result = $objImprovements->addImprovement($improvementData);
        self::getActiveImprovements();
        //$action = "";
        /*$redirectUrl = false;
        if(isset($_POST['action']))
        {
            $action = $_POST['action'];
            switch($action)
            {
                case 'Non Conformance':
                    $improvementData["requested"] = 1;
                    $redirectUrl = true;
                    break;
                case 'Investigation':
                    $improvementData["requested"] = 0;
                    $redirectUrl = true;
                    break;
                default:
                    $improvementData["requested"] = 0;
                    break;
            }
        }
        $improvementData["action_type"] = $action;        
        $result = $objImprovements->addImprovement($improvementData);
        if($redirectUrl)
        {
            
            self::getRequestedImprovements();
        }
        else{
            self::getActiveImprovements();
        }*/
        }
    }
    public static function viewSingleInvestigation()
    {
        global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        global  $objNonConformance;             
        $id = 0;
        if(isset($_GET["ncid"]))
        {
            $id = (int)$_GET["ncid"];
        }
        //allPersons
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allNonConformance'] = $objNonConformance->viewInvestigationDetailsById($id);
        $data['allFiveWhyDetails'] = $objInvestigations->getFiveWhy($id);
         if($objInvestigations->checkFiveWhyExist($id))
         {
             $objInvestigations->hasFive = true;
         }
        $objTemplate->setVariables('title','Non Conformance');
        $objTemplate->setView('templates/investigations','improvement_investigation_why_investigation',$data);
    }    
}

