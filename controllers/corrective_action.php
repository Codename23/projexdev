<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of corrective_actions
 *
 * @author Innovatorshill
 */

class corrective_action extends controller 
{
    //put your code here
    public static function viewAllCorrectiveActions()
    {
     //view all corrective actions
        global $objTemplate; 
        $data = array();           
        global $objCorrectiveAction;
        $data['allCorrectiveActions'] = $objCorrectiveAction->viewCorrectiveActions();
        $objTemplate->setVariables('title','Corrective Actions');
        $objTemplate->setView('templates/corrective_actions','index_tpl',$data);
    }
    
    //put your code here
    public static function viewAllHistoryCorrectiveActions()
    {
     //view all corrective actions
        global $objTemplate; 
        $data = array();           
        global $objCorrectiveAction;
        $data['allCorrectiveActions'] = $objCorrectiveAction->viewCorrectiveActions(true);
        $objTemplate->setVariables('title','Corrective Actions');
        $objTemplate->setView('templates/corrective_actions','index_tpl',$data);
    }
    public static function searchCorrectiveAction()
    {
        //search corrective actions
    }
    public static function addCorrectiveActionPost()
    {
        if(isset($_POST['btnAddCorrectiveAction']))
        {            
            $source = null;
            global $objCorrectiveAction;
            global $objActions;
            
            $objCorrectiveActionData = array();          
            $objCorrectiveActionData['cor_department'] = $_POST['cor_department'];    
            $objCorrectiveActionData['type_field'] = null;
            $objCorrectiveActionData['priority'] = $_POST['priority'];
            $objCorrectiveActionData['system_element'] = $_POST['systemElement'];
            $objCorrectiveActionData['nonConformanceDescription'] = $_POST['nonConformanceDescription'];
            $objCorrectiveActionData['correctiveActionDescription'] = $_POST['correctiveActionDescription'];
            $objCorrectiveActionData['cor_person_responsible'] = $_POST['cor_person_responsible'];
            $objCorrectiveActionData['recommendedImprovements'] = $_POST['recommendedImprovements'];
            $objCorrectiveActionData['cor_person_responsible'] = $_POST['cor_person_responsible']; 
            $objCorrectiveActionData['notify_duty_manager'] = $_POST['dep_manager'];
            $objCorrectiveActionData['notify_person'] = $_POST['notify_corrective_person'];
            $objCorrectiveActionData['master_sign_off'] = $_POST['master_sign_off'];
            $objCorrectiveActionData['correctiveActionDueDate'] = $_POST['correctiveActionDueDate']; 
            //additional information goes here
            //
           
            $objCorrectiveActionData['source_id'] = null;
            $objCorrectiveActionData['source_desc'] = null;
            $objCorrectiveActionData['why_id'] = null;
            $objCorrectiveActionData['resource_id'] = null;
            
            if(isset($_POST['cor_resource']))
            {
                $objCorrectiveActionData['resource_id'] = $_POST['cor_resource'];
            }
            
            //source incident, non conformance , investigation
            if(isset($_POST['ncid']))
            {
                $objCorrectiveActionData['source_id'] = (int)$_POST['ncid'];
                $objCorrectiveActionData['source_desc'] = "noc";
                $objCorrectiveActionData['action'] = "Non-Conformance";
            }            
            if(isset($_POST['why_id']))
            {
                $objCorrectiveActionData['why_id'] = (int)$_POST['why_id'];                
            }
            
            //approval information goes here
            $objCorrectiveActionData['needApproval'] = $_POST['needApproval'];
            $objCorrectiveActionData['status'] = '';
            $result = $objCorrectiveAction->addCorrectiveActionModel($objCorrectiveActionData);
            if($result)
            {
                if(isset($_POST['ncid']))
                {
                    //store action non-conformance
                    $objCorrectiveActionData["source_id"] = (int)$_POST['ncid'];
                    $objCorrectiveActionData['source_type'] = "Non-Conformance";
                    $objCorrectiveActionData['action_taken'] = "Corrective Action";
                    $objCorrectiveActionData['action'] = $_POST["correctiveActionDescription"];
                    $objCorrectiveActionData['action_taken_id'] = $objCorrectiveAction->corrective_action_id;
                    
                    if($objActions->hasCreatedAction($objCorrectiveActionData))
                    {
                        self::viewAllCorrectiveActions();
                    }
                }
                else
                {
                    if(isset($_POST['why_id']))
                    {
                      //go to the previous page
                        /*?>
                       <script>
                        window.location = "<?php echo $_SERVER["HTTP_REFERER"];?>";
                       </script>*/  
                    }
                }                
            }
            else
            {
               self::viewAllCorrectiveActions();
            }            
        }
    }


    public static function addCorrectiveAction()
    {
        //add corective actions
         //view all corrective actions
        $id = 0;
        if(isset($_POST['nt_id']))
        {
            $id = (int)$_POST['nt_id'];
        }
        global $objCorrectiveAction;
        global $objDepartment;
        global $objTemplate; 
        global $objNonConformance;
        global $objEmployee;
        global $objAssets;
        
        $data = array();           
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allResources'] = $objAssets->getAllAsset();
        $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        if(isset($_GET["ncid"]))
        {
            $id = (int)$_GET["ncid"];
        }
        $data["details"] = $objNonConformance->getNonConformanceDetailsById($id);
        $objTemplate->setVariables('title','Add Corrective Actions');
        $objTemplate->setView('templates/corrective_actions','add_corrective_action_tpl',$data);
    }
    public static function showCorrectiveActionHistory()
    {
        //view all corrective actions
        global $objTemplate; 
        $data = array();           
        global $objCorrectiveAction;
        $data['allCorrectiveActions'] = $objCorrectiveAction->viewCorrectiveActions(true);
        $objTemplate->setVariables('title','Corrective Actions');
        $objTemplate->setView('templates/corrective_actions','corrective_action_history',$data);  
    }
    public static function correctiveActionSignOffPost()
    {
      global  $objSignedOff;
      if(isset($_POST['btnMasterSignOff']) && isset($_POST['caid']))
      {          
         $data = array();
         $data["signedOffAction"] = $_POST["masterSignoffActionTaken"];
         $data['signedOffLevel'] = "Master";
         $data["signedOffType"] = 4;
         $data['source_id'] = $_POST['caid'];
         $data["table"] = 'tbl_corrective_actions';
         $result = $objSignedOff->hasSignedOffData($data);
      }     
      //get the history
      self::viewAllHistoryCorrectiveActions(false);
    }
    public static function correctiveActionSignOff()
    {
        global $objTemplate;
        $objTemplate->setVariables('title','Sign Off Corrective Action');
        $objTemplate->setView('templates/corrective_actions','corrective_action_signoff');
    }
    public static function editCorrectiveAction()
    {
        global $objTemplate;
        $objTemplate->setVariables('title','Sign Off Corrective Action');
        $objTemplate->setView('templates/corrective_actions','edit_corrective_action_tpl');
    }
    public static function viewCorrectiveActionDetails()
    {
        global $objTemplate;
        $objTemplate->setVariables('title','Sign Off Corrective Action');
        $objTemplate->setView('templates/corrective_actions','corrective_action_details');
    }
    public static function editCorrectiveActionById()
    {
        //edit corrective action
    }
}
