<?php
/**
 * Controller class containing methods to process all supplier actions
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class action extends controller{
    
    public static function getMailingList()
    {
        if(isset($_GET["request"]))
        {
            global $objActions;       
            $data = array();
             $total = 0;
             
            if($_GET["request"] == "inbox")
            {
                $table = "<table class='table table-inbox table-hover'>
                              <thead>                                
                                <th></th>
                                <th>Request</th>
                                <th>From</th>
                                <th>Subject</th>
                                <th>Status</th>
                                <th>Date</th>                                
                              </thead>
                            <tbody>";
                            
                 $request = $_GET["request"];
                 $row = "";
                 $data = $objActions->getMailingList($request);
                 //foreach    
                
                if($data): foreach($data as $list):   
                    $total++;
                       $row =  '<tr class="">
                                          <td class="view-message"><span class="glyphicon glyphicon-log-out"></span></td>
                                         <td>'.$list["source_name"].'</td>
                                         <td class="view-message">'.$list["sender"].'</td>
                                         <td class="view-message ">'.$list["subject"].'</td>
                                         <td class="view-message  inbox-small-cells"><i class="fa fa-circle text-danger"></i> '.$list["status"].'</td>
                                         <td class="view-message  text-left">'.$list["date_created"].'</td>
                                     </tr>';
                  endforeach;endif;  
                $complete =  $table.$row.'</tbody></table>';   
               $return_notification = array("totals"=>$total,"mail_block"=>$complete);
                echo json_encode($return_notification);
        }
            //all requests
             if($_GET["request"] == "outbox")
            {
                $table = "<table class='table table-inbox table-hover'>
                              <thead>                                
                                <th></th>
                                <th>Request</th>
                                <th>To</th>
                                <th>Subject</th>
                                <th>Status</th>
                                <th>Date</th>                                
                              </thead>
                            <tbody>";
                            
                 $request = $_GET["request"];
                 $row = "";
                 $data = $objActions->getMailingList($request);
                 //foreach    
                if($data): foreach($data as $list): 
                    $total++;
                       $row =  '<tr class="">
                                          <td class="view-message"><span class="glyphicon glyphicon-log-out"></span></td>
                                         <td>'.$list["source_name"].'</td>
                                         <td class="view-message">'.$list["recipient"].'</td>
                                         <td class="view-message ">'.$list["subject"].'</td>
                                         <td class="view-message  inbox-small-cells"><i class="fa fa-circle text-danger"></i> '.$list["status"].'</td>
                                         <td class="view-message  text-left">'.$list["date_created"].'</td>
                                     </tr>';
                   endforeach;endif;          
                $complete =  $table.$row.'</tbody></table>';
                $return_notification = array("totals"=>$total,"mail_block"=>$complete);
                echo json_encode($return_notification);
        }
      }
    }

}