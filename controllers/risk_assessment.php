<?php
/**
 * Controller class containing methods to process all supplier actions
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class risk_assessment extends controller{




    public static function viewRiskAssessmentDashboard(){
        global $objRiskAssessment;
        global $objTemplate;
        global $objEmployee;
        global $objDepartment;
        global $objAssets;
        global $objOccupations;
        global $objSheqTeam;
        $data = array();
        $data['allRequestedUpcoming']= $objRiskAssessment->getRequeqestedRAUpcoming($_SESSION['company_id']);
        $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
        $data['allResources'] = $objAssets->getAllAsset();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['companyOccupations'] = $objOccupations->getCompanyOccupations($_SESSION['company_id']);
        $data['risk_types'] = $objRiskAssessment->getAllRiskTypes();
        $data['risk_templates'] = $objRiskAssessment->getAllRiskTemplates();
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/risk_assessment','new_risk_assessment_tpl',$data);
        
    }
    public static function get_list_departments()
    {
        global $objDepartment;
        $departments = array();
        $data2 = $objDepartment->getAllDepartmentsByListId($_POST['departments']);
        $counter =  0;
        foreach($data2 as $asset)
        {
            $counter++;
            $departments[] = array(
                "status"=>200,
                "num"=>$counter,
                "department"=>$asset["department_name"]);          
        }
        echo json_encode($departments);
    }
    public static function get_list_links()
    {
        global $objAssets;
        global $objOccupations;
        $links_array = array();
        $data2 = $objAssets->getAllAssetsInSource(null,$_POST['res']);
        $data3 = $objOccupations->getCompanyOccupationsInSource($_POST["occ"]);
        $counter = 0;
        foreach($data2 as $asset)
        {
            $counter++;
            $links_array[] = array(
                "status"=>200,
                "num"=>$counter,
                "link"=>"Resource",
                "description"=>$asset["as_name"]);          
        }
        foreach($data3 as $occ)
        {
             $counter++;
            $links_array[] = array(
                "status"=>200,
                "num"=>$counter,
                "link"=>"Occupation",
                "description"=>$occ["name"]);        
        }               
        echo json_encode($links_array);
    }
    
    
    //get list of controls
    public static function get_list_controls_list()
    {
        global $objRiskAssessment;
        $links_array = array();
        $data2 = $objRiskAssessment->listRiskControls($_POST["id"]);        
        $counter = 0;
        foreach($data2 as $r)
        {
            $ass = $r["is_control_in_effect"];
            if($ass == 1)
            {
                $ass = "Yes";
            }
            else
            {
                $ass = "<a href='#view_risk_notes-modal' data-toggle='modal' data-target='#view_risk_notes-modal' onclick='function(){"
                        . " $('.risk_notes_section').html({$r["risk_notes"]})"
                        . "}'>View Notes</a>";
            }
            $counter++;
            $links_array[] = array(
                "status"=>$counter,
                "riskNo"=>$r["risk_no"],
                "photo"=>$r["risk_photo"],
                "hazard_type"=>$r["hazard"],
                "hazard_description"=>$r["hazard_description"],
                "risk_type"=>$r["risk"],
                "risk_description"=>$r["risk_description"],
                "score"=>$r["pscore"],
                "control"=>$r["control_type"],
                "control_description"=>$r["control_description"],
                "control_in_place"=>$ass,
                "new_score"=>$r["new_score"],                
               "action_taken_link"=>"<a href='#' assessment_id='{$r["risk_ass_id"]}'>View</a>");          
        }        
       echo json_encode($links_array);
    }
    public static function post_sheqf_notes()
    {
        if(isset($_POST['sheqfNotesBtn']))
        {
            global $objRiskAssessment;
            $data = array();
            $data["notes"] = $_POST["notes"];
            $data["topic"] = $_POST["topic"];
            $data["group_id"] = $_POST["group"];
            $data["source_id"] = $_POST["request_id"];    
            $result = $objRiskAssessment->addSheQnotes($data);
            if($result)
            {                               
                 $data = array();
                 $data['type'] = 'success';
                 $data['message'] = 'SHEQF note successfully added';  
                 $data["start_id"] = $_POST["request_id"];
                 controller::nextPage("DoStartRiskAssessment","risk_assessment",$data);      
            }
            else
            {
                $data = array();
                $data['type'] = 'error';
                $data['message'] = 'Failed to add sheqf notes';
                $data["start_id"] = $_POST["request_id"];
                controller::nextPage("DoStartRiskAssessment","risk_assessment",$data);      
            }
        }
    }
    public static function update_risk_assessment()
    {
        global $objRiskAssessment;
        if(isset($_POST['addRiskAssessmentBtn']))
        {
            $riskAssess = array();
            if(isset($_POST["controlInEffect"]) == 1)
            {
                $riskAssess["source_id"] = $_POST["form_assessment_id"];
            }
            else
            {
                $riskAssess["source_id"] = $_POST["assessment_id"];
            }            
            $riskAssess["start_id"] = $_POST["start_id"];            
            $riskAssess["source_type"] = "Risk Assessment";            
            $riskAssess["hazard_type"] = $_POST["harzard_type"];
            $riskAssess["hazard_description"] = $_POST["hazard_description"];
            $riskAssess["risk_type"] = $_POST["risk_type"];
            $riskAssess["risk_description"] = $_POST["risk_description"];
            $riskAssess["pscore"] = $_POST["pscore"];
            $riskAssess["control_type"] = $_POST["control_type"];
            $riskAssess["control_description"] = $_POST["control_description"];
            $riskAssess["is_control_in_effect"] = $_POST["controlInEffect"];
            
            $riskAssess["reduced_by"] = null;
            $riskAssess["new_scrore"] = null;
            $riskAssess["risk_notes"] = null;
            $riskAssess["risk_photo"] = null;
            
            if($_POST["controlInEffect"] == 0)
            {
                $riskAssess["risk_notes"] = $_POST["ControlNotes"];
                $riskAssess["reduced_by"] = $_POST["controlYesReduction"];
                $riskAssess["new_scrore"] = $_POST["controlYesScore"];
            }
            else
            {                
                $riskAssess["reduced_by"] = $_POST["controlYesReduction"];
                $riskAssess["new_scrore"] = $_POST["controlYesScore"];
            }                        
            $result = $objRiskAssessment->updateRiskAssessment($riskAssess);
            
            if($result)
            {                               
                 $data = array();
                 $data['type'] = 'success';
                 $data['message'] = 'Assessment successfully updated';  
                 $data["start_id"] = $_POST["start_id"];
                 controller::nextPage("DoStartRiskAssessment","risk_assessment",$data);      
            }
            else
            {
                $data = array();
                $data['type'] = 'error';
                $data['message'] = 'Failed to update Assessment';
                $data["start_id"] = $_POST["start_id"];
                controller::nextPage("DoStartRiskAssessment","risk_assessment",$data);      
            }
        }
        
    }
    public static function updateNotApplicable()
    {
        if(isset($_POST["btnNotApplicableRisk"]))
        {
            global $objRiskAssessment;
            $data = array();            
            $data["assessment_id"] = $_POST["assessment_id"];    
            $result = $objRiskAssessment->updateNaAssessment($data);
            if($result)
            {                               
                 $data = array();
                 $data['type'] = 'success';
                 $data['message'] = 'Assessment successfully added';  
                 $data["start_id"] = $_GET["start_id"];
                 controller::nextPage("DoStartRiskAssessment","risk_assessment",$data);      
            }
            else
            {
                $data = array();
                $data['type'] = 'error';
                $data['message'] = 'Failed to add assessment';
                $data["start_id"] = $_GET["start_id"];
                controller::nextPage("DoStartRiskAssessment","risk_assessment",$data);      
            }
        }
    }
    public static function updateRiskAssessment()
    {
        if(isset($_POST["raSubmitChangesBtn"]))
        {
            global $objRiskAssessment;
            $data = array();            
            $data["assessment_id"] = $_POST["start_id"];    
            $data["request_id"] = $_POST["request_id"];
            $data["revisedBy"] = $_POST["revisedBy"];
            $result = $objRiskAssessment->hasAssessed($data);
            if($result)
            {                               
                 $data = array();
                 $data['type'] = 'success';
                 $data['message'] = 'Assessment successfully completed';  
                 controller::nextPage("RegisterRiskAssessment","risk_assessment",$data);      
            }
            else
            {
                $data = array();
                $data['type'] = 'error';
                $data['message'] = 'Failed to completed assessment';
                $data["start_id"] = $_GET["start_id"];
                controller::nextPage("DoStartRiskAssessment","risk_assessment",$data);      
            }
        }
    }
    public static function submitRiskAssessment()
    {
        if(isset($_POST["raSubmitChanges"]))
        {
            global $objRiskAssessment;
            $data = array();
            $data["start_id"] = $_POST["start_id"];    
            $result = $objRiskAssessment->registerAssessment($data);
            if($result)
            {                               
                 $data = array();
                 $data['type'] = 'success';
                 $data['message'] = 'Assessment successfully registered';  
                 $data["start_id"] = $_GET["start_id"];
                 controller::nextPage("RegisterRiskAssessment","risk_assessment",$data);      
            }
            else
            {
                $data = array();
                $data['type'] = 'error';
                $data['message'] = 'Failed to register assessment';
                $data["start_id"] = $_GET["start_id"];
                controller::nextPage("DoStartRiskAssessment","risk_assessment",$data);      
            }
        }
    }
     public static function doRiskAssessment()
    {
        global $objRiskAssessment;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        $data['inCompleteRiskAssessments']= $objRiskAssessment->getAllInCompleteRiskAssessments($_SESSION['company_id']);
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $objTemplate->setVariables('title','Risk Assessments');
        //$objTemplate->setView('templates/risk_assessment','dashboard_tpl',$data);
        $objTemplate->setView('templates/risk_assessment','new_risk_assessment_do_ra',$data);        
    }
    public static function DoStartRiskAssessment()
    {
        global $objRiskAssessment;
        global $objTemplate;
        global $objSheqTeam;
        global $objEmployee;
        $data = array();
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
        $data["hazard"] = $objRiskAssessment->getAllHazard();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data["risk"] = $objRiskAssessment->getAllRiskSettings();
        $data["control"] = $objRiskAssessment->getAllControlSettings();
        $data['inCompleteRiskAssessments']= $objRiskAssessment->getAllInCompleteRiskAssessments($_SESSION['company_id']);        
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/risk_assessment','new_risk_assessment_start_ra',$data);        
    }
   public static function StartRiskAssessment(){
        global $objRiskAssessment;
        if(isset($_POST["startRiskAssessmentBtn"]))
        {
            $data = array();
            $data["request_id"] = $_POST["risk_requested_id"];
            $data["risk_type_id"] = $_POST["riskAssessmentType"];
            $data["risk_template_id"] = $_POST["riskAssessmentTemplate"];
            $data["description"] = $_POST["description"];
            $data["starting_date"] = $_POST["startingDate"];
            $data["next_revision_date"] = $_POST["nextRevisionDate"];
            $data["frequency"] = $_POST["frequency"];
            $data["departments_list"] = $_POST["department_selected"];
            $data["resources_list"] = $_POST["resources_selected"];
            $data["occupation_list"] = $_POST["occupations_selected"];
            $response = $objRiskAssessment->startRiskAssessment($data);
            if($response)
            {
                 $data['type'] = 'success';
                 $data['message'] = 'Risk assessment successfully started';
                 $data["start_id"] = $objRiskAssessment->getStartRA_Id;
                 controller::nextPage("DoStartRiskAssessment","risk_assessment",$data);        
            }
            else
            {
                 $data['type'] = 'error';
                 $data['message'] = 'Failed to start risk assessment';
                  controller::nextPage("viewRiskAssessmentDashboard","risk_assessment",$data);        
            }
        }
    }    
      public static function RequestedRiskAssessment(){
        global $objRiskAssessment;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allRequestedRA']= $objRiskAssessment->getRequeqestedRA();
        $objTemplate->setVariables('title','Risk Assessments');
        //$objTemplate->setView('templates/risk_assessment','dashboard_tpl',$data);
        $objTemplate->setView('templates/risk_assessment','new_risk_assessment_requested',$data);
        
    }
    public static function RegisterRiskAssessment(){
        global $objRiskAssessment;
        global $objTemplate;
        $data = array();        
        $data['allRegistered']= $objRiskAssessment->getAllRegistered();
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/risk_assessment','new_risk_assessment_register',$data);
        
    }  
   public static function HighRiskAssessment(){
        global $objRiskAssessment;
        global $objTemplate;
        $data = array();
        
        $data['inCompleteRiskAssessments']= $objRiskAssessment->getAllInCompleteRiskAssessments($_SESSION['company_id']);
        $objTemplate->setVariables('title','Risk Assessments');
        //$objTemplate->setView('templates/risk_assessment','dashboard_tpl',$data);
        $objTemplate->setView('templates/risk_assessment','new_risk_assessment_high_risks',$data);
        
    }      
    public static function ControlsRiskAssessment(){
        global $objRiskAssessment;
        global $objTemplate;
        $data = array();
        
        $data['inCompleteRiskAssessments']= $objRiskAssessment->getAllInCompleteRiskAssessments($_SESSION['company_id']);
        $objTemplate->setVariables('title','Risk Assessments');
        //$objTemplate->setView('templates/risk_assessment','dashboard_tpl',$data);
        $objTemplate->setView('templates/risk_assessment','new_risk_assessment_risk_controls',$data);
        
    }     
    ///setting section,
    public static function view_settings()
    {
        global $objRiskAssessment;
        global $objTemplate;
        $data = array();
        $data['risk_types'] = $objRiskAssessment->getAllRiskTypes();
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/risk_assessment','settings/risk_assessment',$data);
    }
    public static function new_risk_assessment_templates()
    {
        global $objRiskAssessment;
        global $objTemplate;
        $data = array();
        $data['risk_types'] = $objRiskAssessment->getAllRiskTypes();
        $data['risk_templates'] = $objRiskAssessment->getAllRiskTemplates();
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/risk_assessment','settings/new_risk_assessment_templates',$data);
    }
    public static function new_risk_assessment_settings()
    {
        global $objRiskAssessment;
        global $objTemplate;
        $data = array();
        $data['hazards'] = $objRiskAssessment->getAllHazard();
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/risk_assessment','settings/new_risk_assessment_settings',$data);
    }
    //
    public static function new_risk_assessment_setup_view()
    {
        global $objRiskAssessment;
        global $objTemplate;
        $data = array();
        $id = (int)$_GET["temp_id"];
        $data["allCategories"] = $objRiskAssessment->getAllCriticalCategories($id);
        $data["allQuestions"] = $objRiskAssessment->getAllCriticalQuestion($id);
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/risk_assessment','settings/new_risk_assessment_setup_view',$data);
    }
     public static function create_categories()
   {
        if(isset($_POST["addCategoriesBtn"]))
        {
            $data = array();
            global $objRiskAssessment;
            $data["template_id"] = $_POST["template_id"];
            $data["category"] = $_POST["category_title"];         
            $result = $objRiskAssessment->addCriticalCategory($data);  
            header("Location: ".BASE_URL."/index.php?action=new_risk_assessment_setup_view&module=risk_assessment&temp_id=".$_POST["template_id"]);
        }
    }
    public static function schedule_requested_ra()
    {
        if(isset($_POST["scheduleBtn"]))
        {
            $data = array();
            global $objRiskAssessment;
            $data["requested_risk_id"] = $_POST["requested_risk_id"];    
            $data["suggestedDate"] = $_POST["suggestedDate"];
            $data["riskAssessor"] = $_POST["riskAssessor"];
            $result = $objRiskAssessment->addRequestOnSchedule($data);  
            self::viewRiskAssessmentDashboard();
        }
    }
    public static function mark_risk_template_active()
    {
        if(isset($_POST["markAsActive"]))
        {
            $data = array();
            global $objRiskAssessment;
            $data["template_id"] = $_POST["template_id"];    
            $data["status"] = 0;
            $result = $objRiskAssessment->updateStatus($data);  
            self::new_risk_assessment_templates();
        }
    }
    public static function create_risk_question()
    {
       if(isset($_POST["createQuestion"]))
        {
            $data = array();
            global $objRiskAssessment;
            $data["template_id"] = $_POST["template_id"];
            $data["category"] = $_POST["category_list"];         
            $data["question"] = $_POST["category_quest"];     
            $data["status"] = "";  
            $result = $objRiskAssessment->addCriticalQuestion($data);  
            header("Location: ".BASE_URL."/index.php?action=new_risk_assessment_setup_view&module=risk_assessment&temp_id=".$_POST["template_id"]);
        }  
    }
    public static function loadRiskAssessment()
    {
       if(isset($_POST["loadAssessment"]))
        {
            $data = array();
            global $objRiskAssessment;
            $data["loadrisk_template_id"] = $_POST["loadrisk_template_id"];
            $data["loadrisk_start_id"] = $_POST["loadrisk_start_id"];      
            $data["loadrisk_type_id"] = $_POST["loadrisk_type_id"];
            $result = $objRiskAssessment->loadAssessment($data); 
            if($result)
            {                               
                 $data = array();
                 $data['type'] = 'success';
                 $data['message'] = 'Assessments successfully loaded';  
                 $data["start_id"] = $_POST["loadrisk_start_id"];
                 controller::nextPage("DoStartRiskAssessment","risk_assessment",$data);      
            }
            else
            {
                $data = array();
                $data['type'] = 'error';
                $data['message'] = 'Failed to load Assessments';
                $data["start_id"] = $_POST["loadrisk_start_id"];
                controller::nextPage("DoStartRiskAssessment","risk_assessment",$data);      
            }
        }  
    }
    public static function post_hazard_settings()
    {
         global $objRiskAssessment;
         if(isset($_POST['addHazardTypesBtn']))
         {
             $hazard = $_POST["hazard"];
             $group = $_POST["group_hazard"];
             $result = $objRiskAssessment->addHazard($hazard,$group);
         }
         self::view_settings();
    }       
    public static function post_risk_types()
    {
         global $objRiskAssessment;
         if(isset($_POST['addRiskTypes']))
         {
             $types = $_POST["types"];
             $group = $_POST["group_types"];
             $result = $objRiskAssessment->addRiskType($types,$group);
         }
         self::view_settings();
    }      
    public static function post_risk_templates()
    {
         global $objRiskAssessment;
         if(isset($_POST['addRiskTemplate']))
         {
             $risk_assessment = $_POST["risk_assesment"];
             $template = $_POST["template"];
             $group = $_POST["group_types"];
             $result = $objRiskAssessment->addRiskTemplate($group,$risk_assessment,$template);
         }
         self::new_risk_assessment_templates();
    } 
    //new_risk_assessment_templates
    public static function view_settings_risk()
    {
        global $objRiskAssessment;
        global $objTemplate;
        $data = array();
        $data['risk'] = $objRiskAssessment->getAllRiskSettings();
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/risk_assessment','settings/new_risk_assessment_settings_risk',$data);
    }
    public static function post_risk_settings()
    {
         global $objRiskAssessment;
         if(isset($_POST['addRiskTypesBtn']))
         {
             $risk = $_POST["risk"];
             $group = $_POST["group_risk"];
             $result = $objRiskAssessment->addRiskSetting($risk,$group);
         }
         self::view_settings_risk();
    }
    //controls
    public static function view_settings_controls()
    {
        global $objRiskAssessment;
        global $objTemplate;
        $data = array();
        $data['control'] = $objRiskAssessment->getAllControlSettings();
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/risk_assessment','settings/new_risk_assessment_settings_controls',$data);
    }
    public static function post_control_settings()
    {
         global $objRiskAssessment;
         if(isset($_POST['addControlTypesBtn']))
         {
             $control = $_POST["controlType"];
             $group = $_POST["group_control"];
             $reduction = null;
             if(isset($_POST["reduction"]))
             {
                $reduction = str_replace("%","", $_POST["reduction"]);
             }
             $result = $objRiskAssessment->addControlSetting($control,$group,$reduction);
         }
         self::view_settings_controls();
    }
    //risk assessors
    public static function view_settings_risk_assessors()
    {
        global $objRiskAssessment;
        global $objTemplate;        
        global $objSheqTeam;
        global $objDepartment;
        global $objEmployee;  
        $data = array();
        $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['risk_assessors'] = $objRiskAssessment->getAllRiskAssessors();
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/risk_assessment','settings/new_risk_assessment_settings_risk_assessors',$data);
    }
    public static function post_risk_assessor()
    {
         global $objRiskAssessment;
         $assorsData = array();
         if(isset($_POST['addRiskAssessorBtn']))
         {
             $assorsData["dep_id"] = $_POST["risk_dep"];
             $assorsData["occupation"] = null;
             $assorsData["employee_id"] = $_POST["risk_emp"];
             $assorsData["risk_dep"] = $_POST["risk_ass_dep_val"];
             $assorsData["group_id"] = $_POST["risk_group_val"];
             $result = $objRiskAssessment->addRiskAssessor($assorsData);
         }
    }
    public static function RescheduleRiskAssessment()
    {
        global $objRiskAssessment;
        $assorsData = array();
        if(isset($_POST['rescheduleRiskAssessmentBtn']))
        {
             $assorsData["risk_requested_id"] = $_POST["reschedule_requested_id"];
             $assorsData["newDate"] = $_POST["newDate"];
             $result = $objRiskAssessment->rescheduleRA($assorsData);
             if($result)
             {
                  $data['type'] = 'success';
                  $data['message'] = 'Risk assessment successfully rescheduled';
                  controller::nextPage("viewRiskAssessmentDashboard","risk_assessment",$data);                         
             }
        }
    }
   public static function post_riskassessment_request()
    {
        if(isset($_POST["sheqfBtn"]))
        {
            global $objRiskAssessment;         
            global $objActions;
            
            $data = array();
            $data["source_id"] = $_POST["source_risk_id"];
            $data["source"] = $_POST["risk_type"];
            $data["due_date"] = $_POST["sheqfDueDate"];
            $data["priority"] = $_POST["sheqfPriority"];
            $data["person_responsible"] = $_POST["sheqfResponsiblePerson"];
            $data["notes"] = $_POST["sheqfNotes"];
            $data["template_id"] = 0;//$_POST["template_id"];
            $response = $objRiskAssessment->addRARequest($data);
            if($response)
            {   
                     //store action non-conformance
                    $data["source_id"] = (int)$_POST["source_risk_id"];
                    $data['source_type'] = "Non-Conformance";
                    $data['action_taken'] = "Requested Risk Assessment";
                    $data['action_taken_id'] = $objRiskAssessment->requested_id;
                    $data['action'] = $_POST["sheqfNotes"];
                    if($objActions->hasCreatedAction($data))
                    {
                        self::RequestedRiskAssessment();
                    }                
            }
            else
            {                               
                      
            }
        }
    }
    public static function get_list_of_templates()
    {
        global $objRiskAssessment;
        $data = array();
        $id = (int)$_GET["group_id"];
        $data = $objRiskAssessment->getAllRiskTemplates($id);        
        $list_templates = array();
        
        if($data) : foreach($data as $info):
            $list_templates[] = array("option_key"=>$info["id"],"value"=>$info["template"]);                
        endforeach;
        endif;        
        echo json_encode($list_templates);
    }
    public static function addRiskAssessment(){

        global $objRiskAssessment;
        $data = array();

        $riskNumber = null;

        $riskAssessmentType =  intval($_POST['riskAssessmentType']);


        if(!isset($_SESSION['riskNumber'])) {//avoid multiple insert of risks

            $riskData = array('riskAssessNumber' => $_POST['riskNo'],
                'riskAssessor' => $_SESSION['user_id'],
                'userTypeId' => $_SESSION['usertype_id'],
                'riskAssessmentDate' => $_POST['riskAssessmentDate'],
                'createdBy' => $_SESSION['user_id'],
                'companyId'=>$_SESSION['company_id'],
                'assessmentType'=>$riskAssessmentType);

            $riskNumber = $objRiskAssessment->addRiskAssessment($riskData);
            $_SESSION['riskNumber'] = $riskNumber;
            $_SESSION['riskAssessmentType'] = $riskAssessmentType;

            if(!is_null($riskNumber)) {

                if(isset($_SESSION['assistanceUserId'])){

                   $addAssistance =  $objRiskAssessment->addRiskAssessmentAssistance($_SESSION['assistanceUserId'],$riskNumber);

                   if($addAssistance){
                       unset($_SESSION['assistanceUserId']);
                   }

                }

                $_SESSION['risk_assessment_no'] = $objRiskAssessment->getRiskAssessmentUniqueNo($_SESSION['riskNumber']);
                

                if(isset($_POST['selectResource'])) {
                    $_SESSION['riskAssessmentItems'] = serialize($_POST['selectResource']);
                    $addRiskItems = $objRiskAssessment->addRiskAssessmentItems($_SESSION['riskNumber'], 2, $_SESSION['riskAssessmentItems']);
                    if ($addRiskItems) {
                        unset($_SESSION['riskAssessmentItems']);
                    }
                }elseif($_POST['departmentName']){
                   $_SESSION['riskAssessmentBaselineItems'] = serialize($_POST['departmentName']);

                    $addRiskItems = $objRiskAssessment->addBaselineRiskAssessmentItems($_SESSION['riskNumber'], 2, $_SESSION['riskAssessmentBaselineItems'] );
                    if ($addRiskItems) {
                        unset($_SESSION['riskAssessmentBaselineItems']);
                    }
                }

                $data['type'] = 'success';
                $data['message'] = 'Risk Assessment added successfully';
                $data['id'] = $_SESSION['riskNumber'];
                $data['riskType'] = $riskAssessmentType;
                controller::nextPage('editRiskAssessmentData','risk_assessment', $data);
            }
        }else{
            controller::nextPage('editRiskAssessmentData','risk_assessment', $data);

        }

    }


    /**
     * Method to add Risk Assessment Data
     */
    public static function addRiskAssessmentData(){

        global $objRiskAssessment;
        if($_POST && isset($_SESSION['riskNumber'])) {

            $addArea = $objRiskAssessment->addRiskAssessmentArea($_POST['risk_area_id'], $_POST['risk_group_id'], $_SESSION['riskNumber']);


            if ($addArea) {

                //Calculate final score
                //$finalScore = $_POST['pscore'] * ((100-intval($_POST['finalScore'])) / 100);

                //add assessment data..
                $riskData = array(
                    'riskHazard' => $_POST['hazard'],
                    'risk' => $_POST['risk'],
                    'score' => $_POST['pscore'],
                    'finalScore' => $_POST['finalScore'],
                    'riskPhoto' => '',
                    'groupId' => $_POST['risk_group_id'],
                    'riskArea' => $_POST['risk_area_id'],
                    'riskId' => $_SESSION['riskNumber'],
                    'createdBy' => $_SESSION['user_id']
                );


                $addAssessmentDataId = $objRiskAssessment->addAssessmentRiskData($riskData);

                //update item is assessed...
                if ($addAssessmentDataId) {

                    if(isset($_POST['ppe']) || isset($_POST['administrative']) || isset($_POST['engineering']) || isset($_POST['substitution']) ||  isset($_POST['elimination'])){

                     $addAssessmentRecommendation = $objRiskAssessment->addRiskRecommendation($_POST, $addAssessmentDataId);

                             if(!$addAssessmentRecommendation){

                                 $data['type'] = 'error';
                                 $data['message'] = "Couldn't add risk assessment recommendations" ;
                                 $data['id'] = $_SESSION['riskNumber'] ;
                                 $data['riskType'] = $_SESSION['riskAssessmentType'];
                                 controller::nextPage('editRiskAssessmentData','risk_assessment', $data);

                             }

                    }

                    $data['type'] = 'success';
                    $data['message'] = 'Risk Assessment added successfully';
                    $data['id'] = $_SESSION['riskNumber'] ;
                    $data['riskType'] = $_SESSION['riskAssessmentType'];
                    controller::nextPage('editRiskAssessmentData','risk_assessment', $data);


                }else{

                    $data['type'] = 'error';
                    $data['message'] = "Couldn't add risk assessment data" ;
                    $data['id'] = $_SESSION['riskNumber'] ;
                    $data['riskType'] = $_SESSION['riskAssessmentType'];
                    controller::nextPage('editRiskAssessmentData','risk_assessment', $data);

                }
            }
        }

    }


    /**
     * Method to View group data(operation,static,maintenance,legal..)
     */
    public static function viewGroupData(){

        global $objTemplate;
        global $objRiskAssessment;

        $data = array();
        if(isset($_POST['groupId'])) {


            $groupId = explode('_', $_POST['groupId']);


            $data['riskAssessmentHeaderInfo'] = $objRiskAssessment->getRiskAssessmentsGroupsHeader($_SESSION['riskAssessmentType']);
            $data['assessedItemsInfo'] = $objRiskAssessment->getRiskAssessmentItems($_SESSION['riskNumber']);

            $data['riskAssessmentArea'] = $objRiskAssessment->getRiskAssessmentsDataAreas();
            $data['riskAssessmentAreas'] = $objRiskAssessment->getRiskAssessmentData($data['riskAssessmentArea'], $_SESSION['riskNumber'], $groupId[1]);

            $objTemplate->setVariables('title', 'Risk Assessments');
            $objTemplate->setView('templates/risk_assessment/ajax_view', 'area_data', $data);

        }else{

            $data['type'] = 'error';
            $data['message'] = "Couldn't load risk assessment data" ;
            $data['id'] = $_SESSION['riskNumber'] ;
            $data['riskType'] = $_SESSION['riskAssessmentType'];
            controller::nextPage('editRiskAssessmentData','risk_assessment', $data);

        }

    }

    /**
     * Method to mark  risk assessment as complete
     */
    public static function completeRiskAssessment(){

        global $objRiskAssessment;

        if(isset($_GET['id'])){

           $isCompleteRiskAssessment =  $objRiskAssessment->completeRiskAssessment($_GET['id']);

           if($isCompleteRiskAssessment){

               $data['type'] = 'success';
               $data['message'] = 'Risk Assessment updated successfully';
               $data['id'] = $_GET['id'];
               controller::nextPage('viewRiskAssessmentDashboard','risk_assessment', $data);
           }

        }else{

            $data['type'] = 'error';
            $data['message'] = 'Please select risk assessment to mark as complete';
            controller::nextPage('viewRiskAssessmentDashboard','risk_assessment', $data);

        }

    }


    /**
     * Method to update single risk assessment via ajax
     */
    public static function updateSingleRecommendation(){


        global $objRiskAssessment;

        if(isset($_POST['recommendationId'])){

            $data_recommendation = array('recommendation_message'=>$_POST['recommendation'],
                                        'recommendationId'=>$_POST['recommendationId']
                                         );

            $isUpdated =  $objRiskAssessment->updateSingleRecommendation($data_recommendation);

            if($isUpdated){

                $data['type'] = 'success';
                $data['message'] = 'Recommendation updated successfully';
                echo json_encode($data);
            }else{
                $data['type'] = 'error';
                $data['message'] = "Couldn't update recommendation.";
                echo json_encode($data);

            }

        }else{
            $data['type'] = 'error';
            $data['message'] = "Couldn't update recommendation, Please select recommendation to update";
            echo json_encode($data);

        }

    }

    /**
     * Method to edit risk assessment data
     */
    public static function editRiskAssessmentData(){

        global $objTemplate;
        global $objRiskAssessment;

        $data = array();

        if(isset($_POST['data_id'])){

            //add assessment data..

            //$finalScore = $_POST['pscore'] * ((100-intval($_POST['finalScore'])) / 100);   //($_POST['finalScore'] / 100) - 1;
            $riskData = array(
                'riskHazard'=>$_POST['hazard'],
                'risk'=>$_POST['risk'],
                'score'=>$_POST['pscore'],
                'finalScore'=>$_POST['finalScore'],
                'riskPhoto'=>'',
                'riskArea'=>$_POST['risk_area_id'],
                'data_id'=>$_POST['data_id'],
                'createdBy'=>$_SESSION['user_id']
            );


            $updateAssessmentDataId = $objRiskAssessment->updateAssessmentRiskData($riskData);


            if($updateAssessmentDataId) {

                if(isset($_POST['ppe']) || isset($_POST['administrative']) || isset($_POST['engineering']) || isset($_POST['substitution']) ||  isset($_POST['elimination'])) {

                    $addAssessmentRecommendation = $objRiskAssessment->updateMultipleRiskRecommendation($_POST, $_POST['data_id']);

                    if (!$addAssessmentRecommendation) {

                        $data['type'] = 'error';
                        $data['message'] = "Couldn't add or update risk assessment recommendations";
                        $data['id'] = $_SESSION['riskNumber'];
                        $data['riskType'] = $_SESSION['riskAssessmentType'];
                        controller::nextPage('editRiskAssessmentData', 'risk_assessment', $data);

                    }
                }

                $data['type'] = 'success';
                $data['message'] = 'Risk Assessment updated successfully';
                $data['id'] = $_SESSION['riskNumber'] ;
                $data['riskType'] = $_SESSION['riskAssessmentType'];
                controller::nextPage('editRiskAssessmentData','risk_assessment', $data);

            }else{

                $data['type'] = 'error';
                $data['message'] = "Couldn't update risk assessment data" ;
                $data['id'] = $_SESSION['riskNumber'] ;
                $data['riskType'] = $_SESSION['riskAssessmentType'];
                controller::nextPage('editRiskAssessmentData','risk_assessment', $data);

            }

        }else{

            //Unset session variables
            if (isset($_SESSION['riskNumber'])) {
                unset($_SESSION['riskNumber']);
            }

            if(isset($_GET['id'])) {
                $_SESSION['riskNumber'] = $_GET['id'];
                $_SESSION['riskAssessmentType'] = $_GET['riskType'];

                $_SESSION['risk_assessment_no'] = $objRiskAssessment->getRiskAssessmentUniqueNo($_SESSION['riskNumber']);
                $data['riskAssessmentInfo'] = $objRiskAssessment->getRiskAssessmentHeaderInfo($_SESSION['riskNumber']);
                $data['riskAssistanceInfo'] = $objRiskAssessment->getRiskAssessmentAssistantInfo($_SESSION['riskNumber']);
                $data['riskAssessmentRecommendations'] = $objRiskAssessment->getRiskAssessmentsRecommendationTypes();
                $data['riskAssessmentHeaderInfo']= $objRiskAssessment->getRiskAssessmentsGroupsHeader($_SESSION['riskAssessmentType']);
                $data['assessedItemsInfo'] = $objRiskAssessment->getRiskAssessmentItems($_SESSION['riskNumber']);
                $data['riskAssessmentGroups'] = $objRiskAssessment->getRiskAssessmentsGroups($_SESSION['riskAssessmentType']);
                $data['riskAssessmentAreas'] = $objRiskAssessment->getRiskAssessmentData($_SESSION['riskNumber']); //getRiskAssessmentsAreas();


                $objTemplate->setVariables('title', 'Risk Assessments');
                $objTemplate->setView('templates/risk_assessment', 'edit_risk_assessment_tpl', $data);

            }else{

                $data['type'] = 'error';
                $data['message'] = "Couldn't edit risk assessment.";
                controller::nextPage('viewRiskAssessmentDashboard','risk_assessment', $data);
            }
        }

    }


    public static function defaultAction()
    {
        return ROOT.'/frontend/templates/login_tpl.php';
    }




}