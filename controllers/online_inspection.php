<?php

class online_inspection extends controller
{
    public static function inspection_online() 
    {
         global $objTemplate;
         global $objOnlineInspections;
         $data = array();
        $data['allOnlineInspectionsUpcoming'] = $objOnlineInspections->getAllOnlineInspectionPlans();
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/online_inspection','inspection_online',$data);
    }  
   public static function inspection_online_inspect_now() {
         global $objTemplate;
         global $objDepartment;
         global $objOnlineInspections;
         global $objEmployee;
        $objTemplate->setVariables('title','Occupation');
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
         $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data["allInspections"] = $objOnlineInspections->getAllInspections();
        $objTemplate->setView('templates/online_inspection','inspection_online_inspect_now',$data);
    }   
    public static function get_list_of_templates()
    {
        global $objOnlineInspections;
        $data = array();
        $id = (int)$_GET["group_id"];
        $data = $objOnlineInspections->getAllInspectionsByGroup($id);        
        $list_templates = array();
        
        if($data) : foreach($data as $info):
            $list_templates[] = array("option_key"=>$info["id"],"value"=>$info["inspection_description"]);                
        endforeach;
        endif;        
        echo json_encode($list_templates);
    }
    public static function inspection_online_start_inspection() {
         global $objTemplate;
         $objTemplate->setVariables('title','Occupation');
         $objTemplate->setView('templates/online_inspection','inspection_online_start_inspection');
    }  
    public static function inspection_online_inspection_plan() 
    {
         global $objTemplate;
         global $objOnlineInspections;
         global $objEmployee;
         global $objDepartment;
         $data = array();
         $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
         $data['allInspectionPlans'] = $objOnlineInspections->getAllOnlineInspectionPlansForView();
         $data["allInspections"] = $objOnlineInspections->getAllInspections();
         $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/online_inspection','inspection_online_inspection_plan',$data);
    }
    public static function inspection_online_create_inspection() 
    {
         global $objTemplate;
         global $objOnlineInspections;
         global $objDepartment;
         global $objAssets;
         $data["allInspections"] = $objOnlineInspections->getAllInspectionsList();
         $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/online_inspection','inspection_online_create_inspection',$data);
    } 
     public static function inspection_online_inspection_history() {
         global $objTemplate;
         global $objOnlineInspections;
         global $objDepartment;
         global $objAssets;
         $data["allInspections"] = $objOnlineInspections->getAllHistoryInspections();         
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/online_inspection','inspection_online_inspection_history',$data);
    } 
    public static function inspection_online_inspection_settings() {
         global $objTemplate;
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/online_inspection','inspection_online_settings');
    } 
    public static function inspection_online_nonconformances() {
         global $objTemplate;
         global $objOnlineInspections;
         $data['allNonConformance'] = $objOnlineInspections->viewAllConformance();
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/online_inspection','inspection_online_nonconformances',$data);
    }
    public static function inspection_online_nonconformances_closed()
    {
       global $objTemplate;
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/online_inspection','inspection_online_nonconformances_closed');  
    }
    public static function inspection_online_resource()
    {
                  global $objTemplate;
                  global $objMaterial;
                  global $objAssets;                  
                  global $objOnlineInspections;
                  $id = (int)$_GET["inspection_id"];
                  $objTemplate->setVariables('title','Online Resource Inspection');
                  $objOnlineInspections->getInspectionDetails($id);
                  $data = array();
                  $data["AllMaterials"] = $objMaterial->getAllMaterials();
                  $data['allResources'] = $objAssets->getAllAsset();  
                  $data["AllMaterialsOnlineInspection"] = $objOnlineInspections->getAllMaterials();
                  $data["allIso"] = $objOnlineInspections->getAllISOs($id);
                  $data["allCategories"] = $objOnlineInspections->getAllCriticalCategories($id);
                  $data["allQuestions"] = $objOnlineInspections->getAllCriticalQuestion($id);
                  $objTemplate->setView('templates/online_inspection','inspection_online_create_resource',$data);
    }    
    public static function create_online_inspection()
    {
        if(isset($_POST["createInspectionBtn"]))
        {
            $data = array();
            global $objOnlineInspections;
            $data["group_id"] = $_POST["group"];
            //$data["description_type"] = $_POST["inspectionType"];
            // $data["specific_type"] = $_POST["specificType"];
             //$data["department"] = $_POST["inspectionDepartment"];
             $data["inspection_description"] = $_POST["inspectionDescription"];
             $result = $objOnlineInspections->addOnlineInspection($data);
            if($result > 0)
            {                               
                 $data = array();
                 $data['type'] = 'success';
                 $data['message'] = 'Inspection Template successfully created';  
                 $data['inspection_id'] = $result;
                 controller::nextPage("inspection_online_resource","online_inspection",$data);      
            }
            else
            {
                $data = array();
                $data['type'] = 'error';
                $data['message'] = 'Failed to create Inspection Template';
                $data['inspection_id'] = $result;
                controller::nextPage("inspection_online_resource","online_inspection",$data);      
            }
             /*if($result)
             {
                 header("Location: ".BASE_URL."/index.php?action=inspection_online_resource&module=online_inspection&inspection_id=".$result);                  
             }
             else
             {
                 self::inspection_online_create_inspection();
             }*/
        }
    }
    public static function create_inspection_online_inspection_plan()
    {
        if(isset($_POST["addInspectionBtn"]))
        {
            $data = array();
            global $objOnlineInspections;
            $data["group_id"] = $_POST["group"];
            $data["description"] = $_POST["description"];
             $data["loadInspectionFrequency"] = $_POST["loadInspectionFrequency"];
             //$data["nextInspectionDate"] = $_POST["nextInspectionDate"];
              //$data["inspector"] = $_POST["inspector"];   
              $data["department"] = $_POST["inspectionDepartment"];
             $result = $objOnlineInspections->addOnlineInspectionPlan($data);
            if($result)
            {                               
                 $data = array();
                 $data['type'] = 'success';
                 $data['message'] = 'Inspection Plan successfully created';  
                 controller::nextPage("inspection_online_inspection_plan","online_inspection",$data);      
            }
            else
            {
                $data = array();
                $data['type'] = 'error';
                $data['message'] = 'Failed to create Inspection Plan';
                controller::nextPage("inspection_online_inspection_plan","online_inspection",$data);      
            }
            // self::inspection_online_inspection_plan();
        }
    }
    public static function schedule_inspection_plan()
    {
        if(isset($_POST["scheduleBtn"]))
        {
            $data = array();
            global $objOnlineInspections;
            $data["plan_id"] = $_POST["plan_id"];    
            $data["ScheduleDate"] = $_POST["ScheduleDate"];
            $data["inspectors_id"] = $_POST["inspectors_id"];
            $result = $objOnlineInspections->addPlanOnSchedule($data);
            if($result)
            {                               
                 $data = array();
                 $data['type'] = 'success';
                 $data['message'] = 'Inspection Plan successfully scheduled';  
                 controller::nextPage("inspection_online","online_inspection",$data);      
            }
            else
            {
                $data = array();
                $data['type'] = 'error';
                $data['message'] = 'Failed to schedule Inspection Plan';
                controller::nextPage("inspection_online_inspection_plan","online_inspection",$data);      
            }
        }
    }
    public static function create_legal_iso()
    {
        if(isset($_POST["createISO"]))
        {
            $data = array();
            global $objOnlineInspections;
            $data["inspection_id"] = $_POST["inspection_id"];
            $data["iso_description"] = $_POST["iso_description"];
            $data["iso_title"] = $_POST["iso_title"];         
            $result = $objOnlineInspections->addISO($data);  
            header("Location: ".BASE_URL."/index.php?action=inspection_online_resource&module=online_inspection&inspection_id=".$_POST["inspection_id"]);
        }
    }
   public static function create_categories()
   {
        if(isset($_POST["createCategory"]))
        {
            $data = array();
            global $objOnlineInspections;
            $data["inspection_id"] = $_POST["inspection_id"];
            $data["category"] = $_POST["category_title"];         
            $result = $objOnlineInspections->addCriticalCategory($data);  
            header("Location: ".BASE_URL."/index.php?action=inspection_online_resource&module=online_inspection&inspection_id=".$_POST["inspection_id"]);
        }
    }
    public static function create_inspection_question()
    {
       if(isset($_POST["createQuestion"]))
        {
            $data = array();
            global $objOnlineInspections;
            $data["inspection_id"] = $_POST["inspection_id"];
            $data["category"] = $_POST["category_list"];         
            $data["question"] = $_POST["category_quest"];     
            $data["status"] = "";  
            $result = $objOnlineInspections->addCriticalQuestion($data);  
             header("Location: ".BASE_URL."/index.php?action=inspection_online_resource&module=online_inspection&inspection_id=".$_POST["inspection_id"]);
        }  
    }
    public static function loadInspectionAssessment()
    {
       if(isset($_POST["loadAssessment"]))
        {
            $data = array();
            global $objOnlineInspections;
            $data["inspection_template_id"] = $_POST["inspection_template_id"];
            $data["inspection_plan_id"] = $_POST["inspection_plan_id"];      
            $result = $objOnlineInspections->loadAssessment($data); 
            if($result)
            {                               
                 $data = array();
                 $data['type'] = 'success';
                 $data['message'] = 'Assessments successfully loaded';  
                 $data["id"] = $_POST["inspection_plan_id"];
                 controller::nextPage("inspection_online_start_inspection","online_inspection",$data);      
            }
            else
            {
                $data = array();
                $data['type'] = 'error';
                $data['message'] = 'Failed to load Assessments';
                $data["id"] = $_POST["inspection_plan_id"];
                controller::nextPage("inspection_online_start_inspection","online_inspection",$data);      
            }
        }  
    }
    public static function submit_inspection()
    {
         if(isset($_POST["createInspectionResourceBtn"]))
        {
             
            $data = array();
            global $objOnlineInspections;
            $data["details"] = $_POST["purposeOfDescription"];
            $data["frequency"] = $_POST["frequency"];
            $data["inspection_id"] = $_POST["inspection_id"];
            //update inspection_details
             $data["present"] = null;
            $result = $objOnlineInspections->updateOnlineInspection($data);
            if($result)
            {
                //loop through each asset
                $listOfAssets = explode(",", $_POST["assets_selected"]);
                $listOfMaterials = explode(",", $_POST["materials_selected"]);
                foreach($listOfAssets as $asset)
                {
                    $data["asset_id"] = $asset;  
                   if($objOnlineInspections->addOnlineInspectionAsset($data))
                   {
                      
                   }
                }
                
                foreach($listOfMaterials as $materials)
                {
                   $data["material_id"] = $materials;
                   if($objOnlineInspections->addOnlineInspectionMaterial($data))
                   {
                      
                   }
                } 
            }
            else
            {
               // echo "Hello World3";
            }
            self::inspection_online();
        }
        else
        {
            //echo "Hello World";
        }
    }
    public static function addCheck()
    {
        if(isset($_POST["inspection_id"]))
        {
            global $objOnlineInspections;
            $response = array("state"=>0,"status"=>"");
            $data = array();
            $data["complained"] = $_POST["answer"];
            $data["inspection_id"] = $_POST["inspection_id"];
            $data["id"] = $_POST["question_id"];
            $status = 200;
            if($_POST["answer"] == 1)
            {
                $result = $objOnlineInspections->updateQuestionsAssessment($data);
                if($result)
                {
                    $response = array("state"=>200,"status"=>"Complaint");
                    $status = 200;
                }
            }
            if($_POST["answer"] == 0)
            {
                $data["id"] = $_POST["question_id"];
                $data["id"] = $_POST["question_id"];
                $data["id"] = $_POST["question_id"];
                                                
                $result = $objOnlineInspections->updateQuestionsAssessment($data);
                if($result)
                {
                    $response = array("state"=>200,"status"=>"Complaint");
                    $status = 200;
                }
            }
            
            echo $status;
        }
    }
    public static function addInspectionCheckStatus()
    {
        if(isset($_POST["addCheckButtonNon"]))
        {
            global $objOnlineInspections;            
            $data = array();
            $data["department_field"] = $_POST["department_field"];
            $data["group_field"] = $_POST["group_field"];            
            $data["complained"] = $_POST["complaint"];
            $data["resource"] = $_POST["check_resource"];
            $data["comments"] = $_POST["check_comments"];
            $data["photo"] = null;
            if(!empty($_FILES['photo']))
            {
                //check the file size, check the               
                $target = $_SERVER['DOCUMENT_ROOT']."/sheq/frontend/media/uploads/";
                $target = $target . basename($_FILES['photo']['name']);
                $encrypted_file = $_FILES['photo']['tmp_name'];            
                if(move_uploaded_file($encrypted_file,$target))
                {
                    $data['photo'] = $_FILES['photo']['name'];  
                }          
            }        
            $data["question_id"] = $_POST["quest_id"];
            $data["inspection_id"] = $_POST["inspection_id"];
            $response = $objOnlineInspections->addInspectionNoConformance($data);
            if($response)
            {             
                $response2 = $objOnlineInspections->updateQuestionsAssessment($data);
            }
            header("Location: ".BASE_URL."/index.php?action=inspection_online_start_inspection&module=online_inspection&id=".$_POST["inspection_id"])."&group=".$_POST["group_field"]."&dep=".$_POST["department_field"];            //adds the information
        }
    }
    public static function addInspectionCheck()
    {
       if(isset($_POST["inspection_id"]))
        {
            global $objOnlineInspections;
            $data = array();
            $data["asset_id"] = $_POST["asset_id"];
            $data["present"] = $_POST["present"];            
            $status = 404;
            if($_POST["location"] == "Asset")
            {
                $response = $objOnlineInspections->updateInspectionAssets($data);
                if($response)
                {
                    //go the history view
                   $status = 200;
                }
            }
            if($_POST["location"] == "Material")
            {
                $response = $objOnlineInspections->updateInspectionMaterials($data);
                if($response)
                {
                    //go the history view
                   $status = 200;
                }
            }
            echo $status;
        } 
    }
    //submit complete
    public static function submitInspection()
    {
        if(isset($_POST["startInspectionBtn"]))
        {
            global $objOnlineInspections;
            $data = array();
            //plan id
            $data["id"] = $_POST["inspection_id"];
            $response = $objOnlineInspections->updateInspectionToInspected($data);
            if($response)
            {                               
                 $data = array();
                 $data['type'] = 'success';
                 $data['message'] = 'Inspection successfully submitted';  
                 $data["id"] = $_POST["inspection_id"];
                 controller::nextPage("inspection_online_inspection_history","online_inspection",$data);      
            }
            else
            {
                $data = array();
                $data['type'] = 'error';
                $data['message'] = 'Failed to submit inspection';
                $data["id"] = $_POST["inspection_id"];
                controller::nextPage("inspection_online_start_inspection","online_inspection",$data);      
            }
        }
    }
}

