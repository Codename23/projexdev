<?php
/**
 * Controller class containing methods to process all supplier actions
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class supplier_client extends controller{




    public static function viewDashboard(){
        global $objTemplate;

        $data = array();

        $objTemplate->setVariables('title','Supplier Info');
        $objTemplate->setView('templates/supplier_client','index_tpl',$data);
    }


}