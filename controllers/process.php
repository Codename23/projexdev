<?php

class process extends controller
{
    public static function defaultAction() {
         global $objTemplate;
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/process','process_dash');
    }
    public static function view_stock_supply() {
         global $objTemplate;
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/process/settings/','stock_supply');
    }
    public static function view_risk_controls() {
         global $objTemplate;
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/process/settings/','risk_controls');
    }
    public static function view_rps() {
         global $objTemplate;
         global $objProcess;
         global $objDepartment;
         global $objAssets;
         $data = array();
        $data["allDepartments"] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data['assets'] = $objAssets->getAllAsset();
        $data["allProcessStoppers"] = $objProcess->getAllProcessStoppers();
        $objTemplate->setVariables('title','Occupation');        
        $objTemplate->setView('templates/process/settings/','rps',$data);
    }
    public static function view_control_sample_point() {
         global $objTemplate;
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/process/settings/','process_control');
    }
    public static function view_products_services() {
         global $objTemplate;
         global $objProcess;
         $data["allProductsServices"] = $objProcess->getAllProductsAndServices();
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/process/settings/','process_products_service',$data);
    }
    public static function view_create_process()
    {
         global $objTemplate;
         global $objProcess;
         global $objMaterial;
        global $objDepartment;
       $data = array();
       $data["AllMaterials"] = $objMaterial->getAllMaterials();
       $data["allMaterialTypes"] = $objMaterial->getMaterialTypes();
       $data["allMaterialCategories"] = $objMaterial->getMaterialCategories();
       $data["allDepartments"] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
         $data["allProductsServices"] = $objProcess->getAllProductsAndServices();
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/process','create_process',$data);
    }
    /*
     * Product Section post information 
     **/
    public static function post_product_service()
    {
        if(isset($_POST["ajaxPostProductService"]))
        {
            global $objProcess;
            $response = $objProcess->addProductService($_POST["product"],$_POST["description"]);
            if($response)
            {
                echo "OK";
            }
            else
            {
                echo "Failed";
            }
        }  
        if(isset($_POST["addProductServiceBtn"]))
        {
            global $objProcess;
            $response = $objProcess->addProductService($_POST["product"],$_POST["description"]);
            self::view_products_services();
        }
    }
    public static function post_process_stoppers()
    {
        if(isset($_POST["ajaxPostProcessStopper"]))
        {
            global $objProcess;
            $response = $objProcess->addProcessStopper($_POST["process_department"],$_POST["description"]);
            if($response)
            {
                echo "OK";
            }
            else
            {
                echo "Failed";
            }
        }  
        if(isset($_POST["addCriticalAssetBtn"]))
        {
            global $objProcess;
            $response = $objProcess->addProcessStopper($_POST["process_department"],$_POST["searchAsset"]);
            self::view_rps();
        }
    }
}

