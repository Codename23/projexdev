<?php

/**
 * Controller class containing methods to process all company related actions
 *
 * @package sheqonline
 * @author Brian Douglas <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

//Include the companies class
//include_once(ROOT.'/classes/companies_class.php');

class companies extends controller
{

    /**
     * var $objCompanies The companies object
     * @access private
     */
    private $objCompanies;

    /**
     * Load all companies view
     */
    public static function viewAllCompanies(){

        global $objCompanies;
        global $objTemplate;
        global $objRegions;

        $data = array();

        $data['countries'] = $objRegions->getAllCountries();
        $data['allCompanies'] = $objCompanies->getAllCompanies();
        $objTemplate->setVariables('title','Company info');
        $objTemplate->setView('templates/company','index_tpl',$data);

    }



    /**
     * Get assigned companies for a user
     * @TODO: Revisit this. maybe remove
     */
    public static function assignedCompanies(){

        //Default company
        global $objCompanies;
        return $objCompanies->getUserCompanies($_SESSION['user_id']);
    }

    /**
     * method to add a new company
     */
    public static function addCompany(){
        global $objCompanies;
        global $objTemplate;

        if(!empty($_POST)){

            $companyData = array(
                "company_name" => $_POST['companyName'],
                "headOfficeName" => $_POST['officeDescription'],
                "vat_number" => $_POST['vatNumber'],
                "logo" =>  '', //$_POST['logo'];
                "pty_number" => $_POST['ptyNumber'],
                "numberofBranch" => $_POST['numberofBranch'],
                "office_branch" => $_POST['officeAddress'],
                "office_number" =>$_POST['officeNumber'],
                "cell_number" => $_POST['cellphoneNumber'],
                "fax_number" =>$_POST['faxNumber'],
                "email" =>  $_POST['email'],
                "country_id" =>$_POST['countryName'],
                "region_id" => $_POST['regionName']

            );

            $addCompany = $objCompanies->addCompany($companyData);

            if($addCompany){

                $data['type'] = 'success';
                $data['message'] = 'Company added successfully';
                controller::nextPage('viewAllCompanies','companies', $data);

            }else{

                $data['type'] = 'error';
                $data['message'] = 'Could not add company';
                controller::nextPage('viewAllCompanies','companies', $data);
            }

        }else{

            $data = array();
            $objTemplate->setVariables('title', 'Add Company');
            $objTemplate->setView('templates/company', 'add_company_tpl', $data);
        }
    }

    /**
     * Method to edit selected company info
     */
    public static function editCompany(){

        global $objCompanies;
        global $objTemplate;
        global $objRegions;

        if(!empty($_POST)){

            $companyData = array(
                "company_name" => $_POST['companyName'],
                "vat_number" => $_POST['vatNumber'],
                "logo" =>  '', //$_POST['logo'];
                "pty_number" =>  $_POST['ptyNumber'],
                "numberofBranch" => $_POST['numberofBranch'],
                "office_branch" =>  $_POST['officeAddress'],
                "office_number" => $_POST['officeNumber'],
                "cell_number" =>  $_POST['cellphoneNumber'],
                "fax_number" => $_POST['faxNumber'],
                "email" =>  $_POST['email'],
                "company_id" => $_POST['id'],
                "country_id" =>$_POST['countryName'],
                "region_id" => $_POST['regionName']
            );

            $modifiedBy = $_SESSION['user_id'];

            $editCompany = $objCompanies->editCompany($companyData,$modifiedBy);


            if($editCompany){

                $data['type'] = 'success';
                $data['message'] = 'Company updated successfully';
                $data['is_active'] = $_POST['is_active'];
                $data['id'] = $_POST['id'];
                controller::nextPage('editCompany','companies', $data);

            }else{

                $data['type'] = 'error';
                $data['message'] = 'Failed to update company';
                $data['is_active'] = $_POST['is_active'];
                $data['id'] = $_POST['id'];
                controller::nextPage('editCompany','companies', $data);
            }
        }else{

            $data = array();
            $id = intval($_GET['id']);

            $data['is_active']  = isset($_GET['is_active']) ? $_GET['is_active'] : null;

            $data['editCompanyInfo'] = $objCompanies->getCompanyInfo($id);
            $data['countries'] = $objRegions->getAllCountries();
            $data['companyRegions'] = $objRegions->getCountryRegions($data['editCompanyInfo']['country_id']);
            $data['companyBranches'] = $objCompanies->getCompanyBranches($id);
            $data['noOfBranches'] = $objCompanies->countCompanyBranches($id);
            $data['noOfDepartments'] = $objCompanies->countCompanyDepartments($id);
            $data['noOfEmployees'] = $objCompanies->countCompanyEmployees($id);

            $objTemplate->setVariables('title', 'Edit Company Info');
            $objTemplate->setView('templates/company', 'edit_company_tpl', $data);

        }

    }


    /**
     * Method to view all companies
     */
    public static function viewAllCompanyBranches(){

        global $objCompanies;
        global $objTemplate;


        $data = array();

        $id = intval($_GET["id"]);

        $data['companyId'] = $id;
        $data['allCompanyBranches'] = $objCompanies->getCompanyBranches($id);
        $objTemplate->setVariables('title','Branches');
        $objTemplate->setView('templates/branch','index_tpl',$data);
    }

    /**
     * Method to add a new branch
     */
    public static function addBranch(){

        global $objCompanies;
        global $objTemplate;

        if(!empty($_POST)){

            $branchData = array(
                "branchName" => $_POST['branchName'],
                "officeAddress" => $_POST['officeAddress'],
                "officeNumber" => $_POST['officeNumber'],
                "faxNumber" => $_POST['faxNumber'],
                "officeEmail" => $_POST['officeEmail'],
                "isHeadOffice" => 0,
                "countryId" => $_POST['branchCountryName'],
                "regionId" => $_POST['branchRegionName'],
                "createdBy" => $_SESSION['user_id'],
                "companyId" =>$_POST['companyId']
            );

            $addBranch = $objCompanies->addBranch($branchData);

            if($addBranch){

                $data['type'] = 'success';
                $data['message'] = 'Branch added successfully';
                $data['is_active'] = $_POST['is_active'];
                $data['id'] = $_POST['companyId'];
                controller::nextPage('editCompany','companies', $data);

            }else{

                $data['type'] = 'error';
                $data['message'] = 'Failed to add branch';
                $data['is_active'] = $_POST['is_active'];
                $data['id'] = $_POST['companyId'];
                controller::nextPage('editCompany','companies', $data);
            }

        }else{

            $data['companyId'] = intval($_GET['id']);

            $objTemplate->setVariables('title','Add Branch');
            $objTemplate->setView('templates/branch','add_branch_tpl',$data);
        }
    }

    /**
     * Method to edit branch
     */
    public static function editBranch(){

        global $objCompanies;
        global $objTemplate;
        global $objRegions;

        if(!empty($_POST)){

            $branchData = array(
                "branchName"=> $_POST['branchName'],
                "officeAddress"=> $_POST['officeAddress'],
                "officeNumber"=> $_POST['officeNumber'],
                "faxNumber"=> $_POST['faxNumber'],
                "officeEmail"=> $_POST['officeEmail'],
                "regionId" => $_POST['branchRegionName'],
                "countryId" => $_POST['branchCountryName'],
                "modifiedBy"=> $_SESSION['user_id'],
                "branchId"=> $_POST['id']
            );

            $editBranch = $objCompanies->editBranch($branchData);

            if($editBranch){

                $data['type'] = 'success';
                $data['message'] = 'Branch updated successfully';
                $data['id'] = $_POST['companyId'];
                $data['is_active'] = $_POST['is_active'];
                controller::nextPage('editCompany','companies', $data);

            }else{

                $data['type'] = 'error';
                $data['message'] = 'Failed to update branch';
                $data['id'] = $_POST['id'];
                $data['is_active'] = $_POST['is_active'];
                controller::nextPage('editBranch','companies', $data);

            }

        }else{
            $data = array();
            $id = intval($_GET["id"]);
            $data['editBranchInfo'] = $objCompanies->getBranchInfo($id);
            $data['countries'] = $objRegions->getAllCountries();
            $data['companyRegions'] = $objRegions->getCountryRegionsByCompany($data['editBranchInfo']['company_id']);
            $objTemplate->setVariables('title', 'Edit Branch Info');
            $objTemplate->setView('templates/branch', 'edit_branch_tpl', $data);
        }
    }


    /**
     * Method to view all departments for a company
     */
    public static function viewAllCompanyDepartments(){

        global $objCompanies;
        global $objTemplate;

        $data = array();
        $companyId = $_GET['id'];//company id

        $data['allCompanyDepartments'] = $objCompanies->getCompanyDepartments($companyId);
        $objTemplate->setVariables('title','Departments');
        $objTemplate->setView('templates/department','index_tpl',$data);

    }


    /**
     * Method to view all departments for a branch
     */
    public static function viewAllBranchDepartments(){

        global $objCompanies;
        global $objTemplate;

        $data = array();

        $id = $_GET['branchId'];//branch id
        $companyId = $_GET['companyId'];//company id
        $data['branchId'] = $id;
        $data['companId'] = $companyId;
        $data['allBranchDepartments'] = $objCompanies->getCompanyDepartments($id,$companyId);
        $objTemplate->setVariables('title','Departments');
        $objTemplate->setView('templates/department','index_tpl',$data);

    }

    /**
     * Method to add a new department
     */
    public static function addDepartment(){
        global $objCompanies;

        if(!empty($_POST)){

            $departmentData = array(
                "departmentName"=> $_POST['departmentName'],
                "buildingName"=> $_POST['buildingName'],
                "floorName"=> $_POST['floorName'],
                "branchId"=> $_POST['branchId'],
                "companyId"=> $_POST['companyId'],
                "createdBy"=> $_SESSION['user_id']
            );

            $addDepartment = $objCompanies->addDepartment($departmentData);

            if($addDepartment){

                $data['type'] = 'success';
                $data['message'] = 'Department added successfully';
                $data['id'] = $_POST['companyId'];
                $data['is_active'] = $_POST['is_active'];
                controller::nextPage('editCompany','companies', $data);

            }else{

                $data['type'] = 'error';
                $data['message'] = 'Failed to add new department';
                $data['id'] = $_POST['companyId'];
                $data['is_active'] = $_POST['is_active'];
                controller::nextPage('editCompany','companies', $data);
            }
        }
    }

    /**
     * Method to edit department
     */
    public static function editDepartment(){


        global $objCompanies;
        global $objTemplate;

        if(!empty($_POST)){


            $departmentData = array(
                "departmentName"=> $_POST['departmentName'],
                "buildingName"=>  $_POST['buildingName'],
                "floorName"=>  $_POST['floorName'],
                "departmentId"=> $_POST['id'],
                "branchId"=>  $_POST['branchId'],
                "companyId"=>  $_POST['companyId'],
                "modifiedBy"=>  $_SESSION['user_id']
            );

            $editDepartment = $objCompanies->editDepartment($departmentData);

            if($editDepartment){

                $data['type'] = 'success';
                $data['message'] = 'Department updated successfully';
                $data['id'] = $_POST['companyId'];
                $data['is_active'] = $_POST['is_active'];
                controller::nextPage('editCompany','companies', $data);


            }else{
                $data['type'] = 'error';
                $data['message'] = 'Failed to update department';
                $data['id'] = $_POST['companyId'];
                $data['is_active'] = $_POST['is_active'];
                controller::nextPage('editCompany','companies', $data);
            }

        }else{

            $data = array();
            $id = intval($_GET["id"]);
            $data['editDepartmentInfo'] = $objCompanies->getDepartmentInfo($id);
            $objTemplate->setVariables('title', 'Edit Department Info');
            $objTemplate->setView('templates/department', 'edit_department_tpl', $data);
        }
    }

    public static function defaultAction()
    {
        return ROOT.'/frontend/templates/login_tpl.php';
    }


}