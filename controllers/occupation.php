<?php

/**
 * Controller class containing methods to process all occupation related actions
 *
 * @package sheqonline
 * @author Brian Douglas<douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

//Include the Occupation class
//include_once(ROOT.'/classes/Occupation_class.php');

class occupation extends controller
{

    /**
     * var $objOccupation The occupation object
     * @access private
     */
    private $objOccupations;


    /**
     *view all occupations
     *
     */
    public static function viewAllOccupation(){

        global $objOccupations;
        global$objTemplate;

        $data = array();

        $data['allOccupations'] = $objOccupations->getAllOccupations();
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/occupation','index_tpl',$data);

    }

    /**
     *view all occupations
     *
     */
    public static function viewCompanyOccupations(){

        global $objOccupations;
        global$objTemplate;

        $data = array();
        $companyId = intval($_GET['id']);
        $data['companyOccupations'] = $objOccupations->getCompanyOccupations($companyId);
        $objTemplate->setVariables('title','Occupation');
        $objTemplate->setView('templates/occupation','index_tpl',$data);

    }

    /**
     * Add Occupation
     */
    public static function addOccupation(){

        global $objOccupations;


        if(!empty($_POST) && isset($_POST['departmentName'])){

            $occupationData = array(
                "occupationName" => $_POST['occupationName'],
                "createdBy" =>  $_SESSION['user_id'],
                "departmentId" => $_POST['departmentName']

            );

            $editDepartment = $objOccupations->addOccupation($occupationData);

            if($editDepartment){

                $data['type'] = 'success';
                $data['message'] = 'Occupation added successfully';
                $data['id'] = $_POST['companyId'];
                $data['is_active'] = $_POST['is_active'];
                controller::nextPage("editCompany","companies",$data);

            }

        }
    }

    /**
     * Edit Occupation
     */
    public static function editOccupation(){


        global $objOccupations;
        global$objTemplate;

        if(!empty($_POST) && isset($_POST['occupationName'])){

            $occupationData = array(
                "occupationName" => $_POST['occupationName'],
                "modifiedBy" =>  $_SESSION['user_id'],
                "occupationId" => $_POST['id']

            );

            $editOccupation = $objOccupations->editOccupation($occupationData);

            if($editOccupation){
                $data['type'] = 'success';
                $data['message'] = 'Occupation updated successfully';
                $data['id'] = $_POST['companyId'];
                $data['is_active'] = $_POST['is_active'];
                controller::nextPage("editCompany","companies",$data);
            }

        }else{
            $data = array();
            $id = intval($_POST["id"]);
            $data['editOccupationInfo'] = $objOccupations->getOccupationInfo($id);

            $objTemplate->setVariables('title', 'Edit Occupation Info');
            $objTemplate->setView('templates/occupation', 'edit_occupation_tpl', $data);
        }

    }



    public static function defaultAction()
    {
        return ROOT.'/frontend/templates/login_tpl.php';
    }


}
