<?php
/**
 * Class containing all methods providing material functionality
 *
 * @package sheqonline
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */
//Include the base controller
include_once 'controller.php';

class material extends  controller {
    /**
     * @access private
     */
    private $objMaterial;
    
    public function __construct()  
    {
        
    }
    /**
     * Default method
     * @return void
     */
    public static function defaultAction()
    {
        self::nextPage('getAllMaterials','material');
    }
    /**
     * Method to view material
     * @return void
     */
    public static function getAllMaterials()
    {
       global $objMaterial;
       global $objTemplate; 
        global $objDepartment;
       $data = array();
       $data["AllMaterials"] = $objMaterial->getAllMaterials();
       $data["allMaterialTypes"] = $objMaterial->getMaterialTypes();
       $data["allMaterialCategories"] = $objMaterial->getMaterialCategories();
       $data["allDepartments"] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
       ///////////////////////////////////////////////////
       $objTemplate->setVariables('title', 'View Material');
       $objTemplate->setView('templates/material', 'material_tpl', $data);
    }
    public static function getAllSettings()
    {
       global $objMaterial;
       global $objTemplate; 
       $data = array();
       $data["allMaterialCategories"] = $objMaterial->getMaterialCategories();
       $objTemplate->setVariables('title', 'View Material');
       $objTemplate->setView('templates/material', 'material_settings', $data);
    }
    public static function getAllSettingsTypes()
    {
       global $objMaterial;
       global $objTemplate; 
       $data = array();
       $data["allMaterialTypes"] = $objMaterial->getMaterialTypes();
       $objTemplate->setVariables('title', 'View Material');
       $objTemplate->setView('templates/material', 'material_settings_types', $data);
    }
    public static function material_view_docs()
    {
       global $objMaterial;
       global $objTemplate; 
       $data = array();
       $data["allMaterialTypes"] = $objMaterial->getMaterialTypes();
       $objTemplate->setVariables('title', 'View Material');
       $objTemplate->setView('templates/material', 'material_view_docs', $data);
    }
    public static function material_view_sop()
    {
       global $objMaterial;
       global $objTemplate; 
       global $objAssets;
       global $objSheqTeam;
       $data = array();
       global $objRiskAssessment;
        $data =  array(); 
        $data["hazard"] = $objRiskAssessment->getAllHazard();
        $data["risk"] = $objRiskAssessment->getAllRiskSettings();
        $data["control"] = $objRiskAssessment->getAllControlSettings();
        $data['source'] = $_GET["source_type"];
        $data['sops'] = $objAssets->getSop($_GET['material_id'],$data['source']);
        $data['sopsDoc'] = $objAssets->getAssetDoc($_GET['material_id'],'SOP',$data['source']);    
        
        $data['risk_assessment'] = $objRiskAssessment->getRiskAssessment($_GET['material_id'],$data['source']);
        $data['sheqf_notes'] = $objRiskAssessment->getSHEQFNotes($_GET['material_id'],$data['source']);
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
       $data["allMaterialTypes"] = $objMaterial->getMaterialTypes();
       $objTemplate->setVariables('title', 'View Material');
       $objTemplate->setView('templates/material', 'material_view_sop', $data);
    }
    public static function post_sop_material()
    {
        global $objAssets;
        $sopData = array();
        if(isset($_POST['addSopDocBtn']))
        {
            $sopData["description"] = $_POST["sopDescription"];
            $sopData["resource_id"] = (int)$_POST["asset_id"];
            $sopData["source"] = $_POST["source_type"];
            $result = $objAssets->addSOP($sopData["description"],$sopData["resource_id"],$sopData["source"]);
        }
        self::material_view_sop();
    }
    public static function upload_sop_document_material()
    {
        //echo $_FILES['docUpload']['name'];
            $sopData = array();            
            global $objAssets;
            $target = $_SERVER['DOCUMENT_ROOT']."/sheq/frontend/documents/";
            $target = $target . basename($_FILES['docUpload']['name']);
            $encrypted_file = $_FILES['docUpload']['tmp_name'];            
            if(move_uploaded_file($encrypted_file,$target))
            {
                $document = $_FILES['docUpload']['name'];  
                $sopData["description"] = $_POST["docDescription"];
                $sopData["resource_id"] = (int)$_POST["asset_id"];
                $sopData["source"] = $_POST["source_type"];
                $result = $objAssets->addAssetDoc($sopData["description"],$sopData["resource_id"],$document,'SOP', $sopData["source"]);                
            }          
        self::material_view_sop();
    
    }
    public static function post_risk_assessment_material()
    {
        global $objRiskAssessment;
        if(isset($_POST['addRiskAssessmentBtn']))
        {
            $riskAssess = array();
            $riskAssess["source_id"] = $_POST["asset_id"];
            $riskAssess["group_id"] = $_POST["risk_group"];
            $riskAssess["hazard_type"] = $_POST["harzard_type"];
            $riskAssess["hazard_description"] = $_POST["hazard_description"];
            $riskAssess["risk_type"] = $_POST["risk_type"];
            $riskAssess["risk_description"] = $_POST["risk_description"];
            $riskAssess["pscore"] = $_POST["pscore"];
            $riskAssess["control_type"] = $_POST["control_type"];
            $riskAssess["control_description"] = $_POST["control_description"];
            $riskAssess["is_control_in_effect"] = $_POST["controlInEffect"];
            $riskAssess["reduced_by"] = $_POST["controlYesReduction"];
            $riskAssess["new_scrore"] = $_POST["controlYesScore"];
            $riskAssess["source_type"] = $_POST["source_type"];
            $objRiskAssessment->addRiskAssessmentPost($riskAssess);
        }
        self::material_view_sop();
    }
    public static function post_sheqf_notes_material()
    {
         if(isset($_POST['sheqfNotesBtn']))
        {
            global $objRiskAssessment;
            $data = array();
            $data["notes"] = $_POST["notes"];
            $data["topic"] = $_POST["topic"];
            $data["group_id"] = $_POST["group"];
            $data["source_id"] = $_POST["asset_id"];    
            $data["source_type"] = $_POST["source_type"];    
            $result = $objRiskAssessment->addSheQnotes($data);
        }
        self::material_view_sop();
    }
    //upload_sop_document_material
    public static function material_view_substances()
    {
       global $objMaterial;
       global $objTemplate; 
       $data = array();
       $data["allMaterialTypes"] = $objMaterial->getMaterialTypes();
       $objTemplate->setVariables('title', 'View Material');
       $objTemplate->setView('templates/material', 'material_view_substances', $data);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public static function post_material_category()
    {
        if(isset($_POST["addCategoryBtn"]))
        {
            global $objMaterial;
            $response = $objMaterial->addMaterialCategory($_POST["category"]);
        }
        //return to the category page
        self::getAllSettings();
    }
    public static function post_material_types()
    {
        if(isset($_POST["addTypesBtn"]))
        {
            global $objMaterial;
            $response = $objMaterial->addMaterialTypes($_POST["types"]);
        }
        //return to the category page
        self::getAllSettingsTypes();
    }
    public static function post_material()
    {
         if(isset($_POST["addMaterialBtn"]))
        {
            global $objMaterial;
            $data = array();
            $data["category_id"] = $_POST["categories"];
            $data["type_id"] = $_POST["types"];
            $data["description"] = $_POST["description"];
            $data["inventory_no"] = $_POST["inventoryNo"];
            $data["department_use_id"] = $_POST["departments_selected"];
            $data["manufacture_name"] = $_POST["manufacturer"];
            $response = $objMaterial->addMaterial($data);
        }
        //return to the category page
        self::getAllMaterials();
    }
    public static function view_material_info()
    {
       global $objMaterial;
       global $objTemplate; 
       $data = array();
       $id = (int)$_GET["material_id"];
       $data["AllMaterialInfo"] = $objMaterial->getAllMaterialsInfoById($id);
       $objTemplate->setVariables('title', 'View Material');
       $objTemplate->setView('templates/material', 'material_view_info_tpl', $data);
    }
    public static function material_view_supply_usage()
    {
       global $objMaterial;
       global $objSupplier;  
       global $objTemplate; 
       $data = array();
       $id = (int)$_GET["material_id"];
       $data["AllMaterialUsage"] = $objMaterial->getAllMaterialsViewUsage($id);
       $data['allSupplier'] = $objSupplier->getAllSuppliers($_SESSION['branch_id']); 
       $objTemplate->setVariables('title', 'View Material');
       $objTemplate->setView('templates/material', 'material_view_supply_usage', $data);
    }
    public static function post_material_view_usage()
    {
        //tbl_material_usage
        if(isset($_POST["addTotalUsageBtn"]))
        {
            global $objMaterial;
            $data = array();
            $data["material_id"] = $_POST["material_id"];
            $data["usage_total_measurement"] = $_POST["totalMeasurementPerMonth"];
            $data["usage_units_pm"] = $_POST["unitsPerMonth"];
            $data["minimum_total_measurement"] = $_POST["totalMeasurementStockLevel"];
            $data["minimum_units"] = $_POST["minimum_units"];
            $data["suppliers"] = $_POST["suppliers_selected"];
            $data["supplier_delivery_days"] = $_POST["delivery_days"];
            $data["supplier_cost_per_unit_inc_vat"] = $_POST["cost"];            
            $response = $objMaterial->addMaterialUsage($data);
        }
        self::material_view_supply_usage();
    }
    ////////////////////////// Remove this shit below !!!!!!!!!!!! ////////////////////////////////////////////
    public static function view() 
    {
        global $objMaterial;
        global $objTemplate;
        global $objAssets;
        global $objCompanies;
        global $objSupplier;
        $data =  array();
        
        $data['materialTypes'] = $objAssets->getAssetType();
        $data['materialCategory'] = $objAssets->getCategoryType();
        $data['materialDepartment'] = $objCompanies->getBranchDepartments($_SESSION['branch_id']);
        $data['materialSuppliers']= $objSupplier->getAllSuppliers($_SESSION['branch_id']);
        $data['materialUnits'] = $objMaterial->getAllMaterialUnit();
        $data['allMaterials'] = $objMaterial->getAllMaterial();
        $objTemplate->setVariables('title', 'View Material');
        $objTemplate->setView('templates/material', 'material_tpl', $data);
    }
    /**
     * Method to view material
     * @return void
     */
    public static function saveMaterial() 
    {
        global $objMaterial;
        $data =  array();
        $materialDetailsArr =  array();
        
        $materialDetailsArr['category_id'] = $_POST['materialCategory']; 
        $materialDetailsArr['type_id'] = $_POST['materialType'];
        $materialDetailsArr['description'] = $_POST['description'];
        $materialDetailsArr['department_use_id'] = $_POST['departmentsToBeUsed'];
        $materialDetailsArr['department_store_id'] = $_POST['storingDepartment'];
        $materialDetailsArr['manufacture_name'] = $_POST['manufacturer']; 
        $materialDetailsArr['supplier_id'] = $_POST['supplier'];
        $materialDetailsArr['cost'] = $_POST['cost'];
        $materialDetailsArr['cost_unit'] = $_POST['cost']; 
        $materialDetailsArr['average_usage'] = $_POST['usagePerDay'];
        $materialDetailsArr['average_usage_unit'] = $_POST['averageUsageUnit']; 
        $materialDetailsArr['stock_level'] = $_POST['minimumStockLevel'];
        $materialDetailsArr['stock_level_unit'] = $_POST['stockLevelUnit'];
        
        $response = $objMaterial->addMaterial($materialDetailsArr);
        
        if($response){  
            $data['type'] = 'success';
            $data['message'] = 'Your material was added successfully.';
            controller::nextPage('view','material', $data);
            
        }else{
            $data['type'] = 'warning';
            $data['message'] = 'Oops! Something went wrong causing user creation to be unsuccessful. Please log all bug <a href="#" >here</a>';
            controller::nextPage('defaultAction','material', $data);
        } 
    }
    /**
     * Method to view material
     * @return void
     */
    public static function nonComformance() 
    {
        global $objMaterial;
        global $objTemplate;
        $data =  array();
        
        $objTemplate->setVariables('title', 'Material Non Comformance');
        $objTemplate->setView('templates/material', 'material_nonconformance_tpl', $data);
    }
    /**
     * Method to view material
     * @return void
     */
    public static function supplier() 
    {
        global $objMaterial;
        global $objTemplate;
        $data =  array();
        
        $objTemplate->setVariables('title', 'Material Supplier');
        $objTemplate->setView('templates/material', 'material_suppliers_tpl', $data);
    }
    /**
     * Method to view material
     * @return void
     */
    public static function viewMaterial() 
    {
        global $objMaterial;
        global $objTemplate;
        $data =  array();
        
        $objTemplate->setVariables('title', 'Material Supplier');
        $objTemplate->setView('templates/material', 'material_view_info_tpl', $data);
    }
    /**
     * Method to view material
     * @return void
     */
    public static function editMaterial() 
    {
        global $objMaterial;
        global $objTemplate;
        $data =  array();
        
        $objTemplate->setVariables('title', 'Material Supplier');
        $objTemplate->setView('templates/material', 'material_edit_info_tpl', $data);
    }
}