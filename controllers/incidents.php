<?php

/**
 * Controller class containing methods to process all company related actions
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

//Include the base controller
include_once 'controller.php';

//Include the companies class
//include_once(ROOT.'/classes/companies_class.php');

class incidents extends controller
{
    /**
     * 
     * Method to load all incidents Template
     */
    
    public static function incidentManagement() 
    {
        global $objTemplate;
        global $objIncidents;
        $data = array();
        
        $data['getAllincident'] = $objIncidents->getAllIncidents();
        
        $objTemplate->setVariables('title', 'Incident management');
        $objTemplate->setView('templates', 'incident/incident_tpl', $data);
        
    }
    
    public static function view() 
    {
        global $objTemplate;
        global $objIncidents;
        global $objAssetinvolved;
        global $objThirdpartyasset;
        global $objPersoninvolved;
        global $objWitnessinvolved;


        $data = array();

        if($_SESSION['incidentid'] == null){
           $_SESSION['incidentid'] = $_GET['id'];
        }
        
        $data['viewincident'] = $objIncidents->getIncident($_GET['id']);
        $data['assetInvolved'] = $objAssetinvolved->getAssetInvolved($_GET['id']);
        $data['thirdpartyAssetInvolved'] = $objThirdpartyasset->getAllThirdPartyAsset($_GET['id']);
        $data['authorities'] = $objIncidents->getAllAuthorities();
        $data['personInvolved'] = $objPersoninvolved->getAllPersonInvolved($_GET['id']);
        
        $data['employee'] = $objPersoninvolved->PersonInvolvedDetails($_GET['id'], 1);
        $data['customer'] = $objPersoninvolved->PersonInvolvedDetails($_GET['id'], 2);
        $data['serviceprovider'] = $objPersoninvolved->PersonInvolvedDetails($_GET['id'], 3);
        $data['supplier'] = $objPersoninvolved->PersonInvolvedDetails($_GET['id'], 4);
        $data['visitor'] = $objPersoninvolved->PersonInvolvedDetails($_GET['id'], 5);
        $data['publicperson'] = $objPersoninvolved->PersonInvolvedDetails($_GET['id'], 6);
        
        $data['witnessEmployee'] = $objWitnessinvolved->witnessInvolvedDetails($_GET['id'], 1);
        $data['witnessCustomer'] = $objWitnessinvolved->witnessInvolvedDetails($_GET['id'], 2);
        $data['witnessServiceprovider'] = $objWitnessinvolved->witnessInvolvedDetails($_GET['id'], 3);
        $data['witnessSupplier'] = $objWitnessinvolved->witnessInvolvedDetails($_GET['id'], 4);
        $data['witnessVisitor'] = $objWitnessinvolved->witnessInvolvedDetails($_GET['id'], 5);
        $data['witnessPublicperson'] = $objWitnessinvolved->witnessInvolvedDetails($_GET['id'], 6);
        
        $objTemplate->setVariables('title', 'Incident management');
        $objTemplate->setView('templates', 'incident/incident_view_tpl', $data);
        
    }
    /**
     * Method to edit incident
     */
    public static function editIncident() 
    {
        global $objTemplate;
        global $objIncidents;
        $data = array();
        
        $objTemplate->setVariables('title', 'Incident management');
        $objTemplate->setView('templates', 'incident/incident_edit_tpl', $data);
        
    }
    /**
     * 
     * Method to load incidents investigation Template
     */
    public static function incidentInvestigation() 
    {
        global $objTemplate;
        
        $objTemplate->setVariables('title', 'Incident management');
        $objTemplate->setView('templates', 'incident/incident_active_investigations_tpl');
    }
    /**
     * 
     * Method to load incident sign off Template
     */
    public static function incidentSignoff() 
    {
        global $objTemplate;
        
        $objTemplate->setVariables('title', 'Incident management');
        $objTemplate->setView('templates', 'incident/incident_signed_off_tpl');
    }
    /**
     * Method to load incident report Template 
     */
    public static function reportIncident() 
    {
        global $objTemplate;
        global $objIncidents;
        global $objDepartment;
        global $objCompanies;
        global $objEmployee;
        global $objAssets;
        global $objUsers;
        global $objClient;
        global $objService;
        
        $data = array();
        
        $_SESSION['incidentid'] = null; 
                
        $data["getAllIncidentCategory"] = $objIncidents->getIncidentCategory();
        $data['getAllAssetType'] = $objAssets->getAssetType();
        $data['getAllAssetCategory'] = $objAssets->getCategoryType();
        $data["getAllDepartment"] = $objDepartment->getDepartmentByCompany($_SESSION['company_id']);
        $data['serviceTypes'] = $objService->getAllServiceTypes();
        $data['companyTypes'] = $objClient->getAllCompanyTypes();
        $data['gender'] = $objClient->getClientContactGender();
        
        $userType = 5;
        
        $data['companyBranches'] = $objCompanies->getCompanyBranches($_SESSION['company_id']);
        $data['allEmployeeFields'] = $objEmployee->getEmployeeFormDetails($userType);
        $data['gender'] = $objEmployee->getEmployeeGender();
        $data['ethnic'] = $objEmployee->getEmployeeRace();
        
        $objTemplate->setVariables('title', 'Report Incident');
        $objTemplate->setView('templates', 'incident/incident_report_tpl', $data);
    }
    /**
     * Method to save the incident
     */
    public static function showIncidentNumber() 
    {
        global $db;
        global $objIncidents;
        
        $data = array();
        $incident_number = $objIncidents->getIncidentNumber();
        echo json_encode($incident_number);
    }
    /**
     * Method to save the incident
     */
    public static function addIncident() 
    {
        global $objIncidents;
        
        $incidentDetailsArr = array(
            'incidentTask' => null,
            'incidentDescription' => null,
            'incidentAuthorities' => null,
            'incidentInvestigator' => null,
            'incidentincidentDate' => null
        );
        $incidentDetailsArr['incidentCategory'] = $_POST['incidentCategory'];
        $incidentDetailsArr['incidentType'] = $_POST['incidentType'];
        $incidentDetailsArr['incidentDate'] = $_POST['incidentDate'];
        $incidentDetailsArr['incidentDepartment'] = $_POST['incidentDepartment'];
        $incidentDetailsArr['incidentDepartmentManager'] = $_POST['incidentDepartmentManager'];
        if(isset($_POST['otherincidentShift'])){
           $incidentDetailsArr['incidentShift'] = $_POST['otherincidentShift'];  
        }else{
           $incidentDetailsArr['incidentShift'] = $_POST['incidentShift'];  
        }
        $incidentDetailsArr['incidentReportedPerson'] = $_POST['reportedPersonID'];
        
        $response = $objIncidents->addIncident($incidentDetailsArr);
        
        echo json_encode($response);
    }
    /**
     * Method to update incident
     */
    public static function updateIncident() 
    {
        global $objIncidents; 
        
        $incidentDetailsArr['incidentCategory'] = $_POST['incidentCategory'];
        $incidentDetailsArr['incidentType'] = $_POST['incidentType'];
        $incidentDetailsArr['incidentDate'] = $_POST['incidentDate'];
        $incidentDetailsArr['incidentDepartment'] = $_POST['incidentDepartmentManager'];
        $incidentDetailsArr['incidentDepartmentManager'] = $_POST['incidentDepartmentManager'];
        $incidentDetailsArr['incidentShift'] = $_POST['incidentShift'];
        $incidentDetailsArr['incidentReportedPerson'] = $_POST['reportedPersonID'];
        $incidentDetailsArr['incidentTask'] = $_POST['incidentTask'];
        $incidentDetailsArr['incidentDescription'] = $_POST['incidentDescription'];
        $incidentDetailsArr['incidentAuthorities'] = implode(',', $_POST['reportToAuthorities']);
        $incidentDetailsArr['incidentInvestigator'] = $_POST['assignInvestigator'];
        $incidentDetailsArr['incidentincidentDate'] = $_POST['investigationDate'];
        
        $response = $objIncidents->editIncident($incidentDetailsArr);
        
        if($response){
            controller::nextPage('incidentManagement','incidents');
        }
    }
    /**
     * Method to save the incident
     */
    public static function savePersonInvolved() 
    {
        global $objPersoninvolved;
        
        $personInvolvedArr = array();
        $personInvolvedArr['person_involved_id'] = $_POST['personInvolvedID'];
        $personInvolvedArr['person_involved_type'] = $_POST['personType'];
        $personInvolvedArr['injured_or_ill'] = $_POST['personInjured'];

        if($_POST['personInjured'] == 1){
            $personInvolvedArr['part_affected'] = implode(',', $_POST['effectedBodyPart']);
            $personInvolvedArr['effect_person'] = implode(',', $_POST['effectPerson']);
            $personInvolvedArr['disease_description'] = $_POST['descriptionDisease'];
            $personInvolvedArr['hours_expected'] = $_POST['daysExpected']; 
        }else{
            $personInvolvedArr['part_affected'] = null;
            $personInvolvedArr['effect_person'] = null;
            $personInvolvedArr['disease_description'] = null;
            $personInvolvedArr['hours_expected'] = null;
        }

        $response = $objPersoninvolved->addPersonInvolved($personInvolvedArr);

    }
    /**
     * Method to save the incident
     */
    public static function saveAssetInvolved() 
    {
        global $objAssetinvolved;
        
        $assetInvolvedArr = array();
        $assetInvolvedArr['asset_id'] = $_POST['assetID'];
        $assetInvolvedArr['involvementType'] = $_POST['involvementType'];
        $assetInvolvedArr['effectonAsset'] = $_POST['effectAsset'];
        $assetInvolvedArr['incidentCause'] = implode(',', $_POST['cause']);
        $assetInvolvedArr['downtowntime'] = $_POST['numberHours'];

        $response = $objAssetinvolved->addAssetInvolved($assetInvolvedArr);

    }
    
    /**
     * Method to save winess
     */
    public static function saveWitness() 
    {
        global $objWitnessinvolved;
        
        $witnessInvolvedArr = array();
        $witnessInvolvedArr['witness_id'] = $_POST['witnessID'];
        $witnessInvolvedArr['witnessType'] = $_POST['witnessIncidentType'];
        $response = $objWitnessinvolved->addWitnessInvolved($witnessInvolvedArr);
    }
     /**
     * Method to save public person
     */
    public static function savePublicPerson() 
    {
        global $objPublicperson;
        
        $publicPersonArr = array();
        $publicPersonArr['firstname'] = strtok($_POST['publicFullname'], ' ');
        $publicPersonArr['lastname'] = strstr($_POST['publicFullname'], ' ');
        $publicPersonArr['id_number'] = $_POST['publicIDNumber'];
        $publicPersonArr['contact_number'] = $_POST['publicContactNumber'];
        $publicPersonArr['email'] = $_POST['publicEmail'];
        $publicPersonArr['company'] = $_POST['publicCompanyName'];
        $response = $objPublicperson->addPublicPerson($publicPersonArr);
        echo $response;
    }
    /**
     * Method to save public person
     */
    public static function saveVisitor() 
    {
        global $objVisitor;
        
        $visitorDetsArr = array();
        $visitorDetsArr['firstname'] = strtok($_POST['visitorFullname'], ' ');
        $visitorDetsArr['lastname'] = strstr($_POST['visitorFullname'], ' ');
        $visitorDetsArr['id_number'] = $_POST['visitorIDNumber'];
        $visitorDetsArr['contact_number'] = $_POST['visitorContactNumber'];
        $visitorDetsArr['email'] = $_POST['visitorEmail'];
        $visitorDetsArr['company'] = $_POST['visitorCompanyName'];
        $response = $objVisitor->addVisitor($visitorDetsArr);
        echo $response;
    }
    /**
     * Method to save the incident
     */
    public static function saveThirdPartyAsset() 
    {
        global $objThirdpartyasset;
        
        $thirdPartyDetArr = array();

        $thirdPartyDetArr['assetType'] = $_POST['partyResourceType'];
        $thirdPartyDetArr['description'] = $_POST['partyResourceDescription'];
        $thirdPartyDetArr['companyassetnumber'] = $_POST['partyResourceNumber'];
        $thirdPartyDetArr['effectonasset'] = $_POST['partyResourceEffectAsset'];
        
        var_dump($thirdPartyDetArr);
        
        $response = $objThirdpartyasset->addThirdPartyAsset($thirdPartyDetArr);
    }
    /**
     * Method to save public person witness
     */
    public static function saveWitnessPublic() 
    {
        global $objPublicperson;
        
        $publicDetailsArr = array();
        $publicDetailsArr['firstname'] = strtok($_POST['witnessPublicFullname'], ' ');
        $publicDetailsArr['lastname'] = strstr($_POST['witnessPublicFullname'], ' ');
        $publicDetailsArr['id_number'] = $_POST['witnessPublicIDNumber'];
        $publicDetailsArr['contact_number'] = $_POST['witnessPublicContactNumber'];
        $publicDetailsArr['email'] = $_POST['witnessPublicEmail'];
        $publicDetailsArr['company'] = $_POST['witnessPublicCompanyName'];
        $response = $objPublicperson->addPublicPerson($publicDetailsArr);
        echo $response;
    }
     /**
     * Method to save public person
     */
    public static function saveWitnessVisitor() 
    {
        global $objVisitor;
        
        $visitorDetsArr = array();
        $visitorDetsArr['firstname'] = strtok($_POST['witnessVisitorFullname'], ' ');
        $visitorDetsArr['lastname'] = strstr($_POST['witnessVisitorFullname'], ' ');
        $visitorDetsArr['id_number'] = $_POST['witnessVisitorIDNumber'];
        $visitorDetsArr['contact_number'] = $_POST['witnessVisitorContactNumber'];
        $visitorDetsArr['email'] = $_POST['witnessVisitorEmail'];
        $visitorDetsArr['company'] = $_POST['witnessVisitorCompany'];
        $response = $objVisitor->addVisitor($visitorDetsArr);
        echo $response;
    }
    /**
     * Method to display all reported person fullname
     */
    public static function showReportedPerson(){
        global $objIncidents;
        $response = $objIncidents->showReportedPerson();
        echo json_encode($response);
    }
    /**
     * 
     * @global type $objAssetinvolved
     */
    public static function showAssetInvolved(){
        global $objAssetinvolved;
        $response = $objAssetinvolved->getAssetInvolved($_SESSION['incidentid']);
        echo json_encode($response);
    }
    /**
     * 
     * @global type $objAssetinvolved
     */
    public static function showThirdPartyAsset(){
        global $objThirdpartyasset;
        $response = $objThirdpartyasset->getAllThirdPartyAsset($_SESSION['incidentid']);
        echo json_encode($response);
    }
    /**
     * 
     * @global type $objCompanies
     */
    public static function getAllCustomerCompanies(){
        global $objClient;
        $response= $objClient->getCompanyNameClients();
        echo json_encode($response);
    }
    /**
     * 
     * @global type $objClient
     */
    public static function getAllProviderCompanies(){
        global $objProvider;
        $response= $objProvider->getProviderCompanies();
        echo json_encode($response);
    }
        /**
     * 
     * @global type $objClient
     */
    public static function getAllSupplierCompanies(){
        global $objSupplier;
        $response= $objSupplier->getSupplierCompanies();
        echo json_encode($response);
    }
    /**
     * 
     * @global type $objIncidents
     */
    public static function getIncidentTypesByCategoryId(){
        global $objIncidents;
        $response= $objIncidents->getIncidentTypesByCategoryId($_POST['incidentCategory']);
        echo json_encode($response);
    }
}