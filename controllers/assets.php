<?php
/**
 * Controller class containing methods to process all company related actions
 * 
 * @package sheqonline
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

//Include the base controller
include_once 'controller.php';

class assets extends controller
{

    /**
     * var $objAsset The asset object
     * @access private
     */
    public function __construct()  
    {
        
    }
    /**
     * Default method
     * @return void
     */
    public static function defaultAction()
    {
        self::nextPage('assetmanagement','assets');
    }
    /**
     * Method to view asset
     * @global $db
     * @global $objTemplate
     * @global $objAssets
     * @return void
     */
    public static function view() 
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();
        
        $data['viewassets'] = $objAssets->getoneAsset($_GET['id']);
        $data['assetQuestions'] = $objAssets->getAssetQuestions();
        
        $objTemplate->setVariables('title', 'View Assets');
        $objTemplate->setView('templates/assets', 'view_asset_tpl', $data); 
    }
    /**
     * Method to view assets
     * @global $db
     * @global $objTemplate
     * @global $objAssets
     * @return void
     */
    public static function statutory_maintenance_plan()
    {
        global $objTemplate;
        global $objAssets;
        global $objDepartment;
        $data =  array();
        $data['assets'] = $objAssets->getAllAssetStatutory(); 
        $data["allDepartments"] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data["statutory_assets"] = $objAssets->getStatutoryAssets();
        $objTemplate->setVariables('title', 'View Assets');
        $objTemplate->setView('templates/assets', 'statutory_maintenance_plan', $data); 
    }
    public static function statutory_plan()
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();
        $id = (int)$_GET["edit"];
        $data['assets'] = $objAssets->getAssetStatutoryById($id);        
        $data["allStatutoryPlans"] = $objAssets->getStatutoryPlansForAsset($id);
        $objTemplate->setVariables('title', 'View Assets');
        $objTemplate->setView('templates/assets', 'statutory_plan', $data); 
    }
   public static function statutory_upcoming()
    {
        global $objTemplate;
        global $objAssets;
        global $objDepartment;
        global $objEmployee;
        global $objSupplier;
        
        $data =  array();
        $data['assets'] = $objAssets->getAllAsset(); 
        $data["allDepartments"] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data['allEmployees'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data["allUpcomingStatutory"] = $objAssets->getUpcomingStatutory();
        $data['allSupplier'] = $objSupplier->getAllSuppliers($_SESSION['branch_id']); 
        $objTemplate->setVariables('title', 'View Assets');
        $objTemplate->setView('templates/assets', 'statutory', $data); 
    }
    public static function job_listing()
    {
         global $objTemplate;
        global $objAssets;
        global $objDepartment;
        global $objEmployee;
        global $objSupplier;
        global $objJobs;
        
        $data =  array();
        $data['assets'] = $objAssets->getAllAsset(); 
        $data["allDepartments"] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data['allEmployees'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data["allStatutoryJobs"] = $objJobs->getAllStatutoryJobs();
        $objTemplate->setVariables('title', 'View Assets');
        $objTemplate->setView('templates/assets', 'statutory_job', $data); 
    }    
    public static function create_statutory_job()
    {
        if(isset($_POST["createJobBtn"]))
        {
            global $objJobs;
            $data = array();
             
            $data["source_type"] = "Statutory";        
            $data["job_description"] = $_POST["description"];            
            $data["start_date"] = $_POST["scheduledStartDateTime"];  
            $data["end_date"] = $_POST["endDateTime"];  
            $data["responsible_persons"] = $_POST["person_responsible_selected"];  
            $data["department_involved"] = $_POST["departments_selected"];  
            $data["maintenance_type"] = $_POST["maintenanceType"];  
            $data["job_details"] = $_POST["fullJobDescription"];  
            $data["risk_level"] = $_POST["risk_level"];  
            $data["hazard_types"] = $_POST["hazardTypes"];  
            $data["supplier_id"] = $_POST["companyName"];  
            $data["resources_used"] = $_POST["resources_used"];  
            $data["parts_used"] = $_POST["parts_used"];                          
            $data["approval_required"] = $_POST["approval_required"];   
            
            //foreach item which has been checked do what needs to be done
             $assets = $_POST["upcoming"];
             $hasImported = false;
             foreach($assets as $asset)
             {
                    //call the insert method             
                    $data["source_id"] = $asset;                          
                    $return = $objJobs->hasCreatedJob($data);
                    if($return)
                    {
                        $hasImported = true;
                    }
            }            
            if($hasImported)
            {
                //redirect to the jobs table
                self::job_listing();
                
            }
            else
            {
                //display error and stay on the same page 
                self::statutory_upcoming();
            }
        }
    }
    public static function schedule_or_reschedule_job()
    {
        if(isset($_POST["scheduleBtn"]))
        {
            global $objAssets;
            $data = array();
            $data[""] = $_POST["startingDateTime"];
            $data[""] = $_POST["endDate"];
            $data[""] = $_POST["department"];
            $data[""] = $_POST["manager"];
            $data[""] = $_POST["sheqfManager"];
            $data[""] = $_POST["source_id"];
        }
    }
    public static function signOffjob()
    {
         if(isset($_POST[""]))
        {
            
        }
    }
    public static function assetmanagement() 
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();
        $data['assets'] = $objAssets->getAllAsset();        
        $objTemplate->setVariables('title', 'View Assets');
        $objTemplate->setView('templates/assets', 'asset_management_tpl', $data);   
    }
    public static function viewAssetStatutory()
    {
        //
        global $objTemplate;
        global $objAssets;
        $data =  array();
        $data['assets'] = $objAssets->getAllAssetsNotImported();        
        $objTemplate->setVariables('title', 'View Assets');
        $objTemplate->setView('templates/assets', 'statutory_maintenance_import', $data);   
    }    
    //post asset
    public static function importAssets()
    {
        global $objAssets;
        $hasImported = false;
        if(isset($_POST["importAssets"]))
        {
            if(!empty($_POST["assetname"]))
            {
                $assets = $_POST["assetname"];
                $hasImported = false;
                foreach($assets as $asset)
                {
                    //call the insert method
                    $objAssets->addAssetToStatutory($asset);
                    $hasImported = true;
                }
                if($hasImported == true)
                {
                    self::statutory_maintenance_plan();
                }                
            }
        }
        if($hasImported == false)
        {
            self::viewAssetStatutory();
        }
    }
    public static function postStatutoryPlan()
    {
        if(isset($_POST["addBtn"]))
        {
             global $objAssets;
             $data = array();             
             $data["statutory_id"] = $_POST["txt_id"];
             $data["int_ex"] = $_POST["internalExternal"];
             $data["type_id"] = $_POST["typeService"];
             $data["description"] = $_POST["description"];
             $data["notes"] = $_POST["notes"];
             $data["frequency"] = $_POST["frequency"];
             $data["due_date"] = $_POST["dueDate"];
             $response = $objAssets->add_statutory_maintenance_plan($data);
             if($response)
             {
                 self::statutory_plan();  
             }
             else
             {
                 self::statutory_plan();
             }
        }
    }
    /**
     * Method to show asset types
     * @global $objAssets
     * @return $array
     */
    public static function showAssetTypes()
    {
        global $objAssets;  
        $data = array();
        $getAllAssetTypesByCategory = $objAssets->getAssetTypeByCategory($_POST["categoryId"]);
        
        if(!empty($getAllAssetTypesByCategory)){
            foreach($getAllAssetTypesByCategory as $assetTypeDetailsArr){
                $data[] = array("id"=>$assetTypeDetailsArr["id"], "name"=>$assetTypeDetailsArr["name"]);
            }  
        }else{
                $data[] = array("id"=>"0", "name"=>"No Types Found");
        }
        //return the result in jason
        echo json_encode($data);
    }
    /**
     * Method to display template
     * @global $objTemplate
     * @global $objAssets
     * @global $objCompanies
     */
    public static function addAsset() 
    {
        global $objTemplate;
        global $objAssets;
        global $objCompanies;
        $data =  array();
        
        $data['assetType'] = $objAssets->getAssetType();
        $data['assetCategory'] = $objAssets->getCategoryType();
        $data['department'] = $objCompanies->getBranchDepartments($_SESSION['branch_id']);
        $data['assetQuestions'] = $objAssets->getAssetQuestions();
        
        $objTemplate->setVariables('title', 'Add Assets');
        $objTemplate->setView('templates/assets', 'add_asset_tpl', $data);   
    }
    /**
     * Method to add asset documents
     * @global $objTemplate
     * @global $objAssets 
     * @global $objCompanies
     * @return void
     */
    public static function addAssetDocument() 
    {
        global $objTemplate;
        global $objAssets;
        global $objCompanies;
        $data =  array();
        
        $data['editassets'] = $objAssets->getoneAsset($_REQUEST['id']);
        $data['assetType'] = $objAssets->getAssetType();
        $data['editCategoryType'] = $objAssets->getCategoryType();
        $data['department'] = $objCompanies->getBranchDepartments($_SESSION['branch_id']);
        $data['assetDocuments'] = $objAssets->getAssetDocuments($_REQUEST['id']);
        $data['assetQuestions'] = $objAssets->getAssetQuestions();

        $objTemplate->setVariables('title', 'Asset Document');
        $objTemplate->setView('templates/assets', 'add_asset_document_tpl', $data);
  
    }
    /**
     * Method to save asset
     * @global $objAssets
     * @return void
     */
    public static function saveAsset() 
    {
        global $objAssets;
        $assetDetailsArr = array();
        $response = array();
        $data = array();
        $data['assetQuestions'] = $objAssets->getAssetQuestions();
        
        $assetDetailsArr['departmentid'] = $_POST['departmentid']; 
        $assetDetailsArr['assetcategoryid'] = $_POST['categoryType'];
        $assetDetailsArr['assettypeid'] = $_POST['assetType'];
        $assetDetailsArr['description'] = $_POST['description'];
        $assetDetailsArr['companyassetnumber'] = $_POST['assetNumber'];
        $assetDetailsArr['manufacture'] = $_POST['manufacturer']; 
        $assetDetailsArr['serialnumber'] = $_POST['inventoryNumber'];
        $assetDetailsArr['datepurchased'] = $_POST['datePurchased'];
        $assetDetailsArr['cost'] = $_POST['cost']; 
        $assetDetailsArr['notes'] = $_POST['notes'];
        
        if(count($data['assetQuestions']) > 0){
            foreach($data['assetQuestions'] as $fieldsNames){
                $fieldname = $fieldsNames['name'];
                $answerslistArr[] = $_POST["$fieldname"];
            }     
        } 
        $assetDetailsArr['answers'] = implode(',', $answerslistArr);  
        $response = $objAssets->addAsset($assetDetailsArr);
        
        if($response['response']){  
            $data['type'] = 'success';
            $data['id'] = $response['asset_id'];
            $data['message'] = 'Your resource is added successfully.';
            controller::nextPage('addAssetDocument','assets', $data);  
        }else{
            $data['type'] = 'warning';
            $data['message'] = 'Oops! Something went wrong causing user creation to be unsuccessful. Please log all bug <a href="#" >here</a>';
            controller::nextPage('assetmanagement','assets', $data);
        } 
    }
    /**
     * Method to save document 
     * @global $objAssets
     * @return void
     */
    public static function saveAssetDocument() 
    {
        global $objAssets;
        $data = array();
        
        $description = $_POST['docDescription'];
        $response = $objAssets->addAssetDocument($_POST['assetid'], $description);
        
        if($response){  
            $data['type'] = 'success';
            $data['id'] = $_POST['assetid'];
            $data['message'] = 'Your asset document is added successfully.';
            controller::nextPage('editAsset','assets', $data);  
        }else{
            $data['type'] = 'warning';
            $data['message'] = 'Oops! Something went wrong causing user creation to be unsuccessful. Please log all bug <a href="#" >here</a>';
            controller::nextPage('assetmanagement','assets', $data);
        } 
    }
    /**
     * 
     * @global $objAssets $objAssets
     */
    public static function saveAssetInvolved() 
    {
        global $objAssets;
        $assetDetailsArr = array();

        $assetDetailsArr['departmentid'] = $_POST['departmentid']; 
        $assetDetailsArr['assetcategoryid'] = $_POST['categoryType'];
        $assetDetailsArr['assettypeid'] = $_POST['assetType'];
        $assetDetailsArr['name'] = $_POST['resourceName'];
        $assetDetailsArr['companyassetnumber'] = $_POST['companyAssetNumber'];
        $assetDetailsArr['manufacture'] = null; 
        $assetDetailsArr['serialnumber'] = null;
        $assetDetailsArr['datepurchased'] = null;
        $assetDetailsArr['expiryDate'] = null;
        $assetDetailsArr['cost'] = null; 
        $assetDetailsArr['description'] = null;
        $assetDetailsArr['answers'] = null;
        $response = $objAssets->addAsset($assetDetailsArr);
    }
    /**
     * 
     */
    public static function saveThirdPartyAsset(){
        global $objAssets;
        $thirdpartyAssetArr = array();
        
        $thirdpartyAssetArr['assetType'] = $_POST['partyResourceType'];
        $thirdpartyAssetArr['description'] = $_POST['partyResourceDescription'];
        $thirdpartyAssetArr['companyassetnumber'] = $_POST['partyResourceNumber'];
        $thirdpartyAssetArr['effectonasset'] = implode(',', $_POST['partyResourceEffectAsset']);
        $response = $objAssets->addThirdPartyAsset($thirdpartyAssetArr);
    }
    
    /**
     * Method to edit asset details
     * @global $objTemplate
     * @global $objAssets 
     * @global $objCompanies
     * @return void
     */
    public static function editAsset() 
    {
        global $objTemplate;
        global $objAssets;
        global $objCompanies;
        $data =  array();
        
        $data['editassets'] = $objAssets->getoneAsset($_REQUEST['id']);
        $data['assetType'] = $objAssets->getAssetType();
        $data['editCategoryType'] = $objAssets->getCategoryType();
        $data['department'] = $objCompanies->getBranchDepartments($_SESSION['branch_id']);
        $data['assetDocuments'] = $objAssets->getAssetDocuments($_REQUEST['id']);
        $data['assetQuestions'] = $objAssets->getAssetQuestions();
        
        if(isset($_REQUEST['type']) && isset($_REQUEST['message'])){
            
            $data['notification']['type'] = urldecode($_REQUEST['type']);
            $data['notification']['message'] = urldecode($_REQUEST['message']);
            
            $objTemplate->setVariables('title', 'Edit Assets');
            $objTemplate->setView('templates/assets', 'edit_asset_tpl', $data);
        } else {
            $objTemplate->setVariables('title', 'Edit Assets');
            $objTemplate->setView('templates/assets', 'edit_asset_tpl', $data);
        }    
    }
    
    /**
     * Method to save edit asset
     * @return void
     */
    public static function saveEditAsset() 
    {
        global $objAssets;
        $assetDetailsArr = array();
        $data = array();
        $answerslistArr = array();
        
        if($_POST['submit']=="cancel"){
            controller::nextPage('assetmanagement','assets');
        }else{
            
            $data['assetQuestions'] = $objAssets->getAssetQuestions();            
            
            $assetDetailsArr['id'] = $_POST['id'];
            $assetDetailsArr['departmentid'] = $_POST['departmentid']; 
            $assetDetailsArr['assetcategoryid'] = $_POST['categoryType'];
            $assetDetailsArr['assettypeid'] = $_POST['assetType'];
            $assetDetailsArr['name'] = $_POST['resourceName'];
            $assetDetailsArr['companyassetnumber'] = $_POST['companyAssetNumber'];
            $assetDetailsArr['manufacture'] = $_POST['manufacture']; 
            $assetDetailsArr['serialnumber'] = $_POST['serialNumber'];
            $assetDetailsArr['datepurchased'] = $_POST['datePurchased'];
            $assetDetailsArr['expiryDate'] = $_POST['dateExpired'];
            $assetDetailsArr['cost'] = $_POST['cost']; 
            $assetDetailsArr['description'] = $_POST['description'];
            
            foreach($data['assetQuestions'] as $fieldsNames){
                $fieldname = $fieldsNames['name'];
                $answerslistArr[] = $_POST["$fieldname"];
            } 
            $assetDetailsArr['answers'] = implode(',', $answerslistArr);
            
            $response = $objAssets->editAsset($assetDetailsArr); 
            
            if($response){  
                $data['type'] = 'success';
                $data['message'] = 'Your details are edited';
                controller::nextPage('assetmanagement','assets', $data);  
            }else{
                $data['type'] = 'warning';
                $data['message'] = 'Oops! Something went wrong causing user creation to be unsuccessful. Please log all bug <a href="#" >here</a>';
                controller::nextPage('assetmanagement','assets', $data);
            } 
        }   
    }
    /**
     * Method to delete assets
     */
    public static function deleteAsset() 
    {
        global $objAssets;
        $data = array();
        $response = $objAssets->deleteAsset($_GET['id']);
        if($response){  
            $data['type'] = 'success';
            $data['message'] = 'Your asset is deleted';
            controller::nextPage('assetmanagement','assets', $data=[]);  
        }else{
            $data['type'] = 'warning';
            $data['message'] = 'Oops! Something went wrong causing user creation to be unsuccessful. Please log all bug <a href="#" >here</a>';
            controller::nextPage('assetmanagement','assets', $data);
        }   
    }
    /**
     * Method to activate assets
     */
    public static function activateAsset() 
    {
        global $objAssets;
        $data = array();
        
        if($_POST['submit']=="cancel"){
            controller::nextPage('assetmanagement','assets');
        }else{
            $response = $objAssets->activateAsset($_GET['id']);
            if($response){  
                $data['type'] = 'success';
                $data['message'] = 'Your asset is activated';
                controller::nextPage('assetmanagement','assets', $data=[]);  
            }else{
                $data['type'] = 'warning';
                $data['message'] = 'Oops! Something went wrong causing user creation to be unsuccessful. Please log all bug <a href="#" >here</a>';
                controller::nextPage('assetmanagement','assets', $data);
            } 
        }   
    }
    /**
     * Method to deactivate assets
     */
    public static function deactivateAsset() 
    {
        global $objAssets;
        $data = array();
        
        if($_POST['submit']=="cancel"){
            controller::nextPage('assetmanagement','assets');
        }else{ 
            $response = $objAssets->deactivateAsset($_GET['id']);
            
            if($response){  
                $data['type'] = 'success';
                $data['message'] = 'Your asset is deactivated';
                controller::nextPage('assetmanagement','assets', $data=[]);  
            }else{
                $data['type'] = 'warning';
                $data['message'] = 'Oops! Something went wrong causing user creation to be unsuccessful. Please log all bug <a href="#" >here</a>';
                controller::nextPage('assetmanagement','assets', $data);
            } 
        }   
    }
    /**
     * Method to display template
     */
    public static function viewAdditionalAsset() 
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();
        
        $data['assetType'] = $objAssets->getAssetType();
        $objTemplate->setVariables('title', 'Add Additional Assets');
        $objTemplate->setView('templates/assets', 'add_additional_info_tpl', $data);   
    }
     /**
     * Method 
     */
    public static function statutoryMaintenance() 
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();
        
        $data['assets'] = $objAssets->getAllAsset();
        
        $objTemplate->setVariables('title', 'statutory maintenance');
        $objTemplate->setView('templates/assets', 'asset_statutory_maintenance_tpl', $data);   
    }
    /**
     * Method 
     */
    public static function calibration() 
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();
        
        $data['assets'] = $objAssets->getAllAsset();
         
        $objTemplate->setVariables('title', 'calibration');
        $objTemplate->setView('templates/assets', 'asset_calibration_tpl', $data);   
    }
    /**
     * Method 
     */
    public static function unlistedAsset() 
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();
        
        $objTemplate->setVariables('title', 'Unlisted Resource');
        $objTemplate->setView('templates/assets', 'asset_unlisted_tpl', $data);   
    }
     /**
     * Method 
     */
    public static function nonComformanceHistory() 
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();
        
        $objTemplate->setVariables('title', 'Non Comformance History');
        $objTemplate->setView('templates/assets', 'asset_nonconformance_history_tpl', $data);   
    }
     /**
     * Method 
     */
    public static function settings() 
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();
        
        $objTemplate->setVariables('title', 'Asset Settings');
        $objTemplate->setView('templates/assets', 'asset_settings_tpl', $data);   
    }
    /**
     * Method 
     */
    public static function archive() 
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();
        
        $objTemplate->setVariables('title', 'Archive');
        $objTemplate->setView('templates/assets', 'asset_archive_tpl', $data);   
    }
    //Resources//
    public static function get_resource_info_documents()
    {
         global $objTemplate;
         global $objAssets;
        $data =  array();        
        $objTemplate->setVariables('title', 'Archive');
        $objTemplate->setView('templates/assets', 'resource_info_documents', $data);   
    }
    public static function get_resource_info_competence()
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();        
        $objTemplate->setVariables('title', 'Archive');
        $objTemplate->setView('templates/assets', 'resource_info_competence', $data);   
    }
    public static function get_resource_info_inspected()
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();        
        $objTemplate->setVariables('title', 'Archive');
        $objTemplate->setView('templates/assets', 'resource_info_inspected', $data);  
    }
    public static function get_resource_info_sop()
    {
        global $objTemplate;
        global $objAssets;
        global $objSheqTeam;
        global $objRiskAssessment;
        $data =  array(); 
        $data["hazard"] = $objRiskAssessment->getAllHazard();
        $data["risk"] = $objRiskAssessment->getAllRiskSettings();
        $data["control"] = $objRiskAssessment->getAllControlSettings();
        $data['source'] = $_GET["source_type"];
        $data['sops'] = $objAssets->getSop($_GET['id'],$data['source']);
        $data['sopsDoc'] = $objAssets->getAssetDoc($_GET['id'],'SOP',$data['source']);    
        
        $data['risk_assessment'] = $objRiskAssessment->getRiskAssessment($_GET['id'],$data['source']);
        $data['sheqf_notes'] = $objRiskAssessment->getSHEQFNotes($_GET['id'],$data['source']);
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
        $objTemplate->setVariables('title', 'Archive');
        $objTemplate->setView('templates/assets', 'resource_info_sop', $data);  
    }
    public static function post_sheqf_notes()
    {
        if(isset($_POST['sheqfNotesBtn']))
        {
            global $objRiskAssessment;
            $data = array();
            $data["notes"] = $_POST["notes"];
            $data["topic"] = $_POST["topic"];
            $data["group_id"] = $_POST["group"];
            $data["source_id"] = $_POST["asset_id"];    
            $data["source_type"] = $_POST["source_type"];    
            $result = $objRiskAssessment->addSheQnotes($data);
        }
        self::get_resource_info_sop();
    }
    public static function ajax_get_control_reduction()
    {
        global $objRiskAssessment;
        $reduction = 0;
        if(isset($_GET["control_id"]))
        {
            $control_id = (int)$_GET["control_id"];
            $data = array();
            $data = $objRiskAssessment->getControlReduction($control_id);
            foreach($data as $control)
            {
                $reduction = $control["reduction"];
                break;
            }
        }       
        echo $reduction;
    }
    public static function get_resource_info_maintenance(){
        global $objTemplate;
        global $objAssets;
        $data =  array();        
        $objTemplate->setVariables('title', 'Archive');
        $objTemplate->setView('templates/assets', 'resource_info_maintenance', $data);  
    }
    public static function get_resource_info_noc(){
        global $objTemplate;
        global $objAssets;
        $data =  array();        
        $objTemplate->setVariables('title', 'Archive');
        $objTemplate->setView('templates/assets', 'get_resource_info_noc', $data);  
    }
    public static function get_resource_substance()
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();        
        $objTemplate->setVariables('title', 'Archive');
        $objTemplate->setView('templates/assets', 'resource_info_substance', $data);  
    }
    //post section
    public static function post_sop()
    {
        global $objAssets;
        $sopData = array();
        if(isset($_POST['addSopDocBtn']))
        {
            $sopData["description"] = $_POST["sopDescription"];
            $sopData["resource_id"] = (int)$_POST["asset_id"];
            $sopData["source"] = $_POST["source_type"];
            $result = $objAssets->addSOP($sopData["description"],$sopData["resource_id"],$sopData["source"]);
        }
       self::get_resource_info_sop();
    }
    public static  function upload_sop_document()
    {
        //echo $_FILES['docUpload']['name'];
            $sopData = array();            
            global $objAssets;
            $target = $_SERVER['DOCUMENT_ROOT']."/sheq/frontend/documents/";
            $target = $target . basename($_FILES['docUpload']['name']);
            $encrypted_file = $_FILES['docUpload']['tmp_name'];            
            if(move_uploaded_file($encrypted_file,$target))
            {
                $document = $_FILES['docUpload']['name'];  
                $sopData["description"] = $_POST["docDescription"];
                $sopData["resource_id"] = (int)$_POST["asset_id"];
                $sopData["source"] = $_POST["source_type"];
                $result = $objAssets->addAssetDoc($sopData["description"],$sopData["resource_id"],$document,'SOP', $sopData["source"]);                
            }          
        self::get_resource_info_sop();
    }
    public static function post_risk_assessment()
    {
        global $objRiskAssessment;
        if(isset($_POST['addRiskAssessmentBtn']))
        {
            $riskAssess = array();
            $riskAssess["source_id"] = $_POST["asset_id"];
            $riskAssess["group_id"] = $_POST["risk_group"];
            $riskAssess["hazard_type"] = $_POST["harzard_type"];
            $riskAssess["hazard_description"] = $_POST["hazard_description"];
            $riskAssess["risk_type"] = $_POST["risk_type"];
            $riskAssess["risk_description"] = $_POST["risk_description"];
            $riskAssess["pscore"] = $_POST["pscore"];
            $riskAssess["control_type"] = $_POST["control_type"];
            $riskAssess["control_description"] = $_POST["control_description"];
            $riskAssess["is_control_in_effect"] = $_POST["controlInEffect"];
            $riskAssess["reduced_by"] = $_POST["controlYesReduction"];
            $riskAssess["new_scrore"] = $_POST["controlYesScore"];
            $riskAssess["source_type"] = $_POST["source_type"];
            $objRiskAssessment->addRiskAssessmentPost($riskAssess);
        }
        self::get_resource_info_sop();
    }
}