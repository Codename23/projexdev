<?php
/**
 * Controller class containing methods to process all supplier actions
 * 
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

//Include the base controller
include_once 'controller.php';

class supplier extends controller{
	
	
	
	
	public static function viewAllSuppliers(){

        global $objSupplier;
        global $objTemplate;
	$data = array();

        $branchId = $_SESSION['branch_id'];
        $data['allSuppliers']= $objSupplier->getAllSuppliers($branchId);
	$objTemplate->setVariables('title','Supplier Info');
	$objTemplate->setView('templates/supplier','index_tpl',$data);
	}


    /**
     *Method to add a new supplier
     */
	public static function addSupplier(){
		
		global $objSupplier;
		global $objService;
		global $objAudit;
                global $objTemplate;

		if(!empty($_POST) && isset($_SESSION['branch_id'])){

            $supplierData = array();

            $supplierData['companyName'] = $_POST['companyName'];
            $supplierData['contactPerson'] = $_POST['contactPerson'];
            $supplierData['contactNumber'] = $_POST['contactNumber'];
            $supplierData['email'] = $_POST['email'];
            $supplierData['serviceType'] = $_POST['supplyType'];
            $supplierData['createdBy'] = $_SESSION['user_id'];
            $supplierData['modifiedBy']= $_SESSION['user_id'];
            $supplierData['branchId']  = $_SESSION['branch_id'];
            
            if($_POST['formType'] != 'ajaxForm' ){ 
                $supplierData['ptyNumber'] = $_POST['ptyNumber'];
                $supplierData['vatNumber'] = $_POST['vatNumber'];
                $supplierData['officeNumber'] = $_POST['officeNumber'];
                $supplierData['website'] = $_POST['website'];
                $supplierData['beeCertificate'] = $_POST['beeCertificate'];
                $supplierData['headOfficeDetails'] = $_POST['headOfficeDetails'];
                $supplierData['headOfficeTown'] = $_POST['headOfficeTown'];
                $supplierData['headOfficeProvince'] = $_POST['headOfficeProvince'];
                $supplierData['serviceType'] = $_POST['supplyType'];
                $supplierData['supplyDetails'] = $_POST['supplyDetails'];
                $supplierData['serviceFrequency'] = $_POST['serviceFrequency'];
                $supplierData['uploadDocuments'] = $_POST['uploadDocuments'];
                $supplierData['complianceConstruction'] = $_POST['complianceConstruction'];
                $supplierData['complianceHighRisk'] = $_POST['complianceHighRisk'];
                $supplierData['complianceDescription'] = $_POST['complianceDescription'];
                $supplierData['applicableSupplier'] = $_POST['applicableSupplier'];
                $supplierData['auditType'] = $_POST['auditType'];
                $supplierData['auditFrequency'] = $_POST['auditFrequency'];
                $supplierData['nextAuditMonth'] = $_POST['nextAuditMonth'];
            }else{
                $supplierData['ptyNumber'] = null;
                $supplierData['vatNumber'] = null;
                $supplierData['officeNumber'] = null;
                $supplierData['website'] = null;
                $supplierData['beeCertificate'] = null;
                $supplierData['headOfficeDetails'] = null;
                $supplierData['headOfficeTown'] = null;
                $supplierData['headOfficeProvince'] = null;
                $supplierData['supplyDetails'] = null;
                $supplierData['serviceFrequency'] = null;
                $supplierData['uploadDocuments'] = null;
                $supplierData['complianceConstruction'] = null;
                $supplierData['complianceHighRisk'] = null;
                $supplierData['complianceDescription'] = null;
                $supplierData['applicableSupplier'] = null;
                $supplierData['auditType'] = null;
                $supplierData['auditFrequency'] = null;
                $supplierData['nextAuditMonth'] =  null;
            }
            $addSupplier =  $objSupplier->addSupplier($supplierData);

            if($addSupplier){

                $data['type'] = 'success';
                $data['message'] = 'Supplier added  successfully';
                controller::nextPage('viewAllSuppliers','supplier', $data=[]);

            }else{

                $data['type'] = 'error';
                $data['message'] = 'Failed to add supplier';
                controller::nextPage('viewAllSuppliers','supplier', $data=[]);
            }
        }else{
			
			$data = array();
            $data['serviceTypes'] = $objService->getAllServiceTypes();//@TODO: get service types depending on company preferences
            $data['auditTypes'] = $objAudit->getAllAuditTypes();//@TODO: get audits types depending on company preferences
			$objTemplate->setVariables('title','Add Supplier');
			$objTemplate->setView('templates/supplier','add_supplier_tpl',$data);
		}
		
	}

    /**
     * Method to edit supplier
     */
	public static function editSupplierInfo(){

        global $objSupplier;


        if(!empty($_POST)){

            $supplierData = array();

            $supplierData['companyName'] = $_POST['companyName'];
            $supplierData['ptyNumber'] = $_POST['ptyNumber'];
            $supplierData['vatNumber'] = $_POST['vatNumber'];
            $supplierData['officeNumber'] = $_POST['officeNumber'];
            $supplierData['contactPerson'] = $_POST['contactPerson'];
            $supplierData['contactNumber'] = $_POST['contactNumber'];
            $supplierData['email'] = $_POST['email'];
            $supplierData['website'] = $_POST['website'];
            $supplierData['headOfficeDetails'] = $_POST['headOfficeDetails'];
            $supplierData['headOfficeTown'] = $_POST['headOfficeTown'];
            $supplierData['headOfficeProvince'] = $_POST['headOfficeProvince'];
            $supplierData['serviceType'] = $_POST['supplyType'];
            $supplierData['supplyDetails'] = $_POST['supplyDetails'];
            $supplierData['serviceFrequency'] = $_POST['serviceFrequency'];
            $supplierData['uploadDocuments'] = $_POST['uploadDocuments'];
            $supplierData['complianceConstruction'] = $_POST['complianceConstruction'];
            $supplierData['complianceHighRisk'] = $_POST['complianceHighRisk'];
            $supplierData['complianceDescription'] = $_POST['complianceDescription'];
            $supplierData['applicableSupplier'] = $_POST['applicableSupplier'];
            $supplierData['auditType'] = $_POST['auditType'];
            $supplierData['auditFrequency'] = $_POST['auditFrequency'];
            $supplierData['nextAuditMonth'] = $_POST['nextAuditMonth'];
            $supplierData['modifiedBy']= $_SESSION['user_id'];
            $supplierData['supplierId']= $_POST['supplierId'];


            $addSupplier =  $objSupplier->editSupplier($supplierData);

            if($addSupplier){

                $data['type'] = 'success';
                $data['message'] = 'Supplier updated successfully';
                $data['id'] = $_POST['supplierId'];
                controller::nextPage('viewSupplierInfo','supplier', $data);

            }else{

                $data['type'] = 'error';
                $data['message'] = 'Failed to update supplier';
                $data['id'] = $_POST['supplierId'];
                controller::nextPage('viewSupplierInfo','supplier', $data);
            }
        }
    }

    public static function viewSupplierInfo(){

        global $objSupplier;
        global $objService;
        global $objTemplate;

        $data = array();
        $id = intval($_GET['id']);
        $data['supplierInfo'] = $objSupplier->getSupplierInfo($id);
        $data['serviceTypes'] = $objService->getAllServiceTypes();//@TODO: get service types depending on company preferences

        $objTemplate->setVariables('title', 'Service Provider');
        $objTemplate->setView('templates/supplier', 'supplier_details_tpl', $data);
    }

    /**
     * Method to delete a provider
     */
    public static function deleteSupplier(){

        global $objSupplier;

        $id = intval($_GET['id']);
        $deleteSupplier = $objSupplier->deleteSupplier($id);

        if($deleteSupplier){

            $data['type'] = 'success';
            $data['message'] = 'Supplier deleted successfully';
            controller::nextPage('viewAllSuppliers','supplier', $data);

        }else{

            $data['type'] = 'error';
            $data['message'] = 'Failed to delete supplier';
            controller::nextPage('viewAllSuppliers','supplier', $data);

        }

    }

    /**
     * Method to request for a new quote
     */
    public static  function requestQuote(){
        global $objTemplate;

        if(!empty($_POST)){

        }else{
            $data = array();
            $objTemplate->setVariables('title','Quotation');
            $objTemplate->setView('templates/supplier','request_quote_tpl',$data);

        }
    }
   /**
    * Method to search client details
    */
    public static function searchSupplier(){
        global $objSupplier;
        
        $SupplierDetsArr = $objSupplier->searchSupplier($_POST['supplierCompanyName'], $_POST['supplierNameSurname']);
        echo json_encode($SupplierDetsArr); 
        
    }
	
}