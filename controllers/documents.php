<?php

/**
 * Controller class containing methods to process all document related actions
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

//Include the base controller
include_once 'controller.php';

class documents extends controller
{

/******* WARRENS code begins here *******/

    /**
     * Method to display the default template
     * 
     * @return void
     */
    public static function defaultAction() 
    {    
        self::nextPage('approvedDocuments','documents'); //change this
    }
   
    /**
     * Method to save a new document
     * 
     * @return void
     */
    public static function saveDocument() 
    {
        global $objTemplate;
        global $objEmployee;
        global $objDepartment;
        global $objDocument;
        global $objBranch;

        $data = array();
        $documentDetsArr = array();
        //$linksArr = array();
        $data['getallEmployee'] = $objEmployee->getAllEmployees($_SESSION['company_id']);
        $data["getAllDepartment"] = $objDepartment->getDepartmentByCompany($_SESSION['company_id']);
        $data["branches"] = $objBranch->getBranchByCompany($_SESSION['company_id']);
       
        $data['documentTypes'] = $objDocument->getDocumentTypes();
        
        $documentDetsArr['group_id'] = $_POST['documentGroup'];
        $documentDetsArr['type_id'] = $_POST['documentType'];
        $documentDetsArr['document_description'] = $_POST['documentDescription'];
        $documentDetsArr['document_no'] = $_POST['documentNo'];
        $documentDetsArr['document_revision_no'] = $_POST['documentRevisionNo'];
        $documentDetsArr['document_system_no'] = $_POST['documentSystemNo'];
        $documentDetsArr['document_originator_id'] = $_POST['documentOriginator'];
        $documentDetsArr['document_revision_frequency'] = $_POST['documentRevisionFrequency'];
        $documentDetsArr['documentNextRevisionDate'] = $_POST['documentNextRevisionDate'];
        $documentDetsArr['branches'] = implode(',', $_POST['branches']);
        $documentDetsArr['approvedBy'] = implode(',', $_POST['approvers']);
        //$documentDetsArr['document_links'] = $linksArr;
        
        $response = $objDocument->addDocument($documentDetsArr);
        
        if(is_int($response)){
           $data['type'] = 'success';
           $data['message'] = "Document was successfully saved."; 
           $data['docId'] = $response;
           $data['getAllAddApproval'] = $objDocument->getAllDocumentAprroval($response);
           $data['documentDetsArr'] = $documentDetsArr;
           
           $objTemplate->setVariables('title', 'Add Document Approval');
           $objTemplate->setView('templates/documents', 'document_control_add_document_tpl', $data);
        }else{
           $data['type'] = 'warning';
           $data['message'] = 'Oops! Something went wrong. Please log all bug <a href="#" >here</a>';
           parent::nextPage('defaultAction','documents', $data);  
        }

    }    
    
    public static function downloadDocument()
    {
        global $objTemplate;
        global $objDocument;
        global $objFolders;
        
        $data = array();
        $documentid = $_REQUEST['docId'];
        $document = $objDocument->getDocument($documentid);
        $folderInfo = $objFolders->getFolderInfo($document['folder_id']);
        
        $data['folder'] = $folderInfo;
        $data['document'] = $document;
        
        $objTemplate->setVariables('title', 'Download Document');
        $objTemplate->setView('templates/documents', 'file', $data);
    }
/******* SUNNYS code begins here *******/
    /**
     * Method to show approved documents
     * @global object $objTemplate
     * @global object $objDocument
     * @global object $objEmployee
     * @global object $objDepartment
     * @return void 
     */
    public static function addDocuments()
    {
        global $objTemplate;
        global $objEmployee;
        global $objDepartment;
        global $objDocument;
        global $objBranch;
        
        $data = array();
        $data['getallEmployee'] = $objEmployee->getAllEmployees($_SESSION['company_id']);
        $data["getAllDepartment"] = $objDepartment->getDepartmentByCompany($_SESSION['company_id']);
        $data["branches"] = $objBranch->getBranchByCompany($_SESSION['company_id']);
        $data['getAllAddApproval'] = $objDocument->getAllDocumentAprroval($_GET['document_id']=1); //TODO
        $data['documentTypes'] = $objDocument->getDocumentTypes();
        
        $objTemplate->setVariables('title', 'Add Documents');
        $objTemplate->setView('templates/documents', 'document_control_add_document_tpl', $data);
    }
    /**
     * Method to show approved documents
     * @return void 
     */
    public static function approvedDocuments() 
    {    
        global $objTemplate;
        global $objDocument;
        global $objEmployee;
        if(isset($_POST['groups'])){
            $groups = implode(',', $_POST['groups']);
        } else {
            $groups = NULL;
        }   
        if(isset($_POST['originators'])){
            $originator = implode(',', $_POST['originators']);
        } else {
            $originator = NULL;
        }           
        
        if(!is_null($groups) && $groups != ""){
            $groupsArr = explode(',', $groups);
        } else {
            $groupsArr = array();
        }
        if(!is_null($originator) && $originator != ""){
            $originatorArr = explode(',', $originator);
        } else {
            $originatorArr = array();
        }
        $data = array();
        $data['getallEmployee'] = $objEmployee->getAllEmployees($_SESSION['company_id']);
        $data['getDocumentTypes'] = $objDocument->getDocumentTypes();
        $data['getallApprovedDocs'] = $objDocument->searchApprovedDocuments($groupsArr, $originatorArr);

        $objTemplate->setVariables('title', 'Approved Documents');
        $objTemplate->setView('templates/documents', 'document_control_tpl', $data);
    }
    
    /**
     * Method to show documents that need to be approved
     *@global $objTemplate;
     *@global $objDocument;
     * @return void 
     */
    public static function approvalDocuments() 
    {    
        global $objTemplate;
        global $objDocument;
        $data = array();
        
        $data['getDocumentTypes'] = $objDocument->getDocumentTypes();
        $data['getallApprovalDocs'] = $objDocument->searchUnapprovedDocuments();
        
        $objTemplate->setVariables('title', 'Approval Documents');
        $objTemplate->setView('templates/documents', 'document_control_approval_tpl', $data);
   
    }
    /**
     * Method to show documents that need to be approved
     * 
     * @return void 
     */
    public static function approveDocuments() 
    {    
        global $objTemplate;
        global $objDocument;
        
        $data = array();
        $data['documentDetails'] = $objDocument->getDocument($_GET['id']);
        $data['approvedBy'] = $objDocument->approvedBy($_GET['id']);
        $data['notificationList'] = $objDocument->getAllDocumentAprroval($_GET['id']);
        
        $objTemplate->setVariables('title', 'Approve Documents');
        $objTemplate->setView('templates/documents', 'document_control_approval_approve_tpl', $data);
   
    }
     /**
     * Method to save person to approve the document
     * @return void 
     */
    public static function saveApprovalPerson(){
        global $objDocument;
        $data = array();
        $documentDetsArr = array();
        
        $documentDetsArr['doc_id'] = 1 ; //$_POST['document_id'] TODO
        $documentDetsArr['employee_id'] = $_POST['employeeId'];
        $documentDetsArr['document_version'] = 1.0; //$_POST['document_version'] TODO
        $documentDetsArr['approval_comments'] = null;
        $documentDetsArr['due_date'] = $_POST['orderDueDate'];
        $documentDetsArr['is_amendments'] = null;
        
        $response = $objDocument->addDocumentApproval($documentDetsArr);
        
        if($response){
           $data['type'] = 'success';
           $data['message'] = $response; 
           parent::nextPage('addDocuments','documents', $data); 
        }else{
           $data['type'] = 'warning';
           $data['message'] = 'Oops! Something went wrong. Please log all bug <a href="#" >here</a>';
           parent::nextPage('defaultAction','documents', $data);  
        }
    }
    /**
     * Method to save document approval 
     * @global object $objDocument
     * @return void 
     */
    public static function saveDocumentApprove(){
        global $objDocument;
        $data = array();
        $documentDetsArr = array();
        
        if($_POST['approveDoc'] == 0){
            $documentDetsArr['approval'] = $_POST['approveDoc']; 
            $documentDetsArr['id'] = $_POST['id'];
            $documentDetsArr['is_amendments'] = $_POST['amendments'];  
            $documentDetsArr['is_approved'] = $_POST['approveDoc'];
            $documentDetsArr['approval_comments'] = $_POST['comments'];
            $documentDetsArr['email'] = implode(',',$_POST['YesEmail']);
        }else{
            $documentDetsArr['approval'] = $_POST['approveDoc']; 
            $documentDetsArr['id'] = $_POST['id'];
            $documentDetsArr['is_amendments'] = null; 
            $documentDetsArr['is_approved'] = $_POST['approveDoc'];
            $documentDetsArr['approval_comments'] = null;
            $documentDetsArr['email'] = implode(',',$_POST['NoEmail']);
        }

        $response = $objDocument->approveDocument($documentDetsArr);
        
        if($response){
           $data['type'] = 'success';
           $data['message'] = $response;
           parent::nextPage('defaultAction','documents', $data); 
        }else{
           $data['type'] = 'warning';
           $data['message'] = 'Oops! Something went wrong. Please log all bug <a href="#" >here</a>';
           parent::nextPage('defaultAction','documents', $data);  
        }
    }
    /**
     * Method to send approvals notifications
     * @global object $objTemplate
     * @global object $objDocument
     */
    public static function approvalNotifications(){
        global $objDocument;
        $data = array();

        if($_POST['approveDoc'] == 1){
            $data['getAllAddApproval'] = $objDocument->getAllDocumentAprroval($_GET['document_id']=1); //TODO
            foreach($data['getAllAddApproval'] as $emailDetailsArr){
                $response = $objDocument->sendEmailNotifications($emailDetailsArr['field_data'], 'User Approval Notification', $_POST['approvalMessage']);
            }
        }
        
        if($response){
           $data['type'] = 'success';
           $data['message'] = $response;
           parent::nextPage('addDocuments','documents', $data); 
        }else{
           $data['type'] = 'warning';
           $data['message'] = 'Oops! Something went wrong. Please log all bug <a href="#" >here</a>';
           parent::nextPage('defaultAction','documents', $data);  
        }
    }
    /**
     * Method to view approval progress
     * @global object $objTemplate
     * @global object $objDocument
     * @return void
     */
    public static function viewApprovalProgress(){
        global $objTemplate;
        global $objDocument;
        $data = array();
        
        $data['documentDetails'] = $objDocument->getDocument($_GET['id']);
        $data['approvedBy'] = $objDocument->approvedBy($_GET['id']);
        $data['getAllApproval'] = $objDocument->getAllDocumentAprroval($_GET['id']);
        
        $objTemplate->setVariables('title', 'View Approval Progress');
        $objTemplate->setView('templates/documents', 'document_control_approval_progress_tpl', $data);
    }
    
    /**
     * Method to revise documents that need to be approved again
     * @global object $objTemplate
     * @global object $objDocument
     * @return void
     */
    public static function reviseDocuments() 
    {    
        global $objTemplate;
        global $objDocument;
        global $objEmployee;
        
        $data = array();
        
        $data['documentDetails'] = $objDocument->getDocument($_GET['id']);
        $data['approvedBy'] = $objDocument->approvedBy($_GET['id']);
        $data['getAllApproval'] = $objDocument->getAllDocumentAprroval($_GET['id']);
        $data['getallEmployee'] = $objEmployee->getAllEmployees($_SESSION['company_id']);
        $data['notificationList'] = $objDocument->getAllDocumentAprroval($_GET['id']);
        
        $objTemplate->setVariables('title', 'Revise Documents');
        $objTemplate->setView('templates/documents', 'document_control_revise_tpl', $data);
    }
    
 /**
     * Method to revise documents that need to be approved again
     * @global object $objTemplate
     * @global object $objDocument
     * @return void
     */
    public static function saveRevision() 
    {    
        global $objTemplate;
        global $objEmployee;
        global $objDepartment;
        global $objDocument;
        global $objBranch;

        $data = array();
        $documentDetsArr = array();

        $documentDetsArr['document_originator_id'] = $_POST['documentOriginator'];
        $documentDetsArr['document_revision_frequency'] = $_POST['documentRevisionFrequency'];
        $documentDetsArr['documentNextRevisionDate'] = $_POST['documentNextRevisionDate'];
        $documentDetsArr['approvedBy'] = implode(',', $_POST['approvers']);
        $documentDetsArr['docId'] = $_POST['docId'];
        
        $response = $objDocument->saveRevision($documentDetsArr);
        
        if(is_int($response)){
           $data['type'] = 'success';
           $data['message'] = "Document was successfully saved."; 
           parent::nextPage('defaultAction','documents', $data);  
        }else{
           $data['type'] = 'warning';
           $data['message'] = 'Oops! Something went wrong. Please log all bug <a href="#" >here</a>';
           parent::nextPage('defaultAction','documents', $data);  
        }
    }    
    /**
     * Method to delete approvals
     * @global $objDocument;
     * @return void 
     */
    public static function deleteApproval() 
    {
        global $objDocument;
        $data =  array();
        $response = $objDocument->deleteApproval($_GET['id']);
        if($response){
           $data['type'] = 'success';
           $data['message'] = $response; 
           parent::nextPage('addDocuments','documents', $data); 
        }else{
           $data['type'] = 'warning';
           $data['message'] = 'Oops! Something went wrong. Please log all bug <a href="#" >here</a>';
           parent::nextPage('defaultAction','documents', $data);  
        }
    }
/******* BRIANS code begins here *******/

    /**
     * Method to view all folders page
     */
    public static  function  viewAllDocumentsRecords(){

        global $objTemplate;
        $objTemplate->setVariables('title','Document Records');
        $objTemplate->setView('templates/documents','document_control_records_tpl',$data =[]);

    }
    /**
     * Method to display the Document Archive tab
     * 
     * @return void
     */
    public static function documentArchive() 
    {
        global $objTemplate;
        global $objDocument;
        
        $data = array();
        $data['getDocumentTypes'] = $objDocument->getDocumentTypes();
        $data['getAllArchivedDocs'] = $objDocument->getArchivedDocuments();
        
        $objTemplate->setVariables('title', 'Archived Documents');
        $objTemplate->setView('templates/documents', 'document_control_archive_tpl', $data);
    }

    /**
     * Method to display the Document Settings tab
     * 
     * @return void
     */
    public static function documentSettings() 
    {
        global $objTemplate;
        global $objDocument;
        
        $data = array();
        //$data['getDocumentTypes'] = $objDocument->getDocumentTypes();
        $data['document_codes'] = $objDocument->getDocumentCodes();
        
        $objTemplate->setVariables('title', 'Document Settings');
        $objTemplate->setView('templates/documents', 'document_control_settings_tpl', $data);
    }
    
    
}    