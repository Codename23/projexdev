<?php


class investigation extends controller
{    
    public static function getAllInvestigations($is_active = null)
    {
        global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigations->getAllInvestigations(false);
        $objTemplate->setVariables('title','Non-Conformance Investigations');
        $objTemplate->setView('templates/investigations','index_tpl',$data);
    }
    public static function viewSingleInvestigation()
    {
        global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        global  $objNonConformance;             
        $id = 0;
        if(isset($_GET["ncid"]))
        {
            $id = (int)$_GET["ncid"];
        }
        //allPersons
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allNonConformance'] = $objNonConformance->viewInvestigationDetailsById($id);
        $data['allFiveWhyDetails'] = $objInvestigations->getFiveWhy($id);
         if($objInvestigations->checkFiveWhyExist($id))
         {
             $objInvestigations->hasFive = true;
         }
        $objTemplate->setVariables('title','Non Conformance');
        $objTemplate->setView('templates/investigations','improvement_investigation_why_investigation',$data);
    }
    public static function signedOffInvestigation()
    {
      global  $objSignedOff;
      if(isset($_POST['txtInvestigationId']))
      {          
         $data = array();
         $data["signedOffAction"] = null;
         $data['signedOffLevel'] = "Master";
         //signs of investigation
         $data["signedOffType"] = 3;
         $data['source_id'] = $_POST['txtInvestigationId'];
         $data["table"] = 'tbl_investigations';
         $result = $objSignedOff->hasSignedOffData($data);
      }     
      //get the history
      self::getAllInvestigationsHistory(false);
    }
    public static function addWhyInvestigation()
    {
         global $objInvestigations;
         global $objNonConformance;
         $objInvestigationsFiveWhyData = array();
         global $objTemplate;
         global $objEmployee;
         $data = array();   
         $id = 0;
         $result = false;
        if(isset($_POST["ncid"]))
        {
            $id = (int)$_POST["ncid"];
        }
        
        if(isset($_POST['btnSubmitFiveWhy']))
        {
          if($objInvestigations->checkFiveWhyExist($id) == false)
          {
                if(isset($_POST['type']))
                {       
                    $hasCompleted = false;
                    $counter = 1;
                    for($t = 0; $t < 5;$t++)
                    {
                        $objInvestigationsFiveWhyData["why"] = "Why ".($t + 1);
                        if(isset($_POST['type'][$t]))
                        {                   
                            $objInvestigationsFiveWhyData["type"] = $_POST['type'][$t];
                            $objInvestigationsFiveWhyData["description"] = $_POST['description'][$t];                
                            $objInvestigationsFiveWhyData["recomm"] = $_POST['recommendedImprovement'][$t];                
                            $objInvestigationsFiveWhyData["priority"] = $_POST['priority'][$t];     
                            if($objInvestigations->addFiveWhy(1,$id,$objInvestigationsFiveWhyData))    
                            {

                            }                        
                        }
                        if($t == 4)
                        {
                                $hasCompleted = true;
                        }
                    }
                    if($hasCompleted == true)
                    {

                    }
                }
          }
        }
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allNonConformance'] = $objNonConformance->viewInvestigationDetailsById($id);
        $data['allFiveWhyDetails'] = $objInvestigations->getFiveWhy($id);
         if($objInvestigations->checkFiveWhyExist($id))
         {
             $objInvestigations->hasFive = true;
         }
        $objTemplate->setVariables('title','Non Conformance');
        $objTemplate->setView('templates/investigations','improvement_investigation_why_investigation',$data);         
    }
    public static function addInvestigation()
    {
        global $objInvestigations;
        $objInvestigationsData = array();
        global $objTemplate;
        global $objEmployee;
        global $objActions;
        
        $data = array();
        $id = 0;
        $continue_investigation = false;
        
        if(!empty($_POST['non_id']))
        {
            $id = (int)$_POST['non_id'];
            $objInvestigationsData['investigate_link_id'] = $id;    
            $continue_investigation = false;
            $objInvestigationsData['investigation_type'] = 1;
            if($_POST['investigationOptions'] == "addInvestigation")
            {
                $continue_investigation = false;
                $objInvestigationsData['investigator_due_date'] = $_POST['investigationDueDate'];
                $objInvestigationsData['investigationInvestigator'] = $_POST['investigationInvestigator'];
            }
            if($_POST['investigationOptions'] == "doInvestigation")
            {
                $continue_investigation = true;
                $objInvestigationsData['investigator_due_date'] = 'NOW()';
                $objInvestigationsData['investigationInvestigator'] = $_POST['doInvestigationInvestigator'];
            }            
            
            //needs to refine this
            $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
            $objInvestigationsData['investigator_status'] = 'Good';            
            $objInvestigationsData['investigationOptions'] = $_POST['investigationOptions'];             
            $result = $objInvestigations->addInvestigation($objInvestigationsData);
            if($result)
            {
                //results
                if($objInvestigations->updateInvestigationStatus($objInvestigationsData['investigationInvestigator'],$id,$objInvestigationsData['investigator_due_date']))
                {
                        //add to action table
                      //store action non-conformance
                    $objInvestigationsData["source_id"] = (int)$id;
                    $objInvestigationsData['source_type'] = "Non-Conformance";
                    $objInvestigationsData['action_taken'] = "Requested Investigation";
                    $objInvestigationsData['action_taken_id'] = $objInvestigations->requested_id;
                    $objInvestigationsData['action'] = $_POST["investigationOptions"];
                    if($objActions->hasCreatedAction($objInvestigationsData))
                    {
                        self::getAllInvestigations();
                    }           
                }
                //end results
            }
        }
        if($continue_investigation == true)
        {
            $data['allInvestigations'] = $objInvestigations->getAllInvestigations();
            $data['ncid'] = $id;
            $objTemplate->setVariables('title','Non-Conformance Investigations');
            $objTemplate->setView('templates/investigations','improvement_investigation_why_investigation',$data);
        }
        else
        {
            $data['allInvestigations'] = $objInvestigations->getAllInvestigations();
            $objTemplate->setVariables('title','Non-Conformance Investigations');
            $objTemplate->setView('templates/investigations','index_tpl',$data); 
        }
    }
    public static function addInvestigatorsPost()
    {
        global $objInvestigators;
        $investigatorsData = array();
        if(isset($_POST['btnAddInvestigator']))
        {
            $investigatorsData["department_id"] = $_POST['investigatorDepartment'];
            $investigatorsData["occupation_id"] = null;
            $investigatorsData["emp_id"] = $_POST['investigatorNameSurname'];
            $investigatorsData["investigation_dep"] = $_POST['departments_selected'];
            $investigatorsData["investigation_group"] = $_POST['groups_selected'];            
            if($objInvestigators->addInvestigator($investigatorsData))
            {
                self::viewInvestigators();
            }
        }
    } 
    public static function updateInvestigators()
    {
        /*global $objInvestigators;
        $investigatorsData = array();
        if(isset($_POST['']))
        {
            $investigatorsData["department_id"] = $_POST[''];
            $investigatorsData["occupation_id"] = $_POST[''];
            $investigatorsData["emp_id"] = $_POST[''];
            $investigatorsData["investigation_dep"] = $_POST[''];
            $investigatorsData["investigation_group"] = $_POST[''];            
            if($objInvestigators->addInvestigator($investigatorsData))
            {
                self::viewInvestigators();
            }
        }*/
    }
    public static function editInvestigators()
    {
        
    }
    public static function improvement_investigaions()
    {
        if(isset($_POST['investigatorSignoffBtn']))
        {
            global $objSignedOff;
            $data = array();
            $data["source_id"] = $_POST["txtInvestigationId"];
            $data["signedOffLevel"] = $_POST["txtMaster"];
            $data["signedOffType"] = $_POST["txtSignOffInvestigation"];
            $return = $objSignedOff->hasSignedOff($data);
            self::getAllInvestigationsHistory();
        }
    }
    public static function getInvestigationReport()
    {
        global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        $id = 0;
        if(isset($_GET['ncid']))
        {
            $id = (int)$_GET['ncid'];
        }
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigations->getAllInvestigations(true);
        $data['allFiveWhyDetails'] = $objInvestigations->getFiveWhy($id);
        $objTemplate->setVariables('title','Non-Conformance Investigations');
        $objTemplate->setView('templates/investigations','improvement_investigation_history_view_report',$data);
    }
    public static function getAllInvestigationsHistory()
    {
        global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigations->getAllInvestigations(true);
        $objTemplate->setVariables('title','Non-Conformance Investigations');
        $objTemplate->setView('templates/investigations','investigation_history',$data);
    }
    public static function viewInvestigators()
    {
        global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        global $objSheqTeam;
        global $objInvestigators;
        //global $objOccupations;
        global $objDepartment;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigators->getAllInvestigators(true);
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
        //$objOccupations->getDepartmentOccupations()
        $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $objTemplate->setVariables('title','Investigators');
        $objTemplate->setView('templates/investigations','investigators',$data); 
    } 
    public static function getEmployeeByDepartment()
    {
        global $objEmployee;
        $id = null;
        if(isset($_GET["depid"]))
        {
            $id = (int)$_GET["depid"];
        }
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id'],$id);
        foreach($data["allPersons"] as $employee)
        {
            $name = $employee["firstname"].' '.$employee['lastname'];
            $employees[] = array(
               "id"=>$employee["uid"],"fullname"=>$name,"state"=>"200"
            );
        }
        echo json_encode($employees);
    }
    public static function viewInvestigatorsNotActive()
    {
        global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        global $objSheqTeam;
        global $objInvestigators;
        //global $objOccupations;
        global $objDepartment;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigators->getAllInvestigators(false);
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
        //$objOccupations->getDepartmentOccupations()
        $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $objTemplate->setVariables('title','Investigators');
        $objTemplate->setView('templates/investigations','investigators_not_active',$data);
    }
    public static function getInvestigatorDetails()
    {
        global $objInvestigators;
        global $objTemplate;
        global $objEmployee;
        global $objSheqTeam;
        global $objDepartment;
        $id = 0;
        if(isset($_GET["inv_id"]))
        {
            $id = (int)$_GET["inv_id"];
        }
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);        
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
        $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data["currentInvestigators"] = $objInvestigators->getInvestigatorById(true,$id);
        $objTemplate->setVariables('title','Investigators');
        $objTemplate->setView('templates/investigations','ajax_get_investigators',$data);
    }
    public static function deleteInvestigator()
    {
        global $objInvestigators;
        $id = 0;
        if(isset($_POST["inv_id"]))
        {
            $id = (int)$_POST["inv_id"];
        }
        return $objInvestigators->hasDeletedInvestigator($id);
    }
    public static function restoreInvestigator()
    {
         global  $objInvestigators;         
         $id = 0;
         if(isset($_GET['ivd']))
         {
             $id = (int)$_GET['ivd'];
         }
         $return = $objInvestigators->hasRestoredInvestigator($id);
         if($return == "succeed")
         {
            self::viewInvestigators();
         }
         else
         {
             self::viewInvestigatorsNotActive();
         }
    }
}

