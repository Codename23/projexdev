<?php
/**
 * Controller class containing methods to process all occupation related actions
 *
 * @package sheqonline
 * @author Brian Douglas<douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';


class employee extends controller {


    public function __construct()
    {

    }
    /**
     * View all company employees
     */
    public static  function  viewAllEmployees(){

        global  $objEmployee;
        global $objTemplate;

        $data = array();
        $companyId = $_SESSION['company_id'];
        $branchId = $_SESSION['branch_id'];


        $data['allEmployees'] = $objEmployee->getAllEmployees($companyId,$branchId);
        $objTemplate->setVariables('title','Employees');
        $objTemplate->setView('templates/employee','index_tpl',$data);
    }

    /**
     * Method to add a new employee
     */
    public static function addEmployee(){

        global $objUsers;
        global $objEmployee;
        global $objCompanies;
        global $objTemplate;

        if(!empty($_POST) && (isset($_POST['occupation'])&& is_numeric($_POST['occupation']))){
            
            $userType = 5;
            $createdBy = $_SESSION['user_id'];
            $companyId = $_SESSION['company_id'];

            $userData = array(
                "user_type_id"=>$userType,
                "firstname"=>$_POST['employee_name'],
                "lastname"=>$_POST['employee_surname'],
                "username"=>$_POST['employee_name'],
                "password"=>$_POST['employee_name'],
                "createdBy"=>$createdBy,
                "modified_by"=>$createdBy
            );
            //Add a new user before adding an employee
            $userId = $objUsers->addUser($userData);

            if($userId){

                $employeeData = $_POST;
                $employeeData['user_type'] = $userType;
                $employeeData['created_by'] = $createdBy;
                //$employeeData['employee_id'] = $userId;
                $employeeData['employee_userid'] = $userId;

                $addEmployee = $objEmployee->addEmployee($employeeData);

                //Now add Employee occupation
                $addEmployeeOccupation = false;
                if($addEmployee && isset($_POST['department_name']) && isset($_POST['branch_name']) && isset($_POST['occupation'])){

                    $occupationData = array(
                        "occupationId" =>$_POST['occupation'],
                        "departmentId" =>$_POST['department_name'],
                        "branchId" =>$_POST['branch_name'],
                        "companyId" =>$companyId,
                        "userId" =>$userId,
                        "userType" =>$userType);

                    $addEmployeeOccupation = $objEmployee->addEmployeeOccupation($occupationData);

                    if($addEmployeeOccupation){
                        $data['type'] = 'success';
                        $data['message'] = 'Employee added successfully ';
                        if($_POST['formType'] != 'ajaxForm'){
                           controller::nextPage("viewAllEmployees","employee",$data); 
                        }
                        
                    }else{
                        if($_POST['formType'] != 'ajaxForm'){
                           controller::nextPage("addEmployee","employee"); 
                        }
                        
                    }
                }elseif($addEmployee){
                    $data['type'] = 'error';
                    $data['message'] = 'Failed to  add occupation';
                    if($_POST['formType'] != 'ajaxForm'){
                       controller::nextPage("viewAllEmployees","employee",$data); 
                    }
                    
                }

            }else{
                $data['type'] = 'error';
                $data['message'] = 'Failed to add employee. Check if user already exists';
                if($_POST['formType'] != 'ajaxForm'){
                   controller::nextPage("addEmployee","employee",$data); 
                }
                
            }


        }else 
            {
            $data = array();
            $company_id = $_SESSION['company_id'];
            $userType = 5;


            //$data['companyUserTypes'] = $objEmployee->getEmployeeTypes();
            $data['companyBranches'] = $objCompanies->getCompanyBranches($company_id);
            $data['allEmployeeFields'] = $objEmployee->getEmployeeFormDetails($userType);
            $data['gender'] = $objEmployee->getEmployeeGender();
            $data['ethnic'] = $objEmployee->getEmployeeRace();
            $objTemplate->setVariables('title', 'Employees');
            $objTemplate->setView('templates/employee', 'add_employee_tpl', $data);
        }
    }

    /**
     * Method to edit Employee Details
     */
    public static function editEmployee(){

        global $objUsers;
        global $objEmployee;

        if(!empty($_POST)){

            $userType = 5;
            $modifiedBy = $_SESSION['user_id'];

            $employeeData = $_POST;
            $employeeData['user_type'] = $userType;
            $employeeData['employeeId'] = $_POST['id'];
            $employeeData['modified_by'] = $modifiedBy;

            $editEmployee = $objEmployee->editEmployee($employeeData);

            if($editEmployee){

                $userData['firstname'] = $_POST['employee_name'];
                $userData['lastname'] = $_POST['employee_surname'];
                $userData['username'] = $_POST['employee_name'];
                $userData['modified_by'] = $modifiedBy;
                $userData['user_id'] = $_POST['id'];
                $editUser = $objUsers->editUser($userData);


                if($editUser && isset($_POST['department_name']) && isset($_POST['branch_name']) && isset($_POST['occupation'])){

                    $occupationData = array(
                        "occupationId" =>$_POST['occupation'],
                        "departmentId" =>$_POST['department_name'],
                        "branchId" =>$_POST['branch_name'],
                        "userId" =>$_POST['id'],
                        "userType" =>$userType);

                    //update employee occupation
                    $addEmployeeOccupation = $objEmployee->updateEmployeeOccupation($occupationData);

                    if($addEmployeeOccupation){
                        $data['type'] = 'success';
                        $data['message'] = 'Employee updated successfully';
                        controller::nextPage("viewAllEmployees","employee",$data);
                    }else{
                        $data['type'] = 'error';
                        $data['message'] = 'Failed to  updated occupation';
                        controller::nextPage("addEmployee","employee");
                    }
                }elseif($editUser){

                    $data['type'] = 'error';
                    $data['message'] = 'Failed to  updated occupation';
                    controller::nextPage("viewAllEmployees","employee",$data);
                }
            }

        }

    }


    /**
     * Method to view employee details
     */
    public static function viewEmployeeInfo(){

        global $objEmployee;
        global $objCompanies;
        global $objOccupations;
        global $objTemplate;

        $data = array();
        $companyId = $_SESSION['company_id'];
        $userId = $_GET['id'];
        $branchId = $_SESSION['branch_id'];
        $data['companyBranches'] = $objCompanies->getCompanyBranches($companyId);
        $data['branchDepartments'] = $objCompanies->getCompanyDepartments($companyId,$branchId);
        $data['departmentOccupations'] = $objOccupations->getCompanyOccupations($companyId);

        //Employee information
        $data['employeeBranch'] = $objEmployee->getEmployeeBranch($userId);
        $data['employeeDepartment'] = $objEmployee->getEmployeeDepartment($userId);
        $data['employeeOccupation'] = $objEmployee->getEmployeeOccupation($userId);
        $data['employeeName'] = $objEmployee->getEmployeeName($userId);
        $data['editEmployeeInfo'] = $objEmployee->getEmployeeInfo($userId);
        $data['gender'] = $objEmployee->getEmployeeGender();
        $data['ethnic'] = $objEmployee->getEmployeeRace();

        $objTemplate->setVariables('title', 'Edit Employees');
        $objTemplate->setView('templates/employee', 'employee_details_tpl', $data);
    }

    /**
     * Method to delete employee
     */
    public static function deleteEmployee()
    {
        global $objEmployee;

            $data = array();
            $employeeData['employeeId'] = $_GET['id'];
            $employeeData['modifiedBy'] = $_SESSION['user_id'];
            $deleteEmployee = $objEmployee->deleteEmployee($employeeData);

            if($deleteEmployee){
                $data['type'] = 'success';
                $data['message'] = 'Employee deleted successfully';
                controller::nextPage('viewAllEmployees','employee', $data);

            }else{
                $data['type'] = 'error';
                $data['message'] = 'Failed to delete employee';
                controller::nextPage('viewAllEmployees','employee', $data);
            }
    }
    /**
     * Method to search employee
     */
    public static function searchEmployee(){
        global $objEmployee;
        $searchEmployeeDetsArr = $objEmployee->searchEmployee($_POST['searchterm'], $_POST['department']);
        echo json_encode($searchEmployeeDetsArr);
        
    }


}