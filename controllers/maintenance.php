<?php
class maintenance extends controller
{
    public function __construct() {
        
    }
    public static function maintenance_view()
    {
        global $objDepartment;
        global $objTemplate;
        global $objEmployee;
        global $objAssets;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data["allDepartments"] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data["active_assets"] = $objAssets->getActiveAssets();
        $objTemplate->setVariables('title','Non-Conformance Investigations');
        $objTemplate->setView('templates/maintenance','maintenance_tpl',$data);
    }
    public static function maintenance_psa()
    {
        global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigations->getAllInvestigations(false);
        $objTemplate->setVariables('title','Maintenance PSA');
        $objTemplate->setView('templates/maintenance','maintenance_psa_tpl',$data);
    }
    public static function maintenance_fmea()
    {
        global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigations->getAllInvestigations(false);
        $objTemplate->setVariables('title','Maintenance FMEA');
        $objTemplate->setView('templates/maintenance','maintenance_fmea_tpl',$data);
    }
    public static function maintenance_schedule()
    {
        global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigations->getAllInvestigations(false);
        $objTemplate->setVariables('title','Maintenance Schedule');
        $objTemplate->setView('templates/maintenance','maintenance_schedule_tpl',$data);
    }
    public static function maintenance_suggested()
    {
        global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigations->getAllInvestigations(false);
        $objTemplate->setVariables('title','Maintenance Suggested');
        $objTemplate->setView('templates/maintenance','maintenance_suggested_tpl',$data);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    public static function view_requested_maintenance()
    {
        global $objMaintenance;
        global $objTemplate;
        global $objEmployee;
        global $objDepartment;        
        global $objSupplier;
        $data = array();
        //$data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data["allDepartments"] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data['allEmployees'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
          $data['allSupplier'] = $objSupplier->getAllSuppliers($_SESSION['branch_id']); 
        $data['allRequestedMaintenance'] = $objMaintenance->getRequeqestedMaintenance();
        $objTemplate->setVariables('title','Requested Maintenance');
        $objTemplate->setView('templates/maintenance','active',$data);
    }
    public static function view_maintenance_jobs()
    {
        global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        global $objJobs;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigations->getAllInvestigations(false);
        $data["allRequestedJobs"] = $objJobs->getAllMaintenanceRequestedJobs();
        $objTemplate->setVariables('title','Requested Maintenance');
        $objTemplate->setView('templates/maintenance','active_job',$data);
    }
    
    public static function view_maintenance_active_history()
    {
         global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigations->getAllInvestigations(false);
        $objTemplate->setVariables('title','Requested Maintenance');
        $objTemplate->setView('templates/maintenance','active_history',$data);
    }
    ///////////////////////////PLAN//////////////////////////////////////////
    public static function view_maintenance_upcoming()
    {
         global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigations->getAllInvestigations(false);
        $objTemplate->setVariables('title','Requested Maintenance');
        $objTemplate->setView('templates/maintenance','maintenance_upcoming',$data);
    }
    public static function view_maintenance_job()
    {
         global $objInvestigations;
        global $objTemplate;        
        global $objDepartment;
        global $objEmployee;
        global $objSupplier;
        
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigations->getAllInvestigations(false);
        $objTemplate->setVariables('title','Requested Maintenance');
        $objTemplate->setView('templates/maintenance','maintenance_job',$data);
    }
    public static function viewAssetMaintenance()
    {
        global $objTemplate;
        global $objAssets;
        $data =  array();
        $data['assets'] = $objAssets->getAllAssetsNotImported();        
        $objTemplate->setVariables('title', 'View Assets');
        $objTemplate->setView('templates/maintenance', 'active_maintenance_import', $data);   
    }    
    public static function view_maintenance_history()
    {
         global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigations->getAllInvestigations(false);
        $objTemplate->setVariables('title','Requested Maintenance');
        $objTemplate->setView('templates/maintenance','maintenance_history',$data);
    }
    public static function view_maintenance_suggested()
    {
        global $objInvestigations;
        global $objTemplate;
        global $objEmployee;
        $data = array();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allInvestigations'] = $objInvestigations->getAllInvestigations(false);
        $objTemplate->setVariables('title','Requested Maintenance');
        $objTemplate->setView('templates/maintenance','maintenance_suggested_tpl',$data);
    }
    public static function viewAssetActive()
    {
        //
        global $objTemplate;
        global $objAssets;
        $data =  array();
        $data['assets'] = $objAssets->getAllActiveAssetsNotImported();        
        $objTemplate->setVariables('title', 'View Assets');
        $objTemplate->setView('templates/assets', 'statutory_maintenance_import', $data);   
    }    
    public static function post_maintenance_request()
    {
        if(isset($_POST["requestMaintenanceBtn"]))
        {
            global $objMaintenance;         
            global $objActions;
            
            $data = array();
            $data["source_id"] = $_POST["source_maintenance_id"];
            $data["source"] = $_POST["maintenance_type"];
            $data["due_date"] = $_POST["maintenanceSuggestedDueDate"];
            $data["priority"] = $_POST["maintenancePriority"];
            $data["description"] = $_POST["maintenanceDescription"];
            $data["notes"] = $_POST["maintenanceNotes"];
            $data["deparment"] = $_POST["maintenanceDepartment"];
            $response = $objMaintenance->addMaintenanceRequest($data);
            if($response)
            {   
                     //store action non-conformance
                    $data["source_id"] = (int)$_POST["source_maintenance_id"];
                    $data['source_type'] = "Non-Conformance";
                    $data['action_taken'] = "Requested Maintenance";
                    $data['action_taken_id'] = $objMaintenance->requested_id;
                    $data['action'] = $_POST["maintenanceDescription"];
                    if($objActions->hasCreatedAction($data))
                    {
                        self::view_requested_maintenance();
                    }                
            }
            else
            {                               
                      
            }
        }
    }
    public static function importAssets()
    {
        global $objAssets;
        $hasImported = false;
        if(isset($_POST["importAssets"]))
        {
            if(!empty($_POST["assetname"]))
            {
                $assets = $_POST["assetname"];
                $hasImported = false;
                foreach($assets as $asset)
                {
                    //call the insert method
                    $objAssets->addAssetToMaintenance($asset);
                    $hasImported = true;
                }
                if($hasImported == true)
                {
                    self::maintenance_view();
                }                
            }
        }
        if($hasImported == false)
        {
            self::viewAssetMaintenance();
        }
    }
    public static function create_maintenance_job()
    {
        if(isset($_POST["createJobBtn"]))
        {
            global $objJobs;
            $data = array();             
            $data["source_type"] = "Maintenance";        
            $data["job_description"] = $_POST["description"];            
            $data["start_date"] = $_POST["scheduledStartDateTime"];  
            $data["end_date"] = $_POST["endDateTime"];  
            $data["responsible_persons"] = $_POST["person_responsible_selected"];  
            $data["department_involved"] = $_POST["departments_selected"];  
            $data["maintenance_type"] = $_POST["maintenanceType"];  
            $data["job_details"] = $_POST["fullJobDescription"];  
            $data["risk_level"] = $_POST["risk_level"];  
            $data["hazard_types"] = $_POST["hazardTypes"];  
            $data["supplier_id"] = $_POST["companyName"];  
            $data["resources_used"] = $_POST["resources_used"];  
            $data["parts_used"] = $_POST["parts_used"];                          
            $data["approval_required"] = $_POST["approval_required"];   
            
            //foreach item which has been checked do what needs to be done
             $assets = $_POST["upcoming"];
             $hasImported = false;
             foreach($assets as $asset)
             {
                    //call the insert method             
                    $data["source_id"] = $asset;                          
                    $return = $objJobs->hasCreatedJob($data);
                    if($return)
                    {
                        $hasImported = true;
                    }
            }            
            if($hasImported)
            {
                /*redirect to the jobs table
                $data["source_id"] = (int)$_POST["source_maintenance_id"];
                $data['source_type'] = "Non-Conformance";
                $data['action_taken'] = "Job Created";
                $data['action'] = $_POST["job_description"];
                if($objActions->updateActionStatus($data))
                {
                    self::view_requested_maintenance();
                }        */
                self::view_maintenance_jobs();                
            }
            else
            {
                //display error and stay on the same page 
                self::view_requested_maintenance();
            }
        }
    }
    public static function create_maintenance_schedule()
    {
         if(isset($_POST["createJobBtn"]))
        {
            global $objJobs;
            $data = array();             
            $data["source_type"] = "Maintenance";        
            $data["job_description"] = $_POST["description"];            
            $data["start_date"] = $_POST["scheduledStartDateTime"];  
            $data["end_date"] = $_POST["endDateTime"];  
            $data["responsible_persons"] = $_POST["person_responsible_selected"];  
            $data["department_involved"] = $_POST["departments_selected"];  
            $data["maintenance_type"] = $_POST["maintenanceType"];  
            $data["job_details"] = $_POST["fullJobDescription"];  
            $data["risk_level"] = $_POST["risk_level"];  
            $data["hazard_types"] = $_POST["hazardTypes"];  
            $data["supplier_id"] = $_POST["companyName"];  
            $data["resources_used"] = $_POST["resources_used"];  
            $data["parts_used"] = $_POST["parts_used"];                          
            $data["approval_required"] = $_POST["approval_required"];   
            
            //foreach item which has been checked do what needs to be done
             $assets = $_POST["upcoming"];
             $hasImported = false;
             foreach($assets as $asset)
             {
                    //call the insert method             
                    $data["source_id"] = $asset;                          
                    $return = $objJobs->hasCreatedJob($data);
                    if($return)
                    {
                        $hasImported = true;
                    }
            }            
            if($hasImported)
            {
                //redirect to the jobs table
                self::view_maintenance_jobs();                
            }
            else
            {
                //display error and stay on the same page 
                self::view_requested_maintenance();
            }
        }
    }
    public static function signoff_maintenance_job()
    {
        
    }
}
