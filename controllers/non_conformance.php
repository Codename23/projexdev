<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class non_conformance extends controller
{
    
    //put your code here
//view all conformance
    public static function viewAllHistoryConformance()
    {
        global  $objNonConformance;
        global $objTemplate;
        global $objSheqTeam;
        global $objDepartment;
        global $objAssets;
        global $objEmployee;
        global $objSupplier;        
        $data = array();           
        $data['allNonConformance'] = $objNonConformance->viewAllConformance(1);
        $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
        $data['allResources'] = $objAssets->getAllAsset();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allSupplier'] = $objSupplier->getAllSuppliers($_SESSION['branch_id']); 
        $objTemplate->setVariables('title','Non Conformance');
        $objTemplate->setView('templates/non_conformance','non_conformance_history_tpl',$data);
    }   
    public static function viewAllArchiveConformance()
    {
        global  $objNonConformance;
        global $objTemplate;
        global $objSheqTeam;
        global $objDepartment;
        global $objAssets;
        global $objEmployee;
        global $objSupplier;        
        $data = array();           
        $data['allNonConformance'] = $objNonConformance->viewAllConformance(2);
        $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
        $data['allResources'] = $objAssets->getAllAsset();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allSupplier'] = $objSupplier->getAllSuppliers($_SESSION['branch_id']); 
        $objTemplate->setVariables('title','Non Conformance');
        $objTemplate->setView('templates/non_conformance','view_archive_non_conformance',$data);
    }
    public static function restore_non_conformance()
    {
         global  $objNonConformance;         
         $id = 0;
         if(isset($_GET['ncid']))
         {
             $id = (int)$_GET['ncid'];
         }
         $return = $objNonConformance->hasRestoredNonConformance($id);
         if($return == "succeed")
         {
            self::viewAllConformance();
         }
         else
         {
             self::viewAllArchiveConformance();
         }
    }
    public static function viewAllConformance($data = null)
    {
        global  $objNonConformance;
        global $objTemplate;
        global $objSheqTeam;
        global $objDepartment;
        global $objAssets;
        global $objEmployee;
        global $objSupplier;        
        $data = array();           
        $data['allNonConformance'] = $objNonConformance->viewAllConformance();
        $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
        $data['allResources'] = $objAssets->getAllAsset();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allSupplier'] = $objSupplier->getAllSuppliers($_SESSION['branch_id']); 
        $objTemplate->setVariables('title','Non Conformance');
        $objTemplate->setView('templates/non_conformance','index_tpl',$data);
    }  
    public static function editNonConformance($data = null)
    {
        global  $objNonConformance;
        global $objTemplate;
        global $objSheqTeam;
        global $objDepartment;
        global $objAssets;
        global $objEmployee;
        global $objSupplier;        
        $data = array();           
        $data['allNonConformance'] = $objNonConformance->viewAllConformance();
        $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
        $data['allResources'] = $objAssets->getAllAsset();
        $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
        $data['allSupplier'] = $objSupplier->getAllSuppliers($_SESSION['branch_id']); 
        $objTemplate->setVariables('title','Non Conformance');
        $objTemplate->setView('templates/non_conformance','edit_noc',$data);
    } 
    public static function non_conformance_view_report()
    {
        global  $objNonConformance;
        global $objTemplate;    
        $data = array();           
        $id = 0;
        if(isset($_GET["ncid"]))
        {
            $id = (int)$_GET["ncid"];
        }
        $data['allNonConformance'] = $objNonConformance->viewDetailsById($id);
        $objTemplate->setVariables('title','Non Conformance');
        $objTemplate->setView('templates/non_conformance','non_conformance_view_report_tpl',$data);  
    }
    public static function view_non_conformance_details()
    {
        global  $objNonConformance;
        global $objTemplate;    
        $data = array();           
        $id = 0;
        if(isset($_GET["ncid"]))
        {
            $id = (int)$_GET["ncid"];
        }
        $data['allNonConformance'] = $objNonConformance->viewDetailsById($id);
        $objTemplate->setVariables('title','Non Conformance');
        $objTemplate->setView('templates/non_conformance','view_non_conformance_details_tpl',$data);
    }
    public static function searchNonConformanceHistory()
    {
         if(isset($_POST['btnSearchConformanceHistory']))
        {
            global  $objNonConformance;
            global $objTemplate;
            global $objSheqTeam;
            global $objDepartment;
            global $objAssets;
            global $objEmployee;
            global $objSupplier;            
            
            $data = array();  
            $groups = "";
            $department = "";
            $status = "";
            if(!isset($_POST['groups_selected']))
            {
                $groups = "";                
            }
            if(isset($_POST['groups_selected']))
            {
                $groups = $_POST['groups_selected'];
            }
            if(!isset($_POST['departments_selected']))
            {
                $department = "";
            }
            if(isset($_POST['departments_selected']))
            {
                $department = $_POST['departments_selected'];
            }            
            if(!isset($_POST['status_selected']))
            {
                $status = "";
            }
            if(isset($_POST['status_selected']))
            {
                $status = $_POST['status_selected'];
            }
            $data['allNonConformance'] = $objNonConformance->searchNonConformance($groups,$department,$status,1);
            $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
            $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
            $data['allResources'] = $objAssets->getAllAsset();
            $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
            $data['allSupplier'] = $objSupplier->getAllSuppliers($_SESSION['branch_id']); 
            $data['results'] = $groups;
            $objTemplate->setVariables('title','Non Conformance');
            $objTemplate->setView('templates/non_conformance','non_conformance_history_tpl',$data);
        }
    }
    public static function searchNonConformance()
    {
        if(isset($_POST['btnSearchConformance']))
        {
            global  $objNonConformance;
            global $objTemplate;
            global $objSheqTeam;
            global $objDepartment;
            global $objAssets;
            global $objEmployee;
            global $objSupplier;            
            
            $data = array();  
            $groups = "";
            $department = "";
            $status = "";
            if(!isset($_POST['groups_selected']))
            {
                $groups = "";                
            }
            if(isset($_POST['groups_selected']))
            {
                $groups = $_POST['groups_selected'];
            }
            if(!isset($_POST['departments_selected']))
            {
                $department = "";
            }
            if(isset($_POST['departments_selected']))
            {
                $department = $_POST['departments_selected'];
            }            
            if(!isset($_POST['status_selected']))
            {
                $status = "";
            }
            if(isset($_POST['status_selected']))
            {
                $status = $_POST['status_selected'];
            }
            $data['allNonConformance'] = $objNonConformance->searchNonConformance($groups,$department,$status);
            $data['allDepartments'] = $objDepartment->getDepartmentByBranch($_SESSION['branch_id']);
            $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
            $data['allResources'] = $objAssets->getAllAsset();
            $data['allPersons'] = $objEmployee->getAllEmployees($_SESSION['company_id'],$_SESSION['branch_id']);
            $data['allSupplier'] = $objSupplier->getAllSuppliers($_SESSION['branch_id']); 
            $data['results'] = $groups;
            $objTemplate->setVariables('title','Non Conformance');
            $objTemplate->setView('templates/non_conformance','index_tpl',$data);
        }
    }
   public static function viewAssetsInvolved()
   {
        global $objAssets;
        $resource = array("status"=>404,"id"=>0,"category"=>"","type"=>"","description"=>"","number"=>"");
        $data2 = $objAssets->getAssesById($_POST['ass_id']);
        foreach($data2 as $asset)
        {
            $resource = array(
                "status"=>200,
                "id"=>$asset["ass_id"],
                "category"=>$asset["cat"],
                "type"=>$asset["tp"],
                "description"=>$asset["des"],
                "number"=>$asset["number"]);          
              break;
        }
        echo json_encode($resource);
   }
   public static function submitImprovementRequest()
   {
       echo "Test succeed";
   }   
   public static function viewPersonInvolved()
   {
        global $objEmployee;
        $resource = array("status"=>404,"id"=>0,"name"=>"","surname"=>"","department"=>"","occupation"=>"");
        $data2 = $objEmployee->getEmployeeById($_POST['person_id'],$_SESSION['branch_id']);
        foreach($data2 as $person)
        {
            $resource = array(
                "status"=>200,
                "id"=>$person["per_id"],
                "name"=>$person["firstname"],
                "surname"=>$person["lastname"],
                "department"=>$person["department_name"],
                "occupation"=>$person["occupation"]);          
              break;
        }
        echo json_encode($resource);
   }
   
   //add controller which connects to the model
    public static function addNoConformance()
    {
        global $objNonConformance; 
        global $objActions;
        $objNonConformanceData = array();          
        $objNonConformanceData['due_date'] = $_POST['dueDate'];    
        $objNonConformanceData['group_field'] = $_POST['group'];
        $objNonConformanceData['department_field'] = $_POST['nc_department'];
        $objNonConformanceData['non_conformance_details'] = $_POST['nonConformanceDetails'];
            
        $objNonConformanceData['recommended_improvement_field'] = $_POST['recommended_improvement_field'];
        $objNonConformanceData['resource_involved'] = $_POST['resource_inolved'];
        $objNonConformanceData['person_involved'] = $_POST['person_involved'];
        $objNonConformanceData['responsible_person'] = $_POST['responsible_person'];        
        $objNonConformanceData['supplier_id'] = $_POST['supplier_id']; 
        $objNonConformanceData['Investigate'] = $_POST['investigator']; 
        $objNonConformanceData['InvestigateDueDate'] = $_POST['investigatorDueDate'];         
        //if no employee, supplier , resource has been selected
                        
        //
        if(!empty($_FILES['uploadPhoto']))
        {
            //check the file size, check the               
            $target = $_SERVER['DOCUMENT_ROOT']."/sheq/frontend/media/uploads/";
            $target = $target . basename($_FILES['uploadPhoto']['name']);
            $encrypted_file = $_FILES['uploadPhoto']['tmp_name'];            
            if(move_uploaded_file($encrypted_file,$target))
            {
                $objNonConformanceData['uploadPhoto'] = $_FILES['uploadPhoto']['name'];  
            }          
        }
        else
        {
            $objNonConformanceData['uploadPhoto'] = null;
        }               
        $result = $objNonConformance->addNoConformance($objNonConformanceData);
        if($result)
        {
            $data["source_id"] = $objNonConformance->get_id;
            $data["source_name"] = "Non-Conformance";
            $data["recipient"] = $_POST["responsible_person"]; //responsible person
            $data["status"] = "Not Done";
            $data["subject"] = $_POST["nonConformanceDetails"];
            $response = $objActions->addToRequestList($data);
            if($response){
                
            }
            
        }
        self::viewAllConformance();                     
    }
    public static function signedOffNonConformance()
    {
      global  $objNonConformance;  
      global  $objSignedOff;
      if(isset($_POST['non_signed_id']))
      {          
         $data = array();
         $data["signedOffAction"] = $_POST["signoffActionTaken"];
         $data['signedOffLevel'] = "Master";
         $data["signedOffType"] = 1;
         $data['source_id'] = $_POST['non_signed_id'];
         $data["table"] = 'tbl_non_conformance';
         $result = $objSignedOff->hasSignedOffData($data);
      }     
      self::viewAllHistoryConformance();
    }
    public static function deleteNonConformance()
    {
        global $objNonConformance;
        $id = 0;
        if(isset($_POST["ncid"]))
        {
            $id = (int)$_POST["ncid"];
        }
        return $objNonConformance->hasDeletedNonConformance($id,$_POST['reason']);
    }
    public static function viewActions()
    {
        global $objActions;
        $actions = $objActions->viewActionsBySourceId($_POST["ass_id"]);
        $actionsTaken = array();
        foreach($actions as $action)
        {
            $status = $action["status"];
            if($status == "")
            {
                $status = "Processing";
            }
            $actionsTaken[] = array(
                "date"=>$action["dt"],
                "action_taken"=>$action["action_taken"],
                "RefNo"=>$action["id"],               
                "action"=>$action["action"],
                "created_by"=>$action["created_by"],
                "action_signed_off"=>$action["action_signed_off"],
                "status"=>$status,
                "action_signed_by"=>"<a href='#'>Sign Off</a>",
                "state"=>200);
        }
       echo json_encode($actionsTaken);
    }
    public static function addImprovementPost()
    {
        global $objImprovements;
        global $objTemplate;
        global $objNonConformance;
        $improvementData = array();
        $id = 0;
        $why = 0;
        if(isset($_POST['noc_id']))
        {
            $id = (int)$_POST['noc_id'];
        }
        else 
        {
            if(isset($_GET["ncid"]))
               {
                   $id = (int)$_GET["ncid"];
               }        
               if(isset($_GET["why_id"]))
               {
                   $why = (int)$_GET["why_id"];
               }
        }
        
        $improvementData["action_link_id"] = $id; 
        $improvementData["why_link_id"] = $why;
        $improvementData["group_id"] = $_POST['improve_group']; 
        $improvementData["improvement_type"] = $_POST['improvementType'];
        $improvementData["improvement_description"] = $_POST['improvementDescription'];
        $improvementData["responsible_person"] = $_POST['responsible_person'];
        $improvementData["managerial_approval"] = $_POST['managerialApproval'];
        $improvementData["sheqf_approval"] = $_POST['sheqfApproval'];
        $action = "";
        $redirectUrl = false;
        if(isset($_POST['action']))
        {
            $action = $_POST['action'];
            switch($action)
            {
                case 'Non Conformance':
                    $improvementData["requested"] = 1;
                    $redirectUrl = true;
                    break;
                case 'Investigation':
                    $improvementData["requested"] = 0;
                    $redirectUrl = true;
                    break;
                default:
                    $improvementData["requested"] = 0;
                    break;
            }
        }
        $improvementData["action_type"] = $action;        
        $result = $objImprovements->addImprovement($improvementData);
        if($redirectUrl)
        {
            
            //self::getRequestedImprovements();
        }
        else{
            //self::getActiveImprovements();
        }
    }
}
