<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Include the base controller
class critical_control extends Controller
{
    public static function critical_control_point() 
    {
        global $objTemplate;
        $data = array();
        $objTemplate->les('title','Occupation');        
        $objTemplate->setView('templates/control/','critical_control_point');
    }
    public static function viewControlSampling() 
    {
        global $objTemplate;
        $data = array();
        $objTemplate->les('title','Occupation');        
        $objTemplate->setView('templates/control/','critical_control_point_control_sampling',$data);
    }
    public static function addControlSampling() {
        global $objTemplate;
        $data = array();
        $objTemplate->les('title','Occupation');        
        $objTemplate->setView('templates/control/','critical_control_point_add_control_sampling',$data);
    }
    public static function viewControlSetup() {
        global $objTemplate;
        $data = array();
        $objTemplate->les('title','Occupation');        
        $objTemplate->setView('templates/control/','critical_control_point_setup',$data);
    }
    public static function postControlType()
    {
        //post control type
        
    }
    
}
