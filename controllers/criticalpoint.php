<?php
/**
 * Controller class containing methods to process all supplier actions
 *
 * @package sheqonline
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class criticalpoint extends controller{




    public static function critical_control_point(){
        global $objRiskAssessment;
        global $objTemplate;
        $data = array();
        $data['inCompleteRiskAssessments']= $objRiskAssessment->getAllInCompleteRiskAssessments($_SESSION['company_id']);
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/control','critical_control_point',$data);
        
    }
    ///setting section
    public static function view_settings()
    {
        global $objccp;
        global $objTemplate;
        global $objSheqTeam;
        $data = array();
        $data['allTypes']= $objccp->getAllControlPointTypes();
        $data['allSheqTeams'] = $objSheqTeam->getAllSheqTeamGroups();
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/control','critical_control_point_setup',$data);
    }
    public static function control_sampling()
    {
        //critical_control_point_control_sampling
        global $objRiskAssessment;
        global $objTemplate;
        $data = array();
        $data['inCompleteRiskAssessments']= $objRiskAssessment->getAllInCompleteRiskAssessments($_SESSION['company_id']);
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/control','critical_control_point_control_sampling',$data);
    }
    public static function post_control_types()
    {
         global $objccp;
         if(isset($_POST['addTypesBtn']))
         {
             $group = $_POST["group"];
             $type = $_POST["type"];
             $result = $objccp->addType($group,$type);
         }
         self::view_settings();
    }     
    public static function addControlPoint()
    {
        global $objRiskAssessment;
        global $objTemplate;
        $data = array();
        $data['inCompleteRiskAssessments']= $objRiskAssessment->getAllInCompleteRiskAssessments($_SESSION['company_id']);
        $objTemplate->setVariables('title','Risk Assessments');
        $objTemplate->setView('templates/control','critical_control_point_add_control_sampling',$data);
    }
    public static function defaultAction()
    {
        return ROOT.'/frontend/templates/login_tpl.php';
    }




}