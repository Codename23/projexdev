<?php

/**
 * Controller class containing methods to process all company related actions
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

//Include the base controller
include_once 'controller.php';

class incidents_ajax extends controller
{
    public static function incidentTypesByCategory() 
    {
        global $objIncidents;

        $id = $_POST["incidentCategory"];   
        $data['getAllIncidentTypesByCategory'] = $objIncidents->getIncidentTypesByCategoryId($id);

    }
    
}