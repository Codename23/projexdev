<?php
/**
 * Controller class containing methods to process all users related actions
 * 
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

//Include the base controller
include_once 'controller.php';

class users extends controller
{

    /**
     * var $objUsers The companies object
     * @access private
     */
    private $objUser;
    
    public function constructor() 
    {
        //Create instance of user object 
        //$this->objUsers = new user();
    }
    /**
     * Method to login
     * @return void
     */
    public static function login() 
    {
        global $objTemplate;
        $data = array();
        
        if(isset($_REQUEST['type']) && isset($_REQUEST['message'])){
            
            $data = array();
            $data['notification']['type'] = urldecode($_REQUEST['type']);
            $data['notification']['message'] = urldecode($_REQUEST['message']);
            
            $objTemplate->setVariables('title', 'Login');
            $objTemplate->setView('templates', 'login_tpl', $data);            
        } else {
            $objTemplate->setVariables('title', 'Login');
            $objTemplate->setView('templates', 'login_tpl', $data);
        }    
    }
    
    /**
     * Method to logout
     * @return void
     */
    public static function logout() 
    {
        global $objUsers;
        $data = array();
        
        $response = $objUsers->logOut();
        $data['type'] = 'success';
        $data['message'] = 'You have successfully logged out';
        controller::nextPage('defaultAction','default', $data);
    }
    /**
     * Method to process login
     * @return void
     */
    public static function processlogin() 
    {
        global $objUsers;
        $data =  array();
        
        $username = $_POST['username'];
        $password = $_POST['password'];
        $response = $objUsers->logIn($username, $password);

        if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']){
            controller::nextPage('dashboard','users');
        } else {
            $data = array();
            $data['type'] = 'danger';
            $data['message'] = $response;
            controller::nextPage('defaultAction','default', $data);
        }
    }
    /**
     * Method to dashboard
     * @return void
     */
    public static function dashboard() 
    {
        global $objTemplate;
        
        if(isset($_REQUEST['type']) && isset($_REQUEST['message'])){
            
            $data = array();
            $data['notification']['type'] = urldecode($_REQUEST['type']);
            $data['notification']['message'] = urldecode($_REQUEST['message']);
            $objTemplate->setVariables('title', 'Dashboard');
            $objTemplate->setView('templates', 'dashboard_tpl');
        } else {
            $objTemplate->setVariables('title', 'Dashboard');
            $objTemplate->setView('templates', 'default_tpl');
        }    
    }
    /**
     * Method to view users
     * @return void
     */
    public static function view() 
    {
        global $objTemplate;
        global $objUsers;
        
        $data = array();
        $data['users'] = $objUsers->getUsers();
        
        if(isset($_REQUEST['type']) && isset($_REQUEST['message'])){
             
            $data['notification']['type'] = urldecode($_REQUEST['type']);
            $data['notification']['message'] = urldecode($_REQUEST['message']);
            
            $objTemplate->setVariables('title', 'User Management');
            $objTemplate->setView('templates', 'users/user_management_tpl', $data);
        
        }else{
        
            $objTemplate->setVariables('title', 'User Management');
            $objTemplate->setView('templates', 'users/user_management_tpl', $data);
        }
    }
    /**
     * Method to display template
     */
    public static function addUser() 
    {
        global $objTemplate;

        $objTemplate->setVariables('title', 'Add User');
        $objTemplate->setView('templates', 'users/adduser_tpl');   
    }
    /**
     * Method to save user
     * @return void
     */
    public static function saveUser() 
    {
        global $objUsers;
        $userDetailsArr = array();
        $data = array();
        
        if($_POST['submit']=="cancel"){
            controller::nextPage('view','users');
        }else{
            $userDetailsArr['firstname'] = $_POST['firstname'];
            $userDetailsArr['lastname'] = $_POST['lastname']; 
            $userDetailsArr['username'] = $_POST['username'];
            $userDetailsArr['password'] = $_POST['password'];  
            $response = $objUsers->addUser($userDetailsArr);
           
            if($response){  
                $data['type'] = 'success';
                $data['message'] = 'Your details are registered.';
                controller::nextPage('view','assets', $data=[]);  
            }else{
                $data['type'] = 'warning';
                $data['message'] = 'Oops! Something went wrong causing user creation to be unsuccessful. Please log all bug <a href="#" >here</a>';
                controller::nextPage('view','assets', $data);
            } 
        }   
    }   
}