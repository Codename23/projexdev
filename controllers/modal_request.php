<?php
/**
 * Controller class containing methods to process all modal requests
 *
 * @package sheqonline
 * @author Brian Douglas<douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */


class modal_request {



    public function __construct()
    {

    }


    /**
     * Handle ajax call for regions
     */
    public static function modalAddEmployee(){

        global $objTemplate;
        global $objCompanies;
        global $objEmployee;

        $data = array();

        $company_id = $_SESSION['company_id'];
        $userType = 5;

        $data['companyBranches'] = $objCompanies->getCompanyBranches($company_id);
        $data['allEmployeeFields'] = $objEmployee->getEmployeeFormDetails($userType);
        $data['gender'] = $objEmployee->getEmployeeGender();
        $data['ethnic'] = $objEmployee->getEmployeeRace();

        $objTemplate->setVariables('title','Add Employee');
        $objTemplate->setView('templates/risk_assessment/modals','add_internal_person_tpl',$data);
    }

    public static function modalAddServiceProvider(){

        global $objTemplate;

        $objTemplate->setVariables('title','Add Service Provider');
        $objTemplate->setView('templates/risk_assessment/modals','add_external_person_tpl',$data = []);


    }

    public static function modalAddRiskAssessment(){

        global $objTemplate;

        $objTemplate->setVariables('title','Add Risk Assessment');
        $objTemplate->setView('templates/risk_assessment/modals','add_risk_assessment_m_tpl',$data = []);


    }


    public static function modalUpdateRiskRecommendations(){
        global $objRiskAssessment;
        global $objTemplate;

        $data['recommendationInfo'] = $objRiskAssessment->getRecommendationInfo($_GET['id']);
        $objTemplate->setVariables('title','Add Risk Assessment');
        $objTemplate->setView('templates/risk_assessment/modals','update_recommendation_tpl',$data);
    }

    public static function modalEditRiskAssessment(){
        global $objRiskAssessment;
        global $objTemplate;


        $data['riskAssessmentInfo']= $objRiskAssessment->getRiskAssessmentInfo($_GET['id']);

        $data['riskAssessmentRecommendations']= $objRiskAssessment->getRiskAssessmentsRecommendationTypes();
        $data['riskRecoo']= $objRiskAssessment->getAllRiskAssessmentsRecommendations($_GET['id']);

        $objTemplate->setVariables('title','Edit Risk Assessment');
        $objTemplate->setView('templates/risk_assessment/modals','edit_risk_assessment_m_tpl',$data );


    }


    public static function modalEditAddScore(){

        global $objTemplate;


        $objTemplate->setVariables('title','Add Score');
        $objTemplate->setView('templates/risk_assessment/modals','add_score_tpl',$data =[] );


    }

    public static function modalEditRiskRecommendations(){

        global $objRiskAssessment;
        global $objTemplate;


        $data['riskRecommendations']= $objRiskAssessment->getAllRiskAssessmentsRecommendations($_GET['recommendationId']);
        $objTemplate->setVariables('title','Risk Assessment Recommendations');
        $objTemplate->setView('templates/risk_assessment/modals','edit_risk_recommendations_m_tpl',$data);

    }

    public static function ajaxAddEditFolder(){
        global $objTemplate;
        global $objFolders;


        //Load all folders
        $data['folderListing'] =  $objFolders->getAllFolderCompany($_SESSION['company_id']);
        // initial parent folder
        $data['parentId'] = '-1';
        if(isset($_POST['parentId']))
        {
            $data['parentId'] = intval($_POST['parentId']);
        }

        if((int)$_POST['selectedFolderId'])//edit folder
        {
            $data['folderInfo'] = $objFolders->getFolderInfo($_POST['selectedFolderId'],$_SESSION['company_id']);
            $objTemplate->setVariables('title','Edit Folder');
            $objTemplate->setView('templates/documents/modals','edit_folder_m_tpl',$data);

        }else{//add new folder

            $objTemplate->setVariables('title','Add New Folder');
            $objTemplate->setView('templates/documents/modals','add_folder_m_tpl',$data);

        }



    }

    /**
     * Add appointment setting
     */
    public static function modalAddAppointmentSettings(){

        global $objRiskAssessment;
        global $objTemplate;


        $data['riskRecommendations']= $objRiskAssessment->getAllRiskAssessmentsRecommendations($_GET['recommendationId']);
        $objTemplate->setVariables('title','Risk Assessment Recommendations');
        $objTemplate->setView('templates/risk_assessment/modals','edit_risk_recommendations_m_tpl',$data);

    }

    /**
     * Edit appointment setting
     */
    public static function modalEditAppointmentSettings(){

        global $objSheqTeam;
        global $objDocument;
        global $objTemplate;


        $data['appointmentSettingInfo']= $objSheqTeam->getAppointmentSettingInfo($_GET['settingId']);
        $data['appointmentGroups'] = $objSheqTeam->getAllSheqTeamGroups();
        $data['appointmentDocuments'] = $objDocument->getAllApprovedDocument($_SESSION['company_id']);
        $data['frequencies'] = $objSheqTeam->getRenewalFrequency();

        $objTemplate->setVariables('title','Appointment Setting');
        $objTemplate->setView('templates/sheq_team/modals','edit_appointment_setting_m',$data);

    }

    /**
     * Edit appointment
     */
    public static  function  modalEditAppointmentAssignment(){

        global $objSheqTeam;
        global $objEmployee;
        global $objTemplate;

        $data['appointmentAssignmentInfo']= $objSheqTeam->getAppointmentAssignmentInfo($_GET['appointmentId']);
        $data['appointmentGroups'] = $objSheqTeam->getAllSheqTeamGroups();
        $data['appointmentEmployees'] = $objEmployee->getAllEmployeeByBranch($_SESSION['branch_id']);

        $objTemplate->setVariables('title','Appointment Setting');
        $objTemplate->setView('templates/sheq_team/modals','edit_appointment_assignment_m',$data);


    }


}