<?php
/**
 * Controller class containing methods to process all ajax requests
 *
 * @package sheqonline
 * @author Brian Douglas<douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */


class ajax_request {


    public function __construct()
    {

    }


    /**
     * Handle ajax call for regions
     */
        //view asset involved
    public static function ajaxCountryRegions(){

        global $objRegions;

        if(!empty($_POST)){
            if(isset($_POST['countryId'])){

                $allRegions= $objRegions->getCountryRegions($_POST['countryId']);

                if(!empty($allRegions)){
                    foreach($allRegions as $region){
                        $data[] = array("id"=>$region["id"], "name"=>$region["name"]);
                    }
                }else{
                    $data[] = array("id"=>"0", "name"=>"No Branch Found");
                }
                //return the result in jason
                echo json_encode($data);
            }
        }
    }

    /**
     * Handle ajax call for branches
     */
    public static function ajaxCompanyBranches(){

        global $objCompanies;

        if(!empty($_POST)){
            if(isset($_POST['companyId'])){

                $defaultBranch = $objCompanies->getCompanyHODBranch($_POST['companyId']);
                $companyInfo = $objCompanies->getCompanyInfo($_POST['companyId']);
                $_SESSION['defaultCompany'] = $companyInfo['company_name'];
                $_SESSION['company_id'] = $companyInfo['id'];
                $_SESSION['defaultBranch'] = null;
                $_SESSION['branch_id'] = null;

                if(!empty($defaultBranch)){
                    $_SESSION['defaultBranch'] = $defaultBranch['branch_name'];
                    $_SESSION['branch_id'] = $defaultBranch["id"];
                    $data[] = array("id"=>$defaultBranch["id"], "name"=>$defaultBranch["branch_name"]);

                }else{
                    $data[] = array("id"=>"0", "name"=>"No Branch Found");
                }
                //return the result in jason
                echo json_encode($data);
            }
        }
    }

    public static function ajaxDefaultBranch(){

        global $objCompanies;
        if(!empty($_POST)){
            if(isset($_POST['branchId'])){

                $branchInfo = $objCompanies->getBranchInfo($_POST['branchId']);
                $_SESSION['defaultBranch'] = $branchInfo['branch_name'];
                $_SESSION['branch_id'] = intval($_POST['branchId']);
                echo json_encode(array('value'=>true));
            }
        }else{
            $_SESSION['branch_id'] = null;
        }

    }


    /**
     * Handle ajax call on branch select options
     */
    public static function ajaxBranchDepartments(){
        global $objCompanies;

        if(!empty($_POST)){

            if(isset($_POST['branch_id'])){
                $allDepartments = $objCompanies->getBranchDepartments($_POST['branch_id']);

                if(!empty($allDepartments)){
                    foreach($allDepartments as $allDepartment){
                        $data[] = array("id"=>$allDepartment["id"], "name"=>$allDepartment["department_name"]);
                    }
                }else{
                    $data[] = array("id"=>"0", "name"=>"No Departments Found");
                }
                //return the result in jason
                echo json_encode($data);

            }
        }

    }

    /**
     * Handle ajax call for occupations
     */
    public static function ajaxDepartmentsOccupations(){

        global $objOccupations;

        if(!empty($_POST)){
            if(isset($_POST['department_id'])){
                $allOccupations = $objOccupations->getDepartmentOccupations($_POST['department_id']);

                if(!empty($allOccupations)){
                    foreach($allOccupations as $occupation){
                        $data[] = array("id"=>$occupation["id"], "name"=>$occupation["name"]);
                    }
                }else{
                    $data[] = array("id"=>"0", "name"=>"No Occupation Found");
                }
                //return the result in jason
                echo json_encode($data);
            }
        }
    }


    /**
     * Handle ajax call for resources
     */
    public static function ajaxAssetType(){

        global $objAssets;;

        if(!empty($_POST)){
            if(isset($_POST['categoryId'])){

                $resourcesTypes = $objAssets->getAssetTypeByCategory($_POST['categoryId']);

                if(!empty($resourcesTypes)){
                    foreach($resourcesTypes as $resourceType){
                        $data[] = array("id"=>$resourceType["id"], "name"=>$resourceType["name"]);
                    }
                }else{
                    $data[] = array("id"=>"0", "name"=>"No resource Type  Found");
                }
                //return the result in jason
                echo json_encode($data);
            }
        }
    }

    /**
     * Handle ajax call for resources
     */
    public static function ajaxSelectRisAssessmentResource(){

        global $objAssets;

        if(!empty($_POST)){
            if(isset($_POST['departmentId']) && isset($_POST['resourceCategory']) && isset($_POST['resourceType'])){
                $allResources = $objAssets->getAllDepartmentCategoryTypes($_POST['departmentId'],$_POST['resourceCategory'],$_POST['resourceType']);

                if(!empty($allResources)){
                    foreach($allResources as $resource){
                        $data[] = array("id"=>$resource["departmentid"].'#'.$resource["id"], "name"=>$resource["name"]);
                    }
                }else{
                    $data[] = array("id"=>"0", "name"=>"No resource  Found");
                }
                //return the result in jason
                echo json_encode($data);
            }
        }
    }



    public static function ajaxAddNewRiskAssessment(){

        global $objAssets;;

        if(!empty($_POST)){
            if(isset($_POST['departmentId']) && isset($_POST['resourceCategory']) && isset($_POST['resourceType']) && isset($_POST['selectResource']) ){


                print_r($_POST);
                exit;
                $allResources = $objAssets->getAllByDepartmentCategoryType($_POST['departmentId'],$_POST['resourceCategory'],$_POST['resourceType']);

                if(!empty($allResources)){
                    foreach($allResources as $resource){
                        $data[] = array("id"=>$resource["id"], "name"=>$resource["name"]);
                    }
                }else{
                    $data[] = array("id"=>"0", "name"=>"No resource  Found");
                }
                //return the result in jason
                echo json_encode($data);
            }
        }
    }

    /**
     * Method to search employees
     */
    public static function ajaxSearchForEmployee(){

        global $objEmployee;
        global $objTemplate;
        $data = array();

        if(!empty($_POST)){
            if(isset($_POST['employeeData'])){

                $data['searchedEmployee']  = $_POST['employeeData'];
                $data['allEmployeeResults'] = $objEmployee->searchEmployee($_POST['employeeData'],$_SESSION['company_id']);

                unset($_POST['employeeData']);
                $objTemplate->setVariables('title','Edit Risk Assessment');
                $objTemplate->setView('templates/risk_assessment/ajax_view','assistance_results',$data );
            }
        }
    }

    /**
     * Method to search employees
     */
    public static function ajaxSearchForEmployeeDocuments(){

        global $objEmployee;
        global $objTemplate;
        $data = array();

        if(!empty($_POST)){
            if(isset($_POST['employeeData'])){

                $data['searchedEmployee']  = $_POST['employeeData'];
                $data['allEmployeeResults'] = $objEmployee->searchEmployee($_POST['employeeData'],$_SESSION['company_id']);

                unset($_POST['employeeData']);
                $objTemplate->setVariables('title','Edit Risk Assessment');
                $objTemplate->setView('templates/risk_assessment/ajax_view','assistance_results',$data );
            }
        }
    }

    /**
     * Method to search employees
     */
    public static function ajaxSearchForUserDocuments(){

        global $objEmployee;
        global $objTemplate;
        $data = array();

        if(!empty($_POST)){
            if(isset($_POST['employeeData'])){

                $data['searchedEmployee']  = $_POST['employeeData'];
                $data['allEmployeeResults'] = $objEmployee->searchEmployee($_POST['employeeData'],$_SESSION['company_id']);

                unset($_POST['employeeData']);
                $objTemplate->setVariables('title','Edit Risk Assessment');
                $objTemplate->setView('templates/risk_assessment/ajax_view','assistance_results',$data );
            }
        }
    }


    /**
     * Method to get side bar side menus
     */
    public static function ajaxSideMenuFolders(){

        global $objFolders;
        global $objTemplate;
        $data = array();
        $folderId = isset($_GET['folder'])? $_GET['folder'] : null;
        $data['folders'] = $objFolders->getFoldersAndFiles($folderId,$_SESSION['company_id'],$_SESSION['branch_id']);
        $objTemplate->setVariables('title','Edit Risk Assessment');
        $objTemplate->setView('templates/documents/ajax_pages','_side_menu_details_tpl',$data);
    }




    /**
     * Method to show folder details
     */
    public  static function ajaxShowFolderDetails(){
        global $objFolders;
        global $objTemplate;
        $data = array();

        $data['folderStats'] = $objFolders->getFoldersStats($_POST['nodeId'],$_SESSION['company_id']);
        $data['folders'] = $objFolders->folderDetailsView($_POST['nodeId'],$_SESSION['company_id'],$_SESSION['branch_id']);
        $data['files'] = $objFolders->folderFilesDetailsView($_POST['nodeId'],$_SESSION['company_id'],$_SESSION['branch_id']);

        $objTemplate->setVariables('title','Edit Risk Assessment');
        $objTemplate->setView('templates/documents/ajax_pages','_folder_details_ajax_tpl',$data);
    }

    /**
     * Method to show file details
     */
    public static function ajaxShowFileDetails(){


        global $objFolders;
        global $objTemplate;
        $data = array();

        $data['fileInfo'] = $objFolders->getFileInformation($_POST['fileId'],$_SESSION['company_id']);

        $objTemplate->setVariables('title','File Details');
        $objTemplate->setView('templates/documents/ajax_pages','_file_details_ajax_tpl',$data);

    }

    /**
     * Method to update folder view types
     */
    public static  function  ajaxUpdateFolderView(){

        if(isset($_POST['folderViewType'])){
            if(in_array($_POST['folderViewType'], array('fileManagerIcon', 'fileManagerList')))
            {
                $_SESSION['folderViewType'] = $_POST['folderViewType'];
            }
        }
    }


    /**
     * Method to remove a folder
     */
    public  static function ajaxRemoveFolder(){

        global $objFolders;
        $data = array();

        if(isset($_POST['folderId'])){

            $isDeleted  = $objFolders->removeSelectedFolder($_POST['folderId']);

            if($isDeleted){
                $data[] = array("error"=>false, "message"=>"Folder Successfully deleted");
            }else{
                $data[] = array("error"=>true, "message"=>"Error deleting Folder");

            }
        }else{

            $data[] = array("error"=>true, "message"=>"Please select folder to delete");
        }

        echo json_encode($data);
    }

    /**
     * Method to search employee by occupation
     *
     */
    public static function searchEmployeeOccupations(){
        global $objEmployee;
        $data = array();

        if(!empty($_POST)){
            $employeeDetails = $objEmployee->searchEmployeeByOccupation($_POST['occupation']);
            if($employeeDetails){
                foreach($employeeDetails as $employeeDetailsArr){
                    $data[] = array("id"=>$employeeDetailsArr["id"], "firstname"=>$employeeDetailsArr["firstname"], "lastname"=>$employeeDetailsArr["lastname"], "field_data"=>$employeeDetailsArr["field_data"]);
                }
            }
            echo json_encode($data);
        }
    }

    /**
     * Method to get selected group appointments
     */
    public static function ajaxGroupAppointments(){
        global $objSheqTeam;

        $data = array();

        if(!empty($_POST)){
            $appointments = $objSheqTeam->getAppointmentSettingByGroup($_POST['groupId']);
            if($appointments){
                foreach($appointments as $appointment){
                    $data[] = array("id"=>$appointment["id"], "name"=>$appointment["appointment_name"]);
                }
            }
        }
        echo json_encode($data);
    }

    /**
     * Method to increment date depending on occurrence
     */
    public static function ajaxNextOccurrence(){

        global $objSheqTeam;

        $data = array();

        if(!empty($_POST)){
            $appointmentData = $objSheqTeam->getAppointmentSettingInfo($_POST['appointmentId']);
            if($appointmentData){
                $expiryDate = self::_increment_date($_POST['appointmentDate'],$appointmentData['frequency_name']);
                $data[] = array("expiry_date"=>$expiryDate);
            }
        }
        echo json_encode($data);

    }

    /**
     * Method to increment occurrence
     * @param $date
     * @param $increment
     * @return string
     */
    function _increment_date($date, $increment)
    {
        $new_date = new DateTime($date);
        $new_date->add(new DateInterval('P' . $increment));

        return $new_date->format('Y-m-d');
    }
    

}